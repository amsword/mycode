from pymongo import MongoClient
import cPickle as pickle

# get the tree structure of the category

host_name = "54.65.168.46"
port = 27017
cached_file = './working_taobao/cached_taobao_itemdetail'

client = MongoClient(host_name, port)
db = client["taobaodm"]
items = db['taobao-itemdetail']

cached_items = [];
cached_keys = ['_id', 'crumbs_items', 'attributes', 'catid', 'catpath', \
    'showing_image', 'image_urls']

empty_count = 0;
num_count = items.count();
curr_count = 0;
save_idx = 0;
print curr_count, num_count, empty_count;
for item in items.find():
    if (curr_count % 100) == 0:
        print curr_count, num_count, empty_count;
    curr_count = curr_count + 1;

    if (curr_count % 1000000) == 0:
        with open(cached_file + '_' + str(save_idx), 'wb') as fp:
            pickle.dump(cached_items, fp, pickle.HIGHEST_PROTOCOL);
            save_idx = save_idx + 1;
            cached_items = [];
    cutted = {}
    for key in cached_keys:
        if item.has_key(key):
            cutted[key] = item[key];
    if not cutted:
        empty_count = empty_count + 1;
    else:
        cached_items.append(cutted);

with open(cached_file + '_' + str(save_idx), 'wb') as fp:
    pickle.dump(cached_items, fp, pickle.HIGHEST_PROTOCOL);
    save_idx = save_idx + 1;
    cached_items = [];


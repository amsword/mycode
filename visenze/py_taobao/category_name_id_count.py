# coding: utf-8
from category_rule import category_rule
from scipy import logical_or
import yaml
import sets;
import numpy as np;
import cPickle as pickle
from label_tree_structure import get_label_tree
import msgpack
from multiprocessing import Pool
from scipy import misc
import uuid;
import os;
import urllib
import time

def get_highest_category():
    category_list = '/home/jianfeng/working/taobao/label_tree_selected_sub_group.txt';
    with open(category_list, 'r') as fp:
        all_lines = fp.readlines();
    category_info = [];
    curr_name = None;
    curr_group_names = [];
    for line in all_lines:
        line = line.replace('\n', '');
        line = line.replace(' ', '');
        if ':' in line:
            if curr_name != None:
                category_info.append((curr_name, curr_group_names));
                curr_name = None;
                curr_group_names = [];
            names = line.split(':');
            assert(len(names) == 2)
            curr_name = names[1];
        elif len(line) > 0:
            curr_group_names.append(line);
    if curr_name != None:
        category_info.append((curr_name, curr_group_names));
        curr_name = None;
        curr_group_names = [];
    return category_info;

category_info = get_highest_category();

# load all the items 
#def load_all_item():
    #cached_file = '/home/jianfeng/working/taobao/cached_taobao_itemdetail'
    #num_cached = 7
    #all_items = [];
    #for i in range(num_cached):
        #with open(cached_file + "_" + str(i), 'r') as fp:
            #print 'start to load...'
            #cached_items = pickle.load(fp);
            #print 'finish loading.'
        #all_items.extend(cached_items)
    #return all_items
def load_all_item():
    cached_file = '/home/jianfeng/working/taobao/cached_selected_taobao_itemdetail'
    with open(cached_file, 'rb') as fp:
        print 'start to load...'
        all_items = pickle.load(fp);
        print 'ended'
    return all_items

all_items = load_all_item();

# collected all the ids for all_items

def generate_ids_array(all_items):
    ids_array = None
    ids_list = [];
    idx_list = [];
    idx_item = 0;
    for item in all_items:
        one_id = [];
        if item.has_key('catid'):
            one_id.append(item['catid'])
        if item.has_key('catpath'):
            for str_id in item['catpath'].split('-'):
                one_id.append(int(float(str_id)))
        if item.has_key('crumbs_items'):
            sub_items = item['crumbs_items'];
            for each in sub_items:
                if each.has_key('url'):
                    url = each['url']
                    start_idx = url.find('cat=');
                    end_idx = url.find('&from');
                    str_id = url[start_idx + 4 : end_idx];
                    one_id.append(int(float(str_id)));
        id_list = list(sets.Set(one_id));
        ids_list.extend(id_list);
        idx_list.extend([idx_item] * len(id_list))
        idx_item = idx_item + 1;
    return np.array(ids_list), np.array(idx_list)  

[ids_array, idx_array] = generate_ids_array(all_items);

# get the category relationship
#label_tree = get_label_tree();

# get the node of leaf's parents
#category_groups = [];
#for name, sub_groups in category_info:
    #sub = []
    #for sub_group in sub_groups:
        #nodes = label_tree.search_nodes(semantic_name = sub_group);
        #good_nodes = [];
        #for node in nodes:
            #if node.is_leaf():
                #continue;
            #is_all_child_leaf = True;
            #for child in node.children:
                #if not child.is_leaf():
                    #is_all_child_leaf = False;
            #if is_all_child_leaf:
                #good_nodes.append(node)
        #assert(len(good_nodes) == 1)
        #good_node = good_nodes[0]
        #sub.append(good_node)
    #category_groups.append(sub)

# save the category name and the id into a file
#file_name_catid = '/home/jianfeng/working/taobao/category_name_catid.txt'
#all_lines = [];
#num_groups = len(category_groups);
#for i in range(num_groups):
    #category_group = category_groups[i];
    #name = category_info[i][0];
    #one_line = str(i) + ":" + name + '\n'
    #all_lines.append(one_line)
    #for category in category_group:
        #one_line = category.semantic_name + ': ' + str(category.catid) + "\n"
        #all_lines.append(one_line)
        #for category_child in category.children:
            #one_line = '\t' + category_child.semantic_name + ": " + str(category_child.catid) + '\n'
            #all_lines.append(one_line)

#with open(file_name_catid, 'w') as fp:
    #fp.writelines(all_lines)

#def item_count(ids_array, catid):
    #return np.sum(ids_array == catid);

# save the category name, the id, the count into a file
#file_name_catid = '/home/jianfeng/working/taobao/category_name_catid.txt'
#all_lines = [];
#num_groups = len(category_groups);
#for i in range(num_groups):
    #print i
    #category_group = category_groups[i];
    #name = category_info[i][0];
    #one_line = str(i) + ":" + name + '\n'
    #all_lines.append(one_line)
    #for category in category_group:
        ## parents
        #catid = category.catid;
        #assert catid == None;
        #has_id = category.up;
        #while True:
            #if has_id.is_root():
                #break;
            #if has_id.catid != None:
                #break;
            #else:
                #has_id = has_id.up;
                #break;
                
        #count = item_count(ids_array, has_id.catid);
        #one_line = category.semantic_name + ': ' + str(catid) + '. ' + has_id.semantic_name + ': ' + str(has_id.catid) + ': ' + str(count) + '; ' + str(category in has_id.children) + '; ' + str(len(has_id.children)) + "\n";
        #all_lines.append(one_line)
        ## all leaf's
        #for category_child in category.children:
            #catid = category_child.catid;
            #if catid != None:
                #count = item_count(ids_array, catid)
                #one_line = '\t' + category_child.semantic_name + ': ' + str(catid) + ': ' + str(count) + '\n'
            #else:
                #one_line = '\t' + category_child.semantic_name + ': ' + str(catid) + '\n'
            #all_lines.append(one_line)

#with open(file_name_catid, 'w') as fp:
    #fp.writelines(all_lines)

## get the attribute info for each category
#def read_category_ids():
    #file_name = '/home/jianfeng/working/taobao/category_ids.txt'
    #with open(file_name, 'r') as fp:
        #all_lines = fp.readlines()
    
    #all_ids = []
    #all_name = []
    #for line in all_lines:
        #ids = []
        #line_splits = line.split(':');
        #ids = [int(float(s)) for s in line_splits[1 : ]];
        #all_ids.append(ids);
        #all_name.append(line_splits[0]);
    #return all_name, all_ids

#def retrieve_image_list(np_ids, np_idx, ids):
    #all_selected = [];
    #for one_id in ids:
        #one_selected = np_idx[np_ids == one_id];
        #assert np.sum(one_selected != np.unique(one_selected)) == 0
        #all_selected.append(one_selected);
    #return np.unique(np.concatenate(all_selected));

def collect_attribute(items):
    result = {}
    for item in items:
        if not item.has_key('attributes'):
            continue;
        attr = item['attributes'];
        for key, value in attr.iteritems():
            if result.has_key(key):
                curr_value = result[key];
            else:
                result[key] = {};
                curr_value = result[key];
            if curr_value.has_key(value):
                curr_value[value] = curr_value[value] + 1;
            else:
                curr_value[value] = 1;
    return result

def print_partial_attribute(attr_count, th_key = 1000, th_value = 100):
    all_key = [];
    all_count = []
    for key, value in attr_count.iteritems():
        c = 0;
        for value_key, value_count in value.iteritems():
            c = c + value_count;
        all_count.append(c)
        all_key.append(key);
    s_idx = sorted(range(len(all_count)), key = lambda k: all_count[k], reverse = True);
    all_lines = [];
    for idx in s_idx:
        one_line = ''
        key = all_key[idx];
        key_count = all_count[idx];
        if key_count < th_key:
            continue
        one_line = one_line + key.encode('u8') + ': ' + str(key_count) + '\n'
        all_lines.append(one_line)
        all_value = [];
        all_value_count = [];
        for value, value_count in attr_count[key].iteritems():
            all_value.append(value);
            all_value_count.append(value_count);
        si = sorted(range(len(all_value_count)), key = lambda k: all_value_count[k], reverse = True);
        for sub_idx in si:
            sub_value = all_value[sub_idx];
            if ' ' in sub_value:
                continue;
            sub_value_count = all_value_count[sub_idx];
            if sub_value_count < th_value:
                break;
            one_line = '\t' + sub_value.encode('u8') + ': ' + str(sub_value_count) + '\n'
            all_lines.append(one_line)
    return all_lines

# all_item = load_all_item();
# all_name, all_ids = read_category_ids()
# [ids_array, idx_array] = generate_ids_array(all_items);
# np_ids, np_idx = generate_ids_array(all_items)

#num_cat = len(all_name);
#out_file_name = '/home/jianfeng/working/taobao/ranked_key_value_count_'
#is_save_spaced = False
#all_lines = [];
#for i in range(num_cat):
    #ids = all_ids[i];
    #selected_idx = retrieve_image_list(np_ids, np_idx, ids);
    #selected_items = [all_items[selected_idx[j]] for j in range(len(selected_idx))]
    #selected_attrs = collect_attribute(selected_items)
    #all_lines = print_partial_attribute(selected_attrs, is_save_spaced)
    #if is_save_spaced:
        #fn = out_file_name + '_with_spaced_value_' + all_name[i] + '.txt';
    #else:
        #fn = out_file_name + '_no_spaced_value_' + all_name[i] + '.txt'
    #with open(fn, 'w') as fp:
        #fp.writelines(all_lines)

def apply_rules(sub_items, rules, ids_array = [], idx_array = []):
    mask = np.array([False] * len(sub_items))
    for rule_method, rule_param in rules.iteritems():
        if rule_method == 'attr_key':
            for each_key in rule_param:
                sub_mask = [sub_item.has_key('attributes') and
                        sub_item['attributes'].has_key(each_key) for sub_item in sub_items]
                sub_mask = np.array(sub_mask);
                mask = logical_or(sub_mask, mask);
        elif rule_method == 'catid':
            all_selected = [];
            for one_id in rule_param:
                one_selected = idx_array[ids_array == one_id]
                sub_mask = np.array([False] * len(sub_items));
                sub_mask[one_selected] = True;
                mask = logical_or(mask, sub_mask)
        elif rule_method == 'attr_key_value':
            for one_key, one_value in rule_param:
                sub_mask = [sub_item.has_key('attributes') and
                        sub_item['attributes'].has_key(one_key) and
                        sub_item['attributes'][one_key] == one_value for sub_item in sub_items]
                sub_mask = np.array(sub_mask);
                mask = logical_or(sub_mask, mask);
    for rule_method, rule_param in rules.iteritems():
        if rule_method == 'not_attr_key':
            for each_key in rule_param:
                for idx in range(len(mask)):
                    if mask[idx] and sub_items[idx]['attributes'].has_key(each_key):
                        mask[idx] = False;
    return mask


#[ids_array, idx_array] = generate_ids_array(all_items);
category_list = {};
# load category rules
category_rules_name = './working_taobao/category_rule.yaml'
with open(category_rules_name, 'r') as fp:
    category_rule = yaml.load(fp);
all_low_name = ['clothes->top', 'clothes->trousers', 'clothes->dress'];
for low_name in all_low_name:
    file_name = './working_taobao/attr_' + low_name.replace('->', '_') + '.yaml';
    with open(file_name, 'r') as fp:
        loaded = yaml.load(fp)
        category_rule = dict(category_rule.items() + loaded.items());

for num_levels in range(5):
    for full_name, rules in category_rule.iteritems():
        if category_list.has_key(full_name):
            continue;
        sub_names = full_name.split('->');
        if len(sub_names) != num_levels + 1:
            continue;
        if num_levels == 0:
            mask = apply_rules(all_items, rules, ids_array, idx_array);
            category_list[full_name] = np.array(np.array(range(len(mask))))[mask];   
        else:
            idx = full_name.rfind('->');
            parent_name = full_name[: idx];
            if not category_list.has_key(parent_name):
                parent_name = parent_name[: parent_name.rfind('->')]
            pl = category_list[parent_name];
            sub_items = [all_items[idx] for idx in pl]
            mask = apply_rules(sub_items, rules);
            category_list[full_name] = pl[mask];

# for each category, save some images


#all_line = [];
#for name, all_idx in category_list.iteritems():
    #all_line.append((name + '\t' + str(len(all_idx)) + '\n').encode('u8'));
#tmp_file = './working_taobao/tmp.txt';
#with open(tmp_file, 'w') as fp:
    #fp.writelines(all_line);

# generate the attribute list for specific categories
#full_name = 'clothes->top'
#selected_items = [all_items[idx] for idx in category_list[full_name]]
#selected_attrs = collect_attribute(selected_items)
#all_lines = print_partial_attribute(selected_attrs)
#out_file_name = '/home/jianfeng/working/taobao/attr_';
#fn = out_file_name + full_name + '.txt';
#fn = fn.replace('>', '_');
#with open(fn, 'w') as fp:
    #fp.writelines(all_lines)

# generate the sample images

final_url = [];
final_name = [];
data_folder = '/home/jianfeng/data/taobao/grouped_images/'
for name, all_idx in category_list.iteritems():
    curr_folder = os.path.join(data_folder, 
            name.replace('/', '_').replace('->', '/'));
    if not os.path.exists(curr_folder):
        os.makedirs(curr_folder);
    all_url = [];
    for idx in all_idx[0 : 10]:
        item = all_items[idx];
        if item.has_key('showing_image'):
            url = item['showing_image'];
            url = url.encode('u8');
            all_url.append(url);
    all_name = [];
    for url in all_url:
        full_name = os.path.join(curr_folder, str(uuid.uuid5(uuid.NAMESPACE_DNS, url.encode('u8'))) + '.jpg')
        all_name.append(full_name)
    final_url.extend(all_url);
    final_name.extend(all_name);

def download_image(info):
    (url, full_name) = info;
    if len(url) < 5:
        return None
    if os.path.exists(full_name):
        return None 
    if url[-4 : ] == '.jpg':
        try:
            urllib.urlretrieve(url, full_name);
            return None 
        except:
            return info
    else:
        ext = url[url.rfind('.') : ];
        tmp_file_name = full_name + ext;
        try:
            urllib.urlretrieve(url, tmp_file_name);
            x = scipy.misc.imread(tmp_file_name);
            scip.misc.imsave(full_name, x);
            return None
        except:
            return info

#for info in zip(final_url, final_name):
    #download_image(info);

pool = Pool(16);
result = pool.map_async(download_image, zip(final_url, final_name));
while not result.ready():
    print "number left: {}".format(result._number_left)
    time.sleep(10)

result_info = result.get();
pool.close();
pool.join();

# cache the images 

from pymongo import MongoClient
from ete2 import Tree;

# get the tree structure of the category

def get_label_tree():
    host_name = "54.65.168.46"
    port = 27017
    
    client = MongoClient(host_name, port)
    db = client["taobaodm"]
    category = db['taobao_product_categories']
    
    cache_category = {};
    # since the size is not large, read them all
    for item in category.find():
        label = item['label'];
        parent_label = item['parent_label'];
        catid = item['catid'];
        name = item['name'];
        assert not cache_category.has_key(label); 
        cache_category[label] = {'parent_label': parent_label, 'catid': catid, 'name': name};
    
    # build the tree structure
    label_tree = Tree();
    for key, value in cache_category.iteritems():
        value['is_visited'] = False;
    for label, value in cache_category.iteritems():
        if value['is_visited']:
            continue;
        path_list = [];
        path_list.append((label, value));
        assert len(label_tree.search_nodes(name = label)) == 0
        parent_label = value['parent_label'];
        finded_parent = None;
        while True:
            if parent_label is None:
                finded_parent = label_tree;
                break;
            finded_nodes = label_tree.search_nodes(name = parent_label);
            assert len(finded_nodes) <= 1;
            if len(finded_nodes) == 1 : 
                finded_parent = finded_nodes[0];
                break;
            path_list.append((parent_label, cache_category[parent_label]));
            parent_label = cache_category[parent_label]['parent_label'];
        assert finded_parent is not None;
        for label_add, value_add in reversed(path_list):
            finded_parent = finded_parent.add_child(name = label_add);
            finded_parent.add_features(parent_label = value_add['parent_label'], catid = value_add['catid'], semantic_name = value_add['name'].encode('utf-8'));
            value_add['is_visited'] = True;
    return label_tree;

file_name = '/home/jianfeng/working/taobao/label_tree.txt'
def save_label_tree(file_name):
    with open(file_name, 'w') as fp:
        fp.write(label_tree.get_ascii(attributes = ['semantic_name']));
    print label_tree.get_ascii(attributes = ['semantic_name'])

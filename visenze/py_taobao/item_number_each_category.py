import pickle
from label_tree_structure import get_label_tree

# get the category list
category_list = '/home/jianfeng/working/taobao/label_tree_selected_result.txt';
with open(category_list, 'r') as fp:
    all_lines = fp.readlines();

# get the meta data for each item
cached_file = '/home/jianfeng/working/taobao/cached_taobao_itemdetail'
num_cached = 5
all_items = [];
for i in range(num_cached):
    with open(cached_file + "_" + str(i), 'r') as fp:
        print 'start to load...'
        cached_items = pickle.load(fp);
        print 'finish loading.'
    for item in cached_items:
        if item.has_key('attributes'):
            item.pop('attributes');
    all_items.extend(cached_items)

# get the category relationship
label_tree = get_label_tree();

# get the id of each category name
num_lines = len(all_lines);
all_ids = [] * num_lines;
for i in range(num_lines):
    one_line = all_lines[i];
    one_id = all_ids[i];
    if len(one_line) == 0:
        continue;
    if ':' in one_line:
        continue;
    print one_line
    break;


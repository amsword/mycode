import glob;
import os;

out_html = '/home/jianfeng/data/taobao/grouped_images/show_sample_images.html';
input_folder = '/home/jianfeng/data/taobao/grouped_images/';

all_line = [];
line = '<!DOCTYPE html>'
all_line.append(line + '\n');
line = '<html>'
all_line.append(line + '\n');
line = '<head>'
all_line.append(line + '\n');
line = '<meta charset="utf-8">'
all_line.append(line + '\n');
line = '</head>'
all_line.append(line + '\n');
line = '<body>'
all_line.append(line + '\n');

# begin
def get_html(folder):
    folder_prefix = '/home/jianfeng/data/taobao/grouped_images/'
    all_sub_folder = [o for o in os.listdir(folder) if \
        os.path.isdir(os.path.join(folder, o))]
    all_line = [];
    is_empty = True 
    line = '<table>'
    all_line.append(line + '\n');
    for sub_folder in all_sub_folder:
        curr_folder = os.path.join(folder, sub_folder)
        #line = '<h2>' + curr_folder.replace(folder_prefix, '') + '</h2>'
        line = '<tr>';
        all_line.append(line + '\n');
        line = '<td>' + curr_folder.replace(folder_prefix, '') + '</td>'
        all_line.append(line + '\n');
        curr_jpgs = glob.glob(os.path.join(curr_folder, '*.jpg'));
        if len(curr_jpgs) > 0:
            is_empty = False
        for curr_jpg in curr_jpgs:
            curr_jpg = curr_jpg.replace(folder_prefix, '');
            line = '<td><img ' + \
                    'src="' + curr_jpg + '" ' + \
                    'with="128" ' + \
                    'height="128" ' + \
                    'alt="a"></td>';
            all_line.append(line + '\n');
        line = '</tr>';
        all_line.append(line + '\n');
    line = '</table>'
    all_line.append(line + '\n');
    line = '<hr>'
    all_line.append(line + '\n');
    if is_empty:
        return [];
    else:
        return all_line
    return all_line


def get_recursive_html(input_folder):
    all_line = [];
    all_line.extend(get_html(input_folder));
    all_sub_folder = [o for o in os.listdir(input_folder) \
            if os.path.isdir(os.path.join(input_folder, o))];
    for sub_folder in all_sub_folder:
        curr_folder = os.path.join(input_folder, sub_folder)
        all_line.extend(get_recursive_html(curr_folder))
    return all_line

all_line.extend(get_recursive_html(input_folder));

# the end
line = '</body>';
all_line.append(line + '\n');
line = '</html>'
all_line.append(line + '\n');

with open(out_html, 'w') as fp:
    fp.writelines(all_line);


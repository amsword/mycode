from pymongo import MongoClient

host_name = "54.65.168.46"
port = 27017
file_pickle = '../../../data/all_kv.pickle'
file_list = '../../../data/all_kv.txt'

client = MongoClient(host_name, port)
db = client["taobaodm"]
collection = db['taobao-itemdetail']

all_kv = {};
all_num = collection.count(); 
curr_num = 0;
# collect all the values; 
for item in collection.find():
    if (curr_num % 50) == 0: 
        print curr_num, all_num;
    # note: the database may be added now, and thus the real count may be different with the currently obtained one
    curr_num = curr_num + 1;
    if item.has_key('attributes'):
        attr = item['attributes']
        for k, v in attr.iteritems():
            vs = v.split(' ')
            set_vs = set(vs);
            if not all_kv.has_key(k):
                all_kv[k] = set_vs
            else:
                all_kv[k].update(set_vs)

print str(all_kv).decode('unicode_escape')

# save the parameter in case of debugging
import pickle
with open(file_pickle, 'w') as fp:
    pickle.dump(all_kv, fp);

with open(file_list, 'w') as fp:
    for k, v_list in all_kv.iteritems():
        s = k
        for v in v_list:
            s = s + u' ' + v
        s = s + u'\n';
        fp.write(s.encode('utf-8'));


from category_rule import category_rule
from scipy import logical_or
import yaml
import sets;
import numpy as np;
import cPickle as pickle
from label_tree_structure import get_label_tree
import msgpack
from multiprocessing import Pool
from scipy import misc
import uuid;
import os;
import urllib
import time

def load_all_item():
    cached_file = '/home/jianfeng/working/taobao/cached_selected_taobao_itemdetail'
    with open(cached_file, 'rb') as fp:
        print 'start to load...'
        all_items = pickle.load(fp);
        print 'ended'
    return all_items

all_items = load_all_item();

all_items_url = [];
for item in all_items: 
    assert item.has_key('showing_image')
    url = item['showing_image'];
    url = url.encode('u8');
    all_items_url.append(url);

all_item_file_name = [ 
    os.path.join('/home/jianfeng/data/taobao/list_image/', 
        str(uuid.uuid5(uuid.NAMESPACE_DNS, url))
        + '.jpg') \
        for url in all_items_url]

def download_image(info):
    (url, full_name) = info;
    if len(url) < 5:
        return None
    if os.path.exists(full_name):
        return None 
    if url[-4 : ] == '.jpg':
        try:
            urllib.urlretrieve(url, full_name);
            return None 
        except:
            return info
    else:
        ext = url[url.rfind('.') : ];
        tmp_file_name = full_name + ext;
        try:
            urllib.urlretrieve(url, tmp_file_name);
            x = scipy.misc.imread(tmp_file_name);
            scip.misc.imsave(full_name, x);
            return None
        except:
            return info

pool = Pool(128);
result = pool.map_async(download_image, zip(all_items_url, all_item_file_name));
while not result.ready():
    print "number left: {}".format(result._number_left)
    time.sleep(10)
result_info = result.get();
pool.close();
pool.join();


﻿Build a generic recognition engine for e-commerce


* Recognize products in one of the sub-domains - Fashion, Furniture, Consumer electronics
* Fashion
   * Apparels
      * Dress
      * Tops
      * Pants
      * Skirts
      * T-Shirts
      * Shirts
      * Sarees
      * Kurtas / Kurtis
      * Lingerie


   * Bags
      * Handbags
      * Clutch
      * Wallet
      * Backpacks
      * Travel suitcase
   * Shoes 
      * Sports shoes
      * Men’s
         * Formal shoes
         * Sandals
         * Slippers
         *       * Women’s
         * Bellies
         * High heels
         * Pumps
         * Flats
         * Sandals
         * Slippers
   * Watches
      * Men’s watches
         * Digital
         * Analog
         *       * Women’s watches
         * Digital
         * Analog
* Furniture
   * Sofa/couch
      * Single seater
      * Three seater
   * Table
      * Dining table
      * Coffee table
   * Chairs
      * Wooden
      * Steel
      * Easy chair
      * Stool
   * Shelf
      * Open book Shelf
      * Cupboard/Drawers
      * TV Stands
      * Display shelf
   * Bed & mattress
   * Carpets
   * Wall paper
   * Lights/lamps
   * Curtains
   * * Consumer electronics
   * Phones
   * Tablets
   * Laptops
   * Desktop
   * Monitor
   * Calculator
   * Chargers
   * Mouse
   * Keyboard
   * Cables
   * Flash drive
   * Hard drives
   * Routers
   * Headphones
   * TV
   * Refrigerator
   * Airconditioner
   * Microwave
   * Kitchen appliance
* 



Build a deep-dive attribute extraction engine for each of these products (could be combined with the classifier engine)


* Tops 
   * Sleeve type
   * Neck type
   * Hemline
   * Shape
   * Pattern type 
   * Shoulder type?
* Dress
   * Everything for tops plus
   * Length of dress
   * Shape
   * * Bottoms / Pants / Skirts
   * Shape
   * Length
   * Pattern
* Sarees
   * Sheer (transparency) 
   * Border/Pallu design type
* Shoes
* Watches
   * Watch face shape
   * Analog/Digital
   * Strap (Metallic/Leather)
   * 







Use logo detection to guess the brand. In many cases such as Watches, Shoes and Bags, the logos are obvious enough.
from pymongo import MongoClient
import pickle
import time

file_pickle = '../../../data/taobao/all_kv.pickle'
file_list = '../../../data/taobao/all_kv.txt'

cached_file = '../../../data/taobao/cached_taobao_itemdetail'
num_cached = 5

all_kv = {};
for i in range(num_cached):
    with open(cached_file + "_" + str(i), 'r') as fp:
        print 'start to load...'
        cached_items = pickle.load(fp);
        print 'finish loading.'
    curr_idx = 0;
    for item in cached_items:
        if item.has_key('attributes'):
            attr = item['attributes']
            for k, v in attr.iteritems():
                vs = v.split(' ')
                if all_kv.has_key(k):
                    v_dic = all_kv[k];
                else:
                    all_kv[k] = {};
                    v_dic = all_kv[k];
                    
                for sub_v in vs:
                    if v_dic.has_key(sub_v):
                        v_dic[sub_v] = v_dic[sub_v] + 1;
                    else:
                        v_dic[sub_v] = 1;


with open(file_pickle, 'w') as fp:
    pickle.dump(all_kv, fp, pickle.HIGHEST_PROTOCOL)



mask = np.array([False] * len(all_items))
for name, all_idx in category_list.iteritems():
    mask[all_idx] = True;

all_idx = np.array(range(len(mask)))[mask]
sub_items = [all_items[idx] for idx in all_idx]

file_name = '/home/jianfeng/working/taobao/cached_selected_taobao_itemdetail'
with open(file_name, 'wb') as fp:
    pickle.dump(sub_items, fp)

import os;
import yaml

def get_file_name(category_name):
    file_name = 'attr_' + category_name + '_m.txt';
    file_name = file_name.replace('>', '_');
    file_name = os.path.join('./working_taobao/', file_name);
    return file_name
def get_output_file(category_name):
    return os.path.join('./working_taobao', 'attr_' + category_name.replace('->', '_') +
            '.yaml')

def add_rule_key_value(attr, value, rules):
    rule_attr_kv = 'attr_key_value'
    if rules.has_key(rule_attr_kv):
        rules[rule_attr_kv].append([attr, value]);
    else:
        rules[rule_attr_kv] = [[attr, value]]
    return rules

def add_rule_key(attr, rules):
    rule_attr_key = 'attr_key'
    if rules.has_key(rule_attr_key):
        rules[rule_attr_key].append(attr)
    else:
        rules[rule_attr_key] = [attr]
    return rules

def add_rule(attr, one_split, rules):
    parts = one_split.split(':');
    # attribute value:
    if len(parts) == 2:
        rules = add_rule_key_value(attr, parts[0], rules);
    elif len(parts) == 3:
        if parts[1] == '*':
            rules = add_rule_key(parts[0], rules)
        else:
            rules = add_rule_key_value(parts[0], parts[1], rules)
    else:
        print 'lenth error: ', parts, one_split
        assert False;
    return rules

def convert_to_yaml(category_name):
    file_name = get_file_name(category_name);
    file_out = get_output_file(category_name);
    with open(file_name, 'r') as fp:
        all_line = fp.readlines();
    result = {};
    for line in all_line:
        #print line
        if line[0] != ' ' and line[0] != '\t':
            line = line.replace(' ', '');
            line = line.replace('\t', '');
            line = line.replace('\n', '');
            if len(line) == 0:
                continue;
            line_splits = line.split(':');
            assert len(line_splits) == 2;
            attr = line_splits[0];
            attr = attr.replace(' ', '');
        else:
            line = line.replace(' ', '');
            line = line.replace('\t', '');
            line = line.replace('\n', '');
            if len(line) == 0:
                continue;
            line_splits = line.split(',');
            assert len(line_splits) >= 1;
            line_first = line_splits[0];
            value = line_first.split(':')[0];
            rules = {};
            for one_split in line_splits:
                #print 'one_split: ' + one_split
                rules = add_rule(attr, one_split, rules);
            result_key = category_name + '->' + attr + '->' + value
            result[result_key] = rules
    with open(file_out, 'w') as fp:
        yaml.safe_dump(result, fp, default_flow_style = False, encoding = 'u8', allow_unicode = True);

category_name = 'clothes->top'
category_name = 'clothes->trousers'
category_name = 'clothes->dress'
convert_to_yaml(category_name);


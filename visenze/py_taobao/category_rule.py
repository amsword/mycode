# encoding: u8
category_rule= {};
category_rule['jewellery'] = {'catid': [50015926]}
category_rule['glasses'] = {'catid': [50023876, 50010368, 50018533]}
category_rule['watch'] = {'catid': [50005700]}
category_rule['bag'] = {'catid': [12765, 50072686, 50006842, 50072688]}
category_rule['shoe'] = {'catid': [50010388, 50016853, 50016853, 50016853, 50006843, 50006843]}
category_rule['clothes'] = {'catid':[50016756, 50008165, 30, 16, 1625]}

category_rule['clothes->top'] = {'attr_key':
        [u'袖长', u'领型', u'衣门襟', u'袖型', u'领子']};
category_rule['clothes->trousers'] = {'attr_key': [u'裤长']};
category_rule['clothes->dress'] = {'attr_key': [u'裙长', u'裙型', u'礼服裙长']};

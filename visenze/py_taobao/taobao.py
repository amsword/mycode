import pickle
import time


file_pickle = '/home/jianfeng/working/taobao/all_kv.pickle'
with open(file_pickle, 'r') as fp:
    all_kv = pickle.load(fp)

all_keys = [];
all_values = [];
for key, value in all_kv.iteritems():
    for sub_key, sub_value in value.iteritems():
        final_key = key + '_' + sub_key
        all_keys.append(final_key);
        all_values.append(sub_value);

s_idx = sorted(range(len(all_values)), key = lambda k: all_values[k]);

all_lines = [];
for i in range(10000):
    idx = s_idx[-(i + 1)]
    key = all_keys[idx];
    value = all_values[idx];
    final_value = key + '\t' + str(value) + '\n';
    final_value = final_value.encode('utf-8');
    all_lines.append(final_value);

with open(save_file, 'w') as fp:
    fp.writelines(all_lines);

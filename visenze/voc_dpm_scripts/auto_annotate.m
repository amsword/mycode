function auto_annotate
clear

%all_data_dir = ...
%{'/home/jianfeng/snow/working/shoe/myntra_shoe_leather_resize/', ...
%'/home/jianfeng/snow/working/shoe/myntra_shoe_sport_resize/', ...
%'/home/jianfeng/snow/working/shoe/voc_format/voc_leather_shoes_resize/', ...
%'/home/jianfeng/snow/working/shoe/voc_format/voc_sports_shoes_resize/'};

all_data_dir = ...
{'/home/jianfeng/snow/working/shoe/myntra_shoe_slipon_resize//'};

num_all = numel(all_data_dir);
for i = 1 : num_all
    DATdir= all_data_dir{i};
    run_one_folder(DATdir);
end

function run_one_folder(DATdir)

ORGlabel='PASshoe';
JPEGdir=fullfile(DATdir,'JPEGImages/');
ANNdir=fullfile(DATdir,'Annotations/');
if ~exist(ANNdir, 'dir')
mkdir(ANNdir);
end

addpath('/home/jianfeng/snow/code/weardex-algorithm/3rdParty/VOCdevkit/PAScode');

d=dir(fullfile(JPEGdir,'*.jpg'));
parfor i=1:length(d),
    image_name = fullfile(JPEGdir, d(i).name);
    [path,name,ext]=fileparts(d(i).name);
    annfile=fullfile(ANNdir, [name, '.txt']);
    %if exist(annfile, 'file')
        %continue;
    %end
    img=imread(image_name);
    if size(img, 3) == 1
        img = repmat(img, [1, 1, 3]); 
        imwrite(img, image_name);
    end
    fprintf('-- Now annotating %s --\n',d(i).name);
    record=PASannotateimg_auto(img);
    record.imgname=d(i).name;
    record.database= ORGlabel;
    
    for j=1:length(record.objects),
        record.objects(j).label=ORGlabel;
        record.objects(j).orglabel=ORGlabel;
        record.objects(j).class = ORGlabel;
    end;
    comments={}; % Extra comments = array of cells (one per line)
    PASwriterecord(annfile,record,comments);
    if (~PAScmprecords(record,PASreadrecord(annfile)))
        PASerrmsg('Records do not match','');
    end;
end;
% return

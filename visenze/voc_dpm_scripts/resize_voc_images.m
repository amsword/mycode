function resize_voc_images
all_input_folder = {'/home/jianfeng/snow/working/shoe/myntra_shoe_slipon'};

for i = 1 : numel(all_input_folder)
    input_folder = all_input_folder{i};
    run_one_folder(input_folder);
end

function run_one_folder(input_folder)
output_folder = [input_folder, '_resize'];


%% get image list
image_pattern = fullfile(input_folder, 'JPEGImages', '*.jpg');
all_images = dir(image_pattern);
from_full_images = cell(numel(all_images), 1);
to_full_images = cell(numel(all_images), 1);
for i = 1 : numel(all_images)
    from_full_images{i} = fullfile(input_folder, 'JPEGImages', all_images(i).name);
    to_full_images{i} = fullfile(output_folder, 'JPEGImages', all_images(i).name);
end

target_folder = fullfile(output_folder, 'JPEGImages');
if ~exist(target_folder, 'dir')
    cmd = ['mkdir -p ', target_folder];
    system(cmd);
end

th = 1000;
% resize the image and save;
for i = 1 : numel(from_full_images)
    from_image_name = from_full_images{i};
    to_image_name = to_full_images{i};
    im = imread(from_image_name);
    if size(im, 1) < th && ...
        size(im, 2) < th
        copyfile(from_image_name, to_image_name);
    else
        longest_dim = max(size(im, 1), size(im, 2));
        ratio = th / longest_dim;
        im = imresize(im, ratio);
        imwrite(im, to_image_name);
    end
end

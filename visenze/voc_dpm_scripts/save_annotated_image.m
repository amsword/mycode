
shoe_type = 'sports_shoes';
root_dir =['/home/jianfeng/snow/working/shoe/voc_format/voc_', shoe_type];

addpath('/home/jianfeng/snow/code/weardex-algorithm/3rdParty/VOCdevkit/PAScode');
image_folder = fullfile(root_dir, 'JPEGImages');
ann_folder = fullfile(root_dir, 'Annotations');

all_images = dir(fullfile(image_folder, '*.jpg'));
si = vision.ShapeInserter;
for i = 1 : numel(all_images)
    sub_name = all_images(i).name;
    [~, sub_name, ext] = fileparts(sub_name); 

    image_path = fullfile(image_folder, [sub_name, '.jpg']);
    ann_path = fullfile(ann_folder, [sub_name, '.txt']);
    im = imread(image_path);
    record = PASreadrecord(ann_path);
    for j = 1 : numel(record.objects)
        bb = record.objects(j).bbox;
        bb = [bb(1), bb(2), bb(3) - bb(1), bb(4) - bb(2)];
        im = step(si, im, int32(bb));
    end
    save_path = ['/home/jianfeng/working/debug/', sub_name, '.jpg'];
    imwrite(im, save_path);
end

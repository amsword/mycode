clear
org_folder =['/home/jianfeng/snow/working/shoe/shoe_oxford/'];
to_folder = ['/home/jianfeng/snow/working/shoe/shoe_oxford_cleaned/'];
prefix = ['product_street_shoes_oxford_'];
label_name = 'PASshoe';
addpath('/home/jianfeng/snow/code/weardex-algorithm/3rdParty/VOCdevkit/PAScode/');
% ------------
org_image_folder = fullfile(org_folder, 'JPEGImages');
org_annotation_folder = fullfile(org_folder, 'Annotations');
all_images = dir(fullfile(org_annotation_folder, '*.txt'));
to_image_folder = fullfile(to_folder, 'JPEGImages');
to_annotation_folder = fullfile(to_folder, 'Annotations');
if ~exist(to_image_folder, 'dir')
    cmd = ['mkdir -p ', to_image_folder];
    system(cmd);
end
if ~exist(to_annotation_folder, 'dir')
    cmd = ['mkdir -p ', to_annotation_folder];
    system(cmd);
end


for i=1:length(all_images)
    sub_name = all_images(i).name;
    [~, sub_name, ~] = fileparts(sub_name);
    [~, sub_name, ext] = fileparts(sub_name);
    
    from_image_file = fullfile(org_image_folder, [sub_name, ext]);
    to_sub_name = [prefix, num2str(i)];
    
    to_image_file = fullfile(to_image_folder, [to_sub_name, '.jpg']);
    if strcmp(ext, '.jpg')
        copyfile(from_image_file, to_image_file);
    else
        im = imread(from_image_file);
        imwrite(im, to_image_file);
    end
    
    from_ann_file = fullfile(org_annotation_folder, [sub_name, ext, '.txt']);
    to_ann_file = fullfile(to_annotation_folder, [to_sub_name, '.txt']);
    rec = PASreadrecord(from_ann_file);
    rec.imgname = [to_sub_name, '.jpg'];
    for j = 1 : numel(rec.objects)
        rec.objects(j).label = label_name;
        rec.objects(j).orglabel = label_name;
        rec.objects(j).class = label_name;
    end
    PASwriterecord(to_ann_file, rec, {});
end
% return

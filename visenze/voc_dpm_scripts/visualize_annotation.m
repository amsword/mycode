clear

% shoe_type = 'sports_shoes';
shoe_type = 'org_street_shoes';
org_folder =['/home/jianfeng/snow/working/shoe/voc_format_out/'];
org_image_folder = fullfile(org_folder, 'JPEGImages');
org_annotation_folder = fullfile(org_folder, 'Annotations');
all_images = dir(fullfile(org_annotation_folder, '*.txt'));

addpath('/home/jianfeng/snow/code/weardex-algorithm/3rdParty/VOCdevkit/PAScode');

si = vision.ShapeInserter;

%for i=1:length(all_images)
    i = 100;
    sub_name = all_images(i).name;
    [~, sub_name, ext] = fileparts(sub_name);
    
    from_image_file = fullfile(org_image_folder, [sub_name, '.jpg']);
    im = imread(from_image_file);
            
    from_ann_file = fullfile(org_annotation_folder, [sub_name, '.txt']);
    
    rec = PASreadrecord(from_ann_file);
    
    for j = 1 : numel(rec.objects)
        bb = rec.objects(j).bbox;
        bb = [bb(1), bb(2), bb(3) - bb(1), bb(4) - bb(2)];
        im = step(si, im, int32(bb));
    end
    imshow(im);
    pause
%end
% return

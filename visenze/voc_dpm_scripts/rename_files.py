import glob
import os

shoe_type = 'sports_shoes'
file_path = '/home/jianfeng/snow/working/shoe/voc_' + shoe_type + '/JPEGImages/';
all_files = glob.glob(file_path + '*.jpg');

prefix = 'product_' + shoe_type + '_'
for file_name in all_files:
    file_dir, file_sub_name = os.path.split(file_name)
    if file_sub_name[0 : len(prefix)] == prefix:
        print file_name
        continue;
    new_file_name = os.path.join(file_dir, prefix + file_sub_name);
    os.rename(file_name, new_file_name)



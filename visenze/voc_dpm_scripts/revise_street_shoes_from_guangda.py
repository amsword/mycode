from scipy import misc
import shutil
import os
import glob

org_folder_root = '/home/jianfeng/snow/working/shoe/voc_org_street_shoes/';
to_folder_root = '/home/jianfeng/snow/working/shoe/voc_street_shoes/'

org_image_folder = os.path.join(org_folder_root, 'JPEGImages');
org_annotation_folder = os.path.join(org_folder_root, 'Annotations');

to_image_folder = os.path.join(to_folder_root, 'JPEGImages');
if not os.path.isdir(to_image_folder):
    os.makedirs(to_image_folder);
to_annotation_folder = os.path.join(to_folder_root, 'Annotations');
if not os.path.isdir(to_annotation_folder):
    os.makedirs(to_annotation_folder)

org_all_annotation_name = glob.glob(os.path.join(org_annotation_folder, '*'));

org_all_sub_image_name = [os.path.basename(image_name) for image_name in \
        org_all_annotation_name];
org_all_sub_image_name = [image_name[:-4] for image_name in \
        org_all_sub_image_name];

idx = 0;
for org_sub_image_name in org_all_sub_image_name:
    org_image_full_name = os.path.join(org_image_folder, org_sub_image_name);
    org_annotation_full_name = os.path.join(org_annotation_folder, \
            org_sub_image_name + '.txt');

    im = misc.imread(org_image_full_name);
    sub_name = 'street_shoes_' + str(idx);
    idx = idx + 1;

    to_image_full_name = os.path.join(to_image_folder, sub_name + '.jpg')
    misc.imsave(to_image_full_name, im);

    to_annotation_full_name = os.path.join(to_annotation_folder, sub_name + \
            '.txt')
    shutil.copy(org_annotation_full_name, to_annotation_full_name);



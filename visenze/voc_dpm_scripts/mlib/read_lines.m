function lines = read_lines(file_name)
fp = fopen(file_name);
lines = textscan(fp, '%s','delimiter', '\n');
lines = lines{1};
fclose(fp);

function bb = simple_detect(im)

mask = edge(rgb2gray(im), 'canny');
b_y = sum(mask, 2);
b_y = b_y >= 1;
[y_start, y_end] = longest_seg(b_y);

b_x = sum(mask(y_start : y_end, :), 1);
b_x = b_x >= 1;
[x_start, x_end] = longest_seg(b_x);

bb = [y_start, y_start, x_end, y_end];

end

function [start_idx, end_idx] = longest_seg(x)
x = reshape(x, [numel(x), 1]);
y = x .* [1 : numel(x)]' / sum(x);
mid_point = sum(y);
mid_point = floor(mid_point);
mid_point
start_idx = find(x(1 : mid_point) == 0, 1, 'last');
end_idx = find(x(mid_point : end) == 0, 1, 'first') + mid_point - 1;
end

function write_lines(lines, file_name)
fp = fopen(file_name, 'w');
for i = 1 : numel(lines)
    fprintf(fp, '%s\n', lines{i});
end
fclose(fp);

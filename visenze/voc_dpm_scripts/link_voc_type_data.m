input_folder = '/home/jenkins/SSD/data/detection/';
output_folder = ...
'/home/jianfeng/snow/code/voc-dpm/cachedir/VOC2030/VOCdevkit/VOC2030/'

addpath('/home/jianfeng/snow/code/weardex-algorithm/3rdParty/VOCdevkit');
link_all_dataset(input_folder, output_folder);

all_subs = dir(input_folder);
num_subs = numel(all_subs);

result = [];
for i = 1 : num_subs
    folder_name = all_subs(i).name;
    if strcmp(folder_name, '.') || ...
        strcmp(folder_name, '..')
        continue;
    end
    fprintf([folder_name, '\n']);
    all_images = dir(fullfile(input_folder, folder_name, 'JPEGImages', '*.jpg'));
    all_annotations = dir(fullfile(input_folder, folder_name, ...
        'Annotations', '*.txt'));
    if numel(all_annotations) == 0
        all_annotations = dir(fullfile(input_folder, folder_name, ...
            'Annotations', '*.xml'));
    end
    assert(numel(all_images) == numel(all_annotations));
    for j = 1 : numel(all_images)
        one_image = all_images(j).name;
        one_ann = all_annotations(j).name;
        sub_name = one_image(1 : end - 4);
        assert(strcmp(sub_name, one_ann(1 : end - 4)) == 1);
        result = [result, {sub_name}];
    end
end

tmp = fullfile(output_folder, 'ImageSets', 'Main', '*');
delete(tmp);
result_file_name = fullfile(output_folder, 'ImageSets', 'Main', 'train.txt');
fp = fopen(result_file_name, 'w');
for i = 1 : numel(result)
    name = result{i};
    fprintf(fp, '%s\n', name);
end
fclose(fp);
copyfile(result_file_name, ...
    fullfile(output_folder, 'ImageSets', 'Main', 'trainval.txt'));

from subprocess import check_call;
from com_utility import *;
import struct;
build_caffe = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";

extract_phase = "train"
data_source = "train"
folder_name = "/home/wangjianfeng/working/cifar100/plus1_loss/merge/";
pretrained_weight = folder_name + r"direct341:lower_bound:0.9_342:upper_bound:1_23:random_seed:1_7:base_lr:0.1_iter_150000.caffemodel";
net_proto = r"/home/wangjianfeng/code/examples/cifar100/plus1_loss/cifar100_merge_extract_";
net_proto = net_proto +	data_source + r".prototxt" ;
blob_name  = "softmax";
output_file = pretrained_weight + blob_name + data_source + "_" + extract_phase + ".feat";
num_mini_batches = 500;
device_id = 2;
machine_name = "deep08";


sub_cmd = "";
sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH";
sub_cmd = sub_cmd + " && ";
sub_cmd = sub_cmd + build_caffe + "extract_features1.bin " + \
	pretrained_weight + " " + net_proto + " " + \
	blob_name + " " + output_file + " " + str(num_mini_batches) + " " + \
	extract_phase + \
	" GPU " + str(device_id);
check_call(["ssh", machine_name, sub_cmd]);

feature = read_mat(output_file, "float");

max_value = feature.max(1);
print max_value.mean()
print max_value.std()
print (max_value > 0.9).sum() / float(max_value.size)
print (max_value > 0.8).sum() / float(max_value.size)

import re
from dateutil import parser
import pdb
from pprint import pprint

def analyze_caffe_output(log):
    lines = log.splitlines()     
    # e.g. I0605 03:50:35.652143 21222 net.cpp:520] input
    info = [_parse(line) for line in lines]
    index = _create_index(info)
    _print(index)
    
def _print(index):
    result = []
    for key in index:
        value = index[key]
        result.append((key, value))
    result.sort(key = lambda x: x[1]['seconds'], reverse = True)
    pprint(result)

def _create_index(info):
    index = {}
    pre = info[0]
    for i in range(1, len(info)):
        curr = info[i]
        key = (pre[1:], curr[1:])
        if key in index:
            index[key]['count'] = index[key]['count'] + 1
            index[key]['seconds'] = index[key]['seconds'] + (curr[0] - pre[0]).total_seconds()
        else:
            index[key] = {'count':1, \
                          'seconds': (curr[0] - pre[0]).total_seconds()}
        pre = curr
    return index

def _parse(line):
    p = 'I[0-9]+ ([0-9,:,\.]+) [0-9]+ (.+):([0-9]+)] (.*)'
    r = re.match(p, line)
    assert r != None
    g = r.groups()
    time = parser.parse(g[0])
    file_name = g[1]
    line_num = g[2]
    value = g[3]
    return (time, file_name, line_num, value)



if __name__ == "__main__":
    file_name = '/home/jianfw/code/py-faster-rcnn/b.txt'
    with open(file_name) as fp:
        lines = fp.read()
    run(lines)

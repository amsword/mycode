from com_utility import throw_cmd
import os
import subprocess
import time
from com_utility import change_field, mkdirs
import numpy
import glob;


class caffe_cmd:
    def __init__(self):
        self.caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"
        self.machine_name = 'deep02'
        self.device_ids = [];
        self.random_seeds = [];

    def set_machine(self, machine_name):
        self.machine_name = machine_name
        assert isinstance(machine_name, basestring);

    def set_device_ids(self, device_ids):
        self.device_ids = device_ids;

    def set_random_seeds(self, random_seeds):
        self.random_seeds = random_seeds;

    def train(self, solver, finetune = "", continuing = "",
            log_file_name = '', is_sync = True):
        sub_cmd = ""
        sub_cmd = sub_cmd + self.caffe_dir + "caffe train --solver=" + solver
        if finetune != "":
        	sub_cmd = sub_cmd + " --weights=" + finetune
        if continuing != "":
        	sub_cmd = sub_cmd + " --snapshot=" + continuing

        p = throw_cmd(self.machine_name, sub_cmd, log_file_name)
        if is_sync:
            p.communicate()
        return p

    def test(self, net_file, weights, num_iter, log_file = ""):
        sub_cmd = ""
        sub_cmd = sub_cmd + self.caffe_dir + "caffe test --model=" + net_file + \
                " " + "--weights=" + weights + \
                " " + "--iterations=" + str(num_iter) + \
                " " + "--gpu=" + str(self.device_ids[0]);

        p = throw_cmd(self.machine_name, sub_cmd, log_file)
        return p;

    def collect_test1(self, prefix, suffix = '', indicator = 'accuracy = '):
        all_files = glob.glob(prefix + "_iter*" + suffix + ".test");
        
        all_iter = [];
        all_acc = [];
        for f in all_files:
            idx_end = f.find('.caffemodel');
            k = f.find('_iter_');
            num_iter = f[k + 6 : idx_end];
            num_iter = int(float(num_iter));
        
            with open(f, 'r') as fp:
                lines = fp.readlines();
                line = None;
                all_lines = [];
                for each_line in lines:
                    if indicator in each_line and 'Batch' not in each_line:
                        line = each_line;
                        all_lines.append(line);
                if len(all_lines) is 0:
                    continue;
                line = all_lines[-1];
                if line is None:
                    continue;
                x = line.split(indicator);
                assert len(x) == 2, x;
                acc = float(x[1]);
                all_acc.append(acc);
                all_iter.append(num_iter);
        seq = sorted(range(len(all_iter)),
                key = lambda k : all_iter[k], reverse = False);
        all_iter = [all_iter[seq[i]] for i in range(len(all_iter))];
        all_acc = [all_acc[seq[i]] for i in range(len(all_acc))];
        return numpy.array(all_acc), numpy.array(all_iter)

    def collect_test(self, folder, prefix):
        print 'change it to collect_test1'
        assert False
        all_files = [f for root, dirs, files in os.walk(folder)
                for f in files
                if prefix + "_iter" in f and 
                    f.endswith('.test')];
        
        all_iter = [];
        all_acc = [];
        for f in all_files:
            num_iter = f[0 : -16];
            k = num_iter.rfind('_');
            num_iter = num_iter[k + 1 :];
            num_iter = int(float(num_iter));
        
            with open(folder + f, 'r') as fp:
                lines = fp.readlines();
                for each_line in lines:
                    if 'accuracy = ' in each_line:
                        line = each_line;
                        break;
                x = line.split('accuracy = ');
                assert len(x) == 2, x;
                acc = float(x[1]);
                all_acc.append(acc);
                all_iter.append(num_iter);
        seq = sorted(range(len(all_iter)),
                key = lambda k : all_iter[k], reverse = False);
        all_iter = [all_iter[seq[i]] for i in range(len(all_iter))];
        all_acc = [all_acc[seq[i]] for i in range(len(all_acc))];
        return numpy.array(all_acc), numpy.array(all_iter)

    def find_test(self, folder, prefix, num_iter):
        print 'change it to find_test1'
        assert False
        while 1:
            all_files = [f for root, dirs, files in os.walk(folder)
                    for f in files
                    if len(f) > len(prefix) and 
                    f[0: len(prefix)] == prefix and 
                    prefix + "_iter_" in f and 
                    not f.endswith('.solverstate') and 
                    not f.endswith('.tee') and 
                    not f.endswith('.prototxt')]

            print all_files

            for f in all_files:
                if not f.endswith('.caffemodel'):
                    continue;
                finded = [f1 for f1 in all_files if len(f1) > len(f) and f in f1]
                if finded != []:
                    assert len(finded) == 1;
                    continue;
                net_file = folder + prefix + "_n.prototxt";
                model_file = folder + f;
                print 'test ', f, net_file, model_file
                self.test(net_file, model_file, num_iter, folder + f + ".test");
            print self.collect_test(folder, prefix)
            print 'begin waiting'
            time.sleep(60 * 5);

    def find_test1(self, net_file, prefix, test_iter):
        if net_file.rfind('/') == -1:
            sub = net_file;
        else:
            sub = net_file[net_file.rfind('/') + 1 : ];
        while 1:
            all_files = [f for f in glob.glob(prefix + "_iter_*")
                    if not f.endswith('.solverstate') and 
                    not f.endswith('.tee') and 
                    not f.endswith('.prototxt') and
                    f.endswith('.caffemodel')]
            print all_files

            # rank the files by the iter
            all_iter = [];
            for f in all_files:
                idx_end = f.find('.caffemodel');
                k = f.find('_iter_');
                num_iter = f[k + 6 : idx_end];
                num_iter = int(float(num_iter));
                all_iter.append(num_iter);
            seq = sorted(range(len(all_iter)),
                    key = lambda k : all_iter[k], reverse = False);
            all_files = [all_files[seq[i]] for i in range(len(all_files))];

            idx = 0;
            for f in all_files:
                print idx, len(all_files);
                idx = idx + 1;
                finded = [f1 for f1 in all_files if len(f1) > len(f) and f in f1]
                if finded != []:
                    assert len(finded) == 1;
                    continue;
                model_file = f;
                out_file_name = model_file + '.' + sub + ".test";
                if os.path.isfile(out_file_name):
                    continue;
                p = self.test(net_file, model_file, test_iter, out_file_name);
                p.communicate();
            print 'begin waiting'
            time.sleep(10);

    def train_revise_solver(self, net_base, solver_base, net_revise, solver_revise,
		snap_prefix, finetune = "", continuing = "", is_sync = False):

        net_revise_r = net_revise.replace(",", "_")
        net_revise_r = net_revise_r.replace("\"", "")
        solver_revise_r = solver_revise.replace(",", "_")
        solver_revise_r = solver_revise_r.replace("\"", "")
        
        folder_name = snap_prefix[:snap_prefix.rfind('/')];
        mkdirs(folder_name);
        snap_prefix = snap_prefix + net_revise_r + "_" + solver_revise_r
        net_use = snap_prefix + "_" + "n.prototxt"
        solver_use = snap_prefix + "_" + "s.prototxt"

        change_field(net_base, net_revise, net_use)
        whole_solver_revise = solver_revise + \
        					  ",snapshot_prefix:" + "\"" + snap_prefix + "\"" + \
        					  ",net:" + "\"" + net_use + "\"";
        change_field(solver_base, whole_solver_revise, solver_use)
        device_set = [];
        device_set.append('\n');
        device_set.append('device_id: ' + str(self.device_ids[0]));
        device_set.append('\n');
        device_set.append('random_seed: ' + str(self.random_seeds[0]));
        device_set.append('\n');
        for i in range(1, len(self.device_ids)):
            device_set.append('device_ids:' + str(self.device_ids[i]));
            device_set.append('\n');
            device_set.append('random_seeds: ' + str(self.random_seeds[i]));
            device_set.append('\n');
        with open(solver_use, 'a') as f:
            f.writelines(device_set);

        cmd = []
        cmd.append("ssh")
        cmd.append(self.machine_name)

        sub_cmd = ""
        sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH"
        sub_cmd = sub_cmd + " && "
        sub_cmd = sub_cmd + self.caffe_dir + "caffe train --solver=" + solver_use
        if finetune != "":
        	sub_cmd = sub_cmd + " --weights=" + finetune
        if continuing != "":
        	sub_cmd = sub_cmd + " --snapshot=" + continuing

        log_file_name = snap_prefix + "_log.tee"
        if os.path.isfile(log_file_name) == True:
            print log_file_name
            assert False
        if is_sync:
            sub_cmd = sub_cmd + " 2>&1 | tee -a " + log_file_name
        sub_cmd = sub_cmd + " && "
        sub_cmd = sub_cmd + "python /home/wangjianfeng/code/mycode/python_code/best_acc.py " + \
                  log_file_name
        if is_sync:
            sub_cmd = sub_cmd + " 2>&1 | tee -a " + log_file_name
        cmd.append(sub_cmd)

        print cmd
        if is_sync:
            p = subprocess.Popen(cmd);
        else:
            with open(log_file_name, "w") as fp:
                p = subprocess.Popen(cmd, stdin=fp, stdout = fp, stderr = fp)

        if is_sync:
            p.communicate();
        return p, log_file_name

    def convert_to_imageset_multi(self,
            root_folder, list_file,
            db_name,
            resize_width,
            resize_height):
        cmd = []
        if root_folder[-1] != '/':
            root_folder = root_folder + '/'
        cmd.append(self.caffe_dir + 'convert_imageset_multi.bin')
        cmd.append("--shuffle=true")
        cmd.append('--backend=lmdb')
        cmd.append('--resize_width=' + str(resize_width))
        cmd.append('--resize_height=' + str(resize_height))
        cmd.append(root_folder)
        cmd.append(list_file)
        cmd.append(db_name)
        return throw_cmd(self.machine_name, cmd)

    def extract_features1(self, pretrained, net_def, blob_name,
                          save_file, num_mini_batch, phase):
        cmd = []
        cmd.append(self.caffe_dir + "extract_features1.bin")
        cmd.append(pretrained)
        cmd.append(net_def)
        cmd.append(blob_name)
        cmd.append(save_file)
        cmd.append(str(num_mini_batch))
        assert phase == 'train' or phase == 'test'
        cmd.append(phase)
        cmd.append('GPU')
        cmd.append(str(self.device_ids[0]))
        p = throw_cmd(self.machine_name, cmd)
        return p

    def extract_with_labels(self, pretrained, net_def, blob_name,
                          save_file, num_mini_batch):
        cmd = []
        cmd.append(self.caffe_dir + "extract_with_labels.bin")
        cmd.append(pretrained)
        cmd.append(net_def)
        cmd.append(blob_name)
        cmd.append(save_file)
        cmd.append(str(num_mini_batch))
        cmd.append('GPU')
        cmd.append(str(self.device_ids[0]))
        p = throw_cmd(self.machine_name, cmd)
        return p


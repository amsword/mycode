import sys
python_code = "/home/wangjianfeng/code/mycode/python_code"
sys.path.append(python_code)
from com_utility import extract_all_acc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from numpy import *

output_file = "/home/wangjianfeng/a.png"

#log_file = 'direct_lower_bound:0.9_upper_bound:1.0_max_iter:200000_log.tee'
#all_acc, all_iter = extract_all_acc(log_file)

log_file = 'fine_tune_lower_bound:0.8_upper_bound:1.0_max_iter:200000_log.tee'
all_acc, all_iter = extract_all_acc(log_file)
#all_acc = concatenate((all_acc, all_acc2))
##all_iter = concatenate((all_iter, all_iter2))

ind = all_acc > 0.3
all_acc = all_acc[ind]
all_iter = all_iter[ind]
plt.close()
plt.plot(all_iter, all_acc, '*-')
plt.grid()
plt.savefig(output_file)

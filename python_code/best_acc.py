#!/usr/bin/python

import sys

def best_acc(log_file):
	fp = open(log_file, "r");
	lines = fp.readlines();
	fp.close();
	
	print "number of lines: ", len(lines)
	best_score = 0;
	for line in lines:
		if "Test net output #0:" in line:
			split_line = line.split("=");
			assert len(split_line) == 2;
			acc = float(split_line[1]);
			if best_score < acc:
				best_score = acc;
	return best_score

if __name__ == "__main__":
	assert len(sys.argv) >= 2
	best_score = best_acc(sys.argv[1]);
	print "best acc: ", best_score

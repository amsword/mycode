import sys
python_code = "/home/wangjianfeng/code/mycode/python_code"
sys.path.append(python_code)
from com_utility import extract_all_acc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

log_file = 'direct_lower_bound:1.0_upper_bound:1.0__log.tee'
output_file = "/home/wangjianfeng/a.png"
all_acc, all_iter = extract_all_acc(log_file)

ind = all_acc > 0.3
all_acc = all_acc[ind]
all_iter = all_iter[ind]
plt.close()
plt.plot(all_iter, all_acc, '*-')
plt.grid()
plt.savefig(output_file)

import numpy;
import struct;
import random;
import os;
import sys
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
import struct


def DistributeFor(idx_start, 
        idx_end, 
        all_resources, 
        func):
    assert idx_end > idx_start;
    num_resources = len(all_resources);
    chunck_size = (idx_end - idx_start) / 2 / num_resources;
    chunck_size = int(chunck_size);
    all_procs = [None] * num_resources;
    allocated = 0;
    while True:
        if allocated >= idx_end - idx_start:
            break; 
        for idx_resource in range(num_resources):
            sub_proc = all_procs[idx_resource];
            if sub_proc is not None and sub_proc.poll() is None:
                continue;
            if allocated >= idx_end - idx_start:
                break; 
            sub_idx_start = idx_start + allocated;
            sub_idx_end = sub_idx_start + chunck_size;
            sub_idx_end = min(sub_idx_end, idx_end);
            resourse = all_resources[idx_resource];
            print 'from ', sub_idx_start, \
                    ' to ', sub_idx_end, \
                    ' by ', resourse;
            all_procs[idx_resource] = func(sub_idx_start, sub_idx_end, 
                    resourse);
            time.sleep(1);
            allocated = allocated + sub_idx_end - sub_idx_start;
        time.sleep(60);
    for p in all_procs:
        if p is not None:
            p.communicate();
def mkdirs(folder):
    s = [];
    while True:
        if not os.path.isdir(folder):
            s.append(folder);
            folder = folder[: folder.rfind('/')];
        else:
            break;
    while len(s) > 0:
        folder = s.pop();
        os.mkdir(folder);

def change_field(file_name_in, patterns, file_name_out):
    #assert not os.path.isfile(file_name_out);
    split_patterns = patterns.split(",")
    split_patterns_no_line = [];
    split_patterns_line = [];
    for pattern in split_patterns:
        one_split = pattern.split(":", 1);
        if len(one_split) < 1:
            print 'ignore', pattern;
        elif one_split[0].isdigit():
            split_patterns_line.append(pattern);
        else:
            split_patterns_no_line.append(pattern);

    try:
        fin = open(file_name_in, "r")
        lines = fin.readlines()
        fin.close();
    except:
        print file_name_in;
        assert False


    fout = open(file_name_out, "w")

    line_number = 0
    for line in lines:
        line_number = line_number + 1
        is_written = False
        for p in split_patterns_line:
            one_pattern = p.split(":", 1)
            if one_pattern[0].isdigit():
                if int(one_pattern[0]) == line_number:
                    if one_pattern[1].split(":")[0] not in line:
                        print one_pattern[1].split(":")[0], line
                        assert False;
                    fout.writelines(one_pattern[1] + "\n")
                    is_written = True
                    break;
        if is_written:
            print "remove: 1", p;
            split_patterns_line.remove(p);

        if is_written == False:
            field_name = line.split(":", 1);
            if len(field_name) == 0:
                is_written = True;
                fout.writelines(line);
            else:
                field_name = field_name[0].strip();
                for p in split_patterns_no_line:
                    if p.split(":")[0] == field_name:
                        fout.writelines(p + "\n");
                        is_written = True;
                        break;
                if is_written:
                    split_patterns_no_line.remove(p);
        if is_written == False:
            fout.writelines(line)

    fout.close();
    x = 0;
    for p in split_patterns_no_line:
        x = x + len(p);
    for p in split_patterns_line:
        x = x + len(p);
    assert x == 0, [split_patterns_line, split_patterns_no_line]
def save_mat(file_name, arr, str_type):
    with open(file_name, 'wb') as fp:
        rows, cols = arr.shape
        fp.write(struct.pack('i', rows));
        fp.write(struct.pack('i', cols));
        if str_type == 'float' or str_type == 'float32':
            s = 'f';
        elif str_type == 'double':
            s = 'd';
        elif str_type == 'int':
            s = 'i';
        else:
            assert False, 'no supprted'
        arr2 = arr.reshape(arr.size);
        for v in arr2:
            fp.write(struct.pack(s, v));

def read_mat(file_name, str_type):
	fp = open(file_name, "rb");
	num = struct.unpack('i', fp.read(4));
	dim = struct.unpack('i', fp.read(4));
	if str_type == "float":
		item_size = 4;
	elif str_type == "int":
		item_size = 4;
	else:
		assert False;
	print type(num)
	if str_type == "float":
		mat = struct.unpack('f' * (num[0] * dim[0]),
			fp.read(item_size * num[0] * dim[0]));
	elif str_type == "int":
		mat = struct.unpack('i' * (num[0] * dim[0]),
			fp.read(item_size * num[0] * dim[0]));
	fp.close();
	mat = numpy.asarray(mat);
	mat = mat.reshape(num[0], dim[0]);
	return mat;

def read_blob(file_name, str_type):
	fp = open(file_name, "rb");
        dim = struct.unpack('i' * 4, fp.read(4 * 4));
	if str_type == "float":
		item_size = 4;
	elif str_type == "int":
		item_size = 4;
	else:
		assert False;
        total = dim[0] * dim[1] * dim[2] * dim[3]
	if str_type == "float":
		data = struct.unpack('f' * (total),
			fp.read(item_size * total));
		diff = struct.unpack('f' * (total),
			fp.read(item_size * total));
	elif str_type == "int":
		data = struct.unpack('i' * total,
			fp.read(item_size * total));
		diff = struct.unpack('i' * total,
			fp.read(item_size * total));
	fp.close();
	data = numpy.asarray(data);
        diff = numpy.asarray(diff);
	data = data.reshape(dim[0], dim[1], dim[2], dim[3]);
        diff = diff.reshape(dim[0], dim[1], dim[2], dim[3]);
	return data, diff;
def extract_acc(log_file, output_id = 0):
    fp = open(log_file, "r");
    lines = fp.readlines();
    fp.close();

    all_acc = [];
    all_iter = [];
    is_to_be = False;
    for line in lines:
        if 'Testing net (#0)' in line:
            split_line = line.split("Iteration");
            assert len(split_line) == 2, split_line;
            split_line = split_line[1].split(",");
            assert len(split_line) >= 1;
            to_be_iter = float(split_line[0]);
            is_to_be = True;
            print line
            continue;
        if is_to_be:
            assert "Test net output " in line, line
            phase = "Test net output #" + str(output_id)
            if phase in line:
                split_line = line.split("=");
                assert len(split_line) == 2;
                acc = float(split_line[1]);
                all_acc.append(acc);
                all_iter.append(to_be_iter);
                is_to_be = False;
    return numpy.array(all_acc), numpy.array(all_iter)

def extract_all_acc(log_file):
    if isinstance(log_file, str):
        all_acc, all_iter  = extract_acc(log_file);
    else:
        all_acc, all_iter = [], []
        for log in log_file:
            sub_acc, sub_iter = extract_acc(log);
            all_acc = all_acc + sub_acc;
            all_iter = all_iter + sub_iter;
    return numpy.array(all_acc), numpy.array(all_iter)

def plot_acc(log_file, output_file):
    if isinstance(log_file, str):
        all_acc, all_iter  = extract_acc(log_file);
    else:
        all_acc, all_iter = [], []
        for log in log_file:
            sub_acc, sub_iter = extract_acc(log);
            all_acc = all_acc + sub_acc;
            all_iter = all_iter + sub_iter;
    plt.close();
    plt.plot(all_iter, all_acc, '*-');
    plt.grid();
    plt.savefig(output_file);
    return all_acc, all_iter;

def concat_snap_prefix(snap_prefix, net_revise, solver_revise):
    net_revise_r = net_revise.replace(",", "_");
    net_revise_r = net_revise_r.replace("\"", "");
    solver_revise_r = solver_revise.replace(",", "_");
    solver_revise_r = solver_revise_r.replace("\"", "");

    snap_prefix = snap_prefix + net_revise_r + "_" + solver_revise_r;
    return snap_prefix;

def throw_cmd(machine_name, sub_cmd, log_file_name = ""):
    if isinstance(sub_cmd, list):
        s = "";
        for c in sub_cmd:
            s = s + " " + c;
        sub_cmd = s;
    cmd = [];
    cmd.append("ssh");
    cmd.append(machine_name);
    curr_dir = os.getcwd();
    sub_cmd = 'cd ' + curr_dir + ' && ' + \
            "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH" + ' && ' + \
            "source /home/wangjianfeng/.bashrc" + ' && ' + \
            sub_cmd;
    cmd.append(sub_cmd);

    print cmd
    if log_file_name == "":
        p = subprocess.Popen(cmd);
    else:
        with open(log_file_name, "w") as fp:
            p = subprocess.Popen(cmd, stdin=fp, stdout = fp, stderr = fp);
    return p;

def train_revise_solver_asyc(net_base, solver_base, net_revise, solver_revise,
		machine_name, device_id, caffe_dir, snap_prefix,
		finetune = "", continuing = ""):
    if caffe_dir[len(caffe_dir) - 1] != "/":
    	caffe_dir = caffe_dir + "/";

    net_revise_r = net_revise.replace(",", "_");
    net_revise_r = net_revise_r.replace("\"", "");
    solver_revise_r = solver_revise.replace(",", "_");
    solver_revise_r = solver_revise_r.replace("\"", "");

    snap_prefix = snap_prefix + net_revise_r + "_" + solver_revise_r;
    net_use = snap_prefix + "_" + "n.prototxt";
    solver_use = snap_prefix + "_" + "s.prototxt";

    change_field(net_base, net_revise, net_use);
    whole_solver_revise = solver_revise + \
    					  ",snapshot_prefix:" + "\"" + snap_prefix + "\"" + \
    					  ",net:" + "\"" + net_use + "\"" + \
    					  ",device_id:" + str(device_id);
    change_field(solver_base, whole_solver_revise, solver_use);

    cmd = [];
    cmd.append("ssh");
    cmd.append(machine_name);

    sub_cmd = "";
    sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH";
    sub_cmd = sub_cmd + " && ";
    sub_cmd = sub_cmd + caffe_dir + "caffe train --solver=" + solver_use;
    if finetune != "":
    	sub_cmd = sub_cmd + " --weights=" + finetune;
    if continuing != "":
    	sub_cmd = sub_cmd + " --snapshot=" + continuing;

    log_file_name = snap_prefix + "_log.tee";
    if os.path.isfile(log_file_name) == True:
        print log_file_name;
        assert False;
    #sub_cmd = sub_cmd + " 2>&1 | tee -a " + log_file_name;
    sub_cmd = sub_cmd + " && ";
    sub_cmd = sub_cmd + "python /home/wangjianfeng/code/mycode/python_code/best_acc.py " + \
              log_file_name;
    cmd.append(sub_cmd);

    print cmd;
    with open(log_file_name, "w") as fp:
        p = subprocess.Popen(cmd, stdin=fp, stdout = fp, stderr = fp);

    return p, log_file_name;

def gpu_mem_info(machine_name):
    cmd = [];
    cmd.append("ssh");
    cmd.append(machine_name);
    sub_cmd = "nvidia-smi | grep \'MiB /\'";
    cmd.append(sub_cmd);
    try:
        lines = subprocess.check_output(cmd);
    except:
        return [], [];
    all_device_id = [];
    lines = lines.splitlines();
    assert len(lines) == 3 or len(lines) == 4, lines
    device_id = 0;
    for line in lines:
        line_splits = line.split("|");
        assert len(line_splits) > 3;
        str = line_splits[2];
        str_split = str.split("/");
        assert len(str_split) == 2;
        str_split[0] = str_split[0].strip();
        str_split[0] = str_split[0][0 : -3];
        used_mem = float(str_split[0]);
        if used_mem < 30:
           all_device_id.append(device_id);
        device_id = device_id + 1;
    all_machine_name = [machine_name for i in range(len(all_device_id))];
    return all_device_id, all_machine_name;

def get_free_gpu(exclude = []):
    all_machine_id = ["01", "02", "03", "04",
                      "05", "06", "07", "08",
                      "09", "10", "11", "12",
                      "13", "14", "15", "16",
                      "17", "18"];
    for ex in exclude:
        all_machine_id.remove(ex);
    all_device_id = [];
    all_machine_name = [];
    random.shuffle(all_machine_id);
    all_id = [];
    all_name = [];
    all_num = [];
    for id in all_machine_id:
    	machine_name = "deep" + id;
        print machine_name;
    	sub_all_device_id, sub_all_machine_names = gpu_mem_info(machine_name);
        all_id.append(sub_all_device_id);
        all_name.append(sub_all_machine_names);
        all_num.append(len(sub_all_device_id));
    seq = sorted(range(len(all_num)), key = lambda k : all_num[k], reverse = True);
    for i in seq:
        all_machine_name.extend(all_name[i]);
        all_device_id.extend(all_id[i]);
    return all_device_id, all_machine_name;

def get_free_gpu_lsls(exclude = []):
    all_machine_id = ["01", "02", "03", "04",
                      "05", "06", "07", "08",
                      "09", "10", "11", "12",
                      "13", "14", "15", "16", "17", "18"];
    for ex in exclude:
        all_machine_id.remove(ex);
    random.shuffle(all_machine_id);
    all_id = [];
    all_name = [];
    all_num = [];
    for id in all_machine_id:
    	machine_name = "deep" + id;
        print machine_name;
    	sub_all_device_id, sub_all_machine_names = gpu_mem_info(machine_name);
        all_id.append(sub_all_device_id);
        all_name.append(sub_all_machine_names);
        all_num.append(len(sub_all_device_id));
    seq = sorted(range(len(all_num)),
                 key = lambda k : all_num[k], reverse = True);
    all_machine_name = [];
    all_device_id = [];
    for i in seq:
        assert len(all_name[i]) == len(all_id[i])
        if len(all_name[i]) == 0:
            continue;
        all_machine_name.append(all_name[i][0]);
        all_device_id.append(all_id[i]);
    return all_device_id, all_machine_name;

def copy_data(from_machine, from_folder, to_machine, to_folder):
    cmd = [];
    cmd.append('rsync');
    cmd.append('wangjianfeng@' + from_machine + ':' + from_folder);
    cmd.append('wangjianfeng@' + to_machine);

def list_files(path):
	result = [];
	for root, directory, files in os.walk(path):
		for f in files:
			result.append(os.path.join(root, f));
	return result;


file_name = "/home/wangjianfeng/caffe-parsingLabelPriorMutiScaleWeightLoss/examples/labelPrior_m/imagelistTestHD.txt";
file_name_out = "/home/wangjianfeng/caffe-parsingLabelPriorMutiScaleWeightLoss/examples/labelPrior_m/imagelistTestHD2.txt";

prefix = "/home/liangxiaodan/superParsing/";

fp = open(file_name, "r");
all_lines = fp.readlines();
fp.close();

fp_out = open(file_name_out, "w");
for line in all_lines:
    line_splits = line.split();
    if len(line_splits) != 3:
        break;
    out = [];
    for s in line_splits:
        idx = s.rfind('../');
        out.append(s[idx + 3:]);
    str = prefix + out[0] + " " + prefix + out[1] + " " + prefix + out[2] + "\n";
    fp_out.writelines(str);
    
fp_out.close();

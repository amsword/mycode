An implementation of 
Jianfeng Wang, Heng Tao Shen, Shuicheng Yan, Nenghai Yu, Shipeng Li, and Jingdong Wang. 
"Optimized Distances for Binary Code Ranking". ACM Multimedia (ACM MM), 2014

Please run demo_optimized_distance.m to train the model, 
test the performance, and plot the comparison result.

Note:
In the demo, the training data and test data are randomly generated. 
Please modify 






function W = trainITQ(Xtraining, bit)

% generate training ans test split and the data matrix
XX = Xtraining';
% center the data, VERY IMPORTANT
% assert(sum(mean(Xtraining, 2)) < 0.0001);

% PCA
[pc, l] = eigs(cov(XX),bit);
XX = XX * pc;
[~, R] = ITQ(XX,100);
W = pc * R;   
W = [W; zeros(1, bit)];
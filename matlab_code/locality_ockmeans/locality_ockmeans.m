function lock_model = locality_ockmeans(...
    Xtraining, para)
%%
% num_coarse_cluster = 256;
num_coarse_cluster = 256;
fine_num_partitions = 8;
fine_sub_dic_size_each_partition = 256;

para_coarse = [];
fine_max_iter = [];

if exist('para', 'var')
    if isfield(para, 'num_coarse_cluster')
        num_coarse_cluster = para.num_coarse_cluster;
    end
    if isfield(para, 'fine_num_partitions')
        fine_num_partitions = para.fine_num_partitions;
    end
    if isfield(para, 'fine_sub_dic_size_each_partition')
        fine_sub_dic_size_each_partition = para.fine_sub_dic_size_each_partition;
    end
    
    if isfield(para, 'coarse_max_iter')
        para_coarse.max_iter = para.coarse_max_iter;
    end
    
    if isfield(para, 'fine_max_iter')
        opt_input_ock.max_iter = para.fine_max_iter;
    end
end

coarse_centers = w_kmeans(Xtraining, num_coarse_cluster);
[d] = sqdist(Xtraining, coarse_centers);
[tmp, coarse_idx] = min(d, [], 2);

opt_input_ock.num_partitions = fine_num_partitions;
opt_input_ock.sub_dic_size_each_partition = fine_sub_dic_size_each_partition;

all_ock_model = cell(num_coarse_cluster, 1);
for i = 1 : num_coarse_cluster
    subXtraining = Xtraining(:, coarse_idx == i);
    subXtraining = bsxfun(@minus, subXtraining, coarse_centers(:, i));
    all_ock_model{i} = ock_training(subXtraining, opt_input_ock);
end

lock_model.coarse_centers = coarse_centers;
lock_model.all_ock_model = all_ock_model;
function eval = jf_test_usplh(W, Xtest, Xbase, StestBase, topK, trueRank_fileseed)
eval = jf_eval_hash3(size(W, 2), 'linear', W, Xtest, Xbase, StestBase, topK, trueRank_fileseed);
function [W_usplh para_usplh] = jf_train_usplh(Xtraining, nb)

USPLHparam.nbits = nb;
USPLHparam.eta=.125;
% USPLHparam.lambda=0.25;
USPLHparam.c_num=100;

fprintf('start USPLH training\n');
USPLHparam = trainUSPLH(Xtraining', USPLHparam);
W_usplh = [USPLHparam.w; USPLHparam.b];

para_usplh = USPLHparam;
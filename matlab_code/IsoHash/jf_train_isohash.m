function W = jf_train_isohash(Xtraining, m, type)

[pc, l] = eigs(cov(Xtraining'),m);

if strcmp(type, 'lp')
    Q = LiftProjection(l,100);
elseif strcmp(type, 'gf')
    Q = GradientFlow(l);
end

W = pc * Q;

W = [W; zeros(1, m)];
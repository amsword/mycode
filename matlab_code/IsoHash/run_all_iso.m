for type = {'labelme', 'peekaboom', 'sift_1m'}; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    % CIFAR10, labelme, sift_1m, peekaboom, GIST1M3, SIFT1M3
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

    for m = [8, 16, 32, 64, 128, 256]
        if strcmp(type, 'sift_1m') && m == 256
            break;
        end
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        if (is_train)
            [W_isohash_lp] = jf_train_isohash(Xtraining, m, 'lp');
            assert(~isempty(strfind(save_file.train_isohash_lp, num2str(m))));
            save(save_file.train_isohash_lp, 'W_isohash_lp');
            
            [W_isohash_gf] = jf_train_isohash(Xtraining, m, 'gf');
            assert(~isempty(strfind(save_file.train_isohash_gf, num2str(m))));
            save(save_file.train_isohash_gf, 'W_isohash_gf');
        else
            load(save_file.train_isohash_lp, 'W_isohash_lp');
            load(save_file.train_isohash_gf, 'W_isohash_gf');
        end
        
        if (is_test)
            eval_isohash_lp = cell(numel(all_topks), 1);
            for k = 1 : numel(all_topks)
                eval_isohash_lp{k} = eval_hash5(size(W_isohash_lp, 2), 'linear', ...
                    W_isohash_lp, Xtest, Xtraining, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_lp, 'eval_isohash_lp');
            
            eval_isohash_gf = cell(numel(all_topks), 1);
            for k = 1 : numel(all_topks)
                eval_isohash_gf{k} = eval_hash5(size(W_isohash_gf, 2), 'linear', ...
                    W_isohash_gf, Xtest, Xtraining, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_gf, 'eval_isohash_gf');
        end
    end
end
%%
for type = {'GIST1M3', 'SIFT1M3'}; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    % CIFAR10, labelme, sift_1m, peekaboom, GIST1M3, SIFT1M3
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    for m = [8, 16, 32, 64, 128, 256]
        if strcmp(type, 'SIFT1M3') && m == 256
            break;
        end
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        if (is_train)
            [W_isohash_lp] = jf_train_isohash(Xtraining, m, 'lp');
            assert(~isempty(strfind(save_file.train_isohash_lp, num2str(m))));
            save(save_file.train_isohash_lp, 'W_isohash_lp');
            
            [W_isohash_gf] = jf_train_isohash(Xtraining, m, 'gf');
            assert(~isempty(strfind(save_file.train_isohash_gf, num2str(m))));
            save(save_file.train_isohash_gf, 'W_isohash_gf');
        else
            load(save_file.train_isohash_lp, 'W_isohash_lp');
            load(save_file.train_isohash_gf, 'W_isohash_gf');
        end
        
        if (is_test)
            eval_isohash_lp = cell(numel(all_topks), 1);
            for k = 1 : numel(all_topks)
                eval_isohash_lp{k} = eval_hash5(size(W_isohash_lp, 2), 'linear', ...
                    W_isohash_lp, Xtest, Xbase, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_lp, 'eval_isohash_lp');
            
            eval_isohash_gf = cell(numel(all_topks), 1);
            for k = 1 : numel(all_topks)
                eval_isohash_gf{k} = eval_hash5(size(W_isohash_gf, 2), 'linear', ...
                    W_isohash_gf, Xtest, Xbase, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_gf, 'eval_isohash_gf');
        end
    end
end
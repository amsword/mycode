function [deltaW] = jf_deltaW_4(W, X, centerIndex, alpha, beta, gamma, lambda, mu, dk, dk2, subI)
%% X and W: the aumented one. 
%% type_active_set:	0:	no active set is applied
%%					1:	the same as the paper describes
%%					2: 	another type	

% hash_dist_pair
N = size(X, 2);
num_center = length(centerIndex);

m = size(W, 2);

WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
s_sigX = sigX .* sigX;
right = sum(s_sigX, 1);
left = right(1, centerIndex)';

hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
			2 * sigX(:, centerIndex)' * sigX;

% N = size(X, 2);

rho = length(dk);
deltaW1 = zeros(size(W));

% D1 = sigX .* (1 - sigX);
% E1 = D1(:, centerIndex);

subJ = ~subI;
for k = 1 : rho
    % prepare F and G-->
    dist_diff = 1 - hash_dist + dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
    P = dist_diff .* (1 - dist_diff);
	
	subIk = subI(:, :, k);
	subJk = subJ(:, :, k);
% 	if (type_active_set == 1)
% 		subJk = subJk & (hash_dist <= dk2(k));
% 	elseif (type_active_set == 2)
	subIk = subIk & (hash_dist > dk2(k));
	subJk = subJk & (hash_dist <= dk2(k));
%     end
   		
    P = (lambda * gamma(k, 2)) * (subJk .* P);
    
    dist_diff = hash_dist - dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    F = dist_diff .* (1 - dist_diff);
    F = subIk .* F;
    
    P = gamma(k, 1) * F - P;
    
    for t = 1 : m
		diff_ij = repmat(sigX(t, centerIndex)', 1, N) - ...
					repmat(sigX(t, :), num_center, 1);
        % here, F is in fact the 'E';
		F = sigX(t, centerIndex) .* (1 - sigX(t, centerIndex));
		F = repmat(F', 1, N) .* diff_ij;
		F = P .* F;
		F = sum(F, 2);
		F = X(:, centerIndex) * F;
        
        D = sigX(t, :) .* (1 - sigX(t, :));
        D = repmat(D, num_center, 1) .* diff_ij;
		D = P .* D;
        D = sum(D, 1);
        D = X * D';
        
        F = F - D;
        deltaW1(:, t) = deltaW1(:, t) + alpha(k) * F;
    end
end

deltaW1 = deltaW1 * (2 * beta / num_center);
deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
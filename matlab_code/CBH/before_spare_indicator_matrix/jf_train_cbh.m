function y = jf_train_cbh(Xtraining, I, cbh_para)
%% cbh_para.mu will be useless. Please delete it later

th_how_good_mu = 0.3; % the threshold for finding the first appropriate mu
mu_step = 4; % the change of mu;
total_neighbors = sum(cbh_para.num_neighbors);
max_search_mu_times = 15;
for i_lambda = 1 : length(cbh_para.lambda)
    para = cbh_para;
    curr_lambda = cbh_para.lambda(i_lambda);
      
	para.lambda = curr_lambda;
    
   
%     ref_mu = total_neighbors * curr_lambda / (curr_lambda + 1); % 1330 is the sum of the graded neighbors
    ref_mu = total_neighbors * curr_lambda;
    ref_mu = max(1, ref_mu);
%     ref_mu = ref_mu / mu_step;
   
%     ref_mu = 0;
	%% find the appropriate mu, that makes the mean(diag(W'*W-1)) large enough
	% the reason is the first mu should not be too large
%     while(1);
        para.mu = ref_mu;
  
		logger(sprintf('Find appropriate mu: start training with lambda = %f, mu = %f\n', ...
            para.lambda, para.mu));
        tic;
      
		W = jf_trainSig(Xtraining, I, para); % the input: the data and the groundtruth, which is obtained by jf_prepare_training_data
        how_good_mu = mean(abs(diag(W' * W - 1)));
		logger(sprintf('end finding mu, mu = %f, with lambda = %f, mean(|diag(W * W - 1)|) = %f, time cost: %f\n', ...
              para.mu, para.lambda, how_good_mu, toc / 60));
%         if (how_good_mu > th_how_good_mu || para.mu <= 1)
% 			break;
% 		else
% 			ref_mu = ref_mu / mu_step;
% 		end
% 	end
	preW = W;
	
    for i_mu = 1 : max_search_mu_times
	    ref_mu = ref_mu * mu_step;
		para.mu = ref_mu;
		W = jf_trainSig(Xtraining, I, para, preW);
        changed = sum(abs(W(:) - preW(:))) / sum(abs(W(:)));
        how_normalize = mean(abs(diag(W' * W - 1)));
        logger(sprintf('end, mu = %f, with lambda = %f, changed_W = %f, w2-1 = %f, time cost: %f\n', ...
                    para.mu, para.lambda, changed, how_normalize, toc / 60));
        preW = W;
        if (how_normalize < 0.01 && changed < 0.05)
            break;
        end
	end
    y(i_lambda).W = W;
    y(i_lambda).para = para;
end
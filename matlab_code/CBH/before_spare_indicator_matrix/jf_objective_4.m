function y = jf_objective_4(W, X, centerIndex, alpha, beta, gamma, lambda,mu, dk, dk2, subI)
%% dk: the threshold in the non-smooth objective version
%% dk2: the threshold in the smooth objective version. when no active set is applied, it is useless
%% the difference compared with _2s, is the smoothed d_(ij)

N = size(X, 2);
num_center = length(centerIndex);

WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
s_sigX = sigX .* sigX;
right = sum(s_sigX, 1);
left = right(1, centerIndex)';
hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
			2 * sigX(:, centerIndex)' * sigX;

% test_1 = 1;
% if (type_active_set == 2)
%     test_1 = 0;
  %  dk2 = dk;
   % dk2(dk2 == 0) = 0.5;
% end

rho = length(dk);
s = 0;
for k = 1 : rho
    diff_dist = hash_dist - dk(k);
    diff_dist = sigmf(diff_dist, [gamma(k, 1), 0]);
            
    subIk = subI(:, :, k);
    
%     if (type_active_set == 2)
    subIk = subIk & (hash_dist > dk2(k));
%     end
    
    diff_dist = subIk .* diff_dist;
    
    s = s + alpha(k) * sum(diff_dist(:));
end

subI = ~subI; % after negation, the subI is in fact subJ
s1 = 0;
for k = 1 : rho
     diff_dist = 1 - hash_dist + dk(k);
     diff_dist = sigmf(diff_dist, [gamma(k, 2), 0]);
    
     subJk = subI(:, :, k);
% 	if (type_active_set ~= 0) % 1 or 2
    subJk = subJk & (hash_dist <= dk2(k));
%     end
    
	diff_dist = subJk .* diff_dist;
	s1 = s1 + alpha(k) * sum(diff_dist(:));
end

wt = diag(W' * W - 1);
y = (s + s1 * lambda) / length(centerIndex) + 1 / 4 * mu * (wt' * wt);
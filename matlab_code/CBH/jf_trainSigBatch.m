function [W, para] = jf_trainSigBatch(data, I, para, initW)
%% data: every column is a point in a high-dimensional space. size: [D N]
%% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
%% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
%% W: the mapping function is W' * X;

%% parameters

beta = para.beta;
[D, N] = size(data);
pd = 1;
%% init W.
if (exist('initW', 'var'))
    'W has been initilized'
    W = initW;
else
    'randome generate W'
    W = [randn(D, para.m); zeros(1, para.m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
end

X = [data; ones(1, N)];

% rd_dk = jf_relax_dk(m, para.dk, para.epsilon);
rd_dk = para.dk;
rd_dk(rd_dk <= 0) = 0.3;
rd_dk2 = rd_dk - 1; % the threshold used for I_{ijk} and J_{ijk}, or be taken as the threshold in smoothed objective domain
% dk2 = dk;
rd_dk2(rd_dk2 <= 0) = 0.3;
rd_dk3 = rd_dk + 1;
rd_dk3(rd_dk3 <= 0) = 0.01;
para.rd_dk = rd_dk;
para.rd_dk2 = rd_dk2;
para.rd_dk3 = rd_dk3;

mu = para.mu;
max_iter = para.max_iter;
min_abs_deltaW = para.min_abs_deltaW;
num_center_sample = min(para.num_center_sample, N);

gamma = para.gamma;
%% calculate alpha_k, the parameters are not defined in the original paper
rho = length(para.dk);

lambda = para.lambda;
alpha = para.alpha;

c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
c2 = 0.5; % change of the step
iter = 0;
max_step_percent = 0.2;
max_iter_find_step = 10;

subI = cell(1, rho);
bad_case = 0;
para
gamma
rd_dk
abs_delta = zeros(max_iter, 1);
relaxed_obj = zeros(max_iter, 1);
exact_obj = zeros(max_iter, 1);

deltaW = zeros(size(W));
if isfield(para, 'pre_deltaW')
    deltaW = para.pre_deltaW;
end

all_W = cell(max_iter, 1);

if (para.is_zero_bias)
    W(end, :) = 0;
end

bad = 0;
max_bad = 5;
while(iter < max_iter && bad < max_bad)
    % calculate the deltaW
    iter = iter + 1; % the sentence is better not to place at the end, because there are some 'continue' within the loop
   
    pre_deltaW = deltaW;
    [deltaW pre how_large_beta] = jf_deltaW_11(...
        W, X, ...
        alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, 1, ...
        I);
    
    all_W{iter} = W;
    
     pre =  cbh_true_obj(W, X, rd_dk, alpha, lambda, I);
     relaxed_obj(iter) = pre;
     exact_obj(iter) = pre;
     
    deltaW = para.momentum * pre_deltaW + ...
        (1 - para.momentum) * deltaW;
    
    length_deltaW = norm(deltaW(:));
    length_W = norm(W(:));
    step = max_step_percent * length_W  / length_deltaW; 
    abs_delta(iter) = length_deltaW;
	squared_length_deltaW = length_deltaW * length_deltaW;
	
    if (length_deltaW < min_abs_deltaW)
        str = ['iter: ' num2str(iter) '. deltaW = 0\n'];
        fprintf(str);
        continue;
    end

    'set learning rate dynamically'
    for i = 1 : max_iter_find_step
        W1 = W - step * deltaW;
        
        if (para.is_zero_bias)
            W1(end, :) = 0;
        end
        
        after = cbh_true_obj(W1, X, rd_dk, alpha, lambda, I);
        
        target = pre - (c1 * step) * squared_length_deltaW;
        if (target <= 0)
            target = pre;
        end
        if (after <= target + 1e-5)
            break;
        else
            step = step * c2;
        end
    end
    
    if i == max_iter_find_step
        if how_large_beta > 0.9
            beta = beta / 2;
        else
            beta = beta * 2;
        end
        bad = bad + 1;
        W1 = W;
        gamma = 2 * gamma;
    else
        bad = 0;
    end
	
    W_changed = norm(W1(:) - W(:)) / norm(W(:));
	obj_changed = (pre - after) / pre;
	str = ['iter: ' num2str(iter) ', ' ...
        'i: ' num2str(i) ', ' ...
        'mu: ' num2str(mu) ...
        '. obj: ' num2str(pre) '->' ...
            num2str(after) ...
            '. W decent: ' num2str(W_changed) ...
            '. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
            '\n'];
	
	fprintf(str);
    
    W = W1;
    
%     s = sum(W .^ 2, 1);
%     W = bsxfun(@rdivide, W, s);
   
end
if (iter >= max_iter)
    para.stop_type = 'max_iter_reached';
end

% if (~isfield(para, 'abs_delta'))
% 	idx_abs_delta = 1;
% else
% 	idx_abs_delta = numel(para.abs_delta) + 1;
% end
% para.abs_delta{idx_abs_delta} = abs_delta;

para.pre_deltaW = deltaW;

if (isfield(para, 'relaxed_obj'))
    relaxed_obj = [para.relaxed_obj; relaxed_obj(1 : iter)];
    para.exact_obj = reshape(para.exact_obj, numel(para.exact_obj), 1);
    exact_obj = [para.exact_obj; exact_obj(1 : iter)];
    all_W = [para.all_W; all_W(1 : iter)];
else
    relaxed_obj = relaxed_obj(1 : iter);
    exact_obj = exact_obj(1 : iter);
    all_W = all_W(1 : iter);
end
para.relaxed_obj = relaxed_obj;
para.exact_obj = exact_obj;
para.all_W = all_W;
#include "mex.h"
#include <assert.h>
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>
//#include "acml.h"

// #pragma comment (lib, "libacml_mp.lib")


void parse_double_matrix(const mxArray* prhs,
				  int *m, int *n, double** ptr)
{
	assert(mxIsDouble(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (double*)mxGetPr(prhs);
}

void parse_int8_matrix(const mxArray* prhs,
				  int *m, int *n, char** ptr)
{
	assert(mxIsInt8(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	
	*ptr = (char*)mxGetPr(prhs);
}

void parse_int_matrix(const mxArray* prhs,
				  int *m, int *n, int** ptr)
{
	assert(mxIsInt32(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (int*)mxGetPr(prhs);
}

 //[deltaW1] = jf_c_deltaW1(...
 //           X_i, X_j, diff_sigX_i, diff_sigX_j, diff_o, 
//				sig_wx_i, sig_wx_j);
void mexFunction_o(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int D_1;
	int N;
	int m;
	double* p_x;
	double* p_diff_sigX;
	double* p_diff_o;
	double* p_sig_wx;
	int* p_center_index;
	double* p_delta_w1;
	double left, right, ci, cj;
	int int_tmp, i, i2, j, t, k, n_center_index;
	double* p_t, *p_i2, *p_j;
	double coef;
	
	parse_double_matrix(prhs[0], &D_1, &N, &p_x);
	parse_double_matrix(prhs[1], &m, &N, &p_diff_sigX);
	parse_double_matrix(prhs[2], &n_center_index, &N, &p_diff_o);
	
	parse_double_matrix(prhs[3], &m, &N, &p_sig_wx);

	parse_int_matrix(prhs[4], &int_tmp, &int_tmp, &p_center_index);

	plhs[0] = mxCreateDoubleMatrix(D_1, m, mxREAL);
	p_delta_w1 = mxGetPr(plhs[0]);
	
	memset(p_delta_w1, 0, D_1 * m * sizeof(double));
	
	for (t = 0; t < m; t++)
	{
		for (j = 0; j < N; j++)
		{
			for (i = 0; i < n_center_index; i++)
			{
				i2 = p_center_index[i] - 1;
				left = p_sig_wx[i2 * m + t];
				right = p_sig_wx[j * m + t];
				if (left == right)
				{
					continue;
				}
				else if (left < right)
				{
					coef = -p_diff_o[j * n_center_index + i];
				}
				else
				{
					coef = p_diff_o[j * n_center_index + i];
				}
		
				ci = coef * p_diff_sigX[i2 * m + t];
				cj = coef * p_diff_sigX[j * m + t];
		
				p_i2 = p_x + D_1 * i2;
				p_j = p_x + D_1 * j;
				p_t = p_delta_w1 + D_1 * t;
				for (k = 0; k < D_1; k++)
				{
				/*	(*p_t) += ci * (*p_i2++) - cj * (*p_j++); 
					p_t++;*/
					p_t[k] = ci * p_i2[k] - cj * p_j[k];
				}
			}
		}
	}

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int D_1;
	int N;
	int m;
	double* p_x_i, *p_x_j;
	double* p_diff_sig_x_i, *p_diff_sig_x_j;
	double* p_diff_o;
	double* p_sig_wx_i, *p_sig_wx_j;
	double* p2_sig_wx_i, *p2_sig_wx_j;
	int* p_center_index;
	double* p_delta_w1;

	int int_tmp, i, i2, j, t, k, n_center_index;
	
	
	int inc = 1;
	
	
	parse_double_matrix(prhs[0], &D_1, &n_center_index, &p_x_i);
	parse_double_matrix(prhs[1], &D_1, &N, &p_x_j);

	parse_double_matrix(prhs[2], &m, &n_center_index, &p_diff_sig_x_i);
	parse_double_matrix(prhs[3], &m, &N, &p_diff_sig_x_j);

	parse_double_matrix(prhs[4], &n_center_index, &N, &p_diff_o);
	
	parse_double_matrix(prhs[5], &m, &n_center_index, &p_sig_wx_i);
	parse_double_matrix(prhs[6], &m, &N, &p_sig_wx_j);

	plhs[0] = mxCreateDoubleMatrix(D_1, m, mxREAL);
	p_delta_w1 = mxGetPr(plhs[0]);
	
	memset(p_delta_w1, 0, D_1 * m * sizeof(double));
	omp_set_num_threads(m);
	
	#pragma omp parallel for shared(p_diff_o, p_delta_w1, \
		p_x_i, p_x_j, N, n_center_index, \
		D_1, p_sig_wx_i, p_sig_wx_j, \
		p_diff_sig_x_i, p_diff_sig_x_j, \
		m)
	for (t = 0; t < m; t++)
	{
		int i, j;
		double* p_t, *p_t_const, *p_i, *p_i_const, *p_j, *p_j_const;
		double left, right, ci, cj, tmp_o, coef;
		int k;
		
		// mexPrintf("num thread: %d\n", omp_get_thread_num());
		p_t_const = p_delta_w1 + D_1 * t;
		for (j = 0; j < N; j++)
		{
			p_j_const = p_x_j + D_1 * j;
			right = p_sig_wx_j[j * m + t];
				
			for (i = 0; i < n_center_index; i++)
			{
				tmp_o = p_diff_o[j * n_center_index + i];
				left = p_sig_wx_i[i * m + t];
				if (left == right)
				{
					continue;
				}
				coef = left < right? -tmp_o : tmp_o;
		
				ci = coef * p_diff_sig_x_i[i * m + t];
				cj = -coef * p_diff_sig_x_j[j * m + t];
		
				p_i = p_x_i + D_1 * i;
				p_j = p_j_const;
				p_t = p_t_const;
				for (k = 0; k < D_1; k++)
				{
					
					//p_t[k] += ci * p_i[k] + cj * p_j[k];
					*p_t += ci * (*p_i++) + cj * (*p_j++);
					p_t++;
				}
				//DAXPY(&D_1, &ci, p_i, &inc, p_t, &inc);
				//DAXPY(&D_1, &cj, p_j, &inc, p_t, &inc);
				//daxpy(D_1, ci, p_i, 1, p_t, 1);
				//daxpy(D_1, cj, p_j, 1, p_t, 1);
			}
		}
	}

}
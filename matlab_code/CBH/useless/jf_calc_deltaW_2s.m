function [deltaW hash_dist_pair] = jf_calc_deltaW_2s(W, X, centerIndex, alpha, beta, gamma, lambda, mu, dk, dk2, subI, type_active_set)
%% X and W: the aumented one. 
%% type_active_set:	0:	no active set is applied
%%					1:	the same as the paper describes
%%					2: 	another type	

% hash_dist_pair
WX = W' * X;
m = size(WX, 1);
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
hash_dist_pair = m / 2 - 1 / 2 * twoSigX(:, centerIndex)' * twoSigX;

% N = size(X, 2);

rho = length(dk);
deltaW1 = zeros(size(W));

D1 = sigX .* (1 - sigX);
E1 = D1(:, centerIndex);

num_center = length(centerIndex);

% if (type_active_set == 1)
%	dk_hat = dk + 1;
% end

test_1 = 1;
% if (type_active_set == 2)
%     test_1 = 0;
%    dk2 = dk;
%    dk2(dk2 == 0) = 0.5;
% end

subJ = ~subI;
for k = 1 : rho
    % prepare F and G-->
    dist_diff = test_1 - hash_dist_pair + dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
    P = dist_diff .* (1 - dist_diff);
	
	subIk = subI(:, :, k);
	subJk = subJ(:, :, k);
	if (type_active_set == 1)
		subJk = subJk & (hash_dist_pair <= dk2(k));
	elseif (type_active_set == 2)
		subIk = subIk & (hash_dist_pair > dk2(k));
		subJk = subJk & (hash_dist_pair <= dk2(k));
    end
%     if (sum(subJk(:)) ~= 0)
%         k
%         sum(sum(hash_dist_pair <= dk2(k)))
%     end
    
		
    P = (lambda * gamma(k, 2)) * (subJk .* P);
    
    dist_diff = hash_dist_pair - dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    F = dist_diff .* (1 - dist_diff);
    F = subIk .* F;
    
    P = P - gamma(k, 1) * F;
    
    for t = 1 : m
        % here, F is in fact the 'E';
        F = E1(t, :)' * twoSigX(t, :);
        F = F .* P;
        F = sum(F, 2);
        F = X(:, centerIndex) * F;
        
        D = twoSigX(t, centerIndex)' * D1(t, :);
        D = D .* P;
        D = sum(D, 1);
        D = X * D';
        
        F = F + D;
        deltaW1(:, t) = deltaW1(:, t) + alpha(k) * F;
    end
end

deltaW1 = deltaW1 * (beta / num_center);
deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
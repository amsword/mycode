function y = jf_objective_function(W, X, beta, gamma, lambda,mu, dk, I, J)
WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
m = size(W, 2);
hash_dist = m / 2 - 1 / 2 * twoSigX' * twoSigX;
N = size(X, 2);

aug_dk(1, 1, :) = dk;
aug_dk = repmat(aug_dk, [N N 1]);
rho = length(dk);

diff_dist = repmat(hash_dist, [1 1 rho]) - aug_dk;
diff_dist2 = 1 - diff_dist;

sig_diff_dist = sigmf(diff_dist, [gamma, 0]);
sig_diff_dist2 = sigmf(diff_dist2, [gamma, 0]);

sig_sum = I .* sig_diff_dist + lambda * J .* sig_diff_dist2;

wt = diag(W' * W - 1);
y = sum(sum(sum(sig_sum))) + 1 / 4 * mu * wt' * wt;
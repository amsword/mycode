function y = jf_objective_6(W, X, centerIndex, alpha, beta, gamma, lambda,mu, dk, dk2, subI)
%% dk: the threshold in the non-smooth objective version
% dk2: the threshold in the smooth objective version. when no active set is applied, it is useless
% the difference compared with _2s, is the smoothed d_(ij)
% subI: a cell array, subI{k} is a sparse matrix 

N = size(X, 2);
num_center = length(centerIndex);

WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
s_sigX = sigX .* sigX;
right = sum(s_sigX, 1);
left = right(1, centerIndex)';
hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
			2 * sigX(:, centerIndex)' * sigX;

rho = length(dk);
s = 0;

inter = cell(rho, 1);
QueryI = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
end

for k = 1 : rho
    subIk = (subI{k} ~= inter{k});

    diff_dist = hash_dist(subIk(:)) - dk(k);
    diff_dist = sigmf(diff_dist, [gamma(k, 1), 0]);
	  
    s = s + alpha(k) * sum(diff_dist(:));
end

s1 = 0;
for k = 1 : rho
    subJk = (QueryI{k} ~= inter{k});
    diff_dist = 1 - hash_dist(subJk(:)) + dk(k);
    diff_dist = sigmf(diff_dist, [gamma(k, 2), 0]);
    
	s1 = s1 + alpha(k) * sum(diff_dist(:));
end

wt = diag(W' * W - 1);
y = (s + s1 * lambda) / length(centerIndex) + 1 / 4 * mu * (wt' * wt);
function deltaW = jf_calc_deltaW_1(W, X, beta, gamma, lambda, mu, dk, I, J)
%% X is the aumented one. The sufffix '1' is also an indicator that the
%% function can handle larger dataset.

% prepare five matrix
% I is ready, if stochastic sampling is applied, please modify it here
% X is also ready
% J is ready, if active set is applied, please modify it here

% hash_dist_pair
WX = W' * X;
m = size(WX, 1);
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
hash_dist_pair = m / 2 - 1 / 2 * (twoSigX' * twoSigX);

N = size(X, 2);

E1 = sigX .* (1 - sigX); 
rho = length(dk);
deltaW1 = zeros(size(W));
for k = 1 : rho
    % prepare F and G---->P
    dist_diff = hash_dist_pair - dk(k);
    dist_diff = sigmf(dist_diff, [gamma, 0]);
    F = dist_diff .* (1 - dist_diff);
    F = F .* I(:, :, k);
    
    dist_diff = 1 - hash_dist_pair + dk(k);
    dist_diff = sigmf(dist_diff, [gamma, 0]);
    P = dist_diff .* (1 - dist_diff);
    P = J(:, :, k) .* P;
    P = lambda * P;
    
    P = P - F;

    clear F dist_diff;
    
    for t = 1 : m
        E = E1(t, :)' * twoSigX(t, :);
        D = E';
        E = E .* P;
        E = sum(E, 2);
        E = X * E;
        
        D = D .* P;
        D = sum(D, 1)';
        D = X * D;
        
        E = E + D;
        deltaW1(:, t) = deltaW1(:, t) + E;
    end 
end

deltaW1 = deltaW1 * (beta * gamma / N);
deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
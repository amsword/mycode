function [W] = jf_mu_trainSig(data, I, para)

length_mu = length(para.mu);

max_nice_W = 2;

times_nice_W = 0;

for i_mu = 1 : length_mu
	sub_para = para;
	sub_para.mu = para.mu(i_mu);
	
	if (i_mu == 1)
		sub_W = jf_trainSig(data, I, sub_para);
	else
		sub_W = jf_trainSig(data, I, sub_para, W);
	end
    
    percent = sum(abs(sub_W(:) - W(:))) / sum(abs(sub_W(:)));
	
	if (percent < 0.001)
		times_nice_W = times_nice_W + 1;
		if (times_nice_W >= max_nice_W)
			break;
		end
	end
	
    ['mu: ' num2str(sub_para.mu) '. W changed: ' num2str(percent)]

    W = sub_W;
end
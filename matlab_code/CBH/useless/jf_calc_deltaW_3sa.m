function deltaW = jf_calc_deltaW_3sa(W, X, centerIndex, beta, gamma, lambda, mu, dk, dk2, I, J)
%% X is the aumented one. The sufffix '1' is also an indicator that the
%% function can handle larger dataset. stochastic gradient decent and
%% active set are applied. the suffix 's' means stochastic; 'a' means
%% active set

% prepare five matrix
% I is ready, if stochastic sampling is applied, please modify it here
% X is also ready
% J is ready, if active set is applied, please modify it here

% hash_dist_pair
WX = W' * X;
m = size(WX, 1);
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
hash_dist_pair = m / 2 - 1 / 2 * twoSigX(:, centerIndex)' * twoSigX;

N = size(X, 2);

rho = length(dk);
deltaW1 = zeros(size(W));

num_center = length(centerIndex);

% prepare E
left = (sigX .* (1 - sigX))';
right = twoSigX';
E1(:, 1, :) = left(centerIndex, :);
E2(1, :, :) = right;
E = repmat(E1, [1 N 1]) .* repmat(E2, [num_center 1 1]);
clear E1 E2;

ET1(1, :, :) = left;
ET2(:, 1, :) = right(centerIndex, :);
ET = repmat(ET1, [num_center 1 1]) .* repmat(ET2, [1 N 1]);
clear ET1 ET2;
clear left right;

for k = 1 : rho
    % prepare F and G
    dist_diff = hash_dist_pair - dk(k);
    dist_diff2 = 1 - dist_diff;
    sig_dist_diff = sigmf(dist_diff, [gamma, 0]);
    F = sig_dist_diff .* (1 - sig_dist_diff);
    sig_dist_diff2 = sigmf(dist_diff2, [gamma, 0]);
    G = sig_dist_diff2 .* (1 - sig_dist_diff2);
    
    % P
    kI = I(centerIndex, :, k);
    kJ = J(centerIndex, :, k) & (hash_dist_pair <= dk2(k));
    P = lambda * kJ .* G - kI .* F;

    EP = E .* repmat(P, [1 1 m]);
    EP = sum(EP, 2);
    EP = reshape(EP, size(EP, 1), size(EP, 3));

    EPT = ET .* repmat(P, [1 1 m]);
    EPT = sum(EPT, 1);
    EPT = reshape(EPT, size(EPT, 2), size(EPT, 3));

    deltaW1 = deltaW1 + X(:, centerIndex) * EP + X * EPT;
end

deltaW1 = deltaW1 * beta * gamma;
deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
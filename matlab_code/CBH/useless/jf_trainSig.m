function [W, para] = jf_trainSig(data, I, para, initW)
%% data: every column is a point in a high-dimensional space. size: [D N]
%% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
%% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
%% W: the mapping function is W' * X;

%% parameters
[D, N] = size(data);
m = para.m;

rd_dk = jf_relax_dk(m, para.dk, para.epsilon);

rd_dk2 = rd_dk + 0.3; % the threshold used for I_{ijk} and J_{ijk}, or be taken as the threshold in smoothed objective domain
% dk2 = dk;
% dk2(dk2 == 0) = 0.3;
rd_dk3 = rd_dk;
rd_dk3(rd_dk3 <= 0) = 0.01;
para.rd_dk = rd_dk;
para.rd_dk2 = rd_dk2;
para.rd_dk3 = rd_dk3;

mu = para.mu;
max_iter = para.max_iter;
min_abs_deltaW = para.min_abs_deltaW;
num_center_sample = min(para.num_center_sample, N);

para.I = I;

% beta and gamma, which is determined by dk and m
beta = -2 * log(para.epsilon) * sqrt(2);
para.beta = beta;

gamma = zeros(length(rd_dk), 2);
% gamma(:, 1) = -2 * log(para.epsilon_gama) ./ (m * (1 - 2 * para.epsilon)
% ^ 2  - 2 * rd_dk + rd_dk3);
gamma(:, 1) = -2 * log(para.epsilon_gama);
gamma(:, 2) = -2 * log(para.epsilon_gama) ./ (2 * rd_dk + 2 - rd_dk2);
para.gamma = gamma;

%% calculate alpha_k, the parameters are not defined in the original paper
rho = length(para.dk);

%% init W.
if (exist('initW', 'var'))
    W = initW;
else
    W = [randn(D, m); zeros(1, m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
end

X = [data; ones(1, N)];
  
lambda = para.lambda;
alpha = para.alpha;
if (para.is_smart_set_lambda)
    WX = W' * X;
    sigX = sigmf(WX, [beta, 0]);
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    
    max_size = 4 * 1024^3 / 8;
    batchsize = min(floor(max_size / N), N);
    nbatch = ceil(N / batchsize);

    c = zeros(rho, 1);
    for i = 1 : nbatch
        idx_start = 1 + (i - 1) * batchsize;
        idx_end = idx_start + batchsize - 1;
        idx_end = min(N, idx_end);
        centerIndex = idx_start : idx_end;
        num_center = length(centerIndex);
        left = right(1, centerIndex)';
        hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
            2 * sigX(:, centerIndex)' * sigX;
        for k = 1 : rho
            c(k) = sum(hash_dist(:) < rd_dk3(k));
        end
    end
    c = c / N;
    [alpha lambda] = jf_find_alpha_lambda(para.num_neighbors, c);
    mu = mu * sum(para.num_neighbors) / sum(alpha .* (para.num_neighbors + lambda * c));
    
    para.lambda = lambda;
    para.alpha = alpha;
end

c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
c2 = 0.5; % change of the step
iter = 0;
max_step_percent = 0.1;
max_iter_find_step = 50;

subI = cell(1, rho);
bad_case = 0;
while(iter < max_iter)
%     tic
    % calculate the deltaW
    iter = iter + 1; % the sentence is better not to place at the end, because there are some 'continue' within the loop
    center_index = randperm(N);
    center_index = center_index([1 : num_center_sample]);
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end
    
    [deltaW] = jf_deltaW_7(...
        W, X, center_index, ...
        alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, ...
        subI);
    
    length_deltaW = norm(deltaW(:));
    length_W = norm(W(:));
    step = max_step_percent * length_W  / length_deltaW; 
    
	squared_length_deltaW = length_deltaW * length_deltaW;
	
    if (length_deltaW < min_abs_deltaW)
        str = ['iter: ' num2str(iter) '. deltaW = 0\n'];
        % logger(str);
        % jf_print_back(last_info_length - 1); % '\n' is two characters.
        fprintf(str);
       
        continue;
    end
    
    pre = jf_objective_7(...
        W, X, center_index, ...
        alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, ...
        subI, true);
  
	for i = 1 : max_iter_find_step
	    W1 = W - step * deltaW;
        after = jf_objective_7(W1, X, center_index, ...
            alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, ...
            subI);
        
        target = pre - (c1 * step) * squared_length_deltaW;
        if (target <= 0)
            target = pre;
        end
        if (after <= target)
            break;
        else
            step = step * c2;
        end
    end
	
    W_changed = norm(W1(:) - W(:)) / norm(W(:));
	obj_changed = (pre - after) / pre;
	str = ['iter: ' num2str(iter) ...
        '. obj decent percent: ' num2str(100 * obj_changed) ...
            '. now_objective_value: ' num2str(after) ...
            '. step: ' num2str(step) ...
            '. W decent: ' num2str(W_changed) ...
            '. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
            '\n'];
	% logger(str);
	% jf_print_back(last_info_length - 1); % '\n' is two characters.
	fprintf(str);
	
	if (obj_changed < 10^-5)
		bad_case = bad_case + 1;
		if (bad_case >= 10)
			break;
		end
	else
		bad_case = 0;
	end
 
% 	if (obj_changed < 0.05)
% 		c1 = c1 * 0.5;
% 		c1 = max(0.01, c1);
% 	end
	
%    if (W_changed > 0.1)
%        max_iter = max(iter + 10, max_iter);
%        max_iter = min(max_iter, 100);
%    end
    
    W = W1;
    
%     tmp_collectW(:, :, tmp_collectW_idx) = W;
%     tmp_collectW_idx = tmp_collectW_idx + 1;
end
 
% for tmp_collectW_idx = 1 : max_iter
%     value(tmp_collectW_idx) = jf_objective_6(...
%         tmp_collectW(:, :, tmp_collectW_idx), X, 1 : N, ...
%         alpha, beta, gamma, lambda, mu, dk, dk2, ...
%         I);
% end
% plot(value, '-s');
%pause;

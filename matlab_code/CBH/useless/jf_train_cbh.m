function [W, out_para] = jf_train_cbh(Xtraining, I, cbh_para, initW)
%% cbh_para.mu will be useless. Please delete it later
% the routines estimate the mu and increase it dynamically

mu_step = 2; % the change of mu;
max_search_mu_times = cbh_para.max_search_mu_times;

para = cbh_para;
total_neighbors = sum(cbh_para.num_neighbors);
ref_mu = total_neighbors * cbh_para.lambda;
ref_mu = max(1, ref_mu) / 10;
para.mu = ref_mu;

str = ['start cbh training: mu = ' num2str(para.mu) ', '];
str = [str 'lambda = ' num2str(para.lambda) ', '];
str = [str 'mu_step = ' num2str(mu_step) ', '];
str = [str 'max_search_mu_times = ' num2str(max_search_mu_times) '\n '];
for i = 1 : length(cbh_para.num_neighbors)
    str = [str 'num_neighbors[' num2str(i) '] = ' num2str(cbh_para.num_neighbors(i)) '\n'];
end

fprintf(str);
tic;
if (exist('initW', 'var'))
    [W out_para] = jf_trainSig_cubic_step(Xtraining, I, para, initW); % the input: the data and the groundtruth, which is obtained by jf_prepare_training_data
else
    [W out_para] = jf_trainSig_cubic_step(Xtraining, I, para);
end
    how_good_mu = mean(abs(diag(W' * W - 1)));
fprintf('one end, mu = %f, mean(|diag(W * W - 1)|) = %f, time cost: %f\n', ...
    para.mu, how_good_mu, toc / 60);

para.is_smart_set_lambda = false;
preW = W;

for i_mu = 1 : max_search_mu_times
    ref_mu = ref_mu * mu_step;
    para.mu = ref_mu;
    tic;
	[W, out_para] = jf_trainSig_cubic_step(Xtraining, I, para, preW);
    changed = sum(abs(W(:) - preW(:))) / sum(abs(W(:)));
    how_normalize = mean(abs(diag(W' * W - 1)));
    fprintf('one end, mu=%f, change_W=%f, w2-1 = %f, time cost: %f\n', ...
        para.mu, changed, how_normalize, toc / 60);
    preW = W;
    if (how_normalize < 0.01 && changed < 0.01)
        break;
    end
end
function [deltaW] = jf_deltaW_6(W, X, centerIndex, alpha, beta, gamma, lambda, mu, dk, dk2, subI)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);
num_center = length(centerIndex);

m = size(W, 2);

WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
s_sigX = sigX .* sigX;
right = sum(s_sigX, 1);
left = right(1, centerIndex)';

hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
			2 * sigX(:, centerIndex)' * sigX;

rho = length(dk);

deltaW1 = zeros(size(W));

inter = cell(rho, 1);
QueryI = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
end

for k = 1 : rho
    [idx_i idx_j] =find(subI{k} ~= inter{k});
      
    idx_one = (idx_j - 1) * num_center + idx_i;
    
	dist_diff = hash_dist(idx_one) - dk(k);	
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
	dist_diff = dist_diff .* (1 - dist_diff);
	
	for t = 1 : m
		subSigI = sigX(t, centerIndex(idx_i))';
		subSigJ = sigX(t, idx_j)';
		sig_diff = subSigI - subSigJ;
		sig_diff = subSigI .* (1 - subSigI) .* sig_diff;
		sig_diff = sig_diff .* dist_diff;
		sig_diff = sparse(idx_i, idx_j, sig_diff, num_center, N);
		sig_diff = sum(sig_diff, 2);
		deltaWt1 = X(:, centerIndex) * sig_diff;
		
		sig_diff = subSigI - subSigJ;
		sig_diff = subSigJ .* (1 - subSigJ) .* sig_diff;
		sig_diff = sig_diff .* dist_diff;
		sig_diff = sparse(idx_i, idx_j, sig_diff, num_center, N);
		sig_diff = sum(sig_diff, 1)';
		deltaWt1 = deltaWt1 - X * sig_diff;
		
		deltaW1(:, t) = deltaW1(:, t) + (alpha(k) * gamma(k, 1)) * deltaWt1;
	end
end

for k = 1 : rho
	% in the following, subIk is in fact subJk, to save memory
	[idx_i idx_j] =find(QueryI{k} ~= inter{k});
	
	idx_one = (idx_j - 1) * num_center + idx_i;
	
	dist_diff = 1 - hash_dist(idx_one) + dk(k);	
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
	dist_diff = dist_diff .* (1 - dist_diff);
	for t = 1 : m
		subSigI = sigX(t, centerIndex(idx_i))';
		subSigJ = sigX(t, idx_j)';
		sig_diff = subSigI - subSigJ;
		sig_diff = subSigI .* (1 - subSigI) .* sig_diff;
		sig_diff = sig_diff .* dist_diff;
		sig_diff = sparse(idx_i, idx_j, sig_diff, num_center, N);
		sig_diff = sum(sig_diff, 2);
		deltaWt1 = X(:, centerIndex) * sig_diff;
		
		sig_diff = subSigI - subSigJ;
		sig_diff = subSigJ .* (1 - subSigJ) .* sig_diff;
		sig_diff = sig_diff .* dist_diff;
		sig_diff = sparse(idx_i, idx_j, sig_diff, num_center, N);
		sig_diff = sum(sig_diff, 1)';
		deltaWt1 = deltaWt1 - X * sig_diff;
		
		deltaW1(:, t) = deltaW1(:, t) - ...
			(lambda * alpha(k) * gamma(k, 2)) * deltaWt1;
	end
end

deltaW1 = deltaW1 * (2 * beta / num_center);
deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
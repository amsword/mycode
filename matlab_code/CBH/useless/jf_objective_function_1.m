function y = jf_objective_function_1(W, X, beta, gamma, lambda,mu, dk, I, J)
%% the suffix 1 means the function can handle dataset with larger points.
%% The larger the suffix is, the more data it can hanle. Here, we relax the
%% summation of k to use for sentence. 
WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
m = size(W, 2);
hash_dist = m / 2 - 1 / 2 * twoSigX' * twoSigX;
N = size(X, 2);

clear WX sigX twoSigX

rho = length(dk);
s = 0;
for k = 1 : rho
    diff_dist = hash_dist - dk(k);
    diff_dist = sigmf(diff_dist, [gamma, 0]);
    kI = I(:, :, k);
    diff_dist = kI .* diff_dist;
    
    s = s + sum(diff_dist(:));
end

s1 = 0;
for k = 1 : rho
    diff_dist = 1 - hash_dist + dk(k);
    diff_dist = sigmf(diff_dist, [gamma, 0]);
    kJ = J(:, :, k);
    diff_dist = kJ .* diff_dist;
    
    s1 = s1 + sum(diff_dist(:));
 end

wt = diag(W' * W - 1);
y = (s + lambda * s1) / N + 1 / 4 * mu * wt' * wt;
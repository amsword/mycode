function deltaW = jf_calc_deltaW(W, X, beta, gamma, lambda, mu, dk, I, J)
%% X is the aumented one

% prepare five matrix
% I is ready, if stochastic sampling is applied, please modify it here
% X is also ready
% J is ready, if active set is applied, please modify it here

% hash_dist_pair
WX = W' * X;
m = size(WX, 1);
sigX = sigmf(WX, [beta, 0]);
twoSigX = 2 * sigX - 1;
hash_dist_pair = m / 2 - 1 / 2 * twoSigX' * twoSigX;

N = size(X, 2);

% augmented dk, for calculation convenience
aug_dk(1, 1, :) = dk;
aug_dk = repmat(aug_dk, [N N 1]);

row = length(dk);
% prepare F and G
dist_diff = (repmat(hash_dist_pair, [1, 1, row]) - aug_dk);
dist_diff2 = 1 - dist_diff;
sig_dist_diff = sigmf(dist_diff, [gamma, 0]);
F = sig_dist_diff .* (1 - sig_dist_diff);
sig_dist_diff2 = sigmf(dist_diff2, [gamma, 0]);
G = sig_dist_diff2 .* (1 - sig_dist_diff2);

% prepare E
E1(:, 1, :) = (sigX .* (1 - sigX))';
E1 = repmat(E1, [1 N 1]);
E2(1, :, :) = twoSigX';
E2 = repmat(E2, [N 1 1]);
E = E1 .* E2;

%
P = lambda * J .* G - I .* F;
P = sum(P, 3);

EP = E .* repmat(P, [1 1 m]);
EP = sum(EP, 2);
EP = reshape(EP, size(EP, 1), size(EP, 3));

% EPT means EP'
EPT = E .* repmat(P', [1 1 m]);
EPT = sum(EPT, 2);
EPT = reshape(EPT, size(EPT, 1), size(EPT, 3));

deltaW1 = X * EP + X * EPT;
deltaW1 = deltaW1 * beta * gamma;

deltaW2 = mu * W * diag(diag(W' * W) - 1);

deltaW = deltaW1 + deltaW2;
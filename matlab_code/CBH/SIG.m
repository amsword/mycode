function Wsig = SIG(data, model_params, train_params, verbose)
%% function for loading and start training the SIFT data
% each training information comes in batch, first loads into memory then
%   start training
% learn_gnd_%d.mat should be stored first

echos = 1; % the number of partitioned gnds, only test 1 patch for training
nb = model_params.nb; d = 40;
Wsig = [1.5*randn(nb, d) zeros(nb, 1)];

for i = 1:echos
    % first load the ground truth
    path = sprintf('data/sift-data/sift/gnds/learn_gnd_%d.mat', i);
    fprintf('loading batch....');
    load(path, 'ind_pos', 'ind_neg', 'ind_train');
    fprintf('done\n');
    KNN_info.ind_pos = ind_pos; % index of neighbor pairs
    KNN_info.ind_neg = ind_neg; % index of non-neighbor pairs
    KNN_info.ind_train = ind_train;
    KNN_info.NNeighbors = [30, 300, 2000, 8000]; % the graded neighbors
    clear ind_pos ind_neg ind_train;
    
    Wsig = trainSIG_SIFT(data, KNN_info, model_params, train_params,...
        verbose, 1.5*Wsig);
    fprintf('===== finish batch %d\n', i);
    save sift-1m-mat Wsig i
end

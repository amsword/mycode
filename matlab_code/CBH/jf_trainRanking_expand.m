function [W, para] = jf_trainRanking_expand(data, I, para, initW)
%% data: every column is a point in a high-dimensional space. size: [D N]
% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
% W: the mapping function is W' * X;

%% parameters

[D, N] = size(data);
m = para.m;
num_center_sample = para.num_center_sample;
% if (para.is_relax_dk)
%     rd_dk = jf_relax_dk(m, [0 : m], para.epsilon, para.pd);
%     para.rd_dk = rd_dk;
% end

mu = para.mu;
max_iter = para.max_iter;
min_abs_deltaW = para.min_abs_deltaW;

% para.I = I;

% beta and gamma, which is determined by dk and m


beta = para.beta;

%% init W.
if (exist('initW', 'var'))
    W = initW;
else
    W = [randn(D, m); zeros(1, m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
end

X = [data; ones(1, N)];

c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
c2 = 0.5; % change of the step
iter = 0;
max_step_percent = 0.3;
max_iter_find_step = 50;

bad_case = 0;
Q = zeros(1, max_iter_find_step);
step = zeros(1, max_iter_find_step + 1);
po = para.po;
pd = para.pd;
num_pairs = num_center_sample * N;

max_N = min(length(para.candidate_idx), N);
num_center_sample = min(max_N, num_center_sample);
fprintf('beta = %f\n', para.beta);
fprintf('epsilon = %f\n', para.epsilon);
fprintf('num_center_sample = %d\n', num_center_sample);
fprintf('max_iter = %d\n', max_iter);

if (para.type_train == 0)
	center_index_all = zeros(max_N, 1);
end
deltaW1 = zeros(size(W));
is_deltaW1_c = false;

global gl_is_multi_thread;
if (isempty(gl_is_multi_thread))
    gl_is_multi_thread = false;
end
para
tic;

abs_delta = zeros(max_iter);

while(iter < max_iter)
%     tic
    % calculate the deltaW
    iter = iter + 1; % the sentence is better not to place at the end, because there are some 'continue' within the loop
    
    if (para.type_train == 0)
        
        center_index_all(:) = randperm(max_N);
        center_index = center_index_all([1 : num_center_sample]);

        subI = double(I(center_index, :));
        center_index = para.candidate_idx(center_index);
        % prepare the subI, since it is a cell array
        
        if (para.is_set_weight)
            weight = m + 1 - subI;
        end
        
        if (para.is_relax_dk)
            error('erroo');
            subI = rd_dk(subI + 1);
        end

		WX = W' * X;
        sigX = 1.0 ./ (1 + exp(-beta * WX));
        how =  sum(sum(sigX <= 0.1 | sigX >= 0.9)) / m / N;
            
        pre_beta = 0;
        for  search_beta = 1 : 0
            error('error');
            if (how > 0.90 && how < 0.95)
                break;
            elseif (how <= 0.9)
                if (pre_beta > beta)
                    t_beta = beta;
                    beta = (pre_beta + t_beta) / 2;
                    pre_beta = t_beta;
                else
                    pre_beta = beta;
                    beta = 2 * beta;
                end
            else
                if (pre_beta < beta)
                    t_beta = beta;
                    beta = (pre_beta + t_beta) / 2;
                    pre_beta = t_beta;
                else
                    pre_beta = beta;
                    beta = beta / 2;
                end
            end
            sigX = 1.0 ./ (1 + exp(-beta * WX));
            how =  sum(sum(sigX <= 0.1 | sigX >= 0.9)) / m / N;
        end
        beta
        how
        
        hash_code = WX > 0;
% 		sigX = sigmf(sigX, [beta 0]);
        trans_sigXcenter = sigX(:, center_index)';
        
		hash_dist = jf_c_hash_distMP(sigX(:, center_index), sigX, 32);
		
		diff_hash = hash_dist - subI;
		if (po == 1)
			diff_hash = abs(diff_hash);
		elseif (po == 2)
			diff_hash = diff_hash .* diff_hash;
        end
        
        if (para.is_set_weight)
            diff_hash = diff_hash .* weight;
        end
        
		obj = sum(sum(diff_hash));
		obj = obj / N / numel(center_index);
		wt = diag(W' * W - 1);
		p_sum = 1 / 4 * mu * (wt' * wt);
		sub_pre = obj;
		obj = obj + p_sum;
		pre = obj;
        
        hash_code = compactbit(hash_code);
        true_hash_dist = jf_hammingDist2(hash_code(:, center_index), hash_code);
        diff_hash = double(true_hash_dist) - subI;
        if (po == 1)
            diff_hash = abs(diff_hash);
        elseif (po == 2)
            diff_hash = diff_hash .* diff_hash;
        end
        
        if (para.is_set_weight)
            diff_hash = diff_hash .* weight;
        end
        
        true_pre = sum(sum(diff_hash)) / N / num_center_sample;
		%% diff
		
		% calculate the partial derivative of the objective function part
		if (po == 1)
			diff_o = sign(hash_dist - subI);
		elseif (po == 2)
			diff_o = 2 * (hash_dist - subI);
        end
        
        if (para.is_set_weight)
            diff_o = diff_o .* weight;
        end
        
		diff_sigX = beta * sigX .* (1 - sigX);
        
        if (gl_is_multi_thread && iter == 1)
            tic;
            for t = 1 : m
                P = bsxfun(@minus, trans_sigXcenter(:, t), sigX(t, :));
                if (pd == 1)
                    P = sign(P);
                elseif (pd == 2)
                    P = 2 * P;
                end
                P = diff_o .* P;
                diff_x = bsxfun(@times, X, diff_sigX(t, :));
                
                P1 = sum(P, 2);
                P11 = bsxfun(@times, diff_x(:, center_index), P1');
                P12 = sum(P11, 2);
                
                P2 = sum(P, 1);
                P21 = bsxfun(@times, diff_x, P2);
                P22 = sum(P21, 2);
                
                deltaW1(:, t) = P12 - P22;
            end
            time_matlab = toc;
            ['matlab cost: ' num2str(time_matlab)]
            tic;
            tmp_deltaW1 = jf_c_deltaW1(X(:, center_index), X, ...
                diff_sigX(:, center_index), diff_sigX, ...
                diff_o, sigX(:, center_index), sigX);
            time_c = toc;
            if (sum(abs(deltaW1(:) - tmp_deltaW1(:))) > 1)
                error('strange');
            end
            ['c cost: ' num2str(time_c)]
            if (time_c < time_matlab)
                is_deltaW1_c = true;
            else
                is_deltaW1_c = false;
            end
        else
            if (gl_is_multi_thread && is_deltaW1_c)
                deltaW1 = jf_c_deltaW1(X(:, center_index), X, ...
                    diff_sigX(:, center_index), diff_sigX, ...
                    diff_o, sigX(:, center_index), sigX);
            else
                for t = 1 : m
                    P = bsxfun(@minus, trans_sigXcenter(:, t), sigX(t, :));
                    if (pd == 1)
                        P = sign(P);
                    elseif (pd == 2)
                        P = 2 * P;
                    end
                    P = diff_o .* P;
                    diff_x = bsxfun(@times, X, diff_sigX(t, :));
                    
                    P1 = sum(P, 2);
                    P11 = bsxfun(@times, diff_x(:, center_index), P1');
                    P12 = sum(P11, 2);
                    
                    P2 = sum(P, 1);
                    P21 = bsxfun(@times, diff_x, P2);
                    P22 = sum(P21, 2);
                    
                    deltaW1(:, t) = P12 - P22;
                end
            end
        end
            

		deltaW1 = deltaW1 / size(X, 2) / num_center_sample;
		deltaW2 = mu * W * diag(diag(W' * W) - 1);
		deltaW = deltaW1 + deltaW2;
    elseif (para.type_train == 3)
        r_idx = rand(num_pairs * 2, 1) * N;
        r_idx = ceil(r_idx);
        idx_row = r_idx(1 : num_pairs);
        idx_col = r_idx(num_pairs + 1 : 2 * num_pairs);
        t_idx_one = sub2ind([N N], idx_row, idx_col);
        exp_dist = double(I(t_idx_one));
        
        [deltaW pre] = jf_deltaW_rankAbs4(...
            W, X, ...
            beta, mu, po, pd, ...
            idx_row, idx_col, exp_dist);
    end
    P = -deltaW;
	
    length_deltaW = norm(deltaW(:));
    
	squared_length_deltaW = length_deltaW * length_deltaW;
	
	abs_delta(iter) = length_deltaW;
	
    if (length_deltaW < min_abs_deltaW)
        str = ['iter: ' num2str(iter) '. deltaW = 0\n'];
        fprintf(str);
        continue;
    end
 
	step(1) = 0;
	Q(1) = pre;
%     Q(1) = true_pre;
    length_W = norm(W(:));
  
	diff_Q1 = sum(deltaW(:) .* P(:));
    length_W  / length_deltaW
	step(2) = max_step_percent * length_W  / length_deltaW;
    is_bad_dir = false;
	for i = 2 : max_iter_find_step
        W1 = W + step(i) * P;
        if (para.type_train == 0)
            %after = jf_objective_rankAbs3(...
            %    W1, X, center_index, ...
            %    beta, mu, po, pd, ...
            %    subI);

			%% hash dist: get the all involved X that should be multiplied by W
			%[hash_dist sigX] = jf_calc_hash_dist(W, X, center_index, beta, pd);
			sigX = W1' * X;
            hash_code = sigX > 0;
            sigX = 1.0 ./ (1 + exp(-beta * sigX));

            hash_dist = jf_c_hash_distMP(sigX(:, center_index), sigX, 32);
           
			diff_hash = hash_dist - subI;
			if (po == 1)
				diff_hash = abs(diff_hash);
			elseif (po == 2)
				diff_hash = diff_hash .* diff_hash;
            end
            
            if (para.is_set_weight)
                diff_hash = diff_hash .* weight;
            end
            
			obj = sum(sum(diff_hash));
			obj = obj / N / numel(center_index);
			wt = diag(W1' * W1 - 1);
			p_sum = 1 / 4 * mu * (wt' * wt);
			sub_after = obj;
			obj = obj + p_sum;
			after = obj;
            
            hash_code = compactbit(hash_code);
            true_hash_dist = jf_hammingDist2(hash_code(:, center_index), hash_code);
            diff_hash = double(true_hash_dist) - subI;
            if (po == 1)
                diff_hash = abs(diff_hash);
            elseif (po == 2)
                diff_hash = diff_hash .* diff_hash;
            end
            
            if (para.is_set_weight)
                diff_hash = diff_hash .* weight;
            end
            
            true_after = sum(sum(diff_hash)) / N / num_center_sample;
           
        elseif (para.type_train == 3)
            after = jf_objective_rankAbs4(...
                W1, X, ...
                beta, mu, po, pd, ...
                idx_row, idx_col, exp_dist);
        end
        Q(i) = after;
%         Q(i) = true_after;
        target = pre - (c1 * step(i)) * squared_length_deltaW;
        if (target <= 0)
            target = pre;
        end
        if (after <= target)
%         if (true_after <= true_pre)
            break;
        else
            next_step = jf_next_step(diff_Q1, step(1 : i), Q(1 : i));
            if (isnan(next_step))
                is_bad_dir = true;
                break;
            end
            step(i + 1) = next_step;
        end
    end
    
    if (is_bad_dir || i == max_iter_find_step)
        continue;
    end
	
    W_changed = norm(W1(:) - W(:)) / norm(W(:));
	obj_changed = (pre - after) / pre;
	str = ['iter: ' num2str(iter) ...
        '. obj: ' num2str(pre) ...
            '->' num2str(after) ...
			'. sub obj: ' num2str(sub_pre) ...
			'->' num2str(sub_after) ...
            '. W decent: ' num2str(W_changed) ...
            '. w^2-1: ' num2str(mean(diag((W1' * W1 - 1)))) ...
            '\n'];
	fprintf(str);
    
    str = ['true obj: ' num2str(true_pre) '->' num2str(true_after) '\n'];
    fprintf(str);
	
	if (obj_changed < 10^-5)
		bad_case = bad_case + 1;
		if (bad_case >= 10)
			break;
		end
	else
		bad_case = 0;
    end
    
%     if (mod(iter, 5) == 0)
        %         calculate true obj
%         hash_code = W' * X > 0;
%         hash_code = compactbit(hash_code);
%         hash_dist = jf_hammingDist2(hash_code(:, center_index), hash_code);
%         diff_hash = double(hash_dist) - subI;
%         if (po == 1)
%             diff_hash = abs(diff_hash);
%         elseif (po == 2)
%             diff_hash = diff_hash .* diff_hash;
%         end
%         true_pre = sum(sum(diff_hash)) / N / num_center_sample;
%     
%         hash_code = W1' * X > 0;
%         hash_code = compactbit(hash_code);
%         hash_dist = jf_hammingDist2(hash_code(:, center_index), hash_code);
%         diff_hash = double(hash_dist) - subI;
%         if (po == 1)
%             diff_hash = abs(diff_hash);
%         elseif (po == 2)
%             diff_hash = diff_hash .* diff_hash;
%         end
%         true_after = sum(sum(diff_hash)) / N / num_center_sample;
%         
%         str = ['true obj: ' num2str(true_pre) '->' num2str(true_after) '\n'];
%         fprintf(str);
%     end
    
    W = W1;
    
    if (para.is_force_normalize_w)
        error('erro');
        normW = sqrt(sum(W .^ 2, 1));
        W = W ./ repmat(normW, size(W, 1), 1);
    end
end
['time cost for one trainRanking', num2str(toc/60) 'minutes']

if (~isfield(para, 'abs_delta'))
	idx_abs_delta = 1;
else
	idx_abs_delta = numel(para.abs_delta) + 1;
end
para.abs_delta{idx_abs_delta} = abs_delta;

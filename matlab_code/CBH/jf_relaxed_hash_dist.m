function hash_dist = jf_relaxed_hash_dist(W, X, centerIndex, beta, p)
WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
num_center = numel(centerIndex);
N = size(X, 2);
if (p == 2)
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, centerIndex)';
    
    hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
        2 * sigX(:, centerIndex)' * sigX;
elseif (p == 1)
    m = size(W, 2);
    hash_dist = zeros(num_center, N);
    for i = 1 : m
        
    end
end
function obj = relaxed_obj_total(W, X, alpha, beta, gamma, ...
						lambda, mu, ...
						dk, dk2, dk3, ...
                        pd, I)

N = size(X, 2);

batch_size = 200;
batch_num = N / batch_size;
batch_num = ceil(batch_num);

obj_value_all = 0;

for i = 1 : batch_num
    i
    idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    idx_end = min(idx_end, size(X, 2));
    center_index = idx_start : idx_end;
    
    subI = cell(numel(I), 1);
    for k = 1 : numel(I)
        subI{k} = I{k}(center_index, :);
    end
    
    loss = relaxed_sub_loss(W, X, center_index, alpha, beta, gamma, ...
        lambda, ...
        dk, dk2, dk3, ...
        pd, subI);
    
    obj_value_all = obj_value_all + loss * (idx_end - idx_start + 1);
end                    

wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);

obj = obj_value_all / N;

obj = obj + p_sum;
                    
end
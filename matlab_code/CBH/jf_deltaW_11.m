function [deltaW obj how_beta_large] = jf_deltaW_11(W, X, alpha, beta, gamma, ...
    lambda, mu, ...
    dk, dk2, dk3, ...
    pd, I)
%% X and W: the aumented one.
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair

N = size(X, 2);

batch_size = 2000;
batch_num = N / batch_size;
batch_num = ceil(batch_num);

rho = length(dk);
subI = cell(1, rho);
all_deltaW = 0;
all_obj = 0;
for i = 1 : batch_num
    ['delta' num2str(i) '/' num2str(batch_num)]
     idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    idx_end = min(idx_end, size(X, 2));
    center_index = idx_start : idx_end;
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end

     [deltaW obj how_beta_large] = jf_deltaW_10(W, X, center_index, alpha, beta, gamma, ...
        lambda, 0, ...
        dk, dk2, dk3, ...
        pd, subI);
     all_deltaW = all_deltaW + deltaW * (idx_end - idx_start + 1);
     all_obj = all_obj + obj * (idx_end - idx_start + 1);
     
end
deltaW = all_deltaW / N;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW + deltaW2;

obj = all_obj / N;
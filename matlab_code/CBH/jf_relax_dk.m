function rd_dk = jf_relax_dk(m, dk, epsilon, pd)
alpha = -2 * log(epsilon);
v0 = quad2d(@(x, y)(func_norm(x, y, alpha, pd)), -1, 0, -1, 0);

v1 = quad2d(@(x, y)(func_norm(x, y, alpha, pd)), -1, 0, 0, 1);

rd_dk = m * v0 + dk * (v1 - v0);

function v = func_norm(x, y, alpha, pd)
v = (abs(1 ./ (1 + exp(-alpha * y)) - 1 ./ (1 + exp(-alpha .* x)))) .^ pd;

occupy = 0.99;
t = norminv(occupy / 2 + 0.5, 0, 1);
sigma = 1 / t;

v = v .* 4 .* normpdf(x, 0, sigma) .* normpdf(y, 0, sigma) / occupy ^ 2;

function v = func_avg(x, y, alpha, pd)
v = (abs(1 ./ (1 + exp(-alpha * y)) - 1 ./ (1 + exp(-alpha .* x)))) .^ pd;
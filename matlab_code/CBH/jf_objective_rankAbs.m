function y = jf_objective_rankAbs(...
                        W, X, center_index, ...
                        alpha, beta, mu, rd_dk, po, pd, ...
                        subI, is_disp)
%% dk: the threshold in the non-smooth objective version
% dk2: the threshold in the smooth objective version. when no active set is applied, it is useless
% the difference compared with _2s, is the smoothed d_(ij)
% subI: a cell array, subI{k} is a sparse matrix 

%% calculate hash dist
rho = numel(rd_dk);

%% hash dist
[hash_dist] = jf_calc_hash_dist(W, X, center_index, beta, pd);
total_length = 0;
y = 0;
for k = 1 : rho
    tmp_dist = hash_dist(subI{k}) - rd_dk(k);
    tmp_dist = abs(tmp_dist) .^ po;
    
    y = y + alpha(k) * sum(tmp_dist);
    total_length = total_length + sum(sum(subI{k}));
end
y = y / total_length;
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
y = y + p_sum;
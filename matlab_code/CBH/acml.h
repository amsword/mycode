#include <math.h>
#undef complex

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ------------------------------------------------------------------ */

/* A complex datatype for use by the C interfaces to ACML routines */
#ifndef _ACML_COMPLEX
#define _ACML_COMPLEX
typedef struct
{
  float real, imag;
} complex;
typedef struct
{
  double real, imag;
} doublecomplex;
#endif /* !defined(_ACML_COMPLEX) */

/*
   These typedefs are for routines that are arguments to other routines,
   e.g. ACML_CGEES_SELECT describes the argument "select" of routine cgees.
 */
typedef int (* ACML_CGEES_SELECT)(complex *);
typedef int (* ACML_CGEESX_SELECT)(complex *);
typedef int (* ACML_CGGES_SELCTG)(complex *, complex *);
typedef int (* ACML_CGGESX_SELCTG)(complex *, complex *);
typedef int (* ACML_DGEES_SELECT)(double *, double *);
typedef int (* ACML_DGEESX_SELECT)(double *, double *);
typedef int (* ACML_DGGES_DELCTG)(double *, double *, double *);
typedef int (* ACML_DGGESX_DELCTG)(double *, double *, double *);
typedef int (* ACML_SGEES_SELECT)(float *, float *);
typedef int (* ACML_SGEESX_SELECT)(float *, float *);
typedef int (* ACML_SGGES_SELCTG)(float *, float *, float *);
typedef int (* ACML_SGGESX_SELCTG)(float *, float *, float *);
typedef int (* ACML_ZGEES_SELECT)(doublecomplex *);
typedef int (* ACML_ZGEESX_SELECT)(doublecomplex *);
typedef int (* ACML_ZGGES_DELCTG)(doublecomplex *, doublecomplex *);
typedef int (* ACML_ZGGESX_DELCTG)(doublecomplex *, doublecomplex *);
typedef void (* ACML_DRANDINITIALIZEUSER_UINI)(int *, int *, int *, int *, int *, int *, int *);
typedef void (* ACML_DRANDINITIALIZEUSER_UGEN)(int *, int *, double *, int *);
typedef void (* ACML_SRANDINITIALIZEUSER_UINI)(int *, int *, int *, int *, int *, int *, int *);
typedef void (* ACML_SRANDINITIALIZEUSER_UGEN)(int *, int *, float *, int *);


extern void daxpy(int n, double alpha, double *x, int incx, double *y, int incy);
extern void DAXPY(int *n, double *alpha, double *x, int *incx, double *y, int *incy);

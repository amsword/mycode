function [idx_row idx_col exp_dist] = jf_sample(...
    I, num_center_sample, num_col_sample, m)

N = size(I, 1);
center_index = randperm(N);
center_index = center_index([1 : num_center_sample]);

% prepare the subI, since it is a cell array
subI = double(I(center_index, :));

idx_row = zeros((m + 1) * num_col_sample, 1);
idx_col = idx_row;
exp_dist = idx_row;
start = 1;
for k = 1 : m + 1
    for i = 1 : num_center_sample
        tmp_idx = find(subI(i, :) == k - 1);
        idx_j = randperm(length(tmp_idx));
        idx_j = tmp_idx(idx_j(1 : num_col_sample));
        idx_i = ones(1, num_col_sample) * center_index(i);
        idx_row(start : num_col_sample + start - 1) = idx_i';
        idx_col(start : num_col_sample + start - 1) = idx_j';
        exp_dist(start : num_col_sample + start -1) = k - 1;
        start = start + num_col_sample;
    end
end
function [obj, hash_dist, sigX, subX, idx_row, idx_col] = jf_objective_rankAbs4(...
                        W, X, ...
                        beta, mu, po, pd, ...
                         idx_row, idx_col, exp_dist)
%% X and W: the aumented one. 	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);
m = size(W, 2);

num = numel(idx_row);
%% hash dist: get the all involved X that should be multiplied by W
is_involved = false(N, 1);
is_involved(idx_row) = true;
is_involved(idx_col) = true;
%
subX = X(:, is_involved);
sigX = W' * subX;
sigX = sigmf(sigX, [beta 0]);

map_idx = zeros(N, 1);
map_idx(is_involved) = 1 : sum(is_involved);
idx_row = map_idx(idx_row);
idx_col = map_idx(idx_col);

hash_dist = zeros(num, 1);
for t = 1 : m
    tmp_diff = sigX(t, idx_row) - sigX(t, idx_col);
    if (pd == 1)
        tmp_diff = abs(tmp_diff);
    elseif (pd == 2)
        tmp_diff = tmp_diff .* tmp_diff;
    end
    hash_dist = hash_dist + tmp_diff';
end

tmp_diff = hash_dist - exp_dist;
if (po == 1)
    tmp_diff = abs(tmp_diff);
elseif (po == 2)
    tmp_diff = tmp_diff .* tmp_diff;
end
obj = sum(tmp_diff) / num;

wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;
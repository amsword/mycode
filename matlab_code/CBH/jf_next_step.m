function next_step = jf_next_step(diff_Q1, step, Q)
%% step(1) == 0
n = numel(step);
if (n == 2)
    next_step = 0.5 * step(2);
    return;
elseif (n == 3)
    coef = [step(2 : 3) .^ 2; step(2 : 3)]' \ (Q(2 : 3) - Q(1))';
    if (abs(coef(1)) < 10 ^ -10)
        next_step = 0.5 * step(n);
    else
        next_step = -coef(2) / 2 / coef(1);
    end
else
    coef = [step(n - 2 : n) .^ 3; step(n - 2 : n) .^ 2; step(n - 2 : n)]' \ (Q(n - 2 : n) - Q(1))';
    next_step = jf_solve_cubic(coef(1), coef(2), coef(3), Q(1));
end

next_step = real(next_step);
if (next_step < 0 || next_step > step(2) || ...
        polyval([coef; Q(1)], next_step) > polyval([coef; Q(1)], step(1)))
    next_step = step(n) * 0.5;
end
function [y obj_content] = jf_exact_objective2(W, X, center_index, alpha, lambda, dk, subI)


num_center = length(center_index);

WX = W' * X;
% bWX = double(WX > 0);
% hash_dist = jf_c_hash_distMP(bWX(:, center_index), bWX, 32);

byte_base = compactbit(WX > 0);
hash_dist = jf_hammingDist3(byte_base(:, center_index), byte_base);
hash_dist = double(hash_dist);

rho = length(dk);

inter = cell(rho, 1);
QueryI = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk(k));
    inter{k} = QueryI{k} & subI{k};
	
	QueryI3{k} = sparse(hash_dist <= dk(k));
    inter3{k} = QueryI3{k} & subI{k};
end

y = 0;
loss_negative = 0;
loss_positive = 0;

obj_content = zeros(rho, 2);

for k = 1 : rho
%     [idx_i idx_j] =find(subI{k} ~= inter3{k});
    
    [idx_i idx_j] = find(subI{k} ~= inter{k});
      
    s = numel(idx_i);
    obj_content(k, 1) = s;
    loss_negative = loss_negative + s * alpha(k);
    y = y + s * alpha(k);
    
    [idx_i idx_j] =find(QueryI3{k} ~= inter3{k});
    s = numel(idx_i);
    obj_content(k, 2) = s;
    loss_positive = loss_positive + s * alpha(k);
    y = y + s * alpha(k) * lambda;
end

y = y / num_center;
obj_content = obj_content / num_center;
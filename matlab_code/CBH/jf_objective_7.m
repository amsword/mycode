function y = jf_objective_7(W, X, centerIndex, alpha, beta, gamma, ...
				lambda,mu, ...
				dk, dk2, dk3, subI,...
                is_disp)
%% dk: the threshold in the non-smooth objective version
% dk2: the threshold in the smooth objective version. when no active set is applied, it is useless
% the difference compared with _2s, is the smoothed d_(ij)
% subI: a cell array, subI{k} is a sparse matrix 

N = size(X, 2);
num_center = length(centerIndex);

WX = W' * X;
sigX = sigmf(WX, [beta, 0]);
s_sigX = sigX .* sigX;
right = sum(s_sigX, 1);
left = right(1, centerIndex)';
hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
			2 * sigX(:, centerIndex)' * sigX;

rho = length(dk);
s = 0;

if (nargin ~= 13)
    is_disp = false;
end

inter = cell(rho, 1);
QueryI = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
	
	QueryI3{k} = sparse(hash_dist <= dk3(k));
    inter3{k} = QueryI3{k} & subI{k};
end

multi_grade_contri = zeros(rho, 2);

for k = 1 : rho
    subIk = (subI{k} ~= inter3{k});
      
    diff_dist = hash_dist(subIk(:)) - dk(k);
    diff_dist = sigmf(diff_dist, [gamma(k, 1), 0]);

    contri = alpha(k) * sum(diff_dist(:));
    
    if (is_disp)
        fprintf('k = %d, count(A) = %f; mean(dist) = %f, alpha * contri(A) = %f\n', ...
            k, full(mean(sum(subIk, 2))), mean(diff_dist), contri / num_center);
    end
    multi_grade_contri(k, 1) = multi_grade_contri(k, 1) + contri;
    s = s + contri;
end

s1 = 0;
for k = 1 : rho
    subIk = (QueryI{k} ~= inter{k});
    
    diff_dist = 1 - hash_dist(subIk(:)) + dk(k);
    diff_dist = sigmf(diff_dist, [gamma(k, 2), 0]);
    
    contri = alpha(k) * sum(diff_dist(:));

    if (is_disp)
        fprintf('k = %d, count(C) = %f; mean(dist) = %f, alpha * contri(C) = %f\n', ...
            k, full(mean(sum(subIk, 2))), full(mean(diff_dist)), contri / num_center);
    end
    multi_grade_contri(k, 2) = multi_grade_contri(k, 2) + contri;
    s1 = s1 + contri;
end

multi_grade_contri(:, 2) = multi_grade_contri(:, 2) * lambda;
multi_grade_contri = multi_grade_contri / num_center;

wt = diag(W' * W - 1);
s_sum = (s + s1 * lambda) / num_center;
p_sum = 1 / 4 * mu * (wt' * wt);
y = s_sum + p_sum;

if (is_disp)
    fprintf('original obj: %f; in which A = %f, lambda*C = %f; penelty: %f\n', s_sum, s / num_center, s1 * lambda / num_center, p_sum);
    for k = 1 : rho
        fprintf('contri(k = %d) = %f\n', k, sum(multi_grade_contri(k, :)) / y);
    end
    fprintf('contri(mu) = %f\n', p_sum / y);
end
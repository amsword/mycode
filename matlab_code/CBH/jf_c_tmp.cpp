#include "mex.h"
#include <assert.h>

void parse_matrix(const mxArray* prhs,
				  int &m, int &n, double* &ptr)
{
	assert(mxIsDouble(prhs));
	m = mxGetM(prhs);
	n = mxGetN(prhs);
	ptr = (double*)mxGetPr(prhs);
}

void parse_matrix(const mxArray* prhs,
					 int& m, int &n, int &k, double* &ptr)
{
	int dim_num = mxGetNumberOfDimensions(prhs);

	assert(dim_num == 3);

	const int* pdim = mxGetDimensions(prhs);

	m = pdim[0];
	n = pdim[1];
	k = pdim[2];
	ptr = mxGetPr(prhs);
}
void parse_matrix(const mxArray* prhs,
					 int& m, int &n,  bool* &ptr)
{
    assert(mxIsLogical(prhs));
	m = mxGetM(prhs);
	n = mxGetN(prhs);
	ptr = (bool*)mxGetPr(prhs);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    assert(nrhs == 1);
    double input;
    int m, n;
    bool* ptr;
    parse_matrix(prhs[0], m, n, ptr);
    
    double* ptr_out;
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    ptr_out = mxGetPr(plhs[0]);
    *ptr_out = ptr[0];
}
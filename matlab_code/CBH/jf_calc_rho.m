function rho = jf_calc_rho(m, percent)


if (m > 32)
% if 0
    b = norminv(percent / 100, 0, 1);
    rho = b * sqrt(m * 0.25) + m * 0.5;
    rho = ceil(rho);
    
else
    target = 2 ^ m * percent / 100;
    
    s = 0;
    for k = 0 : m
        s = s + nchoosek(m, k);
        if (s > target)
            rho = k;
            break;
        end
    end
end
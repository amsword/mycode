function W = trainSIG_SIFT(data, KNN_info, model_params, train_params, verbose, initW)
%% most part is same, except the training sampling part, detailed below
% hashing model parameter
weight = model_params.weight;
thresZ = model_params.thresZ;
thresCode = model_params.thresCode;
rho = model_params.rho;
nb = model_params.nb;
shrink_w = model_params.shrink_w;

% training parameter
nBatch = train_params.nMinibatch;
maxt = train_params.niters;	
maxb = train_params.nSamples / nBatch;
lambda = train_params.lambda;

[d, Ntraining] = size(data.Xtraining);
Xtraining = data.Xtraining;

% initialize W
if (exist('initW'))
    W = initW;
else
    W = [1.5*randn(nb, d) zeros(nb, 1)];
end

eta = .3;

rhos = [0:rho]';
Nrho = length(rhos);

% sample n points
NNeighbors = KNN_info.NNeighbors;
ind_pos = KNN_info.ind_pos; % the index of neighbors
ind_neg = KNN_info.ind_neg; % the index of non-neighbors
ind_train = KNN_info.ind_train; % the index of training points
[KNN_nn, KNN_batch] = size(ind_neg);

npos = nBatch * lambda; % number of neighbor pairs in batch
nneg = nBatch - npos; % number of non-neighbors in batch
for t = 1:maxt
    total_loss = zeros(size(rhos));
    total_rr = total_loss;
    total_rf = total_loss;
    total_ff = total_loss;
    total_fr = total_loss;
    for b = 1:maxb
        x_ind = randi([1, KNN_batch], 1, nBatch); 
        xpoints = ind_train(x_ind); % training data index
        
        y1_rank = randi([1, NNeighbors(end)], 1, npos); %y1 is positive
        y2_rank = randi([1, KNN_nn], 1, nneg); %y2 is negative
        
        y1points = ind_pos(sub2ind(size(ind_pos), y1_rank, x_ind(1:npos))); % positive pairs index
        y2points = ind_neg(sub2ind(size(ind_neg), y2_rank, x_ind(npos+1:end))); % negative pairs index
        
        S = cat(2, bsxfun(@lt, y1_rank, NNeighbors'), zeros([Nrho, nneg])); % get the ground-truth
        
        X = Xtraining(:, xpoints);
        X = [X; ones(1, nBatch)];
        Y = Xtraining(:, [y1points, y2points]);
        Y = [Y; ones(1, nBatch)];
        
        WX = W*X; WY = W*Y;
        sigX = sigmf(WX, [thresCode 0]);
        oneSigX = sigX - 1; twoSigX = 2*sigX - 1;
        sigY = sigmf(WY, [thresCode 0]);
        oneSigY = sigY - 1; twoSigY = 2*sigY - 1;
        
        S = S * 2 - 1;
        Eta = max(repmat(weight, size(S)), S);
        D = repmat(nb/2, 1, nBatch) - 1/2 * sum(twoSigX .* twoSigY, 1);
        
        % distance - rhos
        D2rhos = bsxfun(@minus, D, rhos);
        
        % evaluate the derivative to z
        z = S .* D2rhos + (1 - S) / 2;
        sigZ = sigmf(z, [thresZ, 0]);
        
        % evaluate the derivative to W
        temp = S .* Eta * thresCode * 2;
        prod = sigZ .* (1 - sigZ) .* temp;
        prod = sum(prod, 1);
        prodX = repmat(prod, nb, 1) .* sigX .* oneSigX .* twoSigY;
        prodY = repmat(prod, nb, 1) .* sigY .* oneSigY .* twoSigX;
        dW = prodX * X' + prodY * Y';
        
        % stochastic gradient update
        Winc = eta * (dW / (nBatch) + shrink_w * W);
        W = W - Winc;
        
        total_loss = total_loss + sum(sigZ > 0.5, 2);
        
        if (verbose)
            Xb = 2 * ((WX) > 0) - 1;
            Yb = 2 * ((WY) > 0) - 1;
            Db = repmat(1/2 * nb, [1, size(nBatch)]) - 1/2 * sum(Xb .* Yb);
            Predict = (repmat(Db, size(rhos)) <= repmat(rhos, size(Db)));
            TruePos = (S + 1) / 2;
            rrNum = sum(Predict .* TruePos, 2);
            rfNum = sum(TruePos, 2) - rrNum;
            frNum = sum(Predict, 2) - rrNum;
            ffNum = nBatch - sum(TruePos, 2) - frNum;
    
            total_rr = total_rr + rrNum;
            total_rf = total_rf + rfNum;
            total_ff = total_ff + ffNum;
            total_fr = total_fr + frNum;
        end
    end
    if (mod(t, 20) == 0)
        codes = (W * [Xtraining ; ones(1, size(Xtraining, 2))]) > 0;
        bitsRatio = sum(codes, 2) / size(Xtraining, 2);
        uselessbits = sum(bitsRatio < 0.03) + sum(bitsRatio > (1 - 0.03));
        [Nbuckets, bucketACap] = getBuckets(codes, nb);
        fprintf('echo %d, Nbuckets %d, uselessbits %d, norm(W) = %.3f, loss = %f/%d\n',...
            t, Nbuckets, uselessbits, norm(W), mean(total_loss), train_params.nSamples);
        for i = 1:size(rhos, 1)
            fprintf('Rho = %d, Accuracy (%.3f), RR %d/%d (% .3f), FF %d/%d (%.3f)\n', ...
            rhos(i), double(total_ff(i) + total_rr(i)) / (total_ff(i) + total_fr(i) + total_rf(i) + total_rr(i)),...
            total_rr(i), total_rr(i) + total_rf(i), double(total_rr(i))/(total_rr(i)+total_rf(i)), total_ff(i),...
            total_ff(i)+total_fr(i), double(total_ff(i)) / (total_ff(i) + total_fr(i)));
        end
    end
end
function [W, out_para] = train_cbh_inc_variable(Xtraining, QueryXtraining, ...
    I, cbh_para, initW)
%% cbh_para.mu will be useless. Please delete it later
% the routines estimate the mu and increase it dynamically

step_mu = 1; % the change of mu;
step_gamma = 1.5;
step_beta = 2;

max_search_mu_times = cbh_para.max_search_mu_times;

para = cbh_para;

str = ['start cbh training: \nmu = ' num2str(para.mu) '\n'];
str = [str 'step_mu = ' num2str(step_mu) '\n'];
str = [str 'max_search_mu_times = ' num2str(max_search_mu_times) '\n'];
str = [str 'max_iter = ' num2str(cbh_para.max_iter) '\n'];

subI = I;

fprintf(str);
if (exist('initW', 'var'))
    if (para.type_train == 0)
        [W out_para] = jf_trainRanking_expand(Xtraining, subI, para, initW); % the input: the data and the groundtruth, which is obtained by jf_prepare_training_data
    elseif (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, subI, para, initW);
    end
else
    if (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, subI, para);
    end
end

para.is_smart_set_lambda = false;
preW = W;

for i_mu = 1 : max_search_mu_times
    
    para = out_para;
    para.mu = para.mu * step_mu;
    para.beta = para.beta * step_beta;
    para.gamma = para.gamma * step_gamma;
    
    if (para.type_train == 0)
        [W, out_para] = jf_trainRanking_expand(Xtraining, subI, para, preW);
    elseif (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, subI, para, preW);
    end
    changed = sum(abs(W(:) - preW(:))) / sum(abs(W(:)));
    how_normalize = mean(abs(diag(W' * W - 1)));
    
    fprintf('one end, mu=%f, change_W=%f, w2-1 = %f\n', ...
        para.mu, changed, how_normalize);
    
    preW = W;
    if (how_normalize < 0.01 && changed < 0.01)
        break;
    end
end
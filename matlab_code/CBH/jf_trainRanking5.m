function [W, para] = jf_trainRanking5(X, QueryX, I, para, initW)
%% X: every column is a point in a high-dimensional space. size: [D N]
%% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
%% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
%% W: the mapping function is W' * X;

%% parameters
[D, N] = size(X);
m = para.m;
num_center_sample = para.num_center_sample;
% rd_dk = jf_relax_dk(m, para.dk, para.epsilon);
% para.rd_dk = rd_dk;

mu = para.mu;
max_iter = para.max_iter;
min_abs_deltaW = para.min_abs_deltaW;

% para.I = I;

% beta and gamma, which is determined by dk and m
beta = -2 * log(para.epsilon) / sqrt(2);
para.beta = beta;

%% init W.
if (exist('initW', 'var'))
    W = initW;
else
    W = [randn(D, m); zeros(1, m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
end

X = [X; ones(1, N)];
N_query = size(QueryX, 2);
QueryX = [QueryX; ones(1, N_query)];

c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
c2 = 0.5; % change of the step
iter = 0;
max_step_percent = 0.1;
max_iter_find_step = 50;

bad_case = 0;
Q = zeros(1, max_iter_find_step);
step = zeros(1, max_iter_find_step + 1);
po = para.po;
pd = para.pd;
num_col_sample = min(sum(I(1, :) == 0),...
    sum(I(1, :) == m));

while(iter < max_iter)
%     tic
    % calculate the deltaW
    iter = iter + 1; % the sentence is better not to place at the end, because there are some 'continue' within the loop
    
    [idx_row idx_col exp_dist] = jf_sample(...
        I, num_center_sample, num_col_sample, m);
    
    [deltaW pre] = jf_deltaW_rankAbs5(...
        W, X, QueryX, ...
        beta, mu, po, pd, ...
        idx_row, idx_col, exp_dist);
    P = -deltaW;
	
    length_deltaW = norm(deltaW(:));
    
	squared_length_deltaW = length_deltaW * length_deltaW;
	
    if (length_deltaW < min_abs_deltaW)
        str = ['iter: ' num2str(iter) '. deltaW = 0\n'];
        fprintf(str);
        continue;
    end
 
	step(1) = 0;
	Q(1) = pre;
    length_W = norm(W(:));
  
	diff_Q1 = sum(deltaW(:) .* P(:));
	step(2) = max_step_percent * length_W  / length_deltaW;
    is_bad_dir = false;
	for i = 2 : max_iter_find_step
	    W1 = W + step(i) * P;
        after = jf_objective_rankAbs5(...
                        W1, X, ...
                        beta, mu, po, pd, ...
                         idx_row, idx_col, exp_dist);
        Q(i) = after;
        target = pre - (c1 * step(i)) * squared_length_deltaW;
        if (target <= 0)
            target = pre;
        end
        if (after <= target)
            break;
        else
            next_step = jf_next_step(diff_Q1, step(1 : i), Q(1 : i));
            if (isnan(next_step))
                is_bad_dir = true;
                break;
            end
            step(i + 1) = next_step;
        end
    end
    
    if (is_bad_dir || i == max_iter_find_step)
        continue;
    end
	
    W_changed = norm(W1(:) - W(:)) / norm(W(:));
	obj_changed = (pre - after) / pre;
	str = ['iter: ' num2str(iter) ...
        '. obj decent percent: ' num2str(100 * obj_changed) ...
            '. now_objective_value: ' num2str(after) ...
            '. W decent: ' num2str(W_changed) ...
            '. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
            '\n'];
	fprintf(str);
	
	if (obj_changed < 10^-5)
		bad_case = bad_case + 1;
		if (bad_case >= 10)
			break;
		end
	else
		bad_case = 0;
    end
    W = W1;
end
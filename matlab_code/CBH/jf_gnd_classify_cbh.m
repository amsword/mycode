function classifyI = jf_gnd_classify_cbh(regressionI, rhos)
classifyI = cell(numel(rhos), 1);

for i = 1 : numel(rhos)
    classifyI{i} = sparse(regressionI <= rhos(i));
end
% save('ClassifyStrainingtraining_16.mat', 'I', 'rhos', '-v7.3');

function [obj, hash_dist, sig_X, sig_queryX, X, QueryX, idx_row, idx_col] = ... 
						jf_objective_rankAbs5(...
							W, X, QueryX, ...
							beta, mu, po, pd, ...
							idx_row, idx_col, exp_dist)
%% X and W: the aumented one. 	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);
m = size(W, 2);

[X idx_row] = jf_filter_sampled(X, idx_row);
[QueryX idx_col] = jf_filter_sampled(QueryX, idx_col);

sig_X = sigmf(W' * X, [beta 0]);
sig_queryX = sigmf(W' * QueryX, [beta 0]);

num = numel(idx_row);

hash_dist = zeros(num, 1);

for t = 1 : m
	tmp_diff = sig_queryX(t, idx_row) - sig_X(t, idx_col);
	if (pd == 1)
		tmp_diff = abs(tmp_diff);
	elseif (pd == 2)
		tmp_diff = tmp_diff .* tmp_diff;
	end
	hash_dist = hash_dist + tmp_diff';
end

tmp_diff = hash_dist - exp_dist;
if (po == 1)
    tmp_diff = abs(tmp_diff);
elseif (po == 2)
    tmp_diff = tmp_diff .* tmp_diff;
end
obj = sum(tmp_diff) / num;

wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;
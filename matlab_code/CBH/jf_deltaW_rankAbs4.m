function [deltaW, obj] = jf_deltaW_rankAbs4(...
                                    W, X, ...
                                    beta, mu, po, pd, ...
                                    idx_row, idx_col, exp_dist)
%% X and W: the aumented one. 
%% sample pairs

% hash_dist_pair
m = size(W, 2);

%% hash dist: get the all involved X that should be multiplied by W
[obj hash_dist sigX2 subX idx_row2, idx_col2] = ...
    jf_objective_rankAbs4(...
                        W, X, ...
                        beta, mu, po, pd, ...
                        idx_row, idx_col, exp_dist);
%% diff
deltaW1 = zeros(size(W));

% calculate the partial derivative of the objective function part
if (po == 1)
    diff_o = sign(hash_dist - exp_dist);
elseif (po == 2)
    diff_o = 2 * (hash_dist - exp_dist);
end

diff_sigX2 = beta * sigX2 .* (1 - sigX2);

for t = 1 : m
    tmp_dij = sigX2(t, idx_row2) - sigX2(t, idx_col2);
    if (pd == 1)
        diff_d = sign(tmp_dij);
    elseif (pd == 2)
        diff_d = 2 * tmp_dij;
    end
    P = diff_o .* diff_d';
    diff_x = bsxfun(@times, subX, diff_sigX2(t, :));
   
    P = bsxfun(@times, diff_x(:, idx_row2) - diff_x(:, idx_col2), P');
    deltaW1(:, t) = sum(P, 2);
end

deltaW1 = deltaW1 / numel(idx_row);
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW1 + deltaW2;
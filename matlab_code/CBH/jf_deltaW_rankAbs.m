function [deltaW] = jf_deltaW_rankAbs(...
                                    W, X, center_index, ...
                                    alpha, beta, mu, rd_dk, po, pd, ...
                                    subI)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
rho = numel(rd_dk);
m = size(W, 2);

%% hash dist
[hash_dist sigX] = jf_calc_hash_dist(W, X, center_index, beta, pd);
deltaW1 = zeros(size(W));
total_length = 0;
for k = 1 : rho
    tmp_dist = hash_dist(subI{k}) - rd_dk(k);
    diff_o = po * (abs(tmp_dist) .^ (po - 1)) .* sign(tmp_dist);
    
    [idx_i idx_j] = find(subI{k});
    total_length = total_length + numel(idx_i);
    for t = 1 : m
        tmp_dist = sigX(t, center_index(idx_i)) - sigX(t, idx_j);
        diff_d = pd * abs(tmp_dist) .^ (pd - 1) .* sign(tmp_dist);
        
        coef = sigX(t, center_index(idx_i)) .* (1 - sigX(t, center_index(idx_i))) * beta;
        left = bsxfun(@times, X(:, center_index(idx_i)), coef);
        
        coef = sigX(t, idx_j) .* (1 - sigX(t, idx_j)) * beta;
        right = bsxfun(@times, X(:, idx_j), coef);
        
        tmp_r = bsxfun(@times, left - right, diff_o');
        tmp_r = bsxfun(@times, tmp_r, diff_d);
        deltaW1(:, t) = deltaW1(:, t) + alpha(k) * sum(tmp_r, 2);
    end
end
deltaW1 = deltaW1 / total_length;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW1 + deltaW2;
#include "mex.h"
#include <assert.h>
#include <math.h>
#include <stdio.h>

void parse_matrix(const mxArray* prhs,
				  int &m, int &n, double* &ptr)
{
	assert(mxIsDouble(prhs));
	m = mxGetM(prhs);
	n = mxGetN(prhs);
	ptr = (double*)mxGetPr(prhs);
}

void parse_matrix(const mxArray* prhs,
					 int& m, int &n, int &k, bool* &ptr)
{
	int dim_num = mxGetNumberOfDimensions(prhs);

	assert(dim_num == 3);

	const int* pdim = mxGetDimensions(prhs);

	m = pdim[0];
	n = pdim[1];
	k = pdim[2];
	ptr = (bool*)mxGetPr(prhs);
}

void parse_input(int nrhs, const mxArray *prhs[],
				 double* &pW, double* &pX, 
				 double& beta, double& gamma,
				 double& lambda, double& mu,
				 double* &pdk, int &rho,
				 bool* &pI, bool* &pJ,
				 int& num_points, int& high_dim,
				 int& low_dim)
{
	assert(nrhs == 9);

	int m;
	int n;
	int k;
	double* ptr;

	parse_matrix(prhs[0], high_dim, low_dim, pW);
	
	parse_matrix(prhs[1], m, num_points, pX);
	assert(m == high_dim);
	
	parse_matrix(prhs[2], m, n, ptr);
	assert(m == 1 && n == 1);
	beta = ptr[0];

	parse_matrix(prhs[3], m, n, ptr);
	assert(m == 1 && n == 1);
	gamma = ptr[0];

	parse_matrix(prhs[4], m, n, ptr);
	assert(m == 1 && n == 1);
	lambda = ptr[0];

	parse_matrix(prhs[5], m, n, ptr);
	assert(m == 1 && n == 1);
	mu = ptr[0];

	parse_matrix(prhs[6], m, n, pdk);
	assert(m == 1 || n == 1);
	rho = m * n;

	parse_matrix(prhs[7], m, n, k, pI);
	assert(k == rho);
	assert(m == num_points && n == num_points);

	parse_matrix(prhs[8], m, n, k, pJ);
	assert(k == rho);
	assert(m == num_points && n == num_points);
}

double sigmoid(double x, double alpha)
{
	return 1.0 / (1 + exp(-alpha * x));
}
double product(const double* x1, const double* x2, int dim)
{
	double s = 0;
	for (int i = 0; i < dim; i++)
	{
		s += x1[i] * x2[i];
	}
	return s;
}
double calc_dij(const double* xi, const double* xj, const double* pW, 
				double beta, int high_dim, int low_dim)
{
	double s = 0;
	for (int t = 0; t < low_dim; t++)
	{
		double i_project = product(xi, pW, high_dim);
		double j_project = product(xj, pW, high_dim);

		s += (2 * sigmoid(i_project, beta) - 1) * (2 * sigmoid(j_project, beta) - 1);

		pW += high_dim;
	}
	s = (low_dim - s) / 2.0;

	return s;
}

double calc_objective(const double* pW, const double* pX, 
				 double beta, double gamma,
				 double lambda, double mu,
				 const double* pdk, int rho,
				 const bool* pI, const bool* pJ,
				 int num_points, int high_dim,
				 int low_dim)
{
	const double* xi = pX;
	const double* xj = pX;

	const bool* i_indicator = pI;
	const bool* j_indicator = pI;
	const bool* k_indicator = pI;
	bool is_within;
	int num_plane = num_points * num_points;

	double s = 0;
	for (int i = 0; i < num_points; i++)
	{
		printf("%d\t%d\n", i, num_points);
		for (int j = 0; j < num_points; j++)
		{
			double dij = calc_dij(xi, xj, pW, beta, high_dim, low_dim);
			for (int k = 0; k < rho; k++)
			{
				is_within = *k_indicator;
				if (is_within)
				{
					s += sigmoid(dij - pdk[k], gamma);
				}
				else
				{
					s += lambda * sigmoid(pdk[k] + 1 - dij, gamma);
				}
				k_indicator += num_plane;
			}
			xj += high_dim;
			j_indicator += num_points;
			k_indicator = j_indicator;
		}
		xi += high_dim;
		xj = pX;
		i_indicator++;
		j_indicator = i_indicator;
		k_indicator = i_indicator;
	}
	return s;
}

double calc_quadratic_penalty(double mu, const double* pW, int low_dim, int high_dim)
{
	double s = 0;

	double pro;
	double pro_one;
	for (int t = 0; t < low_dim; t++)
	{
		pro = product(pW, pW, high_dim);
		pro_one = pro - 1;
		s += pro_one * pro_one;
	}

	return s * mu / 4.0;
}

///[obj_value] = jf_c_objective(W, X, beta, gamma, lambda, mu, dk, I, J);
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	double* pW;
	double* pX;
	double beta;
	double gamma;
	double lambda;
	double mu;
	double* pdk;
	int rho;
	bool* pI;
	bool* pJ;
	int num_points;
	int high_dim;
	int low_dim;

	parse_input(nrhs, prhs,
				pW, pX, 
				beta, gamma,
				lambda, mu,
				pdk, rho,
				pI, pJ,
				num_points, high_dim,
				low_dim);

	double s = calc_objective(pW, pX, 
				 beta, gamma,
				 lambda, mu,
				 pdk, rho,
				 pI, pJ,
				 num_points, high_dim,
				 low_dim);
	
	s += calc_quadratic_penalty(mu, pW, low_dim, high_dim);

	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double* pout = mxGetPr(plhs[0]);
	*pout = s;
}
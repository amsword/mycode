function [alpha lambda] = jf_find_alpha_lambda(a, c)

alpha = ones(size(a));
for i = 1 : 10
    lambda = sum(alpha .* a) / sum(alpha .* c);
    alpha = 1 ./ (a + lambda * c);
end
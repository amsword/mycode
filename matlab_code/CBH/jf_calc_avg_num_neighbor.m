function y = jf_calc_avg_num_neighbor(dk, m, N)
rho = length(dk);

y = zeros(rho, 1);

cumulate = zeros(dk(rho) + 1, 1);
cumulate(1) = 1;

for i = 2 : dk(rho) + 1
    cumulate(i) = cumulate(i - 1) + gamma(m + 1) / gamma(i - 1 + 1) / gamma(m - i + 1 + 1);
end

y = cumulate(dk + 1) * N / 2 ^ m;
y = round(y);
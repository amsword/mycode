function [obj hash_dist sigX sigQueryX] = jf_objective_rankAbs6(...
                        W, X, QueryX, ...
                        beta, mu, po, pd, ...
                        subI)
%% X and W: the aumented one. 	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);

%% hash dist: get the all involved X that should be multiplied by W
% [hash_dist sigX] = jf_calc_hash_dist(W, X, center_index, beta, pd);
[hash_dist sigQueryX sigX] = jf_calc_hash_dist2(W, X, QueryX, beta, pd);
obj = hash_dist - subI;
if (po == 1)
    obj = abs(obj);
elseif (po == 2)
    obj = obj .* obj;
end
obj = sum(sum(obj));
obj = obj / N / size(QueryX, 2);
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;
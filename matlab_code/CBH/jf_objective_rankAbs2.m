function y = jf_objective_rankAbs2(...
                        W, X, center_index, ...
                        alpha, beta, mu, rd_dk, po, pd, ...
                        subI, is_disp)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);
rho = numel(rd_dk);

%% hash dist: get the all involved X that should be multiplied by W
num_non_zeros = 0;

idx_col_in_X = cell(rho, 1);
idx_row_in_center = cell(rho, 1);
is_involved_in_X = false(N, 1);
for k = 1 : rho
    [idx_row_in_center{k} idx_col_in_X{k}] = find(subI{k});
    num_non_zeros = num_non_zeros + numel(idx_col_in_X{k});
    is_involved_in_X(idx_col_in_X{k}) = true;
end
is_involved_in_X(center_index) = true;
tmp_v = find(is_involved_in_X);
map_idx = zeros(N, 1);
map_idx(tmp_v) = 1 : length(tmp_v);
sigX = W' * X(:, is_involved_in_X);
sigX = sigmf(sigX, [beta, 0]);

num_non_zeros = 0;
y = 0;
for k = 1 : rho
    % calculate dij
    idx_row_in_X = center_index(idx_row_in_center{k}); % index in X
    idx_row_in_SigX = map_idx(idx_row_in_X); % index in sigX, only involved X is calculated
    idx_col_in_SigX = map_idx(idx_col_in_X{k});
    
    tmp_dij = sigX(:, idx_row_in_SigX) - sigX(:, idx_col_in_SigX);
    if (pd == 1)
        dij = abs(tmp_dij);
    elseif (pd == 2)
        dij = tmp_dij .* tmp_dij;
    end
    dij = sum(dij, 1)';
 
    if (po == 1)
        obj = abs(dij - rd_dk(k));
    elseif (po == 2)
        obj = (dij - rd_dk(k)) .^ 2;
    end
    num_non_zeros = num_non_zeros + length(obj);
    y = y + alpha(k) * sum(obj);
end
y = y / num_non_zeros;
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
y = y + p_sum;
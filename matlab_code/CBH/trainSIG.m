function W = trainSIG(data, model_params, train_params, verbose, initW)
%% main function to train CBH

%%
% hashing model parameter
weight = model_params.weight;
thresZ = model_params.thresZ;
thresCode = model_params.thresCode;
rho = model_params.rho;
nb = model_params.nb;
shrink_w = model_params.shrink_w;

% training parameter
npoints = train_params.nMinibatch; % batch-size
maxt = train_params.niters;	
maxb = train_params.nSamples / (npoints); 
lambda = train_params.lambda;
nBatch = npoints;

[d, Ntraining] = size(data.Xtraining);
Xtraining = data.Xtraining;
NtrainingSqr = Ntraining^2;

% initialize W
if (exist('initW'))
    W = initW;
else
    W = [1.5*randn(nb, d) zeros(nb, 1)];
end

eta = .4; % learning rate in online learning

rhos = model_params.rhos;
Nrho = length(rhos);

% index of all the neighbors in the largest graded neighbor set
indPos = find(data.Strainings(end, :, :) == 1);
nPos = numel(indPos);

for t = 1:maxt
    total_loss = zeros(size(rhos));
    total_rr = total_loss;
    total_rf = total_loss;
    total_ff = total_loss;
    total_fr = total_loss;
    for b = 1:maxb
        % ensure positive pairs at least lambda, same in MLH
        npoints2 = min(round(npoints * max(lambda - (nPos / NtrainingSqr), 0)), npoints);
        npoints1 = npoints - npoints2;
        
        % get the index of the training pairs
        points(1:npoints1) = randi([1, NtrainingSqr], npoints1, 1);
        points(npoints1+1:npoints) = indPos(randi([1, nPos], npoints2, 1));
        
        % transform to index training data
        [x_ind, y_ind] = ind2sub([Ntraining Ntraining], points);
        
        % let S in {-1, 1}
        S = data.Strainings(:, points) * 2 - 1;
        
        X = Xtraining(:, x_ind(:));
        X = [X; ones(1, nBatch)];
        Y = Xtraining(:, y_ind(:));
        Y = [Y; ones(1, nBatch)];
        
        WX = W*X; WY = W*Y;
        % sigmf is the sigmoid function
        sigX = sigmf(WX, [thresCode 0]);
        oneSigX = sigX - 1; twoSigX = 2*sigX - 1;
        sigY = sigmf(WY, [thresCode 0]);
        oneSigY = sigY - 1; twoSigY = 2*sigY - 1;
        
        Eta = max(repmat(weight, size(S)), S); % the weight matrix of all the training pairs
        D = repmat(nb/2, 1, npoints) - 1/2 * sum(twoSigX .* twoSigY, 1);
        
        % distance - rhos
        D2rhos = bsxfun(@minus, D, rhos);
        
        % evaluate the derivative to z
        z = S .* D2rhos + (1 - S) / 2;
        sigZ = sigmf(z, [thresZ, 0]);
        
        % evaluate the derivative to W
        temp = S .* Eta * thresCode * 2;
        prod = sigZ .* (1 - sigZ) .* temp;
        prod = sum(prod, 1);
        prodX = repmat(prod, nb, 1) .* sigX .* oneSigX .* twoSigY;
        prodY = repmat(prod, nb, 1) .* sigY .* oneSigY .* twoSigX;
        dW = prodX * X' + prodY * Y';
        
        % stochastic gradient update
        Winc = eta * (dW / (npoints) + shrink_w * W);
%         ['deltaW percent: ' num2str(sum(abs(Winc(:))) / sum(abs(W(:))))]
        W = W - Winc;
        
        % normalize W
%         normconW = sqrt(sum(W(:, 1:end-1).^2, 1));
%         W(:, 1:end-1) = bsxfun(@rdivide, W(:, 1:end-1), normconW);
        
        total_loss = total_loss + sum(sigZ > 0.5, 2);
        
        % 
        if (verbose)
            Xb = 2 * ((WX) > 0) - 1; % get the {-1, 1} code
            Yb = 2 * ((WY) > 0) - 1;
            Db = repmat(1/2 * nb, [1, size(nBatch)]) - 1/2 * sum(Xb .* Yb);
            Predict = (repmat(Db, size(rhos)) <= repmat(rhos, size(Db)));
            TruePos = (S + 1) / 2;
            rrNum = sum(Predict .* TruePos, 2); % neighbors correctly classified
            rfNum = sum(TruePos, 2) - rrNum; % neighbors not correctly classified
            frNum = sum(Predict, 2) - rrNum; % non-neighbors not correctly classified
            ffNum = nBatch - sum(TruePos, 2) - frNum; % non-neighbors correctly classified
    
            total_rr = total_rr + rrNum;
            total_rf = total_rf + rfNum;
            total_ff = total_ff + ffNum;
            total_fr = total_fr + frNum;
        end
    end
    if (mod(t, 20) == 0)
        codes = (W * [Xtraining ; ones(1, size(Xtraining, 2))]) > 0;
        bitsRatio = sum(codes, 2) / size(Xtraining, 2);
        uselessbits = sum(bitsRatio < 0.03) + sum(bitsRatio > (1 - 0.03));
        [Nbuckets, bucketACap] = getBuckets(codes, nb);
        fprintf('echo %d, Nbuckets %d, uselessbits %d, norm(W) = %.3f, loss = %f/%d\n',...
            t, Nbuckets, uselessbits, norm(W), mean(total_loss), train_params.nSamples);
        for i = 1:size(rhos, 1)
            fprintf('Rho = %d, Accuracy (%.3f), RR %d/%d (% .3f), FF %d/%d (%.3f)\n', ...
            rhos(i), double(total_ff(i) + total_rr(i)) / (total_ff(i) + total_fr(i) + total_rf(i) + total_rr(i)),...
            total_rr(i), total_rr(i) + total_rf(i), double(total_rr(i))/(total_rr(i)+total_rf(i)), total_ff(i),...
            total_ff(i)+total_fr(i), double(total_ff(i)) / (total_ff(i) + total_fr(i)));
        end
    end
end
function [W, out_para] = jf_train_cbh_ranking(Xtraining, QueryXtraining, ...
    I, cbh_para, initW)
%% cbh_para.mu will be useless. Please delete it later
% the routines estimate the mu and increase it dynamically

mu_step = 2; % the change of mu;
max_search_mu_times = cbh_para.max_search_mu_times;

para = cbh_para;

ref_mu = para.mu;

str = ['start cbh training: \nmu = ' num2str(para.mu) '\n'];
str = [str 'mu_step = ' num2str(mu_step) '\n'];
str = [str 'max_search_mu_times = ' num2str(max_search_mu_times) '\n'];
str = [str 'max_iter = ' num2str(cbh_para.max_iter) '\n'];
% str = [str 'is_relax_dk = ' num2str(cbh_para.is_relax_dk) '\n'];
% for i = 1 : length(cbh_para.num_neighbors)
%     str = [str 'num_neighbors[' num2str(i) '] = ' num2str(cbh_para.num_neighbors(i)) '\n'];
% end
if (isempty(I))
    ['loading gnd']
    I = jf_load_gnd(para.gnd_file_name);
    ['complete loading gnd']
end

N = size(I, 2);

fprintf(str);
if (exist('initW', 'var'))
    if (para.type_train == 0)
        [W out_para] = jf_trainRanking_expand(Xtraining, I, para, initW); % the input: the data and the groundtruth, which is obtained by jf_prepare_training_data
%     elseif (para.type_train == 1)
%         error('wrong');
%         [W out_para] = jf_trainRanking4(Xtraining, I, para, initW); % the input: the data and the groundtruth, which is obtained by jf_prepare_training_data
%     elseif (para.type_train == 2)
%         error('wrong');
%         [W out_para] = jf_trainRanking6(Xtraining, QueryXtraining, ...
%             I, para, initW);
    elseif (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, I, para, initW);
    elseif para.type_train == 11
        [W, out_para] = jf_trainSigBatch(Xtraining, I, para, initW);
    end
else
    if (para.type_train == 0)
        [W out_para] = jf_trainRanking_expand(Xtraining, I, para);
%     elseif (para.type_train == 1)
%         error('wrong');
%         [W out_para] = jf_trainRanking4(Xtraining, I, para);
%     elseif (para.type_train == 2)
%         error('wrong');
%         [W out_para] = jf_trainRanking6(Xtraining, QueryXtraining, ...
%             I, para);
    elseif (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, I, para);
    elseif para.type_train == 11
        [W, out_para] = jf_trainSigBatch(Xtraining, I, para);
    end
end
    how_good_mu = mean(abs(diag(W' * W - 1)));
fprintf('one end, mu = %f, mean(|diag(W * W - 1)|) = %f, time cost: %f\n', ...
    para.mu, how_good_mu);

para.is_smart_set_lambda = false;
preW = W;

for i_mu = 1 : max_search_mu_times
    
    ref_mu = ref_mu * mu_step;
    para.mu = ref_mu;

    if (para.type_train == 0)
        [W, out_para] = jf_trainRanking_expand(Xtraining, I, para, preW);
%     elseif (para.type_train == 1)
%         error('wrong');
%         [W, out_para] = jf_trainRanking4(Xtraining, I, para, preW);
%     elseif (para.type_train == 2)
%         error('wrong');
%          [W out_para] = jf_trainRanking6(Xtraining, QueryXtraining, ...
%             I, para, preW);
    elseif (para.type_train == 10)
        [W, out_para] = jf_trainSig8(Xtraining, I, para, preW);
    elseif para.type_train == 11
        [W, out_para] = jf_trainSigBatch(Xtraining, I, para, preW);
    end
    changed = sum(abs(W(:) - preW(:))) / sum(abs(W(:)));
    how_normalize = mean(abs(diag(W' * W - 1)));
    fprintf('one end, mu=%f, change_W=%f, w2-1 = %f\n', ...
        para.mu, changed, how_normalize);
    preW = W;
    if (how_normalize < 0.01 && changed < 0.01)
        break;
    end
end
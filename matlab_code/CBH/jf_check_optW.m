function y = jf_check_optW(Xtraining, input)

N = size(Xtraining, 2);
Xtraining = [Xtraining; ones(1, N)];
y = zeros(length(input), length(input));
for i = 1 : length(input)
%     for i = 6 : 6
    W = input{i}.W;
    s_W = sum(W .^ 2, 1);
    s_W = sqrt(s_W);
    W = bsxfun(@rdivide, W, s_W);
	min_value = jf_objective_7(W, Xtraining, [1 : N],...
        input{i}.para.alpha, input{i}.para.beta, ...
        input{i}.para.gamma, input{i}.para.lambda, ...
        0, input{i}.para.rd_dk, input{i}.para.rd_dk2, input{i}.para.rd_dk3, input{i}.para.I, true);
    y(i, i) = min_value;
    is_good = true;
	for j = 1 : length(input)
%         for j = 4 : 4
        if (j == i)
            continue;
        end
        W = input{j}.W;
        s_W = sum(W .^ 2, 1);
        s_W = sqrt(s_W);
        W = bsxfun(@rdivide, W, s_W);
        value = jf_objective_7(W, Xtraining, [1 : N],...
            input{i}.para.alpha, input{i}.para.beta, ...
            input{i}.para.gamma, input{i}.para.lambda, ...
            0, input{i}.para.rd_dk, input{i}.para.rd_dk2, input{i}.para.rd_dk3, input{i}.para.I, true);
        y(i, j) = value;
        if (value < min_value)
            is_good = false;
            ['not good: i = ' num2str(i) '. value = ' num2str(min_value) ...
                '. j = ' num2str(j) '. value = ' num2str(value)]
        end
    end
    if (is_good)
        ['good: i = ' num2str(i)]
    end
end
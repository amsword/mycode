function [obj obj_content] = cbh_true_obj(W, X, dk, alpha, lambda, I)

N = size(X, 2);

batch_size = 2000;
batch_num = N / batch_size;
batch_num = ceil(batch_num);

rho = numel(I);

obj_content_all = zeros(rho, 2);
obj_value_all = 0;

for i = 1 : batch_num
    ['true: ' num2str(i) '/' num2str(batch_num)]
    idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    idx_end = min(idx_end, size(X, 2));
    center_index = idx_start : idx_end;
    
    subI = cell(numel(I), 1);
    for k = 1 : numel(I)
        subI{k} = I{k}(center_index, :);
    end
    
    [y obj_content] = jf_exact_objective2(W, X, center_index, alpha, lambda, dk, subI);
    obj_content_all = obj_content_all + obj_content * numel(center_index);
    obj_value_all = obj_value_all + y * (idx_end - idx_start + 1);
end

obj = obj_value_all / N;
obj_content = obj_content_all / N;
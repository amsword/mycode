function [sub_X sub_idx] = jf_filter_sampled(X, idx)
N = size(X, 2);
is_involved_queryX = false(1, N);

is_involved_queryX(idx) = true;
sub_X = X(:, is_involved_queryX);

tmp_map_idx = zeros(1, N);
tmp_map_idx(is_involved_queryX) = 1 : sum(is_involved_queryX);
sub_idx = tmp_map_idx(idx);
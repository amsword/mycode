function [hash_dist sigX] = jf_calc_hash_dist(W, X, center_index, beta, pd)
num_center = numel(center_index);
N = size(X, 2);
m = size(W, 2);

sigX = W' * X;
sigX = sigmf(sigX, [beta 0]);

hash_dist = zeros(num_center, N);
for t = 1 : m
    tmp_dist = bsxfun(@minus, sigX(t, center_index)', sigX(t, :));
    if (pd == 1)
        tmp_dist = abs(tmp_dist);
    else
        tmp_dist = tmp_dist .^ 2;
    end
    hash_dist = hash_dist + tmp_dist;
end
#include "mex.h"
#include <assert.h>
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include "acml.h"
#include <acml_mv.h>

#define MAX_THREADS 20


double* gl_p_sig_wx; //p_sig_wx = sig(W' * X);
int gl_m;
int gl_D_1;
int gl_N;
int gl_n_center_index;
double* gl_p_hash_dist;
double* gl_p_diff_x; //beta * phi() * (1 - phi()) * x

int* gl_p_center_index;
double* gl_pX;
double* gl_pW;

double* gl_p_exp_dist;
double gl_beta;
double gl_mu;
double* gl_p_deltaW;

typedef struct SIndex
{
	int start;
	int end;
}SIndex;

void release_gl()
{
	if (gl_p_sig_wx)
	{
		free(gl_p_sig_wx);
		gl_p_sig_wx = NULL;
	}
	if (gl_p_hash_dist)
	{
		free(gl_p_hash_dist);
		gl_p_hash_dist = NULL;
	}
	if (gl_p_diff_x)
	{
		free(gl_p_diff_x);
		gl_p_diff_x = NULL;
	}
}


void init_gl()
{
	release_gl();

	gl_p_sig_wx = (double*)malloc(gl_m * gl_N * sizeof(double));
	if (!gl_p_sig_wx)
	{
		mexErrMsgTxt("no enough memory");
	}

	gl_p_hash_dist = (double*)malloc(gl_n_center_index * gl_N * sizeof(double));
	if (!gl_p_hash_dist)
	{
		mexErrMsgTxt("no enough memory");
	}

	gl_p_diff_x = (double*)malloc(gl_D_1 * gl_N * sizeof(double));
	if (!gl_p_diff_x)
	{
		mexErrMsgTxt("no enough memory");
	}
}



void parse_double_matrix(const mxArray* prhs,
				  int *m, int *n, double** ptr)
{
	assert(mxIsDouble(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (double*)mxGetPr(prhs);
}

void parse_int8_matrix(const mxArray* prhs,
				  int *m, int *n, char** ptr)
{
	assert(mxIsInt8(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	
	*ptr = (char*)mxGetPr(prhs);
}

void parse_int_matrix(const mxArray* prhs,
				  int *m, int *n, int** ptr)
{
	assert(mxIsInt32(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (int*)mxGetPr(prhs);
}




void compute_sig_wx()
{
	char transw = 't';
	char transx = 'n';
	double alpha = -gl_beta;
	double beta = 0.0;
	int i, j, n;
	double *p;


	//-beta * w' * x
	DGEMM(&transw, &transx, &gl_m, &gl_N, &gl_D_1,
		&alpha, gl_pW, &gl_D_1, gl_pX, &gl_D_1, &beta, gl_p_sig_wx, &gl_m, 1, 1);

	n = gl_m * gl_N;
	//exp(-beta * w' * x)
	vrda_exp(n, gl_p_sig_wx, gl_p_sig_wx);

	p = gl_p_sig_wx;
	for (i = 0; i < n; i++)
	{
		(*p) = 1.0 / (1 + (*p));
		p++;
	}
}

void compute_hash_dist()
{
	int i, i2;
	int j;
	int t;
	double s;

	for (j = 0; j < gl_N; j++)
	{
		for (i = 0; i < gl_n_center_index; i++)
		{
			i2 = p_center_index[i];
			s = 0;
			for (t = 0; t < gl_m; t++)
			{
				s += fabs(gl_p_sig_wx[(i2 - 1) * gl_m + t] - 
					gl_p_sig_wx[j * gl_m + t]);
			}
			gl_p_hash_dist[j * gl_n_center_index + i] = s;
		}
	}
}

void compute_diff_x(const double* pX, int t, double beta)
{
	int i;
	int j;
	double coef;
	double tmp;

	for (j = 0; j < gl_N; j++)
	{
		tmp = gl_p_sig_wx[j * gl_m + t];
		coef = beta * tmp * (1 - tmp);
		for (i = 0; i < gl_D_1; i++)
		{
			gl_p_diff_x[j * gl_D_1 + i] = coef * pX[j * gl_D_1 + i];
		}
	}
}
double calc_objective()
{	
	int i; 
	int j;
	int k;
	int t;
	double s, s1;
	double result;
	
	compute_sig_wx();
	
	compute_hash_dist();
	
	result = 0;
	for (j = 0; j < gl_N; j++)
	{
		for (i = 0; i < gl_n_center_index; i++)
		{
			s = gl_p_hash_dist[j * gl_n_center_index + i];
			s -= p_exp_dist[j * gl_n_center_index + i];
			s *= s;
			result += s;
		}
	}
	result /= gl_n_center_index * gl_N;
	
	s = 0;
	s1 = 0;
	for (t = 0; t < gl_m; t++)
	{
		s = 0;
		for (k = 0; k < gl_D_1; k++)
		{
			i = t * gl_D_1 + k;
			s += pW[i] * pW[i];
		}
		s -= 1;
		s *= s;
		s1 += s;
	}
	result += 0.25 * mu * s1;
	
	return result;
}

void ThreadFunc1(LPVOID lp_param)
{
	SIndex* p_index;
	int t;
	int i;
	int k;
	int j;
	int i2;
	double left;
	double right;
	double coef;
	double* p_i2;
	double* p_j;
	double* p_t;
	double ci, cj;

	p_index = (SIndex*)lp_param;
	for (t = p_index->start; t < p_index->end; t++)
	{
		for (j = 0; j < gl_N; j++)
		{
			for (i = 0; i < gl_n_center_index; i++)
			{
				i2 = gl_p_center_index[i] - 1;
				left = gl_p_sig_wx[i2 * gl_m + t];
				right = gl_p_sig_wx[j * gl_m + t];
				if (left == right)
				{
					continue;
				}
				else if (left < right)
				{
					coef = -2;
				}
				else
				{
					coef = 2;
				}
				coef *= gl_p_hash_dist[j * gl_n_center_index + i] - 
					gl_p_exp_dist[j * gl_n_center_index + i];
				
				ci = gl_p_sig_wx[i2 * gl_m + t];
				ci = gl_beta * ci * (1 - ci);
				cj = gl_p_sig_wx[j * gl_m + t];
				cj = gl_beta * cj * (1 - cj);

				p_i2 = gl_pX + gl_D_1 * i2;
				p_j = gl_pX + gl_D_1 * j;
				p_t = gl_p_deltaW + gl_D_1 * t;
				for (k = 0; k < gl_D_1; k++)
				{
					(*p_t) += (ci * (*p_i2++) - cj * (*p_j++)) * coef; 
					p_t++;
				}

			}
		}
	}
}

void allocate_task(int total_task, SIndex all_index[], int max_thread, int *p_num_thread)
{
	int every_task;
	int i;

	if ((total_task % max_thread) == 0)
	{
		every_task = total_task / max_thread;
	}
	else
	{
		every_task = total_task / max_thread + 1;
	}

	for (i = 0; i < total_task / every_task; i++)
	{
		all_index[i].start = i * every_task;
		all_index[i].end = (i + 1) * every_task;
	}
	
	if ((total_task % every_task) != 0)
	{
		all_index[i].start = i * every_task;
		all_index[i].end = total_task;
		i++;
	}
	*p_num_thread = i;
}
void calc_delta_obj(double* p_obj)
{
	int i, i2;
	int j;
	int k;
	int t;
	int num_ele;
	double coef;
	double s;
	double* p_i2;
	double* p_j;
	double* p_t;
	double left;
	double right;
	int N_ij;
	HANDLE  hThreadArray[MAX_THREADS];
	SIndex all_index[MAX_THREADS];
	int num_thread;
	DWORD   dwThreadIdArray[MAX_THREADS];
	double ci, cj;

	*p_obj = calc_objective();
	
	num_ele = gl_D_1 * gl_m;
	memset(gl_p_deltaW, 0, num_ele * sizeof(double));
	
	for (t = 0; t < gl_m; t++)
	{
		for (j = 0; j < gl_N; j++)
		{
			for (i = 0; i < gl_n_center_index; i++)
			{
				i2 = gl_p_center_index[i] - 1;
				left = gl_p_sig_wx[i2 * gl_m + t];
				right = gl_p_sig_wx[j * gl_m + t];
				if (left == right)
				{
					continue;
				}
				else if (left < right)
				{
					coef = -2;
				}
				else
				{
					coef = 2;
				}
				coef *= gl_p_hash_dist[j * gl_n_center_index + i] - 
					gl_p_exp_dist[j * gl_n_center_index + i];
				
				ci = gl_p_sig_wx[i2 * gl_m + t];
				ci = gl_beta * ci * (1 - ci);
				cj = gl_p_sig_wx[j * gl_m + t];
				cj = gl_beta * cj * (1 - cj);

				p_i2 = gl_pX + gl_D_1 * i2;
				p_j = gl_pX + gl_D_1 * j;
				p_t = gl_p_deltaW + gl_D_1 * t;
				for (k = 0; k < gl_D_1; k++)
				{
					(*p_t) += (ci * (*p_i2++) - cj * (*p_j++)) * coef; 
					p_t++;
				}

			}
		}
	}

	//allocate_task(gl_m, all_index, MAX_THREADS, &num_thread);
	//for (t = 0; t < num_thread; t++)
	//{
	//	hThreadArray[t] = CreateThread( 
 //           NULL,                   // default security attributes
 //           0,                      // use default stack size  
 //           ThreadFunc1,       // thread function name
 //           all_index + t,          // argument to thread function 
 //           0,                      // use default creation flags 
 //           &dwThreadIdArray[t]);
	//}
	//WaitForMultipleObjects(num_thread, hThreadArray, TRUE, INFINITE);

	//////////////////////

	N_ij = gl_n_center_index * gl_N;
	for (i = 0; i < num_ele; i++)
	{
		p_deltaW[i] /= N_ij;
	}

	for (t = 0; t < gl_m; t++)
	{
		coef = 0;
		for (i = 0; i < gl_D_1; i++)
		{
			s = pW[t * gl_D_1 + i];
			coef += s * s;
		}
		coef -= 1;
		coef *= mu;
		for (i = 0; i < gl_D_1; i++)
		{
			p_deltaW[t * gl_D_1 + i] += coef * pW[t * gl_D_1 + i];
		}
	}
}




///// function [obj] = jf_c_objective(...
//         W, X, center_index, ...
//         beta, mu, subI)
void mexFunction_obj(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	double* pW;
	int D_1;
	int m;
	
	int int_tmp;
	int N;
	
	int* p_center_index;
	int n_center_index;
	
	double beta;
	double* p_beta;
	
	double mu;
	double* p_mu;
	
	double* p_exp_dist;
	
	double s;
	double* pout;
	
	parse_double_matrix(prhs[0], &D_1, &m, &pW);
	
	parse_double_matrix(prhs[1], &int_tmp, &N, &gl_pX);
	
	parse_int_matrix(prhs[2], &int_tmp, &n_center_index, &gl_p_center_index);
	
	p_beta = &beta;
	parse_double_matrix(prhs[3], &int_tmp, &int_tmp, &p_beta);
	beta = *p_beta;

	p_mu = &mu;
	parse_double_matrix(prhs[4], &int_tmp, &int_tmp, &p_mu);
	mu = *p_mu;
	
	parse_double_matrix(prhs[5], &int_tmp, &int_tmp, &p_exp_dist);

	
	s = calc_objective(pW,  //W si D_1 * m
				gl_pX, gl_p_center_index, 
				beta,  mu,
				p_exp_dist);

	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	pout = mxGetPr(plhs[0]);
	*pout = s;
}

 //[deltaW pre] = jf_deltaW_rankAbs3(...
 //           W, X, center_index, ...
 //           beta, mu, subI);
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int int_tmp;
	double* p_beta;
	double* p_mu;
	
	double s;
	double* p_obj;
	clock_t start, finish;
	char buf[256];

	parse_double_matrix(prhs[0], &gl_D_1, &gl_m, &gl_pW);
	parse_double_matrix(prhs[1], &int_tmp, &gl_N, &gl_pX);
	parse_int_matrix(prhs[2], &int_tmp, &gl_n_center_index, &gl_p_center_index);
	
	p_beta = &gl_beta;
	parse_double_matrix(prhs[3], &int_tmp, &int_tmp, &p_beta);
	gl_beta = *p_beta;

	p_mu = &gl_mu;
	parse_double_matrix(prhs[4], &int_tmp, &int_tmp, &p_mu);
	gl_mu = *p_mu;
	
	parse_double_matrix(prhs[5], &int_tmp, &int_tmp, &gl_p_exp_dist);

	init_gl();

	plhs[0] = mxCreateDoubleMatrix(gl_D_1, gl_m, mxREAL);
	gl_p_deltaW = mxGetPr(plhs[0]);
	
	plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
	p_obj = mxGetPr(plhs[1]);

	start = clock();
	calc_delta_obj();
	finish = clock();
	mexPrintf("%f\n",((double)finish - (double)start) / CLOCKS_PER_SEC);

	release_gl();
}
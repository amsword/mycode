function [hash_dist sigQueryX sigX] = ...
    jf_calc_hash_dist2(W, X, QueryX, beta, pd)

m = size(W, 2);

sigX = W' * X;
sigX = sigmf(sigX, [beta 0]);

sigQueryX = W' * QueryX;
sigQueryX = sigmf(sigQueryX, [beta 0]);

hash_dist = zeros(size(QueryX, 2), size(X, 2));
for t = 1 : m
    tmp_dist = bsxfun(@minus, sigQueryX(t, :)', sigX(t, :));
    if (pd == 1)
        tmp_dist = abs(tmp_dist);
    else
        tmp_dist = tmp_dist .^ 2;
    end
    hash_dist = hash_dist + tmp_dist;
end
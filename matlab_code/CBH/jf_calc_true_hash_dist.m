function Dhamm = jf_calc_true_hash_dist(W, Xtraining, center_index)
B = (W' * [Xtraining; ones(1, size(Xtraining, 2))]) > 0;
B = compactbit(B);
Dhamm = hammingDist2(B(:, center_index), B);
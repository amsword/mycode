function y = jf_exact_objective(W, X, centerIndex, lambda, dk, I)

B = W' * X > 0;

B = compactbit(B);
hash_dist = hammingDist(B(:, centerIndex), B);

rho = length(dk);
s = 0;
for k = 1 : rho
    diff_dist = hash_dist - dk(k);
	diff_dist = diff_dist > 0;
            
    diff_dist = I(centerIndex, :, k) .* diff_dist;
    
    s = s + sum(diff_dist(:));
end

J = ~I;

s1 = 0;
for k = 1 : rho
     diff_dist = 1 - hash_dist + dk(k);
     diff_dist = diff_dist > 0;
     diff_dist = J(centerIndex, :, k) .* diff_dist;
     s1 = s1 + sum(diff_dist(:));
end

y = (s + s1 * lambda) / length(centerIndex);

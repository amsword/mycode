function [deltaW] = jf_deltaW_rankAbs2(...
                                    W, X, center_index, ...
                                    alpha, beta, mu, rd_dk, po, pd, ...
                                    subI)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
N = size(X, 2);
rho = numel(rd_dk);
m = size(W, 2);

%% hash dist: get the all involved X that should be multiplied by W
num_non_zeros = 0;

idx_col_in_X = cell(rho, 1);
idx_row_in_center = cell(rho, 1);
is_involved_in_X = false(N, 1);
for k = 1 : rho
    [idx_row_in_center{k} idx_col_in_X{k}] = find(subI{k});
    num_non_zeros = num_non_zeros + numel(idx_col_in_X{k});
    is_involved_in_X(idx_col_in_X{k}) = true;
end
is_involved_in_X(center_index) = true;
tmp_v = find(is_involved_in_X);
map_idx = zeros(N, 1);
map_idx(tmp_v) = 1 : length(tmp_v);
sigX = W' * X(:, is_involved_in_X);
sigX = sigmf(sigX, [beta, 0]);
diff_sigX = beta * sigX .* (1 - sigX);

%% diff
deltaW1 = zeros(size(W));
for k = 1 : rho
    % calculate dij
    idx_row_in_X = center_index(idx_row_in_center{k}); % index in X
    idx_row_in_SigX = map_idx(idx_row_in_X); % index in sigX, only involved X is calculated
    idx_col_in_SigX = map_idx(idx_col_in_X{k});
    
    tmp_dij = sigX(:, idx_row_in_SigX) - sigX(:, idx_col_in_SigX);
    if (pd == 1)
        dij = abs(tmp_dij);
    elseif (pd == 2)
        dij = tmp_dij .* tmp_dij;
    end
    dij = sum(dij, 1)';
    
    % calculate the partial derivative of the objective function part
    if (po == 1)
        diff_o = sign(dij - rd_dk(k));
    elseif (po == 2)
        diff_o = 2 * (dij - rd_dk(k));
    end
    
	idx_x = false(1, N);
	idx_x(idx_row_in_X) = true;
	idx_x(idx_col_in_X{k}) = true;
	
    tmp_v = find(idx_x);
    sub_map_idx = zeros(N, 1);
    sub_map_idx(idx_x) = 1 : length(tmp_v);
    subX = X(:, idx_x);
    
	idx_sub_diff_sigX = map_idx(idx_x);
	idx_left_in_diff_x = sub_map_idx(idx_row_in_X);
	idx_right_in_diff_x = sub_map_idx(idx_col_in_X{k});
    for t = 1 : m
        if (pd == 1)
            diff_d = sign(tmp_dij(t, :))';
        elseif (pd == 2)
            diff_d = 2 * tmp_dij(t, :)';
        end
		diff_x = bsxfun(@times, subX, diff_sigX(t, idx_sub_diff_sigX));
		diff_x = diff_x(:, idx_left_in_diff_x) - diff_x(:, idx_right_in_diff_x);
		
		tij = bsxfun(@times, diff_x, (diff_o .* diff_d)');
		deltaW1(:, t) = deltaW1(:, t) + alpha(k) * sum(tij, 2);
    end
end

deltaW1 = deltaW1 / num_non_zeros;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW1 + deltaW2;
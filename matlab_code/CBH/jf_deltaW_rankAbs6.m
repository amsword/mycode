function [deltaW, obj] =jf_deltaW_rankAbs6(...
                                        W, X, QueryX, ...
                                        beta, mu, po, pd, ...
                                        subI)
%% X and W: the aumented one. 
%% subI(center_index, [1 : N]).

% hash_dist_pair
m = size(W, 2);

%% hash dist: get the all involved X that should be multiplied by W
[obj hash_dist sigX sigQueryX] = jf_objective_rankAbs6(...
                        W, X, QueryX, ...
                        beta, mu, po, pd, ...
                        subI);
%% diff
deltaW1 = zeros(size(W));

% calculate the partial derivative of the objective function part
if (po == 1)
    diff_o = sign(hash_dist - subI);
elseif (po == 2)
    diff_o = 2 * (hash_dist - subI);
end

diff_sigX = beta * sigX .* (1 - sigX);
diff_sigQueryX = beta * sigQueryX .* (1 - sigQueryX);
for t = 1 : m
    tmp_dij = bsxfun(@minus, sigQueryX(t, :)', sigX(t, :));
    if (pd == 1)
        diff_d = sign(tmp_dij);
    elseif (pd == 2)
        diff_d = 2 * tmp_dij;
    end
    P = diff_o .* diff_d;
    
    diff_QueryX = bsxfun(@times, QueryX, diff_sigQueryX(t, :));
    
    P1 = sum(P, 2);
    P1 = bsxfun(@times, diff_QueryX, P1');
    P1 = sum(P1, 2);
    
    diff_X = bsxfun(@times, X, diff_sigX(t, :));
    P2 = sum(P, 1);
    P2 = bsxfun(@times, diff_X, P2);
    P2 = sum(P2, 2);

    deltaW1(:, t) = P1 - P2;
end

deltaW1 = deltaW1 / size(X, 2) / size(QueryX, 2);
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW1 + deltaW2;
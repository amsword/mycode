function dk = calc_dk(Nk, N, m)
%% in the high-dimensional space, there are length(Nk) grades. in every
%% grade, say k, the number of points within this grade is Nk(k). Total
%% number of points is N. m is the code length of the low-dimensional space

%% Nk(k) / N = \sum_(i=1)^(dk(k)){C_m^i} / 2 ^ m
yk = zeros(1, m + 1);
yk(1) = nchoosek(m, 0);
for i = 2 : m + 1
    yk(i) = yk(i - 1) + nchoosek(m, i - 1);
end

yk = yk / 2 ^ m;
xk = Nk / N;

row = length(Nk);
dk = zeros(row, 1);
for i = 1 : row
    dk(i) = find(yk > xk(i), 1, 'first') - 1;
end
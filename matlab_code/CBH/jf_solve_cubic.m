function x = jf_solve_cubic(a, b, c, d)
tmp_sqrt = sqrt(b^2 - 3 * a * c);
x1 = -b + tmp_sqrt;
x2 = -b - tmp_sqrt;
x1 = x1 / 3 / a;
x2 = x2 / 3 / a;
if (a * x1^3 + b * x1^2 + c * x1 + d < ...
	a * x2 ^ 3 + b * x2 ^ 2 + c * x2)
	x = x1;
else
	x = x2;
end
function [deltaW, obj] = jf_deltaW_rankAbs5(...
							W, X, QueryX, ...
							beta, mu, po, pd, ...
							idx_row, idx_col, exp_dist)
%% X and W: the aumented one. 
%% sample pairs

% hash_dist_pair
m = size(W, 2);

%% hash dist: get the all involved X that should be multiplied by W
[obj, hash_dist, sig_X, sig_queryX, X, QueryX, idx_row, idx_col] = ... 
						jf_objective_rankAbs5(...
							W, X, QueryX, ...
							beta, mu, po, pd, ...
							 idx_row, idx_col, exp_dist);
%% diff
deltaW1 = zeros(size(W));

% calculate the partial derivative of the objective function part
if (po == 1)
    diff_o = sign(hash_dist - exp_dist);
elseif (po == 2)
    diff_o = 2 * (hash_dist - exp_dist);
end

for t = 1 : m
    tmp_dij = sigX2(t, idx_row2) - sigX2(t, idx_col2);
    if (pd == 1)
        diff_d = sign(tmp_dij);
    elseif (pd == 2)
        diff_d = 2 * tmp_dij;
    end
    P = diff_o' .* diff_d;
	
	diff_X = bsxfun(@times, X, beta * sig_X(t, :) .* (1 - sig_X(t, :)));
	diff_queryX = bsxfun(@times, ...
			QueryX, beta * sig_queryX(t, :) .* (1 - sig_queryX(t, :)));
	diff_vec = diff_queryX(:, idx_row) - diff_X(:, idx_col);
	diff_vec = bsxfun(@times, diff_vec, P');
	
    deltaW1(:, t) = sum(diff_vec, 2);
end

deltaW1 = deltaW1 / numel(idx_row);
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW1 + deltaW2;
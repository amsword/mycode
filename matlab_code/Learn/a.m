rng(0);

C = rand(16, 512);
d = rand(16, 1);
Aineq = zeros(2, 512);
Aineq(1, 1 : 256) = 1;
Aineq(2, 257 : 512) = 1;
bineq = zeros(2, 1);
bineq(:) = 2;

c = cell(512, 1);
for i = 1 : 512
    c{i} = 'B';
end
xtype = char(c);

% Opt = opti('H',C' * C + 10^-12 * eye(512, 512),'f',C' * d,'ineq',Aineq,bineq, 'xtype', xtype);
% x = solve(Opt);
%
tic;
x = cplexlsqbilp(C,d,Aineq,bineq);
toc;
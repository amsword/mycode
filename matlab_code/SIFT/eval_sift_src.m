% evaluate SIFT
% make sure S_ind_total, S_num_total, ind_total exits after running
% get_base_gnd.m
load('data/sift-1m.mat', 'query', 'base');

load('data\sift_pc'); % the principle component for the training dataset should be better stored to have consistent results
data_eval.Xbase = pc' * base;
data_eval.Xquery = pc' * query;

data_eval2.Xbase = base;
data_eval2.Xquery = query;
clear base query

nb = 16;
% must have S_ind_total, S_num_total and ind_total first

%% SIG
load('result/sig_sift', 'Wsig');
sig_eval = eval_sift(data_eval, Wsig, nb, [], 'W_based', S_ind_total, S_num_total, ind_total)
save result/sig_sift_eval sig_eval

%% MLH
load('result/mlh_sift', 'Wmlh');
mlh_eval = eval_sift(data_eval, Wmlh, nb, [], 'W_based', S_ind_total, S_num_total, ind_total)
save result/mlh_sift_eval mlh_eval

%% USPLH
load('result/usplh_sift', 'Wusplh');
usplh_eval = eval_sift(data_eval, Wusplh, nb, [], 'W_based', S_ind_total, S_num_total, ind_total)
save result/usplh_sift_eval usplh_eval

%% SH
load('result/sh_sift', 'SHparam');
sh_eval = eval_sift(data_eval, [], nb, SHparam, 'sh', S_ind_total, S_num_total, ind_total)
save result/sh_sift_eval sh_eval

% %% SKLSH
% load('result/sh_sift', 'RFparam');
% [p_sklsh, r_sklsh, n_sklsh] = eval_sift(data_eval, [], nb, RFparam, 'sklsh', S_ind_total, S_num_total)
% save result_16/sklsh_sift_eval p_sklsh r_sklsh n_sklsh

%% LSH
load('result/lsh_sift', 'Wlsh');
lsh_eval = eval_sift(data_eval, Wlsh, nb, [], 'W_based', S_ind_total, S_num_total, ind_total)
save result/lsh_sift_eval lsh_eval

% %% ITQ
% load('result/itq_sift', 'ITQparam');
% [p_itq, r_itq, n_itq] = eval_sift(data_eval, [], nb, ITQparam, 'itq', S_ind_total, S_num_total)

%% CH
ch_eval = eval_sift_ch(data_eval, CH, S_ind_total, S_num_total, ind_total)
save result/ch_sift_eval ch_eval

 %% Precision Figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1,'FontSize',16);
% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[0 21000]);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.05 .7]);
box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');
n = [100, 128, 256, 512, 1024, 2048, 4096, 9192, 20000];
plot1 = plot(n, sh_eval.p_code(n), n, sig_eval.p_code(n), n, mlh_eval.p_code(n),...
    n, usplh_eval.p_code(n), n, lsh_eval.p_code(n),...
    n, ch_eval.p_code(n));
set(plot1(1),'Marker','v',...
    'Color',[0 0 0],...
    'DisplayName','SH');
set(plot1(2),'Marker','square','Color',[1 0 0],'DisplayName','CBH');
set(plot1(3),'Marker','o','Color',[0 0 1],'DisplayName','MLH');
set(plot1(4),'Marker','.','Color',[0 1 1],'DisplayName','USPLH');
set(plot1(5),'Marker','<','Color',[0 1 0],'DisplayName','LSH');
set(plot1(6),'Marker','diamond','Color',[1 1 0],'DisplayName','CH');

% Create xlabel
xlabel('Number of retrieved points','FontSize',16);

% Create ylabel
ylabel('Precision','FontSize',16);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.67960908610671 0.475873015873016 0.20919175911252 0.43047619047619]);

%% Recall Figure
figure2 = figure;

% Create axes
axes2 = axes('Parent',figure2,'FontSize',16);
% Uncomment the following line to preserve the X-limits of the axes
xlim(axes2,[0 21000]);
box(axes2,'on');
grid(axes2,'on');
hold(axes2,'all');

% Create multiple lines using matrix input to plot

n = [100, 128, 256, 512, 1024, 2048, 4096, 9192, 20000];
plot1 = plot(n, sh_eval.r_code(n), n, sig_eval.r_code(n), n, mlh_eval.r_code(n),...
    n, usplh_eval.r_code(n), n, lsh_eval.r_code(n),...
    n, ch_eval.r_code(n));
set(plot1(1),'Marker','v',...
    'Color',[0 0 0],...
    'DisplayName','SH');
set(plot1(2),'Marker','square','Color',[1 0 0],'DisplayName','CBH');
set(plot1(3),'Marker','o','Color',[0 0 1],'DisplayName','MLH');
set(plot1(4),'Marker','.','Color',[0 1 1],'DisplayName','USPLH');
set(plot1(5),'Marker','<','Color',[0 1 0],'DisplayName','LSH');
set(plot1(6),'Marker','diamond','Color',[1 1 0],'DisplayName','CH');

% Create xlabel
xlabel('Number of retrieved points','FontSize',16);

% Create ylabel
ylabel('Recall','FontSize',16);

% Create legend
legend1 = legend(axes2,'show');
set(legend1,...
    'Position',[0.677231907025887 0.128253968253968 0.20919175911252 0.43047619047619]);

%%
plot_labelme_prec2recall(sh_eval.r1, sh_eval.p1, sig_eval.r1, sig_eval.p1,...
    mlh_eval.r1, mlh_eval.p1, usplh_eval.r1, usplh_eval.p1,...
    lsh_eval.r1, lsh_eval.p1, ch_eval.r1, ch_eval.p1)

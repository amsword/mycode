function data = create_sift()

fprintf('Creating 1M-SIFT dataset ... ');

%%only for the visualization
load('data/sift-1m', 'learn');
% data-points are stored in columns
% data downloaded from http://corpus-texmex.irisa.fr/, which is the
% ANN_SIFT1M dataset. 'learn', 'query', 'base' are obtained after using the
% routine provided from http://corpus-texmex.irisa.fr/fvecs_read.m by the
% author

X = double(learn);
clear learn;

% center, then normalize data
gist_mean = mean(X, 2);
X = bsxfun(@minus, X, gist_mean);
normX = sqrt(sum(X.^2, 1));
X = bsxfun(@rdivide, X, normX);

data.Xtraining = X;
data.gist_mean = gist_mean;

data

fprintf('done\n');

function eval = eval_sift(data, W, nb, param, MODE, S_ind_total, S_num_total, ind_total)

[nDim, nBase] = size(data.Xbase);

batch = 100;

if strcmp(MODE, 'W_based')
    baseCodes = compressCodes(data.Xbase, W);
elseif strcmp(MODE, 'sh')
    [baseCodes, U] = compressSH(data.Xbase', param);
    baseCodes = baseCodes';
elseif strcmp(MODE, 'sklsh')
    [baseCodes, U] = RF_compress(data.Xbase', param);
    %baseCodes = baseCodes';
elseif strcmp(MODE, 'itq')
    [baseCodes] = compressITQ(data.Xbase', param.nb);
    baseCodes = baseCodes';
end


S_start_ind = 0;

P_CODE = zeros(batch, 20000);
R_CODE = zeros(size(P_CODE));
PRECRHO = zeros(batch, 4);
NRHO = zeros(size(PRECRHO));
P1 = zeros(1, nb+1);
R1 = zeros(size(P1));
AP = 0;

nbatch = 1;
evalParam.rho = 3;

for i = 1:nbatch
    b_start = (i-1)*batch+1; b_end = i*batch;
    test = data.Xquery(:, b_start:b_end);
    
    if strcmp(MODE, 'W_based')
        testCodes = compressCodes(test, W);
    elseif strcmp(MODE, 'sh')
        [testCodes, U] = compressSH(test', param);
        testCodes = testCodes';
    elseif strcmp(MODE, 'sklsh')
        [testCodes, U] = RF_compress(test', param);
%         testCodes = testCodes';
    elseif strcmp(MODE, 'itq')
        testCodes = compressITQ(test', param.nb);
        testCodes = testCodes';
    end
    D_code = hammingDist(testCodes, baseCodes);
    
    % get the supervision
    S = zeros(batch, nBase, 'uint8');
    S(S_ind_total(S_start_ind+1:S_start_ind+S_num_total(i))) = 1;
    S_start_ind = S_start_ind + S_num_total(i);

    P_code = zeros(batch, 20000);
    R_code = zeros(size(P_code));
    PrecRho = zeros(batch, evalParam.rho+1);
    NRho = zeros(size(PrecRho));
    for n = 1:batch
        ind = n-1+b_start;
        [foo_code, j_code] = sort(D_code(n, :), 'ascend');
        j_code = j_code(:, 1:20000);

        j_truth = ind_total(ind, 1:20000);
        evalParam.foo_code = foo_code;
        result = eval_small(j_truth, j_code, evalParam);
        P_code(n, :) = result.p_code;
        R_code(n, :) = result.r_code;
        PrecRho(n, :) = result.prec_at_rho;
        NRho(n, :) = result.n_points_rho;
    end
    P_CODE = P_CODE+P_code;
    R_CODE = R_CODE+R_code;
    PRECRHO = PRECRHO+PrecRho;
    NRHO = NRHO+NRho;
 
    [p1 r1 rpairs] = evaluation2(S, D_code, nb);
    p1 = p1';
    P1 = P1+p1; R1 = R1+r1;
    AP = AP + sum([(p1(1:end-1)+p1(2:end))/2].*[(r1(2:end)-r1(1:end-1))]);
    fprintf('batch %d\n', i);
end
eval.p_code = mean(P_CODE, 1)/nbatch;
eval.r_code = mean(R_CODE, 1)/nbatch;
eval.a_code = mean(PRECRHO, 1)/nbatch;
eval.n_code = mean(NRHO, 1)/nbatch;
eval.p1 = P1/nbatch;
eval.r1 = R1/nbatch;
eval.AP = AP/nbatch;
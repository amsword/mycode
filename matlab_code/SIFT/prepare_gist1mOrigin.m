% function prepare_gist1mOrigin(save_file)

save_file = src_file;

sift_1m = fvecs_read('F:\v-jianfw\hash\v-jianfw\Data_HashCode\GIST1M\data\gist_base.fvecs');

% mean_removed
sift_1m = double(sift_1m);

mean_value = mean(sift_1m, 2);
sift_1m = bsxfun(@minus, sift_1m, mean_value);

% 
idx_test = randperm(1000000, 100000);
idx_train = setdiff(1 : 1000000, idx_test);

OriginXtraining = sift_1m(:, idx_train);
OriginXtest = sift_1m(:, idx_test);

save(save_file.train, 'OriginXtraining', 'idx_train', '-v7.3');
save(save_file.test, 'OriginXtest', 'idx_test', '-v7.3');

save_mat(OriginXtraining, save_file.c_train, 'double');
save_mat(OriginXtest, save_file.c_test, 'double');


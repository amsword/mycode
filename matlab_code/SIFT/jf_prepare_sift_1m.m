function jf_prepare_sift_1m(save_file)
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

Xtraining = fvecs_read('F:\v-jianfw\HashCode\SIFT\data\sift_learn.fvecs');
Xbase = fvecs_read('F:\v-jianfw\HashCode\SIFT\data\sift_base.fvecs');
Xtest = fvecs_read('F:\v-jianfw\HashCode\SIFT\data\sift_query.fvecs');

Xtraining = double(Xtraining);
Xbase = double(Xbase);
Xtest = double(Xtest);

jf_preprocessing(Xtraining, Xbase, Xtest, save_file);
%% get test-base KNN info
% should be done before running eval_sift_src.m

query = fvecs_read('data\sift_query.fvecs');
base = fvecs_read('data\sift_base.fvecs');

[nDim, nQuery] = size(query);
[nDim, nBase] = size(base);

batch = 100;
nBatch = 1;
prop = 0.02; % 2 percent of points are considered neighbors

S_ind_total = [];
S_num_total = [];
ind_total = [];
for i = 1:nBatch
    D = KNN_sift(query(:, (i-1)*batch+1:i*batch), base);
    tic;
    [sortedD ind] = sort(D, 2);
    toc;
    ind = uint32(ind(:, 1:end));
    thresDist = sortedD(:, prop * nBase);
    S_ind = find(bsxfun(@le, D, thresDist) == 1); %the S_ind is relative to batch D
    S_num = numel(S_ind);
    S_ind_total = cat(1, S_ind_total, S_ind);
    S_num_total = cat(1, S_num_total, S_num);
    ind_total = cat(1, ind_total, ind);
    fprintf('batch %d, S_num_total %d\n', i, S_num);
end
    
    
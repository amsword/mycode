%% get ground-truth information for the 1M training set and stored in batches
batch = 1000; % batch size
nBatch = 100; % number of batch, smaller number for test
nFirst = 20000; % the nearest 20000 points
nLast = 10000; % to sample 10000 points from non-neighbors
data = create_sift();
train = data.Xtraining;
[nDim, nTrain] = size(train);

%ind_all = single(zeros(nFirst, nTrain));
for i = 1:nBatch
    ind_train = randi([1, nTrain], 1, batch); %the index of train data
    ind = KNN_gnd(train, train(:, ind_train), 100); % get the ranked index of training pair Euclidean distance
    ind_pos = ind(1:nFirst, :); % the first points are considered neighbors
    index_neg = randi([nFirst+1, nTrain], 1, nLast);
    ind_neg = ind(index_neg, :); % randomly sampled from the rest as non-neighbors
    name = sprintf('./learn_gnd_%d.mat', i);
    save(name, 'ind_pos', 'ind_neg', 'batch', 'i', 'ind_train'); % store the ground-truth information
    fprintf('%d ', i);
    if mod(i, 10) == 0
        fprintf('\n');
    end
end


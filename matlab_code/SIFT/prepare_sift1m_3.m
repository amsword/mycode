% function prepare_gist1mOrigin(save_file)

save_file = src_file;

gist_base = fvecs_read('E:\v-jianfw\Data\sift-1m\sift.tar\sift\sift\sift_base.fvecs');
gist_learn = fvecs_read('E:\v-jianfw\Data\sift-1m\sift.tar\sift\sift\sift_learn.fvecs');
gist_query = fvecs_read('E:\v-jianfw\Data\sift-1m\sift.tar\sift\sift\sift_query.fvecs');


% mean_removed
gist_base = double(gist_base);
gist_learn = double(gist_learn);
gist_query = double(gist_query);

mean_value = mean(gist_base, 2);
gist_base = bsxfun(@minus, gist_base, mean_value);
gist_learn = bsxfun(@minus, gist_learn, mean_value);
gist_query = bsxfun(@minus, gist_query, mean_value);

% 
OriginXtraining = gist_learn;
OriginXbase = gist_base;
OriginXtest = gist_query;

save_mat(OriginXbase, save_file.c_base, 'double');
save_mat(OriginXtraining, save_file.c_train, 'double');
save_mat(OriginXtest, save_file.c_test, 'double');


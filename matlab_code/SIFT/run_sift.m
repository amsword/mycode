%% create data
data = create_sift(); % create the sift data (only training part, different from LabelMe and Peekaboom)
perform_pca = 1			% whether to perform PCA dimensionality reduction
if (perform_pca)
  data2 = do_pca(data, 40);
else
  data2 = data;
end

nbs = [30];
rhos = [3];

% only test nb = 10
nb = nbs;

%% Following format follows from 'run_labelme.m'
%% SIG
clear train_params model_params;

model_params.weight = 1;
model_params.nb = nb;
model_params.rho = rhos(nbs == nb);
model_params.thresZ = log(1000)/nb * 2;
model_params.thresCode = log(1000)*2;
model_params.shrink_w = 1e-4;

train_params.nMinibatch = 100;
train_params.niters = 140;
train_params.lambda = 0.05;
train_params.nSamples = 10^5;

verbose = 1;
fprintf('start SIG training\n');
Wsig = SIG(data2, model_params, train_params, verbose);

save result/sig_sift Wsig train_params model_params

%% MLH
clear mlh_params
mlh_params.size_batches = 100;
mlh_params.eta = .1;
mlh_params.shrink_w = 1e-4;
mlh_params.lambda = 0.0;
mlh_params.rho = rhos(nbs == nb);
train_iter = 100;
train_zerobias = 1;

data2.MODE = 'hinge';
fprintf('start MLH training\n');
Wmlh = trainMLH(data2, {'hinge', mlh_params.rho, mlh_params.lambda}, nb, ...
		    [mlh_params.eta], .9, [mlh_params.size_batches], 'trainval', train_iter, train_zerobias, ...
		    5, 1, 50, [mlh_params.shrink_w], 1);
Wmlh = Wmlh.W;

save result/mlh_sift Wmlh mlh_params

% %% BRE
% clear best_params
% bre_params.size_batches = 100;
% bre_params.eta = .1;
% bre_params.shrink_w = 1e-4;
% 
% fprintf('start BRE training\n');
% Wbre = MLH(data2, {'bre'}, nb, [bre_params.eta], .9, [bre_params.size_batches], ...
%     'trainval', 100, 1, 5, 1, 50, [bre_params.shrink_w], 1);
% Wbre = Wbre.W;
% 
% save result/bre_sift Wbre bre_params

%% USPLH
data_eval2 = data2;
USPLHparam.nbits = nb;
USPLHparam.eta=.8;
USPLHparam.lambda=0.02;
USPLHparam.c_num=2000;

fprintf('start USPLH training\n');
USPLHparam = trainUSPLH(double(data_eval2.Xtraining'), USPLHparam);
Wusplh = [USPLHparam.w; USPLHparam.b]';

save result/usplh_sift USPLHparam Wusplh

%% SH
SHparam.nbits = nb;

fprintf('start SH training\n');
SHparam = trainSH(double(data_eval2.Xtraining'), SHparam);

save result/sh_sift SHparam

%% SKLSH
% RFparam.gamma = 2;
% RFparam.D = size(data_eval.Xbase, 1); % dim of data
% RFparam.M = nb;
% RFparam = RF_train(RFparam);
% 
% save result/sklsh_sift RFparam

%% LSH
LSHparam.nbits = nb;
LSHparam.L = 1;
[Ndim, Ntrain] = size(data2.Xtraining);

fprintf('start LSH training\n');
Wlsh = lshfunc(LSHparam.L, LSHparam.nbits, Ndim);
Wlsh = [Wlsh.A; Wlsh.b]';
save result/lsh_sift Wlsh LSHparam

%% CH
CHparam.nbits = nb;
CHparam.doMask = 0;
CHparam.eta = .1;
CHparam.L = 3;
CHparam.c_num = 1000;
CHparam.epsilon = 0.005;
CH = trainCH_ext(data_eval2.Xtraining', CHparam);
save result/ch_sift CH


num_dic = 5;
dic_size = 7;
dim = 6;

original_D = cell(num_dic, 1);
original_e = cell(num_dic, 1);
for i = 1 : num_dic
    original_D{i} = rand(dim, dic_size);
    original_e{i} = rand(dic_size, 1);
end
% original_D{i} = 1;
% original_D{1} = [1, 2];
% original_D{2} = [3, 4];
% original_e{1} = [1, 1];
% original_e{2} = [2, 2];


delta = rand(dim, dic_size);

clear y;
k = 1;
for t = [-10 : 10]
    subD = original_D;
    subD{1} = subD{1} + t * delta;
    y(k) = compute_penalty(subD, original_e);
    k = k + 1;
end
% plot([-10 : 10], y);

[coef, S] = polyfit([-10 : 10], y, 4);
coef
% coef * 2
subD = original_D;
gradient = compute_one_gradient(subD, original_e, 1);
sum(sum(gradient .* delta))

%%

delta = rand(dic_size, 1);

clear y;
k = 1;
for t = [-10 : 10]
    subE = original_e;
    subE{1} = subE{1} + t * delta;
    y(k) = compute_penalty(original_D, subE);
    k = k + 1;
end
% plot([-10 : 10], y);

[coef, S] = polyfit([-10 : 10], y, 4);
coef
% coef * 2

[~, gradient] = compute_one_gradient(original_D, original_e, 1);
sum(sum(gradient .* delta))


%%
tic;
mat_D = zeros(dim, num_dic * dic_size);
vec_e = zeros(num_dic * dic_size, 1);
start_index = 1;
for i = 1 : num_dic
    mat_D(:, start_index : start_index + dic_size - 1) = original_D{i};
    vec_e(start_index : start_index + dic_size - 1) = original_e{i};
    start_index = start_index + dic_size;
end

index_k = ones(num_dic, 1);
obj = 0;
while(1)
    s = 0;
    for c1 = 1 : num_dic
        for c2 = 1 : num_dic
            if c1 == c2
                continue;
            end
            s = s + original_D{c1}(:, index_k(c1))' * original_D{c2}(:, index_k(c2));
        end
    end
    s = s / 2;
    
    for c = 1 : num_dic
        s = s - original_e{c}(index_k(c));
    end
    s = s * s;
    obj = obj + s;
    
    is_continue = false;
    for c = num_dic : -1 : 1
        if index_k(c) == dic_size
            continue;
        end
        index_k(c) = index_k(c) + 1;
        is_continue = true;
        break;
    end
    if is_continue
        c = c + 1;
        while (c <= num_dic)
            index_k(c) = 1;
            c = c + 1;
        end
    else
        break;
    end   
end

obj = obj / dic_size ^ (num_dic)
toc;
tic;
compute_penalty(original_D, original_e)
toc;

%% true A2,1
index_k = ones(num_dic, 1);
obj = 0;
while(1)
    s = 0;
    for c1 = 1 : num_dic
        for c2 = 1 : num_dic
            if c1 == c2
                continue;
            end
            for c3 = 1 : num_dic
                for c4 = 1 : num_dic
                    if c3 == c4
                        continue;
                    end
                    s = s + ...
                        original_D{c1}(:, index_k(c1))' * original_D{c2}(:, index_k(c2)) * ...
                        original_D{c3}(:, index_k(c3))' * original_D{c4}(:, index_k(c4));
                end
            end
        end
    end
    s = s / 4;
    
    obj = obj + s;
    
    is_continue = false;
    for c = num_dic : -1 : 1
        if index_k(c) == dic_size
            continue;
        end
        index_k(c) = index_k(c) + 1;
        is_continue = true;
        break;
    end
    if is_continue
        c = c + 1;
        while (c <= num_dic)
            index_k(c) = 1;
            c = c + 1;
        end
    else
        break;
    end   
end
fprintf('%s\n', ['true a21: ' num2str(obj / (dic_size ^ num_dic)) ]);
%% ours A21
IC = zeros(dic_size * num_dic, dic_size * num_dic);
for i = 1 : num_dic
    t = zeros(dic_size * num_dic, 1);
    t((i - 1) * dic_size + 1 : i * dic_size) = 1;
    IC = IC + t * t';
end
t1 = 0;
t2 = 0;
t7 = 0;
t9 = 0;
t10 = 0;
for c = 1 : num_dic
    t1 = t1 + ones(dic_size, 1)' * original_D{c}' * ...
        original_D{c} * ones(dic_size, 1) * ...
        ones(dic_size, 1)' * original_D{c}' * ...
        mat_D * ones(dic_size * num_dic, 1);
    t2 = t2 + ones(dic_size, 1)' * original_D{c}' * ...
        original_D{c} * ones(dic_size, 1) * ...
        ones(dic_size, 1)' * original_D{c}' * ...
        original_D{c} * ones(dic_size, 1);
    t7 = t7 + ones(dic_size * num_dic, 1)' * mat_D' * ...
        original_D{c} * original_D{c}' * original_D{c} * ones(dic_size, 1);
    t9 = t9 + ones(dic_size, 1)' * original_D{c}' * ...
        original_D{c} * original_D{c}' * original_D{c} * ones(dic_size, 1);
    t10 = t10 + trace(original_D{c} * original_D{c}' * original_D{c} * original_D{c}');
end
t3 = mat_D * IC * mat_D' - mat_D * ones(dic_size * num_dic, dic_size * num_dic) * mat_D';
t3 = sum(t3(:) .^ 2);
t4 = ones(dic_size * num_dic, 1)' * mat_D' * mat_D * ones(dic_size * num_dic, 1) - ...
    trace(mat_D * IC * mat_D');
t4 = t4 * t4;
t5 = ones(dic_size * num_dic, 1)' * mat_D' * ...
    mat_D * ones(dic_size * num_dic, 1) * ...
    ones(dic_size * num_dic, 1)' * mat_D' * ...
    mat_D * ones(dic_size * num_dic, 1);
t6 = ones(dic_size * num_dic, 1)' * mat_D' * ...
    mat_D * mat_D' * mat_D * ones(dic_size * num_dic, 1);
t8 = trace(mat_D * IC * mat_D' * mat_D * mat_D');
A21 = 0.25 * dic_size^(num_dic - 4) * ...
    (8 * t1 - 6 * t2 + 2 * t3 + t4 - 2 * t5) + ...
    dic_size ^ (num_dic - 3) * ...
    (t6 - 2 * t7 - t8 + 2 * t9) + ...
    0.5 * dic_size ^ (num_dic - 2) * (trace(mat_D * mat_D' * mat_D * mat_D') - t10);
fprintf('%s\n', ['simplied: ' num2str(A21)]);





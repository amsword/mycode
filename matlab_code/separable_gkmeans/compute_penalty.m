function A2 = compute_penalty(cell_D, cell_e)
% cell_D = original_D;
% cell_e = original_e;

dim = size(cell_D{1}, 1);
num_dic = numel(cell_D);

mean_cell_D = zeros(dim, num_dic);

for c = 1 : num_dic
    mean_cell_D(:, c) = mean(cell_D{c}, 2);
end
sum_D = sum(mean_cell_D, 2);

magnitude_mean_cell_D = sum(mean_cell_D .^ 2, 1);
sum_magnitude_mean_cell_D = sum(magnitude_mean_cell_D);
weighted_mean_cell_D = bsxfun(@times, mean_cell_D, magnitude_mean_cell_D);
sumed_weighted = sum(weighted_mean_cell_D, 2);
t1110 = sumed_weighted' * sum_D;
t1111 = sum(magnitude_mean_cell_D .^ 2);
cov_mean_cell_D = mean_cell_D * mean_cell_D';
t1221 = trace(cov_mean_cell_D * cov_mean_cell_D);
cov_mean_D = sum_D * sum_D';
mag_mean_D = trace(cov_mean_D);
t0110 = trace(cov_mean_D * cov_mean_cell_D);
t0000 = mag_mean_D ^ 2;
t1100 = sum_magnitude_mean_cell_D * mag_mean_D;
t1122 = sum_magnitude_mean_cell_D ^ 2;

cov_cell_D = zeros(dim, dim, num_dic);
cov_cell_D_multi_mean = zeros(dim, num_dic);
for c = 1 : num_dic
    sub_cov_D = cell_D{c} * cell_D{c}' / size(cell_D{c}, 2);
    cov_cell_D(:, :, c) = sub_cov_D;
    cov_cell_D_multi_mean(:, c) = sub_cov_D * mean_cell_D(:, c);
end
sum_cov_cell_D_multi_mean = sum(cov_cell_D_multi_mean, 2);
cov_D = sum(cov_cell_D, 3);
cov_mean_cell_D = mean_cell_D * mean_cell_D';

s0000 = sum_D' * cov_D * sum_D;
s0111 = sum_D' * sum_cov_cell_D_multi_mean;
s1221 = trace(cov_mean_cell_D * cov_D);
s1111 = sum(sum(mean_cell_D .* cov_cell_D_multi_mean));

r0000 = trace(cov_D * cov_D);
r1111 = sum(cov_cell_D(:) .^ 2);

normalized_a21 = 0.25 * (8 * t1110 - 6 * t1111 + ...
    2 * t1221 - 4 * t0110 + t0000 - ...
    2 * t1100 + t1122) + ...
    s0000 - 2 * s0111 - s1221 + 2 * s1111 + ...
    0.5 * (r0000 - r1111);

% A2 = normalized_a21;
% A2 = s0000 - 2 * s0111 - s1221 + 2 * s1111;
% A2 = s0000;
% 'df'
% return;


%% normalized_A22
mean_cell_e = zeros(1, num_dic);
D_c_I_e_c = zeros(dim, num_dic);
for c = 1 : num_dic
    mean_cell_e(c) = mean(cell_e{c});
    D_c_I_e_c(:, c) = cell_D{c} * cell_e{c} / size(cell_D{c}, 2);
end
sum_e = sum(mean_cell_e);

a22_000 = mag_mean_D * sum_e;
a22_011 = sum_D' * sum(bsxfun(@times, mean_cell_D, mean_cell_e), 2);
a22_111 = sum(magnitude_mean_cell_D .* mean_cell_e);
a22_110 = sum_magnitude_mean_cell_D * sum_e;
a22_01_1 = sum_D' * sum(D_c_I_e_c, 2);
a22_11_1 = sum(sum(mean_cell_D .* D_c_I_e_c));
normalized_a22 = a22_000 - 2 * a22_011 + 2 * a22_111 - ...
    a22_110 + 2 * a22_01_1 - 2 * a22_11_1;

% A2 = normalized_a22;
% return;


%% normalized_a23
a23_1_1 = 0;
for c = 1 : num_dic
    a23_1_1 = a23_1_1 + cell_e{c}' * cell_e{c} / numel(cell_e{c});
end
a23_00 = sum_e * sum_e;
a23_11 = sum(mean_cell_e .^ 2);
normalized_a23 = a23_1_1 + a23_00 - a23_11;

%% A_2
A2 = normalized_a21 - normalized_a22 + normalized_a23;

% A2 = normalized_a22;
% 'd'


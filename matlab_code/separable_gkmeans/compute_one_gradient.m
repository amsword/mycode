function [gradient_d, gradient_e] = compute_one_gradient(cell_D, cell_e, idx)
% cell_D = original_D;
% cell_e = original_e;

dim = size(cell_D{1}, 1);
num_dic = numel(cell_D);

mean_cell_D = zeros(dim, num_dic);

for c = 1 : num_dic
    mean_cell_D(:, c) = mean(cell_D{c}, 2);
end
sum_D = sum(mean_cell_D, 2);

cov_cell_D = zeros(dim, dim, num_dic);
pcov_mean = zeros(dim, num_dic);
% debug_cov_cell_D = cov_cell_D;
% debug_cov_cell_D2 = debug_cov_cell_D;
for c = 1 : num_dic
    sub_pcov_D = cell_D{c} * cell_D{c}' / size(cell_D{c}, 2);
    sub_cov_D = sub_pcov_D - mean_cell_D(:, c) * mean_cell_D(:, c)';
    cov_cell_D(:, :, c) = sub_cov_D;
    
    pcov_mean(:, c) = sub_pcov_D * mean_cell_D(:, c);
    
%     debug_cov_cell_D(:, :, c) = sub_pcov_D;
%     debug_cov_cell_D2(:, :, c) = mean_cell_D(:, c) * mean_cell_D(:, c)';
    
end
cov_D = sum(cov_cell_D, 3);
% debug_cov_D = sum(debug_cov_cell_D, 3);
% debug_cov_D2 = sum(debug_cov_cell_D2, 3);

subtract_sum_D = sum_D - mean_cell_D(:, idx);
K = size(cell_D{idx}, 2);

coef = subtract_sum_D * subtract_sum_D' + ...
    (cov_D - cov_cell_D(:, :, idx));
gradient_linear = coef * 2 * cell_D{idx} / K;

%%
const1 = (subtract_sum_D' * subtract_sum_D) * subtract_sum_D * ones(1, K) / K;

magnitude_mean_cell_D = sum(mean_cell_D .^ 2, 1);
weighted_mean_cell_D = bsxfun(@times, mean_cell_D, magnitude_mean_cell_D);
sumed_weighted = sum(weighted_mean_cell_D, 2);
const2 = (sumed_weighted - weighted_mean_cell_D(:, idx)) * ones(1, K) / K;

sum_magnitude_mean_cell_D = sum(magnitude_mean_cell_D);
const3 = (sum_magnitude_mean_cell_D - magnitude_mean_cell_D(idx)) * ...
    subtract_sum_D * ones(1, K) / K;

const4 = (cov_D - cov_cell_D(:, :, idx)) * subtract_sum_D * ones(1, K) / K;

const5 = (sum(pcov_mean, 2) - pcov_mean(:, idx)) * ones(1, K) / K;

cross_cov = zeros(dim, num_dic);
mean_cell_e = zeros(1, num_dic);
for c = 1 : num_dic
    mean_cell_e(c) = mean(cell_e{c});
    cross_cov(:, c) = cell_D{c} * cell_e{c} / numel(cell_e{c}) - mean_cell_D(:, c) * mean_cell_e(c);
end
sum_cross_cov = sum(cross_cov, 2);
const6 = (sum_cross_cov - cross_cov(:, idx)) * ones(1, K) / K;

sum_mean_cell_e = sum(mean_cell_e);

const7 = subtract_sum_D * cell_e{idx}' / K;
const8 = subtract_sum_D * (sum_mean_cell_e - mean_cell_e(idx)) * ones(1, K) / K;

gradient_constant = const1 + 2 * const2 - const3 + 2 * const4 - 2 * const5 - ...
    2 * const6 - 2 * const7 - 2 * const8;


gradient_d = gradient_constant + gradient_linear;

%%
ge_1 = cell_e{idx} / K;
ge_2 = (sum_mean_cell_e - mean_cell_e(idx)) * ones(K, 1) / K;
ge_3 = (subtract_sum_D' * subtract_sum_D) * ones(K, 1) / K;
ge_4 = (sum_magnitude_mean_cell_D - magnitude_mean_cell_D(idx)) * ones(K, 1) / K;
ge_5 = cell_D{idx}' * subtract_sum_D / K;

gradient_e = ge_1 * 2 + 2 * ge_2 - ge_3 + ge_4 - 2 * ge_5;
% gradient_e =  ge_3 - ge_4 + 2 * ge_5;
return;


function [W, sw, sb] = lda(X, label, reduced_dim)
% every column of X is a feature,
% label: 1 : k

[dim N] = size(X);

num_center = max(label);
cluster_center = zeros(dim, num_center);
cluster_size = zeros(1, num_center);

for i = 1 : N
    idx_center = label(i);
    cluster_size(idx_center) = cluster_size(idx_center) + 1;
    cluster_center(:, idx_center) = cluster_center(:, idx_center) + X(:, i);
end
cluster_center = bsxfun(@rdivide, cluster_center, cluster_size);

diff = X - cluster_center(:, label);
sw = diff * diff';

center = mean(X, 2);
sb = zeros(dim, dim);
for i = 1 : numel(num_center)
    diff = cluster_center(:, i) - center;
    sb = sb + cluster_size(i) * (diff * diff');
end

[V, D] = eig(sb, sw);
W = V(:, end - reduced_dim + 1 : end);
x = load(src_file.idx_val_learn_in_train);

idx_val_in_training = x.idx_val_in_training;
idx_learn_in_training = x.idx_learn_in_training;
%%
x = read_mat('assignment_train', 'int32');

assign_val = x(idx_val_in_training, :);
assign_learn = x(idx_learn_in_training, :);

save_mat(assign_val, 'assignment_val_in_train', 'int32');
save_mat(assign_learn, 'assignment_learn_in_train', 'int32');
%%
dir_curr = [pwd '\'];

beta = 0;
coef = 1;
topk_used = 100;

lambda = 0;
alpha = 0;
max_iter = 100;
num_center = 1024;

str_in_dist_prefix = [dir_curr 'origin_table_'];

num_thread = 60;

for alpha = [0]
    for lambda = [10^5]
        
        str_out_prefix = ['origin_table_' 'out_' ...
            'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_'];
        
        dir_curr = [pwd '\'];
        
        str_prefix = ['no_w\' str_out_prefix];
        str_assign_train = [dir_curr 'assignment_learn_in_train'];
        str_assign_test = [dir_curr 'assignment_val_in_train'];
        str_table_pre = [dir_curr str_prefix];
        str_gnd = gnd_file.GndValidationInLearn;
        str_out = [dir_curr str_prefix 'map_validation_perf'];
        
        exeTestmAP_top1percent(str_assign_train, str_assign_test, str_table_pre, ...
            str_gnd, str_out)
        
    end
end
%%

x = dir('*la*_al*perf_1_10_50_100_400_1000_10000_20000');

dir_curr = [pwd '\'];

%
i = 1; 
%
for i = 1 : numel(x)
    [num2str(i) '/' num2str(numel(x))]
    file_name = x(i).name
    idx = strfind(file_name, 'perf_1_10_50_100_400_1000_10000_20000');
    pre_fix = file_name(1 : idx - 1);
    
    assert(exist([pre_fix '0'], 'file') ~= 0);
    dir_curr = [pwd '\'];
    
    str_prefix = [pre_fix];
    str_assign_train = [dir_curr 'assignment_learn_in_train'];
    str_assign_test = [dir_curr 'assignment_val_in_train'];
    str_table_pre = [dir_curr str_prefix];
    str_gnd = gnd_file.GndValidationInLearn;
    str_gnd = 'C:\Users\v-jianfw\Desktop\t\gist1m3\GndValidationLearn.double.bin';
    str_out = [dir_curr str_prefix 'map_validation_perf'];
    exeTestmAP_top1percent(str_assign_train, str_assign_test, str_table_pre, ...
        str_gnd, str_out)
end

%%
dir_curr = [pwd '\'];

pre_fix = 'origin_table_out_la100_al0_be0_co1_all_non_zeros_';

for iter = 0 : 99
    iter
    str_prefix = [pre_fix num2str(iter)];
    str_assign_train = [dir_curr 'assignment_learn_in_train'];
    str_assign_test = [dir_curr 'assignment_val_in_train'];
    str_table_pre = [dir_curr 'no_w\' str_prefix];
    %     str_gnd = gnd_file.GndValidationInLearn;
    str_gnd = 'C:\Users\v-jianfw\Desktop\t\sift1m3\GndValidationLearn.double.bin';
    str_out = [dir_curr 'no_w\' str_prefix 'map_validation_perf'];
    exeTestmAP_top1percent(str_assign_train, str_assign_test, str_table_pre, ...
        str_gnd, str_out)
end


dir_curr = [pwd '\'];

pre_fix = 'origin_table_out_la100_al0_be0_co1_all_zeros_';

for iter = 0 : 99
    iter
    str_prefix = [pre_fix num2str(iter)];
    str_assign_train = [dir_curr 'assignment_learn_in_train'];
    str_assign_test = [dir_curr 'assignment_val_in_train'];
    str_table_pre = [dir_curr 'no_w\' str_prefix];
    %     str_gnd = gnd_file.GndValidationInLearn;
    str_gnd = 'C:\Users\v-jianfw\Desktop\t\sift1m3\GndValidationLearn.double.bin';
    str_out = [dir_curr 'no_w\' str_prefix 'map_validation_perf'];
    exeTestmAP_top1percent(str_assign_train, str_assign_test, str_table_pre, ...
        str_gnd, str_out)
end




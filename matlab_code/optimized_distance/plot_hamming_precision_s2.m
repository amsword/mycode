result_dir,
code_length,
num_partition,

b = strcmp(encoding_scheme, 'ITQ') || ...
    strcmp(encoding_scheme, 'LSH') || ...
    strcmp(encoding_scheme, 'KSH') || ...
    strcmp(encoding_scheme, 'SH');
assert(b);
%
topks = [];
[r_main, topks1] =  load_perf(result_dir, ...
    encoding_scheme, ...
    code_length, ...
    num_partition);
topks = topks1;

[r_lei] =  load_weighted_lei(...
    result_dir, ...
    encoding_scheme, ...
    code_length);

if strcmp(encoding_scheme, 'ITQ')
    r_xiao = load_xiao(result_dir, ...
        encoding_scheme, code_length);
end

%
% for k = 1 : numel(topks)
k = numel(topks) - 1;
figure;
hold on;

plot(r_main{4}.precision(:, k), ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_main{3}.precision(:, k), ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

plot(r_lei{1}.precision(:, k), ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

if strcmp(encoding_scheme, 'ITQ')
    plot(r_xiao{1}.precision(:, k), ...
        'Color', curve_colors(5, :), ...
        'LineWidth', 2, 'LineStyle', '--');
end

plot(r_main{1}.precision(:, k), ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Precision', 'FontSize', 14);

if strcmp(encoding_scheme, 'ITQ')
    legend(...
        [encoding_scheme '-' name_oa], ...
        [encoding_scheme '-' name_os], ...
        [encoding_scheme '-WhRank'], ...
        [encoding_scheme '-QsRank'], ...
        [encoding_scheme '-HM'], ...
        'Location', 'Best');
elseif strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'KSH') || ...
        strcmp(encoding_scheme, 'SH')
    legend(...
        [encoding_scheme '-' name_oa], ...
        [encoding_scheme '-' name_os], ...
        [encoding_scheme '-WhRank'], ...
        [encoding_scheme '-HM'], ...
        'Location', 'Best');
else
    error('fdfs');
end

if strcmp(type, 'MNISTE')
    switch code_length
        case 32
            if num_partition == 3
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.2, 0.9]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.3, 1]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.39, 1]);
                end
            elseif num_partition == 4
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.32, 0.75]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.3, 0.95]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.39, 1]);
                end
            else
                error('dfsd');
            end
        case 64
            if num_partition == 6
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.3, 1]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.35, 1]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.41, 1]);
                end
            elseif num_partition == 8
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.45, 0.95]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.35, 1]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.41, 1]);
                end
            end
            
    end
    xlim([0, 2000]);
elseif strcmp(type, 'SIFT1M3')
    switch code_length
        case 32
            if num_partition == 3
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.32, 0.8]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.52, 0.9]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.43, 0.75]);
                end
            elseif num_partition == 4
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.32, 0.75]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.52, 0.9]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.43, 0.72]);
                end
            else
                error('dfsd');
            end
        case 64
            if num_partition == 6
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.45, 0.95]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.65, 1]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.53, 0.9]);
                end
            elseif num_partition == 8
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.45, 0.95]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.65, 1]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.53, 0.87]);
                end
            end
            
    end
elseif strcmp(type, 'GIST1M3')
    switch code_length
        case 32
            if num_partition == 3
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.15, 0.5]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.25, 0.65]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.25, 0.65]);
                end
            elseif num_partition == 4
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.15, 0.45]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.25, 0.6]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.25, 0.6]);    
                end
            else
                error('dfsd');
            end
        case 64
            if num_partition == 6
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.2, 0.55]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.3, 0.7]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.3, 0.8]);
                end
            elseif num_partition == 8
                if strcmp(encoding_scheme, 'LSH')
                    ylim([0.2, 0.5]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([0.3, 0.65]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([0.3, 0.8]);
                end
            end
            
    end
end

saveas(gca, ...
    [save_dir 's_pre_' ...
    num2str(topks(k)) '_' ...
    encoding_scheme '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
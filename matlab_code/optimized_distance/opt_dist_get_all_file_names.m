
if strcmp(type, 'GIST1M3') || ...
       strcmp(type, 'SIFT1M3') || ...
       strcmp(type, 'MNIST') || ...
       strcmp(type, 'MNISTE') 
    data_format = 'double';
elseif strcmp(type, 'Tiny80M')
    data_format = 'float';
else
    error('dfsdf');
end

data_parent_folder = get_data_parent_folder();
working_dir = ...
    [data_parent_folder type '\working_opt_distance'];
data_dir = [data_parent_folder type '\data\'];

str_training = [data_dir 'c_data\OriginXtraining.' data_format '.bin'];
str_test = [data_dir 'c_data\OriginXTest.' data_format '.bin']; 
if strcmp(type, 'Labelme')
    str_base = str_training;
else
    str_base = [data_dir 'c_data\OriginXBase.' data_format '.bin'];
end

str_gnd_query = [data_dir 'Label.Test.int32.bin'];
str_gnd_base = [data_dir 'Label.Train.int32.bin'];

str_gnd = [data_dir 'STestBase.' data_format '.bin'];
str_gnd_train_train = [data_dir 'STrainTrain.' data_format '.bin'];

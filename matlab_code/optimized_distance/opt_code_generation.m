% hash function generation
if strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'GIST1M3')
    if strcmp(encoding_scheme, 'PQ')
        X = Xtraining;
    else
        X = Xbase;
    end
elseif strcmp(type, 'Tiny80M') || ...
        strcmp(type, 'MNIST') || ...
        strcmp(type, 'MNISTE') 
    X = double(Xtraining);
else
    error('dfs');
end
size(X)

%% get hash functions
if strcmp(encoding_scheme, 'LSH')
    if strcmp(type, 'SIFT1M3') || ...
            strcmp(type, 'GIST1M3')
        rng(1);
    elseif strcmp(type, 'Tiny80M')
        rng(0);
    elseif strcmp(type, 'MNISTE')
        if code_length == 32
            rng(2);
        else
            rng(1);
        end
    else
        error('dfds');
    end
    [W] = train_lsh2(X, code_length);
    
elseif strcmp(encoding_scheme, 'ITQ')
    W = trainITQ2(X, code_length);
    
elseif strcmp(encoding_scheme, 'PQ');
    assert(mod(code_length, 8) == 0);

    all_dic_size = 2 .^ split_code_length(code_length, num_pq_partitions);
    all_dim = split_code_length(size(X, 1), num_pq_partitions);
    
    [R, all_D] = ...
        ck_means(X, all_dim, all_dic_size, 100,...
        [], [], 0);

elseif strcmp(encoding_scheme, 'ck-means')
    assert(mod(code_length, 8) == 0);
    pq_partition = code_length / 8;
    all_dic_size = zeros(1, pq_partition);
    all_dic_size(:) = 256;
    all_dim = zeros(1, pq_partition);
    all_dim(:) = size(X, 1) / pq_partition;
    assert(mod(size(X, 1), pq_partition) == 0);
    
    [R, all_D] = ...
        ck_means(X, all_dim, all_dic_size, 100,...
        [], [], 1);
elseif strcmp(encoding_scheme, 'SH')
    rng(2);
    SHparam = jf_train_sh(X, code_length);
    hash_func = SHparam;
elseif strcmp(encoding_scheme, 'KSH')
    rng(2);
    if ~exist('nn2', 'file')
        para = prepare_ksh(Xtraining, str_training);
        save('para_ksh', 'para');
    else
        load('para_ksh', 'para');
    end
    hash_func = train_ksh(para, code_length);
else
    error('dfsd');
end

%% encoding
if strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    X = Xtest;
    c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
    query_binary_code = compactbit(c);
    
    if strcmp(type, 'SIFT1M3') || ...
            strcmp(type, 'GIST1M3') || ...
            strcmp(type, 'MNIST') || ...
            strcmp(type, 'MNISTE')
        X = Xbase;
        c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
        base_binary_code = compactbit(c);
        
    elseif strcmp(type, 'Tiny80M')
        Nbase = 79292017;
        
        base_binary_code = zeros(...
            ceil(code_length / 8), Nbase, 'uint8');
        
        batch_size = 1000000;
        batch_num = ceil(Nbase / batch_size);
        
        for idx_batch = 1 : batch_num
            point_start = ...
                (idx_batch - 1) * batch_size + 1;
            if point_start  > Nbase
                break;
            end
            
            point_end = point_start + batch_size - 1;
            if point_end > Nbase
                point_end = Nbase;
            end
            
            [num2str(idx_batch) '/' num2str(batch_num) ': ' ...
                num2str(point_start) '/' num2str(point_end)]
            
%             X = read_mat2(str_base, 'float', ...
%                 point_start, point_end);
            X = Xbase(:, point_start : point_end);
            c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
            base_binary_code(:, point_start : point_end) = compactbit(c);
        end
        clear X c
        
    end
    % code of the training data
    %     X = Xtraining;
    %     c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
    %     binary_code = compactbit(c);
elseif strcmp(encoding_scheme, 'PQ') || ...
        strcmp(encoding_scheme, 'ck-means')
    X = double(Xtest);
    query_binary_code = ck_encoding(X, R, all_D);
    
    if strcmp(type, 'SIFT1M3') || ...
            strcmp(type, 'GIST1M3') || ...
            strcmp(type, 'MNIST') || ...
            strcmp(type, 'MNISTE')
        %
        X = Xbase;
        base_binary_code = ck_encoding(X, R, all_D);
    elseif strcmp(type, 'Tiny80M')
        Nbase = 79292017;
        
        base_binary_code = zeros(...
            ceil(code_length / 8), Nbase, 'uint8');
        
        batch_size = 1000000;
        batch_num = ceil(Nbase / batch_size);
        
        for idx_batch = 1 : batch_num
            point_start = (idx_batch - 1) * batch_size + 1;
            if point_start  > Nbase
                break;
            end
            
            point_end = point_start + batch_size - 1;
            if point_end > Nbase
                point_end = Nbase;
            end
            X = read_mat2(str_base, 'float', ...
                point_start, point_end);
            
            base_binary_code(:, point_start : point_end) = ...
                ck_encoding(X, R, all_D);
        end
        clear X c
    end
elseif strcmp(encoding_scheme, 'SH')
    X = double(Xtest);
    query_binary_code = hash_code_SH(X', hash_func);
    query_binary_code = compactbit(query_binary_code);
    
    X = double(Xbase);
    base_binary_code = hash_code_SH(X', hash_func);
    base_binary_code = compactbit(base_binary_code);
elseif strcmp(encoding_scheme, 'KSH')
    X = double(Xtest);
    query_binary_code = ksh_encoding(X, hash_func);
    query_binary_code = compactbit(query_binary_code);
    
    X = double(Xbase);
    base_binary_code = ksh_encoding(X, hash_func);
    base_binary_code = compactbit(base_binary_code);
else
    error('df');
end
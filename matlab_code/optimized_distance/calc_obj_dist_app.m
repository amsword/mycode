function y = calc_obj_dist_app(Xtraining, all_assignment, table, num_centers)
%%

num_point = size(Xtraining, 2);
table = read_multi_mat(2, 'origin_table_out_');
batch_size = 100;
batch_num = ceil(num_point / batch_size);

s = 0;
for batch_idx = 1 : batch_num
    
    [num2str(batch_idx) '/' num2str(batch_num)]
    
    
    batch_start = (batch_idx - 1) * batch_size + 1;
    batch_end = batch_idx * batch_size;
    batch_end = min(batch_end, num_point);
    
    dist_true = sqdist(Xtraining(:, batch_start : batch_end), Xtraining);
    
    dist_app = zeros(batch_end - batch_start + 1, num_point);
    
    for i = 1 : sub_dim_num
        idx = bsxfun(@plus, all_assignment(i, batch_start : batch_end)', (all_assignment(i, :) - 1) * num_centers(i));
        dist_app= dist_app + table{i}(idx);
    end
    v = (dist_true(:) - dist_app(:)) .^ 2;
    s = s + sum(v);
end
function best_idx = find_best_idx(best, best_selected_col)

best_v = -1;
for i = 1 : numel(best)
    v = jf_calc_map(1 : 10^4, best{i}(:, best_selected_col));
    if v > best_v
        best_v = v;
        best_idx = i;
    end
end

if best_v == -1
    best_idx = -1;
end
result_dir,
code_length,
num_partition,

b = strcmp(encoding_scheme, 'ITQ') || ...
    strcmp(encoding_scheme, 'LSH') || ...
    strcmp(encoding_scheme, 'SH') || ...
    strcmp(encoding_scheme, 'KSH');
assert(b);
%
topks = [];
[r_main, topks1] =  load_perf(result_dir, ...
    encoding_scheme, ...
    code_length, ...
    num_partition);
topks = topks1;

[r_lei] =  load_weighted_lei(...
    result_dir, ...
    encoding_scheme, ...
    code_length);

if strcmp(encoding_scheme, 'ITQ')
    [r_xiao] = load_xiao(result_dir, ...
        encoding_scheme, code_length);
end

%
% for k = 1 : numel(topks)
k = numel(topks) - 1;
figure;
hold on;

plot(r_main{4}.dist_ratio, ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_main{3}.dist_ratio, ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

plot(r_lei{1}.dist_ratio, ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

if strcmp(encoding_scheme, 'ITQ')
    plot(r_xiao{1}.dist_ratio, ...
        'Color', curve_colors(5, :), ...
        'LineWidth', 2, 'LineStyle', '--');
end

plot(r_main{1}.dist_ratio, ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Mean average ratio', 'FontSize', 14);



location = 'Best';
if strcmp(type, 'SIFT1M3')
    if strcmp(encoding_scheme, 'LSH')
        switch code_length
            case 32
                if num_partition == 3
                     ylim([1.1, 1.35]);
                elseif num_partition == 4
                    ylim([1.12, 1.35]);
                end
            case 64
                if num_partition == 6
                    ylim([1.05, 1.25]);
                elseif num_partition == 7
                    ylim([1.04, 1.3]);
                elseif num_partition == 8
                    ylim([1.07, 1.25]);
                end
        end
    elseif strcmp(encoding_scheme, 'ITQ')
        switch code_length
            case 32
                if num_partition == 3
                    ylim([1.07, 1.22]);
                elseif num_partition == 4
                    ylim([1.08, 1.2]);
                end
            case 64
                if num_partition == 6
                    ylim([1.04, 1.14]);
                elseif num_partition == 7
                    ylim([1.04, 1.3]);
                elseif num_partition == 8
                    ylim([1.04, 1.2]);
                end
        end
    elseif strcmp(encoding_scheme, 'KSH')
        switch code_length
            case 32
                if num_partition == 3
                    ylim([1.1, 1.3]);
                elseif num_partition == 4
                    ylim([1.1, 1.3]);
                end
            case 64
                if num_partition == 6
                    ylim([1.07, 1.22]);
                elseif num_partition == 7
                    ylim([1.04, 1.3]);
                elseif num_partition == 8
                    ylim([1.08, 1.22]);
                end
        end
    end
    
elseif strcmp(type, 'GIST1M3')
    switch code_length
        case 32
            if num_partition == 3
                if strcmp(encoding_scheme, 'LSH')
                    ylim([1.13, 1.35]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.1, 1.25]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([1.07, 1.3]);
                end
            elseif num_partition == 4
                if strcmp(encoding_scheme, 'LSH')
                    ylim([1.14, 1.35]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.1, 1.25]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([1.07, 1.3]);
                end
            end
        case 64
            if num_partition == 6
                if strcmp(encoding_scheme, 'LSH')
                    ylim([1.11, 1.3]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.09, 1.25]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([1.05, 1.2]);
                end
            elseif num_partition == 7
                ylim([1.09, 1.3]);
            elseif num_partition == 8
                if strcmp(encoding_scheme, 'LSH')
                    ylim([1.12, 1.3]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.1, 1.25]);
                elseif strcmp(encoding_scheme, 'KSH')
                    ylim([1.05, 1.2]);
                end
            end
        otherwise
            %             ylim([0.1, 0.7]);
    end
    location = 'NorthEast';
elseif strcmp(type, 'MNISTE')
    switch code_length
        case 32
            if num_partition == 3
                if strcmp(encoding_scheme, 'KSH')
                    ylim([1.03, 1.25]);
                elseif strcmp(encoding_scheme, 'LSH')
                    ylim([1.08, 1.4]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.045, 1.25]);
                else
                    ylim([1, 1.5]);
                end
            elseif num_partition == 4
                if strcmp(encoding_scheme, 'KSH')
                    ylim([1.05, 1.25]);
                elseif strcmp(encoding_scheme, 'LSH')
                    ylim([1.1, 1.4]);
                elseif strcmp(encoding_scheme, 'ITQ')
                     ylim([1.05, 1.3]);
                else
                    ylim([1, 1.5]);
                end
            end
        case 64
            if num_partition == 6
                if strcmp(encoding_scheme, 'KSH')
                    ylim([1.02, 1.2]);
                elseif strcmp(encoding_scheme, 'LSH')
                    ylim([1.04, 1.25]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.02, 1.15]);
                else
                    ylim([1, 1.3]);
                end
            elseif num_partition == 7
                ylim([1, 1.4]);
            elseif num_partition == 8
                if strcmp(encoding_scheme, 'KSH')
                    ylim([1.02, 1.2]);
                elseif strcmp(encoding_scheme, 'LSH')
                    ylim([1.05, 1.25]);
                elseif strcmp(encoding_scheme, 'ITQ')
                    ylim([1.02, 1.15]);
                else
                    ylim([1, 1.35]);
                end
            end
    end
else
    error('dfsd');
end


if strcmp(encoding_scheme, 'ITQ')
    legend(...
        [encoding_scheme '-' name_oa], ...
        [encoding_scheme '-' name_os], ...
        [encoding_scheme '-WhRank'], ...
        [encoding_scheme '-QsRank'], ...
        [encoding_scheme '-HM'], ...
        'Location', location);
elseif strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'SH') || ...
        strcmp(encoding_scheme, 'KSH')
    legend(...
        [encoding_scheme '-' name_oa], ...
        [encoding_scheme '-' name_os], ...
        [encoding_scheme '-WhRank'], ...
        [encoding_scheme '-HM'], ...
        'Location', location);
else
    error('fdfs');
end



saveas(gca, ...
    [save_dir 's_dist_' ...
    encoding_scheme '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
function exeWeightedOptDistanceEachIter(...
    str_training, ...
    is_load_sym_train_train, ...
    beta, ...
    coef, ...
    topk_used, ...
    str_train_train_nn, ...
    str_assignment, ...
    is_load_inter_data, ...
    str_inter_data, ...
    str_in_dist_prefix, ...
    str_out_put_prefix, ...
    lambda, ...
    alpha, ...
    max_iter, ...
    num_center, ...
    num_thread, ...
    is_zeros)

cmd = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\HashCode\Temps_lookup_table_opt\WeightedOptCCDistance_save_each_result.exe ';
cmd = [cmd str_training ' '];
cmd = [cmd num2str(is_load_sym_train_train) ' '];
cmd = [cmd num2str(beta) ' '];
cmd = [cmd num2str(coef) ' '];
cmd = [cmd num2str(topk_used) ' '];
cmd = [cmd str_train_train_nn ' '];
cmd = [cmd str_assignment ' '];
cmd = [cmd num2str(is_load_inter_data) ' '];
cmd = [cmd str_inter_data ' '];
cmd = [cmd str_in_dist_prefix ' '];
cmd = [cmd str_out_put_prefix ' '];
cmd = [cmd num2str(lambda) ' '];
cmd = [cmd num2str(alpha) ' '];
cmd = [cmd num2str(max_iter) ' '];
cmd = [cmd num2str(num_center) ' '];
cmd = [cmd num2str(num_thread) ' '];
cmd = [cmd num2str(is_zeros)];

system(cmd);
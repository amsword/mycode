result_dir,
code_length,
num_partition,


[r_pq, topks1] =  load_perf(result_dir, ...
    'PQ', ...
    code_length, ...
    num_partition);
topks = topks1;

%
% for k = 1 : numel(topks)
k = numel(topks) - 1;
figure;
hold on;

plot(r_pq{5}.precision(:, k), ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_pq{4}.precision(:, k), ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

plot(r_pq{2}.precision(:, k), ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

plot(r_pq{1}.precision(:, k), ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Precision', 'FontSize', 14);
legend(...
    ['PQ-' name_oa], ...
    ['PQ-' name_os], ...
    ['PQ-AD'], ...
    ['PQ-SD'], ...
    'Location', 'Best');


if strcmp(type, 'MNISTE')
    if code_length == 32
        if num_partition == 3
            ylim([0.45, 1]);
        elseif num_partition == 4
        end
    elseif code_length == 64
        if num_partition == 6
        elseif num_partition == 8
            ylim([0.48, 1]);
        end
    end
    
    xlim([1, 2000]);
elseif strcmp(type, 'SIFT1M3')
    if code_length == 64
        if num_partition == 8
            ylim([0.8, 1]);
        end
    end
elseif strcmp(type, 'GIST1M3')
    if code_length == 32
        if num_partition == 3
            ylim([0.43, 0.92]);
        elseif num_partition == 4
            ylim([0.4, 0.85]);
        end
    elseif code_length == 64
        if num_partition == 6
        elseif num_partition == 8
            ylim([0.45, 0.95]);
        end
    end
else
    error('df');
end

saveas(gca, ...
    [save_dir 's_pre_' ...
    num2str(topks(k)) '_' ...
    'PQ' '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
% end


function [D, obj] = opt_diagonal2(...
       E, G)
%
num_partition = size(E, 1);

assert(size(E, 2) == num_partition);
assert(size(G, 1) == num_partition);
assert(size(G, 2) == num_partition);

max_iter = 1000;

D = cell(num_partition, 1);

for i = 1 : num_partition
    num_bucket = size(E{i, i}, 1);

    D{i} = zeros(num_bucket, num_bucket);
end

clear obj;
tic;
for iter = 1 : max_iter
    for u = 1 : num_partition
        right = 0;
        for t = 1 : num_partition
            if t == u
                continue;
            end
            right = right + E{u, t} * D{t} * E{u, t}';
        end
        right = G{u, u} - right;
        
        left = E{u, u};

        s = size(left, 1);
%         pinv_left = pinv(left);

%         D{u} = pinv_left * right * pinv_left;
          D{u} = (left + 10^-5 * eye(s, s)) \ right / (left + 10^-5 * eye(s, s));
    end
    obj(iter) = loss_approximate_errors(E, G, D);

    if iter > 1 ...
            && abs(obj(iter) - obj(iter - 1)) < 10 ^ -5 ...
            && iter > 50
        break;
    end
    
    if toc > 60 * 5
        break;
    end
end
function y = loss_approximate_errors(E, G, D)

matE = ConstructMat(E);
matG = ConstructMat(G);
if numel(D) == size(E, 1)
    matD = ConstructDiagonal(D);
    
    A = matD * matE;
    y = 0.5 * trace(A * A) - trace(matG * matD');
else
    A = D * matE;
    y = 0.5 * trace(A * A) - trace(matG * D');
end

end


function matD = ConstructDiagonal(D)
num_partition = numel(D);

total_bucket = 0;
for i = 1 : num_partition
    total_bucket = total_bucket + size(D{i}, 1);
    assert(size(D{i}, 1) == size(D{i}, 2));
end 

matD = zeros(total_bucket, total_bucket);

idx = 1;
for i = 1 : num_partition
    rows = size(D{i}, 1);
    matD(idx : idx + rows - 1, ...
            idx : idx + rows - 1) = D{i};
    idx = idx + rows;
end

end
baseline = read_mat('origin_table_perf_1_10_50_100_400_1000_10000_20000', 'double');

%%
idx = 1;

dir_curr = [pwd '\'];

clear best;
clear map_v

all_beta = [0.1, 1, 10, 100, 1000];
all_coef = [0.01, 0.1, 1, 10, 100];
% all_topk_used = [10 50 100 200 400];
all_topk_used = [100];
% all_lambda = [0, 1, 10, 100, 1000];
all_lambda = [1000];

map_v = cell(size(baseline, 2), 1);
for k = 1 : size(baseline, 2)
    map_v{k} = zeros(numel(all_beta), numel(all_coef), numel(all_topk_used), numel(all_lambda));
end
best = cell(numel(all_beta), numel(all_coef), numel(all_topk_used), numel(all_lambda));
best_file_name = best;

alpha = 0;
num_valid = 0;
idx1 = 1;
for idx_beta = 1 : numel(all_beta) 
    beta = all_beta(idx_beta);
    for idx_coef = 1 : numel(all_coef)
        coef = all_coef(idx_coef);
        for idx_topk_used = 1 : numel(all_topk_used)
            topk_used = all_topk_used(idx_topk_used);
            for idx_lambda = 1 : numel(all_lambda)
                lambda = all_lambda(idx_lambda);
                

                    str_out_prefix = ['origin_table_' 'out_' ...
                        'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_tk' num2str(topk_used) '_'];

                str_out_put_prefix = [dir_curr str_out_prefix];
                file_name = [str_out_put_prefix 'perf_1_10_50_100_400_1000_10000_20000'];
                if (exist(file_name, 'file'))
                    best{idx_beta, idx_coef, idx_topk_used, idx_lambda} = read_mat(file_name, 'double');
                    for idx_topk = 1 : size(baseline, 2)
                        map_v{idx_topk}(idx_beta, idx_coef, idx_topk_used, idx_lambda) = ...
                            jf_calc_map(1 : 10^4, best{idx_beta, idx_coef, idx_topk_used, idx_lambda}(:, idx_topk));
                    end
                    best_file_name{idx_beta, idx_coef, idx_topk_used, idx_lambda} = file_name;
                    num_valid = num_valid + 1;
                end
            end
        end
    end
end

%%

% x = dir('*_al0_*perf_1_10_50_100_400_1000_10000_20000');
% x = dir('*perf_1_10_50_100_400_1000_10000_20000');
x = dir('*al*perf_1_10_50_100_400_1000_10000_20000');


baseline = read_mat('origin_table_perf_1_10_50_100_400_1000_10000_20000', 'double');

best = cell(numel(x), 1);
for i = 1 : numel(x)
    best{i} = read_mat(x(i).name, 'double');
end



%%
clc;
idx_topk = 5;
base_y = jf_calc_map(1 : 10^4, baseline(:, idx_topk));
% for idx_topk = 1 : size(baseline, 2)

idx_beta = 1 : numel(all_beta);
idx_coef = 1 : numel(all_coef);
idx_topk_used = 1 : numel(all_topk_used);
idx_lambda = 1 : numel(all_lambda);

for idx_beta = 1 : numel(all_beta);
%     for idx_coef = 1 : numel(all_coef)
        for idx_topk_used = 1 : numel(all_topk_used)
            for idx_lambda = 1 : numel(all_lambda)
 
                x = map_v{idx_topk}(idx_beta, idx_coef, idx_topk_used, idx_lambda);
                x = reshape(x, numel(x), 1);
                
                if numel(find(x > 0)) <= 1
                    continue;
                end
                x(x == 0) = [];
                figure;
                hold on;
                plot(x, '*-');
                ref_y = zeros(numel(x), 1);
                ref_y(:) = base_y;
                plot(1 : numel(x), ref_y, 'k-');
                title(['beta: ' num2str(all_beta(idx_beta(1))) '  '...
                    'coef: ' num2str(all_coef(idx_coef(1))) '   ', ...
                    ' topk_used: ' num2str(all_topk_used(idx_topk_used(1))) '   ', ...
                    'lambda: ' num2str(all_lambda(idx_lambda(1)))]);
%                 ylim([7400, 7500]);
            end
        end
%     end
end
% end


%%
idx_best_beta = -1;
idx_best_coef = -1;
idx_best_topk_used = -1;
idx_best_lambda = -1;
best_v = -1;

idx_topk = 4;

for idx_beta = 1 : numel(all_beta);
    for idx_coef = 1 : numel(all_coef)
        for idx_topk_used = 1 : numel(all_topk_used)
            for idx_lambda = 1 : numel(all_lambda)
 
                x = map_v{idx_topk}(idx_beta, idx_coef, idx_topk_used, idx_lambda);
                x = reshape(x, numel(x), 1);
                if x > best_v
                    best_v = x;
                    idx_best_beta = (idx_beta);
                    idx_best_coef = (idx_coef);
                    idx_best_topk_used = (idx_topk_used);
                    idx_best_lambda = (idx_lambda);
                end
            end
        end
    end
end

best_v

lambda = all_lambda(idx_best_lambda);
alpha = 0;
beta = all_beta(idx_best_beta);
coef = all_coef(idx_best_coef);
topk_used = all_topk_used(idx_best_topk_used);

str_out_prefix = ['origin_table_' 'out_' ...
    'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_tk' num2str(topk_used) '_'];

%%
curve_colors =  [[255 0 0] / 255; ...
    [148 30 249] / 255; ...
    [220 50 176] / 255; ...
    [20 4 229] / 255; ...
    [3 151 219] / 255; ...
    [1 189 37] / 255; ...
    [0 0 0] / 255; ...
    [80 136 170] / 255; ...
    [200 137 30] / 255; ...
    [100 70 230] / 255];

selected_x = 1 : 10^4;

%
i = 2;

all_topks = [1, 10, 50, 100, 400, 1000, 10000, 20000];

for i = 1 : size(baseline, 2)
    
    figure;
    hold on;
    %     plot(1 : 10^4, baseline(:, i), 'b');
    
    %     for k = 1 : size(best2, 1)
    best_v = -1;
    best_k = -1;
    for k = 1 : numel(best)
        if ~isempty(best{k})
            v = jf_calc_map(selected_x, best{k}(:, i));
            if v > best_v
                best_v = v;
                best_k = k;
            end
%              plot(selected_x, best{k}(selected_x, i), 'r');
             
        end
    end
    best_k
    plot(selected_x, best{best_k}(selected_x, i), 'r');
    hold on;
%     all_topks(i)
%     best{best_k}(end, i) -  baseline(end, i)
    %     plot(1 : 10^4, best{27}(:, i), 'g');
    plot(selected_x, baseline(selected_x, i), 'b');
    
    legend('Ours', 'Baseline', 'Location', 'Best');
    set(gca, 'FontSize', 16);
    grid on;
    
    title(num2str(all_topks(i)));
    continue;
    %
    figure;
    plot(1 : 10^4, best{best_k}(:, i) * all_topks(i) ./ ([1 : 10^4]'), 'r');
    hold on;
%     all_topks(i)
%     best{best_k}(end, i) -  baseline(end, i)
    %     plot(1 : 10^4, best{27}(:, i), 'g');
    plot(1 : 10^4, baseline(:, i) * all_topks(i) ./ ([1 : 10^4]'), 'b');
    
    legend('Ours', 'Baseline', 'Location', 'Best');
    set(gca, 'FontSize', 16);
    grid on;
    
    title(num2str(all_topks(i)));
    
    
    figure;
    %
    plot(best{best_k}(:, i), best{best_k}(:, i) * all_topks(i) ./ ([1 : 10^4]'), 'r');
    hold on;
%     all_topks(i)
%     best{best_k}(end, i) -  baseline(end, i)
    %     plot(1 : 10^4, best{27}(:, i), 'g');
    plot(baseline(:, i), baseline(:, i) * all_topks(i) ./ ([1 : 10^4]'), 'b');
    
    legend('Ours', 'Baseline', 'Location', 'Best');
    set(gca, 'FontSize', 16);
    grid on;
    
    title(num2str(all_topks(i)));
    
    %
    
    
    
    %     saveas(gca, [num2str(all_topks(i)) '.eps'], 'psc2');
    %     plot(1 : 10^4, best{idx_best_beta, idx_best_coef, idx_best_topk_used, idx_best_lambda}(:, i), 'k');
    
end
% pause;
% close all;

%%
exeTestTopk(dir_curr, str_out_prefix, gnd_file.STestBase);

exeTestTopk(dir_curr, 'origin_table_', gnd_file.STestBase);
%%
baseline = read_mat('origin_table_epsilon_78390_perf', 'double');
best = read_mat('origin_table_out_la0_al0_be100_co10_tk50_epsilon_78390_perf', 'double');

% baseline = read_mat('origin_table__epsilon_perf_50_100', 'double');
% best = read_mat('origin_table_out_la0_al0_be100_co10_tk50__epsilon_perf_50_100', 'double');

for i= 1 : size(baseline, 2)
    figure;
    plot(baseline(:, i), 'b');
    hold on;
    plot(best(:, i), 'r');
end

%%
baseline = read_mat('origin_table__avg_distance_ratio_perf', 'double');
% best = read_mat('origin_table_out_la0_al0_be100_co10_tk50__avg_distance_ratio_perf', 'double');

best = read_mat('no_w\origin_table_out_la1_al0_be0_co1__avg_distance_ratio_perf', 'double');


semilogx(baseline, 'b');
hold on;
semilogx(best, 'r');

xlim([100, 10^4]);



%%
dir_curr = [pwd '\'];

dir_curr = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\Data_HashCode\SIFT1M3\working_opt_dist\5_100k_train\';
%%
load_no_w;

%%


curve_colors =  [[255 0 0] / 255; ...
    [148 30 249] / 255; ...
    [220 50 176] / 255; ...
    [20 4 229] / 255; ...
    [3 151 219] / 255; ...
    [1 189 37] / 255; ...
    [0 0 0] / 255; ...
    [80 136 170] / 255; ...
    [200 137 30] / 255; ...
    [100 70 230] / 255];

for i = 1 : size(baseline, 2)
    figure;
    for j = 1 : numel(best)
        plot(1 : 10^4, best{j}(:, i), 'Color', curve_colors(j, :));
        hold on;
    end
    
    plot(1 : 10^4, baseline(:, i), 'Color', 'b');
    legend(num2str(all_lambda(1)), ...
        num2str(all_lambda(2)), ...
        num2str(all_lambda(3)), ...
        num2str(all_lambda(4)), ...
        num2str(all_lambda(5)), ...
        'Baseline', 'Location', 'Best');
    
    title(num2str(all_topks(i)));
    saveas(gca, ['no_w\' num2str(all_topks(i)) '.eps'], 'psc2');
    
end

%%
for i = 1 : size(baseline, 2)
    figure;
    plot(baseline2(:, i), 'b-');
    hold on;
    plot(best2{1}(:, i), 'r-');
    
    plot(baseline4(:, i), 'b-');
    plot(best4{1}(:, i), 'r-');
end
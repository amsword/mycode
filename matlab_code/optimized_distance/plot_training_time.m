
clear oa;
clear os hm;
k = 1;
clear all_labels;
all_num_partition = [6 : 11];
for num_partition = all_num_partition
    x = load(['result\ITQ_64_' num2str(num_partition)], ...
        'time_E', 'time_G', 'time_dense', 'time_asy');
    
    oa(k) = x.time_E + x.time_G + x.time_dense;
    os(k) = x.time_E + x.time_G + x.time_asy;
    k = k + 1;
end

oa = oa / 60;
os = os / 60;

figure;
semilogy(all_num_partition, oa, ...
    'Marker', 's', ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);
hold on;

plot(all_num_partition, os, ...
    'Marker', 'o', ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

legend(name_oa, name_os, 'Location', 'Best');
% legend('OA', 'OS', 'Location', 'Best');

grid on;
ylabel('Training time (minutes)', 'FontSize', 14);
xlabel('Number of partitions', 'FontSize', 14);
set(gca, 'FontSize', 14);

save_dir = '\\dragon\Data\Jianfeng\figs\';
save_dir = [save_dir 'SIFT1M3' '\'];
saveas(gca, [save_dir 'ITQ_64_T_TrainingTime.eps'], ...
    'psc2');
function [best_file_name, ratio_file_name, best, ratio_best, map_file, map] = load_w()

% all_file_name = dir('origin_table_out_la*_al*_be*_co*_tk*_perf_1_10_50_100_400_1000_10000_20000');
best_file_name = dir('*al*perf_1_10_50_100_400_1000_10000_20000');
ratio_file_name = dir('*al*_avg_distance_ratio_perf');
map_file = dir('*al*map*10000*20000');

% assert(numel(ratio_file_name) == numel(best_file_name));

best = cell(numel(best_file_name), 1);
ratio_best = cell(numel(ratio_file_name), 1);
map = zeros(2, numel(map_file, 1));

for i = 1 : numel(best_file_name)
    best{i} = read_mat(best_file_name(i).name, 'double');
    
end

for i = 1 : numel(ratio_file_name)
    ratio_best{i} = read_mat(ratio_file_name(i).name, 'double');
end

for i = 1 : numel(map_file)
    map(:, i) = read_mat(map_file(i).name, 'double');
end



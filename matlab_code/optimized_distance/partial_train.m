type
x = pwd;
assert(~isempty(strfind(x, type)));
pause;

sub_dim_num = 8;
sub_dim_num_center = 1024;

%% partition data
Xtraining = read_mat(src_file.c_train, 'double');

dim = size(Xtraining, 1);
num_point = size(Xtraining, 2);

assert(mod(dim, sub_dim_num) == 0);
sub_dim_length = dim / sub_dim_num;

all_train_data = cell(sub_dim_num, 1);

for i = 1 : sub_dim_num
    i
    idx_start = 1 + (i - 1) * sub_dim_length;
    idx_end = i * sub_dim_length;
    
    all_train_data{i} = Xtraining(idx_start : idx_end, :);
    
    save_mat(all_train_data{i}, ['sub_train_' num2str(i)], 'double');
end
clear Xtraining all_train_data
%% k-means

for i = 1 : sub_dim_num
    exeKMeans([pwd '\'], ['sub_train_' num2str(i)]);
end


%% read center tables
all_centers = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    c = read_mat(['sub_train_' num2str(i) '_center.double.bin'], 'double');
    all_centers{i} = c;
end
%% table of baseline
origin_table = cell(numel(sub_dim_num), 1);

for i = 1 : sub_dim_num
    c = all_centers{i};
    t = sqdist(c, c);
    origin_table{i} = t;
end
save_multi_mat(origin_table, 'origin_table_');
%% read test point

if strcmp(type, 'Tiny80M')
    Xtest = read_mat(src_file.c_test, 'float');
    num_test_point = size(Xtest, 2);
    assert(size(Xtest, 1) == dim);
    %
    idx_quantized_sub_center = zeros(sub_dim_num, num_test_point);
    for i = 1 : sub_dim_num
        idx_start = 1 + (i - 1) * sub_dim_length;
        idx_end = i * sub_dim_length;
        
        d = sqdist(Xtest(idx_start : idx_end, :), all_centers{i});
        [~, si] = min(d, [], 2);
        idx_quantized_sub_center(i, :) = si;
    end
    save_mat(idx_quantized_sub_center' - 1, 'assignment_test', 'int32');
    clear Xtest d
else
    
    Xtest = read_mat(src_file.c_test, 'double');
    num_test_point = size(Xtest, 2);
    assert(size(Xtest, 1) == dim);
    %
    idx_quantized_sub_center = zeros(sub_dim_num, num_test_point);
    for i = 1 : sub_dim_num
        idx_start = 1 + (i - 1) * sub_dim_length;
        idx_end = i * sub_dim_length;
        
        d = sqdist(Xtest(idx_start : idx_end, :), all_centers{i});
        [~, si] = min(d, [], 2);
        idx_quantized_sub_center(i, :) = si;
    end
    save_mat(idx_quantized_sub_center' - 1, 'assignment_test', 'int32');
    clear Xtest d
end

%% base-batch

if strcmp(type, 'Tiny80M')
    num_base_point = 79301017;
    base_file_pre = 'E:\v-jianfw\Data\Tiny80M\tiny_code\Base';
    all_assignment = zeros(sub_dim_num, num_base_point);
    
    base_batch_size = 1000000;
    base_batch_num = ceil(num_base_point / 1000000);
    
    for idx_base_batch = 1 : base_batch_num
        idx_base_start = (idx_base_batch - 1) * base_batch_size + 1;
        idx_base_end = idx_base_batch * base_batch_size;
        idx_base_end = min(idx_base_end, num_base_point);
        
        file_name = [base_file_pre  num2str(idx_base_start)  '_'  num2str(idx_base_end) '.float.bin'];
        
        Xbase = read_mat(file_name, 'float');
        %
        batch_assignment = zeros(sub_dim_num, idx_base_end - idx_base_start + 1);
        batch_point_size = 5000;
        batch_point_num = ceil((idx_base_end - idx_base_start + 1) / batch_point_size);
        for i = 1 : sub_dim_num
            idx_dim_start = 1 + (i - 1) * sub_dim_length;
            idx_dim_end = i * sub_dim_length;
            
            
            for idx_point_batch = 1 : batch_point_num
                idx_point_start = (idx_point_batch - 1) * batch_point_size + 1;
                idx_point_end = idx_point_batch * batch_point_size;
                idx_point_end = min(idx_point_end, idx_base_end - idx_base_start + 1);
                
                str = ['idx_base_batch: ' num2str(idx_base_batch) ' i: ' num2str(i) '. '];
                str = [str num2str(idx_point_start) ' - ' num2str(idx_point_end)]
                
                d = sqdist(Xbase(idx_dim_start : idx_dim_end, ...
                    idx_point_start : idx_point_end), all_centers{i});
                [~, si] = min(d, [], 2);
                batch_assignment(i, idx_point_start : idx_point_end) = si;
            end
        end
        all_assignment(:, idx_base_start : idx_base_end) = batch_assignment;
        
    end
    clear Xbase d
    save_mat(all_assignment' - 1, 'assignment_base', 'int32');
else
    Xbase = read_mat(src_file.c_base, 'double');
    num_base_point = size(Xbase, 2);
    % index of data base
    all_assignment = zeros(sub_dim_num, num_base_point);
    
    batch_point_size = 1000;
    batch_point_num = ceil(num_base_point / batch_point_size);
    for i = 1 : sub_dim_num
        idx_start = 1 + (i - 1) * sub_dim_length;
        idx_end = i * sub_dim_length;
        
        for idx_point_batch = 1 : batch_point_num
            idx_point_start = (idx_point_batch - 1) * batch_point_size + 1;
            idx_point_end = idx_point_batch * batch_point_size;
            idx_point_end = min(idx_point_end, num_base_point);
            [num2str(idx_point_start) ' - ' num2str(idx_point_end)]
            d = sqdist(Xbase(idx_start : idx_end, idx_point_start : idx_point_end), all_centers{i});
            [~, si] = min(d, [], 2);
            all_assignment(i, idx_point_start : idx_point_end) = si;
        end
    end
    save_mat(all_assignment' - 1, 'assignment_base', 'int32');
    clear Xbase d
end

sendmail_demo('tiny-gnd');

%%
Xtraining = read_mat(src_file.c_train, 'double');
num_train_point = size(Xtraining, 2);
% index of training data
all_assignment = zeros(sub_dim_num, num_train_point);

batch_point_size = 1000;
batch_point_num = ceil(num_train_point / batch_point_size);
for i = 1 : sub_dim_num
    idx_start = 1 + (i - 1) * sub_dim_length;
    idx_end = i * sub_dim_length;
    
    for idx_point_batch = 1 : batch_point_num
        idx_point_start = (idx_point_batch - 1) * batch_point_size + 1;
        idx_point_end = idx_point_batch * batch_point_size;
        idx_point_end = min(idx_point_end, num_train_point);
        [num2str(idx_point_start) ' - ' num2str(idx_point_end)]
        d = sqdist(Xtraining(idx_start : idx_end, idx_point_start : idx_point_end), all_centers{i});
        [~, si] = min(d, [], 2);
        all_assignment(i, idx_point_start : idx_point_end) = si;
    end
end
save_mat(all_assignment' - 1, 'assignment_train', 'int32');
clear Xtraining d
%% test base line
if strcmp(type, 'Tiny80M')
    exeTestTopk([pwd '\'], 'origin_table_', gnd_file.STestBase, 100000);
else
    exeTestTopk([pwd '\'], 'origin_table_', gnd_file.STestBase, 10000);
end

%%
run_opt_independent;
%%
dir_curr = [pwd '\'];

beta = 1;
coef = 1;
topk_used = 100;
is_load_sym_train_train = 1;
str_assignment = [dir_curr 'assignment_train'];
is_load_inter_data = false;

lambda = 0;
alpha = 0;
max_iter = 1;
num_center = 1024;

str_in_dist_prefix = [dir_curr 'origin_table_'];
str_inter_data = [str_in_dist_prefix 'inter_'];

str_out_prefix = ['origin_table_' 'out_' ...
    num2str(lambda) '_' num2str(alpha) '_' num2str(beta) '_'];

str_out_put_prefix = [dir_curr str_out_prefix];

num_thread = 20;

exeWeightedOptDistance(...
    src_file.c_train, ...
    is_load_sym_train_train, ...
    beta, ...
    coef, ...
    topk_used, ...
    gnd_file.SortedPartTrainTrain, ...
    str_assignment, ...
    is_load_inter_data, ...
    str_inter_data, ...
    str_in_dist_prefix, ...
    str_out_put_prefix, ...
    lambda, ...
    alpha, ...
    max_iter, ...
    num_center, ...
    num_thread);

exeTestTopk(dir_curr, str_out_prefix, gnd_file.STestBase);


%% no weight and only lambda is need to be adjusted
dir_curr = [pwd '\'];

beta = 0;
coef = 1;
topk_used = 100;
is_load_sym_train_train = 1;
str_assignment = [dir_curr 'assignment_train'];
is_load_inter_data = 1;

lambda = 0;
alpha = 0;
max_iter = 100;
num_center = 1024;

str_in_dist_prefix = [dir_curr 'origin_table_'];
str_inter_data = [str_in_dist_prefix 'inter_'];

num_thread = 22;

for alpha = [0]
    for lambda = [1 10 10^2 10^3 10^4]
        
        str_out_prefix = ['origin_table_' 'out_' ...
            'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_'];
        
        str_out_prefix
        
        str_out_put_prefix = [dir_curr 'no_w\' str_out_prefix];
        
        if exist([str_out_put_prefix '0'], 'file')
            continue;
        else
            ['running: ' num2str(lambda)]
        end
        
        exeWeightedOptDistance(...
            src_file.c_train, ...
            is_load_sym_train_train, ...
            beta, ...
            coef, ...
            topk_used, ...
            gnd_file.SortedPartTrainTrain, ...
            str_assignment, ...
            is_load_inter_data, ...
            str_inter_data, ...
            str_in_dist_prefix, ...
            str_out_put_prefix, ...
            lambda, ...
            alpha, ...
            max_iter, ...
            num_center, ...
            num_thread);
        
        is_load_inter_data = 1;
        exeTestTopk(dir_curr, ['no_w\' str_out_prefix], gnd_file.STestBase);
    end
end 

%% there is weight


dir_curr = [pwd '\'];

beta = 1;
coef = 1;
topk_used = 100;
is_load_sym_train_train = 1;
str_assignment = [dir_curr 'assignment_train'];
is_load_inter_data = 1;

lambda = 0;
alpha = 0;
max_iter = 100;
num_center = 1024;

str_in_dist_prefix = [dir_curr 'origin_table_'];
str_inter_data = [str_in_dist_prefix 'inter_'];

num_thread = 20;

for coef = [10]
    for beta = [1, 10, 100]
        for topk_used = [100]
            for lambda = [10]
                str_out_prefix = ['origin_table_' 'out_' ...
                    'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_tk' num2str(topk_used) '_'];
                
                str_out_put_prefix = [dir_curr str_out_prefix];

                if exist([str_out_put_prefix '0'], 'file')
                    continue;
                end
                
                exeWeightedOptDistance(...
                    src_file.c_train, ...
                    is_load_sym_train_train, ...
                    beta, ...
                    coef, ...
                    topk_used, ...
                    gnd_file.SortedPartTrainTrain, ...
                    str_assignment, ...
                    is_load_inter_data, ...
                    str_inter_data, ...
                    str_in_dist_prefix, ...
                    str_out_put_prefix, ...
                    lambda, ...
                    alpha, ...
                    max_iter, ...
                    num_center, ...
                    num_thread);
                
                is_load_sym_train_train = 1;
                
                is_load_inter_data = true;
                exeTestTopk(dir_curr, str_out_prefix, [gnd_file.STestBase]);
            end
        end
    end
end

%% smart adjust parameters

dir_curr = [pwd '\'];

num_para = 4; % beta, coef, topk_used, lambda
best_beta = 10;
best_beta_min = 1;
best_beta_max = 100;

best_coef = 10;
best_coef_min = 1;
best_coef_max = 100;

best_topk_used = 150;
best_topk_used_min = 100;
best_topk_used_max = 200;

best_lambda = 100;
best_lambda_min = 10;
best_lambda_max = 1000;

para_mat = zeros(3, num_para);
para_mat(1, 1) = best_beta_min;
para_mat(2, 1) = best_beta;
para_mat(3, 1) = best_beta_max;
para_mat(1, 2) = best_coef_min;
para_mat(2, 2) = best_coef;
para_mat(3, 2) = best_coef_max;

para_mat(1, 3) = best_lambda_min;
para_mat(2, 3) = best_lambda;
para_mat(3, 3) = best_lambda_max;

para_mat(1, 4) = best_topk_used_min;
para_mat(2, 4) = best_topk_used;
para_mat(3, 4) = best_topk_used_max;


para_perf = zeros(3, num_para);
para_perf(:) = -1;

other_para.gnd_file = gnd_file;
other_para.dir_curr = dir_curr;
other_para.src_file = src_file;

max_iter = 10;
for iter = 1 : max_iter
    for idx_para = 1 : num_para
        five_para = zeros(5, 1);
        five_perf = zeros(5, 1);
        
        five_para([1 3 5]) = para_mat(:, idx_para);
        five_perf([1 3 5]) = para_perf(:, idx_para);
        
        five_para(2) = (para_mat(1, idx_para) + para_mat(2, idx_para)) / 2;
        five_para(4) = (para_mat(2, idx_para) + para_mat(3, idx_para)) / 2;
        
        if idx_para == 4
            five_para(2) = ceil(five_para(2));
            five_para(4) = ceil(five_para(4));
        end
        
        for i = 1 : 5
            ['iter: ' num2str(iter) '...idx_para: ' num2str(idx_para) '...i: ' num2str(i)]
            para_mat
            para_perf
            curr_para = para_mat(2, :);
            curr_para(idx_para) = five_para(i);
            five_perf(i) = run_opt(curr_para, other_para);
        end
        
        [~, max_idx] = max(five_perf);
        
        para_mat(2, idx_para) = five_para(max_idx);
        para_perf(2, idx_para) = five_perf(max_idx);
        
        if max_idx == 1
            para_mat(1, idx_para) = para_mat(2, idx_para) * 0.1;
            para_perf(1, idx_para) = -1;
        else
            para_mat(1, idx_para) = five_para(max_idx - 1);
            para_perf(1, idx_para) = five_perf(max_idx - 1);
        end
        
        if max_idx == 5
            para_mat(3, idx_para) = para_mat(2, idx_para) * 10;
            para_perf(3, idx_para) = -1;
        else
            para_mat(3, idx_para) = five_para(max_idx + 1);
            para_perf(3, idx_para) = five_perf(max_idx + 1);
        end
        
        para_mat(3, 4) = min(para_mat(3, 4), 400);
        
        save;
    end
end
%%
check_result;
opt_dist_plot;
test_mean_overal_ratio;
run_test_validation_performance;
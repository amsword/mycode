function [all_map, topk] = load_all_map(result_dir, ...
    encoding_scheme, ...
    distance_type, ...
    offset)

all_map = zeros(4, 1);

all_code_length = [16, 32, 64, 128];

all_num_partition = [2, 3, 6, 14];
if strcmp(encoding_scheme, 'PQ')
    all_num_partition = [2, 4, 8, 16];
end

if strcmp(distance_type, 'HM') || ...
        strcmp(distance_type, 'OS') || ...
        strcmp(distance_type, 'OA') || ...
        strcmp(distance_type, 'SD') || ...
        strcmp(distance_type, 'AD')
    
    topk = -1;
    for i = 1 : 4
        [~, topks, map] =  ...
            load_perf(result_dir, ...
            encoding_scheme, ...
            all_code_length(i), ...
            all_num_partition(i));
        
        all_map(i) = get_map(map, distance_type, offset);
        if numel(topks) == 0
            continue;
        end
        if topk == -1
            numel(topks)
            topk = topks(end + offset);
        else
            assert(topk == topks(end + offset));
        end
    end 
elseif strcmp(distance_type, 'WhRank')
    for i = 1 : 4
        [~, map] = load_weighted_lei(...
            result_dir, ...
            encoding_scheme, ...
            all_code_length(i));
        all_map(i) = ...
            get_map(map, distance_type, offset);
    end
elseif strcmp(distance_type, 'QsRank')
    for i = 1 : 4
        [~, map] = load_xiao(...
            result_dir, ...
            encoding_scheme, ...
            all_code_length(i));
        all_map(i) = ...
            get_map(map, distance_type, offset);
    end
else
    error('dfs');
end
end

function y = get_map(map, distance_type, offset)

if strcmp(distance_type, 'HM')
    y = map{1}.map(end + offset);
    assert(numel(map) == 4);
elseif strcmp(distance_type, 'OA')
    y = map{end}.map(end + offset);
elseif strcmp(distance_type, 'OS')
    y = map{end - 1}.map(end + offset);
elseif strcmp(distance_type, 'QsRank') || ...
        strcmp(distance_type, 'WhRank')
    y = map{end}.map(end + offset);
elseif strcmp(distance_type, 'SD')
    y = map{1}.map(end + offset);
elseif strcmp(distance_type, 'AD')
    y = map{2}.map(end + offset);
else
    error('dfs');
end

end
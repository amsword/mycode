function v = run_opt(para_curr, other_para)

gnd_file = other_para.gnd_file;
dir_curr = other_para.dir_curr;
src_file = other_para.src_file;

beta = para_curr(1);
coef = para_curr(2);
topk_used = para_curr(3);
lambda = para_curr(4);

if exist([gnd_file.SortedPartTrainTrain '_' num2str(topk_used) '.double.bin'], 'file')
    is_load_sym_train_train = 1;
else
    is_load_sym_train_train = 0;
end

str_assignment = [dir_curr 'assignment_train'];

is_load_inter_data = false;

alpha = 0;
max_iter = 100;
num_center = 1024;

str_in_dist_prefix = [dir_curr 'origin_table_'];
str_inter_data = [str_in_dist_prefix 'inter_'];

str_out_prefix = ['origin_table_' 'out_' ...
    'la' num2str(lambda) '_al' num2str(alpha) ...
    '_be' num2str(beta) '_co' num2str(coef) ...
    '_tk' num2str(topk_used) '_'];

str_out_put_prefix = [dir_curr str_out_prefix];

num_thread = 20;

result_file = [dir_curr str_out_prefix 'perf_1_10_50_100_400_1000_10000_20000'];
if ~exist(result_file, 'file')
    
    exeWeightedOptDistance(...
        src_file.c_train, ...
        is_load_sym_train_train, ...
        beta, ...
        coef, ...
        topk_used, ...
        gnd_file.SortedPartTrainTrain, ...
        str_assignment, ...
        is_load_inter_data, ...
        str_inter_data, ...
        str_in_dist_prefix, ...
        str_out_put_prefix, ...
        lambda, ...
        alpha, ...
        max_iter, ...
        num_center, ...
        num_thread);
    
    exeTestTopk(dir_curr, str_out_prefix, gnd_file.STestBase);
    
end


best = read_mat(result_file, 'double');

v = jf_calc_map(1 : 10^4, best(:, 2));


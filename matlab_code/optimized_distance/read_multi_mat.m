function table = read_multi_mat(num, pre_fix)

table = cell(num, 1);
for i = 0 : (num - 1)
    file_name = [pre_fix num2str(i)];
    table{i + 1} = read_mat(file_name, 'double');
end
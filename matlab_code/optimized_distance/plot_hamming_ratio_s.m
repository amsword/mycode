result_dir,
code_length,
num_partition,

%
topks = [];
[r_lsh, topks1] =  load_perf(result_dir, ...
    'LSH', ...
    code_length, ...
    num_partition);
topks = topks1;
%
[r_itq, topks1] =  load_perf(result_dir, ...
    'ITQ', ...
    code_length, ...
    num_partition);

assert(isempty(find(topks ~= topks1)));
%
% for k = 1 : numel(topks)
k = numel(topks);
figure;
hold on;

plot(r_itq{4}.dist_ratio, ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_itq{3}.dist_ratio, ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

plot(r_itq{1}.dist_ratio, ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2);

plot(r_lsh{4}.dist_ratio, ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2, 'LineStyle', '--');

plot(r_lsh{3}.dist_ratio, ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2, 'LineStyle', '--');

plot(r_lsh{1}.dist_ratio, ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Precision', 'FontSize', 14);
legend(...
    ['ITQ-OA'], ...
    ['ITQ-OS'], ...
    ['ITQ-HM'], ...
    ['LSH-OA'], ...
    ['LSH-OS'], ...
    ['LSH-HM'], ...
    'Location', 'Best');

if strcmp(type, 'MNISTE')
     switch code_length
        case 32
            if num_partition == 3
                ylim([1, 1.5]);
            elseif num_partition == 4
                ylim([1, 1.5]);
            end
        case 64
            if num_partition == 6
                ylim([1, 1.3]);
            elseif num_partition == 7
                ylim([1, 1.4]);
            elseif num_partition == 8
                ylim([1, 1.35]);
            end
    end
elseif strcmp(type, 'SIFT1M3')
    switch code_length
        case 32
            if num_partition == 3
                ylim([1.05, 1.4]);
            elseif num_partition == 4
                ylim([1.05, 1.4]);
            end
        case 64
            if num_partition == 6
                ylim([1.04, 1.3]);
            elseif num_partition == 7
                ylim([1.04, 1.3]);
            elseif num_partition == 8
                ylim([1.04, 1.3]);
            end
    end
elseif strcmp(type, 'GIST1M3')
    switch code_length
        case 32
            if num_partition == 3
                ylim([1.1, 1.4]);
            elseif num_partition == 4
                ylim([1.1, 1.4]);
            end
        case 64
            if num_partition == 6
                ylim([1.09, 1.3]);
            elseif num_partition == 7
                ylim([1.09, 1.3]);
            elseif num_partition == 8
                ylim([1.1, 1.3]);
            end
        otherwise
%             ylim([0.1, 0.7]);
    end
else
    error('dfsd');
end
saveas(gca, ...
    [save_dir 's_dist_' ...
    'LSH_ITQ' '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
% end
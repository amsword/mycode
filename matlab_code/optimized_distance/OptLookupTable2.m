opt_dist_get_all_file_names;

cd(working_dir);

%% load original data
% if strcmp(type, 'Tiny80M')
Xtraining = read_mat(str_training, ...
    [data_format '=>' data_format]);
Xtraining = double(Xtraining);
% end
Xtest = read_mat(str_test, ...
    [data_format '=>' data_format]);
Xtest = double(Xtest);
%%
if strcmp(type, 'GIST1M3') || ...
        strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'Tiny80M')
    Xbase = read_mat(str_base, [data_format '=>' data_format]);
elseif strcmp(type, 'Tiny80M')
    error('not used');
    Xbase = {'float', str_base};
elseif strcmp(type, 'MNIST') || ...
        strcmp(type, 'MNISTE')
    Xbase = Xtraining;
else
    error('dfdsf');
end
%
%% configure

if strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    file_name_binary_code = ...
        ['result\' encoding_scheme '_' num2str(code_length) ...
        '.mat'];
else
    error('dfsd');
end

%% encoding
if strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'GIST1M3')
    if strcmp(encoding_scheme, 'PQ')
        X = Xtraining;
    else
        X = Xbase;
    end
elseif strcmp(type, 'Tiny80M') || ...
        strcmp(type, 'MNIST') || ...
        strcmp(type, 'MNISTE') 
    X = double(Xtraining);
else
    error('dfs');
end
size(X)


rng(1);
[W] = train_lsh2(X, code_length);

x = load(file_name_binary_code, ...
    'query_binary_code', ...
    'base_binary_code');

if strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    X = Xtest;
    c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
    query_binary_code = compactbit(c);
    
    
    X = Xbase;
    c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
    base_binary_code = compactbit(c);
    
    if (isempty(find(x.query_binary_code ~= query_binary_code, 1)) && ...
            isempty(find(x.base_binary_code ~= base_binary_code, 1)))
        save(file_name_binary_code, 'W', '-append');
        file_name_binary_code
    end
    
else
    error('dfdsf');
end

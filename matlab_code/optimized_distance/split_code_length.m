function all_dim = split_code_length(code_length, num_partition)

all_dim = zeros(num_partition, 1);

lower = floor(code_length / num_partition);

all_dim(:) = lower;

num_added = mod(code_length, num_partition);

for i = 1 : num_added
    all_dim(i) = all_dim(i) + 1;
end



x = dir('*_perf_1_10_50_100_400_1000_10000_20000');

dir_curr = [pwd '\'];

%%
i = 1; 
%%
for i = 1 : numel(x)

    file_name = x(i).name;
    idx = strfind(file_name, 'perf_1_10_50_100_400_1000_10000_20000');
    pre_fix = file_name(1 : idx - 1);
    
    assert(exist([pre_fix '0'], 'file') ~= 0);
    
    if ~ exist([pre_fix '_avg_distance_ratio_perf'], 'file')
        exeAverageDistanceRatio(dir_curr, pre_fix, ...
            gnd_file.STestBase, src_file.c_test, src_file.c_base);
    end
    assert( exist([pre_fix '_avg_distance_ratio_perf'], 'file') ~= 0);
end
function E = MmexComputeE(binary_code, num_partitions)

assert(isa(binary_code, 'uint8'));

E = mexComputeE(binary_code, num_partitions);
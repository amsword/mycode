function result = load_w2()

recall_file_name = dir('*al*perf_1_10_50_100_400_1000_10000_20000');
map_file_name = dir('*al*map_perf_100_1000_10000');
ratio_file_name = dir('*al*_avg_distance_ratio_perf');
map5k_file_name = dir('*al*_map_perf_5000');



recall = cell(numel(recall_file_name), 1);
for i = 1 : numel(recall_file_name)
    recall{i} = read_mat(recall_file_name(i).name, 'double');
end

ratio = cell(numel(ratio_file_name), 1);
for i = 1 : numel(ratio_file_name)
    ratio{i} = read_mat(ratio_file_name(i).name, 'double');
end


map = zeros(3, numel(map_file_name));
for i = 1 : numel(map_file_name)
    map(:, i) = read_mat(map_file_name(i).name, 'double');
end

map5k = zeros(1, numel(map5k_file_name));
for i = 1 : numel(map5k_file_name)
    map5k(:, i) = read_mat(map5k_file_name(i).name, 'double');
end


result.recall_file_name = recall_file_name;
result.recall = recall;

result.ratio_file_name = ratio_file_name;
result.ratio = ratio;

result.map_file_name = map_file_name;
result.map = map;

result.map5k_file_name = map5k_file_name;
result.map5k = map5k;

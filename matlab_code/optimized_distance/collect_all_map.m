x = load(save_file.test_itq);
%
ap_itq = x.eval_itq{end - 1}.ap

%%
x = load(save_file.test_mlh);
ap_mlh = x.eval_mlh{end - 1}.ap

%%
x = load(save_file.test_bre);
ap_bre = x.eval_bre{end - 1}.ap

%%
x = read_mat('origin_table_map_perf_100_1000_10000', 'double');
ap_pq = x(end)

%%
w_result = load_w2();
%%
dir_curr = [pwd '\'];
no_w_result = load_no_w2(dir_curr);
%%
ap_scdl = max(w_result.map(end, :))
ap_cdl = max(no_w_result.map(end, :))
function [best, ratio_best, map, para] = load_no_w(dir_curr)

beta = 0;
coef = 1;

str_in_dist_prefix = [dir_curr 'origin_table_'];

all_lambda = [[1 10 10^2 10^3 10^4 10^5 10^6 10^7 10^8]];
idx = 1;

is_best = 0;
is_ratio_best = 0;
is_map = 0;
is_para = 0;

if nargout == 4
    is_best = 1;
    is_ratio_best = 1;
    is_map = 1;
    is_para = 1;
elseif nargout == 3
    is_best = 1;
    is_ratio_best = 1;
    is_map = 1;
elseif nargout == 2
    is_best = 1;
    is_ratio_best = 1;
elseif nargout == 1
    is_best = 1;
end

if is_best
    clear best;
end
if is_ratio_best
    clear ratio_best;
end
if is_map
    clear map;
end
if is_para
    clear para;
end

for alpha = [0]
    for lambda = all_lambda
        
        str_out_prefix = ['origin_table_' 'out_' ...
            'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_'];
        
        str_out_put_prefix = [dir_curr 'no_w\' str_out_prefix];
        
        if exist([str_out_put_prefix 'perf_1_10_50_100_400_1000_10000_20000'], 'file')
            if is_best
                best{idx} = read_mat([str_out_put_prefix 'perf_1_10_50_100_400_1000_10000_20000'], 'double');
            end
            if is_ratio_best
                ratio_best{idx} = read_mat([str_out_put_prefix '_avg_distance_ratio_perf'], 'double');
            end
            if is_para
                para(idx) = lambda;
            end
            if is_map
                map(:, idx) = read_mat([str_out_put_prefix 'map_perf_10000_20000'], 'double');
            end
            idx = idx + 1;
        else
            break;
        end
        
    end
end
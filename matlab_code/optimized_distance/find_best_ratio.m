function idx = find_best_ratio(best)

best_v = 1000000000;


for i = 1 : numel(best)
    v = jf_calc_map(1 : 10^4, best{i});
    if v < best_v
        best_v = v;
        idx = i;
    end
end
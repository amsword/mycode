 if strcmp(type, 'SIFT1M3') || ...
            strcmp(type, 'GIST1M3') || ...
            strcmp(type, 'MNISTE')
        result_dir = ['\\dragon\Data\Jianfeng\Data_HashCode\' type '\working_opt_distance\result\'];
    else
        error('dfds');
    end
result_dir,
code_length,
num_partition,

b = strcmp(encoding_scheme, 'ITQ') || ...
    strcmp(encoding_scheme, 'LSH');
assert(b);
%
topks = [];
[r_main, topks1] =  load_perf(result_dir, ...
    encoding_scheme, ...
    code_length, ...
    num_partition);
topks = topks1;


if strcmp(encoding_scheme, 'ITQ')
    r_xiao = load_xiao(result_dir, ...
        encoding_scheme, code_length);
end

%
% for k = 1 : numel(topks)
k = numel(topks);
figure;
hold on;

plot(r_main{4}.precision(:, k), ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_main{3}.precision(:, k), ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

if strcmp(encoding_scheme, 'ITQ')
    plot(r_xiao{1}.precision(:, k), ...
        'Color', curve_colors(5, :), ...
        'LineWidth', 2, 'LineStyle', '--');
end

plot(r_main{1}.precision(:, k), ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Precision', 'FontSize', 14);

if strcmp(encoding_scheme, 'ITQ')
    legend(...
        [encoding_scheme '-OA'], ...
        [encoding_scheme '-OS'], ...
        [encoding_scheme '-WhRank'], ...
        [encoding_scheme '-QsRank'], ...
        [encoding_scheme '-HM'], ...
        'Location', 'Best');
elseif strcmp(encoding_scheme, 'LSH')
    legend(...
        [encoding_scheme '-OA'], ...
        [encoding_scheme '-OS'], ...
       [encoding_scheme '-HM'], ...
        'Location', 'Best');
else
    error('fdfs');
end

if strcmp(type, 'MNISTE')
    xlim([0, 2000]);
elseif strcmp(type, 'SIFT1M3')
elseif strcmp(type, 'GIST1M3')
    switch code_length
        case 64
            if num_partition == 8
                ylim([0.2, 0.7]);
            else
                ylim([0.2, 0.8]);
            end
        otherwise
            ylim([0.1, 0.7]);
    end
else
    error('dfsd');
end
result_dir = ['\\dragon\Data\Jianfeng\Data_HashCode\' type '\working_opt_distance\result\'];

saveas(gca, ...
    [result_dir 's2_pre_' ...
    num2str(topks(k)) '_' ...
    encoding_scheme '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
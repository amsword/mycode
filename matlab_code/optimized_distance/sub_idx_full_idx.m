function y = sub_idx_full_idx(sub_idx, num_centers)

assert(size(sub_idx, 1) == numel(num_centers))
num_centers = reshape(numel(num_centers), 1);

prod = cumprod(num_centers);
prod = [1; prod(1 : end - 1)];

t1 = bsxfun(@times, sub_idx, prod);
y = sum(t1, 1);
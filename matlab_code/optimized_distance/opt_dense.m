function [D, obj] = opt_dense(...
    binary_code, ...
    num_partition, ...
    Xtraining)

E = MmexComputeE(binary_code, num_partition);

%
[G, centers, error] = MmexComputeG(...
    binary_code, ...
    Xtraining, ...
    E);

%%
matE = ConstructMat(E);
matG = ConstructMat(G);

inv_matE = pinv(matE);
D =  inv_matE * matG * inv_matE;

obj = loss_approximate_errors(E, G, D);


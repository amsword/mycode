function opt_dist = opt_cc_dist(num_centers, all_assignment, Xtraining)

max_iter = 10;

dim = size(Xtraining, 1);
num_point = size(Xtraining, 2);

%% initialize
sub_dim_num = numel(num_centers);


%%  how many points in each sub centers

num_points_in_sub_clusters = cell(sub_dim_num, 1);

for i = 1 : sub_dim_num
    point_count = zeros(num_centers(i), 1);
    for j = 1 : size(all_assignment, 2)
        idx = all_assignment(i, j);
        point_count(idx) = point_count(idx) + 1;
    end
    num_points_in_sub_clusters{i} = point_count;
end

%% compute the centers in full dimensions
centers_in_full_dim = cell(sub_dim_num, 1);

for i = 1 : sub_dim_num
    cf = zeros(dim, num_centers(i));
    
    for j = 1 : num_point
        idx = all_assignment(i, j);
        cf(:, idx) = cf(:, idx) + Xtraining(:, j);
    end
    point_count = num_points_in_sub_clusters{i};
    
    for j = 1 : size(cf, 2)
        if point_count(j) ~= 0
            cf(:, j) = cf(:, j) / point_count(j);
        end
    end
    centers_in_full_dim{i} = cf;
end

%% distortion for each center
all_distortion_full_dim = cell(sub_dim_num, 1);

for i = 1 : sub_dim_num
    all_sub_distortion_full = zeros(num_centers(i), 1);
    cf = centers_in_full_dim{i};
    
    for j = 1 : num_point
        idx = all_assignment(i, j);
        d = Xtraining(:, j) - cf(:, idx);
        all_sub_distortion_full(idx) = all_sub_distortion_full(idx) + sum(d .^ 2);
    end
    
    point_count = num_points_in_sub_clusters{i};
    
    for j = 1 : num_centers(i)
        if point_count(j) ~= 0
            all_sub_distortion_full(j) = all_sub_distortion_full(j) / point_count(j);
        end
    end
    all_distortion_full_dim{i} = all_sub_distortion_full;
end

%%  target distance matrix
all_target_distance = cell(sub_dim_num, 1);

for i = 1 : sub_dim_num
    cf = centers_in_full_dim{i};
    dist = sqdist(cf, cf);
    
    all_sub_distortion_full = all_distortion_full_dim{i};
    dist = dist + bsxfun(@plus, all_sub_distortion_full, all_sub_distortion_full');
    
    point_count = num_points_in_sub_clusters{i};
    dist = dist .* bsxfun(@times, point_count, point_count');
    all_target_distance{i} = dist;
end

%%

all_count_sep = cell(sub_dim_num, 1);

for idx_sub_dim = 1 : sub_dim_num
    all_count_sep{idx_sub_dim} = cell(num_centers(idx_sub_dim), 1);
    
    for idx_center_row = 1 : num_centers(idx_sub_dim)
        num_point_row = num_points_in_sub_clusters{idx_sub_dim}(idx_center_row);
        if num_point_row == 0
            continue;
        end
        ind_rows = (all_assignment(idx_sub_dim, :) == idx_center_row);
        
        all_count_sep{idx_sub_dim}{idx_center_row} = cell(sub_dim_num, 1);
        
        for k = 1 : sub_dim_num
            if k == idx_sub_dim
                continue;
            end
            selected_rows = all_assignment(k, ind_rows);
            all_count_sep{idx_sub_dim}{idx_center_row}{k} = ...
                histc(selected_rows, 1 : num_centers(k));
        end
    end
end

%%
tic
opt_dist = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    n = num_centers(i);
    opt_dist{i} = zeros(n, n);
%     opt_dist{i} = all_lookup_table{i}.table;
end

v = zeros(max_iter, 1);

for iter = 1 : max_iter
    

    
    for idx_sub_dim = 1 : sub_dim_num
        for idx_center_row = 1 : num_centers(idx_sub_dim)
            num_point_row = num_points_in_sub_clusters{idx_sub_dim}(idx_center_row);
            if num_point_row == 0
                continue;
            end
            if mod(idx_center_row, 5) == 0
                [num2str(iter) ' ' num2str(idx_sub_dim) '. ' num2str(idx_center_row)]
            end
            for idx_center_col = idx_center_row : num_centers(idx_sub_dim)
                num_point_col = num_points_in_sub_clusters{idx_sub_dim}(idx_center_col);
                if num_point_col == 0
                    continue;
                end
                s = 0;
                
                for k = 1 : sub_dim_num
                    if k == idx_sub_dim
                        continue;
                    end
                    n1 = all_count_sep{idx_sub_dim}{idx_center_row}{k};
                    n2 =  all_count_sep{idx_sub_dim}{idx_center_col}{k};
                    
                    nn = bsxfun(@times, n1', n2);
                    tmp1 = bsxfun(@times, nn, opt_dist{k});
                    s = s + sum(tmp1(:));
                end
                
                t = all_target_distance{idx_sub_dim}(idx_center_row, idx_center_col);
                
                opt_dist{idx_sub_dim}(idx_center_row, idx_center_col) = ...
                    (t - s) / num_point_row / num_point_col;
            end
        end
        for idx_center_row = 1 : num_centers(idx_sub_dim)
            num_point_row = num_points_in_sub_clusters{idx_sub_dim}(idx_center_row);
            if num_point_row == 0
                continue;
            end
           
            for idx_center_col = 1 : (idx_center_row - 1)
                num_point_col = num_points_in_sub_clusters{idx_sub_dim}(idx_center_col);
                if num_point_col == 0
                    continue;
                end
       
                opt_dist{idx_sub_dim}(idx_center_row, idx_center_col) = ...
                    opt_dist{idx_sub_dim}(idx_center_col, idx_center_row);
            end
        end
    end
end

toc;



%% opt

tic;
opt_dist = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    n = num_centers(i);
    opt_dist{i} = zeros(n, n);
end

for iter = 1 : max_iter
    for idx_sub_dim = 1 : sub_dim_num
        for idx_center_row = 1 : num_centers(idx_sub_dim)
            num_point_row = num_points_in_sub_clusters{idx_sub_dim}(idx_center_row);
            if num_point_row == 0
                continue;
            end
            ind_rows = (all_assignment(idx_sub_dim, :) == idx_center_row);
            
            if mod(idx_center_row, 5) == 0
                [num2str(iter) ' ' num2str(idx_sub_dim) '. ' num2str(idx_center_row)]
            end
            for idx_center_col = 1 : num_centers(idx_sub_dim)
                num_point_col = num_points_in_sub_clusters{idx_sub_dim}(idx_center_col);
                if num_point_col == 0
                    continue;
                end
                
                ind_cols = (all_assignment(idx_sub_dim, :) == idx_center_col);
                s = 0;
                
                for k = 1 : sub_dim_num
                    if k == idx_sub_dim
                        continue;
                    end
                    selected_rows = all_assignment(k, ind_rows);
                    selected_cols = all_assignment(k, ind_cols);
                    n1 = histc(selected_rows, 1 : num_centers(k));
                    n2 = histc(selected_cols, 1 : num_centers(k));
                    
                    nn = bsxfun(@times, n1', n2);
                    tmp1 = bsxfun(@times, nn, opt_dist{k});
                    s = s + sum(tmp1(:));
                end
                
                t = all_target_distance{idx_sub_dim}(idx_center_row, idx_center_col);
                
                opt_dist{idx_sub_dim}(idx_center_row, idx_center_col) = ...
                    (t - s) / num_point_row / num_point_col;
            end
        end
    end
end
toc;

%%
clc;
x = read_mat('opt_dist_labelme_1', 'double');
norm(x(:) - opt_dist{2}(:))

x = read_mat('opt_dist_labelme_0', 'double');
norm(x(:) - opt_dist{1}(:))

%%
for i = 1 : sub_dim_num
    norm(opt_dist{i}(:) - old_opt_dist{i}(:))
end

%%
for i = 0 : sub_dim_num - 1
    file1 = '_opt_dist_sift_';
    file2 = 'opt_dist_sift_';
    x1 = read_mat([file1 num2str(i)], 'double');
    x2 = read_mat([file2 num2str(i)], 'double');
    norm(x1(:) - x2(:))
end

%%
for i = 0 : sub_dim_num - 1
    file1 = 'opt_dist_sift_';
    x1 = read_mat([file1 num2str(i)], 'double');
    opt_dist{i + 1} = x1;
end


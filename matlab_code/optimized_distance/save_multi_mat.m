function save_multi_mat(dist, pre_fix)

for i = 0 : (numel(dist) - 1)
    file_name = [pre_fix num2str(i)];
    save_mat(dist{i + 1}, file_name, 'double');
end
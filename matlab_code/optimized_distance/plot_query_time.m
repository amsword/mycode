
clear oa;
clear os hm;
k = 1;
clear all_labels;
all_num_partition = [6 : 11];
for num_partition = all_num_partition
    x = load(['result\ITQ_64_' num2str(num_partition)], 'all_time');
    
    oa(k) = x.all_time{4}.average_query_time;
    os(k) = x.all_time{3}.average_query_time;
    k = k + 1;
end
x = load('result\ITQ_64_6', 'all_time');
hm = x.all_time{1}.average_query_time;

x = load('result\ITQ_64_Xiao', 'all_time_xiao');
xiao = x.all_time_xiao{1}.average_query_time;

x = load('result\ITQ_64_Lei', 'all_time_lei');
lei = x.all_time_lei{1}.average_query_time;

figure;
plot(all_num_partition, oa, ...
    'Marker', 's', ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);
hold on;
plot(all_num_partition, os, ...
    'Marker', 'o', ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

t = zeros(1, numel(all_num_partition));
t(:) = lei;
plot(all_num_partition, t, ...
    'Marker', '*', ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

t = zeros(1, numel(all_num_partition));
t(:) = xiao;
plot(all_num_partition, t, ...
    'Marker', 'd', ...
    'Color', curve_colors(5, :), ...
    'LineWidth', 2, 'LineStyle', '--');

t(:) = hm;
plot(all_num_partition, t, ...
    'Marker', '*', ...
    'LineWidth', 2, ...
    'Color', curve_colors(3, :), 'LineStyle', '--');

% xlim([5, 11]);
% ylim([0, 0.2]);

lgd = legend(name_oa, name_os, ...
    'WhRank', 'QsRank', ...
    'HM');
% legend('OA', 'OS', 'Location', 'Best');
% ylim([0.005, 0.4]);
ylim([0, 0.4]);
xlim([6, 11]);
set(lgd,...
     'Position',[0.559166666666664 0.477678571428572 0.232142857142857 0.304761904761905]);

grid on;
ylabel('Query time (second)', 'FontSize', 14);
xlabel('Number of partitions', 'FontSize', 14);
set(gca, 'FontSize', 14);

save_dir = 'H:\Data_HashCode\figs\';
save_dir = [save_dir 'SIFT1M3' '\'];
saveas(gca, [save_dir 'ITQ_64_T_time.eps'], 'psc2');

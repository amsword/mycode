
s1 = s;
s2 = s;
%%
s = 0;
for i = 1 : sub_dim_num
    table = all_lookup_table{i}.added_table;
%         table = all_lookup_table{i}.table;
    count = all_count{i};
    table(:) = 0;
    
    m1 = bsxfun(@times, count, count');
    m2 = table .^ 2;
    
    m3 = sqdist(all_centers{i}, all_centers{i});
    m4 = bsxfun(@plus, all_distortion{i}, all_distortion{i}');
    
    r = m2 - 2 * table .* (m3 + m4);
    r = r .* m1;
    s = s + sum(r(:));
    
    m5 = bsxfun(@plus, all_sq_distortion{i}, all_sq_distortion{i}');
    m6 = m3 .^ 2;
    m7 = 2 * all_distortion{i} * all_distortion{i}';
    m8 = 2 * m3 .* (all_distortion{i} * ones(1, num_centers(i)));
    m9 = 2 * m3 .* (ones(num_centers(i), 1) * all_distortion{i}');
    m = m5 + m6 + m7 + m8 + m9;
    m = m .* m1;
    s = s + sum(m(:));
end

%% on test data

s = 0;
for i = 1 : sub_dim_num
    table = all_lookup_table{i}.added_table;
%         table = all_lookup_table{i}.table;
    count = all_count{i};
    table(:) = 0;
    
    m1 = bsxfun(@times, count, count');
    m2 = table .^ 2;
    
    m3 = sqdist(all_centers{i}, all_centers{i});
    m4 = bsxfun(@plus, all_distortion{i}, all_distortion{i}');
    
    r = m2 - 2 * table .* (m3 + m4);
    r = r .* m1;
    s = s + sum(r(:));
    
    m5 = bsxfun(@plus, all_sq_distortion{i}, all_sq_distortion{i}');
    m6 = m3 .^ 2;
    m7 = 2 * all_distortion{i} * all_distortion{i}';
    m8 = 2 * m3 .* (all_distortion{i} * ones(1, num_centers(i)));
    m9 = 2 * m3 .* (ones(num_centers(i), 1) * all_distortion{i}');
    m = m5 + m6 + m7 + m8 + m9;
    m = m .* m1;
    s = s + sum(m(:));
end

%% on test point

SBaseBase = load_gnd2(str_gnd, 1000);
SBaseBase = SBaseBase';
%%
opt_table = read_multi_mat(sub_dim_num, 'origin_table_50000_1000_out_not_scaled_');

tic;

all_topks = [1, 10, 50, 100, 400, 1000];

e0 = zeros(1, numel(all_topks));
e1 = zeros(1, numel(all_topks));

for idx_test = 1 : num_test_point
    [num2str(idx_test) '/' num2str(num_test_point)]
    for idx_topk = 1 : numel(all_topks)
        gnd_idx = SBaseBase(idx_test, 1 : all_topks(idx_topk));
        
        dist_xtest_xtraining_opt_table = zeros(1, all_topks(idx_topk));
        dist_xtest_xtraining_table = zeros(1, all_topks(idx_topk));
        
        for i = 1 : sub_dim_num
            idx = bsxfun(@plus, idx_quantized_sub_center(i, idx_test)', (all_assignment(i, gnd_idx) - 1) * num_centers(i));
            dist_xtest_xtraining_opt_table = dist_xtest_xtraining_opt_table + opt_table{i}(idx);
            dist_xtest_xtraining_table = dist_xtest_xtraining_table + all_lookup_table{i}.table(idx);
        end
        
        true_dist = sqdist(Xtest(:, idx_test), Xtraining(:, gnd_idx));
        e0(idx_topk) = e0(idx_topk) + sum((true_dist - dist_xtest_xtraining_table) .^ 2);
        e1(idx_topk) = e1(idx_topk) + sum((true_dist - dist_xtest_xtraining_opt_table) .^ 2);
    end
end

toc;

%%



curve_colors =  [[255 0 0] / 255; ...
    [0 0 0] / 255; ...
    [0 255 0] / 255; ...
    [0 0 255] / 255; ...
    [148 30 249] / 255; ...
    [80 136 170] / 255];

close all;
% all_type = {'SIFT1M3', 'GIST1M3', 'MNISTE'};
all_type = {'SIFT1M3'};

all_encoding_scheme = {'ITQ'};

type = all_type{1};

opt_dist_get_all_file_names;

cd(working_dir);

%%
plot_map_t;
%%
plot_query_time;
%% training time
plot_training_time;
clear oa;
clear os hm;
k = 1;
clear all_labels;
all_num_partition = [6 : 11];
for num_partition = all_num_partition
    x = load(['result\ITQ_64_' num2str(num_partition)], 'all_map');
    
    oa(k) = x.all_map{4}.map(end);
    os(k) = x.all_map{3}.map(end);
    k = k + 1;
end
%
x = load('result\ITQ_64_6', 'all_map');
hm = x.all_map{1}.map(end);

figure;
plot(all_num_partition, oa, 'Marker', 's', ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);
hold on;
plot(all_num_partition, os, ...
    'Marker', 'o', ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);
%
x = load('result\ITQ_64_Lei', 'all_map_lei');
lei = x.all_map_lei{1}.map(end);
t = zeros(1, numel(all_num_partition));
t(:) = lei;
plot(all_num_partition, t, 'Marker', '*', ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

x = load('result\ITQ_64_Xiao', 'all_map_xiao');
xiao = x.all_map_xiao{1}.map(end);

t = zeros(1, numel(all_num_partition));
t(:) = xiao;
plot(all_num_partition, t, 'Marker', 'd', ...
    'Color', curve_colors(5, :), ...
    'LineWidth', 2, 'LineStyle', '--');

t(:) = hm;
plot(all_num_partition, t, 'Marker', '*', ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

grid on;
ylabel('mAP', 'FontSize', 14);
xlabel('Number of partitions', 'FontSize', 14);

lgd = legend(name_oa, name_os, ...
    'WhRank', 'QsRank', ...
    'HM', 'Location', 'Best');
set(gca, 'FontSize', 14);

set(lgd,...
    'Position',[0.63543998244459 0.365234565196222 0.232142857142857 0.304761904761905]);


ylim([0.53, 0.7]);

save_dir = '\\dragon\Data\Jianfeng\figs\';
save_dir = [save_dir 'SIFT1M3' '\'];
saveas(gca, [save_dir 'ITQ_64_T_map.eps'], 'psc2');

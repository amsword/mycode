function [D_diagonal, obj, ...
    D_dense, obj_dense, ...
    aux, centers, errors] = ...
    merge_opt(binary_code, num_partition, Xtraining)

tic
[D_diagonal, obj] = opt_diagonal(...
    binary_code, ...
    num_partition, ...
    Xtraining);
toc

%
tic;
[D_dense, obj_dense] = opt_dense(...
    binary_code, ...
    num_partition, ...
    Xtraining);
toc
%
tic;
[aux, centers, errors] = opt_query(...
    binary_code, ...
    num_partition, ...
    Xtraining);
toc;
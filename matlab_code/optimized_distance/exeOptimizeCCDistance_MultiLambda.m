function exeOptimizeCCDistance_MultiLambda(str_training, str_assignment, ...
    str_in, str_out, is_all_zeros, lambda_equal, lambda_non_equal, mu, max_iter, num_center)

cmd = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\HashCode\Temps_lookup_table_opt\OptimizedCCDistance_multi_lambda.exe ';
cmd = [cmd str_training ' '];
cmd = [cmd str_assignment ' '];
cmd = [cmd str_in ' '];
cmd = [cmd str_out ' '];
cmd = [cmd num2str(is_all_zeros) ' '];
cmd = [cmd num2str(lambda_equal) ' '];
cmd = [cmd num2str(lambda_non_equal) ' '];
cmd = [cmd num2str(mu) ' '];
cmd = [cmd num2str(max_iter) ' '];
cmd = [cmd num2str(num_center)];

system(cmd);
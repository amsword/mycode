str_training = src_file.c_base;
str_test = src_file.c_test;
sub_dim_num = 2;
sub_dim_num_center = 1024;
str_gnd = gnd_file.STestBase;
% str_gnd = gnd_file.StestTrainingBin;

num_centers = zeros(sub_dim_num, 1);
num_centers(:) = sub_dim_num_center;
clear sub_dim_num_center;

num_point = 10^6;
dim = 128;
sub_dim_length = dim / sub_dim_num;

%%
all_centers = cell(sub_dim_num, 1);
all_assignment = zeros(sub_dim_num, num_point);

%
all_centers{1} = read_mat('d1.double.bin_center.double.bin', 'double');
all_centers{2} = read_mat('d2.double.bin_center.double.bin', 'double');
%%
all_assignment(1, :) = read_mat('d1.double.bin_assign.double.bin', 'int32')' + 1;
all_assignment(2, :) = read_mat('d2.double.bin_assign.double.bin', 'int32')' + 1;

%%
save_mat(all_assignment' - 1, 'assignment_train', 'int32');
%%
%% load test data
clc;
Xtest = read_mat(str_test, 'double');
num_test_point = size(Xtest, 2);
assert(size(Xtest, 1) == dim);

%% quantization
clc;
idx_quantized_sub_center = zeros(sub_dim_num, num_test_point);
for i = 1 : sub_dim_num
    idx_start = 1 + (i - 1) * sub_dim_length;
    idx_end = i * sub_dim_length;
    
    d = sqdist(Xtest(idx_start : idx_end, :), all_centers{i});
    [~, si] = min(d, [], 2);
    idx_quantized_sub_center(i, :) = si;
end

%%
save_mat(int32(idx_quantized_sub_center' - 1), 'assignment_test', 'int32');

%%
all_lookup_table = cell(numel(sub_dim_num), 1);

for i = 1 : sub_dim_num
    c = all_centers{i};
    t = sqdist(c, c);
    all_lookup_table{i} = t;
end
%%
save_multi_mat(all_lookup_table, 'origin_table_');

%%
dir_curr = [pwd '\'];
mu  = 0;
for lambda = [10^5, 10^6, 10^4, 10^3]
        lambda
        max_iter = 100;
        
        str_assignment = [dir_curr  'assignment_train'];
         str_training = src_file.c_base;
        
        is_all_zeros = 0;
        str_in = [dir_curr  'origin_table_'];
        str_out1 = [str_in num2str(lambda) '_' num2str(mu) '_out_not_scaled_'];
        exeOptimizeCCDistance_MultiLambda(str_training, str_assignment, ...
            str_in, str_out1, is_all_zeros, lambda, lambda, mu, max_iter, num_centers(1));
end

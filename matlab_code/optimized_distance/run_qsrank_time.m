%% run Xiao's weighted Hamming distance
if strcmp(encoding_scheme, 'ITQ')
    file_name = ['result\' ...
        encoding_scheme '_' num2str(code_length) ...
        '_Xiao.mat'];
    
    % training
    x = load(file_name_binary_code, 'W');
    W = x.W;
    
    X = Xtest;
    query_unbinarized = bsxfun(...
        @plus, W(1 : end - 1, :)' * X, W(end, :)');
    
    query_base_gnd = gnd;
    epsilon = sum((Xtest - Xbase(:, query_base_gnd(end, :) + 1)) .^ 2, 1);
    epsilon = sqrt(epsilon);
    
    query_unbinarized = [query_unbinarized; epsilon];

    is_run = true;
    
%     if exist(file_name, 'file')
%         x = load(file_name, 'all_time_xiao');
%         if isfield(x, 'all_time_xiao')
%             is_run = false;
%         end
%     end
        
    if is_run
        %
        clear all_dist;
        k = 1;
        all_dist{k} = {6};

        clear all_cri;
k = 1;
all_cri{k} = {5}; k = k + 1; % precision and recall

        clear all_time_xiao;
        for i = 1 : numel(all_dist)
            tic;
            i
            all_time_xiao{i} = MmexTestTrueNeighbors(...
                query_unbinarized, ...
                base_binary_code, ...
                num_candidate, ...
                Xtest, ...
                Xbase, ...
                all_dist{i}, ...
                all_cri);
            toc;
        end
        save(file_name, ...
            'all_time_xiao', '-append');
    end
    
   
    
end
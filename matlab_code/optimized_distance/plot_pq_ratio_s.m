result_dir,
code_length,
num_partition,


[r_pq, topks1] =  load_perf(result_dir, ...
    'PQ', ...
    code_length, ...
    num_partition);
topks = topks1;

%
% for k = 1 : numel(topks)
k = numel(topks) - 1;
figure;
hold on;

plot(r_pq{5}.dist_ratio, ...
    'Color', curve_colors(1, :), ...
    'LineWidth', 2);

plot(r_pq{4}.dist_ratio, ...
    'Color', curve_colors(2, :), ...
    'LineWidth', 2);

plot(r_pq{2}.dist_ratio, ...
    'Color', curve_colors(4, :), ...
    'LineWidth', 2, 'LineStyle', '--');

plot(r_pq{1}.dist_ratio, ...
    'Color', curve_colors(3, :), ...
    'LineWidth', 2, 'LineStyle', '--');

set(gca, 'FontSize', 14);
grid on;
xlabel('Number of the retrieved points', 'FontSize', 14);
ylabel('Mean average ratio', 'FontSize', 14);
legend(...
    ['PQ-' name_oa], ...
    ['PQ-' name_os], ...
    ['PQ-AD'], ...
    ['PQ-SD'], ...
    'Location', 'Best');
if strcmp(type, 'MNISTE')
    switch code_length
        case 32
            if num_partition == 3
                ylim([1.02, 1.12]);
            elseif num_partition == 4
                ylim([1.03, 1.16]);
            end
        case 64
            if num_partition == 6
                ylim([1.01, 1.06]);
            elseif num_partition == 7
                ylim([1.01, 1.12]);
            elseif num_partition == 8
                ylim([1.015, 1.1]);
            end
    end
elseif strcmp(type, 'SIFT1M3')
    switch code_length
        case 32
            if num_partition == 3
                ylim([1.03, 1.16]);
            elseif num_partition == 4
                ylim([1.04, 1.16]);
            end
        case 64
            if num_partition == 6
                ylim([1.02, 1.12]);
            elseif num_partition == 7
                ylim([1.02, 1.1]);
            elseif num_partition == 8
                ylim([1.02, 1.15]);
            end
    end
elseif strcmp(type, 'GIST1M3')
    switch code_length
        case 32
            if num_partition == 3
                ylim([1.04, 1.14]);
            elseif num_partition == 4
                 ylim([1.05, 1.16]);
            end
        case 64
            if num_partition == 6
                ylim([1.03, 1.1]);
            elseif num_partition == 7
                ylim([1.03, 1.15]);
            elseif num_partition == 8
                ylim([1.04, 1.14]);
            end
    end
else
    error('df');
end

saveas(gca, ...
    [save_dir 's_dist_' ...
    'PQ' '_' ...
    num2str(code_length) '_' ...
    num2str(num_partition) ...
    '.eps'], 'psc2');
% end


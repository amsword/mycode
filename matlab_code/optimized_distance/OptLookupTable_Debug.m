
for code_length = [16 32 64 128]
end


%%
rng(1);
[W] = train_lsh2(X, code_length);

c = bsxfun(@ge, W(1 : end - 1, :)' * X, -W(end, :)');
binary_code = compactbit(c);
%
hist(double(binary_code(:)));

%%
for i = 1 : numel(D_diagonal)
    x = find(D_diagonal{i} ~= D_diagonal2{i});
    assert(isempty(x));
end

x = find(D_dense ~= D_dense2);
assert(isempty(x));

x = find(aux2 ~= aux);
assert(isempty(x));

for i = 1 : numel(errors2)
    x = find(errors{i} ~= errors{i});
    assert(isempty(x));
    
    x = find(centers2{i} ~= centers{i});
    assert(isempty(x));
end


x = 

%%
cd('C:\Users\uqjwan34\Desktop\codes2\HashCode');
jf_conf;

%%
% file name
type = 'Labelme'; % Labelme
% SIFT1M3
% GIST1M3
data_parent_folder = get_data_parent_folder();
working_dir = [data_parent_folder type '\working_opt_distance'];
data_dir = [data_parent_folder type '\data\'];

str_training = [data_dir 'c_data\OriginXtraining.double.bin'];
str_test = [data_dir 'c_data\OriginXTest.double.bin'];
if strcmp(type, 'Labelme')
    str_base = str_training;
else
    str_base = [data_dir 'c_data\OriginXBase.double.bin'];
end

str_gnd = [data_dir 'STestBase.double.bin'];
str_gnd_train_train = [data_dir 'STrainTrain.double.bin'];
cd(working_dir);

%%
load('result\PQ_64_8.mat', ...
    'query_binary_code');
%%
clc;
% x = mexTestAllPerformance(...
%     query_binary_code);
x = mexTestAllPerformance(); 
plot(cumsum(x.fixed_topk) / 2000);
clear_dll;


%%
load('result\PQ_64_8.mat', ...
    'query_binary_code', ...
    'base_binary_code', ...
    'num_candidate', ...
    'Xtest', ...
    'Xbase', ...
    'all_dist', ...
    'num_partition', ...
    'R', ...
    'all_D', ...
    'all_cri', ...
    'gnd', ...
    'topks');
load('result\PQ_64_8.mat', 'D_diagonal');
all_dist{2} = {4, num_partition, num_partition, ...
        R, all_D};
    all_dist{6} = {0};
%%
clear all_cri;
k = 1;
all_cri{k} = {0, int32(topks(1)), gnd}; k = k + 1;
all_cri{k} = {1, gnd}; k = k + 1;
all_cri{k} = {2, gnd}; k = k + 1;

result = MmexTestTrueNeighbors(...
    query_binary_code, ...
    base_binary_code, ...
    10, ...
    Xtest, ...
    Xbase, ...
    all_dist{1}, ...
    all_cri);

%%
clc;
clear all_cri;
k = 1;
all_cri{k} = {0, int32(topks(1)), gnd}; k = k + 1;
all_cri{k} = {1, gnd}; k = k + 1;
all_cri{k} = {2, gnd}; k = k + 1;
% clear all_perf;
% for i = 1 : numel(all_dist)
num_candidate = 10;
for i = 1 : 1
    tic;
    i
    all_perf{i} = MmexTestTrueNeighbors(...
        query_binary_code, ...
        base_binary_code, ...
        num_candidate, ...
        Xtest, ...
        Xbase, ...
        all_dist{i}, ...
        all_cri);
    toc;
end
figure;
plot(all_perf{1}.precision);
figure;
plot(all_perf{1}.recall);
figure;
plot(all_perf{1}.dist_ratio);

%%
dim = size(Xtest, 1);
sub_dim = dim / num_partition;

for i = 1 : num_partition
    dim_start = (i - 1) * sub_dim + 1;
    dim_end = dim_start + sub_dim - 1;
    mlookup{i} = sqdist(Xtest(dim_start : dim_end, 1), all_D{i});
end

%%
x = read_mat('lookup', 'double');

for i = 1 : 8
    norm(mlookup{i} - x((i - 1) * 256 + 1 : i * 256)')
end

clear_dll;

%%
% x = read_mat('lookup', 'double');
all_distance = zeros(1, size(base_binary_code, 2));
r = 0;
for k = 1 : size(base_binary_code, 2)
    r = 0;
    for i = 1 : 8
        r = r + mlookup{i}(base_binary_code(i, k) + 1);
    end
    all_distance(k) = r;
end
[s_distance ] = sort(all_distance);

%%
file_name = ['result\' encoding_scheme '_' num2str(code_length) ...
    '_' num2str(num_partition) '.mat'];
save(file_name, 'all_dist', '-append');
%% debug
selected = 2700;
tic;
[diag31] = ...
    MmexTestTrueNeighbors(...
        query_binary_code(:, selected), ...
        base_binary_code, ...
        {num_partition, D_diagonal}, ...
        gnd(:, selected), ...
        num_candidate, ...
        topks, ...
        Xtest, ...
        Xbase, ...
        {1});
toc;

% plot(diag31.recall(:, end), 'r');

%
db3 = split_3_32(base_binary_code);
q = split_3_32(query_binary_code(:, selected));

d = 0;
for k = 1 : 3
    d = d + D_diagonal{k}(q(k) + 1, db3(k, :) + 1);
end

[~, idx] = sort(d);
idx = idx(1 : 10^4);
vec_indicator = zeros(1, 10^6);
vec_indicator(gnd(:, selected) + 1) = 1;
r = vec_indicator(idx);

r = cumsum(r);
rec = r / 20000;
pre = r / [1 : 10^4];

norm(rec(:) - diag31.recall(:, end)) / norm(rec)

%% debug
for i = 1 : 3
    for j = 1 : 3
        assert(norm(E{i, j}' - E{j, i}, 'fro') == 0)
    end
end

%% check E
E = MmexComputeE(base_binary_code, num_partition);

db31 = db3 + 1;
num_bits = [11 11 10];
mE = cell(3, 3);
for i = 1 : 3
    for j = 1 : 3
        mE{i, j} = zeros(2^num_bits(i), 2^num_bits(j));
    end
end

for i = 1 : size(base_binary_code, 2)
    for u = 1 : 3
        for v = 1 : 3
            idx1 = db31(u, i);
            idx2 = db31(v, i);
            mE{u, v}(idx1, idx2) =  mE{u, v}(idx1, idx2) + 1;
        end
    end
end

for i = 1 : 3
    for j = 1 : 3
        norm(mE{i, j} - E{i, j}, 'fro')
    end
end

%% check G
[G, centers, error] = MmexComputeG(...
    base_binary_code, ...
    Xbase, ...
    E);

%%
su = 1;
sv = 2;
sm = 70;
sn = 80;

s = 0;
for i = 1 : 10^6
    u = base_binary_code(su, i);
    u = double(u) + 1;
    if u ~= su
        continue;
    end
    
    for j = 1 : 10^6
        v = base_binary_code(sv, j);
        v = double(v) + 1;
        if v ~= sv
            continue;
        end
        t = Xbase(:, i) - Xbase(:, j);
        t = t .* t;
        s = s + sum(t);
    end
end
norm(G{su, sv}(sm, sn) - s) / norm(s)

%%  G->centers
mCenters = cell(3, 1);
num_buckets = [2048, 2048, 1024];
for i = 1 : 3
    mCenters{i} = zeros(128, num_buckets(i));
end
for i = 1 : 10^6
    for u = 1 : 3
        idx = db31(u, i);
        mCenters{u}(:, idx) = mCenters{u}(:, idx) + ...
            Xbase(:, i);
    end
end
for i = 1 : 3
    num = num_buckets(i);
    for u = 1 : num
        if (E{i, i}(u, u))
            mCenters{i}(:, u) = mCenters{i}(:, u) ...
                / E{i, i}(u, u);
        end
    end
end

s = 0;
for u = 1 : 3
    for m = 1 : num_buckets(u)
        s = s + norm(mCenters{u}(:, m) - centers{u}(:, m));
    end
end
s
%% G->errors
mErrors = cell(3, 1);

for i = 1 : 3
    mErrors{i} = zeros(num_buckets(i), 1);
end
for i = 1 : 10^6
    for u = 1 : 3
        idx = db31(u, i);
        mErrors{u}(idx) = mErrors{u}(idx) + norm(mCenters{u}(:, idx) - ...
            Xbase(:, i))^2;
    end
end
%%
for i = 1 : 3
    num = num_buckets(i);
    for u = 1 : num
        if (E{i, i}(u, u))
            mErrors{i}(u) = mErrors{i}(u) ...
                / E{i, i}(u, u);
        end
    end
end

s = 0;
for u = 1 : 3
    for m = 1 : num_buckets(u)
        s = s + norm(mErrors{u}(m) - error{u}(m));
    end
end
s
%%


mG = cell(3, 3);
for i = 1 : 3
    for j = 1 : 3
        mG{i, j} = zeros(2^num_bits(i), 2^num_bits(j));
    end
end
for u = 1 : 3
    for v = 1 : 3
        for m = 1 : num_buckets(u)
            for n = 1 : num_buckets(v)
                mG{u, v}(m, n) = E{u, u}(m, m) * E{v, v}(n, n) * ...
                    (norm(mCenters{u}(:, m) - mCenters{v}(:, n))^2 + ...
                    mErrors{u}(m) + mErrors{v}(n));
            end
        end
        
    end
end
%%
s2 = 0;
s = 0;
for u = 1 : 3
    for v = 1 : 3
        s = s + norm(mG{u, v} - G{u, v}, 'fro');
        s2 = s2 + norm(G{u, v}, 'fro');

    end
end
s
s2
s / s2

%% performance on the training set
exeExaustiveKNN(str_base, str_base, ...
    str_gnd_train_train, 20000);


[~, gnd_train_train, ~] = read_gnd(str_gnd_train_train, 1000, 'double');

%%
tic
[D_diagonal, obj] = opt_diagonal(...
    binary_code, ...
    num_partition, ...
    Xtraining);
toc

%%
rng(0);
selected = randperm(10^5, 10^4);

%%

save_mat(query_binary_code, 'query_binary_code', 'uint8');
save_mat(base_binary_code, 'base_binary_code', 'uint8');
save_mat(gnd, 'gnd', 'int32');
save_mat(Xtest, 'Xtest', 'double');
save_mat(Xbase, 'Xbase', 'double');
%%
save_mat(R, 'R', 'double');

for i = 1 : 8
    save_mat(all_D{i}, ['center_' num2str(i)], 'double');
end
    
%%
tic;
[r_diag] = ...
    MmexTestTrueNeighbors(...
        binary_code(:, selected), ...
        binary_code, ...
        {num_partition, D_diagonal}, ...
        gnd_train_train(:, selected), ...
        num_candidate, ...
        1000, ...
        Xtraining(:, selected), ...
        Xtraining, ...
        {1});
toc;
%%

all_data = cell(10^5, 1);

%%
for i = 1 : 10^5
    all_data{i} = Xbase;
end


%%

tic;
r_hamm = MmexTestTrueNeighbors(...
    binary_code(:, selected), ...
    binary_code, ...
    [], ...
    gnd_train_train(:, selected), ...
    num_candidate, ...
    1000, ...
    Xtraining(:, selected), ...
    Xtraining, ...
        {0});
toc;

%%
plot(all_perf{1}.precision(:, end), 'b');
hold on;
plot(all_perf{2}.precision(:, end), 'k');
plot(all_perf{3}.precision(:, end), 'g');
plot(all_perf{4}.precision(:, end), 'r');

all_perf{1}.dist_ratio
plot(all_perf{1}.dist_ratio(:, end), 'b');
hold on;
plot(all_perf{2}.dist_ratio(:, end), 'k');
plot(all_perf{3}.dist_ratio(:, end), 'g');
plot(all_perf{4}.dist_ratio(:, end), 'r');

%%
    
%%
[R2, all_D2, ~, obj2]  = ck_means(...
    Xtraining, ...
    [32, 32, 32, 32], ...
    [256, 256, 256, 256], ...
    10, ...
    [], [], 1);
    
    
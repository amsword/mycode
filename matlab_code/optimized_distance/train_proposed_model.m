
if strcmp(type, 'Tiny80M') && iscell(Xbase)
    Xbase = read_mat(str_base, [data_format '=>' data_format]);
end
if strcmp(encoding_scheme, 'PQ') || ...
        strcmp(encoding_scheme, 'ck-means')
    file_name = ['result\' ...
        encoding_scheme '_' ...
        num2str(code_length) '_' ...
        num2str(num_pq_partitions) ...
        '_' num2str(num_partition) ...
        '.mat'];
elseif strcmp(encoding_scheme, 'ITQ') || ...
        strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'SH') || ...
        strcmp(encoding_scheme, 'KSH')
    file_name = ['result\' ...
        encoding_scheme '_' ...
        num2str(code_length) ...
        '_' num2str(num_partition) ...
        '.mat'];
    
else
    error('dfsd');
end
file_name_model = file_name;

if is_load_model && exist(file_name_model, 'file')
    fprintf('load model\n');
    load(file_name_model, ...
        'D_diagonal', ...
        'D_dense', ...
        'aux', ...
        'centers', ...
        'errors');
else
    fprintf('generate model\n');
    if strcmp(type, 'Tiny80M')
        [D_diagonal, obj, ...
            D_dense, obj_dense, ...
            aux, centers, errors] = ...
            merge_opt2(...
            base_binary_code, ...
            num_partition, ...
            {'float', str_base});
    else
        [D_diagonal, obj, ...
            D_dense, obj_dense, ...
            aux, centers, errors] = ...
            merge_opt2(...
            base_binary_code, ...
            num_partition, ...
            Xbase);
    end
    save(file_name_model, ...
        'D_diagonal', ...
        'D_dense', ...
        'aux', ...
        'centers', ...
        'errors', ...
        '-v7.3');
end
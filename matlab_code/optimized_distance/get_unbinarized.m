function unbinarized = get_unbinarized(X, encoding_scheme, hash_func)

if strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    W = hash_func;
    unbinarized = bsxfun(@plus, W(1 : end - 1, :)' * X, W(end, :)');
elseif strcmp(encoding_scheme, 'SH')
    [~, unbinarized] = hash_code_SH(X', hash_func);
elseif strcmp(encoding_scheme, 'KSH')
    [~, unbinarized] = ksh_encoding(X, hash_func);
else
    error('fds');
end
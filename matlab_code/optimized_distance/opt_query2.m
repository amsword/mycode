function [aux] = opt_query2(E)

%
matE = ConstructMat(E);

inv_matE = pinv(matE);
aux = bsxfun(@times, inv_matE, diag(matE)');
aux = aux';
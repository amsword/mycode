%%
all_distortion2 = cell(sub_dim_num, 1);
all_sq_distortion = cell(sub_dim_num, 1);
all_count2 = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    idx_start = 1 + (i - 1) * sub_dim_length;
    idx_end = i * sub_dim_length;
    
    sub_train = Xtraining(idx_start : idx_end, :);
    sub_dim_center = all_centers{i};
    assignment = all_assignment(i, :);

    sq_sub_train = sum(sub_train .^ 2, 1);
    distortion2 = zeros(num_centers(i), 1);
    count2 = zeros(num_centers(i), 1);
    sq_distortion = zeros(num_centers(i), 1);
    
    for j = 1 : numel(assignment)
        idx = assignment(j);
        v = sub_train(:, j) - sub_dim_center(:, idx);
        v2 = sum(v .^ 2);
        
        distortion2(idx) = distortion2(idx) + v2;
        sq_distortion(idx) = sq_distortion(idx) + v2 * v2;
        count2(idx) = count2(idx) + 1;
    end
    
    for j = 1 : numel(count2)
        if count2(j) > 0.5
            distortion2(j) = distortion2(j) / count2(j);
            sq_distortion(j) = sq_distortion(j) / count2(j);
        else
            distortion2(j) = 0;
        end
    end
    all_count2{i} = count2;
    all_distortion2{i} = distortion2;
    all_sq_distortion{i} = sq_distortion;
end


%%

s = 0;
for i = 1 : numel(all_count2)
    s = s + norm(all_distortion2{i} - all_distortion{i});
end
s

clear all_cri;
k = 1;
if strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'GIST1M3') || ...
        strcmp(type, 'MNISTE')
    all_cri{k} = {0, int32(topks), gnd}; k = k + 1; % precision and recall
    all_cri{k} = {1, gnd}; k = k + 1; % precision with the gnd changed
    all_cri{k} = {2, gnd};  k = k + 1; % average distance ratio
    
elseif strcmp(type, 'MNIST')
    all_cri{k} = {3, gnd_query, gnd_base}; k = k +1; % the gnd is labeled
else
    error('fdsf');
end

profile on;
batch_size = 100;
batch_num = ceil(num_test_point / batch_size);

tic;

num_can = 1000;
r_code_sum = zeros(num_can, 1);


for batch_idx = 1 : batch_num
    [num2str(batch_idx) '/' num2str(batch_num)]
    idx_start = (batch_idx - 1) * batch_size + 1;
    idx_end = batch_idx * batch_size;
    idx_end = min(idx_end, num_test_point);
    
    dist_xtest_xtraining = zeros(idx_end - idx_start + 1, num_point);
    
    for i = 1 : sub_dim_num
        idx = bsxfun(@plus, idx_quantized_sub_center(i, idx_start : idx_end)', (all_assignment(i, :) - 1) * num_centers(i));
        dist_xtest_xtraining = dist_xtest_xtraining + all_lookup_table{i}.table(idx);
    end
%     [sorted_xtest_xtraining, idx_xtest_xtraining] = sort(dist_xtest_xtraining, 2);
    idx_xtest_xtraining = mex_partial_sort(dist_xtest_xtraining', num_can);
    idx_xtest_xtraining = idx_xtest_xtraining';
    
    one_idx = (StestBase2(idx_start : idx_end, :) - 1) * (idx_end - idx_start + 1) + ...
        repmat([1 : (idx_end - idx_start + 1)]', 1, size(StestBase2, 2));
    
    fS = false(idx_end - idx_start + 1, num_point);
    fS(one_idx(:)) = true;
    
    result = jf_eval_small3(fS, idx_xtest_xtraining, 100);
    r_code_sum = r_code_sum + result.r_code_sum;
end
r_code_sum = r_code_sum / num_test_point;

toc;

profile viewer
profsave
%%
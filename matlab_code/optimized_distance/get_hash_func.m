if strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    x = load(file_name_binary_code, 'W');
    W = x.W;
    hash_func = W;
elseif strcmp(encoding_scheme, 'SH') || ...
        strcmp(encoding_scheme, 'KSH')
    x = load(file_name_binary_code, 'hash_func');
    hash_func = x.hash_func;
else
    error('df');
end
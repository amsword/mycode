%% run Lei's weighted Hamming distance
if strcmp(encoding_scheme, 'ITQ') || ...
        strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'SH') || ...
        strcmp(encoding_scheme, 'KSH')
    
    file_name = ['result\' ...
        encoding_scheme '_' num2str(code_length) ...
        '_Lei.mat'];
    
    % training
    get_hash_func;
    
    if exist(file_name, 'file')
        x = load(file_name, 'weight_para');
        weight_para = x.weight_para;
    else
        X = Xtraining;
        unbinarized = ...
            get_unbinarized(X, encoding_scheme, hash_func);
        %
        knn_used = floor(size(X, 2) * 0.02);
        [~, knn_graph] = read_gnd(str_gnd_train_train, knn_used, 'double');
        %
        weight_para = WhRankTrain2(unbinarized, knn_graph);
        save(file_name, 'weight_para');
        clear knn_graph;
    end
    %
    X = Xtest;
    query_unbinarized = get_unbinarized(Xtest, encoding_scheme, hash_func);
    
    x = load(file_name, 'all_perf_lei');
    
    is_run = true;
%     if ~isfield(x, 'all_time_lei')
%         is_run = true;
%     end
    
    clear all_dist;
    k = 1;
    
    all_dist{k} = {5, weight_para};
    if is_run
        %
        clear all_cri;
        all_cri{1} = {5};
        clear all_time_lei;
        for i = 1 : numel(all_dist)
            tic;
            i
            all_time_lei{i} = MmexTestTrueNeighbors(...
                query_unbinarized, ...
                base_binary_code, ...
                num_candidate, ...
                Xtest, ...
                Xbase, ...
                all_dist{i}, ...
                all_cri);
            toc;
        end
        save(file_name, 'all_time_lei', '-append');
    end
    
end
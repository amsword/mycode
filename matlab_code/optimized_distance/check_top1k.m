%%

tic;

gnd_topk = all_topks(4)

v2 = 0;
v3 = 0;
for idx_test_point = 1 : num_test_point
    if mod(idx_test_point, 100) == 0
    [num2str(idx_test_point) '/' num2str(num_test_point)]
    end
    for i = 1 : sub_dim_num
        sub_dim_idx_start = 1 + (i - 1) * sub_dim_length;
        sub_dim_idx_end = i * sub_dim_length;
        
        d1 = sqdist(Xtest(sub_dim_idx_start : sub_dim_idx_end, idx_test_point), ...
            Xtraining(sub_dim_idx_start : sub_dim_idx_end, StestBase2(idx_test_point, 1 : gnd_topk)));
        
        
        idx = bsxfun(@plus, idx_quantized_sub_center(i, idx_test_point)', (all_assignment(i, StestBase2(idx_test_point, 1 : gnd_topk)) - 1) * num_centers(i));
        d2 = all_lookup_table{i}.added_table(idx);
        d3 = all_lookup_table{i}.table(idx);
        
        v2 = v2 + sum((d1(:) - d2(:)) .^ 2);
        v3 = v3 + sum((d1(:) - d3(:)) .^ 2);
    end
end
v2
v3


toc;

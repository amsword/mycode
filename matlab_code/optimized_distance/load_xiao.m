function [all_perf_xiao,  all_map_xiao]= load_xiao(...
    result_dir, ...
    HashingType, ...
    code_length)

if strcmp(HashingType, 'ITQ')
    file_name = [result_dir ...
        HashingType '_' ...
        num2str(code_length) '_xiao.mat'];
else
    error('dfd');
end

x = load(file_name, 'all_perf_xiao', 'all_map_xiao');

all_map_xiao = [];
all_perf_xiao = [];
if isfield(x, 'all_perf_xiao')
    all_perf_xiao = x.all_perf_xiao;
end

if isfield(x, 'all_map_xiao')
    all_map_xiao = x.all_map_xiao;
end
sub_dim_num = 2;
sub_dim_num_center = 1024;
%%
Xtraining = read_mat(src_file.c_train, 'double');

all_centers = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    c = read_mat(['sub_train_' num2str(i) '_center.double.bin'], 'double');
    all_centers{i} = c;
end

%%
all_assignment = zeros(sub_dim_num, size(Xtraining, 2));
for i = 1 : sub_dim_num
    c = read_mat(['sub_train_' num2str(i) '_assign.int.bin'], 'int32');
    c = c' + 1;
    all_assignment(i, :) = c;
end
sub_dim_length = size(Xtraining, 1) / sub_dim_num;
%%
all_distortion = cell(sub_dim_num, 1);
all_count = cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    
    idx_start = 1 + (i - 1) * sub_dim_length;
    idx_end = i * sub_dim_length;
    
    sub_train = Xtraining(idx_start : idx_end, :);
    sub_dim_center = all_centers{i};
    assignment = all_assignment(i, :);
    
    sq_sub_train = sum(sub_train .^ 2, 1);
    distortion = zeros(sub_dim_num_center, 1);
    count = zeros(sub_dim_num_center, 1);
    
    for j = 1 : numel(assignment)
        idx = assignment(j);
        distortion(idx) = distortion(idx) + sq_sub_train(j);
        count(idx) = count(idx) + 1;
    end
    
    for j = 1 : numel(count)
        if count(j) > 0.5
            distortion(j) = distortion(j) / count(j) - sum(sub_dim_center(:, j) .^ 2);
        else
            distortion(j) = 0;
        end
    end
    all_count{i} = count;
    all_distortion{i} = distortion;
end

%%
origin_table = cell(numel(sub_dim_num), 1);

for i = 1 : sub_dim_num
    c = all_centers{i};
    t = sqdist(c, c);
    origin_table{i} = t;
end
% build lookup table for each sub dim
all_centers_distortion =  cell(sub_dim_num, 1);
for i = 1 : sub_dim_num
    all_centers_distortion{i} = origin_table{i} + bsxfun(@plus, all_distortion{i}, all_distortion{i}');
end
%%
save_multi_mat(all_centers_distortion, 'origin_table_distortion_');

%%
if strcmp(type, 'Tiny80M')
    exeTestTopk([pwd '\'], 'origin_table_distortion_', gnd_file.STestBase, 100000);
else
    exeTestTopk([pwd '\'], 'origin_table_distortion_', gnd_file.STestBase, 10000);
end
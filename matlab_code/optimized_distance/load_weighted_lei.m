function [all_perf_lei,  all_map_lei]= load_weighted_lei(...
    result_dir, ...
    HashingType, ...
    code_length)

if strcmp(HashingType, 'LSH') || ...
        strcmp(HashingType, 'ITQ') || ...
        strcmp(HashingType, 'SH') || ...
        strcmp(HashingType, 'KSH')
    file_name = [result_dir ...
        HashingType '_' ...
        num2str(code_length) '_lei.mat'];
else
    error('dfd');
end

x = load(file_name, 'all_perf_lei', 'all_map_lei');

all_map_lei = [];
all_perf_lei = [];
if isfield(x, 'all_perf_lei')
    all_perf_lei = x.all_perf_lei;
end

if isfield(x, 'all_map_lei')
    all_map_lei = x.all_map_lei;
end
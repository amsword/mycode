
baseline = read_mat('origin_table_perf_1_10_50_100_400_1000_10000_20000', 'double');
% best_ind = read_mat('origin_table_distortion_perf_1_10_50_100_400_1000_10000_20000', 'double');


ratio_baseline = read_mat('origin_table__avg_distance_ratio_perf', 'double');
% ratio_best_ind = read_mat('origin_table_distortion__avg_distance_ratio_perf', 'double');

map_baseline = read_mat('origin_table_map_perf_100_1000_10000', 'double');
% map_best_ind = read_mat('origin_table_distortion_map_perf_10000_20000', 'double');

map5k_baseline = read_mat('origin_table_map_perf_5000', 'double');

function [G, centers, errors] = MmexComputeG(...
    base_binary_code, ...
    Xbase, ...
    E)

%%
assert(isa(base_binary_code, 'uint8'));

assert(isa(E, 'cell'));

for i = 1 : numel(E)
    assert(isa(E{i}, 'double'));
end

if isa(Xbase, 'double') || isa(Xbase, 'float')
    [G, centers, errors] = mexComputeG(...
        base_binary_code, ...
        Xbase, ...
        E);
elseif iscell(Xbase)
    [G, centers, errors] = mexComputeGExternalMemory(...
        base_binary_code, ...
        Xbase, ...
        E);
else
    error('dfsdf');
end

%% just for test, implement it by matlab
% center
% centers = cell(4, 1);
% for i = 1 : 4
%     centers{i} = zeros(128, 256);
% end
% for i = 1 : 100000
%     code = double(binary_code(:, i));
%     code = code + 1;
%     for u = 1 : 4
%         idx = code(u);
%         centers{u}(:, idx) = centers{u}(:, idx) + Xtraining(:, i);
%     end
% end
% 
% for u = 1 : 4
%     for i = 1 : 256
%         e = E{u, u}(i, i);
%         if e > 0.1
%             centers{u}(:, i) = centers{u}(:, i) / E{u, u}(i, i);
%         end
%     end
% end
% % errors
% errors = cell(4, 1);
% for i = 1 : 4
%     errors{i} = zeros(256, 1);
% end
% for i = 1 : 100000
%     code = double(binary_code(:, i));
%     code = code + 1;
%     for u = 1 : 4
%         idx = code(u);
%         e = centers{u}(:, idx) - Xtraining(:, i);
%         e = sum(e .* e);
%         errors{u}(idx) = errors{u}(idx) + e; 
%     end
% end
% 
% for u = 1 : 4
%     for i = 1 : 256
%         e = E{u, u}(i, i);
%         if e > 0.1
%             errors{u}(i) = errors{u}(i) / E{u, u}(i, i);
%         end
%     end
% end
% 
% % compute
% G2 = cell(4, 4);
% for i = 1 : 4
%     for j = 1 : 4
%         G2{i, j} = zeros(256, 256);
%     end
% end
% 
% for u = 1 : 4
%     for v = 1 : 4
%         for i = 1 : 256
%             for j = 1 : 256
%                 G2{u, v}(i, j) = E{u, u}(i, i) * E{v, v}(j, j) * ...
%                     (sqdist(centers{u}(:, i), centers{v}(:, j)) + ...
%                     errors{u}(i) + errors{v}(j));
%             end
%         end
%         norm(G2{u, v} - G{u, v}, 'fro') / norm(G{u, v}, 'fro')
%     end
% end


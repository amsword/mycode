function exeTestTopk(str_dir, str_prefix, str_gnd, num_candidate)
cmd = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\HashCode\Temps_lookup_table_opt\TestCCDistance.exe ';
cmd = [cmd str_dir ' '];
cmd = [cmd str_prefix ' '];
cmd = [cmd str_gnd];

if exist('num_candidate', 'var')
    cmd = [cmd ' ' num2str(num_candidate)];
end

system(cmd);
batch_size = 100;
batch_num = ceil(num_test_point / batch_size);

tic;

v2 = 0;
v3 = 0;
for batch_idx = 1 : batch_num
    [num2str(batch_idx) '/' num2str(batch_num)]
    idx_start = (batch_idx - 1) * batch_size + 1;
    idx_end = batch_idx * batch_size;
    idx_end = min(idx_end, num_test_point);
    
    dist_xtest_xtraining = zeros(idx_end - idx_start + 1, num_point);
    
    for i = 1 : sub_dim_num
        sub_dim_idx_start = 1 + (i - 1) * sub_dim_length;
        sub_dim_idx_end = i * sub_dim_length;
        
        d1 = sqdist(Xtest(sub_dim_idx_start : sub_dim_idx_end, idx_start : idx_end), ...
            Xtraining(sub_dim_idx_start : sub_dim_idx_end, :));
        
        idx = bsxfun(@plus, idx_quantized_sub_center(i, idx_start : idx_end)', (all_assignment(i, :) - 1) * num_centers(i));
        d2 = all_lookup_table{i}.added_table(idx);
        d3 = all_lookup_table{i}.table(idx);
        v2 = v2 + sum((d1(:) - d2(:)) .^ 2);
        v3 = v3 + sum((d1(:) - d3(:)) .^ 2);
    end

end
v2
v3
toc;

baseline = read_mat('origin_table_perf_1_10_50_100_400_1000_10000_20000', 'double');

best_ind = read_mat('origin_table_distortion_perf_1_10_50_100_400_1000_10000_20000', 'double');

%% no w
dir_curr = [pwd '\'];
load_no_w;

best_no_w = best;

%% w
load_w;
best_w = best;
%%
is_recall = 1;
is_pre = 1;
% is_pr;
all_topks = [1, 10, 50, 100, 400, 1000, 10000, 20000];

% i = numel(all_topks);
i = 1;
for i = 1 : size(baseline, 2)
    
    best_w_idx = find_best_idx(best_w, i);
    best_no_w_idx = find_best_idx(best_no_w, i);
    
    selected_x = 1 : 10^4;
    
%     if all_topks(i) == 100
%         selected_x = 1 : 100;
%     elseif all_topks(i) == 1000
%         selected_x = 1 : 1000;
%     end
    
%     selected_x = 6000 : 10000;
    
    
    if is_pre
        figure;
        hold on;
        plot(selected_x, best_w{best_w_idx}(selected_x, i) * all_topks(i)  ./ (selected_x(1) : selected_x(end))', 'r');
        plot(selected_x, best_no_w{best_no_w_idx}(selected_x, i) * all_topks(i)  ./ (selected_x(1) : selected_x(end))', 'b');
        plot(selected_x, best_ind(selected_x, i) * all_topks(i)  ./ (selected_x(1) : selected_x(end))', 'g');
        plot(selected_x, baseline(selected_x, i) * all_topks(i) ./ (selected_x(1) : selected_x(end))', 'k--');
        
        grid  on;
        set(gca, 'FontSize', 14);
        
        legend('LearnWCO', 'LearnCO', 'LearnInd', 'BaseCC', 'Location', 'Best');
        
%         saveas(gca, ['pre_' num2str(all_topks(i)) '.eps'], 'psc2');
    end
    
    if is_recall
        figure;
        hold on;
        
        plot(selected_x, best_w{best_w_idx}(selected_x, i) , 'r');
        plot(selected_x, best_no_w{best_no_w_idx}(selected_x, i) , 'b');
        plot(selected_x, best_ind(selected_x, i) , 'g');
        plot(selected_x, baseline(selected_x, i), 'k--');
        
        grid  on;
        set(gca, 'FontSize', 14);
        
        legend('LearnWCO', 'LearnCO', 'LearnInd', 'BaseCC', 'Location', 'Best');
%         saveas(gca, ['rec_' num2str(all_topks(i)) '.eps'], 'psc2');
    end
end

function y = split_3_32(x)

num = size(x, 2);

y = zeros(3, num);

t = int32(x(2, :));
t = bitand(t, int32(7));
t = bitshift(t, 8);
t = t + int32(x(1, :));

y(1, :) = t;

t = int32(x(3, :));
t = bitand(t, int32(63));
t = bitshift(t, 5);
y(2, :) = t;
t = int32(x(2, :));
t = bitshift(t, -3);
y(2, :) = y(2, :) + double(t);

t = int32(x(4, :));
t = bitshift(t, 2);
y(3, :) = t;
t = int32(x(3, :));
t = bitshift(t, -6);
y(3, :) = y(3, :) + double(t);






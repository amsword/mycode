function [D, obj] = opt_dense2(...
    E, G)


%%
matE = ConstructMat(E);
matG = ConstructMat(G);

inv_matE = pinv(matE);
D =  inv_matE * matG * inv_matE;

if nargout == 2
    obj = loss_approximate_errors(E, G, D);
end

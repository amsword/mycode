dir_curr = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\Data_HashCode\SIFT1M3\working_opt_dist\3_1024_1024\k_means_by_wj\';

%%
mu  = 0;
for lambda = [10^5]
        lambda
        lambda_non_equal = lambda;
        lambda_equal = lambda * 10;
        
        max_iter = 100;
        str_assignment = [dir_curr  'assignment_train_sift'];
        % str_training = '\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\Data_HashCode\SIFT1M3\working_opt_dist\scaled_sift_1m.double.bin';
        str_training = src_file.c_base;
        
        %
%         is_all_zeros = 1;
%         str_in = 'zero_';
%         str_out0 = [str_in num2str(lambda) '_' num2str(mu) '_out_'];
%         
%         exeOptimizeCCDistance(str_training, str_assignment, ...
%             str_in, str_out0, is_all_zeros, lambda, mu, max_iter, num_centers(1));
        %
        is_all_zeros = 0;
        str_in = 'origin_table_';
        str_out1 = [str_in num2str(lambda) '_' num2str(mu) '_out_not_scaled_'];
        exeOptimizeCCDistance(str_training, str_assignment, ...
            str_in, str_out1, is_all_zeros, lambda, mu, max_iter, num_centers(1));
        
        %
%         x0 = read_mat([str_out0 'ind'], 'double');
%         x1 = read_mat([str_out1 'ind'], 'double');
        %
%         figure;
%         plot(x0, 'b');
%         hold on;
%         plot(x1, 'r');
end

%%
x0 = read_mat([str_out0 'ind'], 'double');
x1 = read_mat([str_out1 'ind'], 'double');
%%
x0 = read_mat('zero_1000_out_ind', 'double');
x1 = read_mat('origin_table_1000_out_not_scaled_ind', 'double');
figure;
plot(x0, 'b');
hold on;
plot(x1, 'r');

%%
clc;
baseline = read_mat('origin_table_perf_1_10_50_100_400_1000_10000_20000', 'double');

clear best;
idx = 1;
best{idx} = read_mat('origin_table_weighted1_1_10_50_100_400_1000_10000_20000', 'double');
idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_1000_out_not_scaled_perf', 'double');
% idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_10000_out_not_scaled_perf', 'double');
% idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_100000_out_not_scaled_perf', 'double');
% idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_1000000_out_not_scaled_perf_1_10_50_100_400_1000', 'double');
% idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_10000000_out_not_scaled_perf_1_10_50_100_400_1000', 'double');
% idx = idx + 1;
% 
% best{idx} = read_mat('origin_table_0_0_0_100000000_out_not_scaled_perf_1_10_50_100_400_1000', 'double');
% idx = idx + 1;
% best1 = read_mat('origin_table_0_0_0_10_out_not_scaled_perf', 'double');

% best2 = read_mat('origin_table_0_0_0_100000_out_not_scaled_perf', 'double');
% best3 = read_mat('origin_table_1000000_0_out_not_scaled_perf', 'double');
% best4 = read_mat('origin_table_1e-006_0_out_not_scaled_perf', 'double');
% best2 = read_mat('zero_100000_out_perf', 'double');
%%
for i = 1 : size(baseline, 2)
    figure;
    plot(baseline(:, i), 'b');
    hold on;
    
    for k = 1 : numel(best)
        plot(best{k}(:, i), 'r');
%         pause;
        
    end
%     plot(best1(:, i), 'k');
%     plot(best2(:, i), 'g');
%     plot(best3(:, i), 'y');
%     plot(best4(:, i), 'y');
%     legend('base', '1000000000', '100000000', '10000000', '1000000');
%     xlim([0, 1000]);
end
pause;
close all;
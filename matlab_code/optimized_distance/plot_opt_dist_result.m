%%
close all;
curve_colors =  [[255 0 0] / 255; ...
    [0 0 0] / 255; ...
    [0 255 0] / 255; ...
    [0 0 255] / 255; ...
    [80 136 170] / 255];
%
all_type = {'SIFT1M3', 'GIST1M3', 'MNISTE'};
% all_type = {'MNISTE'};
all_encoding_scheme = {'LSH', 'ITQ', 'KSH', 'PQ'};
% all_encoding_scheme = {'LSH'};

name_oa = 'OAD';
name_os = 'OSD';

%%
close all;
for ktype = 1 : numel(all_type)
    type = all_type{ktype};
    % SIFT1M3, GIST1M3; MNISTE
    for code_length = [64]
        %         close all;
        if code_length == 32
            all_num_partition = [3, 4];
        elseif code_length == 64
%             all_num_partition = [6];
            all_num_partition = [6 8];
        end
        for num_partition = all_num_partition
            num_pq_partitions = num_partition;
            
            %
            if strcmp(type, 'SIFT1M3') || ...
                    strcmp(type, 'GIST1M3') || ...
                    strcmp(type, 'MNISTE')
                result_dir = ['H:\Data_HashCode\' type '\working_opt_distance\result\'];
            else
                error('dfds');
            end
            cd(result_dir);
            %             return;
            for k_encoding = 1 : numel(all_encoding_scheme)
                encoding_scheme = all_encoding_scheme{k_encoding};
                save_dir = 'C:\Users\J\OneDrive\Documents\Research\MyPaper\optimized_distance\ACMMM14\figs\2\';
                save_dir = [save_dir type '\'];
                %
                if ~strcmp(encoding_scheme, 'PQ')
                    
                    plot_hamming_precision_s2;
                    plot_hamming_ratio_s2;
                else
                    if mod(num_partition, 4) ~= 0
                        continue;
                    end
                    %
                    plot_pq_precision_s;
                    plot_pq_ratio_s;
                end
            end
        end
    end
end
return;
%%
curve_colors =  [[255 0 0] / 255; ...
    [0 0 0] / 255; ...
    [0 255 0] / 255; ...
    [0 0 255] / 255; ...
    [80 136 170] / 255];

close all;

all_encoding_scheme = {'LSH', 'ITQ', 'KSH', 'PQ'};
% all_encoding_scheme = {'ITQ'};

% all_encoding_scheme = {'PQ'};

for ktype = 1 : numel(all_type)
% for ktype = 2 : 2
    type = all_type{ktype};
    if strcmp(type, 'SIFT1M3') || ...
            strcmp(type, 'GIST1M3') || ...
            strcmp(type, 'MNISTE')
        result_dir = ['H:\Data_HashCode\' type '\working_opt_distance\result\'];
    else
        error('dfds');
    end
    cd(result_dir);
    
    save_dir = 'C:\Users\J\OneDrive\Documents\Research\MyPaper\optimized_distance\ACMMM14\figs\2\';
    save_dir = [save_dir type '\'];
    
    for k_encoding = 1 : numel(all_encoding_scheme)
        encoding_scheme = ...
            all_encoding_scheme{k_encoding};
%         plot_opt_dist_map(...
%             result_dir, ...
%             encoding_scheme, ...
%             save_dir, ...
%             curve_colors);
        plot_opt_dist_map;
    end
end

%%
plot_opt_dist_different_t;
%%


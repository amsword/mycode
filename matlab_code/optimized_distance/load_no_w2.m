function result = load_no_w2(dir_curr)

beta = 0;
coef = 1;

all_lambda = [[1 10 10^2 10^3 10^4 10^5 10^6 10^7 10^8]];
idx = 1;

for alpha = [0]
    for lambda = all_lambda
        
        str_out_prefix = ['origin_table_' 'out_' ...
            'la' num2str(lambda) '_al' num2str(alpha) '_be' num2str(beta) '_co' num2str(coef) '_'];
        
        str_out_put_prefix = [dir_curr 'no_w\' str_out_prefix];
        
        if exist([str_out_put_prefix 'perf_1_10_50_100_400_1000_10000_20000'], 'file')
            result.recall{idx} = read_mat([str_out_put_prefix 'perf_1_10_50_100_400_1000_10000_20000'], 'double');
        end
        
        file = [str_out_put_prefix '_avg_distance_ratio_perf'];
        if exist(file, 'file')
            result.ratio{idx} = read_mat(file, 'double');
        end
        
        result.para(idx) = lambda;
        
        file = [str_out_put_prefix 'map_perf_100_1000_10000'];
        if exist(file, 'file')
            result.map(:, idx) = read_mat(file, 'double');
        end
        
        file = [str_out_put_prefix 'map_perf_5000'];
        if exist(file, 'file')
            result.map5k(:, idx) = read_mat(file, 'double');
        end
        
        file = [str_out_put_prefix 'map_validation_perf_900'];
       if exist(file, 'file')
            result.val(:, idx) = read_mat(file, 'double');
       else
           file = [str_out_put_prefix 'map_validation_perf_4500'];
           if exist(file, 'file')
               result.val(:, idx) = read_mat(file, 'double');
           end
       end
       
        
        idx = idx + 1;
    end
end
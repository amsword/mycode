% add path for matlab
cd('C:\Users\uqjwan34\Desktop\codes2\HashCode');
jf_conf;
% clear;

% File name
type = 'SIFT1M3'; % Labelme

% SIFT1M3
% GIST1M3
% Tiny80M
% MNIST
% MNISTE
opt_dist_get_all_file_names;

cd(working_dir);

%% load original data
% if strcmp(type, 'Tiny80M')
Xtraining = read_mat(str_training, ...
    [data_format '=>' data_format]);
Xtraining = double(Xtraining);
% end
Xtest = read_mat(str_test, ...
    [data_format '=>' data_format]);
Xtest = double(Xtest);
%%
if strcmp(type, 'GIST1M3') || ...
        strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'Tiny80M')
    Xbase = read_mat(str_base, [data_format '=>' data_format]);
elseif strcmp(type, 'Tiny80M')
    error('not used');
    Xbase = {'float', str_base};
elseif strcmp(type, 'MNIST') || ...
        strcmp(type, 'MNISTE')
    Xbase = Xtraining;
else
    error('dfdsf');
end
%
%% configure
code_length = 64;
num_candidate = 10000;
num_partition = 6;
num_pq_partitions = num_partition;
is_load_model = 1;
encoding_scheme = 'ITQ'; % ITQ; LSH, PQ, ck-means

if strcmp(type, 'Tiny80M')
    topks = floor(79292017 * 0.0001);
    topks = [1 topks topks * 2];
elseif strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'GIST1M3')
    topks = [1 10 100 1000 10000, 20000];
elseif strcmp(type, 'MNISTE')
    topks = [1 600 1200];
elseif strcmp(type, 'MNIST')
    topks = [];
else
    error('dfdsfs');
end

num_add = mod(code_length, num_partition);
num_hold = num_partition - num_add;
base = floor(code_length / num_partition);
cost = 2 ^ base * num_hold + 2 ^ (base + 1) * num_add;
cost = cost * cost;
cost = cost * 8 / 1024^3
assert(cost < 1);

if strcmp(encoding_scheme, 'PQ') || ...
        strcmp(encoding_scheme, 'ck-means')
    file_name_binary_code = ...
        ['result\' encoding_scheme '_' num2str(code_length) ...
        '_' num2str(num_pq_partitions) ...
        '.mat'];
elseif strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    file_name_binary_code = ...
        ['result\' encoding_scheme '_' num2str(code_length) ...
        '.mat'];
else
    error('dfsd');
end
%%

if strcmp(encoding_scheme, 'PQ') || ...
        strcmp(encoding_scheme, 'ck-means')
    file_name = ['result\' encoding_scheme '_' num2str(code_length) ...
        '_' num2str(num_pq_partitions) ...
        '.mat'];
    
    if ~exist(file_name, 'file')
        fprintf('online generate the codes\n');
        opt_code_generation;
        save(file_name, ...
            'base_binary_code', ...
            'query_binary_code', ...
            'R', ...
            'all_D', ...
            '-v7.3');
    else
        fprintf('load the binary code\n');
        x = load(...
            file_name, ...
            'base_binary_code', ...
            'query_binary_code', ...
            'all_D', ...
            'R');
        all_D = x.all_D;
        R = x.R;
        base_binary_code = x.base_binary_code;
        query_binary_code = x.query_binary_code;
    end
    
elseif strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    file_name = ['result\' encoding_scheme '_' num2str(code_length) ...
        '.mat'];
    
    if ~exist(file_name, 'file')
        fprintf('online generate the codes\n');
        opt_code_generation;
        save(file_name, ...
            'base_binary_code', ...
            'query_binary_code', ...
            'W', ...
            '-v7.3');
    else
        fprintf('load the binary code\n');
        x = load(...
            file_name, ...
            'base_binary_code', ...
            'query_binary_code', ...
            'W');
        base_binary_code = x.base_binary_code;
        query_binary_code = x.query_binary_code;
        
        W = x.W;
        
    end
end

%% train the model

if strcmp(type, 'Tiny80M') && iscell(Xbase)
    Xbase = read_mat(str_base, [data_format '=>' data_format]);
end
if strcmp(encoding_scheme, 'PQ')
    file_name = ['result\' ...
        encoding_scheme '_' ...
        num2str(code_length) '_' ...
        num2str(num_pq_partitions) ...
        '_' num2str(num_partition) ...
        '.mat'];
elseif strcmp(encoding_scheme, 'ITQ') || ...
        strcmp(encoding_scheme, 'LSH')
    file_name = ['result\' ...
        encoding_scheme '_' ...
        num2str(code_length) ...
        '_' num2str(num_partition) ...
        '.mat'];
else
    error('dfsd');
end

if is_load_model && exist(file_name, 'file')
    fprintf('load model\n');
    load(file_name, ...
        'D_diagonal', ...
        'D_dense', ...
        'aux', ...
        'centers', ...
        'errors');
else
    fprintf('generate model\n');
    if strcmp(type, 'Tiny80M')
        [D_diagonal, obj, ...
            D_dense, obj_dense, ...
            aux, centers, errors] = ...
            merge_opt2(...
            base_binary_code, ...
            num_partition, ...
            {'float', str_base});
    else
        [D_diagonal, obj, ...
            D_dense, obj_dense, ...
            aux, centers, errors] = ...
            merge_opt2(...
            base_binary_code, ...
            num_partition, ...
            Xbase);
    end
    save(file_name, ...
        'D_diagonal', ...
        'D_dense', ...
        'aux', ...
        'centers', ...
        'errors', ...
        '-v7.3');
end

%%
clear all_dist;
k = 1;

if strcmp(encoding_scheme, 'PQ') || ...
        strcmp(encoding_scheme, 'ck-means')
    pq_table = cell(num_pq_partitions, 1);
    for i = 1 : num_pq_partitions
        pq_table{i} = sqdist(all_D{i}, all_D{i});
    end
    all_dist{k} = {1, num_pq_partitions, pq_table}; k = k + 1;
    
    all_dist{k} = {4, num_pq_partitions, num_pq_partitions, ...
        R, all_D}; k = k + 1;
elseif strcmp(encoding_scheme, 'LSH') || ...
        strcmp(encoding_scheme, 'ITQ')
    all_dist{k} = {0}; k = k + 1;
end

all_dist{k} = {1, num_partition, D_diagonal}; k = k + 1;
all_dist{k} = {2, num_partition, D_dense}; k = k + 1;
all_dist{k} = {3, num_partition, aux, centers, errors}; k = k + 1;


%%
if strcmp(type, 'GIST1M3') || ...
        strcmp(type, 'SIFT1M3') || ...
        strcmp(type, 'Tiny80M')
    tic;
    [~, gnd] = read_gnd(...
        str_gnd, ...
        20000, ...
        data_format);
    toc;
elseif strcmp(type, 'MNISTE')
    [~, gnd] = read_gnd(...
        str_gnd, ...
        1200, ...
        data_format);
elseif strcmp(type, 'MNIST')
    gnd_query = read_mat(str_gnd_query, 'int32=>int32');
    gnd_base = read_mat(str_gnd_base, 'int32=>int32');
end

%%
clc;
get_topk_cri;

%%
clear all_perf;
file_name = ['result\' ...
    encoding_scheme '_' num2str(code_length) ...
    '_' num2str(num_partition) '.mat'];

x = load(file_name, 'all_perf');
if ~isfield(x, 'all_perf')
    for i = 1 : numel(all_dist)
        tic;
        i
        all_perf{i} = MmexTestTrueNeighbors(...
            query_binary_code, ...
            base_binary_code, ...
            num_candidate, ...
            Xtest, ...
            Xbase, ...
            all_dist{i}, ...
            all_cri);
        toc;
    end
    
    save(file_name, ...
        'topks', 'all_dist', ...
        'all_perf', '-append', '-v7.3');
else
    all_perf = x.all_perf;
end


%% mAP
file_name = ['result\' ...
    encoding_scheme '_' num2str(code_length) ...
    '_' num2str(num_partition) '.mat'];

x = load(file_name, 'all_map');
if ~isfield(x, 'all_map')
    clc;
    k = 1;
    clear all_cri;
    all_cri{k} = {4, int32(topks), gnd};  k = k + 1; % map
    
    clear all_map;
    for i = 1 : numel(all_dist)
        tic;
        i
        all_map{i} = MmexTestTrueNeighbors(...
            query_binary_code, ...
            base_binary_code, ...
            size(base_binary_code, 2), ...
            Xtest, ...
            Xbase, ...
            all_dist{i}, ...
            all_cri);
        toc;
    end
    save(file_name, ...
        'all_map', '-append', '-v7.3');
end

%%
run_weightedlei;
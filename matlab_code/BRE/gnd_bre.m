function [Ktrain, Dist, is, js, nns, nns2] = gnd_bre(Xtraining)

Ktrain = Xtraining' * Xtraining;

n = size(Xtraining, 2);

Dist = .5*(diag(Ktrain)*ones(1,n) + ones(n,1)*diag(Ktrain)' - 2*Ktrain);
Dist = Dist / max(max(Dist));

nns = cell(1, n);
total_nns = 0;
for i = 1:n
    rp = randperm(n);
    nns{i} = rp(1:floor(.05*n))';
    %remove any (i,i) pairs
    nns{i} = setdiff(nns{i},i);
    total_nns = total_nns + numel(nns{i});
end

js = zeros(total_nns, 1);
is = js;
js_start = 1;
nns2 = cell(1, n);
for i = 1:n
    js(js_start : js_start + numel(nns{i}) - 1) = nns{i};
    is(js_start : js_start + numel(nns{i}) - 1) = i;
    js_start = js_start + numel(nns{i});
    nns2{i} = []; 
end

for i = 1:n
    for j = 1:length(nns{i})
        nns2{nns{i}(j)} = [nns2{nns{i}(j)} i];
    end
end
function [trained_bre] = jf_train_bre(Xtraining, m, bre_gnd_info)
%% bre_para.m:	code length

% Ktrain = Xtraining' * Xtraining;
Ktrain = bre_gnd_info.Ktrain;
%set parameters for bre:
params.disp = 0;
params.n = size(Ktrain,1);
params.numbits = m;
params.K = Ktrain;
params.hash_size = 50;
hash_inds = zeros(params.hash_size,params.numbits);
for b = 1:params.numbits
    rp = randperm(params.n);
    hash_inds(:,b) = rp(1:params.hash_size)';
end
params.hash_inds = hash_inds;
W0 = .001*randn(params.hash_size,params.numbits);

fprintf('Running BRE\n.');
[W,H] = jf_BRE(W0,params, bre_gnd_info);

trained_bre.W = W;
trained_bre.H = H;
trained_bre.hash_inds = hash_inds;
trained_bre.Xtraining = Xtraining;
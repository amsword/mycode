r = 4;
x = (-r: 0.02 : r);
y = normpdf(x);
figure;
plot(x,y); 
grid;
xlabel('x'); 
ylabel('p')
axis off;
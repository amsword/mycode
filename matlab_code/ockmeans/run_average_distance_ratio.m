function run_average_distance_ratio()

line = ['dic_num: ' num2str(dic_num)];
fprintf('%s\n', line);
line = ['s0: ' num2str(s0)];
fprintf('%s\n', line);
line = ['method: ' num2str(method)];
fprintf('%s\n', line);
line = ['method_type: ' num2str(method_type)];
fprintf('%s\n', line);
line = ['working_dir: ' num2str(working_dir)];
fprintf('%s\n', line);

dim = size(Xtraining, 1);
dic_dim = dim / dic_num;
assert(mod(dim, dic_num) == 0);
% dic_size = 256;
max_iter = 100;

file_name = get_file_name(...
    dic_num, dic_size, ...
    s0, method, method_type);

file_name = [working_dir '\' file_name]

x = load(file_name, 'retrieval_result');
retrieval_result = x.retrieval_result;
%
dist_ratio = mexResultDistanceRatio(...
    retrieval_result, ...
    Xtest, ...
    Xbase, ...
    ground_truth_file);


dist_ratio_sym
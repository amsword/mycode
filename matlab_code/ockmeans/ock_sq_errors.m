function y = ock_sq_errors(ock_model, Xbase, ock_codes)

num_point = size(Xbase, 2);
if num_point < 5 * 10^6
    y = ock_sq_errors_batch(ock_model, Xbase, ock_codes);
else
    batch_size = 10^6;
    batch_num = ceil(num_point / batch_size);
    
    for batch_idx = 1 : batch_num
        idx_start = (batch_idx - 1) * batch_size + 1;
        if idx_start > num_point
            continue;
        end
        
        idx_end = idx_start + batch_size - 1;
        if idx_end > num_point
            idx_end = num_point;
        end
        fprintf('%f\t%f\n', idx_start, idx_end);
        
        subXbase = Xbase(:, idx_start : idx_end);
        subock_codes = ock_codes(:, idx_start : idx_end);
        suby = ock_sq_errors_batch(ock_model, subXbase, subock_codes);
        if batch_idx == 1
            y = zeros(num_point, 1);
        end
        y(idx_start : idx_end) = suby;
    end
end
end



function y = ock_sq_errors_batch(ock_model, Xbase, ock_codes)


if ock_model.is_optimize_R
    Xbase = ock_model.R' * Xbase;
end

assert(ock_model.num_sub_dic_each_partition == 1);
num_partitions = numel(ock_model.all_D);
idx_dim_start = 1;
y = 0;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    sub_dim = size(subD, 1);
    idx_dim_end = idx_dim_start + sub_dim - 1;
    
    t = Xbase(idx_dim_start : idx_dim_end, :) - subD(:, double(ock_codes(i, :)) + 1);
    t = t .^ 2;
    y = y + sum(t, 1);
    
    idx_dim_start = idx_dim_end + 1;
end
end
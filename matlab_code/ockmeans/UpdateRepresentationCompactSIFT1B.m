function [mat_compact_B] = UpdateRepresentationCompactSIFT1B(...
    idx_start, idx_end, R, all_D, s0, method)

file_name = get_sift1b_dir_name();
file_name = [file_name '1milliard.p1.siftbin'];

% idx_start = 1001;
% idx_end = 79302017;
Nbase = idx_end - idx_start + 1;
batch_size = 10^7;
batch_num = ceil(Nbase / batch_size);

for i = 1 : batch_num
    batch_start = (i - 1) * batch_size + idx_start;
    batch_end = i * batch_size + idx_start - 1;
    
    batch_end = min(Nbase, batch_end);
    
    [num2str(i) '/' num2str(batch_num) ': ' num2str(batch_start) '/' num2str(batch_end)]
    
   
    Xbase = bvecs_read(file_name, ...
        [batch_start batch_end]);
    
    Xbase = double(Xbase);
    
    sub_B = UpdateRepresentationCompact(Xbase, R, all_D, s0, method);
    
    if i == 1
        mat_compact_B = zeros(size(sub_B, 1), Nbase, class(sub_B));
    end
    mat_compact_B(:, batch_start - idx_start + 1 : ...
        batch_end - idx_start + 1) = sub_B;
end
for code_length  = [16 32 64 128]
    used = 1 : 100;
    
    lo = 'NorthWest';
    if code_length == 32
        if strcmp(type, 'GIST1M3')
            used = int32(10 .^ [2 : 0.1 : 4]);
            lo = 'SouthEast';
        elseif strcmp(type, 'SIFT1M3')
            used = int32(10 .^ [1 : 0.2 : 4]);
            lo = 'SouthEast';
        elseif strcmp(type, 'SIFT1B')
            used = int32(10 .^ [1 : 0.2 : 4]);
        end
    elseif code_length == 64
        if strcmp(type, 'GIST1M3')
            used = int32(10 .^ [0 : 0.2 : 3]);
        elseif strcmp(type, 'SIFT1M3')
            lo = 'SouthEast';
            used = int32(10 .^ [0 : 0.1 : 2]);
        elseif strcmp(type, 'SIFT1B')
            used = int32(10 .^ [0 : 0.2 : 4]);
            lo = 'SouthEast';
        end
    elseif code_length == 128
        if strcmp(type, 'GIST1M3')
            used = int32(10 .^ [1 : 0.2 : 3]);
            lo = 'SouthEast';
        elseif strcmp(type, 'SIFT1M3')
            used = int32(10 .^ [0 : 0.1 : 2]);
            lo = 'SouthEast';
        elseif strcmp(type, 'SIFT1B')
            used = int32(10 .^ [0 : 0.2 : 3]);
            lo = 'SouthEast';
        end
    elseif code_length == 16
        used = int32(10 .^ [0 : 0.2 : 4]);
    end
    
    figure
    for is_sym = [0 1]
        clear pq ck jck ock
        var_name = 'rec';
        if is_sym
            var_name = [var_name '_sym'];
        end
        dic_num = code_length / 8;
        pq = load(['pq_' num2str(dic_num) '_256'], var_name);
        ck = load(['ck_' num2str(dic_num) '_256'], var_name);
        
        dic_num = dic_num / 2;
        jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
        ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
        
        if is_sym
            semilogx(used, ock.rec_sym(used), 'r--', 'LineWidth', 2, ...
                'Marker', marker_ock);
            hold on;
            plot(used, jck.rec_sym(used), 'g--', 'LineWidth', 2, ...
                'marker', marker_eck);
            plot(used, ck.rec_sym(used), 'k--', 'LineWidth', 2, ...
                'marker', marker_ck);
            plot(used, pq.rec_sym(used), 'b--', 'LineWidth', 2, ...
                'marker', marker_pq);
        else
            semilogx(used, ock.rec(used), 'r', 'LineWidth', 2, ...
                'marker', marker_ock);
            hold on;
            plot(used, jck.rec(used), 'g', 'LineWidth', 2, ...
                'marker', marker_eck);
            plot(used, ck.rec(used), 'k', 'LineWidth', 2, ...
                'marker', marker_ck);
            plot(used, pq.rec(used), 'b', 'LineWidth', 2, ...
                'marker', marker_pq);
        end
    end
    
%     if strcmp(type, 'GIST1M3')
%         if code_length == 64
%             if is_sym
%                 ylim([0, 0.55]);
%             end
%         end
%     end
    
    legend('OCKM-A', ...
        'ECKM-A', ...
        'CKM-A', ...
        'PQ-A', ...
        'OCKM-D', ...
        'ECKM-D', ...
        'CKM-D', ...
        'PQ-D', ...
        'Location', lo);
    
    xlabel('Number of Retrieved Points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', gca_font_size);
    grid on;
    set(gcf, 'PaperPositionMode', 'auto');
    saveas(gca, ['figs\' type '_' num2str(code_length) '_rec_both.eps'],'psc2');
%     close;
end

return;
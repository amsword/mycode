function y = Recovered(B, all_D)
D = ConstructD(all_D);
y = D * B;
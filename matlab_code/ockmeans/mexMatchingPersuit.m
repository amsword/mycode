function new_compact_SubB = mexMatchingPersuit(...
    subZ, subD, num_dic, opt_input_encode)

assert(num_dic == 1);
new_compact_SubB = MatchingPersuit3(subZ, subD, num_dic, opt_input_encode);
k = 10;
sym = zeros(4, 4);
asym = sym;
all_dic_num = [2 4 8 16];
for is_sym = [0 1]
var_name = 'rec';
if is_sym
    var_name = [var_name '_sym'];
end
for idx_dic_num = 1 : 4
    dic_num = all_dic_num(idx_dic_num);
    pq = load(['pq_' num2str(dic_num) '_256'], var_name);
    ck = load(['ck_' num2str(dic_num) '_256'], var_name);
    jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
    
    if is_sym
        sym(1, idx_dic_num) = pq.rec_sym(k);
        sym(2, idx_dic_num) = ck.rec_sym(k);
        sym(3, idx_dic_num) = jck.rec_sym(k);
        sym(4, idx_dic_num) = ock.rec_sym(k);
    else
        asym(1, idx_dic_num) = pq.rec(k);
        asym(2, idx_dic_num) = ck.rec(k);
        asym(3, idx_dic_num) = jck.rec(k);
        asym(4, idx_dic_num) = ock.rec(k);
    end
end
end
%
figure;
semilogx(all_dic_num, asym(4, :), 'r-s', 'LineWidth', 2, ...
    'MarkerSize', 10);
hold on;
plot(all_dic_num, asym(3, :), 'G-s', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, asym(2, :), 'k-o', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, asym(1, :), 'b-*', 'LineWidth', 2, ...
    'MarkerSize', 10);

semilogx(all_dic_num, sym(4, :), 'r--s', 'LineWidth', 2, ...
    'MarkerSize', 10);
hold on;
plot(all_dic_num, sym(3, :), 'G--s', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, sym(2, :), 'k--o', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, sym(1, :), 'b--*', 'LineWidth', 2, ...
    'MarkerSize', 10);

lo = 'NorthWest';

  ylim([0, 1]);
if strcmp(type, 'GIST1M3')
      
elseif strcmp(type, 'SIFT1M3')
        lo = 'SouthEast';
end
 legend('OCKM-A', ...
        'ECKM-A', ...
        'CKM-A', ...
        'PQ-A', ...
        'OCKM-S', ...
        'ECKM-S', ...
        'CKM-S', ...
        'PQ-S', ...
        'Location', lo);

xlabel('M', 'FontSize', 14);
ylabel(['Recall'], 'FontSize', 14);
set(gca, 'XTick', all_dic_num);
set(gca, 'FontSize', 16);
xlim([2^0.7, 2^4.3]);
grid on;
set(gcf, 'PaperPositionMode', 'auto');

    saveas(gca, ['figs\' type '_rec_both.eps'],'psc2');


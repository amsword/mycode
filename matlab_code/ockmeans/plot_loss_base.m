clear x;
x = zeros(4, 4);
all_dic_num = [2 4 8 16];
for idx_dic_num = 1 : 4
    dic_num = all_dic_num(idx_dic_num);
    pq = load(['pq_' num2str(dic_num) '_256'], 'loss_base');
    ck = load(['ck_' num2str(dic_num) '_256'], 'loss_base');
    jck = load(['jck_' num2str(dic_num) '_256_2'], 'loss_base');
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], 'loss_base');
    
    x(1, idx_dic_num) = pq.loss_base;
    x(2, idx_dic_num) = ck.loss_base;
    x(3, idx_dic_num) = jck.loss_base;
    x(4, idx_dic_num) = ock.loss_base;
end

% x(:, 3) = x(:, 2) / 2;

figure;
semilogx(all_dic_num, x(4, :), 'r-', 'LineWidth', 2, ...
    'MarkerSize', 10, ...
    'Marker', marker_ock);
hold on;
plot(all_dic_num, x(3, :), 'G-', 'LineWidth', 2, ...
    'MarkerSize', 10, ...
    'Marker', marker_eck);
plot(all_dic_num, x(2, :), 'k-o', 'LineWidth', 2, ...
    'MarkerSize', 10, ...
    'Marker', marker_ck);
plot(all_dic_num, x(1, :), 'b-*', 'LineWidth', 2, ...
    'MarkerSize', 10, ...
    'Marker', marker_pq);

lo = 'NorthEast';
if strcmp(type, 'GIST1M3')
    ylim([2, 12] * 10^5);
    lo = 'SouthWest';
end

legend('OCKM', ...
    'ECKM', ...
    'CKM', ...
    'PQ', ...
    'Location', lo);

xlabel('M', 'FontSize', 14);
ylabel('Distortion', 'FontSize', 14);
set(gca, 'XTick', all_dic_num);
set(gca, 'FontSize', 16);
xlim([2^0.7, 2^4.3]);

grid on;
set(gcf, 'PaperPositionMode', 'auto');
saveas(gca, ['figs\' type '_loss_base.eps'],'psc2');

function [ock_model, compactB, sub_ock_models]  = ock_hierarchy_trianing(Xtraining, opt_input_ock)

assert(opt_input_ock.num_partitions == 1);
num_sub_dic_each_partition = opt_input_ock.num_sub_dic_each_partition;
sub_dic_size_each_partition = opt_input_ock.sub_dic_size_each_partition;
num_sub_problem = log(num_sub_dic_each_partition) / log(2);
num_sub_problem = floor(num_sub_problem);
assert(2 .^ (num_sub_problem) == num_sub_dic_each_partition);

clear opt_input_ock2;
opt_input_ock2.num_sub_dic_each_partition = 1;
opt_input_ock2.num_partitions = num_sub_dic_each_partition;
opt_input_ock2.sub_dic_size_each_partition = sub_dic_size_each_partition;
opt_input_ock2.max_iter = 30;
%         opt_input_ock2.max_iter = 30;
opt_input_ock2.num_grouped = opt_input_ock.num_grouped;

[sub_dim_start_idx] = dim_split(size(Xtraining, 1), opt_input_ock2.num_partitions);
init_all_D = RandomInitDictionary(Xtraining, sub_dim_start_idx, ...
    opt_input_ock2.sub_dic_size_each_partition * ...
    opt_input_ock2.num_sub_dic_each_partition);
opt_input_ock2.init_all_D = init_all_D;


sub_ock_models = cell(num_sub_problem, 1);

for idx_sub_problem = 1 : num_sub_problem
    [ock_model, compactB] = ock_training(Xtraining, opt_input_ock2);
    
    sub_ock_models{idx_sub_problem}.input = opt_input_ock2;
    sub_ock_models{idx_sub_problem}.output.model = ock_model;
    sub_ock_models{idx_sub_problem}.output.compactB = compactB;

    %
    opt_input_ock2.num_sub_dic_each_partition = ...
        opt_input_ock2.num_sub_dic_each_partition * 2;
    opt_input_ock2.num_partitions = opt_input_ock2.num_partitions / 2;
    [opt_input_ock2.init_R, opt_input_ock2.init_all_D] = construct_init_dictionary(...
        ock_model.R, ...
        ock_model.all_D);
    opt_input_ock2.init_compactB = compactB;
end
opt_input_ock.init_all_D = opt_input_ock2.init_all_D;
opt_input_ock.max_iter = 30;
opt_input_ock.init_compactB = compactB;

[ock_model, compactB] = ock_training(Xtraining, opt_input_ock);

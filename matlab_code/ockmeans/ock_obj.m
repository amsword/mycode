function y = ock_obj(Xtraining, ock_codes, ock_model)

num_partitions = numel(ock_model.all_D);

if ock_model.is_optimize_R
    Z = ock_model.R' * Xtraining;
else
    Z = Xtraining;
end

y = RelatedFunctions(0, Z, ock_model.all_D, ock_codes);
return;

y = 0;
idx_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    idx_end = idx_start + size(subD, 1) - 1;
    subZ = Z(idx_start : idx_end, :);
    subZ = subZ - subD(:, double(ock_codes(i, :)) + 1);
    subZ = subZ .^ 2;
    y = y + sum(subZ(:));
    idx_start = idx_end + 1;
end
%% dist ratio
for code_length  = [32 64 128]

for is_sym = [0 1];
% all_dic_num = [4 8 16];
clear pq ck jck ock
used = 1 : 1000;
var_name = 'dist_ratio';
if is_sym
    var_name = [var_name '_sym'];
end

dic_num = code_length / 8;
pq = load(['pq_' num2str(dic_num) '_256'], var_name);
ck = load(['ck_' num2str(dic_num) '_256'], var_name);

dic_num = dic_num / 2;
jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);

lo = 'NorthEast';
if code_length == 32
    if strcmp(type, 'SIFT1M3')
        lo = 'NorthEast';
    end
elseif code_length == 64
elseif code_length == 128

end

if is_sym
    plot(used, ock.dist_ratio_sym(used), 'r', 'LineWidth', 2);
    hold on;
    plot(used, jck.dist_ratio_sym(used), 'g', 'LineWidth', 2);
    plot(used, ck.dist_ratio_sym(used), 'k', 'LineWidth', 2);
    plot(used, pq.dist_ratio_sym(used), 'b', 'LineWidth', 2);
else
    plot(used, ock.dist_ratio(used), 'r', 'LineWidth', 2);
    hold on;
    plot(used, jck.dist_ratio(used), 'g', 'LineWidth', 2);
    plot(used, ck.dist_ratio(used), 'k', 'LineWidth', 2);
    plot(used, pq.dist_ratio(used), 'b', 'LineWidth', 2);
end



xlabel('Number of Retrieved Points', 'FontSize', 14);
ylabel('Mean Overall Ratio', 'FontSize', 14);
if strcmp(type, 'SIFT1M3')
    if code_length == 128
        if is_sym
            ylim([1.01, 1.1]);
        else
            ylim([1.01, 1.05]);
        end
    elseif code_length == 32
        if is_sym
            ylim([1.1, 1.3]);
        else
            ylim([1.05, 1.25]);
        end
    elseif code_length == 64
        if is_sym
            ylim([1.05 1.2]);
        else
            ylim([1.025 1.12]);
        end
    end
elseif strcmp(type, 'GIST1M3')
    if code_length == 128
        if is_sym
            ylim([1.03, 1.08]);
            lo = 'NorthWest';
        else
            ylim([1.02, 1.08]);
            lo = 'NorthWest';
        end
    elseif code_length == 32
        if is_sym
            ylim([1.07, 1.25]);
        else
            ylim([1.05, 1.25]);
        end
    elseif code_length == 64
        if is_sym
            ylim([1.05 1.21]);
        else
            ylim([1.04 1.15]);
        end
    end
end

legend('OCKM', ...
    'ECKM', ...
    'CKM', ...
    'PQ', ...
    'Location', lo);

set(gca, 'FontSize', 16);
grid on;
set(gcf, 'PaperPositionMode', 'auto');
if is_sym
    saveas(gca, ['figs\' type '_' num2str(code_length) '_dist_ratio_sym.eps'],'psc2');
else
    saveas(gca, ['figs\' type '_' num2str(code_length) '_dist_ratio.eps'],'psc2');
end
close;
end
end

function all_D = RandomInitDictionary2(Xtraining, sub_dim_start_idx, dic_size, num_sub_dic_each_partition)

num_dic = numel(sub_dim_start_idx) - 1;

all_D = cell(num_dic, 1);

sub_dic_size_each_partition = dic_size / num_sub_dic_each_partition;
assert(mod(dic_size, num_sub_dic_each_partition) == 0);

for i = 1 : numel(all_D)
    
    idx_dim_start = sub_dim_start_idx(i);
    idx_dim_end = sub_dim_start_idx(i + 1) - 1;
    
    all_D{i} = BestErrorGreedyRandom(...
        Xtraining, idx_dim_start, idx_dim_end, num_sub_dic_each_partition, sub_dic_size_each_partition);
end
end

function subD = BestErrorGreedyRandom(Xtraining, idx_dim_start, idx_dim_end, ...
    num_sub_dic_each_partition, sub_dic_size_each_partition)

num_point = size(Xtraining, 2);

Ds = cell(num_sub_dic_each_partition, 1);
for i = 1 : num_sub_dic_each_partition
    rp = randperm(num_point);
    rp = rp(1 : sub_dic_size_each_partition);
    X = Xtraining(idx_dim_start : idx_dim_end, rp);
    for j = 1 : (i - 1)
        D = Ds{j};
        distance_2_dic = sqdist(X, D);
        [tmp_useless, idx] = min(distance_2_dic, [], 2);
        X = X - D(:, idx);
    end
    Ds{i} = X;
end

subD = zeros(idx_dim_end - idx_dim_start + 1, num_sub_dic_each_partition * sub_dic_size_each_partition);
for i = 1 : num_sub_dic_each_partition
    idx_start = (i - 1) * sub_dic_size_each_partition + 1;
    idx_end = i * sub_dic_size_each_partition;
    subD(:, idx_start : idx_end) = Ds{i};
end

end
function D = ConstructD(all_D)
num_rows = 0;
num_cols = 0;

for i = 1 : numel(all_D)
    num_rows = num_rows + size(all_D{i}, 1);
    num_cols = num_cols + size(all_D{i}, 2);
end

D = zeros(num_rows, num_cols);

idx_start_row = 1;
idx_start_col = 1;
for i = 1 : numel(all_D)
    [rows, cols] = size(all_D{i});
    D(idx_start_row : (idx_start_row + rows - 1), idx_start_col : (idx_start_col + cols - 1)) = all_D{i};
    idx_start_row = idx_start_row + rows;
    idx_start_col = idx_start_col + cols;
end

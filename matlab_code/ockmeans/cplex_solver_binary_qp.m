str_dir = 'H:\codes2\VSProject2\data\';
str_subZ = [str_dir 'subZ.double.bin'];
str_subD = [str_dir 'subD.double.bin'];

s0 = 2;
%%

subZ = read_mat(str_subZ, 'double');
subD = read_mat(str_subD, 'double');

b_dim = size(subD, 2);
Aineq = zeros(s0, b_dim);
for i = 1 : s0
    idx_start = (i - 1) * b_dim / s0 + 1;
    idx_end = idx_start + b_dim / s0 - 1;
    Aineq(i, idx_start : idx_end) = 1;
end
bineq = zeros(s0, 1);
bineq(:) = 1;

[x,resnorm,residual,exitflag,output] = cplexlsqbilp(subD, subZ(:, 1), ...
    Aineq, bineq);
function B = ConstructB(all_B)

num_point = size(all_B{1}, 2);
dic_size = size(all_B{1}, 1);
dic_num = numel(all_B);

B = zeros(dic_size * dic_num, num_point, 'int8');

for i = 1 : dic_num
    idx_start = (i - 1) * dic_size + 1;
    idx_end = i * dic_size;
    B(idx_start : idx_end, :) = full(all_B{i});
end
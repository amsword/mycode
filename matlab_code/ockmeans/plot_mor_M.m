k = 10;
x = zeros(4, 4);
for is_sym = [0 1]
all_dic_num = [2 4 8 16];
var_name = 'dist_ratio';
if is_sym
    var_name = [var_name '_sym'];
end
for idx_dic_num = 1 : 4
    dic_num = all_dic_num(idx_dic_num);
    pq = load(['pq_' num2str(dic_num) '_256'], var_name);
    ck = load(['ck_' num2str(dic_num) '_256'], var_name);
    jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
    
    if is_sym
        x(1, idx_dic_num) = pq.dist_ratio_sym(k);
        x(2, idx_dic_num) = ck.dist_ratio_sym(k);
        x(3, idx_dic_num) = jck.dist_ratio_sym(k);
        x(4, idx_dic_num) = ock.dist_ratio_sym(k);
    else
        x(1, idx_dic_num) = pq.dist_ratio(k);
        x(2, idx_dic_num) = ck.dist_ratio(k);
        x(3, idx_dic_num) = jck.dist_ratio(k);
        x(4, idx_dic_num) = ock.dist_ratio(k);
    end
end

semilogx(all_dic_num, x(4, :), 'r-s', 'LineWidth', 2, ...
    'MarkerSize', 10);
hold on;
plot(all_dic_num, x(3, :), 'G-s', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, x(2, :), 'k-o', 'LineWidth', 2, ...
    'MarkerSize', 10);
plot(all_dic_num, x(1, :), 'b-*', 'LineWidth', 2, ...
    'MarkerSize', 10);

lo = 'NorthEast';
if strcmp(type, 'GIST1M3')
    if is_sym
        ylim([1, 1.3]);
       
    else
        ylim([1, 1.3]);
    end
end

if strcmp(type, 'GIST1M3')
    legend('OCKM', ...
        'ECKM', ...
        'CKM', ...
        'PQ', ...
        'Location', lo);
else
    legend('OCKM', ...
        'ECKM', ...
        'CKM', ...
        'PQ', ...
        'Location', 'NorthEast');
end
xlabel('M', 'FontSize', 14);
ylabel(['Mean Overal Ratio'], 'FontSize', 14);
set(gca, 'XTick', all_dic_num);
set(gca, 'FontSize', 16);
xlim([2^0.7, 2^4.3]);
grid on;
set(gcf, 'PaperPositionMode', 'auto');
if is_sym
    saveas(gca, ['figs\' type '_mor_sym.eps'],'psc2');
else
    saveas(gca, ['figs\' type '_mor.eps'],'psc2');
end
close;
end

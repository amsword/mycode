function file_name = get_sift1b_dir_name()

machine_name = getComputerName();
machine_name(end) = [];
if strcmp(machine_name, 'dewberry')
    file_name = '\\DRAGON\Data\Jianfeng\Data_HashCode\SIFT1B\data\';
elseif strcmp(machine_name, 'dragon')
    file_name = 'E:\Data\Jianfeng\Data_HashCode\SIFT1B\data\';
elseif strcmp(machine_name, 'gs631-2798')
    file_name = 'C:\Users\uqjwan34\Desktop\document\Data_HashCode\SIFT1B\data\';
end
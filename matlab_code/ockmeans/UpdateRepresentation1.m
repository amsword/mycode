function [cell_sparse_B] = UpdateRepresentation(...
    Z, all_D, old_cell_sparse_B, num_sub_dic_each_partition, ...
    opt_input_encode, sub_dim_start_idx)

num_partitions = numel(all_D);

cell_sparse_B = cell(num_partitions, 1);

for i = 1 : num_partitions
    subD = all_D{i};

    idx_dim_start = sub_dim_start_idx(i);
    idx_dim_end = sub_dim_start_idx(i + 1) - 1;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    new_compact_SubB = mexMatchingPersuit(subZ, subD, num_sub_dic_each_partition, opt_input_encode);

    new_full_SubB = Compact2Binary(new_compact_SubB, size(subD, 2), ...
        mod(opt_input_encode, 10));
    
    new_error = sum((subZ - subD * new_full_SubB) .^ 2, 1);
    
    if num_sub_dic_each_partition >= 0 ...
            && ~isempty(old_cell_sparse_B) ...
            && num_sub_dic_each_partition ~= 1
        oldSubB = old_cell_sparse_B{i};
        original_error = sum((subZ - subD * oldSubB) .^ 2, 1);
        no_need_update = new_error > original_error;
        if sum(no_need_update) ~= 0
            warning('here');
        end
        new_full_SubB(:, no_need_update) = oldSubB(:, no_need_update);
    end
    cell_sparse_B{i} = sparse(new_full_SubB);
end
function reconstructed = reconstruct_subD(subD, new_compact_SubB)

reconstructed = 0;
num_sub_dic_each_partition = size(new_compact_SubB, 1);

for j = 1 : num_sub_dic_each_partition
    if j == 1
        reconstructed = subD(:, double(new_compact_SubB(j, :)) + 1);
    else
        reconstructed = reconstructed + subD(:, double(new_compact_SubB(j, :)) + 1);
    end
end
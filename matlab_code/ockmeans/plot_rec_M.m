k = 10;
x = zeros(4, 4);
all_dic_num = [2 4 8 16];
for is_sym = [0 1]
var_name = 'rec';
if is_sym
    var_name = [var_name '_sym'];
end
for idx_dic_num = 1 : 4
    dic_num = all_dic_num(idx_dic_num);
    pq = load(['pq_' num2str(dic_num) '_256'], var_name);
    ck = load(['ck_' num2str(dic_num) '_256'], var_name);
    jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
    
    if is_sym
        x(1, idx_dic_num) = pq.rec_sym(k);
        x(2, idx_dic_num) = ck.rec_sym(k);
        x(3, idx_dic_num) = jck.rec_sym(k);
        x(4, idx_dic_num) = ock.rec_sym(k);
    else
        x(1, idx_dic_num) = pq.rec(k);
        x(2, idx_dic_num) = ck.rec(k);
        x(3, idx_dic_num) = jck.rec(k);
        x(4, idx_dic_num) = ock.rec(k);
    end
end
%

semilogx(all_dic_num, x(4, :), 'r-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_ock);
hold on;
plot(all_dic_num, x(3, :), 'G-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_eck);
plot(all_dic_num, x(2, :), 'k-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_ck);
plot(all_dic_num, x(1, :), 'b-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'marker', marker_pq);

lo = 'NorthWest';

if strcmp(type, 'GIST1M3')
    if is_sym
        ylim([0, 0.75]);
    else
        ylim([0, 0.9]);
    end
else
    if ~is_sym
        lo = 'SouthEast';
    end
end
legend('OCKM', ...
        'ECKM', ...
        'CKM', ...
        'PQ', ...
        'Location', lo);

xlabel('M', 'FontSize', 14);
ylabel(['Recall'], 'FontSize', 14);
set(gca, 'XTick', all_dic_num);
set(gca, 'FontSize', 16);
xlim([2^0.7, 2^4.3]);
grid on;
set(gcf, 'PaperPositionMode', 'auto');
if is_sym
    saveas(gca, ['figs\' type '_rec_sym.eps'],'psc2');
else
    saveas(gca, ['figs\' type '_rec.eps'],'psc2');
end
close
end

function [R, all_D, all_B, obj]  = sckmeans(Xtraining, ...
    dic_dim, dic_size, s0, ...
    method, ...
    max_iter, init_all_D, init_R, init_B, is_opt_R)

dim = size(Xtraining, 1);
num_point = size(Xtraining, 2);
dic_num = dim / dic_dim;

rng(0);
num_split = s0;
% all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size);
if s0 == -1
    num_split = 1;
end
% all_D = RandomInitDictionary2(Xtraining, dic_dim, dic_size, num_split);
if method >= 2
    if isempty(init_all_D)
        all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size);
%         all_D = RandomInitDictionary3(Xtraining, dic_dim, dic_size, num_split);
    else
        if size(init_all_D{1}, 1) == dic_dim && size(init_all_D{1}, 2) == dic_size
            all_D = init_all_D;
        else
            assert(dic_dim / size(init_all_D{1}, 1) == numel(init_all_D) / dic_num);
            all_D = InitFromFinerD(init_all_D, dic_dim);         
        end
    end
else
    all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size);
end

if isempty(init_B)
    all_B = cell(dic_num, 1);
    for i = 1 : dic_num
        all_B{i} = sparse(dic_size, num_point);
    end
else
    all_B = init_B;
end

if isempty(init_R)
	R = eye(dim, dim);
else
	R = init_R;
end
all_B = UpdateRepresentation(Xtraining, R, all_D, all_B, s0, method);

obj = zeros(3 * max_iter, 1);

i = 1;
total_time = 0;
for iter = 1 : max_iter
    tic
    [num2str(iter) '/' num2str(max_iter)]
    
    if is_opt_R
        R = UpdateRotation(Xtraining, all_D, all_B);
    end
    
    obj(i) = DistortionLoss(Xtraining, R, all_D, all_B);
    i = i + 1;
    
    all_D = UpdateDictionary(Xtraining, R, all_B, dic_dim, num_split);
    
    obj(i) = DistortionLoss(Xtraining, R, all_D, all_B);
    
    i = i + 1;

%     oldB = B;
    all_B = UpdateRepresentation(Xtraining, R, all_D, all_B, s0, method);
    obj(i) = DistortionLoss(Xtraining, R, all_D, all_B);
    i = i + 1;
    
    time_iter = toc;
    total_time = total_time + time_iter;
    fprintf('time/iter: %f. Left: %f\n', total_time / iter, total_time / iter * (max_iter - iter));
end

iter


global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

% type = 'sift_1m'; % peekaboom; labelme; sift_1m
% m = 32; % 12, 18, 24, sift32
type
m

%
all_topks = [1 10 100 200 400, 50];

 pre_fix = '';

[save_file save_figure] = train_save_file2(type, m, 0, pre_fix);

load(save_file.test_sh, 'eval_sh');
if is_mlh_val
    load(save_file.test_mlh, 'eval_mlh');
else
    load(save_file.test_mlh2, 'eval_mlh');
end
load(save_file.test_usplh, 'eval_usplh');
load(save_file.test_lsh, 'eval_lsh');
% load(save_file.test_ch, 'eval_ch');
load(save_file.test_isohash_lp);
load(save_file.test_isohash_gf);
load(save_file.test_bre, 'eval_bre');
load(save_file.test_classification, 'eval_classification');
load(save_file.test_itq, 'eval_itq');
%%
% map = zeros(6, 3);
% for idx = 3 : 5
%     map(6, idx - 2) = jf_calc_map(eval_classification{idx}.rec, eval_classification{idx}.pre);
%     map(5, idx - 2) = jf_calc_map(eval_mlh{idx}.rec, eval_mlh{idx}.pre);
%     map(4, idx - 2) = jf_calc_map(eval_usplh{idx}.rec, eval_usplh{idx}.pre);
%     map(3, idx - 2) = jf_calc_map(eval_bre{idx}.rec, eval_bre{idx}.pre);
%     map(2, idx - 2) = jf_calc_map(eval_sh{idx}.rec, eval_sh{idx}.pre);
%     map(1, idx - 2) = jf_calc_map(eval_lsh{idx}.rec, eval_lsh{idx}.pre);
% end
% save(['map_' type '_' num2str(m)], 'map');
%  
%
is_marker = true;

curve_colors =  [[255 0 0] / 255; ...
    [148 30 249] / 255; ...
    [220 50 176] / 255; ...
    [20 4 229] / 255; ...
    [3 151 219] / 255; ...
    [1 189 37] / 255; ...
    [0 0 0] / 255; ...
    [80 136 170] / 255; ...
    [200 137 30] / 255; ...
    [100 70 230] / 255];
markers = {'s', 'v', 'o', '>', '<', 'diamond', 'hexagram', '+', 'x'};

%%
for k = 1 : numel(all_topks)
%     break;
    % for k = 4 : 4
    
    h = figure;
%     plot1 = plot(...
%         pos, eval_classification{k}.r_code(pos), '-p', ...
%         pos, eval_mlh{k}.r_code(pos), '-*', ...
%         pos, eval_usplh{k}.r_code(pos), '-<', ...
%         pos, eval_bre{k}.r_code(pos), '-d', ...
%         pos, eval_sh{k}.r_code(pos), '-o', ...
%         pos, eval_lsh{k}.r_code(pos), '-v', ...
%         'LineWidth', 2);
   
%     pos = 1 : 1001;
%     plot1 = plot(...
%         pos, eval_classification{k}.r_code(pos), '-', ...
%         pos, eval_mlh{k}.r_code(pos), '-', ...
%         pos, eval_usplh{k}.r_code(pos), '-', ...
%         pos, eval_bre{k}.r_code(pos), '-', ...
%         pos, eval_sh{k}.r_code(pos), '-', ...
%         pos, eval_lsh{k}.r_code(pos), '-', ...
%         'LineWidth', 2);
        plot1 = semilogx(...
            eval_classification{k}.avg_retrieved, eval_classification{k}.rec, '-', ...
            eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, '-', ...
            eval_itq{k}.avg_retrieved, eval_itq{k}.rec, '-', ...
            eval_isohash_lp{k}.avg_retrieved, eval_isohash_lp{k}.rec, '-', ...
            eval_isohash_gf{k}.avg_retrieved, eval_isohash_gf{k}.rec, '-', ...
            eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, '-', ...
            'LineWidth', 2);

   legend( 'OPH', 'MLH', ...
        'ITQ', ...
        'IsoHashLp', ...
        'IsoHashGf', ...
        'LSH', ...
        'Location', 'Best');
    
    xlim([10, 10^4]);
    
    if idx_type == 4
        xlim([10^2, 10^5]);
    end

    grid on;
    
    % Create xlabel
    xlabel('Number of retrieved points','FontSize',14);
    
    % Create ylabel
    ylabel(['Recall of ' num2str(all_topks(k)) '-NN'],'FontSize',14);
    set(gca, 'FontSize', 14);
    
    for i = 1 : numel(plot1)
        set(plot1(i), 'Color', curve_colors(i, :));
        set(plot1(i), 'Marker', markers{i});
    end
%     saveas(gca, [num2str(all_topks(k)) '.bmp']);
    if is_mlh_val
        saveas(gca, [figs_path 'v\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
    else
        saveas(gca, [figs_path 'nv\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
    end
    
     saveas(gca, [type '_' num2str(m) '_' num2str(all_topks(k)) '-NN.jpg']);
%     saveas(gca, [save_figure type '_' num2str(m) 'topk' num2str(all_topks(k)) '_ranking_rec.fig']);
%     saveas(gca, [save_figure type '_' num2str(m) 'topk' num2str(all_topks(k)) '_ranking_rec.eps'], 'psc2');
end
% clear eval_sh eval_mlh eval_usplh eval_lsh eval_bre eval_classification eval_itq
    
% pause;
% close all;
return;
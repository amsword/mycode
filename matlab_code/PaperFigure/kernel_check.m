global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

all_topks = [1 10 100 200 400, 50, 5];

 file_pre_date = '';

[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);

cbh = load(save_file.test_classification);
mlh = load(save_file.test_mlh);
koph = load(save_file.test_classification_k);

for k = 1 : 7
    figure;
    semilogx(cbh.eval_classification{k}.avg_retrieved, cbh.eval_classification{k}.rec, 'r-*', 'LineWidth', 2);
    hold on;
%     semilogx(eval_classification_k{k}.avg_retrieved, eval_classification_k{k}.rec, 'k-o', 'LineWidth', 2);
    
    semilogx(koph.eval_classification_k{k}.avg_retrieved, koph.eval_classification_k{k}.rec, 'g-*', 'LineWidth', 2);
    
    semilogx(mlh.eval_mlh{k}.avg_retrieved, mlh.eval_mlh{k}.rec, 'b->', 'LineWidth', 2);
    
    legend('OPH', 'OPH\_kernel', 'MLH', 'Location', 'Best');

    xlim([10, 10^3]);

    if strcmp(type, 'GIST1M3')
        xlim([10^2, 10^4]);
    end
    
    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    
    if strcmp(type, 'labelme') || strcmp(type, 'peekaboom')
        x = load(save_file.train_classification_k);
        if size(x.W, 1) > 512
            tmp1 = x.W(1 : 512, :);
            t1 = mean(tmp1(:) .^ 2);
            
            tmp2 = x.W(513 : end - 1, :);
            t2 = mean(tmp2(:) .^ 2);
            title([num2str(t1) '----' num2str(t2)]);
        end
    elseif strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')
        x = load(save_file.train_classification_k);
        tmp1 = x.W(1 : 128, :);
        t1 = mean(tmp1(:) .^ 2);
        
        tmp2 = x.W(129 : end - 1, :);
        t2 = mean(tmp2(:) .^ 2);
        title([num2str(t1) '----' num2str(t2)]);
    end
    
    
    grid on;
    saveas(gca, [type '_' num2str(m) '_' num2str(all_topks(k)) '.eps'], 'psc2');
end

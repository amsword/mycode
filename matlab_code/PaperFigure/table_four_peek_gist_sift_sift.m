global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;
all_types2 = {'peekaboom', 'GIST1M3', 'sift_1m', 'SIFT1M3'};

% for k = 1 : numel(all_topks)
for k = 1 : 7
    table = [];
    for idx_type = 1 : 4
        type = all_types2{idx_type};
        %     type
        file_pre_date = [];
        all_topks(k)
        all_m = [32, 64, 128, 256];
        %     all_m = [64];
        all_sh = zeros(1, numel(all_m));
        all_mlh= zeros(1, numel(all_m));
        all_usplh= zeros(1, numel(all_m));
        all_lsh= zeros(1, numel(all_m));
        all_bre= zeros(1, numel(all_m));
        all_classification= zeros(1, numel(all_m));
        all_itq= zeros(1, numel(all_m));
        idx = 1;
        clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
        clear all_itq all_isohash all_ksh all_agh;
        for m = all_m
            %     for m = 64
            %         m
            load_eval_data;
            
            all_sh(idx)  = jf_calc_map(eval_sh{k}.rec, eval_sh{k}.pre);
            all_mlh(idx) = jf_calc_map(eval_mlh{k}.rec, eval_mlh{k}.pre);
            all_usplh(idx) = jf_calc_map(eval_usplh{k}.rec, eval_usplh{k}.pre);
            all_lsh(idx) = jf_calc_map(eval_lsh{k}.rec, eval_lsh{k}.pre);
            all_bre(idx) = jf_calc_map(eval_bre{k}.rec, eval_bre{k}.pre);
            all_classification(idx) = jf_calc_map(eval_classification{k}.rec, eval_classification{k}.pre);
            if ~is_no_itq
                all_itq(idx) = jf_calc_map(eval_itq{k}.rec, eval_itq{k}.pre);
                all_isohash(idx) = jf_calc_map(eval_isohash{k}.rec, eval_isohash{k}.pre);
            end
            all_ksh(idx) = jf_calc_map(eval_ksh{k}.rec, eval_ksh{k}.pre);
            all_agh(idx) = jf_calc_map(eval_agh{k}.rec, eval_agh{k}.pre);
            
            idx = idx + 1;
        end
        
        if ~is_no_itq
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq; ...
                all_isohash; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        else
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq, -1; ...
                all_isohash, -1; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        end
        
        continue;
    end
    write_table_p_g_s_s;
end

return;
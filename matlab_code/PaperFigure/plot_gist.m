
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

% type = 'sift_1m'; % peekaboom; labelme; sift_1m
% m = 32; % 12, 18, 24, sift32
type
m

all_topks = [1 10 100 200 400, 50, 5];

file_pre_date = '';

[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);

clear_eval_data;
load(save_file.test_sh, 'eval_sh');
if is_mlh_val
    load(save_file.test_mlh, 'eval_mlh');
else
    load(save_file.test_mlh, 'eval_mlh');
end

load(save_file.test_classification, 'eval_classification');


%%
pad = 0;
for k = 1 : numel(all_topks) - 1
    h = figure;

    rec = inter_p(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 9);
    
%     semilogx(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
    semilogx([9; eval_classification{k}.avg_retrieved], [rec; eval_classification{k}.rec], 'r-o', 'LineWidth', 2);
    
    hold on;
%     rec = inter_p(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, 9);
%     semilogx([9; eval_sh{k}.avg_retrieved], [pad; eval_sh{k}.rec], 'b-o', 'LineWidth', 2);
    
    rec = inter_p(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 9);
    semilogx([9; eval_mlh{k}.avg_retrieved], [pad; eval_mlh{k}.rec], 'k-*', 'LineWidth', 2);
    
    lgend = legend('OPH', 'MLH', 'Location', 'Best');
    grid on;
    
    xlim([10, 1000]);
    
    set(gca, 'FontSize', 18);
    set(lgend, 'FontSize', 14);
    saveas(gca, [num2str(m) '_' num2str(all_topks(k)) '.eps'], 'psc2');
    
end

return;

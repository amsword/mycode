if ~is_sup
    if all_topks(k) == 1
        if strcmp(type, 'labelme')
            if m == 32
            else
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
                ylim([0.1, 0.9]);
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
                ylim([0, 0.75]);
            elseif m == 64
                ylim([0, 0.9]);
            elseif m == 128
                ylim([0, 1]);
            elseif m == 256
                ylim([0, 1]);
            end
        elseif strcmp(type, 'sift_1m')
            if m == 32
                ylim([0.05, 0.9]);
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
                ylim([0, 0.65]);
            elseif m == 64
                ylim([0, 0.9]);
            elseif m == 128
            end
        end    
    elseif all_topks(k) == 5
        if strcmp(type, 'labelme')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            elseif m == 256
            else
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
                ylim([0, 0.6]);
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
            end
        end
    elseif all_topks(k) == 10
        if strcmp(type, 'labelme')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            else
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
                ylim([0, 0.6]);
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 64
            elseif m == 128
                ylim([0, 0.8]);
                location = 'SouthEast';
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            end
        end
    elseif all_topks(k) == 50
        if strcmp(type, 'labelme')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
                ylim([0, 0.6]);
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        end
    elseif all_topks(k) == 100
        if strcmp(type, 'labelme')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
            else
            end
        elseif strcmp(type, 'peekaboom')
            if m == 128
                ylim([0, 0.8]);
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        end
    elseif all_topks(k) == 200
        if strcmp(type, 'labelme')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 128
                 ylim([0, 0.8]);
            end
        end
    elseif all_topks(k) == 400
        if strcmp(type, 'labelme')
            if m == 128
                ylim([0, 0.8]);
            end
        elseif strcmp(type, 'peekaboom')
            if m == 32
            elseif m == 64
            elseif m == 128
                ylim([0, 0.8]);
            end
        elseif strcmp(type, 'GIST1M3')
            if m == 32
            elseif m == 64
                ylim([0, 0.6]);
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        elseif strcmp(type, 'sift_1m')
            if m == 64
            elseif m == 128
                ylim([0, 0.8]);
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
            elseif m == 128
                ylim([0, 0.8]);
            elseif m == 256
            end
        end
    end
end

function yi = inter_p(x, y, xi)
x = [0; x];
y = [0; y];
yi = interp1q(x, y, xi);
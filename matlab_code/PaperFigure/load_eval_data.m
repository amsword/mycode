file_pre_date = '';

[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);


clear_eval_data;
load(save_file.test_sh, 'eval_sh');
if is_mlh_val
    load(save_file.test_mlh, 'eval_mlh');
else
    load(save_file.test_mlh, 'eval_mlh');
end
load(save_file.test_usplh, 'eval_usplh');
load(save_file.test_lsh, 'eval_lsh');
load(save_file.test_bre, 'eval_bre');
load(save_file.test_classification, 'eval_classification');

is_no_itq = (strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')) && ...
    m > 128;
if ~is_no_itq
    load(save_file.test_itq, 'eval_itq');
    x1 = load(save_file.test_isohash_gf);
    v1 = jf_calc_map(x1.eval_isohash_gf{6}.rec, x1.eval_isohash_gf{6}.pre);
    x2 = load(save_file.test_isohash_lp);
    v2 = jf_calc_map(x2.eval_isohash_lp{6}.rec, x2.eval_isohash_lp{6}.pre);
    if v1 > v2
        eval_isohash = x1.eval_isohash_gf;
    else
        eval_isohash = x2.eval_isohash_lp;
    end
end
%%

%%
x1 = load(save_file.test_agh_one);
v1 = jf_calc_map(x1.eval_agh_one{6}.rec, x1.eval_agh_one{6}.pre);
x2 = load(save_file.test_agh_two);
v2 = jf_calc_map(x2.eval_agh_two{6}.rec, x2.eval_agh_two{6}.pre);
if v1 > v2
    eval_agh = x1.eval_agh_one;
else
    eval_agh = x2.eval_agh_two;
end
%%
x = load(save_file.test_ksh);
eval_ksh = x.eval_ksh;

clear all;
all_types = {'labelme', 'peekaboom', 'sift_1m', 'SIFT1M3', 'GIST1M3'};

all_code = [16, 32, 64, 128, 256];
% all_code = [128];
is_mlh_val = 1;
figs_path = '\\cng0060xl2\D$\Research\Hashing\OPHHash\figs\';
all_topks = [1 10 100 200 400, 50, 5];
curve_colors =  [[255 0 0] / 255; ...
    [148 30 249] / 255; ...
    [220 50 176] / 255; ...
    [20 4 229] / 255; ...
    [3 151 219] / 255; ...
    [1 189 37] / 255; ...
    [0 0 0] / 255; ...
    [80 136 170] / 255; ...
    [200 137 30] / 255; ...
    [100 70 230] / 255];
markers = {'s', 'v', 'o', '>', '<', 'diamond',  'x', '+', 'hexagram', '*'};

%%
clc;
is_sup = 0;
for idx_type = 4 : 4
%     for idx_code = 1 : numel(all_code)
            for idx_code = 2 : 2
        type = all_types{idx_type};
        m = all_code(idx_code)
        plot_figure;  
        %         plot_figure_ranking
        %         plot_gist;
        %         check_perf_ksh;
        %         check_ksh_agh;
%                 check_kernel_oph;
        %         kernel_check;
%         pause;                               
%         close all;
    end
end
% pause;
% close all;
return;

%%
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

for idx_type = 1 : 5
    type = all_types{idx_type};
    file_pre_date = [];
    r = 2000;
    k = 1;
    %     h = 5;
    all_m = [16, 32, 64, 128, 256];
    %     all_m = [64];
    
    idx = 1;
    for m = all_m
        m;
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        load_eval_data;
        
        all_sh(idx) = inter_p(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, r);
        all_mlh(idx) = inter_p(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, r);
        all_usplh(idx) = inter_p(eval_usplh{k}.avg_retrieved, eval_usplh{k}.rec, r);
        all_lsh(idx) = inter_p(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, r);
        all_bre(idx) = inter_p(eval_bre{k}.avg_retrieved, eval_bre{k}.rec, r);
        all_classification(idx) = inter_p(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, r);
        all_ksh(idx) = inter_p(eval_ksh{k}.avg_retrieved, eval_ksh{k}.rec, r);
        all_agh(idx) = inter_p(eval_agh{k}.avg_retrieved, eval_agh{k}.rec, r);
        
        if ~is_no_itq
            all_itq(idx) = inter_p(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, r);
            all_isohash(idx) = inter_p(eval_isohash{k}.avg_retrieved, eval_isohash{k}.rec, r);
        end
        
        idx = idx + 1;
    end
    all_classification(4) - all_mlh(4)
    %
    figure;
    plot_code_length;
    
    xlim([2^3.5, 2^8.5]);
    
    if strcmp(type, 'peekaboom') && r == 100
        ylim([0.2, 1]);
    end
    if strcmp(type, 'GIST1M3') && r == 100
        ylim([0, 0.55]);
    end
    
    
    set(gca, 'XTick', all_m);
    grid on;
    %     xlabel('Code length','FontSize',18);
    
    % Create ylabel
    %     ylabel(['Recall'],'FontSize',18);
    set(gca, 'FontSize', 18);
    set(lgend, 'FontSize', 14);
    if is_mlh_val
        saveas(gca, [figs_path 'v\codes\' ...
            num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
    else
        saveas(gca, [figs_path 'nv\codes\' ...
            num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
    end
end
pause;
close all;

%%
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

for idx_type = 5 : 5
    type = all_types{idx_type};
    file_pre_date = [];
    r = 2000;
    k = 1;
    %     h = 5;
    all_m = [16, 32, 64, 128, 256];
    %     all_m = [64];
    
    idx = 1;
    for m = all_m
        m;
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        x = load(save_file.train_itq);
        assert(size(x.W_itq, 2) == m)
    end

end
% pause;
% close all;


%%
x_code_length;
%%
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;
table = [];
% for k = 1 : numel(all_topks)
for k = 3 : 6
    for idx_type = 1 : 4
        type = all_types{idx_type};
        %     type
        file_pre_date = [];
        all_topks(k)
        all_m = [16, 32, 64, 128, 256];
        %     all_m = [64];
        all_sh = zeros(1, numel(all_m));
        all_mlh= zeros(1, numel(all_m));
        all_usplh= zeros(1, numel(all_m));
        all_lsh= zeros(1, numel(all_m));
        all_bre= zeros(1, numel(all_m));
        all_classification= zeros(1, numel(all_m));
        all_itq= zeros(1, numel(all_m));
        idx = 1;
        clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
        clear all_itq all_isohash all_ksh all_agh;
        for m = all_m
            %     for m = 64
            %         m
            load_eval_data;
            
            all_sh(idx)  = jf_calc_map(eval_sh{k}.rec, eval_sh{k}.pre);
            all_mlh(idx) = jf_calc_map(eval_mlh{k}.rec, eval_mlh{k}.pre);
            all_usplh(idx) = jf_calc_map(eval_usplh{k}.rec, eval_usplh{k}.pre);
            all_lsh(idx) = jf_calc_map(eval_lsh{k}.rec, eval_lsh{k}.pre);
            all_bre(idx) = jf_calc_map(eval_bre{k}.rec, eval_bre{k}.pre);
            all_classification(idx) = jf_calc_map(eval_classification{k}.rec, eval_classification{k}.pre);
            if ~is_no_itq
                all_itq(idx) = jf_calc_map(eval_itq{k}.rec, eval_itq{k}.pre);
                all_isohash(idx) = jf_calc_map(eval_isohash{k}.rec, eval_isohash{k}.pre);
            end
            all_ksh(idx) = jf_calc_map(eval_ksh{k}.rec, eval_ksh{k}.pre);
            all_agh(idx) = jf_calc_map(eval_agh{k}.rec, eval_agh{k}.pre);
            
            idx = idx + 1;
        end
        
        if ~is_no_itq
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq; ...
                all_isohash; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        else
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq, -1; ...
                all_isohash, -1; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        end
        
        %
        figure;
        plot_code_length;
        xlim([2^3.5, 2^8.5]);
        
        if strcmp(type, 'labelme') && all_topks(k) == 50
            ylim([0, 0.55]);
        end
        
        set(gca, 'XTick', all_m);
        grid on;
        xlabel('Code length', 'FontSize', 14);
        ylabel(['mAP'],'FontSize',14);
        
        set(gca, 'FontSize', 18);
        set(lgend, 'FontSize', 14);
        
        if is_mlh_val
            saveas(gca, [figs_path 'v\codes\map' ...
                num2str(all_topks(k)) '_' type '.eps'], 'psc2');
        else
            saveas(gca, [figs_path 'nv\codes\map' ...
                num2str(all_topks(k)) '_' type '.eps'], 'psc2');
        end
    end
end
pause;
close all;
return;
%%
%
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;

% for k = 1 : numel(all_topks)
for k = 1 : 7
    table = [];
    for idx_type = 1 : 4
        type = all_types{idx_type};
        %     type
        file_pre_date = [];
        all_topks(k)
        all_m = [32, 64, 128, 256];
        %     all_m = [64];
        all_sh = zeros(1, numel(all_m));
        all_mlh= zeros(1, numel(all_m));
        all_usplh= zeros(1, numel(all_m));
        all_lsh= zeros(1, numel(all_m));
        all_bre= zeros(1, numel(all_m));
        all_classification= zeros(1, numel(all_m));
        all_itq= zeros(1, numel(all_m));
        idx = 1;
        clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
        clear all_itq all_isohash all_ksh all_agh;
        for m = all_m
            %     for m = 64
            %         m
            load_eval_data;
            
            all_sh(idx)  = jf_calc_map(eval_sh{k}.rec, eval_sh{k}.pre);
            all_mlh(idx) = jf_calc_map(eval_mlh{k}.rec, eval_mlh{k}.pre);
            all_usplh(idx) = jf_calc_map(eval_usplh{k}.rec, eval_usplh{k}.pre);
            all_lsh(idx) = jf_calc_map(eval_lsh{k}.rec, eval_lsh{k}.pre);
            all_bre(idx) = jf_calc_map(eval_bre{k}.rec, eval_bre{k}.pre);
            all_classification(idx) = jf_calc_map(eval_classification{k}.rec, eval_classification{k}.pre);
            if ~is_no_itq
                all_itq(idx) = jf_calc_map(eval_itq{k}.rec, eval_itq{k}.pre);
                all_isohash(idx) = jf_calc_map(eval_isohash{k}.rec, eval_isohash{k}.pre);
            end
            all_ksh(idx) = jf_calc_map(eval_ksh{k}.rec, eval_ksh{k}.pre);
            all_agh(idx) = jf_calc_map(eval_agh{k}.rec, eval_agh{k}.pre);
            
            idx = idx + 1;
        end
        
        if ~is_no_itq
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq; ...
                all_isohash; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        else
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq, -1; ...
                all_isohash, -1; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        end
        
        continue;
    end
    write_table;
end

return;

%%
table_five_data_set_less_load;
table_five_data_set_less_load_ranking;
% table_five_data_set;
%%
% table_four_peek_gist_sift_sift;

%%
how_many_points;

%%
clc;
r = 100;
k = 1;

for idx_type = 1 : 4
    type = all_types(idx_type);
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    
    load(save_file.test_sh, 'eval_sh');
    if is_mlh_val
        load(save_file.test_mlh, 'eval_mlh');
    else
        load(save_file.test_mlh2, 'eval_mlh');
    end
    
    load(save_file.test_classification, 'eval_classification');
    
    'mlh'
    mlh = inter_p(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, r)
    'classification'
    oph = inter_p(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, r)
    oph - mlh
    pause;
end




if is_sup
    if all_topks(k) == 1
        if strcmp(type, 'labelme')
            ylim([0.1, 1]);
        elseif strcmp(type, 'peekaboom')
            ylim([0.1, 1]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0.1, 1]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 1]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 1]);
        end
    elseif all_topks(k) == 5
        if strcmp(type, 'labelme')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'peekaboom')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 1]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 1]);
        end
    elseif all_topks(k) == 10
        if strcmp(type, 'labelme')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'peekaboom')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0.1, 0.9]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 1]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 1]);
        end
    elseif all_topks(k) == 50
        if strcmp(type, 'labelme')
            ylim([0.1, 0.8]);
        elseif strcmp(type, 'peekaboom')
            ylim([0.1, 0.8]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0.1, 0.8]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 0.9]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 0.9]);
        end
    elseif all_topks(k) == 100
        if strcmp(type, 'labelme')
            ylim([0, 0.8]);
        elseif strcmp(type, 'peekaboom')
            ylim([0, 0.8]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0, 0.8]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 0.9]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 0.9]);
        end
    elseif all_topks(k) == 200
        if strcmp(type, 'labelme')
            ylim([0, 0.7]);
        elseif strcmp(type, 'peekaboom')
            ylim([0, 0.7]);
        elseif strcmp(type, 'GIST1M3')
            ylim([0, 0.7]);
        elseif strcmp(type, 'sift_1m')
            ylim([0, 0.8]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([0, 0.8]);
        end
    elseif all_topks(k) == 400
        if strcmp(type, 'peekaboom')
            if m == 32
                ylim([0, 0.5]);
            elseif m == 64
                ylim([0, 0.6]);
            elseif m == 128
                ylim([0, 0.7]);
            end
        elseif strcmp(type, 'sift_1m')
            if m == 64
                ylim([0, 0.65]);
            elseif m == 128
                ylim([0, 0.75]);
            end
        elseif strcmp(type, 'SIFT1M3')
            if m == 32
                ylim([0, 0.3]);
            elseif m == 128
                ylim([0, 0.55]);
            elseif m == 256
                ylim([0, 0.65]);
            end
        end
    end
end

if ~is_no_itq
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'ITQ', 'IsoHash', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
else
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
end
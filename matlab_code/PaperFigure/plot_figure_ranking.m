
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

% type = 'sift_1m'; % peekaboom; labelme; sift_1m
% m = 32; % 12, 18, 24, sift32
type
m

all_topks = [1 10 100 200 400, 50, 5];

load_eval_data;

%%
for k = 1 : numel(all_topks)
    
%     for k = 7 : 7
    
    h = figure;
    
%     plot_figure_avg_rec;
    plot_figure_ranking_each;
    grid on;
    
    set(gca, 'FontSize', 18);
    set(lgend, 'FontSize', 14);
%     saveas(gca, [num2str(all_topks(k)) '.bmp']);
    if is_mlh_val
        saveas(gca, [figs_path 'v\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN_1_1k.fig']);
        if is_sup
%             saveas(gca, [figs_path 'v\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN_sup.eps'], 'psc2');
        else
%             saveas(gca, [figs_path 'v\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
        end
    else
%         saveas(gca, [figs_path 'nv\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
    end
    
end
    
% pause;
% close all;
% return;

%%
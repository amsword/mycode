global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;

all_types2 = {'labelme', 'peekaboom', 'GIST1M3', 'sift_1m', 'SIFT1M3'};

idx_ks = 1 : 7;
all_table = cell(numel(idx_ks), 1);

for k = 1 : numel(idx_ks)
    all_table{k} = zeros(10, 1);
end

table = [];
idx = 1;
for idx_type = 1 : 5
    type = all_types2{idx_type};
    %     type
    file_pre_date = [];
    all_m = [32, 64, 128, 256];
    %     all_m = [64];
    
    clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
    clear all_itq all_isohash all_ksh all_agh;
    for m = all_m
        %     for m = 64
         type
         m
        load_eval_data;
        
        for k = idx_ks
            if ~is_no_itq
                all_table{k}(:, idx) = [jf_calc_map(eval_classification{k}.r_code, eval_classification{k}.p_code); ...
                    jf_calc_map(eval_mlh{k}.r_code, eval_mlh{k}.p_code); ...
                    jf_calc_map(eval_bre{k}.r_code, eval_bre{k}.p_code); ...
                    jf_calc_map(eval_itq{k}.r_code, eval_itq{k}.p_code); ...
                    jf_calc_map(eval_isohash{k}.r_code, eval_isohash{k}.p_code); ...
                    jf_calc_map(eval_ksh{k}.r_code, eval_ksh{k}.p_code); ...
                    jf_calc_map(eval_agh{k}.r_code, eval_agh{k}.p_code); ...
                    jf_calc_map(eval_sh{k}.r_code, eval_sh{k}.p_code); ...
                    jf_calc_map(eval_usplh{k}.r_code, eval_usplh{k}.p_code); ...
                    jf_calc_map(eval_lsh{k}.r_code, eval_lsh{k}.p_code)];
            else
                all_table{k}(:, idx) = [jf_calc_map(eval_classification{k}.r_code, eval_classification{k}.p_code); ...
                    jf_calc_map(eval_mlh{k}.r_code, eval_mlh{k}.p_code); ...
                    jf_calc_map(eval_bre{k}.r_code, eval_bre{k}.p_code); ...
                    -1; ...
                    -1; ...
                    jf_calc_map(eval_ksh{k}.r_code, eval_ksh{k}.p_code); ...
                    jf_calc_map(eval_agh{k}.r_code, eval_agh{k}.p_code); ...
                    jf_calc_map(eval_sh{k}.r_code, eval_sh{k}.p_code); ...
                    jf_calc_map(eval_usplh{k}.r_code, eval_usplh{k}.p_code); ...
                    jf_calc_map(eval_lsh{k}.r_code, eval_lsh{k}.p_code)];
            end
        end
        
        idx = idx + 1;
    end
end

for k = 1 : numel(idx_ks)
    table = all_table{k};
    write_table_five_column(table, all_topks(idx_ks(k)), all_m);
end

all_types = {'labelme', 'peekaboom', 'sift_1m'};

for idx_type = 1 : 3
    type = all_types{idx_type};
    [save_file save_figure] = train_save_file2(type, m, 0, pre_fix);
    
    curve_colors =  [[255 0 0] / 255; ...
        [148 30 249] / 255; ...
        [220 50 176] / 255; ...
        [20 4 229] / 255; ...
        [3 151 219] / 255; ...
        [1 189 37] / 255; ...
        [0 0 0] / 255; ...
        [80 136 170] / 255; ...
        [200 137 30] / 255; ...
        [100 70 230] / 255];
    
    
    for which = 1 : 3
        figure
        clear data;
        x12 = load(['map_' type '_12']);
        data(1, :) = x12.map(:, which);
        
        x18 = load(['map_' type '_18']);
        data(2, :) = x18.map(:, which);
        
        x24 = load(['map_' type '_24']);
        data(3, :) = x24.map(:, which);
        
        if (strcmp(type, 'sift_1m'))
            x32 = load(['map_' type '_32']);
            data(4, :) = x32.map(:, which);
            h = bar([12 18 24 32], data);
            switch which
                case 1
                    axis([8 36 0 0.2]);
                case 2
                    axis([8 36 0 0.2]);
                case 3
                    axis([8 36 0 0.25]);
            end
        elseif (strcmp(type, 'labelme'))
            h = bar([12 18 24], data);
            switch which
                case 1
                    axis([8 28 0 0.3]);
                case 2
                    axis([8 28 0 0.4]);
                case 3
                    axis([8 28 0 0.5]);
            end
        else
            h = bar([12 18 24], data);
            switch which
                case 1
                    axis([8 28 0 0.15]);
                case 2
                    axis([8 28 0 0.2]);
                case 3
                    axis([8 28 0 0.25]);
            end
        end
        
        legend('LSH', 'SH', 'BRE', 'USPLH', 'MLH', 'OPH', 'Location', 'Best');
        
        for i = 1 : numel(h)
            set(h(i), 'FaceColor', curve_colors(7 - i, :));
        end
        
        xlabel('Code length', 'FontSize', 14);
        ylabel('Mean Average Precision', 'FontSize', 14);
        set(gca, 'FontSize', 14);
        
%         saveas(gca, [save_figure type '_topk' num2str(all_topks(which + 2)) '.fig']);
%         saveas(gca, [save_figure type '_topk' num2str(all_topks(which + 2)) '.eps'], 'psc2');
    end
end
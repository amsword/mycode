function write_image_result3(type, m, idx_topks, idx_selected, all_methods, is_diff, is_ap)


seq = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'};

file_name = 'tmp.tex';

fp = fopen(file_name, 'w');

line = '\begin{figure}[t]';
fprintf(fp, '%s\n', line);

line = '\centering';
fprintf(fp, '%s\n', line);

line = '\begin{tabular}{@{}';

num_can = 10;
width = 0.074;

fprintf(fp, '%s', line);
fprintf(fp, 'c');
for i = 1 : ((num_can) + 1)
    fprintf(fp, '@{~}c');
end
fprintf(fp, '@{}}\n');


for idx_method = 1 : (numel(all_methods) + 1)
    
    line = ['\scriptsize{(' seq{idx_method} ')}&'];
    fprintf(fp, '%s', line);
    if is_diff
        line = ['\cfbox{blue}{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/diff_' num2str(idx_topks) '_' num2str(idx_selected) '.eps}}'];
    else
        line = ['\cfbox{blue}{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/' num2str(idx_topks) '_' num2str(idx_selected) '.eps}}'];
    end
    fprintf(fp, '%s\n', line);
    
    if idx_method == 1
        for j = 1 : num_can
            if is_diff
                line = ['&{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/diff_'  num2str(idx_topks) '_' num2str(idx_selected) '_linear_scan_' num2str(j) '.eps}}'];
            else
                line = ['&{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/' num2str(idx_topks) '_' num2str(idx_selected) '_linear_scan_' num2str(j) '.eps}}'];
            end
            fprintf(fp, '%s\n', line);
        end
    else
        for j = 1 : num_can
            if is_diff
                line = ['&{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/diff_' num2str(idx_topks) '_' num2str(idx_selected) '_' all_methods{idx_method - 1} '_' num2str(j) '.eps}}'];
            else
                line = ['&{\includegraphics[width = ' num2str(width) '\linewidth, bb= 258 377 351 469 ]{figs/v/' type '/' num2str(m) '/' num2str(idx_topks) '_' num2str(idx_selected) '_' all_methods{idx_method - 1} '_' num2str(j) '.eps}}'];
            end
            fprintf(fp, '%s\n', line);
        end
    end
    line = '\\';
    fprintf(fp, '%s\n', line);
end
line = '\end{tabular}';
fprintf(fp, '%s\n', line);

line = '\caption{Qualitative results on ';
if strcmp(type, 'labelme')
    line = [line 'Labelme'];
else
    line = [line 'Peekaboom'];
end
line = [line '. The first image of each row is a query image. '...
    'The next six images are the results of OPH ranked by the hamming distance. }'];
fprintf(fp, '%s\n', line);

line = '\end{figure}';
fprintf(fp, '%s\n', line);

fclose(fp);
function write_table_five_column(table, topk, all_m)
app = {'OPH', 'MLH', 'BRE', 'ITQ', 'IsoHash', 'KSH', 'AGH', 'SH', 'USPLH', 'LSH'};
all_sets = {'Labelme', 'Peekaboom', 'GIST1M', 'SIFT100K', 'SIFT1M'};
fp = fopen(['\\cng0060xl2\D$\Research\Hashing\OPHHash\map5_' num2str(topk) '.tex'], 'w');

line = '\begin{table*}';
fprintf(fp, '%s\n', line);

line = '\centering';
fprintf(fp, '%s\n', line);

line = '%\addtolength{\tabcolsep}{-2pt}';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '\caption{Mean average precision with ground truth defined as ';
fprintf(fp, '%s%d-NNs}\n', line, topk);

line = '\label{tbl:map:';
fprintf(fp, '%s%d}\n', line, topk);

line = '\scriptsize';
fprintf(fp, '%s\n', line);

line = '%\tiny';
fprintf(fp, '%s\n', line);

line = '% \footnotesize';
fprintf(fp, '%s\n', line);

line = '% \small';
fprintf(fp, '%s\n', line);

line = '%\begin{tabular}{|@{}c@{}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|}';
fprintf(fp, '%s\n', line);

line = '\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}';
fprintf(fp, '%s\n', line);


line = '\hline';
fprintf(fp, '%s\n', line);

line = '&';
fprintf(fp, '%s\n', line);

for i = 1 : numel(app)
    fprintf(fp, '& %s', app{i});
end
fprintf(fp, '\\\\\\hline\n');

[~, idx] = sort(table, 1);

ind = false(size(table, 1), size(table, 2));
for i = 1 : size(table, 2)
    ind(idx(end, i), i) = true;
end

num_ms = numel(all_m);

for i = 1 : 5
    line = ['\multirow{' num2str(num_ms) '}{*}{' all_sets{i} '}'];
    fprintf(fp, '%s', line);
    
    for idx_m = 1 : num_ms
        line = [num2str(all_m(idx_m)) ];
        fprintf(fp, '& %s', line);
        
        for idx_app = 1 : numel(app)
            idx_i_table = idx_app;
            idx_j_table = (i - 1) * num_ms + idx_m;
            if table(idx_i_table, idx_j_table) == -1
                fprintf(fp, '& - ');
            else
                if ind(idx_i_table, idx_j_table)
                    fprintf(fp, '& $\\textbf{%.2f}$', 100 * table(idx_i_table, idx_j_table));
                else
                    fprintf(fp, '& $%.2f$', 100 * table(idx_i_table, idx_j_table));
                end
            end
        end
        if idx_m == num_ms
            fprintf(fp, '\\\\\\hline\n');
        else
            fprintf(fp, '\\\\\\cline{2-12}\n');
        end
    end
    
end

line = '\end{tabular}';
fprintf(fp, '%s\n', line);
line  = '\end{table*}';
fprintf(fp, '%s\n', line);

fclose(fp);
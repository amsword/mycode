x = load(save_file.train_classification);
W = x.W;

Xtraining = read_mat(src_file.c_train, 'double');
Xtest = read_mat(src_file.c_test, 'double');
StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

eval_types.is_hamming_ranking = 1;
eval_types.is_ap = 1;
%%
all_methods = {'OPH', 'MLH', 'BRE', 'ITQ', 'IsoHash', 'KSH', 'AGH', 'SH', 'USPLH', 'LSH'};
idx_method = 1;
%% OPH
x = load(save_file.train_classification);
W = x.W;
[Dhamm, ap] = eval_hash_hamming_dist_ap(size(W, 2), 'linear', W, ...
    Xtest, Xtraining, ...
    StestBase2', all_topks, gnd_file.TestBaseSeed, [], ...
    metric_info, eval_types);

[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%%
x = load(save_file.train_mlh, 'W_mlh');
W_mlh = x.W_mlh;
[Dhamm, ap] = eval_hash_hamming_dist_ap(size(W_mlh, 1), 'linear', ...
    W_mlh', Xtest, Xtraining, StestBase2', ...
    all_topks, [], [], metric_info, eval_types);
[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%% bre
x = load(save_file.train_bre, 'trained_bre');
trained_bre = x.trained_bre;

[Dhamm, ap] = eval_hash_hamming_dist_ap(size(trained_bre.W, 2), ...
    'bre', trained_bre, ...
    Xtest, Xtraining, StestBase2', ...
    all_topks, [], [], metric_info, eval_types);
[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%% itq
x = load(save_file.train_itq, 'W_itq');
W_itq = x.W_itq;
[Dhamm, ap] = eval_hash_hamming_dist_ap(size(W_itq, 2), 'linear', ...
    W_itq, Xtest, Xtraining, StestBase2', ...
    all_topks, [], [], metric_info, eval_types);
[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%% iso
x1 = load(save_file.test_isohash_gf);
v1 = jf_calc_map(x1.eval_isohash_gf{6}.rec, x1.eval_isohash_gf{6}.pre);
x2 = load(save_file.test_isohash_lp);
v2 = jf_calc_map(x2.eval_isohash_lp{6}.rec, x2.eval_isohash_lp{6}.pre);
if v1 > v2
    eval_isohash = x1.eval_isohash_gf;
    x = load(save_file.train_isohash_gf, 'W_isohash_gf');
    W_isohash = x.W_isohash_gf;
else
    eval_isohash = x2.eval_isohash_lp;
    x = load(save_file.train_isohash_lp, 'W_isohash_lp');
    W_isohash = x.W_isohash_lp;
end

[Dhamm, ap]  = eval_hash_hamming_dist_ap(size(W_isohash, 2), 'linear', ...
    W_isohash, Xtest, Xtraining, StestBase2', ...
    all_topks, gnd_file.TestBaseSeed, [], metric_info, eval_types);
[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%% KSH
x = load(save_file.train_ksh, 'para_out');
para_out = x.para_out;

[Dhamm, ap]  = eval_hash_hamming_dist_ap(size(para_out.A1, 2), 'ksh', ...
    para_out, Xtest, Xtraining, StestBase2', ...
    all_topks, [], [], metric_info, eval_types);

[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%% AGH
anchor = read_mat([src_file.cluster_center num2str(300)], 'double');
    
x1 = load(save_file.test_agh_one);
v1 = jf_calc_map(x1.eval_agh_one{6}.rec, x1.eval_agh_one{6}.pre);
x2 = load(save_file.test_agh_two);
v2 = jf_calc_map(x2.eval_agh_two{6}.rec, x2.eval_agh_two{6}.pre);
if v1 > v2
    x = load(save_file.train_agh_one, 'para_out');
    para_out = x.para_out;
        
    para_out.anchor = anchor;
    [Dhamm, ap] = eval_hash_hamming_dist_ap(para_out.m, 'agh', ...
            para_out, Xtest, Xtraining, StestBase2', ...
            all_topks, [], [], metric_info, eval_types);
else
    x = load(save_file.train_agh_two, 'para_out', 'type');
    para_out = x.para_out;
    para_out.anchor = anchor;
    [Dhamm, ap] = eval_hash_hamming_dist_ap(para_out.m, 'agh', ...
        para_out, Xtest, Xtraining, StestBase2', ...
        all_topks, [], [], metric_info, eval_types);
end
[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

% sh
x = load(save_file.train_sh, 'SHparam');
SHparam = x.SHparam;
[Dhamm, ap] = eval_hash_hamming_dist_ap(SHparam.nbits, 'sh', SHparam, ...
    Xtest, Xtraining, StestBase2', ...
    all_topks, gnd_file.TestBaseSeed, ...
    [], metric_info, eval_types);

[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

% usplh

x = load(save_file.train_usplh, 'W_usplh', 'para_usplh');
W_usplh = x.W_usplh;

[Dhamm, ap] = eval_hash_hamming_dist_ap(size(W_usplh, 2), 'linear', ...
    W_usplh, Xtest, Xtraining, StestBase2', ...
    all_topks, gnd_file.TestBaseSeed, [], metric_info, eval_types);

[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

% lsh
x = load(save_file.train_lsh, 'W_lsh', 'para_lsh');
W_lsh = x.W_lsh;
[Dhamm, ap] = eval_hash_hamming_dist_ap(m, 'linear', ...
    W_lsh, Xtest, Xtraining, StestBase2', ...
    all_topks, [], [], metric_info, eval_types);

[s_dhamm, s_idx] = sort(Dhamm, 2);
all_methods{idx_method}
save([all_methods{idx_method} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');
idx_method = idx_method + 1;

%%


%
if strcmp(type, 'labelme');
    x = load('LabelMe_gist');
else
    x = load('Peekaboom_gist');
end
all_images = x.img;
x = load('tmp_train_test_idx');
test_idx_in_all = x.idx_test;
train_idx_in_all = x.idx_train;
%
all_s_idx = cell(numel(all_methods), 1);
all_ap = cell(numel(all_methods), 1);
for i = 1 : numel(all_methods)
    x = load([all_methods{i} '_query_result_' num2str(m)], 's_idx', 'ap');
    all_s_idx{i} = x.s_idx;
    all_ap{i} = x.ap;
end

%
dir_path = '\\cng0060xl2\D$\Research\Hashing\OPHHash\figs\';
load([all_methods{1} '_query_result_' num2str(m)], 's_dhamm', 's_idx', 'ap');


pre_fix = [];

if eval_types.is_ap
    'pre_fix is 1'
    pre_fix = 1;
end

%%
for idx_topk_selected = [2]

for is_diff = [0]

if is_diff
    [s_ap, s_idx_ap] = sort(all_ap{2}{idx_topk_selected} - all_ap{1}{idx_topk_selected});
else
    [s_ap, s_idx_ap] = sort(-ap{idx_topk_selected});
    s_ap = -s_ap;
end
%%%
for idx_selected = [690 : 800]
    idx_selected
    selected_idx_in_test = s_idx_ap(idx_selected);
    %
    selected_idx_in_all = test_idx_in_all(selected_idx_in_test);
    imshow(all_images(:, :, :, selected_idx_in_all));
    if is_diff
        saveas(gca, [dir_path 'v\' type '\' num2str(m) '\diff_' num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '.eps'], 'psc2');
    else
        saveas(gca, [dir_path 'v\' type '\' num2str(m) '\' num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '.eps'], 'psc2');
    end
    close;
    
    selected_can_idx_in_train = StestBase2(1 : 10, selected_idx_in_test);
    selected_can_idx_in_all = train_idx_in_all(selected_can_idx_in_train);
    %
    for idx_can = 1 : 10
        idx_img = selected_can_idx_in_all(idx_can);
        imshow(all_images(:, :, :, idx_img));
        if is_diff
            saveas(gca, [dir_path 'v\' type '\' num2str(m) '\diff_' num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '_' 'linear_scan' '_' num2str(idx_can) '.eps'], 'psc2');
        else
            saveas(gca, [dir_path 'v\' type '\' num2str(m) '\' num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '_' 'linear_scan' '_' num2str(idx_can) '.eps'], 'psc2');
        end
        close;
    end
    
    for idx_method = 1 : numel(all_methods)
        selected_can_idx_in_train = all_s_idx{idx_method}(selected_idx_in_test, 1 : 10);
        selected_can_idx_in_all = train_idx_in_all(selected_can_idx_in_train);
        %
        for idx_can = 1 : 10
            idx_img = selected_can_idx_in_all(idx_can);
            imshow(all_images(:, :, :, idx_img));
            if is_diff
                saveas(gca, [dir_path 'v\' type '\' num2str(m) '\diff_' num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '_' all_methods{idx_method} '_' num2str(idx_can) '.eps'], 'psc2');
            else
                saveas(gca, [dir_path 'v\' type '\' num2str(m) '\'  num2str(pre_fix) '_' num2str(idx_topk_selected) '_' num2str(idx_selected) '_' all_methods{idx_method} '_' num2str(idx_can) '.eps'], 'psc2');
            end
            close;
        end
    end
end
end
end
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

is_plot_rec = 1;

k = 1;
for k = 1 : numel(all_topks)
    for idx_type = 1 : 5
        type = all_types{idx_type};
        file_pre_date = [];
        
        all_m = [16, 32, 64, 128, 256];
        %     all_m = [64];
        idx = 1;
        for m = all_m
            m;
            r = round(m / 5);
            [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
            
            load_eval_data;
            
            if is_plot_rec
                all_sh(idx) = eval_sh{k}.rec(r)
                all_mlh(idx) = eval_mlh{k}.rec(r);
                all_usplh(idx) = eval_usplh{k}.rec(r);
                all_lsh(idx) = eval_lsh{k}.rec(r);
                all_bre(idx) = eval_bre{k}.rec(r);
                all_classification(idx) = eval_classification{k}.rec(r);
                all_ksh(idx) = eval_ksh{k}.rec(r);
                all_agh(idx) = eval_agh{k}.rec(r);
                
                if ~is_no_itq
                    all_itq(idx) = eval_itq{k}.rec(r);
                    all_isohash(idx) = eval_isohash{k}.rec(r);
                end
            else
                all_sh(idx) = eval_sh{k}.pre(r)
                all_mlh(idx) = eval_mlh{k}.pre(r);
                all_usplh(idx) = eval_usplh{k}.pre(r);
                all_lsh(idx) = eval_lsh{k}.pre(r);
                all_bre(idx) = eval_bre{k}.pre(r);
                all_classification(idx) = eval_classification{k}.pre(r);
                all_ksh(idx) = eval_ksh{k}.pre(r);
                all_agh(idx) = eval_agh{k}.pre(r);
                
                if ~is_no_itq
                    all_itq(idx) = eval_itq{k}.pre(r);
                    all_isohash(idx) = eval_isohash{k}.pre(r);
                end
            end
            idx = idx + 1;
        end
        %
        figure;
        plot_code_length;
        
        clear all_sh all_isohash all_itq all_agh all_ksh all_classification
        clear all_bre all_lsh all_usplh all_mlh
        
        xlim([2^3.5, 2^8.5]);
        
        if strcmp(type, 'peekaboom') && r == 100
            ylim([0.2, 1]);
        end
        if strcmp(type, 'GIST1M3') && r == 100
            ylim([0, 0.55]);
        end
        
        
        set(gca, 'XTick', all_m);
        grid on;
        
        set(gca, 'FontSize', 18);
        set(lgend, 'FontSize', 14);
        if is_plot_rec
            if is_mlh_val
                saveas(gca, [figs_path 'v\codes\rec_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            else
                saveas(gca, [figs_path 'nv\codes\rec_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            end
        else
            if is_mlh_val
                saveas(gca, [figs_path 'v\codes\pre_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            else
                saveas(gca, [figs_path 'nv\codes\pre_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            end
        end
    end
end
pause;
close all;

%%
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

is_plot_rec = 1;

clear all_itq 
k = 1;
% for k = 1 : numel(all_topks)
for k = 1 : 1
    for idx_type = 5 : 5
        type = all_types{idx_type};
        file_pre_date = [];
        
%         if idx_type >= 3
%             continue;
%         end
        
        all_m = [16, 32, 64, 128, 256];
        %     all_m = [64];
        idx = 1;
        for m = all_m
            m;
            r = 1000;
            [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
            
            load_eval_data;
            
            if is_plot_rec
                all_sh(idx) = eval_sh{k}.r_code(r)
                all_mlh(idx) = eval_mlh{k}.r_code(r);
                all_usplh(idx) = eval_usplh{k}.r_code(r);
                all_lsh(idx) = eval_lsh{k}.r_code(r);
                all_bre(idx) = eval_bre{k}.r_code(r);
                all_classification(idx) = eval_classification{k}.r_code(r);
                all_ksh(idx) = eval_ksh{k}.r_code(r);
                all_agh(idx) = eval_agh{k}.r_code(r);
                
                if ~is_no_itq
                    all_itq(idx) = eval_itq{k}.r_code(r);
                    all_isohash(idx) = eval_isohash{k}.r_code(r);
                end
            else
                all_sh(idx) = eval_sh{k}.p_code(r)
                all_mlh(idx) = eval_mlh{k}.p_code(r);
                all_usplh(idx) = eval_usplh{k}.p_code(r);
                all_lsh(idx) = eval_lsh{k}.p_code(r);
                all_bre(idx) = eval_bre{k}.p_code(r);
                all_classification(idx) = eval_classification{k}.p_code(r);
                all_ksh(idx) = eval_ksh{k}.p_code(r);
                all_agh(idx) = eval_agh{k}.p_code(r);
                
                if ~is_no_itq
                    all_itq(idx) = eval_itq{k}.p_code(r);
                    all_isohash(idx) = eval_isohash{k}.p_code(r);
                end
            end
            idx = idx + 1;
        end
        %
        figure;
        plot_code_length;
        
        clear all_sh all_isohash all_itq all_agh all_ksh all_classification
        clear all_bre all_lsh all_usplh all_mlh
        
        xlim([2^3.5, 2^8.5]);

        if strcmp(type, 'labelme') && r == 50
            ylim([0.15, 1]);
        end
        
        if strcmp(type, 'peekaboom') && r == 100
            ylim([0.15, 1]);
        end
        if strcmp(type, 'GIST1M3') && r == 100
            ylim([0, 0.6]);
        end
        
        set(gca, 'XTick', all_m);
        grid on;
        
        set(gca, 'FontSize', 18);
        set(lgend, 'FontSize', 14);
        if is_plot_rec
            if is_mlh_val
                saveas(gca, [figs_path 'v\codes\r_code_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            else
                saveas(gca, [figs_path 'nv\codes\r_code_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            end
        else
            if is_mlh_val
                saveas(gca, [figs_path 'v\codes\p_code_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            else
                saveas(gca, [figs_path 'nv\codes\p_code_' ...
                    num2str(all_topks(k)) '_' num2str(r) '_' type '.eps'], 'psc2');
            end
        end
    end
end
pause;
close all;


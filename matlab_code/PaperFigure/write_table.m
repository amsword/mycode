app = {'OPH', 'MLH', 'BRE', 'ITQ', 'ISH', 'KSH', 'AGH', 'SH', 'USPLH', 'LSH'};

fp = fopen(['\\cng0060xl2\D$\Research\Hashing\OPHHash\map_' num2str(all_topks(k)) '.tex'], 'w');

line = '\begin{table*}[H]';
fprintf(fp, '%s\n', line);

line = '\centering';
fprintf(fp, '%s\n', line);

line = '%\addtolength{\tabcolsep}{-2pt}';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '\caption{Mean average precision with ground truth defined as ';
fprintf(fp, '%s%d-NNs}\n', line, all_topks(k));

line = '\label{tbl:map:';
fprintf(fp, '%s%d}\n', line, all_topks(k));

line = '%\scriptsize';
fprintf(fp, '%s\n', line);

line = '\tiny';
fprintf(fp, '%s\n', line);

line = '% \footnotesize';
fprintf(fp, '%s\n', line);

line = '% \small';
fprintf(fp, '%s\n', line);

line = '%\begin{tabular}{|@{}c@{}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|}';
fprintf(fp, '%s\n', line);

line = '\begin{tabular}{|@{}c@{}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|c@{~}|}';
fprintf(fp, '%s\n', line);

line = '\hline'; 
fprintf(fp, '%s\n', line);

line = '&';
fprintf(fp, '%s\n', line);

line = '\multicolumn{4}{c|}{Labelme} &';
fprintf(fp, '%s\n', line);

line = '\multicolumn{4}{c|}{Peekaboom} &';
fprintf(fp, '%s\n', line);


line = '\multicolumn{4}{c|}{SIFT100K} &';
fprintf(fp, '%s\n', line);

line = '\multicolumn{4}{c|}{SIFT1M} \\\hline';
fprintf(fp, '%s\n', line);

line = '& 32 & 64 & 128 & 256 & 32 & 64 & 128 & 256 & 32 & 64 & 128 & 256 & 32 & 64 & 128 & 256 \\\hline';
fprintf(fp, '%s\n', line);

[~, idx] = sort(table, 1);

ind = false(size(table, 1), size(table, 2));
for i = 1 : size(table, 2)
    ind(idx(end, i), i) = true;
end

for i = 1 : numel(app)
    fprintf(fp, '%s', app{i});
    
    for ki = 1 : size(table, 2)
        if table(i, ki) == -1
            fprintf(fp, '& - ');
        else
            if ind(i, ki)
                fprintf(fp, '& \\textbf{%.3f}', table(i, ki));
            else
                fprintf(fp, '& %.3f', table(i, ki));
            end
        end
    end
    fprintf(fp, '\\\\\\hline\n');
end

line = '\end{tabular}';
fprintf(fp, '%s\n', line);
line  = '\end{table*}';
fprintf(fp, '%s\n', line);

fclose(fp);
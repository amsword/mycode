function write_table_five(table, topk, all_m)
app = {'OPH', 'MLH', 'BRE', 'ITQ', 'ISH', 'KSH', 'AGH', 'SH', 'USPLH', 'LSH'};

fp = fopen(['\\cng0060xl2\D$\Research\Hashing\OPHHash\map5_' num2str(topk) '.tex'], 'w');

line = '\begin{table*}';
fprintf(fp, '%s\n', line);

line = '\centering';
fprintf(fp, '%s\n', line);

line = '%\addtolength{\tabcolsep}{-2pt}';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '% \scriptsize';
fprintf(fp, '%s\n', line);

line = '\caption{Mean average precision with ground truth defined as ';
fprintf(fp, '%s%d-NNs}\n', line, topk);

line = '\label{tbl:map:';
fprintf(fp, '%s%d}\n', line, topk);

line = '\scriptsize';
fprintf(fp, '%s\n', line);

line = '%\tiny';
fprintf(fp, '%s\n', line);

line = '% \footnotesize';
fprintf(fp, '%s\n', line);

line = '% \small';
fprintf(fp, '%s\n', line);

line = '%\begin{tabular}{|@{}c@{}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|@{~}c@{~}|}';
fprintf(fp, '%s\n', line);

line = '\begin{tabular}{|@{}c@{}|';
fprintf(fp, '%s', line);

for i = 1 : size(table, 2)
    fprintf(fp, 'c@{}|');
end
fprintf(fp, '}\n');


line = '\hline'; 
fprintf(fp, '%s\n', line);

line = '\multirow{2}{*}{$\times 10^{-2}$}&';
fprintf(fp, '%s\n', line);

line = ['\multicolumn{' num2str(numel(all_m)) '}{c|}{Labelme} &'];
fprintf(fp, '%s\n', line);

line = ['\multicolumn{' num2str(numel(all_m)) '}{c|}{Peekaboom} &'];
fprintf(fp, '%s\n', line);

line = ['\multicolumn{' num2str(numel(all_m)) '}{c|}{GIST1M} &'];
fprintf(fp, '%s\n', line);

line = ['\multicolumn{' num2str(numel(all_m)) '}{c|}{SIFT100K} &'];
fprintf(fp, '%s\n', line);

line = ['\multicolumn{' num2str(numel(all_m)) '}{c|}{SIFT1M}  \\\cline{2-' num2str(size(table, 2) + 1) '}'];
fprintf(fp, '%s\n', line);

str = [];
for i = 1 : numel(all_m)
    str = [str ' & ' num2str(all_m(i))];
end

for i = 1 : (size(table, 2) / numel(all_m))
    fprintf(fp, str); 
end
line = '\\\hline';
fprintf(fp, '%s\n', line);

[~, idx] = sort(table, 1);

ind = false(size(table, 1), size(table, 2));
for i = 1 : size(table, 2)
    ind(idx(end, i), i) = true;
end

for i = 1 : numel(app)
    fprintf(fp, '%s', app{i});
    
    for ki = 1 : size(table, 2)
        if table(i, ki) == -1
            fprintf(fp, '& - ');
        else
            if ind(i, ki)
                fprintf(fp, '& \\textbf{%.2f}', 100 * table(i, ki));
            else
                fprintf(fp, '& %.2f', 100 * table(i, ki));
            end
        end
    end
    fprintf(fp, '\\\\\\hline\n');
end

line = '\end{tabular}';
fprintf(fp, '%s\n', line);
line  = '\end{table*}';
fprintf(fp, '%s\n', line);

fclose(fp);
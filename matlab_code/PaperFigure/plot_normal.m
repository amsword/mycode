function plot_normal(w, type)
% w(1)x + w(2)y + w(3) = 0;

left = -1;
right = 1;
if abs(w(1)) > abs(w(2))
    x = left : 0.1 : right;
    y = w(2) * x;
    y = y / w(1);
    plot(x, y, type);
else
    y = left : 0.1 : right;
    x = w(1) * y;
    x = x / w(2);
    plot(x, y, type);
end
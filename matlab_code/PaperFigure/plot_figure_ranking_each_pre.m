
t = 1;
num_used = 1000;

semilogx(1 : num_used, eval_classification{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
hold on;
semilogx(1 : num_used, eval_mlh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_bre{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));

t = t + 1;

if ~is_no_itq
    semilogx(1 : num_used, eval_itq{k}.p_code(1 : num_used), ...
        'Color', curve_colors(t, :));
    t = t + 1;
    semilogx(1 : num_used, eval_isohash{k}.p_code(1 : num_used), ...
        'Color', curve_colors(t, :));
    t = t + 1;
else
    t = t + 1;
    t = t + 1;
end


semilogx(1 : num_used, eval_ksh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_agh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;

semilogx(1 : num_used, eval_sh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_usplh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_lsh{k}.p_code(1 : num_used), ...
    'Color', curve_colors(t, :));

%% xlim
xlim([1, 10^3]);
if k == 1
    xlim([1, 10^3]);
end

if strcmp(type, 'GIST1M3')
    xlim([10^2, 10^4]);
end
xlim([1, 10^3]);

%% location
location = 'Best';

if ~is_no_itq
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'ITQ', 'ISH', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
else
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
end
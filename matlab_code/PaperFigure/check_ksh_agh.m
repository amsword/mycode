global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

all_topks = [1 10 100 200 400, 50];

 file_pre_date = '';

[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
ksh = load(save_file.test_ksh);
cbh = load(save_file.test_classification);
mlh = load(save_file.test_mlh);

agh_one = load(save_file.test_agh_one);
agh_two = load(save_file.test_agh_two);
lsh = load(save_file.test_lsh);


for k = 1 : 6
    figure;
    semilogx(cbh.eval_classification{k}.avg_retrieved, cbh.eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
    hold on;
    semilogx(mlh.eval_mlh{k}.avg_retrieved, mlh.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2);
    semilogx(ksh.eval_ksh{k}.avg_retrieved, ksh.eval_ksh{k}.rec, 'k-d', 'LineWidth', 2);

    semilogx(agh_one.eval_agh_one{k}.avg_retrieved, agh_one.eval_agh_one{k}.rec, 'g-<', 'LineWidth', 2);
    semilogx(agh_two.eval_agh_two{k}.avg_retrieved, agh_two.eval_agh_two{k}.rec, 'g->', 'LineWidth', 2);
    
    semilogx(lsh.eval_lsh{k}.avg_retrieved, lsh.eval_lsh{k}.rec, 'c-*', 'LineWidth', 2);
    
    
    legend('OPH', 'MLH', 'KSH', 'AGH1', 'AGH2', 'LSH', 'Location', 'Best');
    
    xlim([10, 10^3]);
    
    if idx_type == 4
        xlim([10^2, 10^5]);
    end
    
    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    grid on;
    saveas(gca, [type '_' num2str(m) '_' num2str(all_topks(k)) '.eps'], 'psc2');
end


t = 1;
num_used = 1000;

semilogx(1 : num_used, eval_classification{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
hold on;
semilogx(1 : num_used, eval_mlh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_bre{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));

t = t + 1;

if ~is_no_itq
    semilogx(1 : num_used, eval_itq{k}.r_code(1 : num_used), ...
        'Color', curve_colors(t, :));
    t = t + 1;
    semilogx(1 : num_used, eval_isohash{k}.r_code(1 : num_used), ...
        'Color', curve_colors(t, :));
    t = t + 1;
else
    t = t + 1;
    t = t + 1;
end


semilogx(1 : num_used, eval_ksh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_agh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;

semilogx(1 : num_used, eval_sh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_usplh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));
t = t + 1;
semilogx(1 : num_used, eval_lsh{k}.r_code(1 : num_used), ...
    'Color', curve_colors(t, :));

%% xlim
xlim([1, 10^3]);
if k == 1
    xlim([1, 10^3]);
end

if strcmp(type, 'GIST1M3')
    xlim([10^2, 10^4]);
end
xlim([1, 10^3]);

%% location
location = 'Best';
if strcmp(type, 'labelme') || strcmp(type, 'peekaboom') || ...
        strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')
    if k == 1
        location = 'SouthEast';
        if strcmp(type, 'SIFT1M3')
            location = 'NorthWest';
        end
    else
        location = 'NorthWest';
    end
elseif strcmp(type, 'GIST1M3')
    location = 'NorthWest';
end


%% ylim
if (all_topks(k) == 5 || all_topks(k) == 10 || all_topks(k) == 50 || all_topks(k) == 200 || all_topks(k) == 400)
    if ~is_sup
        if m == 64
            if strcmp(type, 'labelme')
                ylim([0.2, 0.9]);
                if all_topks(k) == 5
                    location = 'SouthEast';
                end
            elseif strcmp(type, 'peekaboom')
                ylim([0.05, 0.7]);
            elseif strcmp(type, 'sift_1m')
                ylim([0.03, 0.8]);
            elseif strcmp(type, 'SIFT1M3')
                ylim([0, 0.7]);
            end
        end
    end
elseif all_topks(k) == 1
    if strcmp(type, 'labelme')
        ylim([0.2, 1]);
    elseif strcmp(type, 'peekaboom')
        ylim([0.1, 1]);
    elseif strcmp(type, 'sift_1m')
        ylim([0.05, 1]);
    elseif strcmp(type, 'SIFT1M3')
        ylim([0, 1]);
    end
    if m == 32
        location = 'NorthWest';
    end
end

%%
ylim_is_paper;

%%
ylim_is_supp;
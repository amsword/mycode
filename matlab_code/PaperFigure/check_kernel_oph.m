
global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

% type = 'sift_1m'; % peekaboom; labelme; sift_1m
% m = 32; % 12, 18, 24, sift32
type
m

all_topks = [1 10 100 200 400, 50, 5];

file_pre_date = '';

[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);


clear_eval_data;
% load(save_file.test_mlh, 'eval_mlh');
% load(save_file.test_classification, 'eval_classification');
load(save_file.test_classification_k, 'eval_classification_k');

x = load(save_file.train_classification_k);
size(x.W)
return;

%%
for k = 1 : numel(all_topks)
    h = figure;
    semilogx(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 'b-*');
    hold on;
    semilogx(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 'r-o');
    semilogx(eval_classification_k{k}.avg_retrieved, eval_classification_k{k}.rec, 'k-<');
    
    grid on;
    
    set(gca, 'FontSize', 18);
    
    if m == 16
        xlim([10^2, 10^4]);
    else
        xlim([10, 10^3]);
    end
    
    if strcmp(type, 'GIST1M3')
        xlim([100, 10^4]);
    end
    legend('MLH', 'OPH', 'KernelOPH', 'Location', 'Best');
    saveas(gca, [figs_path 'v\' type '\' num2str(m) '\' num2str(all_topks(k)) '-NN_kerneloph.eps'], 'psc2');
end

% pause;
% close all;
% return;
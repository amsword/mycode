
t = 1;

semilogx(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
hold on;
semilogx(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_bre{k}.avg_retrieved, eval_bre{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);

t = t + 1;

if ~is_no_itq
    semilogx(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, ...
        'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
    t = t + 1;
    semilogx(eval_isohash{k}.avg_retrieved, eval_isohash{k}.rec, ...
        'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
    t = t + 1;
else
    t = t + 1;
    t = t + 1;
end


semilogx(eval_ksh{k}.avg_retrieved, eval_ksh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_agh{k}.avg_retrieved, eval_agh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;

semilogx(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_usplh{k}.avg_retrieved, eval_usplh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);

%% xlim
xlim([10^1, 10^3]);
if k == 1
    xlim([10^1, 10^3]);
end

if strcmp(type, 'GIST1M3')
    xlim([10^2, 10^4]);
end

%% location
location = 'Best';
if strcmp(type, 'labelme') || strcmp(type, 'peekaboom') || ...
        strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')
    if k == 1
        location = 'SouthEast';
        if strcmp(type, 'SIFT1M3')
            location = 'NorthWest';
        end
    else
        location = 'NorthWest';
    end
elseif strcmp(type, 'GIST1M3')
    location = 'NorthWest';
end


%% ylim
if (all_topks(k) == 5 || all_topks(k) == 10 || all_topks(k) == 50 || all_topks(k) == 200 || all_topks(k) == 400)
    if ~is_sup
        if m == 64
            if strcmp(type, 'labelme')
                ylim([0.2, 0.9]);
                if all_topks(k) == 5
                    location = 'SouthEast';
                end
            elseif strcmp(type, 'peekaboom')
                ylim([0.05, 0.7]);
            elseif strcmp(type, 'sift_1m')
                ylim([0.03, 0.8]);
            elseif strcmp(type, 'SIFT1M3')
                ylim([0, 0.7]);
            end
        end
    end
elseif all_topks(k) == 1
    if strcmp(type, 'labelme')
        ylim([0.2, 1]);
    elseif strcmp(type, 'peekaboom')
        ylim([0.1, 1]);
    elseif strcmp(type, 'sift_1m')
        ylim([0.05, 1]);
    elseif strcmp(type, 'SIFT1M3')
        ylim([0, 1]);
    end
    if m == 32
        location = 'NorthWest';
    end
end

%%
ylim_is_paper;

%%
ylim_is_supp;
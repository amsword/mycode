global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;

all_types2 = {'labelme', 'peekaboom', 'GIST1M3', 'sift_1m', 'SIFT1M3'};

% for k = 1 : numel(all_topks)
for k = 1 : 7
    table = [];
    for idx_type = 1 : 5
        type = all_types2{idx_type};
        %     type
        file_pre_date = [];
        all_topks(k)
        all_m = [16, 32, 64];
        %     all_m = [64];
        all_sh = zeros(1, numel(all_m));
        all_mlh= zeros(1, numel(all_m));
        all_usplh= zeros(1, numel(all_m));
        all_lsh= zeros(1, numel(all_m));
        all_bre= zeros(1, numel(all_m));
        all_classification= zeros(1, numel(all_m));
        all_itq= zeros(1, numel(all_m));
        all_isohash = zeros(1, numel(all_m));
        idx = 1;
        clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
        clear all_itq all_isohash all_ksh all_agh;
        for m = all_m
            %     for m = 64
            %         m
            load_eval_data;
            
            all_sh(idx)  = eval_sh{k}.ap;
            all_mlh(idx) = eval_mlh{k}.ap;
            all_usplh(idx) = eval_usplh{k}.ap;
            all_lsh(idx) = eval_lsh{k}.ap;
            all_bre(idx) = eval_bre{k}.ap;
            all_classification(idx) = eval_classification{k}.ap;
            if ~is_no_itq
                all_itq(idx) = eval_itq{k}.ap;
                all_isohash(idx) = eval_isohash{k}.ap;
            end
            all_ksh(idx) = eval_ksh{k}.ap;
            all_agh(idx) = eval_agh{k}.ap;
            
            idx = idx + 1;
        end
        
        if ~is_no_itq
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq; ...
                all_isohash; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        else
            x = [all_classification; ...
                all_mlh; ...
                all_bre; ...
                all_itq, -1; ...
                all_isohash, -1; ...
                all_ksh; ...
                all_agh; ...
                all_sh; ...
                all_usplh; ...
                all_lsh];
            table = [table, x];
        end
        
        continue;
    end
    write_table_five;
end
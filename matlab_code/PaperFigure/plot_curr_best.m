% load(save_file.test_sh, 'eval_sh');
load(save_file.test_mlh, 'eval_mlh');
% load(save_file.test_usplh, 'eval_usplh');
load(save_file.test_lsh, 'eval_lsh');
load(save_file.test_itq, 'eval_itq');
% load(save_file.test_mbq, 'eval_mbq');
% load(save_file.test_ch, 'eval_ch');
% load(save_file.test_bre, 'eval_bre');
%     load(save_file.test_cbh, 'eval_cbh');
% load(save_file.test_classification, 'eval_classification');

%%
load('\\msra-msm-02\c$\users\v-jianfw\desktop\v-jianfw\hashcode\cBRE\16\labelme16');
for k = 1 : 6
    figure
    best_v = 0;
    best_idx = 0;
    for i = 1 : numel(all_result)
        v = sum(all_result{i}.eval{k}.r_code(1 : 1000));
        if v > best_v
            best_v = v;
            best_idx = i;
        end
    end
    best_idx
    plot(all_result{best_idx}.eval{k}.r_code, 'r');
    hold on;
    plot(all_result{1}.eval{k}.r_code, 'c');
    plot(eval_mbq{k}.r_code, 'g');
    plot(eval_mlh{k}.r_code, 'b');
    plot(eval_itq{k}.r_code, 'k');
    plot(eval_lsh{k}.r_code, 'y');
    xlim([0 2000]);
end
pause;
close all;

%%
for k = 1 : 6
    figure

    plot(eval_cth{k}.r_code, 'r');
    hold on;
    plot(eval_mbq{k}.r_code, 'g');
    plot(eval_mlh{k}.r_code, 'b');
    plot(eval_itq{k}.r_code, 'k');
    plot(eval_lsh{k}.r_code, 'y');
    xlim([0 2000]);
end
pause;
close all;

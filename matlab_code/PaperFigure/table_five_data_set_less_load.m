global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();
clear r;

all_types2 = {'labelme', 'peekaboom', 'GIST1M3', 'sift_1m', 'SIFT1M3'};

idx_ks = 1 : 7;
all_table = cell(numel(idx_ks), 1);

for k = 1 : numel(idx_ks)
    all_table{k} = zeros(10, 1);
end

table = [];
idx = 1;
for idx_type = 1 : 5
    type = all_types2{idx_type};
    %     type
    file_pre_date = [];
    all_m = [32, 64, 128, 256];
    %     all_m = [64];
    
    clear all_sh all_mlh all_usplh all_lsh all_bre all_classification;
    clear all_itq all_isohash all_ksh all_agh;
    for m = all_m
        %     for m = 64
         type
         m
        load_eval_data;
        
        for k = idx_ks
            if ~is_no_itq
                all_table{k}(:, idx) = [eval_classification{k}.ap; ...
                    eval_mlh{k}.ap; ...
                    eval_bre{k}.ap; ...
                    eval_itq{k}.ap; ...
                    eval_isohash{k}.ap; ...
                    eval_ksh{k}.ap; ...
                    eval_agh{k}.ap; ...
                    eval_sh{k}.ap; ...
                    eval_usplh{k}.ap; ...
                    eval_lsh{k}.ap];
            else
                all_table{k}(:, idx) = [eval_classification{k}.ap; ...
                    eval_mlh{k}.ap; ...
                    eval_bre{k}.ap; ...
                    -1; ...
                    -1; ...
                    eval_ksh{k}.ap; ...
                    eval_agh{k}.ap; ...
                    eval_sh{k}.ap; ...
                    eval_usplh{k}.ap; ...
                    eval_lsh{k}.ap];
            end
        end
        
        idx = idx + 1;
    end
end
%%
for k = 1 : numel(idx_ks)
    table = all_table{k};
    write_table_five_column(table, all_topks(idx_ks(k)), all_m);
end
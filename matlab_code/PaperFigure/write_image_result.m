function write_image_result(type)

dir_project = '\\cng0060xl2\D$\Research\Hashing\OPHHash\';
file_name = [dir_project 'image_result' type '.tex'];

fp = fopen(file_name, 'w');

line = '\begin{figure}[t]';
fprintf(fp, '%s\n', line);

line = '\centering';
fprintf(fp, '%s\n', line);

num_rows = 7;
num_can = 6;
width = 0.135;

line = '\begin{tabular}{@{}';

fprintf(fp, '%s', line);
fprintf(fp, 'c@{~}');
for i = 1 : (num_can)
    fprintf(fp, 'c@{}');
end
fprintf(fp, '}\n');

for i = 1 : num_rows
    line = ['\includegraphics[width = ' num2str(width) '\linewidth]{figs/v/' type '/128/' num2str(i) '_0.eps}'];
    fprintf(fp, '%s\n', line);
    
    for j = 1 : num_can
        line = ['&\includegraphics[width = ' num2str(width) '\linewidth]{figs/v/' type '/128/' num2str(i) '_' num2str(j) '.eps}'];
        fprintf(fp, '%s\n', line);
    end
    line = '\\';
    fprintf(fp, '%s\n', line);
end

line = '\end{tabular}';
fprintf(fp, '%s\n', line);

line = '\caption{Qualitative results on ';
if strcmp(type, 'labelme')
    line = [line 'Labelme'];
else
    line = [line 'Peekaboom'];
end
line = [line '. The first image of each row is a query image. '...
    'The next six images are the results of OPH ranked by the hamming distance. }'];
fprintf(fp, '%s\n', line);

line = ['\label{fig:visible_' type '}'];
fprintf(fp, '%s\n', line);

line = '\end{figure}';
fprintf(fp, '%s\n', line);

fclose(fp);
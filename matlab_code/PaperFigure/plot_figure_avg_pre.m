
t = 1;

semilogx(eval_classification{k}.avg_retrieved, eval_classification{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
hold on;
semilogx(eval_mlh{k}.avg_retrieved, eval_mlh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_bre{k}.avg_retrieved, eval_bre{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);

t = t + 1;

if ~is_no_itq
    semilogx(eval_itq{k}.avg_retrieved, eval_itq{k}.pre, ...
        'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
    t = t + 1;
    semilogx(eval_isohash{k}.avg_retrieved, eval_isohash{k}.pre, ...
        'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
    t = t + 1;
else
    t = t + 1;
    t = t + 1;
end


semilogx(eval_ksh{k}.avg_retrieved, eval_ksh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_agh{k}.avg_retrieved, eval_agh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;

semilogx(eval_sh{k}.avg_retrieved, eval_sh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_usplh{k}.avg_retrieved, eval_usplh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);
t = t + 1;
semilogx(eval_lsh{k}.avg_retrieved, eval_lsh{k}.pre, ...
    'Color', curve_colors(t, :), 'Marker', markers{t}, 'LineWidth', 2);

%% xlim
xlim([1, 10^3]);
if k == 1
    xlim([1, 10^3]);
end

%% location
location = 'Best';
%%
% ylim_is_paper;

%%
% ylim_is_supp;

if ~is_no_itq
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'ITQ', 'ISH', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
else
    lgend = legend( 'OPH', 'MLH', 'BRE', ...
        'KSH', 'AGH', ...
        'SH', 'USPLH', 'LSH', ...
        'Location', location);
end
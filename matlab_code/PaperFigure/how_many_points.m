all_types = {'labelme', 'peekaboom', 'sift_1m', 'SIFT1M3'};

all_code = [32, 64, 128];
% all_code = [256];
is_mlh_val = 1;
figs_path = '\\cng0060xl2\D$\Research\Hashing\OPHHash\figs\';
all_topks = [1 10 100 200 400, 50];

clc;
r = 0.9; 
k = 1;
idx_type = 3;
type = all_types{idx_type};
m = 64;

load_eval_data;

'bre'
inter_p(eval_bre{k}.rec, eval_bre{k}.avg_retrieved, r)
'usplh'
inter_p(eval_usplh{k}.rec, eval_usplh{k}.avg_retrieved, r)
'sh'
inter_p(eval_sh{k}.rec, eval_sh{k}.avg_retrieved, r)
'mlh'
inter_p(eval_mlh{k}.rec, eval_mlh{k}.avg_retrieved, r)
'classification'
inter_p(eval_classification{k}.rec, eval_classification{k}.avg_retrieved, r)
'ITq'
inter_p(eval_itq{k}.rec, eval_itq{k}.avg_retrieved, r)
'lsh'
inter_p(eval_lsh{k}.rec, eval_lsh{k}.avg_retrieved, r)
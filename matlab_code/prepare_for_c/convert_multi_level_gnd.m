function convert_multi_level_gnd(matlab_data_file, c_data_file)

I = jf_load_gnd(matlab_data_file);
rho = numel(I);

[m, n] = size(I{1});
if (m ~= n)
    error('dfjls;df');
end

fp = fopen(c_data_file, 'wb');
fwrite(fp, m, 'int32');
fwrite(fp, m, 'int32');

for i = 1 : m
       if (mod(i, 20) == 0)
           i
       end
%     tmp_sp_vec = sparse(1, idx, rho, 1, n);
      tmp_sp_vec = zeros(1, m, 'int32');
      tmp_sp_vec(I{rho}(i, :)) = rho;
      
    for k = (rho - 1) : -1 : 1
        tmp_sp_vec(I{k}(i, :)) = k;
    end
    
    [idx2]  = find(tmp_sp_vec);
    non_zeros = numel(idx2);
    fwrite(fp, non_zeros, 'int32');
    fwrite(fp, idx2 - 1, 'int32'); % c is zero-based
    fwrite(fp, tmp_sp_vec(idx2), 'int32'); % grade is also int
end

fclose(fp);
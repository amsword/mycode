function convert_para(para, save_file)
fp = fopen(save_file, 'w');

fprintf(fp, 'm = %f\n', para.m);
fprintf(fp, 'lambda = %f\n', para.lambda);
fprintf(fp, 'beta = %f\n', para.beta);
fprintf(fp, 'mu = %f\n', para.mu);
fprintf(fp, 'rho = %d\n', numel(para.dk));
fprintf(fp, 'dk:\n');
rho = numel(para.dk);
for i = 1 : rho
    fprintf(fp, '%f\n', para.dk(i));
end
fprintf(fp, 'dk2:\n');
for i = 1 : rho
    fprintf(fp, '%f\n', para.dk(i));
end

fprintf(fp, 'dk3:\n');
for i = 1 : rho
    
    fprintf(fp, '%f\n', para.dk(i));
    
end

fprintf(fp, 'gamma1\n');
for i = 1 : rho
    
    fprintf(fp, '%f\n', para.gamma(i, 1));
end
fprintf(fp, 'gamma2\n');
for i = 1 : rho
    fprintf(fp, '%f\n', para.gamma(i, 2));
end

fprintf(fp, 'alpha\n');
for i = 1 : rho
    fprintf(fp, '%f\n', para.alpha(i));
end
fclose(fp);

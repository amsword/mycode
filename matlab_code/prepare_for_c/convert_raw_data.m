function convert_raw_data(matlab_data_file, c_data_file)
data = jf_load_origin_training(matlab_data_file);

fp = fopen(c_data_file, 'w');
[dim n] = size(data);
fwrite(fp, n, 'int32');
fwrite(fp, dim, 'int32');

fwrite(fp, data, 'double');

fclose(fp);
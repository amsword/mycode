function jf_print_back(N)
if (N <= 0)
	return;
end

for i = 1 : N
	fprintf('\b');
end
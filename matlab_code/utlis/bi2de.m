function y = bi2de(x)
[m, n] = size(x);
y = zeros(m, 1);
for i = 1 : m
    y(i) = bi2de_one(x(i, :), n);
end

function y = bi2de_one(x, n)
y = polyval(x, 0.5) * 2^(n - 1);
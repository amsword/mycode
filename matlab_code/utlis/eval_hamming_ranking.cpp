#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"
#include <vector>
#include <list>
using namespace std;

/*
function [retrieved_per_dist retrieved_good_per_dist]  = jf_eval_hash_lookup(S, Dhamm, maxn)

Input: 
  S: full matrix!!!. Element is byte type, S(i, j) indicate whether j is the neighbor of i
 *Dhamm: estimated hamming dist
 *maxn: maximum code length considered.

Output:
    result.pre(n)
 *  result.rec();
 *  result.total_retrieved_pairs
 *result.total_retrieved_good_pairs
 *result.total_good_pairs

/* Input Arguments */
#define	S   	prhs[0]  // Uint8 vector of size n x 1
#define DHAMM    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	RETRIEVED_GOOD_SUM	plhs[0]  // Double vector 1 x m binary hamming distance

const int lookup [] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};

void ScanResult(int n_test, int n_base,
        bool* p_S, unsigned short* p_Dhamm,
        int maxn, 
        double* p_retrieved_good_sum)
{
    int i;
    int j;
	unsigned short d;
	int k;
    //initialize
   memset(p_retrieved_good_sum, 0, sizeof(double) * n_base);
	
	k = 0;
    
	vector<list<int>> vec_lst;

	for (int i = 0; i < n_test; i++)
    {
		vec_lst.clear();
		vec_lst.resize(maxn + 1);

        for (int j = 0; j < n_base; j++)
		{
			int curr_hamming_dist = p_Dhamm[j * n_test + i];
			bool is_right = p_S[j * n_test + i];
			if (is_right) vec_lst[curr_hamming_dist].push_back(1);
			else vec_lst[curr_hamming_dist].push_back(0);
		}

		int good_now = 0;
		int idx = 0;
		for (int j = 0; j < vec_lst.size(); j++)
		{
			list<int> &lst = vec_lst[j];
			for (list<int>::iterator iter = lst.begin(); iter != lst.end(); iter++)
			{
				good_now += *iter;
				p_retrieved_good_sum[idx++] += good_now;
			}
		}

    }
}

//[retrieved_good_sum] = 
//          result.eval_hamming_ranking(S, Hamming_dist, m)

void mexFunction(int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
    int n_test, n_base, maxn;
	double* p_retrieved_good_sum;
	bool* p_S;
	unsigned short* p_Dhamm;
   
    /* Check for proper number of arguments */    
    if (nrhs != 3) 
	{ 
		mexErrMsgTxt("Three input arguments required."); 
    }
    
    if (!mxIsLogical(S))
	mexErrMsgTxt("Matrix must be bool");
    
    if (!mxIsUint16(DHAMM))
	mexErrMsgTxt("Vector must be uInt16");
    
    /* Get dimensions of inputs */
    n_test = (int) mxGetM(S); 
    n_base = (int) mxGetN(S); 
    
    if ((int)mxGetM(DHAMM)!= n_test || (int) mxGetN(DHAMM) != n_base)
	{
		mexErrMsgTxt("Dimension mismatch btw. matrix and vector");
    }
	maxn = (double)*mxGetPr(prhs[2]);
	
    // Create output array
    RETRIEVED_GOOD_SUM = mxCreateNumericMatrix(n_base, 1, mxDOUBLE_CLASS, mxREAL);
    p_retrieved_good_sum = (double*) mxGetPr(RETRIEVED_GOOD_SUM);

	p_S = (bool*) mxGetPr(S);
	p_Dhamm = (unsigned short*) mxGetPr(DHAMM);
	
    ScanResult(n_test, n_base,
        p_S, p_Dhamm,
        maxn, 
        p_retrieved_good_sum);    
}
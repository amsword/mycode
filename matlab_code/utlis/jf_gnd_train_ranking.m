function [exp_dist avg_num_neighbour all_s_idx] = jf_gnd_train_ranking(src, m)
%%
OriginXtraining = jf_load_origin_training(src);

if (nargout == 1)
	[exp_dist] = ...
		jf_gnd_train_query_ranking(OriginXtraining, OriginXtraining, m);
else
	[exp_dist avg_num_neighbour all_s_idx] = ...
		jf_gnd_train_query_ranking(OriginXtraining, OriginXtraining, m);
end
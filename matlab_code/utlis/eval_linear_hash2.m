function [p1 r1] = eval_linear_hash2(W, data)




metric_info.type = 0;

eval = eval_hash4(size(W, 1), 'linear', W', data.Xtest, data.Xtraining, data.StestTraining, ...
    [],  [], [], metric_info);


p1 = eval.pre;
r1 = eval.rec;


function data = do_normalize(data, nb);

% performing PCA on the data structure
d = size(data.Xtraining, 1);
data.Xtraining = data.Xtraining - repmat(mean(data.Xtraining), d, 1);
%data.Xtraining = data.Xtraining ./ repmat(max(abs(data.Xtraining)), d, 1);
if (isfield(data, 'Xtest'))
    data.Xtest = data.Xtest - repmat(mean(data.Xtest), d, 1);
    %data.Xtest = data.Xtest ./ repmat(max(abs(data.Xtest)), d, 1);
end
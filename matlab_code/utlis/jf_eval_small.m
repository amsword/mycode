function result = jf_eval_small(trueRank, rank, topK, cared_num_can)
% hamming ranking
% result.p_code
% result.p_code_sum
% result.r_code
% result.r_code_sum

[nimagestest, sizedatabase] = size(rank);

j = trueRank(:,1:topK);
jj = false(size(rank));

for n = 1:nimagestest
  jj(n,:) = ismember(rank(n,:), j(n,:));
end

cum = cumsum(jj,2);

P = cum / topK;

if nargin == 4
    result.every_r_code = P(:, cared_num_can);
end

result.r_code = mean(P,1)';
result.r_code_sum = sum(P, 1)';

P = bsxfun(@rdivide, cum, [1:sizedatabase]);
result.p_code = mean(P,1)';
result.p_code_sum = sum(P, 1)';
function [p_code_sum r_code_sum] = eval_hamming_rankingm(S, Dhamm, m, topk)

good_sum = eval_hamming_ranking(S, Dhamm, m);

r_code_sum = good_sum / topk;
p_code_sum = good_sum ./ [1 : numel(good_sum)]';
    


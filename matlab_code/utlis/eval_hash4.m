function eval = eval_hash4(m, type, para, Xtest, Xbase, StestBase, ...
    topK,  trueRank_fileseed, irrelevance, metric_info)
%% m: code length
%% type: linear, para is W
%% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
%% Xbase(:, i) is the neighbor of Xtest(:, i);
%% topK and DtestBase are used to do hamming ranking

global gl_is_hamming_ranking;
global gl_is_topK;
global gl_is_hash_lookup;
global gl_is_ndcg;
global gl_is_success_rate;

gl_is_hamming_ranking = 0;
gl_is_topK = 0;
gl_is_hash_lookup = 1;
gl_is_success_rate = 0;
gl_is_ndcg = 0;

Nbase = size(Xbase, 2);
Ntest = size(Xtest, 2);

batchsize = 0.1 * 1024^3 / 8 / Nbase; % if the memory is an issue, test batch by batch.
batchsize = floor(batchsize);
nbatch = ceil(Ntest / batchsize);

if (gl_is_hamming_ranking)
    % hanmming ranking
    P_CODE = zeros(Nbase, 1);
    R_CODE = zeros(size(P_CODE));
end

if (gl_is_hash_lookup)
    % hamming lookup
    total_good_pairs = 0;
    total_retrieved_good_pairs = zeros(m + 1, 1);
    total_retrieved_pairs = total_retrieved_good_pairs;
end

if (gl_is_topK)
    %% topK ranking
    total_precision_topK = zeros(Nbase, 1);
end

if (gl_is_ndcg)
    ndcg_m = max(max(irrelevance));
    relevance = double(ndcg_m) + 1 - double(irrelevance);
    clear irrelevance;
    
    total_dcg_1 = zeros(1, Nbase);
    total_dcg_2 = total_dcg_1;
end

if (gl_is_success_rate)
    num_success = 0;
end

is_linear = false;
is_ch = false;
is_sh = false;
is_bre = false;

nwords = ceil(m / 8);
if (strcmp(type, 'linear'))
    is_linear = true;
    W = para;
    B_base = W' * [Xbase; ones(1,Nbase)] > 0;
    byte_base = compactbit(B_base);
    
    %     B_test = false(m, batchsize);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');
elseif (strcmp(type, 'ch'))
    is_ch = true;
    
    cell_byte_base = cell(para.L, 1);
    for j = 1 : para.L
        cell_byte_base{j} = compressCH(Xbase', para.A{j}, para.B{j});
    end
    
    Dhamm = zeros(batchsize, Nbase, 'uint16');
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    %     D_code = zeros(batchsize, Nbase, 'uint16');
elseif (strcmp(type, 'sh'))
    [B_base] = hash_code_SH(Xbase', para);
    is_sh = true;
    byte_base = compactbit(B_base);
elseif (strcmp(type, 'bre'))
    is_bre = true;
    W = para.W;
    hash_inds = para.hash_inds;
    %     Ktest = Xbase' * para.Xtraining; % it should be Kbase, but we consider the memory
    B_base = false(Nbase, m);
    for b = 1 : m
        Ktest = Xbase' * para.Xtraining(:, hash_inds(:,b));
        %         B_base(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
        B_base(:,b) = Ktest * W(:,b) > 0;
    end
    byte_base = compactbit(B_base');
    
    %     Ktest = zeros(batchsize, size(para.Xtraining, 2));
    trans_B_test = false(batchsize, m);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');
end

if (metric_info.type ~= 0 && (is_ch || gl_is_hash_lookup))
    error('NO supporting ch withe other metric type');
end

for i = 1:nbatch
    b_start = 1 + (i - 1) * batchsize;
    if (b_start > Ntest)
        break;
    end
    b_end = i*batchsize;
    b_end = min(b_end, Ntest);
    
    if (is_ch)
        if ((b_end - b_start + 1) ~= batchsize)
            Dhamm = zeros(b_end - b_start + 1, Nbase, 'uint16');
        end
        Dhamm(:) = Dhamm(:)+inf;
        for j = 1 : para.L
            byte_test = compressCH(Xtest(:, b_start:b_end)', para.A{j}, para.B{j});
            D_code = jf_hammingDist2(byte_test, cell_byte_base{j});
            Dhamm = min(D_code, Dhamm);
        end
    else
        if (is_linear)
            % no need to delete the previous space
            B_test= W' * [Xtest(:, [b_start : b_end]); ones(1,b_end - b_start + 1)] > 0;
        elseif (is_sh)
            B_test = hash_code_SH(Xtest(:, b_start : b_end)', para);
        elseif (is_bre)
            if ((b_end - b_start + 1) ~= batchsize)
                trans_B_test = false(b_end - b_start + 1, m);
            end
            Ktest = Xtest(:, [b_start : b_end])' * para.Xtraining;
            for b = 1 : m
                trans_B_test(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
            end
            B_test = trans_B_test';
        end
        
        if (metric_info.type == 0)
            byte_test = compactbit(B_test);
            Dhamm = jf_hammingDist2(byte_test, byte_base);
        else
            Dhamm = calc_metric_dist(B_test, B_base', metric_info);
        end
    end
    
    if (gl_is_hash_lookup)
        % hamming lookup
        %result = jf_eval_hash_lookup_fast(StestBase(b_start:b_end, :), Dhamm, m);
        % Note: here StestBase is the index, from 1.
        S = StestBase(b_start:b_end, :);
        result.total_good_pairs = numel(S);
        
        num_test = b_end - b_start + 1;
        
        % construct the full index matrix
        one_idx = (S - 1) * (num_test) + ...
            repmat([1 : num_test]', 1, size(S, 2));
        
        fS = false(num_test, Nbase);
        fS(one_idx) = true;
        
        [result.total_retrieved_pairs result.total_retrieved_good_pairs] = ...
            jf_eval_hash_lookup2(fS, uint16(Dhamm), m);
        
        result.total_retrieved_pairs = cumsum(result.total_retrieved_pairs);
        result.total_retrieved_good_pairs = cumsum(result.total_retrieved_good_pairs);
        result.pre =  result.total_retrieved_good_pairs ./ result.total_retrieved_pairs;
        result.rec= result.total_retrieved_good_pairs / result.total_good_pairs;
        
        total_good_pairs = result.total_good_pairs + total_good_pairs;
        total_retrieved_good_pairs = ...
            result.total_retrieved_good_pairs + total_retrieved_good_pairs;
        total_retrieved_pairs = ...
            total_retrieved_pairs + result.total_retrieved_pairs;
    end
    
    if (gl_is_hamming_ranking || gl_is_topK)
        % hamming ranking
        file_name = jf_gen_gnd_file(trueRank_fileseed, b_start, b_end);
        load_result = load(file_name, 'idx');
        trueRank = load_result.idx;
        clear load_result;
    end
    
    if (gl_is_hamming_ranking || gl_is_ndcg || gl_is_topK)
        [~, Rank] = sort(Dhamm, 2, 'ascend');
    end
    
    if (gl_is_hamming_ranking)
        result = jf_eval_small(trueRank, Rank, topK);
        P_CODE = P_CODE + result.p_code_sum;
        R_CODE = R_CODE + result.r_code_sum;
    end
    
    if (gl_is_topK)
        %% topK ranking
        result = jf_eval_topK(trueRank, Rank);
        
        total_precision_topK = total_precision_topK + ...
            result.topK_sum';
    end
    
    if (gl_is_ndcg)
        result = jf_sum_DCG(Rank, relevance(b_start : b_end, :));
        total_dcg_1 = total_dcg_1 + result.s_dcg1;
        total_dcg_2 = total_dcg_2 + result.s_dcg2;
    end
    
    if (gl_is_success_rate)
        [min_dist] = min(Dhamm, [], 2);
        num_success = num_success + sum(min_dist <= 0.01);
    end
    
    fprintf('batch %d/%d\n', i, nbatch);
    
    if (gl_is_hamming_ranking)
        eval.p_code = P_CODE / Ntest;
        eval.r_code = R_CODE / Ntest;
    end
    
    if (gl_is_hash_lookup)
        eval.rec = total_retrieved_good_pairs / total_good_pairs;
        eval.pre = total_retrieved_good_pairs ./ total_retrieved_pairs;
        
        eval.avg_retrieved_good = total_retrieved_good_pairs / Ntest;
        eval.avg_retrieved = total_retrieved_pairs / Ntest;
        eval.avg_good = total_good_pairs / Ntest;
    end
    
    if (gl_is_topK)
        eval.avg_precision_topK = total_precision_topK / Ntest;
    end
    
    if (gl_is_ndcg)
        [~, Rank] = sort(relevance(1, :), 'descend');
        result = jf_sum_DCG(Rank, relevance(1, :));
        eval.ndcg1 = total_dcg_1 / Ntest ./ result.s_dcg1;
        eval.ndcg2 = total_dcg_2 / Ntest ./ result.s_dcg2;
    end
    
    if (gl_is_success_rate)
        eval.success_rate = num_success / Ntest;
    end
end
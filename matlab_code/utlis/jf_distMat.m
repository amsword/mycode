function D = jf_distMat(X1, X2, range)
% Pairwise Euclidian distances between data points
% Each data point is one column

if (nargin == 1)
    R = X1'*X1;
    sqrX1 = sum(X1.^2);
    D = bsxfun(@plus, sqrX1', sqrX1);
    D = real(sqrt(D - 2*R));
elseif (nargin == 2)
    R = X1'*X2;
    sqrX1 = sum(X1.^2);
    sqrX2 = sum(X2.^2);
    D = bsxfun(@plus, sqrX1', sqrX2);
    D = real(sqrt(D-2*R));
elseif (nargin == 3)
    R = X1(:, range(1) : range(2))' * X1;
    sqrX2 = sum(X1.^2);
    sqrX1 = sqrX2(range(1) : range(2));
    D = bsxfun(@plus, sqrX1', sqrX2);
    D = real(sqrt(D-2*R));
end
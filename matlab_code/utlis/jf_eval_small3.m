function [result] = jf_eval_small3(fS, Rank, topk)

nrows = size(Rank, 1);
ncols = size(Rank, 2);

one_idx = (Rank - 1) * (nrows) + ...
    repmat([1 : nrows]', 1, ncols);

% x = fS(one_idx(:));
% good = reshape(x, nrows, ncols);
good = fS(one_idx);

assert(size(good, 1) == nrows && size(good, 2) == ncols);

good = cumsum(good, 2);

rec = good / topk;
pre = bsxfun(@rdivide, good, 1 : ncols);

x1 = diff([zeros(nrows, 1), rec], 1, 2);
result.ap_sum = sum(sum(x1 .* pre));

good = sum(good, 1);

result.r_code_sum = (good / topk)';
result.p_code_sum = (good ./ [1 : ncols])';
    


function I = jf_load_gnd(src_file, idx_start, idx_end)
if (nargin == 1)
    load(src_file, 'I');
elseif (nargin == 3)
    how_long = numel(src_file);
    if (strcmp(src_file(how_long - 3 : how_long), '.mat'))
        src_file = src_file(1 : how_long - 4);
    end
    src_file = [src_file num2str(idx_start) '_' num2str(idx_end)];
    load(src_file, 'I');
end
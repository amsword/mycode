function W = get_initW(initW, D, m)
if (isempty(initW))
    'random generate W'
    W = [randn(D, m); zeros(1, m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
else
    'W has been initilized'
    W = initW;
end
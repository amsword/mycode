function [I avg_num] = load_gnd_epsilon(src_file, epsilon)
% I{i} stores the idx of the ground truth
I = read_epsilon_gnd(src_file, 'double', epsilon);

avg_num = 0;
for i = 1 : numel(I)
    avg_num = avg_num + numel(I{i});
end
avg_num = avg_num / numel(I);

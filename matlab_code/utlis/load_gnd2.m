function I = load_gnd2(src_file, topk, idx_start, idx_end)
if (nargin == 2)   
    [~, I] = read_gnd(src_file, topk, 'double');
    I = I + 1;
    I = double(I);
     
elseif (nargin == 3)
    how_long = numel(src_file);
    if (strcmp(src_file(how_long - 3 : how_long), '.mat'))
        src_file = src_file(1 : how_long - 4);
    end
    src_file = [src_file num2str(idx_start) '_' num2str(idx_end)];
    load(src_file, 'I');
    
else
    error('error...');
end
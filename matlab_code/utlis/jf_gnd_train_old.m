function I = jf_gnd_train_old(src, avg_num_neighbour, dst)
OriginXtraining = jf_load_origin_training(src);

N = size(OriginXtraining, 2);
dist = jf_distMat(OriginXtraining);
s_dist = sort(dist, 2);
rho = length(avg_num_neighbour);
thresDists = zeros(N, rho);
for i = 1:rho
    thresDists(:, i) = s_dist(:, avg_num_neighbour(i)); % the threshold distance for graded neighbors
end
clear s_dist
I = cell(rho, 1);
for k = 1:rho
    I{k} = bsxfun(@le, dist, thresDists(:, k));
end
clear dist;

jf_save_gnd(I, dst);
function y = jf_gen_gnd_file(file_seed, idx_start, idx_end)
y = [file_seed '_' num2str(idx_start) '_' num2str(idx_end)];
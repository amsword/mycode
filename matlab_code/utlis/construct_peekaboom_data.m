function data = construct_peekaboom_data(Xtraining, Xtest, Xbase, sizeSets, avgNNeighbors, proportionNeighbors, data)
%% peekaboom data construction is slightly different
% only extract part of the data as training data (Xtrain) and the rest as
% database for query (Xbase)
%%

[Ntraining, Ntest] = deal(sizeSets(1), sizeSets(2));
Nbase = size(Xbase, 1);
Dtraining = distMat(Xtraining);

% NNeighbors = [30, 300, 1500];
NNeighbors = [30, 300, 1300];
%NNeighbors = [30, 300, 900];
nRhos = size(NNeighbors, 2);

if (~isempty(avgNNeighbors))
    tic;
    sortedD = sort(Dtraining, 2);
    toc;
    threshDist = mean(sortedD(:,avgNNeighbors));
    data.avgNNeighbors = avgNNeighbors;
    
    threshDists = zeros(Ntraining, nRhos);
    for i = 1:nRhos
        % threshDists(i) = mean(sortedD(:, NNeighbors(i)));
        threshDists(:, i) = sortedD(:, NNeighbors(i));
    end
    clear sortedD;
else
    
  sortedD = sort(Dtraining(:));
  threshDist = sortedD(ceil(proportionNeighbors * numel(sortedD)));
  data.proportionNeighbors = proportionNeighbors;
end

for i = 1:nRhos
    %thresDistMat = repmat(threshDists(:, i), [1, Ntraining]);
    tic;
    data.Strainings(i, :, :) = bsxfun(@lt, Dtraining, threshDists(:, i));
    toc;
    %data.Strainings(i, :, :) = (Dtraining < thresDistMat);
end


data.Xtraining = Xtraining;
data.Xtest = Xtest;
clear Xtraining;
clear Xtest;
data.Straining = Dtraining < threshDist;
data.Dtraining = Dtraining; % just for BRE
clear Dtraining;

% tiya: Straining and StestTraining is the neighbor-indicator
data.DtestTraining = distMat(data.Xtest, Xbase); % size = [Ntest x Ntraining]
data.avgNNeighbors = avgNNeighbors;
data.Xbase = Xbase;
data.StestTraining = data.DtestTraining < threshDist;
data.Ntraining = Ntraining;
data.Ntest = Ntest;
data.threshDist = threshDist;
data.threshDists = mean(threshDists, 1);
data.Nbase = size(Xbase, 2);
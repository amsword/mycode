function eval = eval_hash_all_topk5_all_zero(m, type, para, Xtest, Xbase, StestBase, ...
    topKs,  trueRank_fileseed, irrelevance, metric_info, eval_types)
% m: code length
% type: linear, para is W
% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
% Xbase(:, i) is the neighbor of Xtest(:, i);
% topK and DtestBase are used to do hamming ranking
% NOTE: no need to load true ranking for hamming ranking

is_hamming_ranking = eval_types.is_hamming_ranking;
is_topK = eval_types.is_topk; assert(~is_topK);
is_hash_lookup = eval_types.is_hash_lookup;
is_ndcg = eval_types.is_ndcg; assert(~is_ndcg);
is_success_rate = eval_types.is_success_rate; assert(~is_success_rate);

Nbase = size(Xbase, 2);
Ntest = size(Xtest, 2);

batchsize = 0.1 * 1024^3 / 8 / Nbase; % if the memory is an issue, test batch by batch.
batchsize = floor(batchsize);
nbatch = ceil(Ntest / batchsize);

if (is_hamming_ranking)
    % hanmming ranking
    P_CODE = cell(numel(topKs), 1);
    R_CODE = cell(numel(topKs), 1);
    ap = cell(numel(topKs), 1);
    
    for k = 1 : numel(topKs)
        P_CODE{k} = zeros(Nbase, 1);
        R_CODE{k} = zeros(Nbase, 1);
        ap{k} = 0;
    end
    
end

is_linear = false;
is_ch = false;
is_sh = false;
is_bre = false;
is_ksh = false;
is_agh = false;

nwords = ceil(m / 8);
if (strcmp(type, 'linear'))
    is_linear = true;
end

if (metric_info.type ~= 0 && (is_ch || is_hash_lookup))
    error('NO supporting ch withe other metric type');
end

for i = 1:nbatch
    b_start = 1 + (i - 1) * batchsize;
    if (b_start > Ntest)
        break;
    end
    b_end = i*batchsize;
    b_end = min(b_end, Ntest);
    
    Dhamm = zeros(b_end - b_start + 1, Nbase, 'uint16');
    Dhamm(:) = 0;
    
    for k = 1 : numel(topKs)
        if (is_hash_lookup || is_hamming_ranking)
            S = StestBase(b_start:b_end, 1 : topKs(k));
            result.total_good_pairs = numel(S);
            
            num_test = b_end - b_start + 1;
            
            % construct the full index matrix
            one_idx = (S - 1) * (num_test) + ...
                repmat([1 : num_test]', 1, size(S, 2));
            
            fS = false(num_test, Nbase);
            fS(one_idx) = true;
        end
        
        if (is_hash_lookup)
            % hamming lookup
            %result = jf_eval_hash_lookup_fast(StestBase(b_start:b_end, :), Dhamm, m);
            % Note: here StestBase is the index, from 1.
            
            [result.total_retrieved_pairs result.total_retrieved_good_pairs] = ...
                jf_eval_hash_lookup2(fS, uint16(Dhamm), m);
            
            result.total_retrieved_pairs = cumsum(result.total_retrieved_pairs);
            result.total_retrieved_good_pairs = cumsum(result.total_retrieved_good_pairs);
            result.pre =  result.total_retrieved_good_pairs ./ result.total_retrieved_pairs;
            result.rec= result.total_retrieved_good_pairs / result.total_good_pairs;
            
            total_good_pairs{k} = result.total_good_pairs + total_good_pairs{k};
            total_retrieved_good_pairs{k} = ...
                result.total_retrieved_good_pairs + total_retrieved_good_pairs{k};
            total_retrieved_pairs{k} = ...
                total_retrieved_pairs{k} + result.total_retrieved_pairs;
        end
        
        if (is_topK)
            % hamming ranking
            file_name = jf_gen_gnd_file(trueRank_fileseed, b_start, b_end);
            load_result = load(file_name, 'idx');
            trueRank = load_result.idx;
            clear load_result;
        end
        
        if (is_ndcg || is_topK)
            [~, Rank] = sort(Dhamm, 2, 'ascend');
        end
        
        if (is_hamming_ranking)
            %    [p_code_sum r_code_sum] = eval_hamming_rankingm(S, Dhamm, m, topK)
            %             [~, Rank] = sort(Dhamm, 2, 'ascend');
            %             result2 = jf_eval_small3(fS, Rank, topKs(k));
            result = eval_hamming_ranking3(fS, Dhamm, m, topKs(k));
            %             assert(sum(abs(result2.p_code_sum - result.p_code_sum)) / sum(result.p_code_sum) < 10^-7);
            %             assert(sum(abs(result2.r_code_sum - result.r_code_sum)) / sum(result.r_code_sum) < 10^-7);
            %             assert(abs(result.ap_sum - result2.ap_sum) < 10^-5);
            %         result = jf_eval_small(S, Rank, topK);
            %         result = jf_eval_small(trueRank, Rank, topK);
            P_CODE{k} = P_CODE{k} + result.p_code_sum;
            R_CODE{k} = R_CODE{k} + result.r_code_sum;
            ap{k} =ap{k} + result.ap_sum;
        end
        
        if (is_topK)
            %% topK ranking
            result = jf_eval_topK(trueRank, Rank);
            
            total_precision_topK = total_precision_topK + ...
                result.topK_sum';
        end
        
        if (is_ndcg)
            result = jf_sum_DCG(Rank, relevance(b_start : b_end, :));
            total_dcg_1 = total_dcg_1 + result.s_dcg1;
            total_dcg_2 = total_dcg_2 + result.s_dcg2;
        end
        
        if (is_success_rate)
            [min_dist] = min(Dhamm, [], 2);
            num_success = num_success + sum(min_dist <= 0.01);
        end
    end
    
    fprintf('batch %d/%d\n', i, nbatch);
end

eval = cell(numel(topKs), 1);

if (is_hamming_ranking)
    for k = 1 : numel(topKs)
        eval{k}.p_code = P_CODE{k} / Ntest;
        eval{k}.r_code = R_CODE{k} / Ntest;
        eval{k}.ap = ap{k} / Ntest;
    end
end

if (is_hash_lookup)
    for k = 1 : numel(topKs)
        eval{k}.rec = total_retrieved_good_pairs{k} / total_good_pairs{k};
        eval{k}.pre = total_retrieved_good_pairs{k} ./ total_retrieved_pairs{k};
        
        eval{k}.avg_retrieved_good = total_retrieved_good_pairs{k} / Ntest;
        eval{k}.avg_retrieved = total_retrieved_pairs{k} / Ntest;
        eval{k}.avg_good = total_good_pairs{k} / Ntest;
    end
end

if (is_topK)
    eval.avg_precision_topK = total_precision_topK / Ntest;
end

if (is_ndcg)
    [~, Rank] = sort(relevance(1, :), 'descend');
    result = jf_sum_DCG(Rank, relevance(1, :));
    eval.ndcg1 = total_dcg_1 / Ntest ./ result.s_dcg1;
    eval.ndcg2 = total_dcg_2 / Ntest ./ result.s_dcg2;
end

if (is_success_rate)
    eval.success_rate = num_success / Ntest;
end
#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"
#include <vector>
#include <list>
#include <omp.h>
#include <algorithm>
using namespace std;

/*
 * function [retrieved_per_dist retrieved_good_per_dist]  = jf_eval_hash_lookup(S, Dhamm, maxn)
 *
 * Input:
 * S: full matrix!!!. Element is byte type, S(i, j) indicate whether j is the neighbor of i
 *Dhamm: estimated hamming dist
 *maxn: maximum code length considered.
 *
 * Output:
 * result.pre(n)
 *  result.rec();
 *  result.total_retrieved_pairs
 *result.total_retrieved_good_pairs
 *result.total_good_pairs
 *
 * /* Input Arguments */
#define	IN_DIST   	prhs[0]  // Uint8 vector of size n x 1
#define IN_TOPK    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	OUT_NN_IDX	plhs[0]  // Double vector 1 x m binary hamming distance

void ScanResult(int n_test, int n_base,
        double* p_dist,
        int topk,
        double* p_out)
{
    int* seq_idx = new int[n_base * n_test];
    
    omp_set_num_threads(24);
    #pragma omp parallel for
    for (int j = 0; j < n_test; j++)
    {
        int* p_curr_idx = seq_idx + j * n_base;
        double* p_curr_dist = p_dist + j * n_base;
        
        for (int  i = 0; i < n_base; i++)
        {
            p_curr_idx[i] = i;
        }
        partial_sort(p_curr_idx, p_curr_idx + topk, p_curr_idx + n_base, [p_curr_dist](int left, int right)
        {
            return p_curr_dist[left] <= p_curr_dist[right];
        });
        
        double* p_curr_out = p_out + j * topk;
        for (int i = 0; i < topk; i++)
        {
            *p_curr_out++ = p_curr_idx[i] + 1;
        }
    }
    delete seq_idx;
}

//[retrieved_good_sum] =
//          mex_partial_sort(dist, topk)

void mexFunction(int nlhs, mxArray *plhs[],
        int nrhs, const mxArray*prhs[] )
        
{
    int n_test, n_base, maxn;
    double* p_out_nn_idx;
    double* p_dist;
    int topk;
    double* p;
    
    /* Check for proper number of arguments */
    if (nrhs != 2)
    {
        mexErrMsgTxt("Three input arguments required.");
    }
    
    /* Get dimensions of inputs */
    n_base = (int) mxGetM(IN_DIST);
    n_test = (int) mxGetN(IN_DIST);
    p_dist = (double*)mxGetPr(IN_DIST);
    
    p = (double*)mxGetPr(IN_TOPK);
    topk = *p;
    // Create output array
    OUT_NN_IDX = mxCreateNumericMatrix(topk, n_test, mxDOUBLE_CLASS, mxREAL);
    p_out_nn_idx = (double*) mxGetPr(OUT_NN_IDX);
    
    ScanResult(n_test, n_base,
        p_dist,
        topk,
        p_out_nn_idx);
}
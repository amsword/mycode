function result = eval_small(j_truth, j_code, evalParam)
% the reason of the following two conditional statement is because,
% sometimes, it seems the code in Tiansheng's codes will call this
% function with j_code (N*1)
if (size(j_truth, 2) == 1)
    j_truth = j_truth';
end

if (size(j_code, 2) == 1)
    j_code = j_code';
end


evalParam.M = 400;
% recall@number
result.r_code = neighbors_recall(j_truth, j_code, evalParam.M);
% precision@number
result.p_code = neighbors_precision(j_truth, j_code, evalParam.M);

% accuracy@number
rho_max = evalParam.rho;
% [b, i, j] = unique(evalParam.foo_code, 'last');
% k = 1;
% for rho = 0:rho_max
%     if b(k)~=rho
%         n_true_points(rho+1) = 0;
%         n_points_rho(rho+1) = 0;
%         prec_at_rho(rho+1) = 0;
%     else
%         n_points_rho(rho+1) = i(k);
%         n_true_points(rho+1) = sum(ismember(j_code(1:i(k)), j_truth(1:i(k))));
%         prec_at_rho(rho+1) = n_true_points(rho+1) / n_points_rho(rho+1);
%         k = k+1;
%     end
% end
% result.prec_at_rho = prec_at_rho;
% result.n_points_rho = n_points_rho;

rho = rho_max;
result.prec_at_rho = zeros(1, rho+1);
result.n_points_rho = zeros(1, rho+1);


    

    
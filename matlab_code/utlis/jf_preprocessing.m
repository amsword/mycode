function jf_preprocessing(Xtraining, Xbase, Xtest, save_file);
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

%% configuration. we will not change it generally.
is_mean_removed = 1;
is_normalize = 0;
is_perform_pca = 0;
num_princomp = 40;

%% mean removing
center_train = mean(Xtraining, 2);
if (is_mean_removed)
	Xtraining = mean_remove(Xtraining, center_train);
	Xbase = mean_remove(Xbase, center_train);
	Xtest = mean_remove(Xtest, center_train);
end

%% normalization
if (is_normalize)
    OriginXtraining = unit_normalize(Xtraining);
	OriginXbase = unit_normalize(Xbase);
	OriginXtest = unit_normalize(Xtest);
end

%% pca
if (is_perform_pca)
    [Xtraining, pc] = jf_do_pca(OriginXtraining, num_princomp); 
	
	Xbase = pc' * OriginXbase;
	
	Xtest = pc' * OriginXtest;
end

save(save_file.train, 'Xtraining', 'OriginXtraining');
save(save_file.base, 'Xbase', 'OriginXbase');
save(save_file.test, 'Xtest', 'OriginXtest');

save(save_file.other_info, ...
    'is_mean_removed', 'is_normalize',...
    'is_perform_pca', 'num_princomp', 'pc');

function y = mean_remove(x, center)
y = x - repmat(center, 1, size(x, 2));

function y = unit_normalize(x)
normX = sqrt(sum(x.^2, 1));
y = bsxfun(@rdivide, x, normX);
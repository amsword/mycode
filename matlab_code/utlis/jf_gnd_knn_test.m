function jf_gnd_knn_test(Ntraining, Ntest, ...
    avgNNeighbors, save_file_sign, save_gnd_seed, type)
% load(test_file, 'OriginXtest');
% load(base_file, 'OriginXbase');
% load(base_file, 'OriginXtraining');

% Ntest = size(OriginXtest, 2);
% Nbase = size(OriginXbase, 2);

max_size = 0.1 * 1024^3 / 8;
batchsize = floor(max_size / Ntraining);
nbath = ceil(Ntest / batchsize);

subSs = cell(nbath, 1);
num_nonzeros = 0;
for i = 1 : nbath
    ['start ' num2str(i) ' batch of ' num2str(nbath)]
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, Ntest);
    num = idx_end - idx_start + 1;
    
%     DtestBase = jf_distMat(OriginXtest(:, idx_start : idx_end), ...
%         OriginXbase); % size = [Ntest x Ntraining]
    
    if (strcmp(type, 'knn'))
        file_name = jf_gen_gnd_file(save_gnd_seed, idx_start, idx_end);
        x = load(file_name, 'idx', 'idx_start', 'idx_end');
        
        if (x.idx_start ~= idx_start || ...
                x.idx_end ~= idx_end)
            error('error');
        end
        idx = double(x.idx);
        clear x;
        
%         [sortedD, idx] = sort(DtestBase, 2);
        sub_idx = idx(:, [1 : avgNNeighbors]);
        
        subS = false(idx_end - idx_start + 1, Ntraining);
        sub_idx = (sub_idx - 1) * num + repmat((1 : num)', 1, avgNNeighbors);
        subS(sub_idx(:)) = true;
        [idx_i, idx_j] = find(subS);
        
        subSs{i}.idx_i = idx_i + idx_start - 1;
        subSs{i}.idx_j = idx_j;
        num_nonzeros = num_nonzeros + numel(idx_i);
    end
end

idx_i = zeros(num_nonzeros, 1);
idx_j = zeros(num_nonzeros, 1);

start = 1;
for i = 1 : nbath
    tmp_num = numel(subSs{i}.idx_i);
    idx_i(start : start + tmp_num - 1) = subSs{i}.idx_i;
    idx_j(start : start + tmp_num - 1) = subSs{i}.idx_j;
    start = start + tmp_num;
end
StestBase = sparse(idx_i, idx_j, true, Ntest, Ntraining);

jf_save_gnd(StestBase, save_file_sign);
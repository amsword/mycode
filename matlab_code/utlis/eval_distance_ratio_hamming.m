function mean_ratio = eval_distance_ratio_hamming(m, type, para, Xtest, Xbase, TrueDistTestBase, ...
      metric_info)
% m: code length
% type: linear, para is W
% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
% Xbase(:, i) is the neighbor of Xtest(:, i);
% topK and DtestBase are used to do hamming ranking
% NOTE: no need to load true ranking for hamming ranking

Nbase = size(Xbase, 2);
Ntest = size(Xtest, 2);

batchsize = 0.1 * 1024^3 / 8 / Nbase; % if the memory is an issue, test batch by batch.
batchsize = floor(batchsize);
nbatch = ceil(Ntest / batchsize);

    mean_ratio = zeros(1, 10^4);



is_linear = false;
is_ch = false;
is_sh = false;
is_bre = false;
is_ksh = false;
is_agh = false;

nwords = ceil(m / 8);
if (strcmp(type, 'linear'))
    is_linear = true;
    W = para;
    B_base = W' * [Xbase; ones(1,Nbase)] > 0;
    byte_base = compactbit(B_base);
    
    Dhamm = zeros(batchsize, Nbase, 'uint16');
elseif (strcmp(type, 'ch'))
    is_ch = true;
    
    cell_byte_base = cell(para.L, 1);
    for j = 1 : para.L
        cell_byte_base{j} = compressCH(Xbase', para.A{j}, para.B{j});
    end
    
    Dhamm = zeros(batchsize, Nbase, 'uint16');

elseif (strcmp(type, 'sh'))
    [B_base] = hash_code_SH(Xbase', para);
    is_sh = true;
    byte_base = compactbit(B_base);
elseif (strcmp(type, 'bre'))
    is_bre = true;
    W = para.W;
    hash_inds = para.hash_inds;
    %     Ktest = Xbase' * para.Xtraining; % it should be Kbase, but we consider the memory
    B_base = false(Nbase, m);
    for b = 1 : m
        Ktest = Xbase' * para.Xtraining(:, hash_inds(:,b));
        %         B_base(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
        B_base(:,b) = Ktest * W(:,b) > 0;
    end
    byte_base = compactbit(B_base');
    
    %     Ktest = zeros(batchsize, size(para.Xtraining, 2));
    trans_B_test = false(batchsize, m);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');
elseif (strcmp(type, 'ksh'))
    is_ksh = true;
    KBase = sqdist(Xbase, para.anchor);
    KBase = exp(-KBase/(2*para.sigma));
    KBase = bsxfun(@minus, KBase, para.mvec);
    A1 = para.A1;
    tY = A1'*KBase' > 0;
    
    byte_base = compactbit(tY);
    
    %     Ktest = zeros(batchsize, size(para.Xtraining, 2));
    trans_B_test = false(batchsize, m);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');
elseif strcmp(type, 'agh')
    is_agh = true;
    if strcmp(para.type, 'one')
        tY = OneLayerAGH_Test(Xbase', para.anchor', para.W, para.s, para.sigma);
    elseif strcmp(para.type, 'two')
        tY = TwoLayerAGH_Test(Xbase', para.anchor', para.W, para.thres, para.s, para.sigma);
    end
    byte_base = compactbit(tY');
    trans_B_test = false(batchsize, m);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');
end

if (metric_info.type ~= 0 && (is_ch || is_hash_lookup))
    error('NO supporting ch withe other metric type');
end

for i = 1:nbatch
    b_start = 1 + (i - 1) * batchsize;
    if (b_start > Ntest)
        break;
    end
    b_end = i*batchsize;
    b_end = min(b_end, Ntest);
    
    if (is_ch)
        if ((b_end - b_start + 1) ~= batchsize)
            Dhamm = zeros(b_end - b_start + 1, Nbase, 'uint16');
        end
        Dhamm(:) = Dhamm(:)+inf;
        for j = 1 : para.L
            byte_test = compressCH(Xtest(:, b_start:b_end)', para.A{j}, para.B{j});
            D_code = jf_hammingDist3(byte_test, cell_byte_base{j});
            Dhamm = min(D_code, Dhamm);
        end
    else
        if (is_linear)
            % no need to delete the previous space
            B_test= W' * [Xtest(:, [b_start : b_end]); ones(1,b_end - b_start + 1)] > 0;
        elseif (is_sh)
            B_test = hash_code_SH(Xtest(:, b_start : b_end)', para);
        elseif (is_bre)
            if ((b_end - b_start + 1) ~= batchsize)
                trans_B_test = false(b_end - b_start + 1, m);
            end
            Ktest = Xtest(:, [b_start : b_end])' * para.Xtraining;
            for b = 1 : m
                trans_B_test(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
            end
            B_test = trans_B_test';
        elseif is_ksh
            Ktest = sqdist(Xtest(:, [b_start : b_end]), para.anchor);
            Ktest = exp(-Ktest/(2*para.sigma));
            Ktest = bsxfun(@minus, Ktest, para.mvec);
            B_test = A1'*Ktest' > 0;
        elseif is_agh
            if strcmp(para.type, 'one')
                tY = OneLayerAGH_Test(Xtest(:, [b_start : b_end])', para.anchor', para.W, para.s, para.sigma);
            elseif strcmp(para.type, 'two')
                tY = TwoLayerAGH_Test(Xtest(:, [b_start : b_end])', para.anchor', para.W, para.thres, para.s, para.sigma);
            end
            B_test = tY';
        end
        
        if (metric_info.type == 0)
            byte_test = compactbit(B_test);
            Dhamm = jf_hammingDist3(byte_test, byte_base);
        else
            Dhamm = calc_metric_dist(B_test, B_base', metric_info);
        end
    end
    
    [~, Rank] = sort(Dhamm, 2, 'ascend');
    Rank = Rank(:, 1 : 10^4);
    
    for idx_test = b_start : b_end
        idx_row = idx_test - b_start + 1;
        idx_retrieved = Rank(idx_row, :);
        ann_dist = sqdist(Xtest(:, idx_test), Xbase(:, idx_retrieved));
        true_dist = TrueDistTestBase(1 : 10^4, idx_test)';
        ratio = ann_dist ./ true_dist;
        ratio(true_dist < 0.001) = 1;
        ratio = sqrt(ratio);
        ratio = cumsum(ratio);
        ratio = ratio ./ (1 : 10^4);
        mean_ratio = mean_ratio + ratio;
    end
        
    fprintf('batch %d/%d\n', i, nbatch);
end

mean_ratio = mean_ratio / size(Xtest, 2);

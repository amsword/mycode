function str = struct2str(s)

all_fields = fieldnames(s);
num_fields = numel(all_fields);

if num_fields == 0
    str = '_';
    return;
end

str = [];
for i = 1 : num_fields
    if i ~= 1
        str = [str '_'];
    end
    str = [str all_fields(i) '_'];
    
    field_content = s.(all_fields{i});
    
    if isnumeric(field_content)
        str = [str num2str_c(field_content, '_')];
    elseif isstr(field_content)
        str = [str field_content];
    else
        error('what it is, no supporting now');
    end
end

end
function str = num2str_c(num, c)
str = [];
for i = 1 : numel(num)
    if i ~= 1
        str = [str c];
    else
        str = [str num2str(num(i))];
    end
end
end
function all_confs = get_confs(conf)

all_fields = fieldnames(conf);

num_field = numel(all_fields);

num_confs = zeros(num_field + 1, 1);
num_confs(1) = 1;
for i = 1 : num_field
    field_name = all_fields{i};
    field_content = conf.(field_name);
    num_confs(i + 1) = numel(field_content);
end

cum_num_confs = cumprod(num_confs);
all_confs = cell(cum_num_confs(end), 1);

num_replicate = 1;
for i = num_field : -1 : 1
    field_name = all_fields{i};
    field_content = conf.(field_name);
    
    offset = 1;
    for j = 1 : cum_num_confs(i)
        for k = 1 : num_confs(i + 1)
            for m = 1 : num_replicate
                if iscell(field_content)
                    all_confs{offset}.(field_name) = field_content{k};
                else
                    all_confs{offset}.(field_name) = field_content(k);
                end
                offset = offset + 1;
            end
        end
    end
    num_replicate = num_replicate * num_confs(i);
end


for i = 1 : numel(all_confs)
    all_confs{i}
end
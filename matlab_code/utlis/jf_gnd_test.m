function jf_gnd_test(test_file, training_file, save_gnd_seed, type)
load(test_file, 'OriginXtest');
% load(base_file, 'OriginXbase');
load(training_file, 'OriginXtraining');

Ntest = size(OriginXtest, 2);
Nbase = size(OriginXtraining, 2);

max_size = 0.1 * 1024^3 / 8;
batchsize = min(floor(max_size / Nbase), Ntest);
nbath = ceil(Ntest / batchsize);

% subSs = cell(nbath, 1);
% num_nonzeros = 0;
for i = 1 : nbath
    ['start ' num2str(i) ' batch of ' num2str(nbath)]
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, Ntest);
%     num = idx_end - idx_start + 1;
    
    DtestBase = jf_distMat(OriginXtest(:, idx_start : idx_end), ...
        OriginXtraining); % size = [Ntest x Ntraining]
    
    if (strcmp(type, 'knn'))
        [sortedD, idx] = sort(DtestBase, 2);
%         sub_idx = idx(:, [1 : avgNNeighbors]);
%         subS = false(idx_end - idx_start + 1, Nbase);
%         sub_idx = (sub_idx - 1) * num + repmat((1 : num)', 1, avgNNeighbors);
%         subS(sub_idx(:)) = true;
%         [idx_i, idx_j] = find(subS);
        
%         subSs{i}.idx_i = idx_i + idx_start - 1;
%         subSs{i}.idx_j = idx_j;
%         num_nonzeros = num_nonzeros + numel(idx_i);
        
        file_name = jf_gen_gnd_file(save_gnd_seed, idx_start, idx_end);
        idx = uint32(idx);
        save(file_name, 'idx', 'idx_start', 'idx_end', '-v7.3');
    end
end

% idx_i = zeros(num_nonzeros, 1);
% idx_j = zeros(num_nonzeros, 1);
% 
% start = 1;
% for i = 1 : nbath
%     tmp_num = numel(subSs{i}.idx_i);
%     idx_i(start : start + tmp_num - 1) = subSs{i}.idx_i;
%     idx_j(start : start + tmp_num - 1) = subSs{i}.idx_j;
%     start = start + tmp_num;
% end
% StestBase = sparse(idx_i, idx_j, true, Ntest, Nbase);
% 
% jf_save_gnd(StestBase, save_file_sign);
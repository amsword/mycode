function [Nbuckets, bucketACap] = getBuckets(codes, nb)
de = bi2de(double(codes'));
bucketSet = unique(de);
Nbuckets = numel(bucketSet);
bucketCap = hist(de, bucketSet);
bucketACap = mean(bucketCap);
end
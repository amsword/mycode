function y = sigmf(x, para)

y = 1.0 ./ (1 + exp(-para(1) * (x - para(2))));
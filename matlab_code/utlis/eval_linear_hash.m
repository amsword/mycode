function [p1 r1 rpairs] = eval_linear_hash(W, data)


Ntest = data.Ntest;
Xtest = data.Xtest;
StestTraining = data.StestTraining;
B2 = W * [Xtest; ones(1,Ntest)] > 0;

if isfield(data, 'Xbase') ~= 1
    Ntraining = data.Ntraining;
    Xtraining = data.Xtraining;
    B1 = W * [Xtraining; ones(1,Ntraining)] > 0;
else
    Nbase = data.Nbase;
    Xbase = data.Xbase;
    B1 = W * [Xbase; ones(1, Nbase)] > 0;
end
B1 = compactbit(B1);
B2 = compactbit(B2);
% Dhamm = hammingDist(B2, B1);
% Dhamm = jf_hammingDist2(B2, B1);
Dhamm = jf_hammingDist3(B2, B1);

% Dhamm = hammingDist2(B2, B1);		% a faster version of hammingDist
% [p1 r1 rpairs] = evaluation2(StestTraining, Dhamm, size(W,1));
result = jf_eval_hash_lookup_fast(StestTraining, Dhamm, size(W,1));
p1 = result.pre;
r1 = result.rec;
% p1 = p1';
% rpairs = full(rpairs');
% p1 = full(p1);
% r1 = full(r1);

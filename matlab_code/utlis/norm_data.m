whole = cat(2, base, learn, query);

gist_mean = mean(whole, 2);
whole = bsxfun(@minus, whole, gist_mean);

base = bsxfun(@minus, base, gist_mean);
normX = sqrt(sum(base.^2, 1));
base = bsxfun(@rdivide, base, normX);

learn = bsxfun(@minus, learn, gist_mean);
normX = sqrt(sum(learn.^2, 1));
learn = bsxfun(@rdivide, learn, normX);

query = bsxfun(@minus, query, gist_mean);
normX = sqrt(sum(query.^2, 1));
query = bsxfun(@rdivide, query, normX);
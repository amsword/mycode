function result = jf_eval_small2(fS, rank)
% hamming ranking
% result.p_code
% result.p_code_sum
% result.r_code
% result.r_code_sum

[nimagestest, sizedatabase] = size(rank);

jj = false(size(rank));

for n = 1:nimagestest
  jj(n,:) = ismember(rank(n,:), find(fS(n,:)));
end

cum = cumsum(jj,2);

idx = find(cum(:, end) == 0, 1);
if ~isempty(idx)
    idx
end

P = bsxfun(@rdivide, cum, cum(:, end));

result.r_code = mean(P,1)';
result.r_code_sum = sum(P, 1)';

P = bsxfun(@rdivide, cum, [1:sizedatabase]);
result.p_code = mean(P,1)';
result.p_code_sum = sum(P, 1)';
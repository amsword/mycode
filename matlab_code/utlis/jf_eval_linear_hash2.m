function eval = jf_eval_linear_hash2(W, Xbase, Xtest, StestBase, topK, DtestBase)
%% W: codeLength * Dimension. 
%% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
%% Xbase(:, i) is the neighbor of Xtest(:, i);
%% topK and DtestBase are used to do hamming ranking

m = size(W, 2);

Nbase = size(Xbase, 2);
Ntest = size(Xtest, 2);

batchsize = Ntest; % if the memory is an issue, test batch by batch.
nbatch = Ntest / batchsize;
% nbatch = 1;

is_hamming_ranking = true;
if (~exist('topK', 'var'))
    is_hamming_ranking = false;
elseif (~exist('DtestBase', 'var'))
    DtestBase = ~StestBase;
end
    

% hanmming ranking
P_CODE = zeros(Nbase, 1);
R_CODE = zeros(size(P_CODE));

% hamming lookup
total_good_pairs = 0;
total_retrieved_good_pairs = zeros(m + 1, 1);
total_retrieved_pairs = total_retrieved_good_pairs;

B_base = W' * [Xbase; ones(1,Nbase)] > 0;
B_base = compactbit(B_base);

for i = 1:nbatch
    b_start = 1+(i-1)*batchsize;
    b_end = i*batchsize;
    
    B_test = W' * [Xtest(:, [b_start : b_end]); ones(1,Ntest)] > 0;
    B_test = compactbit(B_test);
    
    Dhamm = hammingDist(B_test, B_base);
    
	% hamming lookup
    result = jf_eval_hash_lookup(StestBase(b_start:b_end, :), Dhamm, m);
	total_good_pairs = result.total_good_pairs + total_good_pairs;
	total_retrieved_good_pairs = ...
		result.total_retrieved_good_pairs + total_retrieved_good_pairs;
	total_retrieved_pairs = ... 
		total_retrieved_pairs + result.total_retrieved_pairs;
	
    % hamming ranking
    if (is_hamming_ranking)
        [foo_truth, trueRank] = sort(DtestBase(b_start:b_end, :), 2, 'ascend');
        [foo_code, Rank] = sort(Dhamm, 2, 'ascend');
        result = jf_eval_small(trueRank, Rank, topK);
        
        P_CODE = P_CODE + result.p_code_sum;
        R_CODE = R_CODE + result.r_code_sum;
    end
    
    fprintf('batch %d\n', i);
end
eval.p_code = P_CODE / Ntest;
eval.r_code = R_CODE / Ntest;

eval.rec = total_retrieved_good_pairs / total_good_pairs;
eval.pre = total_retrieved_good_pairs ./ total_retrieved_pairs;

eval.avg_retrieved_good = total_retrieved_good_pairs / Ntest;
eval.avg_retrieved = total_retrieved_pairs / Ntest;
eval.avg_good = total_good_pairs / Ntest;
function eval = eval_hash_ch(data, param)
%% Field of data: Xtraining, Xtest,

nb = param.nbits;

if isfield(data, 'Xbase')
    [nDim, nBase] = size(data.Xbase);
    Xbase = data.Xbase';
    ndxtrain = 1:nBase;
else
    [nDim, nBase] = size(data.Xtraining);
    Xbase = data.Xtraining';
    ndxtrain = data.ndxtrain;
end
ndxtest = data.ndxtest;

L = size(param.A, 2);
nbase = size(Xbase, 1);
II = 1:nbase;

Xtest = data.Xtest';
evalParam.rho = 2;% rho_max = 2;

batchsize = 200;
nbatch = length(ndxtest) / batchsize;
% nbatch = 1;

P_CODE = zeros(batchsize, length(ndxtrain));
R_CODE = zeros(size(P_CODE));
PRECRHO = zeros(batchsize, 3);
NRHO = zeros(size(PRECRHO));
P1 = zeros(1, nb+1);
R1 = zeros(size(P1));
pairs1 = zeros(size(P1));
AP = 0;

for i = 1:nbatch
    b_start = 1+(i-1)*batchsize;
    b_end = i*batchsize;
    
    D = zeros(batchsize, length(ndxtrain), 'uint8');
    D = D+inf;
    D2 = zeros(batchsize, length(ndxtrain), 'single');
    for j = 1:L
        baseCodes = compressCH(Xbase, param.A{j}, param.B{j});
        testCodes = compressCH(Xtest(b_start:b_end, :), param.A{j}, param.B{j});
        D_code = uint8(hammingDist(testCodes, baseCodes));
        D = min(D_code, D);
        D2 = D2+single(D_code);
    end
    D2 = D2/(max(max(D2))+1);
    D2 = single(D)+D2;
    [foo_code, j_code] = sort(D, 2, 'ascend');

    D_truth = data.DtestTraining;
    [foo_truth, j_truth] = sort(D_truth, 2, 'ascend');
    
    [p1 r1 rpairs] = evaluation2(data.StestTraining(b_start:b_end, :), D, param.nbits);
    p1 = p1';
    r1 = r1';
    P1 = P1+p1;R1 = R1+r1;
    pairs1 = pairs1 + rpairs';
    AP = AP + sum([(p1(1:end-1)+p1(2:end))/2].*[(r1(2:end)-r1(1:end-1))]);


    evalParam.rho = 2; % rho_max = 2;
    P_code = zeros(batchsize, length(ndxtrain));
    R_code = zeros(size(P_code));
    PrecRho = zeros(batchsize, evalParam.rho+1);
    NRho = zeros(size(PrecRho));

    for n = 1:batchsize
        ind = b_start+n-1;
        evalParam.foo_code = foo_code(n, :);
        n_truth = ndxtrain(j_truth(ind, :));
        n_code = ndxtrain(j_code(n, :));
        result = eval_small(n_truth, n_code, evalParam);
        P_code(n, :) = result.p_code;
        R_code(n, :) = result.r_code;
        PrecRho(n, :) = result.prec_at_rho;
        NRho(n, :) = result.n_points_rho;
    end
    P_CODE = P_CODE+P_code;
    R_CODE = R_CODE+R_code;
    PRECRHO = PRECRHO+PrecRho;
    NRHO = NRHO+NRho;
    fprintf('batch %d\n', i);
end
eval.p_code = mean(P_CODE, 1)/nbatch;
eval.r_code = mean(R_CODE, 1)/nbatch;
eval.a_code = mean(PRECRHO, 1)/nbatch;
eval.n_code = mean(NRHO, 1)/nbatch;
eval.p1 = P1/nbatch;
eval.r1 = R1/nbatch;
eval.AP = AP/nbatch;
eval.pairs1 = pairs1;




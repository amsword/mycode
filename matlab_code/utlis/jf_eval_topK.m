function result = jf_eval_topK(trueRank, rank)
% hamming ranking
% result.p_code
% result.p_code_sum
% result.r_code
% result.r_code_sum

[nimagestest, sizedatabase] = size(trueRank);

[trueRank idx_tr] = sort(trueRank, 2);
[rank idx_r] = sort(rank, 2);
I = idx_tr > idx_r;
r1 = zeros(size(trueRank));
idx_tr = bsxfun(@plus, (idx_tr - 1) * nimagestest, (1 : nimagestest)');
r1(idx_tr(I)) = 1;

r2 = zeros(size(trueRank));
idx_r = bsxfun(@plus, (idx_r - 1) * nimagestest, (1 : nimagestest)');
r2(idx_r(~I)) = 1;

r1 = r1 + r2;
r1 = cumsum(r1, 2);
r1 = bsxfun(@rdivide, r1, [1 : sizedatabase]);
result.topK_sum = sum(r1, 1);
result.topK = result.topK_sum / nimagestest;
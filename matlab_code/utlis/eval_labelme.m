function [ap p_code] = eval_labelme(W, data);

if isfield(data, 'Xbase')
    B1 = W * [data.Xbase; ones(1, data.Nbase)] > 0;
else
    B1 = W * [data.Xtraining; ones(1, data.Ntraining)] > 0;
end
B2 = W * [data.Xtest; ones(1, data.Ntest)] > 0;

% ndxtrain = 1:data.Ntraining;
% ndxtest = data.Ntraining+1:data.Ntraining+data.Ntest;
if isfield(data, 'Xbase')
    ndxtrain = 1:data.Nbase;
else
    ndxtrain = data.ndxtrain;
end
% ndxtrain = data.ndxtrain;
ndxtest = data.ndxtest;
% code(:,ndxtrain) = B1;
% code(:,ndxtest)  = B2;
% code = compactbit(code);
% code = uint8(code);

B1 = compactbit(B1);
B1 = uint8(B1);
B2 = compactbit(B2);
B2 = uint8(B2);

% P_code = zeros(numel(ndxtrain), numel(ndxtest));
P_code = zeros(size(B1, 2), size(B2, 2));
% compute Hamming distance values
% D_code = hammingDist(code(:,ndxtest),code(:,ndxtrain));
D_code = hammingDist(B2, B1);
% D_code = hammingDist2(code(:,ndxtest),code(:,ndxtrain));		% a faster version of hammingDist

for n = 1:length(ndxtest)
  
  [foo, j_code] = sort(D_code(n, :), 'ascend'); % I assume that smaller distance means closer
  j_code = ndxtrain(j_code);
  
  % get groundtruth sorting
  D_truth = data.DtestTraining(n, :);
  [foo, j_truth] = sort(D_truth);
  j_truth = ndxtrain(j_truth);
  
  % evaluation
  P_code(:,n) = neighbors_recall(j_truth, j_code);
end

p_code = mean(P_code,2);
ap = 0;
%ap = mean(p_code(1:data.max_care));

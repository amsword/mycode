#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"
#include <vector>
#include <list>
using namespace std;

/*
function [retrieved_per_dist retrieved_good_per_dist]  = jf_eval_hash_lookup(S, Dhamm, maxn)

Input: 
S: full matrix!!!. Element is byte type, S(i, j) indicate whether j is the neighbor of i
*Dhamm: estimated hamming dist
*maxn: maximum code length considered.

Output:
result.pre(n)
*  result.rec();
*  result.total_retrieved_pairs
*result.total_retrieved_good_pairs
*result.total_good_pairs

/* Input Arguments */
#define	S   	prhs[0]  // Uint8 vector of size n x 1
#define DHAMM    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	RETRIEVED_GOOD_SUM	plhs[0]  // Double vector 1 x m binary hamming distance
#define	AP_SUM	plhs[1]  // Double vector 1 x m binary hamming distance
#define MAX_NUM_HAMMING_DIST  257
// p_S and p_Dhamm is c-style. row-first
void ScanResult(int n_test, int n_base,
	bool* p_S, unsigned short* p_Dhamm,
	int maxn, 
	int topk,
	double* p_retrieved_good_sum, 
	double *p_ap_sum)
{
	int i;
	int j;
	unsigned short d;
	int k;
	int count_all[MAX_NUM_HAMMING_DIST];
	vector<int> seq[MAX_NUM_HAMMING_DIST];
	int count_good[MAX_NUM_HAMMING_DIST];
	int pre_count = 0;
	int num_founded = 0;

	for (int i = 0; i <= maxn; i++)
	{
		seq[i].resize(topk);
	}


	//initialize
	memset(p_retrieved_good_sum, 0, sizeof(double) * n_base);

	k = 0;

	for (int i = 0; i < n_test; i++)
	{
		memset(count_all, 0, sizeof(int) * MAX_NUM_HAMMING_DIST);
		memset(count_good, 0, sizeof(int) * MAX_NUM_HAMMING_DIST);
		for (int j = 0; j < n_base; j++)
		{
			int dist = *p_Dhamm;
			if (*p_S)
			{
				seq[dist][count_good[dist]] = count_all[dist];
				count_good[dist]++;
			}

			count_all[dist]++;
			p_Dhamm++;
			p_S++;
		}

		num_founded = 0;
		pre_count = 0;
		for (int i = 0; i <= maxn; i++)
		{
			for (int j = 0; j < count_good[i]; j++)
			{
				num_founded++;
				int idx = pre_count + seq[i][j];
				p_retrieved_good_sum[idx]++;
				*p_ap_sum += num_founded / ((double)idx + 1);
			}
			pre_count += count_all[i];
		}
		if (num_founded != topk)
		{
			mexErrMsgTxt("strainge....."); 
		}
	}

	*p_ap_sum /= (double)topk;
}

//[retrieved_good_sum] = 
//          result.eval_hamming_ranking(S, Hamming_dist, m)

void mexFunction(int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray*prhs[] )

{ 
	int n_test, n_base, maxn, topk;
	double* p_retrieved_good_sum;
	double* p_ap_sum;
	bool* p_S;
	unsigned short* p_Dhamm;

	/* Check for proper number of arguments */    
	if (nrhs != 4) 
	{ 
		mexErrMsgTxt("Four input arguments required."); 
	}

	if (!mxIsLogical(S))
	{
		mexErrMsgTxt("Matrix must be bool");
	}
	if (!mxIsUint16(DHAMM))
	{
		mexErrMsgTxt("Vector must be uInt16");
	}
	/* Get dimensions of inputs */
	n_base = (int) mxGetM(S); 
	n_test = (int) mxGetN(S); 

	if ((int)mxGetM(DHAMM)!= n_base || 
		(int) mxGetN(DHAMM) != n_test)
	{
		mexErrMsgTxt("Dimension mismatch btw. matrix and vector");
	}
	maxn = (double)*mxGetPr(prhs[2]);
	if (maxn > 256)
	{
		mexErrMsgTxt("code length must be less than 256");
	}
	topk = (double)*mxGetPr(prhs[3]);
	// Create output array
	RETRIEVED_GOOD_SUM = mxCreateNumericMatrix(n_base, 1, mxDOUBLE_CLASS, mxREAL);
	p_retrieved_good_sum = (double*) mxGetPr(RETRIEVED_GOOD_SUM);

	AP_SUM = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
	p_ap_sum = (double*) mxGetPr(AP_SUM);

	p_S = (bool*) mxGetPr(S);
	p_Dhamm = (unsigned short*) mxGetPr(DHAMM);

	ScanResult(n_test, n_base,
		p_S, p_Dhamm,
		maxn, 
		topk,
		p_retrieved_good_sum, 
		p_ap_sum);    
}
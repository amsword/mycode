#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"

/*
dist = hammD2 (mat1, mat2)

Input: 
   mat1, mat2: compact uint8 bit vectors. Each binary code is one column.
   size(mat1) = [nwords, ndatapoints1]
   size(mat2) = [nwords, ndatapoints2]

Output:
   dist:	Hamming distances between columns of mat1 and mat2
   size(dist) = [ndatapoints1, ndatapoints2]

/* Input Arguments */
#define	MAT1   	prhs[0]  // Uint8 vector of size n x 1
#define MAT2    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	OUTPUT	plhs[0]  // Double vector 1 x m binary hamming distance

const int lookup [] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};

int match(unsigned char*P, unsigned char*Q, int codelb) {
     int output = 0;
     int i;
    switch(codelb) {
//         case 4:
//             return __popcnt(*(unsigned int*)P ^ *(unsigned int*)Q);
//             break;
//         case 8:
//             return __popcnt64(*(unsigned long long*)P ^ *(unsigned long long*)Q);
//             break;
//         case 16:
//             return    __popcnt64(((unsigned long long*)P)[0] ^ ((unsigned long long*)Q)[0]) \
//                     + __popcnt64(((unsigned long long*)P)[1] ^ ((unsigned long long*)Q)[1]);
//             break;
//         case 32:
//             return    __popcnt64(((unsigned long long*)P)[0] ^ ((unsigned long long*)Q)[0]) \
//                     + __popcnt64(((unsigned long long*)P)[1] ^ ((unsigned long long*)Q)[1]) \
//                     + __popcnt64(((unsigned long long*)P)[2] ^ ((unsigned long long*)Q)[2]) \
//                     + __popcnt64(((unsigned long long*)P)[3] ^ ((unsigned long long*)Q)[3]);
//             break;
        default:
            for (i=0; i<codelb; i++)
                output+= lookup[P[i] ^ Q[i]];
            return output;
            break;
    }
}

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
    int nWords, nVecs1, nVecs2, nWords_Vecs,  i, j;
    unsigned int dist;
    unsigned short* outputp;
    unsigned char tmp, *pMat2, *pMat1, *pV;
    
    /* Check for proper number of arguments */    
    if (nrhs != 2) { 
	mexErrMsgTxt("Two input arguments required."); 
    }
    
    if (!mxIsUint8(MAT2))
	mexErrMsgTxt("Matrix must be uInt8");
    
    if (!mxIsUint8(MAT1))
	mexErrMsgTxt("Vector must be uInt8");
    
    /* Get dimensions of inputs */
    nWords = (int) mxGetM(MAT2); 
    nVecs2 = (int) mxGetN(MAT2); 
    nWords_Vecs = (int)  mxGetM(MAT1); 
    nVecs1 = (int) mxGetN(MAT1);
    
    if (nWords!=nWords_Vecs)
	mexErrMsgTxt("Dimension mismatch btw. matrix and vector");
    
    // Create output array
    OUTPUT = mxCreateNumericMatrix(nVecs1, nVecs2, mxUINT16_CLASS, mxREAL);
    outputp = (unsigned short*) mxGetPr(OUTPUT);
    
    pMat1 = (unsigned char*) mxGetPr(MAT1);
    pMat2 = (unsigned char*) mxGetPr(MAT2);
    
    for (j=0; j<nVecs2; j++)
	for (i=0; i<nVecs1; i++)
	    *(outputp++) = match(&pMat1[i*nWords], &pMat2[j*nWords], nWords);
    
    return;    
}




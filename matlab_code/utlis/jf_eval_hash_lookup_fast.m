function result = jf_eval_hash_lookup_fast(S, Dhamm, maxn)
% result.pre
% result.rec
% result.total_good_pairs
% result.total_retrieved_good_pairs
% result.total_retrieved_pairs

% Input:
%    S = true neighbors [Ntest * Ndataset], can be a full matrix NxN
%    Dhamm = estimated distances
%    maxn = number of distinct distance values to be considered
%
% Output:
%
%               exp. # of good pairs inside hamming ball of radius <= (n-1)
%  score(n) = --------------------------------------------------------------
%               exp. # of total pairs inside hamming ball of radius <= (n-1)
%
%               exp. # of good pairs inside hamming ball of radius <= (n-1)
%  recall(n) = --------------------------------------------------------------
%                          exp. # of total good pairs 

result.total_good_pairs = sum(S(:)==1);

[result.total_retrieved_pairs result.total_retrieved_good_pairs] = ...
    jf_eval_hash_lookup2(full(S), uint16(Dhamm), maxn);

result.total_retrieved_pairs = cumsum(result.total_retrieved_pairs);
result.total_retrieved_good_pairs = cumsum(result.total_retrieved_good_pairs);
result.pre =  result.total_retrieved_good_pairs ./ result.total_retrieved_pairs;
result.rec= result.total_retrieved_good_pairs / result.total_good_pairs;
   
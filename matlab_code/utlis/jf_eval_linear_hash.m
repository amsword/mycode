function [p1 r1 rpairs] = jf_eval_linear_hash(W, Xbase, Xtest, StestTraining)
%% W: codeLength * Dimension. 
%% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
%% Xbase(:, i) is the neighbor of Xtest(:, i);

Ntest = size(Xtest, 2);
B_test = W' * [Xtest; ones(1,Ntest)] > 0;
Ntraining = size(Xbase, 2);
B_base = W' * [Xbase; ones(1,Ntraining)] > 0;

B_base = compactbit(B_base);
B_test = compactbit(B_test);
Dhamm = hammingDist(B_test, B_base);
% Dhamm = hammingDist2(B_test, B_base);		% a faster version of hammingDist
[p1 r1 rpairs] = evaluation2(StestTraining, Dhamm, size(W,2));
p1 = p1';
r1 = r1';
rpairs = full(rpairs') / size(StestTraining, 1);
p1 = full(p1);
r1 = full(r1);
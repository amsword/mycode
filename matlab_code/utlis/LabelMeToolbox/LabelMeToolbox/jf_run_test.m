% Parameters:
clear;
param.imageSize = 32;
param.orientationsPerScale = [8 8 8];
param.numberBlocks = 4;
param.fc_prefilt = 4;

cifar_gist = zeros(50000, 384);
k = 1;
for idx_batch = 1 : 5
    str = ['data_batch_' num2str(idx_batch)];
    x = load(str);
    for i = 1 : 10000
        img = reshape(x.data(i, :), 32, 32, 3);
        
        for j = 1 : 3
            img(:, :, j) = img(:, :, j)';
        end
        
        [gist, param] = LMgist(img, '', param);
        cifar_gist(k, :) = gist;
        k = k + 1;
    end
end

save('training', 'cifar_gist');

%
gist_test = zeros(10000, 384);
k = 1;

str = ['test_batch.mat'];
x = load(str);
for i = 1 : 10000
    img = reshape(x.data(i, :), 32, 32, 3);
    
    for j = 1 : 3
        img(:, :, j) = img(:, :, j)';
    end
    
    [gist, param] = LMgist(img, '', param);
    gist_test(k, :) = gist;
    k = k + 1;
end
save('test', 'gist_test');
%%
mean_value = mean(cifar_gist, 1);
cifar_gist = bsxfun(@minus, cifar_gist, mean_value);
gist_test = bsxfun(@minus, gist_test, mean_value);
%%
Xtraining = cifar_gist';
Xtest = gist_test';
save('training', 'Xtraining');
save('test', 'Xtest');
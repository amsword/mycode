function result = eval_hamming_ranking3(fS, Dhamm, m, topk)

[good result.ap_sum] = eval_hamming_ranking20(fS', Dhamm', m, topk);

good = cumsum(good, 1);

result.r_code_sum = (good / topk);
result.p_code_sum = (good ./ (1 : numel(good))');
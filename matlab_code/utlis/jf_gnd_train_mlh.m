function [mlh_I] = jf_gnd_train_mlh(src, topK, mlh_dst)
%% I: for cbh
%   mlh_I, for mlh
OriginXtraining = jf_load_origin_training(src);

N = size(OriginXtraining, 2);

max_size = 0.1 * 1024^3 / 8;
batchsize = min(floor(max_size / N), N);
nbath = ceil(N / batchsize);

mlh_non_zeros = 0;

mlh_idx_i = cell(nbath, 1);
mlh_idx_j = cell(nbath, 1);

for i = 1 : nbath
    ['i: ' num2str(i) ' total: ' num2str(nbath)]
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, N);
    dist = jf_distMat(OriginXtraining, [], [idx_start, idx_end]);
    [s_dist] = sort(dist, 2);
	
    mlh_thres = s_dist(:, topK);
    curr_I = bsxfun(@le, dist, mlh_thres);
    [idx_i, idx_j] = find(curr_I);
    mlh_idx_i{i} = idx_i + idx_start - 1;
    mlh_idx_j{i} = idx_j;
    mlh_non_zeros = mlh_non_zeros + numel(idx_j);
end

idx_i = zeros(mlh_non_zeros, 1);
idx_j = zeros(mlh_non_zeros, 1);
start = 1;
for i = 1 : nbath
    tmp_num = numel(mlh_idx_i{i});
    idx_i(start : start + tmp_num - 1) = mlh_idx_i{i};
    idx_j(start : start + tmp_num - 1) = mlh_idx_j{i};
    start = start + tmp_num;
end
mlh_I = sparse(idx_i, idx_j, true, N, N);
jf_save_gnd(mlh_I, mlh_dst);
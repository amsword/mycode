function [bucketCap entropy bin_idx] = jf_how_average(W, Xtraining)
N = size(Xtraining, 2);

codes = W' * [Xtraining; ones(1,N)] > 0;

de = bi2de(double(codes'));
bucketSet = unique(de);
bucketSet = sort(bucketSet);

% bucketCap1 = hist(de, bucketSet);
[bucketCap bin_idx] = histc(de, bucketSet);

% if (sum(abs(bucketCap(:) - bucketCap1(:))) > 0)
%     error('errror');
% end

p = bucketCap / sum(bucketCap);
entropy = sum(-p .* log(p));
function codes = compressCodes(X, W)
% get the code for test and train data
% option is 'sh' use spectral hashing, 'w_based' use W
% output - uint8 codes

[Ndim, Ndata] = size(X);
codes = ((W * [X; ones(1, Ndata)])>0);
codes = compactbit(codes); codes = uint8(codes);


function eval = eval_hash6(m, type, para, B_test, B_base, StestBase, ...
    topK,  trueRank_fileseed, irrelevance, metric_info, eval_types)
% m: code length
% type: linear, para is W
% Xbase and Xtest: the datapoint, every column is a point. StestTraining(i, j): the indicator whether
% Xbase(:, i) is the neighbor of Xtest(:, i);
% topK and DtestBase are used to do hamming ranking
% NOTE: no need to load true ranking for hamming ranking

is_hamming_ranking = eval_types.is_hamming_ranking;
is_topK = eval_types.is_topk;
is_hash_lookup = eval_types.is_hash_lookup;
is_ndcg = eval_types.is_ndcg;
is_success_rate = eval_types.is_success_rate;

Nbase = size(B_base, 2);
Ntest = size(B_test, 2);

batchsize = 0.1 * 1024^3 / 8 / Nbase; % if the memory is an issue, test batch by batch.
batchsize = floor(batchsize);
nbatch = ceil(Ntest / batchsize);

if (is_hamming_ranking)
    % hanmming ranking
    P_CODE = zeros(Nbase, 1);
    R_CODE = zeros(size(P_CODE));
end

if (is_hash_lookup)
    % hamming lookup
    total_good_pairs = 0;
    total_retrieved_good_pairs = zeros(m + 1, 1);
    total_retrieved_pairs = total_retrieved_good_pairs;
end

if (is_topK)
    %% topK ranking
    total_precision_topK = zeros(Nbase, 1);
end

if (is_ndcg)
    ndcg_m = max(max(irrelevance));
    relevance = double(ndcg_m) + 1 - double(irrelevance);
    clear irrelevance;
    
    total_dcg_1 = zeros(1, Nbase);
    total_dcg_2 = total_dcg_1;
end

if (is_success_rate)
    num_success = 0;
end

is_linear = false;
is_ch = false;
is_sh = false;
is_bre = false;

nwords = ceil(m / 8);
if (strcmp(type, 'linear'))
    is_linear = true;
   
%     B_base = W' * [B_base; ones(1,Nbase)] > 0;
    byte_base = compactbit(B_base);
    
    %     B_test = false(m, batchsize);
    %     byte_test = zeros(nwords, batchsize, 'uint8');
    Dhamm = zeros(batchsize, Nbase, 'uint16');

end

for i = 1:nbatch
    b_start = 1 + (i - 1) * batchsize;
    if (b_start > Ntest)
        break;
    end
    b_end = i*batchsize;
    b_end = min(b_end, Ntest);
    
    if (is_linear)
        % no need to delete the previous space
        %             B_test= W' * [Xtest(:, [b_start : b_end]); ones(1,b_end - b_start + 1)] > 0;
        subB_test = B_test(:, [b_start : b_end]);
    end
    
    if (metric_info.type == 0)
        byte_test = compactbit(subB_test);
        Dhamm = jf_hammingDist3(byte_test, byte_base);
    else
        Dhamm = calc_metric_dist(subB_test, B_base', metric_info);
    end
    
    if (is_hash_lookup || is_hamming_ranking)
        S = StestBase(b_start:b_end, 1 : topK);
        result.total_good_pairs = numel(S);
        
        num_test = b_end - b_start + 1;
        
        % construct the full index matrix
        one_idx = (S - 1) * (num_test) + ...
            repmat([1 : num_test]', 1, size(S, 2));
        
        fS = false(num_test, Nbase);
        fS(one_idx) = true;
    end
    
    if (is_hash_lookup)
        % hamming lookup
        %result = jf_eval_hash_lookup_fast(StestBase(b_start:b_end, :), Dhamm, m);
        % Note: here StestBase is the index, from 1.
        
        [result.total_retrieved_pairs result.total_retrieved_good_pairs] = ...
            jf_eval_hash_lookup2(fS, uint16(Dhamm), m);
        
        result.total_retrieved_pairs = cumsum(result.total_retrieved_pairs);
        result.total_retrieved_good_pairs = cumsum(result.total_retrieved_good_pairs);
        result.pre =  result.total_retrieved_good_pairs ./ result.total_retrieved_pairs;
        result.rec= result.total_retrieved_good_pairs / result.total_good_pairs;
        
        total_good_pairs = result.total_good_pairs + total_good_pairs;
        total_retrieved_good_pairs = ...
            result.total_retrieved_good_pairs + total_retrieved_good_pairs;
        total_retrieved_pairs = ...
            total_retrieved_pairs + result.total_retrieved_pairs;
    end
    
    if (is_topK)
        % hamming ranking
        file_name = jf_gen_gnd_file(trueRank_fileseed, b_start, b_end);
        load_result = load(file_name, 'idx');
        trueRank = load_result.idx;
        clear load_result;
    end
    
    if (is_hamming_ranking || is_ndcg || is_topK)
        [~, Rank] = sort(Dhamm, 2, 'ascend');
    end
    
    if (is_hamming_ranking)
%    [p_code_sum r_code_sum] = eval_hamming_rankingm(S, Dhamm, m, topK)
        result = jf_eval_small(S, Rank, topK);
%         result = jf_eval_small(trueRank, Rank, topK);
        P_CODE = P_CODE + result.p_code_sum;
        R_CODE = R_CODE + result.r_code_sum;
    end
    
    if (is_topK)
        %% topK ranking
        result = jf_eval_topK(trueRank, Rank);
        
        total_precision_topK = total_precision_topK + ...
            result.topK_sum';
    end
    
    if (is_ndcg)
        result = jf_sum_DCG(Rank, relevance(b_start : b_end, :));
        total_dcg_1 = total_dcg_1 + result.s_dcg1;
        total_dcg_2 = total_dcg_2 + result.s_dcg2;
    end
    
    if (is_success_rate)
        [min_dist] = min(Dhamm, [], 2);
        num_success = num_success + sum(min_dist <= 0.01);
    end
    
    fprintf('batch %d/%d\n', i, nbatch);
end

if (is_hamming_ranking)
    eval.p_code = P_CODE / Ntest;
    eval.r_code = R_CODE / Ntest;
end

if (is_hash_lookup)
    eval.rec = total_retrieved_good_pairs / total_good_pairs;
    eval.pre = total_retrieved_good_pairs ./ total_retrieved_pairs;
    
    eval.avg_retrieved_good = total_retrieved_good_pairs / Ntest;
    eval.avg_retrieved = total_retrieved_pairs / Ntest;
    eval.avg_good = total_good_pairs / Ntest;
end

if (is_topK)
    eval.avg_precision_topK = total_precision_topK / Ntest;
end

if (is_ndcg)
    [~, Rank] = sort(relevance(1, :), 'descend');
    result = jf_sum_DCG(Rank, relevance(1, :));
    eval.ndcg1 = total_dcg_1 / Ntest ./ result.s_dcg1;
    eval.ndcg2 = total_dcg_2 / Ntest ./ result.s_dcg2;
end

if (is_success_rate)
    eval.success_rate = num_success / Ntest;
end
#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"

/*
function [retrieved_per_dist retrieved_good_per_dist]  = jf_eval_hash_lookup(S, Dhamm, maxn)

Input: 
  S: full matrix!!!. Element is byte type, S(i, j) indicate whether j is the neighbor of i
 *Dhamm: estimated hamming dist
 *maxn: maximum code length considered.

Output:
    result.pre(n)
 *  result.rec();
 *  result.total_retrieved_pairs
 *result.total_retrieved_good_pairs
 *result.total_good_pairs

/* Input Arguments */
#define	S   	prhs[0]  // Uint8 vector of size n x 1
#define DHAMM    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	RETRIEVED_PER_DIST	plhs[0]  // Double vector 1 x m binary hamming distance
#define RETRIEVED_GOOD_PER_DIST	plhs[1]

const int lookup [] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};

void ScanResult(int n_test, int n_base,
        bool* p_S, unsigned short* p_Dhamm,
        int maxn, 
        double* p_retrieved_per_dist, 
        double* p_retrieved_good_per_dist)
{
    int i;
    int j;
	unsigned short d;
	int k;
    //initialize
    for (i = 0; i <= maxn; i++)
	{
        p_retrieved_per_dist[i] = 0;
		p_retrieved_good_per_dist[i] = 0;
	}
	
	k = 0;
	for (j = 0; j < n_base; j++)
	{
		for (i = 0; i < n_test; i++)
		{
			d = p_Dhamm[k];
			
			p_retrieved_per_dist[d]++;
			if (p_S[k])
			{
				p_retrieved_good_per_dist[d]++;
			}
			k++;
		}
	}
}

void mexFunction(int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
    int n_test, n_base, maxn;
	double* p_retrieved_per_dist;
	double* p_retrieved_good_per_dist;
	bool* p_S;
	unsigned short* p_Dhamm;
   
    /* Check for proper number of arguments */    
    if (nrhs != 3) 
	{ 
		mexErrMsgTxt("Three input arguments required."); 
    }
    
    if (!mxIsLogical(S))
	mexErrMsgTxt("Matrix must be bool");
    
    if (!mxIsUint16(DHAMM))
	mexErrMsgTxt("Vector must be uInt16");
    
    /* Get dimensions of inputs */
    n_test = (int) mxGetM(S); 
    n_base = (int) mxGetN(S); 
    
    if ((int)mxGetM(DHAMM)!= n_test || (int) mxGetN(DHAMM) != n_base)
	{
		mexErrMsgTxt("Dimension mismatch btw. matrix and vector");
    }
	maxn = (double)*mxGetPr(prhs[2]);
	
    // Create output array
    RETRIEVED_PER_DIST = mxCreateNumericMatrix(maxn + 1, 1, mxDOUBLE_CLASS, mxREAL);
    p_retrieved_per_dist = (double*) mxGetPr(RETRIEVED_PER_DIST);
		
	RETRIEVED_GOOD_PER_DIST = mxCreateNumericMatrix(maxn + 1, 1, mxDOUBLE_CLASS, mxREAL);
    p_retrieved_good_per_dist = (double*) mxGetPr(RETRIEVED_GOOD_PER_DIST);
	    
	p_S = (bool*) mxGetPr(S);
	p_Dhamm = (unsigned short*) mxGetPr(DHAMM);
	
    ScanResult(n_test, n_base,
        p_S, p_Dhamm,
        maxn, 
        p_retrieved_per_dist, 
        p_retrieved_good_per_dist);    
}
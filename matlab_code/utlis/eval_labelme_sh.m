function [ap p_code] = eval_labelme_sh(data, nb);

ndxtest = data.ndxtest;
Xtest = data.Xtest';
SHparam.nbits = nb; % number of bits to code each sample

Xtraining = data.Xtraining';
   % training
SHparam = trainSH(Xtraining, SHparam);

   % compress training and test set
if isfield(data, 'Xbase') == 1
    [B1, U1] = compressSH(data.Xbase', SHparam);
    ndxtrain = 1:data.Nbase;
else
    [B1,U1] = compressSH(Xtraining, SHparam);
    ndxtrain = data.ndxtrain;
end
[B2,U2] = compressSH(Xtest, SHparam);

D_code = hammingDist(B2', B1'); 

for n = 1:length(ndxtest)
  
  [foo, j_code] = sort(D_code(n, :), 'ascend'); % I assume that smaller distance means closer
  j_code = ndxtrain(j_code);
  
  % get groundtruth sorting
  D_truth = data.DtestTraining(n,:);
  [foo, j_truth] = sort(D_truth);
  j_truth = ndxtrain(j_truth);
  
  % evaluation
  P_code(:,n) = neighbors_recall(j_truth, j_code);
end

p_code = mean(P_code,2);
ap = 0;
%ap = mean(p_code(1:data.max_care));

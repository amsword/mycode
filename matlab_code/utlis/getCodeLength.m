function m = getCodeLength(NTraining, Nneighbors, rho)
m = rho;
while(true)
    sum = 0;
    for j = 0:rho
        sum = sum + nchoosek(m, j);
    end
    sum
    if NTraining / 2^m * sum < Nneighbors
        break
    end
    m = m+1;
end
m

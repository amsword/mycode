#include <mex.h>
#include <stdio.h>
#include <math.h>
#include "intrin.h"
#include <vector>
#include <list>
using namespace std;

/*
function [retrieved_per_dist retrieved_good_per_dist]  = jf_eval_hash_lookup(S, Dhamm, maxn)

Input: 
  S: full matrix!!!. Element is byte type, S(i, j) indicate whether j is the neighbor of i
 *Dhamm: estimated hamming dist
 *maxn: maximum code length considered.

Output:
    result.pre(n)
 *  result.rec();
 *  result.total_retrieved_pairs
 *result.total_retrieved_good_pairs
 *result.total_good_pairs

/* Input Arguments */
#define	fS   	prhs[0]  // Uint8 vector of size n x 1
#define RANK    prhs[1]  // Uint8 matrix of size n x m

/* Output Arguments */
#define	RETRIEVED_GOOD_SUM	plhs[0]  // Double vector 1 x m binary hamming distance

const int lookup [] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};

void ScanResult(int n_test, int n_base,
        bool* p_S, unsigned short* p_rank,
        int maxn, 
        double* p_retrieved_good_sum)
{
    int i;
    int j;
	unsigned short d;
	int k;
    const bool* p_S2 = p_S;
    //initialize
   memset(p_retrieved_good_sum, 0, sizeof(double) * n_base);
	
	k = 0;
    
    for (int j = 0; j < n_base; j++)
    {
        for (int i = 0; i < n_test; i++)
        {
            int r = *p_rank - 1;
            if (p_S2[r * n_test + r])
            {
                p_retrieved_good_sum[j]++;
            }
            p_S++;
            p_rank++;
        }
    }
}

//[retrieved_good_sum] = 
//          result.eval_hamming_ranking(S, Hamming_dist, m)

void mexFunction(int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
    int n_test, n_base, maxn;
	double* p_retrieved_good_sum;
	bool* p_S;
	unsigned short* p_rank;
   
    /* Check for proper number of arguments */    
    if (nrhs != 3) 
	{ 
		mexErrMsgTxt("Three input arguments required."); 
    }
    
    if (!mxIsLogical(fS))
	mexErrMsgTxt("Matrix must be bool");
    
    if (!mxIsUint16(RANK))
	mexErrMsgTxt("Vector must be uInt16");
    
    /* Get dimensions of inputs */
    n_test = (int) mxGetM(fS); 
    n_base = (int) mxGetN(fS); 
    
    if ((int)mxGetM(RANK)!= n_test || (int) mxGetN(RANK) != n_base)
	{
		mexErrMsgTxt("Dimension mismatch btw. matrix and vector");
    }
	maxn = (double)*mxGetPr(prhs[2]);
	
    // Create output array
    RETRIEVED_GOOD_SUM = mxCreateNumericMatrix(n_base, 1, mxDOUBLE_CLASS, mxREAL);
    p_retrieved_good_sum = (double*) mxGetPr(RETRIEVED_GOOD_SUM);

	p_S = (bool*) mxGetPr(fS);
	p_rank = (unsigned short*) mxGetPr(RANK);
	
    ScanResult(n_test, n_base,
        p_S, p_rank,
        maxn, 
        p_retrieved_good_sum);    
}
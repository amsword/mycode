function [I mlh_I] = jf_gnd_train(src, avg_num_neighbour, dst, topK, mlh_dst)
%% I: for cbh
%   mlh_I, for mlh
OriginXtraining = jf_load_origin_training(src);

N = size(OriginXtraining, 2);

max_size = 4 * 1024^3 / 8;
batchsize = min(floor(max_size / N), N);
nbath = ceil(N / batchsize);

rho = length(avg_num_neighbour);

I = cell(rho, 1);

num_nonzeros = zeros(1, rho);
mlh_non_zeros = 0;
for i = 1 : rho
    I{i} = sparse([], [], false, N, N, (avg_num_neighbour(i) + 1) * N);
end

subI = cell(nbath, rho);

mlh_idx_i = cell(nbath, 1);
mlh_idx_j = cell(nbath, 1);

for i = 1 : nbath
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, N);
    dist = jf_distMat(OriginXtraining, [], [idx_start, idx_end]);
    [s_dist] = sort(dist, 2);
    
    thresDists = zeros(idx_end - idx_start + 1, rho);
    
    for k = 1:rho
        thresDists(:, k) = s_dist(:, avg_num_neighbour(k)); % the threshold distance for graded neighbors
        mlh_thres = s_dist(:, topK);
    end

    for k = 1:rho
        curr_I = bsxfun(@le, dist, thresDists(:, k));
        [idx_i, idx_j] = find(curr_I);
        subI{i, k}.idx_i = idx_i + idx_start - 1;
        subI{i, k}.idx_j = idx_j;
        num_nonzeros(k) = num_nonzeros(k) + numel(idx_j);
    end
    
    curr_I = bsxfun(@le, dist, mlh_thres);
    [idx_i, idx_j] = find(curr_I);
    mlh_idx_i{i} = idx_i + idx_start - 1;
    mlh_idx_j{i} = idx_j;
    mlh_non_zeros = mlh_non_zeros + numel(idx_j);
end

for k = 1 : rho
    idx_i = zeros(num_nonzeros(k), 1);
    idx_j = zeros(num_nonzeros(k), 1);
    
    start = 1;
    for i = 1 : nbath
        tmp_num = numel(subI{i, k}.idx_i);
        idx_i(start : start + tmp_num - 1) = subI{i, k}.idx_i;
        idx_j(start : start + tmp_num - 1) = subI{i, k}.idx_j;
        start = start + tmp_num;
    end
    I{k} = sparse(idx_i, idx_j, true, N, N);
end

idx_i = zeros(mlh_non_zeros, 1);
idx_j = zeros(mlh_non_zeros, 1);
start = 1;
for i = 1 : nbath
    tmp_num = numel(mlh_idx_i{i});
    idx_i(start : start + tmp_num - 1) = mlh_idx_i{i};
    idx_j(start : start + tmp_num - 1) = mlh_idx_j{i};
    start = start + tmp_num;
end
mlh_I = sparse(idx_i, idx_j, true, N, N);
jf_save_gnd(mlh_I, mlh_dst);
jf_save_gnd(I, dst);
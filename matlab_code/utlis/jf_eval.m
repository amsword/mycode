function result = jf_eval(StestBase, Dhamm, nb, topK, DtestBase)
%% including  hamming ranking and hash lookup

% hamming lookup
result.lookup = jf_eval_hash_lookup(StestBase, Dhamm, param.nbits);

% hamming ranking
[foo_code, Rank] = sort(Dhamm, 2, 'ascend');
if (~exist('DtestBase', 'var'))
	DtestBase = ~StestBase;
end

[foo_truth, trueRank] = sort(DtestBase, 2, 'ascend');

result.ranking = jf_eval_small(trueRank, Rank, topK);
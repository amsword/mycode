function h = lin2nonlinH(x, prob)
h = [];
if ~isempty(prob.lin2nonlin.h)
     h = prob.lin2nonlin.h(x, prob);
end
if ~isempty(prob.lin2nonlin.beq)
    h(end+1:end+length(prob.lin2nonlin.beq)) = prob.lin2nonlin.Aeq * x - prob.lin2nonlin.beq;
end
h = h(:);

function dc = lin2nonlinDC(x, prob, c)
if nargin<3; c = prob.c(x, prob); end

%todo: enhance me!
dc = prob.lin2nonlin.dc(x, prob, c);
if size(dc,2) < length(c)%analitical dc
    A = prob.lin2nonlin.A;
    b = prob.lin2nonlin.b;
    s = A*x-b;
    A(s<=0,:) = 0;
    %dc(:,end+1:end+length(prob.lin2nonlin.b)) = A.';    
    switch prob.env
        case 'matlab'
            dc(:,end+1:end+length(prob.lin2nonlin.b)) = A.';
        case 'octave'
            dc = sparse([dc A.']);
        otherwise
            prob.err('unknown environment')
    end
end

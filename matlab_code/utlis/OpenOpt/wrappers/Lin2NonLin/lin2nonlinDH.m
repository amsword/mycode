function dh = lin2nonlinDH(x, prob, h)

if nargin<3
    h = prob.h(x, prob);
end

%todo: enhance me!
% dh = prob.lin2nonlin.dh(x, prob, h);
dh = prob.lin2nonlin.dh(x, prob);
if size(dh,2) < length(h)%analitical dh
    Aeq = prob.lin2nonlin.Aeq;
%     beq = prob.lin2nonlin.beq;
%     s = Aeq * x - beq;
%     ind = find(s<0);
%     Aeq(ind,:) = -Aeq(ind,:);
    switch prob.env
        case 'matlab'
            dh(:,end+1:end+length(prob.lin2nonlin.beq)) = Aeq.';
        case 'octave'
            dh = sparse([dh Aeq.']);
        otherwise
            prob.err('unknown environment')
    end
end

function c = lin2nonlinC(x, prob)
c = [];
if ~isempty(prob.lin2nonlin.c)
    c = prob.lin2nonlin.c(x, prob);
end
if ~isempty(prob.lin2nonlin.b)
    try
        J = size(c,1);
        for j = 1:size(x,2)
            c(J+1:J+length(prob.lin2nonlin.b),j) = max(prob.lin2nonlin.A * x(:, j) - prob.lin2nonlin.b, 0);
        end
    catch
        keyboard
    end
end
c = c(:);

function prob = lin2nonlin(prob)

if isequal(prob.env, 'octave')% fixing temporary Octave bug - sparse matricies resizing
    A = full(prob.A);
    Aeq = full(prob.Aeq);
else
    A = sparse(prob.A);
    Aeq = sparse(prob.Aeq);
end


b = prob.b;
beq = prob.beq;

if isempty(prob.lb); prob.lb = -inf*ones(prob.n,1); end
if isempty(prob.ub); prob.ub = inf*ones(prob.n,1); end
indLB = find(~isinf(prob.lb) & prob.ub ~= prob.lb);
indUB = find(~isinf(prob.ub) & prob.ub ~= prob.lb);
indEQ = find(prob.lb==prob.ub);
%resizing (+reallocating)
initLenB = length(b);
initLenBeq = length(beq);

if length(indLB) || length(indUB)
    A(end+length(indLB)+length(indUB), prob.n) = 0;
    b(end+length(indLB)+length(indUB), 1) = 0;
end

if length(indEQ)
    Aeq(end+length(indEQ), prob.n) = 0;
    beq(end+length(indEQ), 1) = 0;
end


for i = 1:length(indLB)
    A(initLenB+i, indLB(i)) = -1;
    b(initLenB+i) = -prob.lb(indLB(i));
end

for i = 1:length(indUB)
    A(initLenB+length(indLB)+i, indUB(i)) = 1;
    b(initLenB+length(indLB)+i) = prob.ub(indUB(i));
end


for i = 1:length(indEQ)
    Aeq(initLenBeq+i, indEQ(i)) = 1;
    beq(initLenBeq+i) = prob.lb(indEQ(i));% = prob.ub(indEQ(i)), because they are the same
end

prob.lb = -inf*ones(size(prob.lb));
prob.ub = inf*ones(size(prob.ub));

%%%%%%%% inequalities %%%%%%%%

prob.lin2nonlin.A = sparse(A);
prob.lin2nonlin.b = sparse(b);

if ~isempty(A)
    %todo: modify prob.mu?
    prob.nc = prob.nc + length(b);
    prob.A = [];
    prob.b = [];
end
if isempty(prob.c)
    prob.lin2nonlin.c = [];
else
    prob.lin2nonlin.c = prob.c;
end

if (isempty(prob.c) || isempty(prob.dc)) && isempty(b)
    prob.lin2nonlin.dc = [];
else
    prob.lin2nonlin.dc = prob.dc;
    prob.dc = @lin2nonlinDC;
end
if isempty(prob.c) && ~isempty(b); prob.lin2nonlin.dc = @(varargin) A.'; end

if ~isempty(prob.c) || ~isempty(A)
    prob.c = @lin2nonlinC;
else prob.c = [];
end
%disp(3)
%%%%%%%% equalities %%%%%%%%
prob.lin2nonlin.Aeq = sparse(Aeq);
prob.lin2nonlin.beq = sparse(beq);

if ~isempty(Aeq)
    %todo: modify prob.lambda?
    prob.nh = prob.nh + length(beq);
    prob.Aeq = [];
    prob.beq = [];
end

if (isempty(prob.h) || isempty(prob.dh))  && isempty(beq)
    prob.lin2nonlin.dh = [];
else
    prob.lin2nonlin.dh = prob.dh;
    prob.dh = @lin2nonlinDH;
end

if isempty(prob.h)
    prob.lin2nonlin.h = [];
else
    prob.lin2nonlin.h = prob.h;
end

if isempty(prob.h) && ~isempty(beq); prob.lin2nonlin.dh = @(varargin) Aeq.'; end
%disp(4)
if ~isempty(prob.h) || ~isempty(Aeq)
    prob.h = @lin2nonlinH;
else prob.h = [];
end



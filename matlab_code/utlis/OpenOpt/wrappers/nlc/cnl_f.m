function r = cnl_f(x, prob)
F = prob.cnl.f(x, prob);
r = F;
if ~isempty(prob.c)
    C = prob.c(x, prob);
    switch prob.advanced.cPenalty 
        case 'linear'
            r = r+sum(C);
        case 'norm2'
            r = r+sqrt(sum(C.^2)/length(C));%MU comes from cnl_h
        otherwise
            prob.err('isn''t implemented yet')
    end            
end
if ~isempty(prob.h)
    H = prob.h(x, prob);
    switch prob.advanced.cPenalty 
        case 'linear'
            r = r+sum(H);
        case 'norm2'
            r = r+sqrt(sum(H.^2)/length(H));%LAMBDA comes from cnl_h
        otherwise
            prob.err('isn''t implemented yet')
    end            
end
% if prob.advanced.debug
%     C = prob.primal.c(x, prob);
%     disp([F sum(C)])
% end

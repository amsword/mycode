function c = cnl_c(x, prob)

c_primal = prob.cnl.c(x,prob);
c_primal(c_primal<0) = 0;%todo: check me!
if all(c_primal <= 0); c = zeros(size(c_primal)); return; else
    MU = prob.mu;
    C = c_primal;
    switch prob.advanced.cPenalty 
        case 'linear'
            c = MU .* C;
        case 'norm2'
            c = MU .* C;
%             c = MU .* norm(C, 2);
%         case 'muNormLogMu'
%             under construction
%             Dist = norm(x - prob.xInactive{1})+norm(x - prob.xInactive{2});
%             c = MU .* Dist .* log(1 + MU .* C);
        otherwise
            prob.err('isn''t implemented yet')
    end
end

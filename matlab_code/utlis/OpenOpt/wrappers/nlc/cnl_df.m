function dF = cnl_df(x, prob, varargin)
dF = prob.cnl.df(x, prob, varargin{:});
% disp(norm(x))
if isfield(prob.cnl, 'dc') && ~isempty(prob.cnl.dc)
    dC = prob.dc(x, prob);
    switch prob.advanced.cPenalty
        case 'linear'
            dF = dF + sum(dC,2);
        case 'norm2'
%             C = prob.c(x, prob);
            dF = dF + sum(dC,2);%MU comes from cnl_c
        otherwise
            prob.err('isn''t implemented yet')
    end   
    
end

if isfield(prob.cnl, 'dh') && ~isempty(prob.cnl.dh)
    dH = prob.dh(x, prob);
    dF = dF + sum(dH,2);
end

% ind_down = find(x < prob.cnl.lb & -dF < 0);
% ind_up = find(x > prob.cnl.ub & -dF > 0);
% % disp(length(ind_down)+length(ind_up))
% dF(ind_down) = -0.2*dF(ind_down);
% dF(ind_up) = -0.2*dF(ind_up);


% disp(x')
% if prob.advanced.debug
%     C = prob.primal.c(x, prob);
%     disp([F C H])
% end

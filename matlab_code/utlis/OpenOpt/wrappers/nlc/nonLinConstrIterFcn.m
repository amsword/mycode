function r = nonLinConstrIterFcn(prob, r)
global globstat
nCheck = 10;
buferLength = 5;
ExtractRoutineParamsFromProb;

x = globstat.xPrevF(:, 1);
violations = prob.advanced.getViolations(x, prob);
[MV ind]= max(violations);
if ~isfield(globstat.tmp, 'maxViolation')
    globstat.tmp.maxViolation = MV;
elseif length(globstat.tmp.maxViolation) < buferLength
    globstat.tmp.maxViolation(end+1) = MV;
else
    globstat.tmp.maxViolation = [globstat.tmp.maxViolation(2:end) MV];
end


if ~isfield(globstat.tmp, 'maxViolation')
    globstat.tmp.maxViolation = MV;
else
    if (globstat.nIter==1 || ~mod(globstat.nIter, nCheck))...
            && MV>prob.TolCon
        vec = [prob.mu; prob.lambda];
        if ~vec(ind) || all(MV > globstat.tmp.maxViolation(1:end-1))
            r.istop = -8.8888;
        end
    end
end
r = prob.cnl.iterfcn(prob,r);




function dh = cnl_dh(x, prob)%, h
% if nargin<3
    h = prob.cnl.h(x, prob);
    if isempty(h); dh = []; return; end
%     if prob.advanced.debug
%         prob.pWarn('providing h to cnl_dh() sometimes significally reduces time of calculations');
%     end
% end

% dh_primal = prob.cnl.dh(x,prob, h)
dh_primal = prob.cnl.dh(x,prob);% (prob.n x prob.nh) matrix for x been vector, not matrix or cell!
dh_primal(:, h<0) = - dh_primal(:, h<0);
if ~any(dh_primal); dh = dh_primal; return; end
LAMBDA = prob.lambda;
switch prob.advanced.hPenalty
    case 'linear'
        dh = (ones(prob.n,1) * LAMBDA.') .* dh_primal;
    case 'norm2'
        dh = (ones(prob.n,1) * LAMBDA.') .* dh_primal;
%         dh = h .* (LAMBDA ./ norm(h)) .* dh_primal;%LAMBDA should be 1 x 1
%         dh = -((LAMBDA .* sign(h)) * ones(1,prob.n))' .* dh_primal;
        %debug
%         dh2 = zeros(prob.n, length(h));
%         h0 = prob.h(x,prob) ;
%         for i=1:prob.n
%             x(i) = x(i) + prob.diffInt;
%             dh2(i,:) = (prob.h(x,prob) - h0) / prob.diffInt;
%             x(i) = x(i) - prob.diffInt;
%         end
%         if norm(dh-dh2)>1e-2
%         disp(full(dh))
% %         dh = dh2;
%         end
        
        %debug end
        
%     case 'muNormLogMu'
%         dist1 = norm(x - prob.xInactive{1});
%         dist2 = norm(x - prob.xInactive{2});
%         dist = dist1+dist2;
% %         dh =  ones(prob.n, 1) * (LAMBDA .* (-1/2) ./ (dist1+dist2) .* log(1 + LAMBDA .* abs(h))).'+ ...
%              dh_primal.*(ones(prob.n, 1) * ((dist .* LAMBDA.^2 .* sign(h) ./ (1 + LAMBDA .* abs(h)))).'); 
    otherwise
        prob.err('isn''t implemented yet')
end

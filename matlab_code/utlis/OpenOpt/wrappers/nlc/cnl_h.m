function h = cnl_h(x, prob)

h_primal = prob.cnl.h(x,prob);

LAMBDA = prob.lambda;
H = h_primal;
switch prob.advanced.hPenalty
    case 'linear' 
        h = LAMBDA .* abs(H);
    case 'norm2'
        h = LAMBDA .* abs(H);
%         h = LAMBDA .* norm(H, 2);
%     case 'muNormLogMu'
%             under construction        
%         Dist = norm(x - prob.xInactive{1})+norm(x - prob.xInactive{2});
%         h = LAMBDA .* Dist .* log(1 + LAMBDA .* abs(H));
    otherwise
        prob.err('isn''t implemented yet')
end


function dc = cnl_dc(x, prob)
% x = [0.80000000001; 1];
% if nargin<3
c = prob.cnl.c(x, prob);
% if prob.advanced.debug
%     prob.pWarn('providing c to cnl_dc() sometimes significally reduces time of calculations');
% end

%todo: is it essential?
c(c<0)=0;

%dc_primal = prob.cnl.dc(x,prob, c)
dc_primal = prob.cnl.dc(x,prob);% (prob.n x prob.nc) matrix for x been vector, not matrix or cell!


dc_primal(:,c<=0) = 0;% btw c<0 must already be replaced by 0
if ~any(dc_primal); dc = dc_primal; return; end

MU = prob.mu;
switch prob.advanced.cPenalty
    case 'linear'
        dc = (ones(prob.n,1) * MU.') .* dc_primal;
    case 'norm2'
        dc = (ones(prob.n,1) * MU.') .* dc_primal;
        %dc = c .* (MU ./ norm(c)) .* dc_primal;%MU should be 1 x 1
%         c3 = prob.c(x,prob);
%         dc2 = zeros(size(dc));
%         for i=1:prob.n
%             x(i)=x(i)+prob.diffInt;
%             dc2(i,:) = (prob.c(x,prob) - c3)/prob.diffInt;
%             x(i)=x(i)-prob.diffInt;
%         end
%         disp(full([dc dc2 dc-dc2]))
%         disp(norm(dc-dc2))
        
        
%     case 'muNormLogMu'
        %under construction
%         
%         x1 = x - prob.xInactive{1};
%         x2 = x - prob.xInactive{2};
%         dist1 = norm(x1);
%         dist2 = norm(x2);
%         dist = dist1+dist2;
%         dc = -(x1./ dist1 + x2./dist2) * (MU .* log(1 + MU .* c)).'+ ...
%             dc_primal .* (ones(prob.n, 1) * ((dist .* MU.^2 ./ (1 + MU .* c))).');
%          
%          %debug
%          dc2 = zeros(prob.n,length(c));
%          c3 = prob.c(x,prob);
%          for i=1:prob.n
%             x(i)=x(i)+prob.diffInt;
%             dc2(i,:) = (prob.c(x,prob) - c3)/prob.diffInt;
%             x(i)=x(i)-prob.diffInt;             
%          end
%          disp([dc dc2 dc-dc2])
%          dc=dc2;
%          %debug end
    otherwise
        prob.err('isn''t implemented yet')
end
%         c = %MU .* dist .* log(1 + MU .* abs(c));

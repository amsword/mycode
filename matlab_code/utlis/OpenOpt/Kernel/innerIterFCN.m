function r = innerIterFCN(prob, r)
global globstat

% if ~globstat.nIter; globstat.tmp.min_violations = violations; end
% if norm(globstat.tmp.min_violations, 1)<= norm(violations, 1)
%     violationsAreDecreased = 1;
% else
%     violationsAreDecreased = 0;
% end
% % if r.f< globstat.ff && (~any(violations>prob.TolCon) || norm(violations,1) <= norm(globstat.tmp.prev_violations, 1)) ||...
% %         (globstat.ff-norm(violations,1)>=r.f-norm(globstat.tmp.prev_violations, 1) && ~any(violations>prob.TolCon))
% 
% 
% % WARN! don't replace ~any() by all() - it doesn't work for empty matricies 
% if r.f < globstat.ff %&& (~any(violations>prob.TolCon) || violationsAreDecreased)
%      %&& ~(any(violations>prob.TolCon) && norm(violations,1) > norm(globstat.tmp.prev_violations, 1))
%     globstat.inner.ff = f;
%     globstat.inner.xf = x;
% end%todo: handle fiter - xiter mismatch case 
% 
% if violationsAreDecreased
%     globstat.tmp.min_violations = violations;
% end



% if isfield(r,'x'); x = r.x; else x = r.xf; end
% if isfield(r,'f'); f = r.f; else f = r.ff; end

istop = 0;% continue calculations
% if size(x,2)~=1 || size(x,1)~=prob.n
%     prob.err('you must pass (size prob.n x 1) vector x to the outputfcn')
% end
if ~isfield(globstat, 'inner')
    globstat.inner.nIter = 0;
end
    
% if prob.advanced.traceIterF
%     globstat.inner.iterFvals(globstat.inner.nIter) = f;
% end
% if prob.advanced.traceIterX
%     globstat.inner.iterFvals(:, globstat.inner.nIter) = x;
% end

% if ~isempty(prob.user.outputfcn)
%     feval(prob.user.outputfcn,prob, r);
% end

mf = r.f;
if globstat.inner.nIter%so fields lastX & lastOptimFVal already exist
    EpsFun = norm(globstat.inner.lastOptimFVal - mf);
    EpsX = norm(globstat.inner.lastX - r.x);
    if EpsFun<prob.TolFun && EpsFun && ~ismember(4, prob.advanced.ignoreIStop); istop = 4; end
    globstat.inner.lastOptimFVal = mf;
    if EpsX<prob.TolX && EpsX && ~ismember(3, prob.advanced.ignoreIStop); istop = 3; end
    globstat.inner.lastX = r.x;
else
    globstat.inner.lastOptimFVal = mf;
    globstat.inner.lastX = r.x; %#ok<FNDSB>
end
globstat.inner.nIter = globstat.inner.nIter + 1;


if globstat.inner.nIter >= prob.MaxIter
    istop = -7;%max iter exeeded; 
    globstat.inner.msg = ['max Iter limit ' num2str(prob.MaxIter) ' has been exeeded'];    
    globstat.inner.nIter = 0;
end

if isfield(prob.advanced, 'cputimeStart') && cputime - prob.advanced.cputimeStart > prob.MaxCPUTime
    istop = -8;
    globstat.inner.msg = ['CPU time limit ' num2str(prob.MaxCPUTime) ' has been exeeded'];
end

if isfield(prob.advanced, 'timeStart') && 24*60*60*(now-prob.advanced.timeStart) > prob.MaxTime
    istop = -9;
end

if (isfield(r,'f') && isreal(r.f) && ~isempty(r.f) && r.f<prob.fEnough) || (isfield(r,'ff') && isnumeric(r.ff) && ~isempty(r.ff) && r.ff<prob.fEnough)
    istop = 10;
end

if istop || r.istop
    r.ff = r.f; 
    r.xf = r.x;
    r.istop = istop; 
end



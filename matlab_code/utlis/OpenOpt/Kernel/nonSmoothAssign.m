function prob = nonSmoothAssign(varargin)
% ooAssign analog for non-smooth funcs
% see usage of ooAssign

prob = ooInit();
prob.diffInt = 1e-5;
[prob.TolFun prob.TolCon prob.TolX] = deal(1e-4);

i0 = 3; %#ok<NASGU>
assignTo = 'prob'; %#ok<NASGU>
v = varargin;
%first param - objective function: name or func handler
if iscell(v{1}); prob.f = v{1}; else prob.f = {v{1}}; end
prob.primal.x0 = v{2};

prob.x0 = v{2};
prob.n = length(prob.x0);
prob.lb = -inf * ones(prob.n,1);
prob.ub =  inf * ones(prob.n,1);

assignScript;



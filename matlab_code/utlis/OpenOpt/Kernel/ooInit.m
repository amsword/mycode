function prob = ooInit()

prob.name = 'unnamed';

prob.varargin = {};%additional parameters to objFun & related

prob.advanced.traceIterX = 0;%traceIterF always = 1, need for graphics
prob.advanced.traceX = 0;% beware - may not satisfy constraints
prob.advanced.traceF = 0;% beware - may not satisfy constraints
prob.advanced.getViolations = @ooGetViolations;
prob.advanced.ignoreIStop = [];%if value from this array will be encountered in iterfcn than r.istop will be reset to 0 (continue calculations)

prob.check.df = 0;% check numerical &
prob.check.dh = 0;% user-supplied gradients
prob.check.dc = 0;
% lines with difference less than maxViolation will be not shown
prob.check.maxViolation = 1e-8;


prob.advanced.cPenalty = 'linear';% other under construction
prob.advanced.hPenalty = 'linear';% other under construction

prob.graphics.plotOnlyCurrentMinimum = false;
prob.doPlot = false;% draw picture or not
prob.graphics.minNPoints2draw = 1;%no figure updating untill numNewPoints<minNPoints2draw
prob.graphics.drawingInOneWindow = true;%some solvers for the same problem

%what do you want on label x?
prob.graphics.typeX = 'time';%case-unsensitive
%other values: CPUTime, nIter
%ignores time, spent on figure updatings
%nIter not recomended because iterations of different solvers take
%different time
%cputime is unrecomended on computers with several CPU
%because some solvers can handle different number of CPU units
%so time is best provided no other programs consume much cputime

prob.graphics.markerSize = 10; % currently works only in MATLAB

%you can redirect these ones
prob.err = @ooerr;
prob.warn = @warning;
prob.assert = @ooassert; % user can already have other assert.m file
%persistent warning, is called no more than 1 times per 
%MATLAB or Octave session
prob.pWarn = @ooPWarn;
%for future purposes, now they are disp()
prob.info = @ooinfo;
prob.hint = @oohint;

prob.useScaling = false;%if true, ScaleFactor or TypicalX must be provided, else x0 will be used as  TypicalX. However, OpenOpt automatic scaling isn't tested properly yet.

prob.debug = 0;
% prob.graphics.lowerBoundForPlotEstim = 0;%for future implement

prob.iterPrint = 10;
%if prob.iterPrint<0 -- no output
%if prob.iterPrint==0 -- final output only

prob.normx = @(x) norm(x, 2);% = norm(x) = sqrt(sum(x.^2))
%use ordinary norm where possible, it's for future implementation


prob.MaxIter = 400;
prob.MaxFunEvals = 1e5;
prob.MaxCPUTime = inf;
prob.MaxTime = inf;
prob.MaxLineSearch = 500;
prob.TolX = 1e-6;
prob.TolGrad = 1e-6;
prob.TolFun = 1e-6;
prob.TolCon = 1e-6;

prob.lb=[];
prob.ub=[];

%finite-difference gradient aproximation step
prob.diffInt=1e-7;

prob.isVectoriezed = 0;% isn't tested properly yet


prob.parallel.f = 0;% 0 - don't use parallel calclations, 1 - use
prob.parallel.c = 0;
prob.parallel.h = 0;
%others are under development


prob.f0 = [];

prob.justCheck = 0;%for future implementation
prob.iterfcn = @ooIter;% this parameter is only for OpenOpt developers, not common users

prob.draw = @oodraw;

if exist('matlabroot', 'builtin')
    prob.parallel.fun = @dfeval;
    prob.env = 'matlab';
    prob.graphics.timePlotInterval = 0.08;%no more than 1 update per this time (sec)
end
if exist('OCTAVE_VERSION') %#ok<EXIST>
    prob.parallel.fun = @(varargin) error('in field prob.parallel.fun you must provide parallelization func similar to MATLAB @dfeval');
    prob.env= 'octave';
    prob.graphics.timePlotInterval = 0.8;%no more than 1 update per this time (sec)
end

%non-linear constraints
prob.c = []; % c(x)<=0
prob.h = []; % h(x)=0

prob.goalType = 'minimum';
% other possible values: 'maximum'

% todo: implement in a field prob classification:
% 'minimax', 'multiobjective'(or goalattain?), etc

prob.user.outputfcn = [];

prob.fPattern = [];
prob.cPattern = [];
prob.hPattern = [];

% A * x <= b inequalities
prob.A = [];
prob.b = [];

% Aeq * x = b equalities
prob.Aeq = [];
prob.beq = [];

%prob.primal.* fields are for OpenOpt developers only, not common users
prob.primal.f = [];
prob.primal.df = [];
prob.primal.d2f = [];
prob.primal.c = [];
prob.primal.h = [];
prob.primal.dc = [];
prob.primal.dh = [];
prob.primal.d2c = [];
prob.primal.d2h = [];

prob.fEnough = -inf;%if value less than fEnough will be obtained
%solver will be stopped.
%this param is handled in iterfcn of OpenOpt Kernel

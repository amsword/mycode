if ~exist('strtrim.m', 'file'); strtrim = @strtrim2; end%Octave
v2 = {};
for i = i0:length(v) %#ok<NODEF>
    vstr = v{i};
    while ~isempty(vstr)
        [str vstr] = strtok(vstr, ';'); %#ok<STTOK>
        v2{end+1} = str; %#ok<AGROW>
    end
end
v = v2;
i=1;
while i<=length(v)
    if ~ischar(v{i});prob.err(11);end
    i1 = length(find(v{i}(end)=='+'));
    i2 = length(find(v{i}(end)=='-'));
    i3 = length(find(v{i}==':'));
    i4 = length(find(v{i}=='='));
    switch i1+i2+i3+i4
        case 0
            if i>=length(v); prob.err('incorrect assignment'); end
            eval([assignTo '.' v{i} '=v{i+1};'])
            i = i+2;
            %I could use setfield but it don't work with
            %nested fields, for example 'user.set1.param5'
            %(at least in MATLAB2006a)
        case 1
            if i1
                v{i}(end)=[];
                eval([assignTo '.' v{i} '=true;'])
            elseif i2
                v{i}(end)=[];
                eval([assignTo '.' v{i} '=false;'])
            elseif i3
                [fieldName fieldVal] = strtok(v{i}, ':');%setting as string
                fieldVal(1)=[];%suppressing ':'
                eval([assignTo '.' strtrim(fieldName) '=''' strtrim(fieldVal) ''';'])
            else
                [fieldName fieldVal] = strtok(v{i}, '=');
                fieldVal=strtrim(fieldVal);
                fieldVal(1)=[];%suppressing '='
%                 fV = str2num(fieldVal);%#ok<NASGU,ST2NM> %not str2double because complex can occure
%                 if isempty(fV);fV = fieldVal; end
                eval([assignTo '.' strtrim(fieldName) '=' fieldVal ';'])
            end
            i = i+1;
        otherwise
            prob.err('incorrect assignment');
    end
end


function dh = oo_dh(x, prob, fh)
%TODO: ADD CONSPATTERN
if isempty(prob.primal.dh)
    %todo: if objfunc is vectorized - modify this!(below)
    %todo:replace by parfor
    if nargin<3; h = oo_h(x, prob); else h = fh; end
    if isfield(prob, 'hPattern') && ~isempty(prob.hPattern)
        IND = find(any(prob.hPattern, 2)).';%todo: make it persistant
        dh = spalloc(length(h), prob.n, nnz(prob.hPattern));
    else
        IND = 1:prob.n;
        dh = spalloc(length(h), prob.n, length(1:prob.nh)*prob.n);
    end
    
%     switch prob.env
        %todo: x -> X
%         case 'matlab'
                for i = IND%indPattern(j)
                    x(i)=x(i)+prob.diffInt;
                    dh(:,i) = oo_h(x, prob)-h;
                    x(i)=x(i)-prob.diffInt;
                end
%         case 'octave'
%                 for i = IND%indPattern(j)
%                     x(i)=x(i)+prob.diffInt;
%                     d_h = oo_h(x, prob)-h;
% 		    %todo: add 'full', not 'sparse' version
%                     for k = find(d_h.');  dh(i,k) = d_h(k); end
%                     x(i)=x(i)-prob.diffInt;
%                 end
%         otherwise
%             prob.err('unknown environment')
%     end

    dh = dh.' ./ prob.diffInt;
    return
end

if prob.useScaling
    dh = prob.primal.dh(x .* prob.ScaleFactor, prob.varargin{:}).*prob.ScaleFactor;    
else
    dh = prob.primal.dh(x, prob.varargin{:});
end

global globstat
%todo: what about ceq counting?
globstat.nDHEvals = globstat.nDHEvals+size(x,2);


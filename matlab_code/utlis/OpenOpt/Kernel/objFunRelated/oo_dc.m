function dc = oo_dc(x, prob, fc)
fc = [];
%TODO: ADD CONSPATTERN
if isempty(prob.primal.dc)
    %todo: if objfunc is vectorized - modify this!(below)
    %todo:replace by parfor
    if nargin<3||isempty(fc); c = oo_c(x, prob); else c = fc; end
    indActiveIneqConstraints = find(c>0);
    %don't replace length(c) by prob.nc!
    if isempty(indActiveIneqConstraints); dc = sparse(prob.n, length(c)); return;  end
    if isfield(prob, 'cPattern') && ~isempty(prob.cPattern)
%         indPattern = @(j) find(prob.cPattern(j,:))';
        IND = find(any(prob.cPattern(:,indActiveIneqConstraints),2)).';
        dc = spalloc(length(c), prob.n, nnz(prob.cPattern));
    else
%         indPattern = @(j) 1:prob.n;
        IND = 1:prob.n;
        %don't replace length(c) by prob.nc!
        dc = spalloc(length(c), prob.n, length(indActiveIneqConstraints)*prob.n);
    end
%     switch prob.env
%         case 'matlab'
% 
%             for i = IND % IPj
%                 x(i)=x(i)+prob.diffInt;
%                 dc(:,i) = oo_c(x, prob)-c;
%                 x(i)=x(i)-prob.diffInt;
%             end
% 
%         case 'octave'
            for i = IND % IPj
                x(i)=x(i)+prob.diffInt;
                dc(:,i) = oo_c(x, prob)-c;
                x(i)=x(i)-prob.diffInt;
            end
%             for i = IND%IPj
%                 x(i)=x(i)+prob.diffInt;
%                 d_c = oo_c(x, prob)-c;
%                 for k = find(d_c.');  dc(i,k) = d_c(k); end
%                 x(i)=x(i)-prob.diffInt;
%             end
%         otherwise
%             prob.err('unknown environment')
%     end
    dc = dc.' ./ prob.diffInt;
    return
end

if prob.useScaling
    dc = prob.primal.dc(x .* prob.ScaleFactor, prob.varargin{:}).*prob.ScaleFactor;    
else
    dc = prob.primal.dc(x, prob.varargin{:});
end

global globstat
%todo: what about ceq counting?
globstat.nDCEvals = globstat.nDCEvals+size(x,2);


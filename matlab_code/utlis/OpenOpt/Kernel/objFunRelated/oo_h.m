function h = oo_h(x, prob)
global globstat

if isfield(globstat, 'xPrevH') && isequal(x, globstat.xPrevH)
    h = globstat.hPrevH; return
end
globstat.xPrevH = x;

if prob.useScaling
    x = x .* repmat(prob.ScaleFactor, 1, size(x,2));
end

if prob.isVectoriezed || isvector(x)
    h = prob.primal.h(x, prob.varargin{:});
    h = h(:);
else
    h = zeros(prob.primal.nh, size(x,2));%allocating for the same size
    for i=1:size(x,2)% todo: replace by parfor()?
        h(:,i) = prob.primal.h(x(:,i), prob.varargin{:});
    end
end


globstat.hPrevH = h;

globstat.nHEvals = globstat.nHEvals+size(x,2);

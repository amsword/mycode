function c = oo_c(x, prob)
global globstat
if isfield(globstat, 'xPrevC') && isequal(x, globstat.xPrevC)
    c = globstat.cPrevC; return
end
globstat.xPrevC = x;

if prob.useScaling
    x = x .* repmat(prob.ScaleFactor, 1, size(x,2));
end

if prob.isVectoriezed || isvector(x)
    c = prob.primal.c(x, prob.varargin{:});
    c = c(:);
else
    c = zeros(prob.primal.nc, size(x,2));%allocating for the same size
    for i=1:size(x,2)% todo: replace by parfor()?
        c(:,i) = prob.primal.c(x(:,i), prob.varargin{:});
    end
%     c = cell(size(x,2),1);%allocating for the same size
%     for i=1:size(x,2)% todo: replace by parfor()?
%         if prob.useScaling
%             c{i} = prob.primal.c(x(:,i) .* prob.ScaleFactor, prob);
%         else
%             c{i} = prob.primal.c(x(:,i), prob);
%         end
%     end
end


globstat.cPrevC = c;

globstat.nCEvals = globstat.nCEvals+size(x,2);

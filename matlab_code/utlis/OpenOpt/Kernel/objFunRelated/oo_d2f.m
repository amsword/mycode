function d2f = oo_d2f(x, prob)
%todo: check me!
if prob.useScaling
    d2f = prob.primal.d2f(x .* prob.ScaleFactor, prob);
    %debug
%     d2f2 = d2f;
%     for i=1:prob.n
%         B(i,i) = prob.ScaleFactor(i)^2;
%     end
%     d2f2 = d2f2 * B;
    %debug end
    for i=1:prob.n
        d2f(i,:) = d2f(i,:) * prob.ScaleFactor(i);
        d2f(:,i) = d2f(:,i) * prob.ScaleFactor(i);
    end
    ds = d2f2-d2f;
else
    d2f = prob.primal.d2f(x, prob);
end

if strcmp(prob.goalType, 'maximum'); d2f = -d2f; end

global globstat
globstat.nHessEvals = globstat.nHessEvals+1;

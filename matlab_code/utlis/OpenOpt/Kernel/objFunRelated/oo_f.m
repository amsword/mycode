function f = oo_f(x, prob)
% x = vector prob.n x 1 or matrix X=[x1 x2 x3...xk], size(X)=[prob.n, k]
global globstat
if isfield(globstat, 'xPrevF') && isequal(x, globstat.xPrevF)
    f = globstat.fPrevF; return
end
globstat.xPrevF = x;


if size(x,1)~=prob.n; prob.err('incorrect x passed to obj fun'); end
if prob.useScaling
    x = x .* repmat(prob.ScaleFactor, 1, size(x,2));
end

f = zeros(length(prob.primal.f),size(x,2));
for j = 1:length(prob.primal.f)
    fun = prob.primal.f{j};
    if isnumeric(fun)%isvector(cell) also yields 1
        f(j,:) = sum(repmat(fun(:), 1, size(x,2)) .* x);
    elseif prob.isVectoriezed || isvector(x)
        f(j,:) = sum(fun(x, prob.varargin{:}));
    else
        if prob.parallel.f
            VARARGIN = cell(size(x,1),1);
            XX = cell(size(x,1),1);
            for i = 1:size(x,1)
                XX{i} = x(:,i);
                VARARGIN{i} = prob.varargin;
            end
            FF = prob.parallel.fun(fun, XX, VARARGIN);
            tmp = [FF{:}];
            f(j,:) = tmp(:) .';
            disp('ok')
        else
            for i=1:size(x,2)
                f(j,i) = fun(x(:,i), prob.varargin{:});
            end
        end
    end
end
f = sum(f,1);
f = f(:);
if strcmp(prob.goalType, 'maximum'); f = -f; end

globstat.fPrevF = f;

% try
    globstat.nFunEvals = globstat.nFunEvals + size(x,2);
% catch
%    keyboard 
% end
if prob.advanced.traceF
    globstat.F(globstat.nFunEvals:globstat.nFunEvals+size(x,2)) = f(:);
end
if prob.advanced.traceX
    globstat.X(:,globstat.nFunEvals-size(x,2)+1:globstat.nFunEvals) = x;%may not satisfy constraints
end


function df = oo_df(x, prob, arg, ObjFun)
global globstat
%todo: if x is matrix!
%HINT!! F0 is allowed to be empty!!
ObjFun = @oo_f;
if isempty(prob.primal.df)
%     if nargin>=4
        objFun = ObjFun;
%     else
%         objFun = prob.f;
%     end
    if isequal(globstat.xPrevF, x)
        f0 = globstat.fPrevF;
        X = x * ones(1, prob.n) + prob.diffInt*speye(prob.n);
        df = (objFun(X, prob) - f0)./prob.diffInt;
    else
        X = [x x * ones(1, prob.n) + prob.diffInt*speye(prob.n)];
        d_f = objFun(X, prob);
        df = (d_f(2:end) - d_f(1))./prob.diffInt;
    end
    return
end

if prob.useScaling
    df = prob.primal.df(x .* prob.ScaleFactor, prob.varargin{:}).*prob.ScaleFactor;    
else
    df = prob.primal.df(x, prob.varargin{:});
end

if strcmp(prob.goalType, 'maximum'); df = -df; end


globstat.nGradEvals = globstat.nGradEvals+1;

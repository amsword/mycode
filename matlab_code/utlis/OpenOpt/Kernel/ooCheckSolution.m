function r = ooCheckSolution(prob, r)
r.isFeasible = 1;

conds = {
    any(isnan(r.ff)) 'obtained solution with NaN in Fval Final';...
    any(isnan(r.xf)) 'obtained solution with at least one coord NaN';...
    ~isempty(prob.lb) && any(isfinite(prob.lb)) && any(r.xf - prob.primal.lb < -prob.TolCon) 'exist coord i: x[i] < lb[i]';...
    ~isempty(prob.ub) && any(isfinite(prob.ub)) && any(r.xf - prob.primal.ub > prob.TolCon) 'exist coord i: x[i] > ub[i]';...
    ~isempty(prob.A) && any(prob.primal.A * r.xf > prob.primal.b + prob.TolCon) 'exist coord i: (A*x)[i] > b[i]';...
    ~isempty(prob.Aeq) && any(abs(prob.primal.Aeq * r.xf - prob.primal.beq) > prob.TolCon) 'exist coord i: (Aeq*x)[i] ~= beq[i]';...
    ~isempty(prob.primal.c) && any(prob.primal.c(r.xf, prob.varargin{:}) > prob.TolCon) 'exist coord i: (c(x))[i] > 0';...
    ~isempty(prob.primal.h) && any(abs(prob.primal.h(r.xf, prob.varargin{:})) > prob.TolCon) 'exist coord i: (ceq(x))[i] ~= 0';
    };
for i=1:size(conds, 1)
    if conds{i, 1} %#ok<BDSCI>
        msg = ['solution is infiasible: ' conds{i,2}];
        prob.infiasibleMsg = msg;
        r.isFeasible = 0;
        if r.istop>=0
            r.istop = str2num(['-0.' num2str(r.istop)]); %#ok<ST2NM>
        end
    end
end


function r = ooRun(prob, solver)
%some problems with recursive ooRun call exist
%maybe, I will fix it later //Dmitrey

isNotCell = ~iscell(prob) && ~iscell(solver);

if ~iscell(prob); prob = {prob}; end
if ~iscell(solver); solver = {solver}; end
    
% todo: add "skip" parameter - ignore ooRunProbSolver failure, 
% run other probs
for i=1:length(prob)
    for j=1:length(solver)
        r{i,j} = ooRunProbSolver(prob{i}, solver{j}); %#ok<AGROW>
        % isinteger is absent in Octave
    end
end

if isNotCell; r = r{1,1}; end

function r = stopcase(arg)

if isstruct(arg); istop = arg.istop;
else istop = arg; end

if istop>0
    r = 1;
elseif ismember(istop, [-7 -8 -9 -10])
    r = 0;
else
    r = -1;
end

function ooCheck(prob)

if ~isempty(prob.A) && (length(prob.b)~=size(prob.A,1) || size(prob.A,2)~=prob.n)
    prob.err('incorrect A or b size');
end

if ~isempty(prob.Aeq) && (length(prob.beq)~=size(prob.Aeq,1) || size(prob.Aeq,2)~=prob.n)
    prob.err('incorrect Aeq or beq size');
end

if length(prob.b) ~= size(prob.b, 1)
    prob.err('b must be N x 1 vector');
end

if length(prob.beq) ~= size(prob.beq, 1)
    prob.err('beq must be N x 1 vector');
end

if ~isempty(prob.ub) && size(prob.ub, 1) ~= prob.n
    prob.err('ub must be N x 1 vector');
end

if ~isempty(prob.lb) && size(prob.lb, 1) ~= prob.n
    prob.err('lb must be N x 1 vector');
end
CheckStructHasEqualFieldNames(prob, prob);
fn = fieldnames(prob);
for i = 1:length(fn);
    Field = getfield(prob, fn{i}); %#ok<GFLD>
    if isstruct(Field)
        CheckStructHasEqualFieldNames(Field, prob);
    end
end
nskiplines=0;
separator = '==========================================================================';
for fn = {'df' 'dc' 'dh'}
    fun_end = fn{1};
    if getfield(prob.check, fun_end) %#ok<GFLD>
        prob.assert(~isempty(getfield(prob, fun_end)), ['you must provide gradient for check or turn option prob.check.' fun_end ' off']) %#ok<GFLD>
        nskiplines = nskiplines + ooCheckGradient(prob, fun_end, separator);
    end
end
if nskiplines
    disp([int2str(nskiplines) ' lines with difference less than'])
    disp(['prob.check.maxViolation = ' num2str(prob.check.maxViolation) ' were skipped'])
    disp(separator)
    disp(' ')
end
end% ooCheck

function CheckStructHasEqualFieldNames(s, prob)
fn = fieldnames(s);
U = unique(upper(fn));
if length(U) ~= length(fn)
%     prob.warn('Equal fieldnames in struct exist, that differs only in lower/upper case');
    for i=1:length(fn)
        for j=i+1:length(fn)
            if isequal(upper(fn{i}), upper(fn{j}))
                prob.warn(['2 fields differs only in case: ' fn{i} ' and ' fn{j}]);
                disp('press any key to continue')
                pause
            end
        end
    end
    
end
end% CheckStructHasEqualFieldNames

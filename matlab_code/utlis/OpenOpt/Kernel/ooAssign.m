function prob = ooAssign(varargin)
% ooAssign usage:
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2', value2, ...)
% only 2 parameters of ooAssign required 
% 1st is objFun(string or handler)
% 2nd is x0
% if value2 is numeric, like 8.15 or [15 8 80], you can use
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2=value2', param3, value3, ...)
% 
% if value2 is string, like 'asdf' or 'maximum', you can use:
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2:asdf', param3, value3, ...)
% 
% if value2 is bool (true or false, i.e. 0 or 1), you can use
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2+', param3, value3, ...)
% or
% ooAssign(objFun, x0, 'param1', value1, 'param2-', param3, value3, ...)
% '+' means true(1), '-' means false(0)

% you can use ';' delimiter inside ooAssign:
% ooAssign(objFun, x0, 'param1=value1; param2=value2;param3=value3; etc'
% for example 
% p = ooAssign(@sin, 8, 'goalType:maximum; JohnSmith.param1=transpose([1 2 3])');
% p.JohnSmith.param2 = [2 3 4; 4 5 6; 6 7 8];
%
% for non-smooth funcs nonSmoothAssign() is prefered
% (it sets other default TolFun, TolCon, TolX, diffInt fields of prob)

prob = ooInit();
i0 = 3; %#ok<NASGU>
assignTo = 'prob'; %#ok<NASGU>
v = varargin;
%first param - objective function: name or func handler
if iscell(v{1}); prob.f = v{1}; else prob.f = {v{1}}; end
prob.primal.x0 = v{2};

prob.x0 = v{2};
prob.n = length(prob.x0);
prob.lb = -inf * ones(prob.n,1);
prob.ub =  inf * ones(prob.n,1);

assignScript;



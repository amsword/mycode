function r = msgstop(arg)
%see also stopcase.m

if isstruct(arg); istop = arg.istop;
else istop = arg; end

switch istop
    case 0
        r = 'undefined';
    case 1
        r = 'problem has been solved, stop criterium description unimplemented yet';
    case 2
        r = '(sub)gradient norm less than prob.TolGrad';
    case 3
        r = 'norm(x[k+1]-x[k]) less than prob.TolX';
    case 4
        r = 'norm(f[k+1]-f[k]) less than prob.TolFun';
    case 5
        r = 'no enhancement for several iterations';
    case 9
        r = 'Temperature is too cold';
    case 10
        r = 'Value better or equal to user-specified fEnough is reached';
    case 11
        r = 'all variables are fixed';
    case 12
        r = 'max consec rejections exeeded';
    case 13 % used in nonSmoothSolve
        r = 'for all i |F[i](x)| < prob.TolFun';
    %negative
    case -2
        r = 'objFun(x0) calculation faled or equals to NaN';
    case -3
        r = 'gradient(x0) has coordinate that equals to NaN';
    case -4
        r = 'x with at least one coord NaN has been obtained during calculations, currently OpenOpt can''t handle this situation, maybe it will be implemented in future';
    case -5
        r = 'no line-search minimum found (problem seems to be unbounded). You can turn prob.MaxLineSearch';
    case -7
        r = 'Iter Num exeeded prob.MaxIter';
    case -8
        r = 'cputime exeeded prob.MaxCPUTime';
    case -9
        r = 'time exeeded prob.MaxTime';
    case -10
        r = 'fun evals num exeeded prob.MaxFunEvals';
    case -11
        r = 'no feasible solution obtained';
    case -12
        r = 'post-solver check: solution is infeasible';%see also istop = -100.something in "otherwise"
    case -13
        r = 'max number of updating Lagrange multipliers is exeeded';
    otherwise
        if istop < 0 && istop > -1
            r = 'post-solver check: solver reports "OK", but solution is infeasible';
        else
            r = 'this stop code is unimplemented yet, please update the file msgstop.m';
            warning(r); %#ok<WNTAG>
        end
end

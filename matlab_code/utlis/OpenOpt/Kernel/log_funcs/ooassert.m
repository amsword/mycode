function ooassert(cond,msg)
%ASSERT - Similar to the C "assert" function
%
% ASSERT(cond,msg)
%
% cond should be an expression which is believed to be true.
% msg is a message describing the assertion.
%
% e.g. ASSERT(x>5 && x<20,'x is within the allowed limits')
%
% This should not be thought of as "normal" error handling.
% It is designed to help catch bugs in the code, and so the
% call stack is printed in the command window (so that it
% is not lost if the error is caught).
%taken by Dmitrey from matlab files exchange area, 
%I don't remember who is author
if ~cond
    fprintf(1,'Assertion Failure: %s\n',msg);
    if exist('OCTAVE_VERSION')%todo:replace by globstat
    dbwhere
    else%matlab
    dbstack;
    end
    error('MATLAB:Utilities:AssertionFailure','Assertion Failure: %s',msg);
end

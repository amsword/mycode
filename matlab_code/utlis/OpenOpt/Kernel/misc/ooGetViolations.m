function v = ooGetViolations(x, prob)
%todo: implement decode(x)

if ~isempty(prob.primal.h)
    H = oo_h(x, prob);
else H = []; 
end

if ~isempty(prob.primal.c)
    C = oo_c(x, prob);
else C = []; 
end
if prob.useScaling
    X = x .* prob.ScaleFactor;
else X = x;
end
if ~isempty(prob.primal.b)
    s1 = max(prob.primal.A*X-prob.primal.b, 0);
else s1 = []; 
end

if ~isempty(prob.primal.beq)
    s2 = abs(prob.primal.Aeq*X-prob.primal.beq);
else s2 = [];
end

v = [...
    max(C(:), 0);... % x, not X
    s1;...
    max(prob.primal.lb(prob.advanced.finiteLB) - X(prob.advanced.finiteLB), 0);...
    max(-prob.primal.ub(prob.advanced.finiteUB) + X(prob.advanced.finiteUB), 0);...
    abs(H(:));... % x, not X
    s2;...
    ];


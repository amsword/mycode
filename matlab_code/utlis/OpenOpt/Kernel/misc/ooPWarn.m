function ooPWarn(msg)
persistent pWarns
if ismember(msg, pWarns)
    return
else
    warning(msg);
    pWarns{end+1} = msg;
end

function r = strtrim2(str)
r = str;

% if r(end) == ' '
%     ind = find(r ~= ' ', 1, 'last');
%this form of 'find' doesn't works in Octave
% r(ind+1:end)=[];
% end

% if r(1) == ' '
%     ind = find(r ~= ' ', 1, 'first');
%this form of 'find' doesn't works in Octave    
    
%     r(1:ind-1)=[];
% end

while r(end) == ' '; r(end) = []; end
while r(1) == ' '; r(1) = []; end

% by Dmitrey
% if variable structName exist => it will be "opened"
% elseware will be "opened" struct prob.<solverName> (if exist)
if ~exist('structName', 'var') && isfield (prob, 'solverName') && isfield(prob, prob.solverName)
    structName = ['prob.' prob.solverName];
    eval(['Struct=' structName ';'])
    fn = fieldnames(Struct);
    for i = 1:length(fn)
        eval([fn{i} '=' structName '.' fn{i} ';'])
    end
end



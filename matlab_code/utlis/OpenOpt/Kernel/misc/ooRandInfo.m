function Str = ooRandInfo(prob)
persistent msgs
if isempty(msgs)
    msgs.main = {...
        1 'Use param prob.doPlot = true/false to turn graphic output on/off';
        1 'Use param prob.fEnough - enough value for your objFun';
        1 'Want faster MATLAB bintprog analog (via a GRASP version by some icyb workers) in future OpenOpt ver? Donate something!';
        1 'Octave users, do you want MATLAB fmincon() analog in future OpenOpt ver? Donate something!';
        1 'Try using prob.parallel.f=1 for using dfeval() in numerical (sub)gradient obtaining (mdce, etc must be turned; c,h under development). Octave users must provide similar to dfeval func in prob.parallel.fun.';
        1 'You can use prob.graphics.typeX=''time'', ''cputime'', ''nIter'' for x axis, case-unsensetive';
        1 'In future OpenOpt versions (if they will exist) ralg & nonSmoothSolve will be enhanced!';
        1 'Check internet for newer OpenOpt version!';
        1.5 'Passing additional parameters: prob.varargin = {param1 param2 ... param_n}'
        1 'Use prob.check.df, .dc, .dh = 1 for auto check gradients provided by user';
        1 'Your objFun consists of agregats? Some options can speedup your program!';
        1.5 'Use prob.iterPrint<0 to turn the annoing messages off!';
        1.5 'Use prob.iterPrint=0 for final output only!';
        1 'Sometimes providing prob.fPattern, prob.cPattern, prob.hPattern can speedup your code!';
        1.5 'Problems with nonsmooth optimization? icyb optim department can help you!';
        1.5 'Problems with net optimization? icyb optim department can help you!';
        1 'OpenOpt ominsearch & nonSmoothSolve interface is similar to MATLAB fminsearch & fsolve';
        1 'Use icyb.kiev.ua claster from World TOP 500 with VERY CHEAP CPUtime!';
        1.5 'Increasing prob.diffInt, prob.TolFun for noisy problems can be very helpfull!';
        0.15 'Connect your own solver to the OpenOpt project!';
        0.15 'In future OpenOpt versions (if they will exist) ralg & nonSmoothSolve will be enhanced';
        0.15 'Found OpenOpt usefull? Please donate!';
        0.15 'Do you want further OpenOpt development? Please donate!';
        0.15 'Do you want more solvers in the OpenOpt project? Please donate!';
        0.15 'Do you want OpenOpt for Python project? Please donate!';
        1.5 'Increasing prob.TolCon, prob.TolGrad, prob.TolFun, prob.TolX can be very helpfull!';
        1.5 'Both MATLAB & free GNU Octave (www.octave.org) can run OpenOpt solvers!';
	1 'Using typical Lagrange multipliers from output structure sometimes can speedup your code!';
    };
    msgs.weights = [msgs.main{:,1}];
    msgs.weights = msgs.weights / sum(msgs.weights);
    msgs.counter = 0;
end
msgs.cumsum = cumsum(msgs.weights);

switch prob.env
    case 'matlab'
        ind = find(rand<msgs.cumsum, 1, 'first');
    case 'octave'
        ind = find(rand<msgs.cumsum, 1);%todo: check for future Octave ver for same find() syntaxis as matlab
    otherwise
        prob.err('unknown environment')
end
Str = msgs.main{ind,2};

v = msgs.weights(ind)/2;
msgs.weights = msgs.weights + v / (length(msgs.weights)-1);
msgs.weights(ind) = v;

%for avoiding numerical errors, in automatic calculations in endless cycle
msgs.counter = msgs.counter+1;
if msgs.counter>150
    msgs.weights = msgs.weights ./ sum(msgs.weights);
    msgs.counter = 1;
end

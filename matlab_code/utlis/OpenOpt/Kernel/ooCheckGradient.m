function nskiplines = ooCheckGradient(prob, fun_end, separator)
nskiplines = 0;
userFun = getfield(prob, fun_end); %#ok<GFLD>
info_user = feval(userFun, prob.x0, prob); %#ok<GFLD>
prob.primal = setfield(prob.primal, fun_end, []); %#ok<SFLD>
info_numerical = feval(getfield(prob, fun_end), prob.x0, prob); %#ok<GFLD>
if any(size(info_numerical)~=size(info_user))
    prob.err(['user-supplied gradient ' fun_end ' has other size than the one, obtained numerically: ' int2str(size(info_numerical, 1)) ' x ' int2str(size(info_numerical, 2)) ' expected, ' int2str(size(info_user, 1)) ' x ' int2str(size(info_user, 2)) ' obtained'])
    return
end
Diff = info_user-info_numerical;
d = [info_user(:) info_numerical(:) Diff(:)];

nskiplines = sum(abs(Diff(:)) < prob.check.maxViolation);

disp(['OpenOpt checks user-supplied gradient ' fun_end ' (size: ' int2str(size(info_user, 1)) ' x ' int2str(size(info_user, 2)) ')'])
disp(['(according to prob.diffInt = ' num2str(prob.diffInt) ')'])
if isempty(userFun); prob.warn(['prob.check.' fun_end ' is turned on, but no user-supplied gradient is provided']);return;end



if any(abs(Diff(:)) >= prob.check.maxViolation)
    ss = '';
    if ismember(fun_end, {'dc' 'dh'})
        ss = [' i,j: ' fun_end '[i]/dx[j]'];
    end
    s = [fun_end ' num:  ' ss '   user-supplied:    numerical:      difference:'];
    disp(s)
end
ns = ceil(log10(size(d,1)));
for i=1:size(Diff,1)
    for j=1:size(Diff,2)
        if abs(Diff(i,j)) < prob.check.maxViolation; continue; end
        k = size(Diff,2)*(i-1)+j;
        nSpaces = ns - floor(log10(k));
        if ismember(fun_end, {'dc' 'dh'})
            s1 = [repmat(' ', 1, 10 - floor(log10(i))) int2str(i)];
            s2 = [int2str(j) repmat(' ', 1, max(4 - floor(log10(j)),0))];
            ss = [s1 ' / ' s2];
        else
            ss = '   ';
        end
        s = [repmat(' ',1,nSpaces) sprintf('%-d     %s   %+0.4e    %+0.4e    %+0.4e', k, ss, d(i,1), d(i,2), Diff(i,j))];
        disp(s);
    end
end
disp(['max(abs(' fun_end '_user - ' fun_end '_numerical)) = ' num2str(norm(d(:,3),'inf'))]);
disp(['sum(abs(' fun_end '_user - ' fun_end '_numerical)) = ' num2str(norm(d(:,3),1))]);
disp(separator)

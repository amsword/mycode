function [x fval exitflag output] = ominsearch(fun, x0, options)
prob = ooAssign(fun, x0);
if nargin<3 && exist('optimset', 'file')
    try
        options = optimset(@fminsearch);
    catch
        options = [];
    end
end
prob = parseMATLABOptimToolboxOptions(options, prob);
if ischar(prob.MaxIter); prob.MaxIter=400; end
if ischar(prob.MaxFunEvals); prob.MaxFunEvals=1e4; end
r = ooRun(prob, @ralg);
x = r.xf;
fval = r.ff;

% exitflag can be OTHER (than MATLAB fminsearch)! 
% So using ominsearch is not recomended, use it only for comparing
exitflag = stopcase(r);


output.algorithm = r.alg;
output.funcCount = r.nFunEvals;
output.iterations = r.nIter;
output.message = r.msg;
output.randInfo = r.randInfo;

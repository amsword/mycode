function r = ooSolveIterFcn(prob, r)

r = prob.osolve.iterfcn(prob,r);
% disp(prob.h(r.xf,prob))
if isfield(prob, 'lambda'); Dlambda = prob.lambda;
else Dlambda = 1;
end
if isfield(prob, 'mu'); Dmu = prob.mu;
else Dmu = 1;
end

if norm(prob.h(r.xf,prob)./Dlambda, inf) < prob.TolFunSolve && ...
        (isempty(prob.c) || norm(prob.c(r.xf,prob)./Dmu, inf) < prob.TolCon)
    r.istop = 13;
end

function prob = parseMATLABOptimToolboxOptions(options, prob)
if isempty(options); return; end
FN = fieldnames(options);
for fn = (FN(:))'
    f = fn{1};
    switch f
        case {'TolFun' 'TolX' 'MaxFunEvals' 'MaxIter' 'MaxTime' 'iterPrint'}
            prob = setfield(prob, f, getfield(options, f)); %#ok<SFLD,GFLD>
    end
end
if ischar(prob.MaxIter); prob.MaxIter=400; end
if ischar(prob.MaxFunEvals); prob.MaxFunEvals=1e4; end
if isempty(prob.MaxTime); prob.MaxTime=inf; end

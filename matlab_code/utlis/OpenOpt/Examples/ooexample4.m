%Lemarechal: convex, non-smooth problem
N=10;
probLemarechal = nonSmoothAssign(@Lemarechal, zeros(N,1), 'name', 'Lemarechal10', ...
    'iterPrint=25;df = @GLemarechal;useScaling-',...
    'MaxIter=1500; doPlot+; ScaleFactor=[100 1 1 1 1 1 1 1 1 1]');
% probLemarechal.varargin = {N};
% probLemarechal.fEnough=-0.005;

r12 = ooRun(probLemarechal, 'ralg') %#ok<NOPRT>

r13 = ooRun(probLemarechal, 'anneal') %#ok<NOPRT>

% probLemarechal.lb = -1 * ones(probLemarechal.n,1);
% probLemarechal.ub = 1 * ones(probLemarechal.n,1);
xopt = [0.0242    0.0077   -0.0087   -0.0002    0.0475   -0.0892    0.0183   -0.0208    0.0133   -0.0032]';
probLemarechal.r0 = norm(xopt)*1.1;
r17 = ooRun(probLemarechal, 'ShorEllipsoid')%#ok<NOPRT> %need prob.r0

probLemarechal.lb = xopt-0.05;
probLemarechal.ub = xopt+0.05;

r14 = ooRun(probLemarechal, 'ralg') %#ok<NOPRT>
% or
probLemarechal.hPSO.maxInnerIter = 175;
probLemarechal.hPSO.innerSolver = 'ralg';
r15 = ooRun(probLemarechal, 'hPSO') %#ok<NOPRT>


%this line sometimes yields MATLAB error
%sometimes - not
%??? Insufficient number of outputs from right hand side of equal sign to satisfy assignment.
% probLemarechal.c = @(x, p) norm(x)-0.25;

r18 = ooRun(probLemarechal, 'GAconstrain') %#ok<NOPRT>


% probLemarechal.c = [];
% probLemarechal.lb = [];
% probLemarechal.ub = [];
% r16 = ooRun(probLemarechal, 'buscarnd')%#ok<NOPRT> %need r0




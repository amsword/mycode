% powers12:
% objfun(x) = 1.2 .^ (0:n-1) .* abs(x); n = length(x)

% prob = ooAssign(@powers12, ones(60,1), 'name :powers12; MaxIter = 2500');
N = 60;
prob = ooAssign(@powers12 , ones(N,1), 'name :powers12; MaxIter =2500;df = @Gpowers12');
prob.doPlot = 1;
% prob.useScaling = 1;

%VERY STRICT!
prob.h = @(x) [1e+2*(-1/x(8)+0.8); 1e+3*(x(15)+0.15)]; 
prob.hPattern = sparse(N, 2);
prob.hPattern(8, 1)=1;
prob.hPattern(15, 2)=1;

%VERY STRICT!
prob.c = @(x) [1e+2*(cos(x(9))*sin(x(20))+cos(0.1)); 1e+5*(sin(x(16))+sin(0.55))]; 
prob.cPattern = sparse(N,2);
prob.cPattern(9,1)=1;
prob.cPattern(20,1)=1;
prob.cPattern(16,2)=1;


prob.lb(1:2) = 0.4;
prob.ub(3:4) = -0.3;

prob.lb([5 7]) = 0.2;
prob.ub([5 7]) = 0.2;

%A * x <= b
prob.A = sparse(2,60); 
prob.A(1,10)=1;
prob.A(2,11)=1;
prob.b = [-0.1 -0.11];

%Aeq * x = beq
prob.Aeq = sparse(2,60);
prob.Aeq(1, 12) = 1;
prob.Aeq(2, 13) = 1;
prob.beq = [-0.05 0.15];

prob.TolCon = 1e-3;
prob.TolFun = 1e-5;
% prob.doPlot = 0;
% prob.debug = 1;
prob.iterPrint=100;
% profile on
r = ooRun(prob, 'ralg')%#ok<NOPRT> %<-local non-smooth solver
% profile viewer


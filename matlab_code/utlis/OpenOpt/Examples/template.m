function r = myRoutineName(prob)
r = oor('param1:stringvalue1', 'param2:stringvalue2', 'paramN:stringvalueN');
% example:
% r = oor('authors: Dmitrey, openopt@ukr.net, icq 275976670', ...
%     'alg:Naum Z. Shor r-algorithm with adaptive space transformation',...
%     'lastChanger: Dmitrey', ...
%     'moreinfo:Ukrainian Science Academy, Instityte of cybernetics (www.icyb.kiev.ua), optimization department',...
%     'adverticement:for faster ralg fortran code or hand-turning ralg parameters contact openopt@ukr.net or Petro I. Stetsyuk stetsyuk@d120.icyb.kiev.ua');
% any params above could be ommited
% r will have structure
% r.param1 = value1
% ...
% r.paramN = valueN
% So, you can set it by youself without calling oor
% but it is hightly recommended for future compability

% in future OpenOpt ver the following parameters should be added 
% to r definition:
% r = oor(...,'lb+', 'ub+', 'A-', 'Aeq-', 'ceq+', 'c-', etc)
% sign "+" means parameter is required for the solver
% sign "-" means the solver can't handle this prob
% otherwise - doesn't matter
% or you can set it bu hands
% r.lb = '+';...

% for future implementation:    
% if prob.justCheck; return; end%only check is solver appropriate for the prob

%setting you default options
% myopt1 = val1;...
[alp h0 nh q1 q2] = ...
    deal(2.0, 1.0, 3, 1.0, 1.1);

%checking are any solver parameters is structure prob.(name of solver)
ExtractRoutineParamsFromProb;%script-file
%if present - they will replace defaults: 
% if prob.myRoutineName.nh exist => it will change the nh value


if prob.iterfcn(prob, r); return; end%maybe need stop already? 
%Solver body
while 1 % for iter = 1:prob.maxIter not needed, it is handled  in iterfcn

    %<ITER BODY>
    
    r.x = x in end of iter%if absend, r.xf will be used in iterfcn & should exist
    r.f = f in end of iter%if absend, r.ff will be used in iterfcn & should exist

    if(iter solution r.x better than stored solution r.xf)
        %usually r.f<r.xf, maximum probs are 
        %converting to minimum automatically
        r.xf = r.x;
        r.ff = r.f;
    end

    r = prob.iterfcn(prob, r);
    if r.istop || (other your stop criteria)
        return
    end%checks for some stop conditions
    %see ooIter for more details
end

% NB! optim value, stored in iterfcn for output, may not satisfy constraints (like in case of Lagrange multipliers)
% so it should be handled inside of routine better
%!Attantion!
% check that every exit point of your alg will return appropriate values of
% r.istop (check for some stop criteria in ooIter.m) r.xf & r.ff (that you didn't forget to ascribe optim values to thefields)
% because it's very hard to trace the routine, looking for such cases
% for those who will check/modify/update your routine

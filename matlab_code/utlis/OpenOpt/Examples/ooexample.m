% Some OpenOpt examples
% 
% =========================================================================
% how ooAssign() works: 
% 
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2', value2, ...)
% only 2 parameters of ooAssign required 
% 1st is objFun(string or handler)
% 2nd is x0
% if value2 is numeric, like 8.15 or [15 8 80], you can use
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2=value2', param3, value3, ...)
% 
% if value2 is string, like 'asdf' or 'maximum', you can use:
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2:asdf', param3, value3, ...)
% 
% if value2 is bool (true or false, i.e. 0 or 1), you can use
% prob = ooAssign(objFun, x0, 'param1', value1, 'param2+', param3, value3, ...)
% or
% ooAssign(objFun, x0, 'param1', value1, 'param2-', param3, value3, ...)
% '+' means true(1), '-' means false(0)

% you can use ';' delimiter inside ooAssign:
% ooAssign(objFun, x0, 'param1=value1; param2=value2;param3=value3; etc'
% for example 
% p = ooAssign(@sin, 8, 'goalType:maximum; JohnSmith.param1=transpose([1 2 3])');
% p.JohnSmith.param2 = [2 3 4; 4 5 6; 6 7 8];
% =========================================================================
% how ooRun works
% r = ooRun(prob (or cell of probs), solver(or cell of solvers))
% if any of 2 args is cell, then r is cell too, elseware r is struct
clc
% =========================================================================

%RosenbrockBanana
ooexample1 % see ooexample1.m

% powers12: objfun(x) = 1.2 .^ (0:n-1) .* abs(x); n = length(x)
ooexample2

%Hilbert: smooth, badly-scaled problem; start - zeros, solution - ones
ooexample3

%Lemarechal: convex, non-smooth problem
ooexample4
% =========================================================================


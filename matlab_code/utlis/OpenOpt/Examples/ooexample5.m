N=10;
probLemarechal = ooAssign(@Lemarechal, zeros(N,1), 'name', 'Lemarechal10', ...
    'iterPrint=25;df = @GLemarechal;useScaling-',...
    'MaxIter=1500; doPlot+');
probLemarechal = ooAssign(@Lemarechal, zeros(N,1), 'name', 'Lemarechal10', ...
    'iterPrint=25;useScaling-',...
    'MaxIter=1500; doPlot+');

%probLemarechal.parallel.f = 1;

% probLemarechal.varargin = {N};
%
% probLemarechal.check.df =1;
%probLemarechal.check.dc =1;
%probLemarechal.check.dh =1;
% probLemarechal.df = @GLemarechal;

probLemarechal.lb(1) = 0.025;
probLemarechal.lb(2) = 0.008;
probLemarechal.ub(3) = -0.009;
probLemarechal.ub(4) = -0.0003;
probLemarechal.Aeq = zeros(2,N);
probLemarechal.Aeq(1,5)=1;
probLemarechal.Aeq(2,6)=1;
probLemarechal.beq(1)=0.0478;
probLemarechal.beq(2)=-0.09;

probLemarechal.A = zeros(2,N);
probLemarechal.A(1,7)=1;
probLemarechal.b(1)=0.018;
probLemarechal.A(2,8)=1;
probLemarechal.b(2)=-0.021;
probLemarechal.c = @LemC;
probLemarechal.dc = @LemDC;


probLemarechal.h = @LemH;
probLemarechal.dh = @LemDH;
probLemarechal.graphics.typeX='cputime';
% probLemarechal.debug=1;
probLemarechal.TolCon = 1e-5;
probLemarechal.TolFun = 1e-5;
% probLemarechal.graphics.drawingInOneWindow = 0; 
% probLemarechal.advanced.cPenalty = 'norm2';
% probLemarechal.advanced.hPenalty = 'norm2';
rr = ooRun(probLemarechal, 'ralg'); %#ok<NOPRT>


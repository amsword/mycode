%RosenbrockBanana
prob = ooAssign(@RosenbrockBanana, [-1.2;-1], ...
    'iterPrint=1; MaxIter=1500; df=@GRosenbrockBanana');
%fields of prob can be also setled like this:
prob.name = 'Rosenbrock Banana';
prob.doPlot = 1;
prob.graphics.typeX = 'time';

r = ooRun(prob, 'ralg')%#ok<NOPRT> %<-local UC solver, uses x0

r2 = ooRun(prob, 'anneal')%#ok<NOPRT> %<-global UC solver, uses x0



prob.r0 = 3;
r3 = ooRun(prob, 'ShorEllipsoid')%#ok<NOPRT> %<-local, needs r0

r3_2 = ooRun(prob, 'buscarnd') %#ok<NOPRT> global
prob.lb = -[1; 1];
prob.ub = [0.99; 0.99];


prob.hPSO.merit = @ralg;
prob.hPSO.maxInnerIter = 100;
r3_3 = ooRun(prob, 'hPSO') %#ok<NOPRT>
r3_4 = ooRun(prob, 'GAconstrain') %#ok<NOPRT>

%Hilbert: smooth, badly-scaled problem; start - zeros, solution - ones
N=10;
probHilb = ooAssign(@MinusHilbert, zeros(N,1), ...
    'iterPrint=50; doPlot+; name: Hilb->Max; ralg.alp= 2.15; df= []',...
    'goalType:maximum;useScaling-;ScaleFactor=[1 1 1 1 1 1 1 1 1 1]', 'MaxIter=500');

probHilb.df = @GMinusHilbert;
probHilb.graphics.typeX = 'cputime';%case-unsensetive; see more cases in ooInit

r5 = ooRun(probHilb, 'ralg') %#ok<NOPRT>

%or
r6 = ooRun(probHilb, 'anneal')%#ok<NOPRT> %<-global UC solver, uses x0
%or
probHilb.r0 = norm(probHilb.x0)+0.1;%start - zeros, optim - ones
r7 = ooRun(probHilb, 'ShorEllipsoid')%#ok<NOPRT> %need prob.r0
%or examples with bounds
r8 = ooRun(probHilb, 'buscarnd')%#ok<NOPRT> %need r0

probHilb.x0 = 0.15+0.1*rand(size(probHilb.x0));
probHilb.lb = -1.3*ones(size(probHilb.x0));
probHilb.ub = 1.4*ones(size(probHilb.x0));


probHilb.hPSO.innerSolver = @ralg;
probHilb.hPSO.maxInnerIter = 100;
r10 = ooRun(probHilb, 'hPSO')%#ok<NOPRT> %<-global solver, requires maxInnerIter, uses lb, ub & inner UC solver, ralg or like that

%global solver GAconstrain can handle A, b (Ax<=b)
%but not very well
probHilb.A = ones(2,probHilb.n); probHilb.A(2,2)=2;
probHilb.b = [9.9; 11.5];

probHilb.c = @HilbC;
% description:
% function r = HilbC(x)
% r = 5-sum(x)-1e-4*norm(x)

r11 = ooRun(probHilb, 'GAconstrain')%#ok<NOPRT> % <- this solver don't use x0 info
%because of linear inequalities Ax<b solution differs from ones(n,1)

function dr = GLemarechal(x)
n = length(x);
persistent A b N
N=n;
if isempty(A) || any(size(A{1}) ~= [N,N]) || length(A)~=n

    
    for k=1:n
        A{k}=zeros(N,N);
        for j=1:N
            for i=1:j-1
                A{k}(i,j) = exp(i/j)*cos(i*j)*sin(k);
            end
        end
        A{k} = A{k} + A{k}';
        b{k} = zeros(1,N);
    end

    for k=1:n
        for i=1:N
            A{k}(i,i) = i*abs(sin(k))/10+sum(abs(A{k}(i,:)));
            b{k}(i) = exp(i/k)*sin(i*k);
        end
    end
end

vectVal = zeros(1,n);
for k=1:n
    vectVal(k) = x' * A{k} * x - b{k} * x;
end
indMax = find(vectVal == max(vectVal),1,'first');
try
dr = 2 * A{indMax} * x - b{indMax}';
catch
    keyboard
end

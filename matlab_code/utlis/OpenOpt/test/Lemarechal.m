function r = Lemarechal(x)

n = length(x);
% n=10;
% if nargin>1
%     n = prob.user.n;
% else
%     n = 10;
% end
x = x(:);
persistent A b N
N=n;
if isempty(A) || any(size(A{1}) ~= [N,N]) || length(A)~=n

    
    for k=1:n
        A{k}=zeros(N,N);
        for j=1:N
            for i=1:j-1
                A{k}(i,j) = exp(i/j)*cos(i*j)*sin(k);
            end
        end
        A{k} = A{k} + A{k}';
        b{k} = zeros(1,N);
    end

    for k=1:n
        for i=1:N
            A{k}(i,i) = i*abs(sin(k))/10+sum(abs(A{k}(i,:)));
            b{k}(i) = exp(i/k)*sin(i*k);
        end
    end
end

if ischar(x)
    x0 = ones(N,1);
    switch x
        case 'getStartPoint'
            r = x0; return
        case 'getOptimPoint'
            mm_opts = optimset(@fminimax);
            mm_opts.TolX = 1e-15;
            mm_opts.TolFun = 1e-15;
            mm_opts.TolCon = 1e-8;
            mm_opts.MaxFunEvals = 1e8;
            x_optim = fminimax(@getVectVal,x0,[],[],[],[],[],[],[],mm_opts, A, b);
            r = x_optim; return
    end
end

fval = zeros(n,1);
for k=1:n
    fval(k) = x' * A{k} * x - b{k} * x;
end
r = max(fval);


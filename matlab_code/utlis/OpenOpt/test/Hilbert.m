function r = Hilbert(x)
n = length(x);
% n=101;
x = x(:);


persistent A b
if isempty(b) || length(b) ~= n
    A = hilb(n);
    b = sum(A);
end
    
r = 0.5 * (x' * A * x) - b * x;

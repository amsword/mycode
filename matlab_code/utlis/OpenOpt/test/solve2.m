function r = solve2(x)
persistent A b
n = length(x);
if length(b)~=n
    A = pascal(n);
    b = A*ones(n,1);
end
r = 1e3 * (A*x-b);
r = r+1e-6*rand(size(r));


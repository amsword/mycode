function df = dSimpleFun(x)
df = [sign(x(1)); sign(x(2))];

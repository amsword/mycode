function c = LemC(x, varargin)
% c(1) = sin(x(9))-sin(0.013);
c(1) = x(9)^2 - 0.013^2;
c(2) = sin(x(10))-sin(-0.004);

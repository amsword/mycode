function dr = GHilbert(x)
persistent A b
n = length(x);
if isempty(b) || length(b) ~= n
    A = hilb(n);
    b = sum(A);
end
dr = A*x-b(:);

function c = rb_c(x)
c(1) = x(1) - 0.8;
c(2) = sin(x(2)) - sin(0.64);
% c(3) = x(1)+x(2) - 1.44;
c = c(:);

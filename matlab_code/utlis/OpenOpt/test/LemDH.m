function dh = LemDH(x, varargin)
dh = sparse(length(x), 2);

dh(9,1) = cos(x(9));
dh(10,2) = cos(x(10));

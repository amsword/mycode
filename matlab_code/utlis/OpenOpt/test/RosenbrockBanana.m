function r = RosenbrockBanana(x)

if ischar(x)
    switch x
        case 'getStartPoint'
            r = [-1.2; -1];% 1st call:  out = x0
            return
        case 'getOptimPoint'
            r = [1;1];
            return
    end
end
fval = 100*(x(2)-x(1)^2)^2+(1-x(1))^2;
r = fval;
% if nargout>1
%     dr = [-400*(x(2)-x(1)^2)*x(1)-2+2*x(1);200*x(2)-200*x(1)^2];
% end


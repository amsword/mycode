function dc = rb_dc(x)
% dc = [cos(x(1)) 0 1; 0   cos(x(2)) 1];
dc = [1 0; 0 cos(x(2))];

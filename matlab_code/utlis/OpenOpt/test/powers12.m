function r = powers12(x, p)
% n = prob.n;
n=length(x);
x = x(:);
% if ischar(x)
%     x0 = ones(n,1);
%     switch x
%         case 'getStartPoint'
%             r = x0; return
%         case 'getOptimPoint'
%             r = zeros(n,1); return
%     end
% end
r = sum(1.2 .^ (0:n-1)' .* abs(x));
% 
% if nargout>1
%     dr = 1.2 .^ (0:n-1)' .* sign(x);
% end

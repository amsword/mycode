function df = gpowers2(x, varargin)
global funcount
if isempty(funcount); funcount=0;end
funcount = funcount+1;
n = length(x);
df = 1.2 .^ (0:n-1)' .* sign(x);

function dr = GRosenbrockBanana(x)
dr = [-400*(x(2)-x(1)^2)*x(1)-2+2*x(1);200*x(2)-200*x(1)^2];

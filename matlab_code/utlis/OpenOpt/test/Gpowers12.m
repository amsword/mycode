function df = Gpowers12(x)
n = length(x);
df = 1.2 .^ (0:n-1)' .* sign(x);

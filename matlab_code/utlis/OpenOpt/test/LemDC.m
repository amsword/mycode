function dc = LemDC(x, varargin)
dc = sparse(length(x), 2);

dc(9,1) = 2*x(9);
dc(10,2) = cos(x(10));

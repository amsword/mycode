function F = fsolve_fun1(x)
F = [2*x(1) - x(2) - exp(-x(1));
      -x(1) + 2*x(2) - exp(-x(2))];
F = sum(abs(F));

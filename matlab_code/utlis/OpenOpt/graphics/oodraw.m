function oodraw(prob, r)
global globstat
persistent NpointsPlotted currMin lastTask colors specifiers

% creating new figure, if it must be done
isNewFigure = (~isempty(lastTask) && ~isequal(lastTask, {prob.name; prob.primal.fName})) ...
    || (~globstat.graphics.drawingInOneWindow && globstat.nIter ==1)...
    || ~isfield(globstat.graphics, 'figureHandler')...
    || (isequal(prob.env, 'matlab') && ~ishandle(globstat.graphics.figureHandler));
%|| (isequal(prob.env, 'octave') && ~ishandle(globstat.graphics.figureHandler))
lastTask={prob.name; prob.primal.fName}; % x0 can be absent

if isNewFigure
    colors = {'b' 'k' 'c' 'r' 'g'};
    switch prob.env
        case 'matlab'
            specifiers = {'-' ':' '-.' '--'};
        case 'octave'
            specifiers = {'-' '.' '@' '-@' '^'};
    end
    globstat.graphics.figureHandler = figure;
    globstat.graphics.colorCount = 0;
    globstat.graphics.specifierCount = 1;
    globstat.graphics.nTrajectories = 0;
    globstat.graphics.ghandlers = [];
    globstat.graphics.solverNames = [];
    hold on
    grid on
end


%creating new trajectory, if needed
isNewTrajectory = globstat.nIter == 1;
if isNewTrajectory
    globstat.graphics.colorCount = globstat.graphics.colorCount + 1;
    if globstat.graphics.drawingInOneWindow 
        if globstat.graphics.colorCount > length(colors)
            globstat.graphics.colorCount = 1;
            globstat.graphics.specifierCount = globstat.graphics.specifierCount + 1;
            if globstat.graphics.specifierCount > length(specifiers)
                prob.warn('line types number exeeded')
                globstat.graphics.specifierCount = 1;
            end
        end
    end
end

switch prob.env
    case 'matlab'
        h=get(globstat.graphics.figureHandler,'CurrentAxes');
    case 'octave'
        h={};
end

%setting color & specifier
color = colors{globstat.graphics.colorCount};
specifier = specifiers{globstat.graphics.specifierCount};

%setting xlabel, ylabel, title etc
if isNewTrajectory
    globstat.graphics.nTrajectories = globstat.graphics.nTrajectories+1;
    [v v_str] = oover;
    switch prob.env
        case 'matlab'
            set(globstat.graphics.figureHandler, 'Name', ['OpenOpt v.' v_str '  http://sourceforge.net/projects/openopt' ])
            title(h, {['problem: ' prob.name '     solver: ' prob.solverName]})
            ylabel(h, 'objFun(x)');
        case 'octave'
            title(['problem: ' prob.name '     solver: ' prob.solverName])
            ylabel('objFun(x)');
            %TODO: howto set figure Name?
    end

    switch lower(prob.graphics.typeX)
        case 'cputime'
            xlabel('CPU Time elapsed (without graphic output), sec');
        case 'time'
            xlabel('Time elapsed (without graphic output), sec');
        case 'niter'
            xlabel('nIter');
        otherwise
            prob.err('unknown typeX graphic output')
    end
    %     if isfinite(prob.maxIter); xlim([0 prob.maxIter]);
    %     else xlim([0 1e3]); end
    %     globstat.graphics.ylimprev = [min([prob.lowerBoundForPlotEstim; f]) max(f)+.15*abs(max(f))];
    %     ylim(globstat.graphics.ylimprev);
    % ylimnew = [min([globstat.graphics.ylimprev(1); f]) max([globstat.graphics.ylimprev(2); f+.15*abs(max(f))])];
    % if any(globstat.graphics.ylimprev~=ylimnew)
    % %     ylim(ylimnew);
    % end
    % globstat.graphics.ylimprev=ylimnew;
    NpointsPlotted = 0;
end

%%%%%%%%%%%%%%%% getting data to plot %%%%%%%%%%%%%%%
if NpointsPlotted>0; IND = NpointsPlotted:globstat.nIter;
else IND=1; end
yy = globstat.iterFvals(IND);
if prob.graphics.plotOnlyCurrentMinimum
    for j=IND%todo: min is slow in 1x1 comparison vs if-then-else
        yy(j) = min(currMin, globstat.iterFvals(j));
        currMin = min(currMin, yy(j));
    end
end
switch lower(prob.graphics.typeX)
    case 'niter'
        xx = IND-1;
    case 'cputime'
        xx = globstat.iterCPUTime(IND);
    case 'time'
        xx = globstat.iterTime(IND);
    otherwise
        prob.err('unknown labelX case')
end
if strcmp(prob.goalType, 'maximum'); yy = -yy; end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isNewTrajectory && globstat.isFinished
    switch prob.env
        case 'matlab'
            if ~isempty(globstat.iterFvals); globstat.graphics.ghandlers(globstat.graphics.nTrajectories) = plot(h, xx, yy,   specifier, 'Color', color, 'MarkerSize', prob.graphics.markerSize); end
            globstat.graphics.solverNames{globstat.graphics.nTrajectories} = prob.solverName;
            legend(globstat.graphics.ghandlers, globstat.graphics.solverNames);
            plot(h, xx(1), yy(1), [specifier 'd'], 'Color', color, 'MarkerSize', prob.graphics.markerSize)
            plot(h, xx(end), yy(end), specifier, 'Color', color);
        case 'octave'
            %if ~isempty(globstat.iterFvals); %globstat.graphics.ghandlers(globstat.graphics.nTrajectories) =
            %plot(xx, yy, [specifier color ';;']); end
            %globstat.graphics.solverNames{globstat.graphics.nTrajectories} = prob.solverName;
            %legend(globstat.graphics.ghandlers, globstat.graphics.solverNames);
            %plot(h, xx(1), yy(1), [specifier 'd'], 'Color', color, 'MarkerSize', prob.graphics.markerSize)
            %plot(h, xx(end), yy(end), specifier, 'Color', color);
    end
elseif isNewTrajectory
    globstat.graphics.solverNames{globstat.graphics.nTrajectories} = prob.solverName;
    switch prob.env
        case 'matlab'
            if ~isempty(globstat.iterFvals); globstat.graphics.ghandlers(globstat.graphics.nTrajectories) = plot(h, xx, yy, specifier, 'Color', color, 'MarkerSize', prob.graphics.markerSize); end
            legend(globstat.graphics.ghandlers, globstat.graphics.solverNames);
            plot(h, xx, yy, [specifier 'd'], 'Color', color, 'MarkerSize', prob.graphics.markerSize)
        case 'octave'
            plot(xx, yy, [specifier color ';' prob.solverName ';'])
    end
else
    switch prob.env
        case'matlab'
            plot(h, xx, yy, specifier, 'Color', color);
        case 'octave'
            plot(xx, yy, [specifier color ';;']);
    end
end
if globstat.isFinished
    if ~r.istop; s = 'o';%(circle) - undefined
    elseif r.istop<0; s = 'v';%solver reports he failed to solve the prob OK
    else s = 'p';%solver reports he solved the prob OK
    end
    if s == 'p' && ~r.isFeasible; s = 's'; end %square - contact solver developer
    if ~stopcase(r); s = '>'; end % MaxTime, MaxCPUTime, MaxIter, MaxFunEvals or etc is exeeded
    switch prob.env
        case 'matlab'
            plot(h, xx(end), yy(end), [specifier s], 'Color', color, 'MarkerSize', prob.graphics.markerSize);
        case 'octave'
            plot(xx(end), yy(end), [color ';;']);
    end
end

NpointsPlotted = globstat.nIter;
drawnow


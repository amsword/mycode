function data = create_data(MODE, operand1, operand2, operand3)
%% Peekaboom is slightly different, part of the dataset is used for training, while the rest is database for query.
% i.e., training use Xtrain, query use Xbase
%%

data.MODE = MODE;


 if (strcmp(MODE, 'euc-peekaboom'))
  fprintf('Creating %s dataset ... ', MODE);

  % Create the peekaboom dataset.
  load('data/Peekaboom_gist', 'gist', 'ndxtest');

  % data-points are stored in columns
  X = double(gist');
  nImages = size(gist, 1);
  nTest = 2000;
  nTrain = 15000; % 8000 is for BRE
  nBase = nImages;
  clear gist;
  
  % center, then normalize data
  gist_mean = mean(X, 2);
  X = bsxfun(@minus, X, gist_mean);
  normX = sqrt(sum(X.^2, 1));
  X = bsxfun(@rdivide, X, normX);
  
  ndxtest = randi([1, nImages], 1, nTest);
  ndxtrain = randi([1, nImages], 1, nTrain);
  ndxtrain = setdiff(ndxtrain, ndxtest);
  Xtraining = X(:, ndxtrain);
  Xtest = X(:, ndxtest);
  Xbase = X;
  clear X;
  
  data = construct_peekaboom_data(Xtraining, Xtest, Xbase, [numel(ndxtrain), numel(ndxtest), nBase], operand1, operand2, data);
  % see bottom for construct_data(...)
  data.gist_mean = gist_mean;
  data.ndxtest = ndxtest;
  data.ndxtrain = ndxtrain;
  
else
  error('The given mode is not recognized.\n');
end

fprintf('done\n');


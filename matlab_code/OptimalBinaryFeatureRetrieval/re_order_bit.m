function y = re_order_bit(ubits, W)

[rows, cols] = size(ubits);

expand = zeros(rows * 8, cols);

for i = 1 : rows
    for j = 1 : 8
        tmp = bitget(ubits(i, :), j);
        expand((i - 1) * 8 + j, :) = tmp;
    end
end

y = W * expand > 0.5;
y = compactbit(y);
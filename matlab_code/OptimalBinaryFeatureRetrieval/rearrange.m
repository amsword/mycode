function [best_idx, worst_idx] =  rearrange(codes, subbands)
%% codes are the original codes' entropy,
% subbands are the number of subbands
% best_idx and worst_idx are the best nd worst codes selection's idx

%%
m=length(codes);
s = subbands;
d = m/s;
best_idx = zeros(s, d);
worst_idx = zeros(s,d);
codes_bak = codes;
fprintf('Ent:\n');
fprintf('%d, ', codes);
fprintf('\nMeanEnt: %d\n', mean(codes));

%% Best case
meanEnt = mean(codes);
bias = codes - meanEnt;
biasSqrt = bias.^2;
[~, idx_biasSqrt] = sort(biasSqrt, 'descend');

for i=1:subbands
    best_idx(i, 1) = idx_biasSqrt(i);
    codes(idx_biasSqrt(i))=10;
end

for j=2:d
    for i=1:subbands
        curMean = (sum(codes_bak( best_idx(i, 1:j-1))) + codes)/j;
        [~, idx] = min((curMean - meanEnt) .^2);
        best_idx(i, j) = idx;
        codes(idx)=10;
    end
end

best_idx = reshape(best_idx', 1, m);
best = codes_bak(best_idx);
lsq=0;
for i=1:s
    lsq = lsq+ (mean(best((i-1)*d+1:i*d))- meanEnt).^2;
end
fprintf('Best Case:\nBest Index:');
fprintf('%d, ', best_idx);
fprintf('\nBest Ent:');
fprintf('%d, ', best);
fprintf('\nLeast Square Error: %d\n', lsq);

%% Worst case
[~, worst_idx] = sort(codes_bak);
worst = codes_bak(worst_idx);
lsq=0;
for i=1:s
    lsq = lsq+ (mean(worst((i-1)*d+1:i*d))- meanEnt).^2;
end
fprintf('\nWorst Case:\nWorst Index:');
fprintf('%d, ', worst_idx);
fprintf('\nWorst Ent:');
fprintf('%d, ', worst);
fprintf('\nLeast Square Error: %d\n', lsq);




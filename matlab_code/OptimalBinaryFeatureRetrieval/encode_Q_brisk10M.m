% function encode_B_brisk10M(data_file, save_file)

data_file = '_512_Q.uint8';

Q = read_mat(data_file, 'uint8');


if m ~= 512
    rng(0);
    rp = randperm(512, m);

    rb = zeros(m, size(Q, 2), 'uint8');
    
    for i = 1 : m
        idx_byte = floor((rp(i) - 1) / 8) + 1;
        idx_bit = mod((rp(i) - 1), 8) + 1;
        
        subB = Q(idx_byte, :);
        bits = bitget(subB, idx_bit);
        rb(i, :) = bits;
    end
    Q = compactbit(rb);
    save_mat(Q, query_file, 'uint8');
end
function Aeq_column = generate_Aeq_column(code_length)

all_idx = cell(code_length, 1);
for t = 1 : code_length
   rows = zeros(1, code_length);
   rows(:) = t;
   all_idx{t}.rows = rows;
   all_idx{t}.cols = ((t - 1) * code_length + 1) : (t * code_length);
end

total_length = code_length * code_length;

rows = zeros(total_length, 1);
cols = zeros(total_length, 1);

for t = 1 : code_length
    idx_start = (t - 1) * code_length + 1;
    idx_end = t * code_length;
    
    rows(idx_start : idx_end) = all_idx{t}.rows;
    cols(idx_start : idx_end) = all_idx{t}.cols;
end

Aeq_column = sparse(rows, cols, 1, code_length, code_length ^ 2);



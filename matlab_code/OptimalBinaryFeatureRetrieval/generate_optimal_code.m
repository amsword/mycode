function [q, exitflag, output] = generate_optimal_code(entropy_column, band_num)
% rng(0);
% entropy_column = rand(10, 1);
% entropy_column = [0.1 : 0.1 : 1]';
% band_num = 5;

code_length = numel(entropy_column);
band_length = code_length / band_num;
%% generate target value
target_column = generate_target_value(entropy_column, band_num);

%% generate the matrix in the objective function
C = generate_matrix_obj(entropy_column, band_num);

%% generate Aeq, beq;
Aeq_column = generate_Aeq_column(code_length);
Aeq_rows = generate_Aeq_row(code_length); 
Aeq = [Aeq_column; Aeq_rows];

%% generate beq;
beq = generate_beq(code_length);

%%
options = cplexoptimset('MaxIter',2,'Display','iter', 'MaxTime', 3600);
x0 = eye(code_length, code_length);
x0 = x0(:);
[x,resnorm,residual,exitflag,output] = cplexlsqbilp(C, target_column, [], [], Aeq, beq, [], [], x0, options);
q = reshape(x, code_length, code_length);
% entropy_column
% q * entropy_column

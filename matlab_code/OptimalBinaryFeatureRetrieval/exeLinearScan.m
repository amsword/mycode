function cmd = exeLinearScan(input_file, ...
    data_base_file, out_file, Q, N, K, R, is_disk, is_run)

cmd = 'H:\codes2\VSProject2\x64\Release\mih1.exe';

cmd = [cmd ' linear_scan'];
if is_disk
    cmd = [cmd '_disk'];
end
cmd = [cmd ' ' input_file];
cmd = [cmd ' ' data_base_file];
cmd = [cmd ' ' out_file];


if R > -1
    cmd = [cmd ' ' '-R ' num2str(R)];
end

cmd = [cmd ' -Q ' num2str(Q)];

cmd = [cmd ' -N ' num2str(N)];

cmd = [cmd ' ' '-K ' num2str(K)];

if is_run
    system(cmd);
end
function y = read_mat_uint8(file_name, type)

fp = fopen(file_name, 'r');
m = fread(fp, 1, 'int32');
n = fread(fp, 1, 'int32');

batch_size = 1000000;
batch_num = ceil(m / batch_size);

y = zeros(n, m, 'uint8');
for i = 1 : batch_num
    idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    idx_end = min(idx_end, m);
    
    y(:, idx_start : idx_end) = ...
        uint8(fread(fp, [n,idx_end - idx_start + 1], type));
end

fclose(fp);
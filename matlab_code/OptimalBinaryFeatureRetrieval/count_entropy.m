function [y, z] = count_entropy(ubits)

[rows, cols] = size(ubits);

y = zeros(rows * 8, 1);
z = zeros(rows * 8, 1);

for i = 1 : rows
    for j = 1 : 8
        tmp = bitget(ubits(i, :), j);
        x1 = sum(tmp == 0);
        x2 = cols - x1;
        x1 = x1 / cols;
        x2 = x2 / cols;
        en = -x1 * log(x1) - x2 * log(x2);
        en = en / log(2);
        
        
        y((i - 1) * 8 + j, :) = en;
        z((i - 1) * 8 + j, :) = x1;
    end
end
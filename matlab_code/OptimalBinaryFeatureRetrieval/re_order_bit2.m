function y = re_order_bit2(ubits, idx)

[rows, cols] = size(ubits);

expand = zeros(rows * 8, cols, 'uint8');

for i = 1 : rows
    for j = 1 : 8
        tmp = bitget(ubits(i, :), j);
        expand((i - 1) * 8 + j, :) = tmp;
    end
end

y = uint8(expand(idx, :));
y = compactbit(y);
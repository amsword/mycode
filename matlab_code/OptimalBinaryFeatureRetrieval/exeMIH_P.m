function cmd = exeMIH_P(input_file, ...
    data_base_file, out_file, Q, N, K, R, m, is_run)

cmd = 'H:\codes2\VSProject2\x64\Release\mih7.exe';

cmd = [cmd ' mih_p'];
cmd = [cmd ' ' input_file];
cmd = [cmd ' ' data_base_file];
cmd = [cmd ' ' out_file];

if R >= 0
    cmd = [cmd ' ' '-R ' num2str(R)];
end

cmd = [cmd ' ' '-K ' num2str(K)];


cmd = [cmd ' -m ' num2str(m)];
cmd = [cmd ' -Q ' num2str(Q)];
cmd = [cmd ' -N ' num2str(N)];

if is_run
    system(cmd);
end
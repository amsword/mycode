function report_info(r)

% fprintf('\n');

% str = ['time cost: ' num2str(r.cput)];

str = [num2str(r.cput)];

if isfield(r, 'stat')
%     s = r.stat(end, 1);
%     str = [str '(' num2str(s) ')'];
     s = mean(r.stat(2, :));
     str = [str '(' num2str(s) ')'];

    if abs(mean(r.stat(end, :)) - r.cput) > 10^-5
        error('df');
    end
end

fprintf('%s\t', str);

count = sum(r.res ~= 0, 1);

str = ['avg point: ' num2str(sum(count) / size(r.res, 2))];
fprintf('%s\n', str);

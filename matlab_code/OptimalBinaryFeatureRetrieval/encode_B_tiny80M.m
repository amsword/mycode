% error('check the mean-removed parameters, not correct');
idx_start = 1001;
idx_end = 79302017;
Nbase = idx_end - idx_start + 1;
batch_size = 1000000;
batch_num = ceil(Nbase / batch_size);

for i = 1 : batch_num
    batch_start = (i - 1) * batch_size + idx_start;
    batch_end = i * batch_size + idx_start - 1;
    
    batch_end = min(Nbase, batch_end);
    
    [num2str(i) '/' num2str(batch_num) ': ' num2str(batch_start) '/' num2str(batch_end)]
    
    Xbase = read_tiny_gist_binary(...
        batch_start : batch_end);
    Xbase = double(Xbase);
    
    codes_bool = W' * [Xbase; ones(1, size(Xbase, 2))] > 0;
    codes_bit = compactbit(codes_bool);
    
    if i == 1
        B = zeros(size(codes_bit, 1), Nbase, 'uint8');
    end
    B(:, batch_start - idx_start + 1 : ...
        batch_end - idx_start + 1) = codes_bit;
end
%     save(query_file, 'B', '-v7.3');
save_mat(B, data_base_file, 'uint8');
% clear codes_bit B;
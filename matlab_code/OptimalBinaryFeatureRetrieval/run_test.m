NQ = 100;
N = 10^6;
K = 1000;
R = 0;
m = 2;

exeLinearScan('H:\codes2\VSProject2\data\siftCodeCompactSmall3.mat', ...
    'H:\codes2\VSProject2\data\siftResults.mat', ...
    NQ, N, K, R);

x = load('H:\codes2\VSProject2\data\siftResults.mat');
x.linscan.cput

%
exeMIH('H:\codes2\VSProject2\data\siftCodeCompactSmall3.mat', ...
    'H:\codes2\VSProject2\data\siftResults_mih.mat', ...
    NQ, N, K, R, m);
x = load('H:\codes2\VSProject2\data\siftResults_mih.mat');
x.ret.cput
ret = x.ret;

%%
exeMIH_P('H:\codes2\VSProject2\data\siftCodeCompactSmall3.mat', ...
    'H:\codes2\VSProject2\data\siftResults_mih_p.mat', ...
    NQ, N, K, R, m);
x = load('H:\codes2\VSProject2\data\siftResults_mih_p.mat');
x.ret.cput
ret = x.ret;


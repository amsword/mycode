function beq = generate_beq(code_length)

beq = zeros(2 * code_length, 1);
beq(:) = 1;
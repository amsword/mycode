% case_all;
cd('H:\codes2\HashCode');
jf_conf;
clear;

%
m = 64;
type = 'SIFT1B'; %% Tiny80M, SIFT1B, BRISK10M, BToyRetrieval
file_pre_date = '';
jf_compare;
working_dir = [gl_data_parent_folder type '\working_mihm\'];

str_base = src_file.c_train;
str_query = src_file.c_test;

is_lsh = 0;
is_mlh = 1;

if strcmp(type, 'BRISK10M')
    is_lsh = 0;
    is_mlh = 0;
end

if is_lsh
    hash_func_key_word = 'LSH'
elseif is_mlh
    hash_func_key_word = 'MLH'
else
    hash_func_key_word = [];
    'no hash function'
end

query_file_prefix = [working_dir hash_func_key_word '_'];
data_base_file_prefix = query_file_prefix;

out_file_pre_linscan = [working_dir hash_func_key_word '_'];
out_file_pre_mih = [working_dir hash_func_key_word '_'];
out_file_pre_mih_p = [working_dir hash_func_key_word '_'];

query_file_prefix = [query_file_prefix num2str(m) '_Q.uint8'];
data_base_file_prefix = [data_base_file_prefix num2str(m) '_B.uint8'];

out_file_pre_linscan = [out_file_pre_linscan num2str(m)];
out_file_pre_mih = [out_file_pre_mih num2str(m)];
out_file_pre_mih_p = [out_file_pre_mih_p num2str(m)];

cd(working_dir)

%% get the binary codes
gen_binary_code;

%% linear scan
NQ = 1000;
for N = [1 : 9] * 10^8
% for is_opt = [1 -1]
for is_opt = 0

if is_opt == 0
    query_file = query_file_prefix;
    data_base_file = data_base_file_prefix;
else
    query_file = [query_file_prefix '_' num2str(is_opt)];
    data_base_file = [data_base_file_prefix '_' num2str(is_opt)];
end

if strcmp(type, 'BRISK10M')
    N = min(N, 10^7)
elseif strcmp(type, 'Tiny80M')
    N = min(N, 79302017 - 1000)
end

K = 1000;
% for R = 3
for R = [0 1 2 3 4]
num_hash_tables = 2;
is_run = 0;
% for is_disk = 1
for is_disk = [0]
     suff_linear = ['_' num2str(N / 1000000) '_' ...
        num2str(R) ...
        '_' num2str(K) '.mat'];
    
    suff = ['_' num2str(N / 1000000) '_' ...
        num2str(R) ...
        '_' num2str(num_hash_tables) ...
        '_' num2str(K) '.mat'];
    
    if is_opt == 0
        suff_omi = suff;
    else
        suff_omi = ['_' num2str(N / 1000000) '_' ...
            num2str(R) ...
            '_' num2str(num_hash_tables) ...
            '_' num2str(K) '_' num2str(is_opt) '.mat'];
    end
    
    out_file_linscan = [out_file_pre_linscan '_linscan_' ...
        num2str(is_disk) ...
        suff_linear];
    
    out_file_mih = [out_file_pre_mih '_mih_' ...
        num2str(is_disk) ...
        suff];
    
    out_file_mih_p = [out_file_pre_mih_p '_mih_p' suff_omi];
    %
    if ~is_opt
        cmd_lin = exeLinearScan(query_file, ...
            data_base_file, ...
            out_file_linscan, NQ, N, K, R, is_disk, is_run);
        %
        cmd_mih = exeMIH(query_file, ...
            data_base_file, ...
            out_file_mih, ...
            NQ, N, K, R, num_hash_tables, is_disk, is_run);
    end
    %
    
    if ~is_disk
        cmd_mih_p = exeMIH_P(query_file, ...
            data_base_file, ...
            out_file_mih_p, ...
            NQ, N, K, R, ...
            num_hash_tables, is_run);
    end
%
% x = load(out_file_linscan);
% report_info(x.linscan);
% 
% x = load(out_file_mih);
% report_info(x.ret);
% 
% x = load(out_file_mih_p);
% report_info(x.ret);
fprintf('\n');
end
end
end
end

%% MIH
B = read_mat_uint8(data_base_file_prefix, 'uint8');

[en_col pro] = count_entropy(B);
num_hash_tables = 2;
%
[best_idx, worst_idx] = rearrange(en_col, 2);

best_B = re_order_bit2(B, best_idx);
save_mat(best_B, [data_base_file_prefix '_1'], 'uint8');

worst_B = re_order_bit2(B, worst_idx);
save_mat(worst_B, [data_base_file_prefix '_-1'], 'uint8');
%
Q = read_mat_uint8(query_file_prefix, 'uint8');

best_Q = re_order_bit2(Q, best_idx);
save_mat(best_Q, [query_file_prefix '_1'], 'uint8');

worst_Q = re_order_bit2(Q, worst_idx);
save_mat(worst_Q, [query_file_prefix '_-1'], 'uint8');

save('dimension_selection.mat', 'en_col', 'pro');
%%
tic;
[permutation_matrix, exitflag, output] = ...
    generate_optimal_code(en_col(1 : 32), num_hash_tables);
toc;
%%
re_order_bit;

%%
s = 32;
y = (2 .^(512 ./ s)) .* s / 1024^3



%% get training data

if is_lsh || is_mlh
    if strcmp(type, 'Tiny80M')
        Xtraining = read_tiny_gist_binary(...
            1001 : (1001 + 100000 - 1));
    elseif strcmp(type, 'SIFT1B')
        Xtraining = bvecs_read('../data/learn.bvecs', [1, 100000]);
        Xtraining = double(Xtraining);
    end
    
    mean_train = mean(Xtraining, 2);
    Xtraining = bsxfun(@minus, Xtraining, mean_train);
else
    'no training data'
end

%% hash functions
rng(0);
if is_lsh
    [W para_lsh] = jf_train_lsh(Xtraining, m);
    tmp_b = W(end, :)' - W(1 : end - 1, :)' * mean(Xtraining, 2);
    W(end, :) = tmp_b;
    
    clear Xtraining
elseif is_mlh
    s = sqrt(mean(sum(Xtraining .^ 2, 1)));
    Xtraining = Xtraining ./ s;
    
%     save_mat(Xtraining, src_file.c_train, 'double');
    
% exeExaustiveKNN(src_file.c_train, ...
%     src_file.c_train, ...
%     gnd_file.SortedPartTrainTrain, 1000);
    
    mlh_Strainingtraining = read_classification_gnd(gnd_file.SortedPartTrainTrain, 400);
    mlh_Strainingtraining = mlh_Strainingtraining{1};
    
    [W_mlh] = jf_train_mlh(Xtraining, ...
        mlh_Strainingtraining,...
        m);
    W = W_mlh';
    W(1 : end - 1, :) = W(1 : end - 1, :) / s;
    
    W(end, :) = W(end, :) - mean_train' * W(1 : end - 1, :);
    save(save_file.train_mlh, 'W');
else
    'no hash function is needed'
end


%% encode B
if strcmp(type, 'Labelme')
    encode_B_labelme;
elseif strcmp(type, 'Tiny80M')
    load(save_file.train_mlh, 'W');
    encode_B_tiny80M;
elseif strcmp(type, 'SIFT1B')
    load(save_file.train_mlh, 'W');
    encode_B_sift1b;
elseif strcmp(type, 'BRISK10M')
        encode_B_brisk10M;
else
    error('error type');
end

%% encode Q
if strcmp(type, 'Labelme')
    Xtest = read_mat(str_query, 'double');
    codes_bool = W' * [Xtest; ones(1, size(Xtest, 2))] > 0;
    Q = compactbit(codes_bool);
    save(query_file, 'Q', '-v7.3', '-append');
    clear codes_bit B;
elseif strcmp(type, 'Tiny80M')
    Xtest = read_tiny_gist_binary(...
        1 : 1000);
    Xtest = double(Xtest);
    codes_bool = W' * [Xtest; ones(1, size(Xtest, 2))] > 0;
    Q = compactbit(codes_bool);
    save_mat(Q, query_file, 'uint8');
%     clear codes_bit Q;
elseif strcmp(type, 'SIFT1B')
    Xtest = bvecs_read('../data/queries.bvecs');
    Xtest = double(Xtest);
    codes_bool = W' * [Xtest; ones(1, size(Xtest, 2))] > 0;
    Q = compactbit(codes_bool);
%     save(query_file, 'Q', '-v7.3', '-append');
    save_mat(Q, query_file, 'uint8');
    clear codes_bit Q;
elseif strcmp(type, 'BRISK10M')
    encode_Q_brisk10M;
else
    error('dfd');
end
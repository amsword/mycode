function C = generate_matrix_obj(entropy_column, band_num)

code_length = numel(entropy_column);
band_length = code_length / band_num;

all_non_zeros = cell(band_num, 1);
for t = 1 : band_num
    rows = repmat([(t - 1) * band_length + 1 : t * band_length]', 1, code_length);
    cols = repmat([1 : code_length], band_length, 1);
    vs = repmat(entropy_column', band_length, 1);
    
    all_non_zeros{t}.cols = sub2ind([code_length, code_length], ...
        rows(:), cols(:));
    all_non_zeros{t}.rows = zeros(code_length * band_length, 1);
    all_non_zeros{t}.rows(:) = t;
    all_non_zeros{t}.v = vs(:);
end


total_length = code_length * code_length;

num_non_zeros_each = band_length * code_length;
rows = zeros(total_length, 1);
cols = zeros(total_length, 1);
vs = zeros(total_length, 1);
for t = 1 : band_num
    idx_begin = (t - 1) * num_non_zeros_each + 1;
    idx_end = t * num_non_zeros_each;
    rows(idx_begin : idx_end) = all_non_zeros{t}.rows;
    cols(idx_begin : idx_end) = all_non_zeros{t}.cols;
    vs(idx_begin : idx_end) = all_non_zeros{t}.v(:);
end

C = sparse(rows, cols, vs, band_num, code_length ^ 2);
% function encode_B_brisk10M(data_file, save_file)

data_file = '_512_B.uint8';

B = read_mat(data_file, 'uint8');


if m ~= 512
    rng(0);
    rp = randperm(512, m);

    rb = zeros(m, size(B, 2), 'uint8');
    
    for i = 1 : m
        idx_byte = floor((rp(i) - 1) / 8) + 1;
        idx_bit = mod((rp(i) - 1), 8) + 1;
        
        subB = B(idx_byte, :);
        bits = bitget(subB, idx_bit);
        rb(i, :) = bits;
    end
    
    B = compactbit(rb);
    save_mat(B, data_base_file, 'uint8');
    
end
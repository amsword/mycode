function target_column = generate_target_value(entropy_column, band_num)

code_length = numel(entropy_column);
band_length = code_length / band_num;

target = mean(entropy_column) * band_length;
target_column = zeros(band_num, 1);
target_column(:) = target;
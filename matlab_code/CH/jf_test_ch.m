function eval = jf_test_ch(param, Xtest, Xbase, StestBase, topK, trueRank_fileseed)
%% Xbase is not the PCA one. Please notice
% eval.pre
% eval.rec
% eval.p_code
% eval.r_code
% eval.avg_retrieved_good = total_retrieved_good_pairs / Ntest;
% eval.avg_retrieved = total_retrieved_pairs / Ntest;
% eval.avg_good = total_good_pairs / Ntest;
eval = jf_eval_hash3(param.nbits, 'ch', param, Xtest, Xbase, StestBase, topK, trueRank_fileseed);
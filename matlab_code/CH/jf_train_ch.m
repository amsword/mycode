function CH = jf_train_ch(Xtraining, nb, table_num)

CHparam.nbits = nb;
CHparam.doMask = 0;
CHparam.eta = .1;
CHparam.L = table_num;
CHparam.c_num = 100;
CHparam.epsilon = .005;

CH = trainCH_ext(Xtraining', CHparam);
clear;
clc;
type = 'PeekaboomOrigin'; % LabelmeOrigin, PeekaboomOrigin

is_pca = 0;
all_topks = [1 10 100 200 400 50];
all_code_length = [16 24 32 48 64];

all_eval_lsh = cell(4, 1);
all_eval_mbq = cell(4, 1);
all_eval_itq = cell(4, 1);
idx = 1;
for m = all_code_length
    if strcmp(type, 'LabelmeOrigin')
        switch m
            case 64
                file_pre_date = '2012_5_20_8_20_5_';
            case 48
                file_pre_date = '2012_5_20_8_20_5_';
            case 32
                file_pre_date = '2012_5_6_14_34_1_';
            case 24
                file_pre_date = '2012_5_18_8_14_20_';
            case 16
                file_pre_date = '2012_5_18_7_54_56_';
        end
    elseif strcmp(type, 'PeekaboomOrigin')
        switch m
            case 64
                file_pre_date = '2012_5_20_8_20_5_';
            case 48
                file_pre_date = '2012_5_20_8_20_5_';
            case 32
                file_pre_date = '2012_5_6_14_34_1_';
            case 24
                file_pre_date = '2012_5_19_16_46_2_';
            case 16
                file_pre_date = '2012_5_19_16_45_33_';
        end
    end
    [save_file save_figure] = train_save_file2(type, m, is_pca, file_pre_date);
    
    eval = load(save_file.test_itq, 'eval_itq');
    all_eval_itq{idx} = eval.eval_itq;
    
    eval = load(save_file.test_mbq, 'eval_mbq');
    all_eval_mbq{idx} = eval.eval_mbq;
    
    eval = load(save_file.test_lsh, 'eval_lsh');
    all_eval_lsh{idx} = eval.eval_lsh;
    idx = idx + 1;
end
%%
all_topks = [1 10 100 200 400 50];
th = 1000;
for k = 1 : numel(all_topks)
    figure;
    y_itq = zeros(4, 1);
    for i = 1 : 4
        y_itq(i) = all_eval_itq{i}{k}.r_code(th);
    end
    plot([16, 24, 32, 64], y_itq, 'b');
    hold on;
    
    y_mbq = zeros(4, 1);
    for i = 1 : 4
        y_mbq(i) = all_eval_mbq{i}{k}.r_code(th);
    end
    plot([16, 24, 32, 64], y_mbq, 'r');
    
    y_lsh = zeros(4, 1);
    for i = 1 : 4
        y_lsh(i) = all_eval_lsh{i}{k}.r_code(th);
    end
    plot([16, 24, 32, 64], y_lsh, 'k');
        
end

pause;
close all;

%%
dir = '';
th = 100;
for k = 1 : numel(all_topks)
    Y = zeros(numel(all_code_length), 3);
    
    for m = 1 : numel(all_code_length)
        Y(m, 1) = all_eval_lsh{m}{k}.r_code(th);
        Y(m, 2) = all_eval_itq{m}{k}.r_code(th);
        Y(m, 3) = all_eval_mbq{m}{k}.r_code(th);
    end
    figure;
    bar(all_code_length, Y, 1);        
    legend('LSH', 'ITQ', 'MBQ', 'Location', 'NorthWest');
    grid on;
    xlabel('Code length', 'FontSize', 14);
    ylabel(['Recall@' num2str(th)], 'FontSize', 14);
    set(gca, 'FontSize', 14);
    
    saveas(gca, [dir 'lsh_itq_mbq_' type '_'  num2str(all_topks(k)) '_' num2str(th) '.eps'], 'psc2');
end

pause;
close all;



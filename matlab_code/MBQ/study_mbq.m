%
for k = 1 : 6
    figure
    plot(eval_mbq{k}.r_code, 'r');
    hold on;
    plot(eval_itq{k}.r_code, 'b');
    plot(eval_lsh{k}.r_code, 'k');
    xlim([0 2000]);
end

pause;
close all;

%%
curr_eval = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval{k} = eval_hash5(...
        size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), ...
        [], [], metric_info, eval_types);
end

%%
for i = 1 : numel(all_result)
    W = all_result{i}.W;
    curr_eval = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        curr_eval{k} = eval_hash5(...
            size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), ...
            [], [], metric_info, eval_types);
    end
    all_result{i}.eval = curr_eval;
end

%%
% load(save_file.test_itq, 'eval_itq');
for k = 1 : 6
    figure
    idx = 1;   
    
    
    plot(curr_eval{k}.r_code, 'r');
    hold on;
    
    hold on;
    plot(eval_itq{k}.r_code, 'b');
   
    xlim([0 1000]);
end

pause;
close all;
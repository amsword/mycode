function [W obj] = trainMBQ(Xtraining, m)
%% minimize binary quantization errors.

D = size(Xtraining, 1);
W = randn(D, m); % initialize

obj = zeros(50, 1);
for i = 1 : 50
    B = W' * Xtraining > 0;
    B = 2 * B - 1;
    [U, S, V] = svd(Xtraining * B');
    
    W = U(:, 1 : m) * V';
  
    diff = B - W' * Xtraining;
    obj(i) = trace(diff * diff');
end

W = [W; zeros(1, m)];
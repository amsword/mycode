function W = mbq(X, m)
% min |B - XW|_2, s.t. W' * W = I;

D = size(X, 2);
W = randn(D, m); % initialize
[U, ~, ~] = svd(W);
W = U(:, 1 : m);

for i = 1 : 50
    B = X * W > 0;
    B = 2 * B - 1;
    [U, ~, V] = svd(X' * B);
    
    W = U(:, 1 : m) * V';
end
function W = trainMBQ3(Xtraining, m)

D = size(Xtraining, 1);
N = size(Xtraining, 2);
W = randn(D, m); % initialize

coef = 1;
offset = 0;

Xtraining = Xtraining';

obj = zeros(50, 1);

% update W
for iter = 1 : 50
    for i = 1 : 5
        B = Xtraining * W > 0;
        B = 2 * B - 1;
        [U, S, V] = svd(Xtraining' * (coef * B + offset));
        
        W = U(:, 1 : m) * V';
    end

    B = Xtraining * W > 0;
    B = 2 * B - 1;
   
    % update coef
    tmp_a = trace(B' * B);
    tmp_b = trace(B' * (offset - Xtraining * W));
    coef = -tmp_b / tmp_a;
    
    % update offset
    tmp_b = trace(ones(m, N) * (coef * B - Xtraining * W));
    offset = -tmp_b / N;
end

W = [W; zeros(1, m)];
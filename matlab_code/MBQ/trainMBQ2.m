% function [W obj] = trainMBQ2(Xtraining, m)
%% minimize binary quantization errors.

all_length = [16 : 8 : 64];

[V D] = eigs(cov(Xtraining'), all_length(end)); 

all_result = cell(numel(all_length), 1);
for i = 1 : numel(all_length)
    W1 = V(:, 1 : all_length(i));
    X1 = Xtraining' * W1;
    W2 = mbq(X1, m);
    W = W1 * W2;
    W = [W; zeros(1, m)];
    all_result{i}.W = W;
end


function [para_out, Xtraining] = cal_kernel_para(Xtraining, anchor)

error('fsd');

Xtraining = sqdist(anchor, Xtraining);
epsilon = mean(Xtraining, 2);
Xtraining = bsxfun(@rdivide, Xtraining, -epsilon * 2);
Xtraining = exp(Xtraining);
offset = mean(Xtraining, 2);
Xtraining = bsxfun(@minus, Xtraining, offset);

para_out.anchor = anchor;
para_out.epsilon = epsilon;
para_out.offset = offset;
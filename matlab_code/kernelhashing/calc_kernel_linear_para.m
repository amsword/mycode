function [para_out, Xtraining] = calc_kernel_linear_para(Xtraining, anchor)

Origin = Xtraining;

Xtraining = sqdist(anchor, Xtraining);
epsilon = mean(Xtraining, 2);
Xtraining = bsxfun(@rdivide, Xtraining, -epsilon * 2);
Xtraining = exp(Xtraining);
offset = mean(Xtraining, 2);
Xtraining = bsxfun(@minus, Xtraining, offset);

am1 = norm(Origin(:));
am1 = am1 / sqrt(size(Origin, 1));

am2 = norm(Xtraining(:));
am2 = am2 / sqrt(size(Xtraining, 1));

scalar = am1 / am2;
Xtraining = Xtraining * scalar;
Xtraining = [Origin; Xtraining];

para_out.anchor = anchor;
para_out.epsilon = epsilon;
para_out.offset = offset;
para_out.scalar = scalar;

function [binary, unbinarized] = ksh_encoding(X, para)

KBase = sqdist(X, para.anchor);
KBase = exp(-KBase/(2*para.sigma));
KBase = bsxfun(@minus, KBase, para.mvec);
A1 = para.A1;
unbinarized = A1'*KBase';
binary = unbinarized > 0;
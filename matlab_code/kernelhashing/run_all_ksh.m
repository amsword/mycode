for type = {'labelme', 'peekaboom', 'sift_1m'}; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    % CIFAR10, labelme, sift_1m, peekaboom, GIST1M3, SIFT1M3
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);
    para = prepare_ksh(Xtraining, src_file.c_train);
    for m = [8, 16, 32, 64, 128, 256]
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        [para_out] = train_ksh(para, m);
        save(save_file.train_ksh, 'para_out', 'type');
        eval_ksh = cell(1, numel(all_topks));
        for k = 1 : numel(all_topks)
            eval_ksh{k} = eval_hash5(size(para_out.A1, 2), 'ksh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_ksh, 'eval_ksh', 'type');
    end
end
%
for type = {'SIFT1M3', 'GIST1M3'}; 
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    para = prepare_ksh(Xtraining, src_file.c_train);
    
    for m = [8, 16, 32, 64, 128, 256]
         [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
          [para_out] = train_ksh(para, m);
        save(save_file.train_ksh, 'para_out', 'type');
        eval_ksh = cell(1, numel(all_topks));
        for k = 1 : numel(all_topks)
            eval_ksh{k} = eval_hash5(size(para_out.A1, 2), 'ksh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_ksh, 'eval_ksh', 'type');
    end
end
function [para_out] = train_ksh(para, r)

traindata = para.subTrain;
anchor = para.anchor;
S0 = para.S0;

num_anchor = size(anchor, 2);
% traindata = para.subTrain;
trn = size(traindata, 2);
% label_index = randperm(N, trn);

KK = sqdist(traindata, anchor);
sigma = mean(mean(KK,2));
KK = exp(-KK / (2*sigma));
mvec = mean(KK);
KK = bsxfun(@minus, KK, mvec);

S = r * S0;

% projection optimization
RM = KK'*KK; 
A1 = zeros(num_anchor,r);
flag = zeros(1,r);
for rr = 1:r
    [rr]
    if rr > 1
        S = S-y*y';
    end
    
    LM = KK'*S*KK;
    [U,V] = eig(LM,RM);
    eigenvalue = diag(V)';
    [eigenvalue,order] = sort(eigenvalue,'descend');
    A1(:,rr) = U(:,order(1));
    tep = A1(:,rr)'*RM*A1(:,rr);
    A1(:,rr) = sqrt(trn/tep)*A1(:,rr);
    clear U;    
    clear V;
    clear eigenvalue; 
    clear order; 
    clear tep;  
    
    [get_vec, cost] = OptProjectionFast(KK, S, A1(:,rr), 500);
    y = double(KK*A1(:,rr)>0);
    ind = find(y <= 0);
    y(ind) = -1;
    clear ind;
    y1 = double(KK*get_vec>0);
    ind = find(y1 <= 0);
    y1(ind) = -1;
    clear ind;
    if y1'*S*y1 > y'*S*y
        flag(rr) = 1;
        A1(:,rr) = get_vec;
        y = y1;
    end
end

% W = [A1; zeros(1, r)];
para_out.A1 = A1;
para_out.anchor = anchor;
para_out.sigma = sigma;
para_out.mvec = mvec;
function X = kernel_transform(X, para)

anchor = para.anchor;
epsilon = para.epsilon;
offset = para.offset;
scalar = para.scalar;

Origin = X;
X = sqdist(anchor, X);
X = bsxfun(@rdivide, X, -epsilon * 2);
X = exp(X);
X = bsxfun(@minus, X, offset);

X = X * scalar;

X = [Origin; X];


for type = {'GIST1M3'}; 
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
%     Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
%     para = prepare_ksh(Xtraining, src_file.c_train);
    
    for m = [8, 16, 32, 64, 128, 256]
         [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
%           [para_out] = train_ksh(para, m);
%         save(save_file.train_ksh, 'para_out', 'type');
        x = load(save_file.train_ksh);
        para_out = x.para_out;
        x = load(save_file.test_ksh);
        eval_ksh = x.eval_ksh;
        if numel(eval_ksh) == 7
             error('dfjsdf');
            continue;
        end
%         eval_ksh = cell(1, numel(all_topks));
        for k = numel(all_topks) : numel(all_topks)
            eval_ksh{k} = eval_hash5(size(para_out.A1, 2), 'ksh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_ksh, 'eval_ksh', 'type');
    end
end
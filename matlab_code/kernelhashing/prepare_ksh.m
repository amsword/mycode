function para = prepare_ksh(Xtraining, str_c_train)

trn = 10000;

N = size(Xtraining, 2);
sample = randperm(N, 300);
anchor = Xtraining(:, sample');

label_index = randperm(N, trn);

subTrain = Xtraining(:, label_index);

S0 = sqdist(subTrain, subTrain);
S0 = sqrt(S0);
S0 = real(S0);
S0 = S0 / max(S0(:));
S0 = 1 - 2 * S0;

save_mat(subTrain, 'sub_train', 'double');
save_mat(label_index, 'label_index', 'int32');

k = 0.05 * N;
k = ceil(k);
exeExaustiveKNN('sub_train', str_c_train, 'nn', k);
s1 = load_gnd2('nn', k);
for j = 1 : numel(label_index)
    x = ismember(label_index, s1(:, j));
    S0(j, x) = 1;
    S0(x, j) = 1;
end

exeExaustiveKFarthest('sub_train', str_c_train, 'nn2', k);
s1 = load_gnd2('nn2', k);
for j = 1 : numel(label_index)
    x = ismember(label_index, s1(:, j));
    S0(j, x) = -1;
    S0(x, j) = -1;
end

para.anchor = anchor;
para.subTrain = subTrain;
para.S0 = S0;
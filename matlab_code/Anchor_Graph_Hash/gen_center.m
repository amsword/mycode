all_types = {'labelme', 'peekaboom', 'sift_1m', 'SIFT1M3', 'GIST1M3'};

global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

num_center = 1000;

for idx_type = 5 : 5
        idx_type
        type = all_types{idx_type};
        [src_file] = jf_src_data_file_name(type);

        Xtraining = read_mat(src_file.c_train, 'double');
        
%         C = vl_kmeans(Xtraining, 300);
        C = vl_kmeans(Xtraining, num_center);
        save_mat(C, [type '_cluster_center' num2str(num_center)], 'double');
end
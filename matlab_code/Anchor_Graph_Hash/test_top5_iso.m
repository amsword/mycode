[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
all_topks = [1 10 100 200 400 50, 5];

if is_isohash
    
    if (strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')) && m == 256
        return;
    end
    
    x = load(save_file.train_isohash_lp, 'W_isohash_lp');
    W_isohash_lp = x.W_isohash_lp;
    x = load(save_file.train_isohash_gf, 'W_isohash_gf');
    W_isohash_gf = x.W_isohash_gf;
    
    x = load(save_file.test_isohash_lp, 'eval_isohash_lp');
    eval_isohash_lp = x.eval_isohash_lp;
    
    for k = numel(all_topks) : numel(all_topks)
        eval_isohash_lp{k} = eval_hash5(size(W_isohash_lp, 2), 'linear', ...
            W_isohash_lp, Xtest, Xtraining, StestBase2', ...
            all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    end
    save(save_file.test_isohash_lp, 'eval_isohash_lp');
    
    x = load(save_file.test_isohash_gf, 'eval_isohash_gf');
    eval_isohash_gf = x.eval_isohash_gf;
    for k = numel(all_topks) : numel(all_topks)
        eval_isohash_gf{k} = eval_hash5(size(W_isohash_gf, 2), 'linear', ...
            W_isohash_gf, Xtest, Xtraining, StestBase2', ...
            all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    end
    save(save_file.test_isohash_gf, 'eval_isohash_gf');
end

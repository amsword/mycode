[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
all_topks = [1 10 100 200 400 50, 5];


if is_isohash
    x = load(save_file.train_isohash_lp, 'W_isohash_lp');
    W_isohash_lp = x.W_isohash_lp;
    
    x = load(save_file.train_isohash_gf, 'W_isohash_gf');
    W_isohash_gf = x.W_isohash_gf;
    
    if (is_test)
        x = load(save_file.test_isohash_lp);
        eval_isohash_lp = x.eval_isohash_lp;
        if numel(eval_isohash_lp) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_isohash_lp{k} = eval_hash5(size(W_isohash_lp, 2), 'linear', ...
                    W_isohash_lp, Xtest, Xbase, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_lp, 'eval_isohash_lp');
        end
        
        x = load(save_file.test_isohash_gf);
        eval_isohash_gf = x.eval_isohash_gf;
        
        if numel(eval_isohash_gf) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_isohash_gf{k} = eval_hash5(size(W_isohash_gf, 2), 'linear', ...
                    W_isohash_gf, Xtest, Xbase, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            save(save_file.test_isohash_gf, 'eval_isohash_gf');
        end
    end
    
end

if (is_bre)
    x = load(save_file.train_bre, 'trained_bre');
    trained_bre = x.trained_bre;
    
    if (is_test)
        x = load(save_file.test_bre,  'eval_bre');
        eval_bre = x.eval_bre;
        if numel(eval_bre) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_bre{k} = eval_hash5(size(trained_bre.W, 2), ...
                    'bre', trained_bre, ...
                    Xtest, Xtraining, StestBase2', ...
                    all_topks(k), [], [], metric_info, eval_types);
            end
            save(save_file.test_bre,  'eval_bre');
        end
    end
end
%%
if (is_lsh)
    x = load(save_file.train_lsh, 'W_lsh');
    W_lsh = x.W_lsh;
    if (is_test)
        x = load(save_file.test_lsh, 'eval_lsh');
        eval_lsh = x.eval_lsh;
        if numel(eval_lsh) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_lsh{k} = eval_hash5(m, 'linear', ...
                    W_lsh, Xtest, Xtraining, StestBase2', ...
                    all_topks(k), [], [], metric_info, eval_types);
            end
            save(save_file.test_lsh, 'eval_lsh', 'all_topks');
        end
    end
end

if is_ksh
    x = load(save_file.train_ksh, 'para_out');
    para_out = x.para_out;
    if is_test
        x = load(save_file.test_ksh, 'eval_ksh');
        eval_ksh = x.eval_ksh;
        if numel(eval_ksh) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_ksh{k} = eval_hash5(size(para_out.A1, 2), 'ksh', ...
                    para_out, Xtest, Xtraining, StestBase2', ...
                    all_topks(k), [], [], metric_info, eval_types);
            end
            save(save_file.test_ksh, 'eval_ksh');
        end
    end
end

if (is_mlh)
    x = load(save_file.train_mlh, 'W_mlh');
    W_mlh = x.W_mlh;
    
    if (is_test)
        x = load(save_file.test_mlh, 'eval_mlh');
        eval_mlh = x.eval_mlh;
        if numel(eval_mlh) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_mlh{k} = eval_hash5(size(W_mlh, 1), 'linear', ...
                    W_mlh', Xtest, Xtraining, StestBase2', ...
                    all_topks(k), [], [], metric_info, eval_types);
            end

            save(save_file.test_mlh, 'eval_mlh', 'type');
        end
    end
end

if (is_itq)
    if (strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3') )&& m == 256
        
    else
        x = load(save_file.train_itq, 'W_itq');
        W_itq = x.W_itq;
        if (is_test)
            x = load(save_file.test_itq, 'eval_itq');
            eval_itq = x.eval_itq;
            if numel(eval_itq) == numel(all_topks) - 1
                for k =  numel(all_topks) : numel(all_topks)
                    eval_itq{k} = eval_hash5(size(W_itq, 2), 'linear', ...
                        W_itq, Xtest, Xtraining, StestBase2', ...
                        all_topks(k), [], [], metric_info, eval_types);
                end
                save(save_file.test_itq, 'eval_itq');
            end
        end
    end
end
%
if (is_sh)
    load(save_file.train_sh, 'SHparam');
    
    if (is_test)
        x = load(save_file.test_sh, 'eval_sh');
        eval_sh = x.eval_sh;
        if numel(eval_sh) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_sh{k} = eval_hash5(SHparam.nbits, 'sh', SHparam, ...
                    Xtest, Xtraining, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, ...
                    [], metric_info, eval_types);
            end
            
            save(save_file.test_sh, 'eval_sh', 'all_topks');
        end
    end
end

if (is_usplh)
    
    x = load(save_file.train_usplh, 'W_usplh');
    W_usplh = x.W_usplh;
    
    if (is_test)
        x = load(save_file.test_usplh, 'eval_usplh');
        eval_usplh = x.eval_usplh;
        if numel(eval_usplh) == numel(all_topks) - 1
            for k = numel(all_topks) : numel(all_topks)
                eval_usplh{k} = eval_hash5(size(W_usplh, 2), 'linear', ...
                    W_usplh, Xtest, Xtraining, StestBase2', ...
                    all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
            end
            
            save(save_file.test_usplh, 'eval_usplh');
        end
    end
end

if is_agh
    x = load(save_file.train_agh_one, 'para_out');
    para_out = x.para_out;
    para_out.anchor = anchor;
    x = load(save_file.test_agh_one, 'eval_agh_one');
    eval_agh_one = x.eval_agh_one;
    if numel(eval_agh_one) == numel(all_topks) - 1
        for k = numel(all_topks) : numel(all_topks)
            eval_agh_one{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_one, 'eval_agh_one');
        clear eval_agh_one para_out;
    end
    
    x = load(save_file.train_agh_two, 'para_out');
    para_out = x.para_out;
    x = load(save_file.test_agh_two, 'eval_agh_two');
    eval_agh_two = x.eval_agh_two;
    para_out.anchor = anchor;
    if numel(eval_agh_two) == numel(all_topks) - 1
        for k = numel(all_topks) : numel(all_topks)
            eval_agh_two{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_two, 'eval_agh_two', 'type');
    end
end
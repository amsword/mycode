function para_out = train_agh(Xtraining, anchor, m, type)

s = 2;
if strcmp(type, 'one')
    [~, W, sigma] = OneLayerAGH_Train(Xtraining', anchor', m, s, 0);
    para_out.W = W;
    para_out.sigma = sigma;

elseif strcmp(type, 'two')
    [~, W, thres, sigma] = TwoLayerAGH_Train(Xtraining', anchor', m, s, 0);
    para_out.thres = thres;
    para_out.W = W;
    para_out.sigma = sigma;
end
para_out.s = 2;
para_out.type = type;
para_out.m = m;

end
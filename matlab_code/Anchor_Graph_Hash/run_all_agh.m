

for type = {'labelme', 'peekaboom', 'sift_1m'}; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    % CIFAR10, labelme, sift_1m, peekaboom, GIST1M3, SIFT1M3
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

    anchor = read_mat([type{1} '_cluster_center300'], 'double');
    
    for m = [8, 16, 32, 64, 128, 256]
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        [para_out] = train_agh(Xtraining, anchor, m, 'one');
        save(save_file.train_agh_one, 'para_out', 'type');
        para_out.anchor = anchor;
        eval_agh_one = cell(1, numel(all_topks));
        for k = 1 : numel(all_topks)
            eval_agh_one{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_one, 'eval_agh_one', 'type');
        clear eval_agh_one para_out;
        
        [para_out] = train_agh(Xtraining, anchor, m, 'two');
        save(save_file.train_agh_two, 'para_out', 'type');
        eval_agh_two = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_two{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_two, 'eval_agh_two', 'type');
        
    end
end
clear Xtraining Xtest StestBase2;
%
for type = {'SIFT1M3', 'GIST1M3'}
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    anchor = read_mat([type{1} '_cluster_center300'], 'double');
        
    for m = [8, 16, 32, 64, 128, 256]
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        [para_out] = train_agh(Xtraining, anchor, m, 'one');
        save(save_file.train_agh_one, 'para_out', 'type');
        eval_agh_one = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_one{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_one, 'eval_agh_one', 'type');
        clear eval_agh_one para_out;
        
        [para_out] = train_agh(Xtraining, anchor, m, 'two');
        save(save_file.train_agh_two, 'para_out', 'type');
        eval_agh_two = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_two{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_two, 'eval_agh_two', 'type');
 
    end
end

%%


for type = {'labelme', 'peekaboom', 'sift_1m'}; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    % CIFAR10, labelme, sift_1m, peekaboom, GIST1M3, SIFT1M3
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

    anchor = read_mat([type{1} '_cluster_center300'], 'double');
    
    for m = [8, 16, 32, 64, 128, 256]
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        x1 = load(save_file.train_agh_one);
        if strcmp(x1.type, type)
            continue;
        end
        type
        m
        pause;
        [para_out] = train_agh(Xtraining, anchor, m, 'one');
        save(save_file.train_agh_one, 'para_out', 'type');
        para_out.anchor = anchor;
        eval_agh_one = cell(1, numel(all_topks));
        for k = 1 : numel(all_topks)
            eval_agh_one{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_one, 'eval_agh_one', 'type');
        clear eval_agh_one para_out;
        
        [para_out] = train_agh(Xtraining, anchor, m, 'two');
        save(save_file.train_agh_two, 'para_out', 'type');
        eval_agh_two = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_two{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_two, 'eval_agh_two', 'type');
        
    end
end
clear Xtraining Xtest StestBase2;
%
for type = {'SIFT1M3', 'GIST1M3'}
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    anchor = read_mat([type{1} '_cluster_center300'], 'double');
        
    for m = [8, 16, 32, 64, 128, 256]
        [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
        
        [para_out] = train_agh(Xtraining, anchor, m, 'one');
        save(save_file.train_agh_one, 'para_out', 'type');
        eval_agh_one = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_one{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_one, 'eval_agh_one', 'type');
        clear eval_agh_one para_out;
        
        [para_out] = train_agh(Xtraining, anchor, m, 'two');
        save(save_file.train_agh_two, 'para_out', 'type');
        eval_agh_two = cell(1, numel(all_topks));
        para_out.anchor = anchor;
        for k = 1 : numel(all_topks)
            eval_agh_two{k} = eval_hash5(para_out.m, 'agh', ...
                para_out, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_agh_two, 'eval_agh_two', 'type');
 
    end
end
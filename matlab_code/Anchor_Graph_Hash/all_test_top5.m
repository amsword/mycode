%%
clear;
metric_info.type = 0;
eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = 0;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;
pca_dim = 64;
is_init_orth = 0;

is_plot = 0;
is_plot_lookup = 0;
is_plot_ranking = 1;

global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

%%
for itype = {'labelme', 'peekaboom', 'sift_1m'}
    % for itype = {'labelme'}
    type = itype{1};
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    
    Xtraining = read_mat(src_file.c_train, 'double');
    Xtest = read_mat(src_file.c_test, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);
    %
    anchor = read_mat([type '_cluster_center300'], 'double');
    
    for m = [16, 32, 64, 128, 256]
        %             for m = [16]
        file_pre_date = '';
        %
        % file_pre_date = [];
        % m = 64 % code length
        is_cbh = 0;
        is_bsd = 0;
        
        
        is_regression = 0;
        is_classification = 0;
        is_bre = 0;
        is_mlh = 0;
        is_lsh = 0;
        is_sh = 1;
        is_ch = 1;
        is_usplh = 1;
        is_itq = 0;
        is_mbq = 0;
        is_ksh = 0;
        is_agh = 1;
        is_train = 0;
        is_test = 1;
        is_isohash = 1;
        is_bre_all = false;
        
        test_top5_iso;
    end
end
%

%%
for type = {'GIST1M3'}
    [src_file] = jf_src_data_file_name(type);
    gnd_file = jf_gnd_file_name(type, 0, 0);
    
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    anchor = read_mat([type{1} '_cluster_center300'], 'double');
    %
    Xtraining = Xbase;
    %
    for m = [16, 32, 64, 128, 256]
        
        file_pre_date = '';
        is_cbh = 0;
        is_bsd = 0;
        
        is_regression = 0;
        is_classification = 1;
        is_bre = 1;
        is_mlh = 1;
        is_lsh = 1;
        is_sh = 1;
        is_ch = 1;
        is_usplh = 1;
        is_itq = 1;
        is_mbq = 1;
        is_ksh = true;
        is_agh = 1;
        is_train = 0;
        is_test = 1;
        is_isohash = 1;
        is_bre_all = false;
        
        test_top5;
        
    end
end
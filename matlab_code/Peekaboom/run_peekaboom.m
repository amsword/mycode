%% create data
data = create_data('euc-peekaboom', 400,[]);
perform_pca = 1			% whether to perform PCA dimensionality reduction
if (perform_pca)
  data2 = do_pca(data, 40);
else
  data2 = data;
end

nbs = [11];
rhos = [2];

% only test nb = 10
nb = 11;

%% SIG
clear train_params model_params;

model_params.weight = .65; % 0.8 better?
model_params.nb = nb;
model_params.rho = rhos(nbs == nb);
model_params.thresZ = log(1000)/nb * 2;
model_params.thresCode = log(1000);
model_params.shrink_w = 1e-5;

model_params.rhos = calc_dk([30, 300, 1300], size(data2.Xtraining, 2), nb);

train_params.nMinibatch = 100;
train_params.niters = 200;
train_params.lambda = 0.5;
train_params.nSamples = 10^5;

verbose = 1;
fprintf('start SIG training\n');
% Wsig = onlineHC_S(data2, model_params, train_params, verbose);
 Wsig = trainSIG(data2, model_params, train_params, verbose);
% Wsig_0 = trainSIG_single(data2, model_params, train_params, verbose, 0);

save result/sig_peekaboom Wsig train_params model_params

% %% SIG_DIM
% clear train_params model_params;
% 
% model_params.weight = .6; % 0.8 better?
% model_params.nb = nb;
% model_params.rho = rhos(nbs == nb);
% model_params.thresZ = log(1000)/nb * 2;
% model_params.thresCode = log(1000);
% model_params.shrink_w = 1e-5;
% 
% train_params.nMinibatch = 100;
% train_params.niters = 100;
% train_params.lambda = 0.0;
% train_params.nSamples = 10^5;
% 
% verbose = 1;
% fprintf('start SIG training\n');
% for i = 0:rhos(nbs == nb)
%     Wsig_dim{i+1} = trainSIG_single(data2, model_params, train_params, verbose, i);
% end
% 
% for i = 0:rhos(nbs == nb)
%     sig_eval_dim{i+1} = eval_hash(Wsig_dim{i+1}, data2, 'w_based', [])
% end
% 
% n = [100, 128, 256, 512, 1024, 2048];
% plot1 = plot(n, sig_eval_dim{3}.p_code(n), n, sig_eval.p_code(n));
% 
% figure
% n = [100, 128, 256, 512, 1024, 2048];
% plot1 = plot(n, sig_eval_dim{3}.r_code(n), n, sig_eval.r_code(n));

%% MLH
clear mlh_params
mlh_params.size_batches = 100;
mlh_params.eta = .1;
mlh_params.shrink_w = 1e-4;
mlh_params.lambda = 0.1;
mlh_params.rho = rhos(nbs == nb);
train_iter = 100;
train_zerobias = 1;

fprintf('start MLH training\n');
Wmlh = MLH(data2, {'hinge', mlh_params.rho, mlh_params.lambda}, nb, ...
		    [mlh_params.eta], .9, [mlh_params.size_batches], 'trainval', train_iter, train_zerobias, ...
		    5, 1, 50, [mlh_params.shrink_w], 1);
Wmlh = Wmlh.W;

save result/mlh_peekaboom Wmlh mlh_params

%% BRE
clear best_params
bre_params.size_batches = 100;
bre_params.eta = .1;
bre_params.shrink_w = 1e-4;

fprintf('start BRE training\n');

Wbre = MLH(data2, {'bre'}, nb, [bre_params.eta], .9, [bre_params.size_batches], ...
    'trainval', 100, 1, 5, 1, 50, [bre_params.shrink_w], 1);
Wbre = Wbre.W;

save result/bre_peekaboom Wbre bre_params

%% USPLH
USPLHparam.nbits = nb;
USPLHparam.eta=.8;
USPLHparam.lambda=0.25;
USPLHparam.c_num=100;

fprintf('start USPLH training\n');
USPLHparam = trainUSPLH(data.Xtraining', USPLHparam);
Wusplh = [USPLHparam.w; USPLHparam.b]';

save result/usplh_peekaboom USPLHparam Wusplh

%% SH
SHparam.nbits = nb;

fprintf('start SH training\n');
SHparam = trainSH(data2.Xtraining', SHparam);

save result/sh_peekaboom SHparam

%% LSH
LSHparam.nbits = nb;
LSHparam.L = 1;
[Ndim, Ntrain] = size(data2.Xtraining);

fprintf('start LSH training\n');
Wlsh = lshfunc(LSHparam.L, LSHparam.nbits, Ndim);
Wlsh = [Wlsh.A; Wlsh.b]';
save result/lsh_peekaboom Wlsh LSHparam

%% CH
CHparam.nbits = nb;
CHparam.doMask = 0;
CHparam.eta = .1;
CHparam.L = 3;
CHparam.c_num = 200;
CHparam.epsilon = .005;
CH = trainCH_ext(data.Xtraining', CHparam);
save result/ch_peekaboom CH

% %% SKLSH
% RFparam.gamma = 1;
% RFparam.D = size(data.Xtraining, 1); % dim of data
% RFparam.M = nb;
% RFparam = RF_train(RFparam);
% save result/sklsh_labelme RFparam
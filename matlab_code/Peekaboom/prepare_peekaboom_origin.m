function prepare_peekaboom_origin(save_file)
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

load('\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\PeekaboomOrigin\data\Peekaboom_gist.mat', ...
    'gist');

N = size(gist, 1);
rp = randperm(N);
ndxtrain = rp(2001 : N);
ndxtest = rp([1 : 2000]);


gist = double(gist);
mean_value = mean(gist, 1);
gist = bsxfun(@minus, gist, mean_value);

OriginXtraining = gist(ndxtrain, :);
OriginXtraining = OriginXtraining';

OriginXtest = gist(ndxtest, :);
OriginXtest = OriginXtest';

save(save_file.train, 'OriginXtraining', 'ndxtrain');
save(save_file.test, 'OriginXtest', 'ndxtest');

save_mat(OriginXtraining, save_file.c_train, 'double');
save_mat(OriginXtest, save_file.c_test, 'double');

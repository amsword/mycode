rho_max = 2
nb = 11;

%% SIG
W = Wsig;
sig_eval = eval_hash(W, data2, 'w_based', [])

%% MLH
W = Wmlh;
mlh_eval = eval_hash(W, data2, 'w_based', [])


%% BRE
W = Wbre;
bre_eval = eval_hash(W, data2, 'w_based', [])

%% LSH
W = Wlsh;
lsh_eval = eval_hash(W, data2, 'w_based', [])

%% SH
sh_eval = eval_hash(W, data2, 'sh', SHparam)

%% USPLH
W = Wusplh;
usplh_eval = eval_hash(W, data, 'w_based', [])

% %% SKLSH
% sklsh_eval = eval_hash(W, data, 'sklsh', RFparam)

%% CH
ch_eval = eval_hash_ch(data, CH)


%% Precision Figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1,'FontSize',16);
% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[0 2100]);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.05 0.25]);
box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');
n = [100, 128, 256, 512, 1024, 2048];
plot1 = plot(n, sh_eval.p_code(n), n, sig_eval.p_code(n), n, mlh_eval.p_code(n),...
    n, usplh_eval.p_code(n), n, lsh_eval.p_code(n),...
    n, ch_eval.p_code(n), n, bre_eval.p_code(n));
set(plot1(1),'Marker','v',...
    'Color',[0 0 0],...
    'DisplayName','SH');
set(plot1(2),'Marker','square','Color',[1 0 0],'DisplayName','CBH');
set(plot1(3),'Marker','o','Color',[0 0 1],'DisplayName','MLH');
set(plot1(4),'Marker','.','Color',[0 1 1],'DisplayName','USPLH');
set(plot1(5),'Marker','<','Color',[0 1 0],'DisplayName','LSH');
set(plot1(6),'Marker','diamond','Color',[1 1 0],'DisplayName','CH');
set(plot1(7),'Marker','hexagram','Color',[1 0 1],'DisplayName','BRE');

% Create xlabel
xlabel('Number of retrieved points','FontSize',16);

% Create ylabel
ylabel('Precision','FontSize',16);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.67960908610671 0.475873015873016 0.20919175911252 0.43047619047619]);

%% Recall Figure
figure2 = figure;

% Create axes
axes2 = axes('Parent',figure2,'FontSize',16);
% Uncomment the following line to preserve the X-limits of the axes
xlim(axes2,[0 5100]);
box(axes2,'on');
grid(axes2,'on');
hold(axes2,'all');

% Create multiple lines using matrix input to plot

n = [100, 128, 256, 512, 1024, 2048, 3000, 5000];
plot1 = plot(n, sh_eval.r_code(n), n, sig_eval.r_code(n), n, mlh_eval.r_code(n),...
    n, usplh_eval.r_code(n), n, lsh_eval.r_code(n),...
    n, ch_eval.r_code(n), n, bre_eval.r_code(n));
set(plot1(1),'Marker','v',...
    'Color',[0 0 0],...
    'DisplayName','SH');
set(plot1(2),'Marker','square','Color',[1 0 0],'DisplayName','CBH');
set(plot1(3),'Marker','o','Color',[0 0 1],'DisplayName','MLH');
set(plot1(4),'Marker','.','Color',[0 1 1],'DisplayName','USPLH');
set(plot1(5),'Marker','<','Color',[0 1 0],'DisplayName','LSH');
set(plot1(6),'Marker','diamond','Color',[1 1 0],'DisplayName','CH');
set(plot1(7),'Marker','pentagram','Color',[1 0 1],'DisplayName','BRE');

% Create xlabel
xlabel('Number of retrieved points','FontSize',16);

% Create ylabel
ylabel('Recall','FontSize',16);

% Create legend
legend1 = legend(axes2,'show');
set(legend1,...
    'Position',[0.677231907025887 0.128253968253968 0.20919175911252 0.43047619047619]);

%%
plot_labelme_prec2recall(sh_eval.r1, sh_eval.p1, sig_eval.r1, sig_eval.p1,...
    mlh_eval.r1, mlh_eval.p1, usplh_eval.r1, usplh_eval.p1,...
    lsh_eval.r1, lsh_eval.p1, ch_eval.r1, ch_eval.p1, bre_eval.r1, bre_eval.p1)

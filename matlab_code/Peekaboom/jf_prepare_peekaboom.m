function jf_prepare_peekaboom(save_file)
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

load('F:\v-jianfw\HashCode\Peekaboom\data\Peekaboom_gist.mat', ...
    'gist');
gist = double(gist);
% load('data\LabelMe_gist.mat', ...
%     'gist');
% load('labelme1002');

N = size(gist, 1);
rp = randperm(N);
ndxtrain = rp(2001 : N);
ndxtest = rp([1 : 2000]);

Xtraining = gist(ndxtrain, :);
Xtraining = Xtraining';

Xtest = gist(ndxtest, :);
Xtest = Xtest';

Xbase = Xtraining;

jf_preprocessing(Xtraining, Xbase, Xtest, save_file);
% function para = trainPQHashing(Xtraining, Xtest)

num_split = 2;
is_kmeans = 0;


dim = size(pcXtraining, 1);
assert(mod(dim, num_split) == 0);

idx = cell(num_split, 1);
for k = 1 : numel(idx)
    idx{k} = k : num_split : (dim + k - num_split);
%     idx{k} = ((k - 1) * dim / num_split + 1) : (k * dim / num_split);
end

center = cell(num_split, 1);
num_points = size(pcXtraining, 2);
if is_kmeans
    for i = 1 : num_split
        sub = pcXtraining(idx{i}, :);
        center{i} = vl_kmeans(sub, num_center_each);
    end
else
    for i = 1 : num_split
        sub = pcXtraining(idx{i}, :);
        ridx = randperm(num_points);
        ridx = ridx(1 : num_center_each);
        center{i} = sub(:, ridx);
    end
end

para.idx = idx;
para.center = center;

Xtraining2 = dist_center(pcXtraining, para);
s = mean(Xtraining2(:));

s = coef * s;

para.s = s;

Xtraining2 = exp(-0.5 * Xtraining2 / s);

mean_value = mean(Xtraining2, 2);
Xtraining2 = bsxfun(@minus, Xtraining2, mean_value);

Xtest2 = dist_center(pcXtest, para);
Xtest2 = exp(-0.5 * Xtest2 / s);
Xtest2 = bsxfun(@minus, Xtest2, mean_value);

%%
clear W_lsh_kernel eval_epsilon_lsh_kernel
for iter = 1 : 10
    [W_lsh_kernel{iter} para_lsh] = jf_train_lsh(Xtraining2, m);
    eval_epsilon_lsh_kernel{iter} = eval_hash7(m, 'linear', ...
        W_lsh_kernel{iter}, Xtest2, Xtraining2, StestBase3, ...
        [], [], [], metric_info, eval_types);
end

retrived = 0;
good = 0;
for i = 1 : 10
    retrived = eval_epsilon_lsh_kernel{i}.avg_retrieved + retrived;
    good = eval_epsilon_lsh_kernel{i}.avg_retrieved_good + good;
end
retrived = retrived / 10;
good = good / 10;
rec = good / eval_epsilon_lsh_kernel{1}.avg_good;
pre = good ./ retrived;



% 
% plot(eval_epsilon_lsh.avg_retrieved, eval_epsilon_lsh.rec, 'b*-');
% hold on;
% for iter = 1 : 10
%     plot(eval_epsilon_lsh_kernel{iter}.avg_retrieved, eval_epsilon_lsh_kernel{iter}.rec, 'ro-');
% end
% xlim([0, 10000]);
%%


%  [W para_mlh] = jf_train_mlh(Xtraining2, ...
%             mlh_Strainingtraining,...
%             m);
% W = W';
% eval_epsilon_mlh_kernel = eval_hash7(m, 'linear', ...
%         W, Xtest2, Xtraining2, StestBase3, ...
%         [], [], [], metric_info, eval_types);
    
% plot(eval_epsilon_mlh.avg_retrieved, eval_epsilon_mlh.rec, 'b*-');
% hold on;
% plot(eval_epsilon_mlh_kernel.avg_retrieved, eval_epsilon_mlh_kernel.rec, 'ro-');
% xlim([0, 10000]);
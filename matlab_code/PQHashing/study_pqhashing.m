epsilon = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, 50);
[StestBase3 avg_num] = load_gnd_epsilon(gnd_file.DistanceTestTrainBin, epsilon);

%%
load(save_file.train_mlh);
W = W_mlh';
eval_epsilon_mlh = eval_hash7(m, 'linear', ...
        W, Xtest, Xtraining, StestBase3, ...
        [], [], [], metric_info, eval_types);

%%
[pc, ~] = eig(cov(Xtraining'));
para.pc = pc;
pcXtraining = pc' * Xtraining;
pcXtest = pc' * Xtest;

%%
k = 1;
clear all_para;
for num_center_each = [75]
    for coef = [1]
        all_para{k}.num_center_each = num_center_each;
        all_para{k}.coef = coef;
        k = k + 1;
    end
end

%%
clear all_result;
for k2 = 1 : numel(all_para)
    num_center_each = all_para{k2}.num_center_each;
    coef = all_para{k2}.coef;
    
    trainPQHashing;
    
%     all_result{k2}.para = para;
%     all_result{k2}.eval = eval_epsilon_mlh_kernel;
%     save('mlh', 'all_result');
end


%%
clear W_lsh eval_epsilon_lsh
for iter = 1 : 10
    [W_lsh{iter} para_lsh] = jf_train_lsh(Xtraining, m);
    eval_epsilon_lsh{iter} = eval_hash7(m, 'linear', ...
        W_lsh{iter}, Xtest, Xtraining, StestBase3, ...
        [], [], [], metric_info, eval_types);
end

lsh_retrived = 0;
lsh_good = 0;
for i = 1 : 10
    lsh_retrived = eval_epsilon_lsh{i}.avg_retrieved + lsh_retrived;
    lsh_good = eval_epsilon_lsh{i}.avg_retrieved_good + lsh_good;
end
lsh_retrived = lsh_retrived / 10;
lsh_good = lsh_good / 10;
lsh_rec = lsh_good / eval_epsilon_lsh_kernel{1}.avg_good;
lsh_pre = lsh_good ./ lsh_retrived;


%%
load(save_file.train_lsh);
eval_epsilon_lsh = eval_hash7(m, 'linear', ...
    W_lsh, pcXtest, pcXtraining, StestBase3, ...
    [], [], [], metric_info, eval_types);
%%

for i = 1 : numel(StestBase3)
    x(i) = numel(StestBase3{i});
end

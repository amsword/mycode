function y = dist_center(X, para)

idx = para.idx;
center = para.center;

X2 = cell(numel(idx), 1);

for i = 1 : numel(idx);
    sub = X(idx{i}, :);
    rep = jf_distMat(center{i}, sub);
    rep = rep .^ 2;
    X2{i} = rep;
end

cdim = 1;
for i = 1 : numel(center)
    cdim = size(center{i}, 2) * cdim;
end
num_points = size(X, 2);
y = zeros(cdim, num_points);

%
for i = 1 : num_points
    t = 0;
    for k = 1 : numel(X2)
        t = bsxfun(@plus, t, X2{k}(:, i)');
        t = reshape(t, numel(t), 1);
    end
    y(:, i) = t;
end
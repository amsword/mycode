function str = get_src_parent_folder()

if ispc()
    curr_file = mfilename('fullpath');
    idx = find(curr_file == '\', 1, 'last');
    str = curr_file(1 : idx);
else
    curr_file = mfilename('fullpath');
    idx = find(curr_file == '/', 1, 'last');
    str = curr_file(1 : idx);
end
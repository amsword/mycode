eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = false;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;
pca_dim = 64;
is_init_orth = 0;

is_plot = 0;
is_plot_lookup = 0;
is_plot_ranking = 1;
%% used to testing for the result from va
clear;
global gl_is_hash_lookup;
gl_is_hash_lookup = true;
global gl_is_topK;
gl_is_topK = true;
global gl_is_hamming_ranking;
gl_is_hamming_ranking = true;


is_pca = false;
type = 'peekaboom'; % peekaboom; labelme; sift_1m;
m = 12 % code length
pre_fix = '2012_5_20_14_37_36_';

global gl_data_parent_folder;
gl_data_parent_folder = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\';

[src_file ] = jf_src_data_file_name(type);
[save_file] = jf_train_save_file(type, m, is_pca, pre_fix);

load(save_file.train_cbh, 'W', 'para_out');

if (is_pca)
    Xtraining = jf_load_training(src_file.train);
    Xbase = jf_load_base(src_file.base);
    Xtest = jf_load_test(src_file.test);
else
    Xtraining = jf_load_origin_training(src_file.train);
    Xbase = jf_load_origin_base(src_file.base);
    Xtest = jf_load_origin_test(src_file.test);
end

N_train = size(Xtraining, 2);
topK_train = floor(N_train * 0.02);

N_base = size(Xbase, 2);
topK_base = floor(N_base * 0.02);

gnd_file = jf_gnd_file_name(type, m, topK_train, topK_base);

StestBase = jf_load_gnd([gnd_file.StestTraining]);

curr_eval  = jf_test_cbh(W, Xtest, Xbase, ...
    StestBase, topK_base, gnd_file.TestBaseSeed);

[bucketCap en bin_idx] = jf_how_average(W, Xtraining);
save(save_file.train_cbh, 'W', 'para_out', 'type', '-v7.3');
eval_cbh = curr_eval;
save(save_file.test_cbh, 'eval_cbh', 'curr_eval', 'bucketCap', 'en', '-v7.3');

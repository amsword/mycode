ratio = 0.1;

Xtraining = read_mat(src_file.c_train, 'double');

Ntraining = size(Xtraining, 2);

%%
num_val = floor(Ntraining * ratio);

%%
rp = randperm( Ntraining);
idx_val_in_training = rp(1 : num_val);
idx_learn_in_training = rp(num_val + 1 : end);

%%
XValidation = Xtraining(:, idx_val_in_training);
save_mat(XValidation, src_file.c_val_train, 'double');

XLearn = Xtraining(:, idx_learn_in_training);
save_mat(XLearn, src_file.c_learn_train, 'double');

%%
save(src_file.idx_val_learn_in_train, 'idx_learn_in_training', 'idx_val_in_training');

%%
exeExaustiveKNN(src_file.c_val_train, src_file.c_learn_train, gnd_file.GndValidationInLearn, 5000);


function y = jf_gen_u_full_file_name(file_name, subfolder)

global gl_data_parent_folder;

if (nargin == 1)
    file_path = [gl_data_parent_folder 'result\'];
else
    file_path = [gl_data_parent_folder subfolder '\result\'];
end

if (~exist(file_path, 'dir'))
    mkdir(file_path);
end

curr_time = clock;
y = [file_path ...
        num2str(curr_time(1)) '_' ...
        num2str(curr_time(2)) '_' ...
        num2str(curr_time(3)) '_' ...
        num2str(curr_time(4)) '_' ...
        num2str(curr_time(5)) '_' ...
        num2str(floor(curr_time(6))) '_' ...
		file_name];

    





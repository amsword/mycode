function [save_file] = gl_save_hashing(data_folder, m)

suffix = 0;
path_working = [data_folder 'working\'];

save_file.train_cbh = [path_working 'train_cbh' num2str(m) '_' num2str(suffix)];
save_file.test_cbh = [path_working 'test_cbh' num2str(m) '_' num2str(suffix)];

save_file.train_classification = [path_working 'train_classification' num2str(m) '_' num2str(suffix)];
save_file.test_classification = [path_working 'test_classification' num2str(m) '_' num2str(suffix)];

save_file.train_classification_k = [path_working 'train_classification_k' num2str(m) '_' num2str(suffix)];
save_file.test_classification_k = [path_working 'test_classification_k' num2str(m) '_' num2str(suffix)];


save_file.train_classification_one_level = [path_working 'train_classification_one_level' num2str(m) '_' num2str(suffix)];
save_file.test_classification_one_level = [path_working 'test_classification_one_level' num2str(m) '_' num2str(suffix)];

save_file.train_bre = [path_working 'train_bre' num2str(m) '_' num2str(suffix)];
save_file.test_bre = [path_working 'test_bre' num2str(m) '_' num2str(suffix)];

save_file.train_ch = [path_working 'train_ch' num2str(m) '_' num2str(suffix)];
save_file.test_ch = [path_working 'test_ch' num2str(m) '_' num2str(suffix)];

save_file.train_lsh = [path_working 'train_lsh' num2str(m) '_' num2str(suffix)];
save_file.test_lsh = [path_working 'test_lsh' num2str(m) '_' num2str(suffix)];

% save_file.train_mlh = [path_dir 'train_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
save_file.train_mlh = [path_working 'train_mlh' num2str(m) '_' num2str(suffix)];
save_file.test_mlh = [path_working 'test_mlh' num2str(m) '_' num2str(suffix)];

save_file.train_mlh2 = [path_working 'train_mlh2' num2str(m) '_' num2str(suffix)];

save_file.test_mlh2 = [path_working 'test_mlh2' num2str(m) '_' num2str(suffix)];


save_file.train_mlh1k = [path_working 'train_mlh1k' num2str(m) '_' num2str(suffix)];
save_file.test_mlh1k = [path_working 'test_mlh1k' num2str(m) '_' num2str(suffix)];

save_file.train_sh = [path_working 'train_sh' num2str(m) '_' num2str(suffix)];
save_file.test_sh = [path_working 'test_sh' num2str(m) '_' num2str(suffix)];

save_file.train_usplh = [path_working 'train_usplh' num2str(m) '_' num2str(suffix)];
save_file.test_usplh = [path_working 'test_usplh' num2str(m) '_' num2str(suffix)];

save_file.train_itq = [path_working 'train_itq' num2str(m) '_' num2str(suffix)];
save_file.test_itq = [path_working 'test_itq' num2str(m) '_' num2str(suffix)];

save_file.train_ksh = [path_working 'train_ksh' num2str(m) '_' num2str(suffix)];
save_file.test_ksh = [path_working 'test_ksh' num2str(m) '_' num2str(suffix)];


save_file.train_c_bre_init = [path_working 'train_c_bre_init' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_1000 = ['train_c_bre_1000' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_1000 = ['train_c_bre_1000' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_1002 = ['train_c_bre_1002' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_1002 = ['train_c_bre_1002' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2000 = ['train_c_bre_2000' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2000 = ['train_c_bre_2000' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2100 = ['train_c_bre_2100' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2100 = ['train_c_bre_2100' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2110 = ['train_c_bre_2110' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2110 = ['train_c_bre_2110' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2001 = ['train_c_bre_2001' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2001 = ['train_c_bre_2001' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2101 = ['train_c_bre_2101' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2101 = ['train_c_bre_2101' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2111 = ['train_c_bre_2111' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2111 = ['train_c_bre_2111' num2str(m) '_' num2str(suffix)];


save_file.train_c_bre_2002 = ['train_c_bre_2002' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2002 = ['train_c_bre_2002' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2102 = ['train_c_bre_2102' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2102 = ['train_c_bre_2102' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2112 = ['train_c_bre_2112' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2112 = ['train_c_bre_2112' num2str(m) '_' num2str(suffix)];

save_file.train_linear_bre = [path_working 'train_linear_bre' num2str(m) '_' num2str(suffix)];
save_file.test_linear_bre = [path_working 'test_linear_bre' num2str(m) '_' num2str(suffix)];

save_file.train_isohash_lp = [path_working 'train_isohash_lp' num2str(m) '_' num2str(suffix)];
save_file.test_isohash_lp = [path_working 'test_isohash_lp' num2str(m) '_' num2str(suffix)];

save_file.train_isohash_gf = [path_working 'train_isohash_gf' num2str(m) '_' num2str(suffix)];
save_file.test_isohash_gf = [path_working 'test_isohash_gf' num2str(m) '_' num2str(suffix)];

save_file.train_agh_one = [path_working 'train_agh_one' num2str(m) '_' num2str(suffix)];
save_file.test_agh_one = [path_working 'test_agh_one' num2str(m) '_' num2str(suffix)];

save_file.train_agh_two = [path_working 'train_agh_two' num2str(m) '_' num2str(suffix)];
save_file.test_agh_two = [path_working 'test_agh_two' num2str(m) '_' num2str(suffix)];

save_file.train_mbq = [path_working 'train_mbq' num2str(m) '_' num2str(suffix)];
save_file.test_mbq = [path_working 'test_mbq' num2str(m) '_' num2str(suffix)];

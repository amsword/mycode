function [save_file folder_figure] = jf_train_save_file(type, m, topK, suffix, prefix)

global gl_data_parent_folder;

if (strcmp(type, 'labelme'))
    path_dir = [gl_data_parent_folder ...
        'labelme\result\'];
elseif (strcmp(type, 'peekaboom'))
    path_dir = [gl_data_parent_folder ...
        'result\peekaboom\'];
elseif (strcmp(type, 'sift_1m'))
    path_dir = [gl_data_parent_folder ...
        'result\sift_1m\'];
elseif (strcmp(type, 'notre'))
    path_dir = [gl_data_parent_folder 'result\Notre\'];
elseif (strcmp(type, 'tiny'))
    path_dir = [gl_data_parent_folder 'result\tiny\'];
elseif (strcmp(type, 'ForQuantize'))
    path_dir = [gl_data_parent_folder 'ForQuantize\result\'];
else
    error('no valid type');
end

folder_figure = [path_dir 'figure\'];
if (exist('prefix', 'var'))
    path_dir = [path_dir ...
        prefix];
else
    curr_time = clock;

    path_dir = [path_dir ...
        num2str(curr_time(1)) '_' ...
        num2str(curr_time(2)) '_' ...
        num2str(curr_time(3)) '_' ...
        num2str(curr_time(4)) '_' ...
        num2str(curr_time(5)) '_' ...
        num2str(floor(curr_time(6))) '_'];
end

save_file.train_cbh = [path_dir 'train_cbh' num2str(m) '_' num2str(suffix)];
save_file.test_cbh = [path_dir 'test_cbh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_classification = [path_dir 'train_classification' num2str(m) '_' num2str(suffix)];
save_file.test_classification = [path_dir 'test_classification' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_classification_one_level = [path_dir 'train_classification_one_level' num2str(m) '_' num2str(suffix)];
save_file.test_classification_one_level = [path_dir 'test_classification_one_level' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_bre = [path_dir 'train_bre' num2str(m) '_' num2str(suffix)];
save_file.test_bre = [path_dir 'test_bre' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_ch = [path_dir 'train_ch' num2str(m) '_' num2str(suffix)];
save_file.test_ch = [path_dir 'test_ch' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_lsh = [path_dir 'train_lsh' num2str(m) '_' num2str(suffix)];
save_file.test_lsh = [path_dir 'test_lsh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

% save_file.train_mlh = [path_dir 'train_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
save_file.train_mlh = [path_dir 'train_mlh' num2str(m) '_' num2str(suffix)];
save_file.test_mlh = [path_dir 'test_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_sh = [path_dir 'train_sh' num2str(m) '_' num2str(suffix)];
save_file.test_sh = [path_dir 'test_sh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_usplh = [path_dir 'train_usplh' num2str(m) '_' num2str(suffix)];
save_file.test_usplh = [path_dir 'test_usplh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
function map = jf_calc_map(r, p)

idx = (r ~= 0);
r = r(idx);
p = p(idx);

r = reshape(r, numel(r), 1);
p = reshape(p, numel(p), 1);

n = numel(r);
map = sum(diff([0; r]) .* ([p(1); p(1 : n - 1)] + p(1 : n)) / 2);
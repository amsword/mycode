%%
clear;
global gl_is_hash_lookup;
gl_is_hash_lookup = 1;
global gl_is_topK;
gl_is_topK = 1;
global gl_is_hamming_ranking;
gl_is_hamming_ranking = 1;
global gl_is_ndcg;
gl_is_ndcg = 0;
global gl_is_success_rate;
gl_is_success_rate = 1;

is_pca = 0;

is_plot = 0;
is_plot_lookup = 1;
is_plot_ranking = 0;

gnd_percent = 0.02;
m = 24;
type = 'tiny';
%%
global gl_data_parent_folder;
gl_data_parent_folder = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\';
global gl_is_multi_thread;
gl_is_multi_thread = false;
[src_file] = jf_src_data_file_name(type);
file_pre_date = '2012_5_25_18_18_37_';
%%
if (is_pca)
    Xtraining = jf_load_training(src_file.train);
    Xtest = jf_load_test(src_file.test);
else
    Xtraining = jf_load_origin_training(src_file.train);
    Xtest = jf_load_origin_test(src_file.test);
end

%%

Ntraining = size(Xtraining, 2);
topK_train = floor(Ntraining * gnd_percent);

Ntest = size(Xtest, 2);

topK = topK_train;


gnd_file = jf_gnd_file_name(type, m, topK_train, topK_train);

StestBase = jf_load_gnd([gnd_file.StestTraining]);

[save_file save_figure] = jf_train_save_file(type, m, topK_train, is_pca, file_pre_date);

%%

    num_center_sample = 200;


load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\result\tiny\2012_5_25_18_18_36_train_cbh24_00.042667.mat', ...
    'W', 'para_out');

%%
I = jf_load_gnd(gnd_file.RankStrainingtraining);
%%
for i = 1 : 3
    i
    cbh_para2 = para_out;
    cbh_para2.is_set_weight = 0;
    cbh_para2.is_force_normalize_w = 0;
    cbh_para2.mu = para_out.mu * 2;
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        cbh_para2, W);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    [bucketCap en] = jf_how_average(W, Xtraining);
    eval_cbh = curr_eval;
    save([save_file.test_cbh num2str(para_out.mu) '2.mat'], ...
        'eval_cbh', 'curr_eval', 'bucketCap', 'en', '-v7.3');
    
    result{i}.W = W;
    result{i}.para_out = para_out;
    result{i}.eval_cbh = eval_cbh;
end
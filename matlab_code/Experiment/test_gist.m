%%
% clear;

eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = 0;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;
pca_dim = 64;
is_init_orth = 0;

is_plot = 0;
is_plot_lookup = 0;
is_plot_ranking = 1;

% for gnd_percent = [0.02]
%     gnd_percent
for m = [64]
    type = 'GIST1M3'; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
    file_pre_date = '';
end

global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

global gl_is_multi_thread;
gl_is_multi_thread = false;

[src_file] = jf_src_data_file_name(type);

metric_info.type = 0;

topK_train = 400;

all_topks = [1 10 100 200 400 50];

gnd_file = jf_gnd_file_name(type, m, topK_train);

'dedicated save file name'
%     [save_file save_figure] = jf_train_save_file(type, m, topK, is_pca, file_pre_date);
%     [save_file save_figure] = train_save_file(type, m, topK, is_pca, file_pre_date);
[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);


%%
Xtest = read_mat(src_file.c_test, 'double');
Xbase = read_mat(src_file.c_base, 'double');
StestBase2 = load_gnd2(gnd_file.STestBase, 400);
%%
clear best_result2;
load([type '_' num2str(m)], 'best_result2');
if isfield(best_result2{1}, 'eval')
    'a done'
    return;
end

parfor  i = 1 : numel(best_result2)
    
    W = best_result2{i}.W;
    para_out = best_result2{i}.para_out;
    
    all_eval = cell(numel(all_topks), 1);
    for k = 1 : numel(all_topks)
        curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
            Xtest, Xbase, ...
            StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
            metric_info, eval_types);
        all_eval{k} = curr_eval;
    end
    best_result2{i}.eval = all_eval;
end
save([type '_' num2str(m)], 'best_result2', '-v7.3');
'done'
function [save_file folder_figure] = train_save_file2(type, m, suffix, prefix)

global gl_data_parent_folder;

if (strcmp(type, 'Labelme'))
    path_working = [gl_data_parent_folder ...
        'labelme\working\'];
    path_dir = [gl_data_parent_folder ...
        'labelme\result\'];
elseif (strcmp(type, 'LabelmeOrigin'))
    path_working = [gl_data_parent_folder ...
        'labelmeOrigin\working\'];
    path_dir = [gl_data_parent_folder ...
        'labelmeOrigin\result\'];
elseif (strcmp(type, 'peekaboom'))
    path_working = [gl_data_parent_folder ...
        'peekaboom\working\'];
    path_dir = [gl_data_parent_folder ...
        'peekaboom\result\'];
elseif strcmp(type, 'PeekaboomOrigin')
    path_working = [gl_data_parent_folder ...
        'PeekaboomOrigin\working\'];
    path_dir = [gl_data_parent_folder ...
        'PeekaboomOrigin\result\'];
elseif (strcmp(type, 'sift_1m'))
    path_dir = [gl_data_parent_folder ...
        'SIFT\result\'];
    path_working = [gl_data_parent_folder ...
        'SIFT\working\'];
elseif strcmp(type, 'SIFTOrigin')
    path_dir = [gl_data_parent_folder ...
        'SIFTOrigin\result\'];
    path_working = [gl_data_parent_folder ...
        'SIFTOrigin\working\'];
elseif strcmp(type, 'CIFAR10')
    path_dir = [gl_data_parent_folder ...
        'CIFAR10\result\'];
    path_working = [gl_data_parent_folder ...
        'CIFAR10\working\'];
elseif (strcmp(type, 'notre'))
    path_dir = [gl_data_parent_folder 'result\Notre\'];
elseif (strcmp(type, 'tiny'))
    path_dir = [gl_data_parent_folder 'result\tiny\'];
elseif (strcmp(type, 'ForQuantize'))
    path_dir = [gl_data_parent_folder 'ForQuantize\result\'];
elseif strcmp(type, 'GIST1M3')
    path_dir = [gl_data_parent_folder 'GIST1M3\result\'];
    path_working =  [gl_data_parent_folder 'GIST1M3\working\'];
elseif strcmp(type, 'SIFT1M3')
    path_dir = [gl_data_parent_folder 'SIFT1M3\result\'];
    path_working =  [gl_data_parent_folder 'SIFT1M3\working\'];
elseif strcmp(type, 'Tiny80M')
    path_dir = [gl_data_parent_folder 'Tiny80M\result\'];
    path_working =  [gl_data_parent_folder 'Tiny80M\working\'];
else
%     error('no valid type');
path_dir = [gl_data_parent_folder type '\result\'];
path_working =  [gl_data_parent_folder type '\working\'];
end

folder_figure = [path_dir 'figure\'];

if (exist('prefix', 'var'))
    
    path_working = [path_working prefix];
    
else
    curr_time = clock;
    
    path_working = [path_working ...
        num2str(curr_time(1)) '_' ...
        num2str(curr_time(2)) '_' ...
        num2str(curr_time(3)) '_' ...
        num2str(curr_time(4)) '_' ...
        num2str(curr_time(5)) '_' ...
        num2str(floor(curr_time(6))) '_'];
end

save_file.train_cbh = [path_working 'train_cbh' num2str(m) '_' num2str(suffix)];
save_file.test_cbh = [path_working 'test_cbh' num2str(m) '_' num2str(suffix)];

save_file.train_classification = [path_working 'train_classification' num2str(m) '_' num2str(suffix)];
save_file.test_classification = [path_working 'test_classification' num2str(m) '_' num2str(suffix)];

save_file.train_classification_k = [path_working 'train_classification_k' num2str(m) '_' num2str(suffix)];
save_file.test_classification_k = [path_working 'test_classification_k' num2str(m) '_' num2str(suffix)];


save_file.train_classification_one_level = [path_working 'train_classification_one_level' num2str(m) '_' num2str(suffix)];
save_file.test_classification_one_level = [path_working 'test_classification_one_level' num2str(m) '_' num2str(suffix)];

save_file.train_bre = [path_working 'train_bre' num2str(m) '_' num2str(suffix)];
save_file.test_bre = [path_working 'test_bre' num2str(m) '_' num2str(suffix)];

save_file.train_ch = [path_working 'train_ch' num2str(m) '_' num2str(suffix)];
save_file.test_ch = [path_working 'test_ch' num2str(m) '_' num2str(suffix)];

save_file.train_lsh = [path_working 'train_lsh' num2str(m) '_' num2str(suffix)];
save_file.test_lsh = [path_working 'test_lsh' num2str(m) '_' num2str(suffix)];

% save_file.train_mlh = [path_dir 'train_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
save_file.train_mlh = [path_working 'train_mlh' num2str(m) '_' num2str(suffix)];
save_file.test_mlh = [path_working 'test_mlh' num2str(m) '_' num2str(suffix)];

save_file.train_mlh2 = [path_working 'train_mlh2' num2str(m) '_' num2str(suffix)];

save_file.test_mlh2 = [path_working 'test_mlh2' num2str(m) '_' num2str(suffix)];


save_file.train_mlh1k = [path_working 'train_mlh1k' num2str(m) '_' num2str(suffix)];
save_file.test_mlh1k = [path_working 'test_mlh1k' num2str(m) '_' num2str(suffix)];

save_file.train_sh = [path_working 'train_sh' num2str(m) '_' num2str(suffix)];
save_file.test_sh = [path_working 'test_sh' num2str(m) '_' num2str(suffix)];

save_file.train_usplh = [path_working 'train_usplh' num2str(m) '_' num2str(suffix)];
save_file.test_usplh = [path_working 'test_usplh' num2str(m) '_' num2str(suffix)];

save_file.train_itq = [path_working 'train_itq' num2str(m) '_' num2str(suffix)];
save_file.test_itq = [path_working 'test_itq' num2str(m) '_' num2str(suffix)];

save_file.train_ksh = [path_working 'train_ksh' num2str(m) '_' num2str(suffix)];
save_file.test_ksh = [path_working 'test_ksh' num2str(m) '_' num2str(suffix)];


save_file.train_c_bre_init = [path_working 'train_c_bre_init' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_1000 = ['train_c_bre_1000' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_1000 = ['train_c_bre_1000' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_1002 = ['train_c_bre_1002' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_1002 = ['train_c_bre_1002' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2000 = ['train_c_bre_2000' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2000 = ['train_c_bre_2000' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2100 = ['train_c_bre_2100' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2100 = ['train_c_bre_2100' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2110 = ['train_c_bre_2110' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2110 = ['train_c_bre_2110' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2001 = ['train_c_bre_2001' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2001 = ['train_c_bre_2001' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2101 = ['train_c_bre_2101' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2101 = ['train_c_bre_2101' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2111 = ['train_c_bre_2111' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2111 = ['train_c_bre_2111' num2str(m) '_' num2str(suffix)];


save_file.train_c_bre_2002 = ['train_c_bre_2002' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2002 = ['train_c_bre_2002' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2102 = ['train_c_bre_2102' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2102 = ['train_c_bre_2102' num2str(m) '_' num2str(suffix)];

save_file.train_c_bre_2112 = ['train_c_bre_2112' num2str(m) '_' num2str(suffix)];
save_file.test_c_bre_2112 = ['train_c_bre_2112' num2str(m) '_' num2str(suffix)];

save_file.train_linear_bre = [path_working 'train_linear_bre' num2str(m) '_' num2str(suffix)];
save_file.test_linear_bre = [path_working 'test_linear_bre' num2str(m) '_' num2str(suffix)];

save_file.train_isohash_lp = [path_working 'train_isohash_lp' num2str(m) '_' num2str(suffix)];
save_file.test_isohash_lp = [path_working 'test_isohash_lp' num2str(m) '_' num2str(suffix)];

save_file.train_isohash_gf = [path_working 'train_isohash_gf' num2str(m) '_' num2str(suffix)];
save_file.test_isohash_gf = [path_working 'test_isohash_gf' num2str(m) '_' num2str(suffix)];

save_file.train_agh_one = [path_working 'train_agh_one' num2str(m) '_' num2str(suffix)];
save_file.test_agh_one = [path_working 'test_agh_one' num2str(m) '_' num2str(suffix)];

save_file.train_agh_two = [path_working 'train_agh_two' num2str(m) '_' num2str(suffix)];
save_file.test_agh_two = [path_working 'test_agh_two' num2str(m) '_' num2str(suffix)];

save_file.train_mbq = [path_working 'train_mbq' num2str(m) '_' num2str(suffix)];
save_file.test_mbq = [path_working 'test_mbq' num2str(m) '_' num2str(suffix)];

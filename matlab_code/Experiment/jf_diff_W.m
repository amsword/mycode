function P = jf_diff_W(W1, W2)
P = inv(W1' * W1) * (W1' * W2);
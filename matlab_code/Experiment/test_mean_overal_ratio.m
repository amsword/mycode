 Xtest = read_mat(src_file.c_test, 'double');
 
 Xbase = read_mat(src_file.c_base, 'double');
 %%
[~, ~, TrueDistanceTestBaseGnd] = read_gnd(gnd_file.STestBase, 10000, 'double');

%%
x = load(save_file.train_mlh, 'W_mlh');
W_mlh = x.W_mlh;

mean_ratio = eval_distance_ratio_hamming(size(W_mlh, 1), 'linear', ...
     W_mlh', Xtest, Xbase, TrueDistanceTestBaseGnd, metric_info);

 mean_ratio = real(mean_ratio);
 %
save(save_file.test_mlh, 'mean_ratio', '-append');

%%
x = load(save_file.train_itq, 'W_itq');
W_itq = x.W_itq;


mean_ratio = eval_distance_ratio_hamming(size(W_itq, 2), 'linear', ...
    W_itq, Xtest, Xbase, TrueDistanceTestBaseGnd, metric_info);

mean_ratio = real(mean_ratio);
%%
save(save_file.test_itq, 'mean_ratio', '-append');

%%

x = load(save_file.train_bre, 'trained_bre');
trained_bre = x.trained_bre;

mean_ratio = eval_distance_ratio_hamming(size(trained_bre.W, 2), ...
    'bre', trained_bre, ...
    Xtest, Xbase, TrueDistanceTestBaseGnd, ...
    metric_info);
mean_ratio = real(mean_ratio);
%
save(save_file.test_bre,  'mean_ratio', '-append');


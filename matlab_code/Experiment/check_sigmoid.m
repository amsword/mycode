x = 10;

beta = [0 : 0.01 : 2];
y = zeros(numel(beta, 1));
for i = 1 : length(beta)
    y(i) = beta(i) * sigmf(x, [beta(i), 0]) * (1 - sigmf(x, [beta(i), 0]));
end

sp = 1.5434 / x;
plot(beta, y, '.');
hold on;
plot(sp, sp * sigmf(x, [sp, 0]) * (1 - sigmf(x, [sp, 0])), 'o');
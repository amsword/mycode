
N = 20019;
v = zeros(1, 1000);
for M = 1 : 1000
    v(M) = log(N - M) - log(N) + M / (N - M);
end
plot(v);
%%
M = fsolve(@(M)(log(N - M) - log(N) + M / (N - M)), 100)
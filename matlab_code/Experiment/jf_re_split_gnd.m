Nbase = 10^6;
Ntest = 10^4;
save_gnd_seed1 = 'F:\v-jianfw\Data_HashCode\SIFT\data\TestBaseSeed';
save_gnd_seed2 = 'F:\v-jianfw\Data_HashCode\SIFT\data\new\TestBaseSeed';

max_size = 4 * 1024^3 / 8;
batchsize = min(floor(max_size / Nbase), Ntest);
nbath = ceil(Ntest / batchsize);

n_max_size = 1 * 1024^3 / 8;
n_batchsize = min(floor(n_max_size / Nbase), Ntest);
n_nbath = ceil(Ntest / n_batchsize);

av_idx = [];
av_DtestBase = [];
av_sortedD = [];
av_idx_start = 1;
% av_idx_end;
for i = 1 : nbath
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, Ntest);
      
    file_name = jf_gen_gnd_file(save_gnd_seed1, idx_start, idx_end);
    load(file_name, 'idx', 'DtestBase', 'sortedD', 'idx_start', 'idx_end');
    
    av_idx = [av_idx; idx];
    av_DtestBase = [av_DtestBase; DtestBase];
    av_sortedD = [av_sortedD; sortedD];
    av_idx_end = idx_end;
    
    ['start ' num2str(i) ' batch of ' num2str(nbath)]
    while(1)
        idx_start = av_idx_start;
        idx_end = idx_start + n_batchsize - 1;
        idx_end = min(idx_end, Ntest);
        if (av_idx_end < idx_end)
            break;
        else
            idx = av_idx(1 : idx_end - idx_start + 1, :);
            av_idx(1 : idx_end - idx_start + 1, :) = [];
            
            DtestBase = av_DtestBase(1 : idx_end - idx_start + 1, :);
            av_DtestBase(1 : idx_end - idx_start + 1, :) = [];
            
            sortedD = av_sortedD(1 : idx_end - idx_start, :);
            av_sortedD(1 : idx_end - idx_start, :) = [];
            
            file_name = jf_gen_gnd_file(save_gnd_seed2, idx_start, idx_end);
            save(file_name, 'idx', 'DtestBase', 'sortedD', 'idx_start', 'idx_end', '-v7.3');
            av_idx_start = idx_end + 1;
        end
    end
end
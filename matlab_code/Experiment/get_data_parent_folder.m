function gl_data_parent_folder = get_data_parent_folder()
% curr_file = mfilename('fullpath');
% idx = find(curr_file == '\', 2, 'last');
% root_path = curr_file(1 : idx);
% str = [root_path 'Data_HashCode\'];


machine_name = getComputerName();
machine_name(end) = [];
if strcmp(machine_name, 'dewberry')
    gl_data_parent_folder = '\\dragon\Data\Jianfeng\Data_HashCode\';
elseif strcmp(machine_name, 'dragon')
    gl_data_parent_folder = 'E:\Data\Jianfeng\Data_HashCode\';
else
    gl_data_parent_folder = 'H:\Data_HashCode\';
    
end
function [gnd_file] = jf_gnd_file_name(type, m, topK_train, topK_base)

global gl_data_parent_folder;

if (strcmp(type, 'Labelme'))
    data_path = [gl_data_parent_folder ...
        'Labelme\data\'];
elseif (strcmp(type, 'LabelmeOrigin'))
    data_path = [gl_data_parent_folder ...
        'LabelmeOrigin\data\'];
elseif (strcmp(type, 'peekaboom'))
    data_path = [gl_data_parent_folder ...
        'Peekaboom\data\'];
elseif strcmp(type, 'PeekaboomOrigin')
    data_path = [gl_data_parent_folder ...
        'PeekaboomOrigin\data\'];
elseif (strcmp(type, 'sift_1m'))
    data_path = [gl_data_parent_folder ...
        'SIFT\data\'];
elseif strcmp(type, 'SIFTOrigin')
    data_path = [gl_data_parent_folder ...
        'SIFTOrigin\data\'];
elseif (strcmp(type, 'notre'))
    data_path = [gl_data_parent_folder 'NotreDame\data\'];
elseif (strcmp(type, 'tiny'))
    data_path = [gl_data_parent_folder 'tiny1M\data\'];
elseif (strcmp(type, 'ForQuantize'))
    data_path = [gl_data_parent_folder 'ForQuantize\data\'];
elseif strcmp(type, 'CIFAR10')
    data_path = [gl_data_parent_folder 'CIFAR10\data\'];
elseif strcmp(type, 'GIST1M')
    data_path = [gl_data_parent_folder 'GIST1M\data\'];
elseif strcmp(type, 'GIST1M3')
    data_path = [gl_data_parent_folder 'GIST1M3\data\'];
elseif strcmp(type, 'SIFT1M3')
    data_path = [gl_data_parent_folder 'SIFT1M3\data\'];
elseif strcmp(type, 'Tiny80M')
    data_path = [gl_data_parent_folder 'Tiny80M\data\'];
else
    data_path = [gl_data_parent_folder type '\data\'];
%     error('invalid type');
end

gnd_file.GndValidationInLearn = [data_path 'GndValidationLearn' '.double.bin'];

gnd_file.MLH_Strainingtraining = [data_path 'MLH_Strainingtraining_Knn_topK' num2str(topK_train) '.mat'];
% gnd_file.Strainingtraining = [data_path 'rStrainingtrainingKnn_m' num2str(m) '_rho' num2str(rho) '.mat'];
% gnd_file.RankSortIdxStrainingtraining = [data_path 'RankSortIdxStrainingtraining_m' num2str(m)];
gnd_file.RankStrainingtraining = [data_path 'StrainingtrainingKnn_m', num2str(m) '.mat'];
gnd_file.RankSQuerytrainingtraining = [data_path 'RankSQuerytrainingtraining_m', num2str(m) '.mat'];
gnd_file.ClassifyStrainingtraining = [data_path 'ClassifyStrainingtraining_' num2str(m) '.mat'];
gnd_file.ClassifyStrainingtrainingAll = [data_path 'ClassifyStrainingtraining.mat'];
% gnd_file.StestTraining = [data_path 'StestTrainingKnn_topK' num2str(topK_base) '.mat'];
gnd_file.StestTrainingBin = [data_path 'STestTraining.double.bin'];
gnd_file.RankStestTrainingNDCG = [data_path 'StestTrainingNDCG_m' num2str(m) '.mat'];
gnd_file.RankStestTrainingNDCG = [data_path 'StestTrainingNDCG_m10.mat'];
gnd_file.TestBaseSeed = [data_path 'TestBaseSeed'];
gnd_file.DistanceTestTrainBin = [data_path 'DistanceTestTrainBin'];
gnd_file.SortedDistanceTestTrainBin = [data_path 'SortedDistanceTestTrainBin'];
gnd_file.SortedDistanceTrainTrainBin = [data_path 'c_data\SortedDistanceTrainTrain.double.bin'];
gnd_file.DistanceTrainTrainBin = [data_path 'c_data\DistanceTrainTrain.double.bin'];
gnd_file.SortedPartTrainTrain = [data_path 'STrainTrain400.double.bin'];
gnd_file.STestBase = [data_path 'STestBase.double.bin'];
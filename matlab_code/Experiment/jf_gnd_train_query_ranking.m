function [exp_dist avg_num_neighbour all_s_idx] = ...
    jf_gnd_train_query_ranking(X1, X2, m)
%% I: for cbh
%   mlh_I, for mlh

N1 = size(X1, 2);
N2 = size(X2, 2);

max_size = 0.5 * 1024^3 / 8;
batchsize = min(floor(max_size / N2), N1);
nbath = ceil(N1 / batchsize);

exp_dist = zeros(N1, N2, 'int8');

if (nargout == 3)
	all_s_idx = zeros(N1, N2, 'int32');
end
%% prepare the number of neighbors in different grades.
avg_num_neighbour = jf_calc_avg_num_neighbor([0 : m], m, N2);
avg_num_neighbour(m + 1) = N2;
%

for i = 1 : nbath
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, N1);
 
	dist = jf_distMat(X1(:, idx_start : idx_end), X2);
    [~, s_idx] = sort(dist, 2);
	if (nargout == 3)
		all_s_idx(idx_start : idx_end, :) = s_idx;
    end
	for k = 1 : (m + 1)
		if (k == 1)
			pre = 1;
		else
			pre = avg_num_neighbour(k - 1) + 1;
        end
        
        if (avg_num_neighbour(k) < pre)
            continue;
        end
        
		idx = bsxfun(@plus, (s_idx(:, pre : avg_num_neighbour(k)) - 1) * N1, ...
            [idx_start: idx_end]');
		exp_dist(idx(:)) = k - 1;
    end
    ['bath ' num2str(i) '/' num2str(nbath)]
end
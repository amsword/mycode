clear pre_fix;
k = 1;

pre_fix{k} = '2012_4_30_17_23_15_';
codes(k) = 6;
k = k + 1;

pre_fix{k} = '2012_5_1_17_56_31_';
codes(k) = 8;
k = k + 1;

pre_fix{k} = '2012_5_3_18_12_14_';
codes(k) = 10;
k = k + 1;

pre_fix{k} = '2012_5_6_14_34_1_';
codes(k) = 32;
k = k + 1;

pre_fix{k} = '2012_5_4_20_26_16_';
codes(k) = 64;
k = k + 1;

pre_fix{k} = '2012_5_8_14_40_35_';
codes(k) = 96;
k = k + 1;

type = 'labelme'; % peekaboom; labelme; sift_1m;

global gl_data_parent_folder;
gl_data_parent_folder = 'F:\v-jianfw\Data_HashCode\';

%%
clear cut_dist_lookup_mlh;
k = 3;
for i = 1 : numel(pre_fix)
    [save_file] = jf_train_save_file(type, codes(i), false, pre_fix{i});
    
    load(save_file.test_sh, 'eval_sh');
    load(save_file.test_mlh, 'eval_mlh');
    load(save_file.test_usplh, 'eval_usplh');
    load(save_file.test_lsh, 'eval_lsh');
    load(save_file.test_ch, 'eval_ch');
    load(save_file.test_bre, 'eval_bre');
    load(save_file.test_cbh, 'eval_cbh');
    
    if (numel(eval_cbh.rec) ~= codes(i) + 1)
        error('error');
    end
    
%     map_lookup_sh(i) = jf_calc_map(eval_sh.rec, eval_sh.pre);
%     map_lookup_mlh(i) = jf_calc_map(eval_mlh.rec, eval_mlh.pre);
%     map_lookup_usplh(i) = jf_calc_map(eval_usplh.rec, eval_usplh.pre);
%     map_lookup_lsh(i) = jf_calc_map(eval_lsh.rec, eval_lsh.pre);
%     map_lookup_ch(i) = jf_calc_map(eval_ch.rec, eval_ch.pre);
%     map_lookup_bre(i) = jf_calc_map(eval_bre.rec, eval_bre.pre);
%     map_lookup_cbh(i) = jf_calc_map(eval_cbh.rec, eval_cbh.pre);
%     
%     map_ranking_sh(i) = jf_calc_map(eval_sh.r_code, eval_sh.p_code);
%     map_ranking_mlh(i) = jf_calc_map(eval_mlh.r_code, eval_mlh.p_code);
%     map_ranking_usplh(i) = jf_calc_map(eval_usplh.r_code, eval_usplh.p_code);
%     map_ranking_lsh(i) = jf_calc_map(eval_lsh.r_code, eval_lsh.p_code);
%     map_ranking_ch(i) = jf_calc_map(eval_ch.r_code, eval_ch.p_code);
%     map_ranking_bre(i) = jf_calc_map(eval_bre.r_code, eval_bre.p_code);
%     map_ranking_cbh(i) = jf_calc_map(eval_cbh.r_code, eval_cbh.p_code);
    
    cut_dist_lookup_sh(i) = eval_sh.pre(k);
    cut_dist_lookup_mlh(i) = eval_mlh.pre(k);
    cut_dist_lookup_usplh(i) = eval_usplh.pre(k);
    cut_dist_lookup_lsh(i) = eval_lsh.pre(k);
    cut_dist_lookup_ch(i) = eval_ch.pre(k);
    cut_dist_lookup_bre(i) = eval_bre.pre(k);
    cut_dist_lookup_cbh(i) = eval_cbh.pre(k);
        
end
%%
save('map', ...
    'map_lookup_sh', 'map_lookup_mlh',...
    'map_lookup_usplh', 'map_lookup_lsh',...
    'map_lookup_ch', 'map_lookup_bre',...
    'map_lookup_cbh',...
    'map_ranking_sh', 'map_ranking_mlh',...
    'map_ranking_usplh', 'map_ranking_lsh',...
    'map_ranking_ch', 'map_ranking_bre',...
    'map_ranking_cbh');

%%
load('map');
%%
plot1 = plot(1 : numel(codes), map_ranking_cbh,...
    1 : numel(codes), map_ranking_sh,...
    1 : numel(codes), map_ranking_mlh,...
    1 : numel(codes), map_ranking_usplh, ...
    1 : numel(codes), map_ranking_lsh,...
    1 : numel(codes), map_ranking_ch, ...
    1 : numel(codes), map_ranking_bre);

k = 1;
set(plot1(k),'Marker','s', 'Color', [1 0 0]);
k = k + 1;
set(plot1(k),'Marker','v', 'Color',[0 0 0]);
k = k + 1;
set(plot1(k),'Marker','o','Color',[0 0 1]);
k = k + 1;
set(plot1(k),'Marker','.','Color',[0 1 1]);
k = k + 1;
set(plot1(k),'Marker','<','Color',[0 1 0]);
k = k + 1;
set(plot1(k),'Marker','diamond','Color',[0.8 0.4 0.2]);
k = k + 1;
set(plot1(k),'Marker','hexagram','Color',[1 0 1]);

legend('RSH', 'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
    'Location', 'Best');

% Create xlabel
xlabel('Code length','FontSize',14);

% Create ylabel
ylabel('MAP','FontSize',14);
set(gca, 'FontSize', 14);
set(gca,'XTickLabel',{'6','8','10','32','64', '96'});

saveas(gca, [type '_ranking_map.fig']);
saveas(gca, [type '_ranking_map.eps'], 'psc2');

%%
plot1 = plot(1 : numel(codes), map_lookup_cbh,...
    1 : numel(codes), map_lookup_sh,...
    1 : numel(codes), map_lookup_mlh,...
    1 : numel(codes), map_lookup_usplh, ...
    1 : numel(codes), map_lookup_lsh,...
    1 : numel(codes), map_lookup_ch, ...
    1 : numel(codes), map_lookup_bre);

k = 1;
set(plot1(k),'Marker','s', 'Color', [1 0 0]);
k = k + 1;
set(plot1(k),'Marker','v', 'Color',[0 0 0]);
k = k + 1;
set(plot1(k),'Marker','o','Color',[0 0 1]);
k = k + 1;
set(plot1(k),'Marker','.','Color',[0 1 1]);
k = k + 1;
set(plot1(k),'Marker','<','Color',[0 1 0]);
k = k + 1;
set(plot1(k),'Marker','diamond','Color',[0.8 0.4 0.2]);
k = k + 1;
set(plot1(k),'Marker','hexagram','Color',[1 0 1]);

% Create xlabel
xlabel('Code length','FontSize',14);

% Create ylabel
ylabel('MAP','FontSize',14);
legend('RSH', 'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
    'Location', 'Best');
set(gca, 'FontSize', 14);
set(gca,'XTickLabel',{'6','8','10','32','64', '96'});

saveas(gca, [type '_lookup_map.fig']);
saveas(gca, [type '_lookup_map.eps'], 'psc2');

%%

plot1 = plot(1 : numel(codes), cut_dist_lookup_cbh,...
    1 : numel(codes), cut_dist_lookup_sh,...
    1 : numel(codes), cut_dist_lookup_mlh,...
    1 : numel(codes), cut_dist_lookup_usplh, ...
    1 : numel(codes), cut_dist_lookup_lsh,...
    1 : numel(codes), cut_dist_lookup_ch, ...
    1 : numel(codes), cut_dist_lookup_bre);

k = 1;
set(plot1(k),'Marker','s', 'Color', [1 0 0]);
k = k + 1;
set(plot1(k),'Marker','v', 'Color',[0 0 0]);
k = k + 1;
set(plot1(k),'Marker','o','Color',[0 0 1]);
k = k + 1;
set(plot1(k),'Marker','.','Color',[0 1 1]);
k = k + 1;
set(plot1(k),'Marker','<','Color',[0 1 0]);
k = k + 1;
set(plot1(k),'Marker','diamond','Color',[0.8 0.4 0.2]);
k = k + 1;
set(plot1(k),'Marker','hexagram','Color',[1 0 1]);

% Create xlabel
xlabel('Code length','FontSize',14);

% Create ylabel
ylabel('MAP','FontSize',14);
legend('RSH', 'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE');
set(gca, 'FontSize', 14);
set(gca,'XTickLabel',{'6','8','10','32','64', '96'});
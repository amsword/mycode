function [gnd_file] = gnd_file_name(type, m, topK_train)

global gl_data_parent_folder;

if (strcmp(type, 'labelme'))
    data_path = [gl_data_parent_folder ...
        'Labelme\data\'];
elseif (strcmp(type, 'labelme_origin'))
    data_path = [gl_data_parent_folder ...
        'LabelmeOrigin\data\'];
elseif (strcmp(type, 'peekaboom'))
    data_path = [gl_data_parent_folder ...
        'Peekaboom\data\'];
elseif (strcmp(type, 'sift_1m'))
    data_path = [gl_data_parent_folder ...
        'SIFT\data\'];
elseif (strcmp(type, 'notre'))
    data_path = [gl_data_parent_folder 'NotreDame\data\'];
elseif (strcmp(type, 'tiny'))
    data_path = [gl_data_parent_folder 'tiny1M\data\'];
elseif (strcmp(type, 'ForQuantize'))
    data_path = [gl_data_parent_folder 'ForQuantize\data\'];
else
    error('invalid type');
end

gnd_file.MLH_Strainingtraining = [data_path 'MLH_Strainingtraining_Knn_topK' num2str(topK_train) '.mat'];
% gnd_file.Strainingtraining = [data_path 'rStrainingtrainingKnn_m' num2str(m) '_rho' num2str(rho) '.mat'];
% gnd_file.RankSortIdxStrainingtraining = [data_path 'RankSortIdxStrainingtraining_m' num2str(m)];
gnd_file.RankStrainingtraining = [data_path 'StrainingtrainingKnn_m', num2str(m) '.mat'];
gnd_file.RankSQuerytrainingtraining = [data_path 'RankSQuerytrainingtraining_m', num2str(m) '.mat'];
gnd_file.ClassifyStrainingtraining = [data_path 'ClassifyStrainingtraining_' num2str(m) '.mat'];
gnd_file.ClassifyStrainingtrainingAll = [data_path 'ClassifyStrainingtraining.mat'];
% gnd_file.StestTraining = [data_path 'StestTrainingKnn_topK' num2str(topK_base) '.mat'];
gnd_file.StestTrainingBin = [data_path 'STestTraining.double.bin'];
gnd_file.RankStestTrainingNDCG = [data_path 'StestTrainingNDCG_m' num2str(m) '.mat'];
gnd_file.RankStestTrainingNDCG = [data_path 'StestTrainingNDCG_m10.mat'];
gnd_file.TestBaseSeed = [data_path 'TestBaseSeed'];
Ntraining

num1 = 10;
num2 = 100;
num3 = 500;

[~, idx1] = read_gnd('STrainTrain500.double.bin', num1, 'double');
[~, idx2] = read_gnd('STrainTrain500.double.bin', num2, 'double');
[~, idx3] = read_gnd('STrainTrain500.double.bin', num3, 'double');

idx1 = idx1';
idx2 = idx2';
idx3 = idx3';

idx1 = idx1 + 1;
idx2 = idx2 + 1;
idx3 = idx3 + 1;

I = cell(3, 1);

assert(Ntraining == size(idx1, 1), [num2str(size(idx1, 1))]);
assert(num1 == size(idx1, 2), [num2str(size(idx1, 2))]);
is = repmat([1 : Ntraining]', 1, num1);
I{1} = sparse(is(:), double(idx1(:)), true, Ntraining, Ntraining);


assert(Ntraining == size(idx2, 1), [num2str(size(idx2, 1))]);
assert(num2 == size(idx2, 2), [num2str(size(idx2, 2))]);
is = repmat([1 : Ntraining]', 1, num2);
I{2} = sparse(is(:), double(idx2(:)), true, Ntraining, Ntraining);

assert(Ntraining == size(idx1, 1));
assert(num3 == size(idx3, 2));
is = repmat([1 : Ntraining]', 1, num3);
I{3} = sparse(is(:), double(idx3(:)), true, Ntraining, Ntraining);

rhos = [num1 num2 num3];
save('ClassifyStrainingtraining.mat', 'rhos', 'I', '-v7.3');

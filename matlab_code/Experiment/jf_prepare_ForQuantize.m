function jf_prepare_ForQuantize(save_file)
fid = fopen('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\ForQuantize\src_data\1.70f', 'r');
m = fread(fid, 1, 'int32');
n = fread(fid, 1, 'int32');
data = fread(fid, [n m], 'float32');
data = double(data);
fclose(fid);

test_idx = randperm(m, 10 * 10^3);
train_idx = setdiff([1 : m], test_idx);

OriginXtest = data(:, test_idx);
OriginXtraining = data(:, train_idx);
read_me = 'from 1.70f';
save(save_file.train, 'train_idx', 'OriginXtraining', 'read_me');
save(save_file.test, 'test_idx', 'OriginXtest', 'read_me');
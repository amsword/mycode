x  = load(save_file.train_classification);

classify_para = x.para_out;
if isfield(classify_para, 'all_W')
    classify_para = rmfield(classify_para, 'all_W');
end
if isfield(classify_para, 'exact_obj')
    classify_para = rmfield(classify_para, 'exact_obj');
end
if isfield(classify_para, 'pre_deltaW')
    classify_para = rmfield(classify_para, 'pre_deltaW');
end
ta = classify_para.dk;

clear all_alpha;
i = 1;
all_alpha(i, :) = [1, 1, 1, 1]; i = i + 1;

if isfield(classify_para, 'gnd_selected')
    gnd_selected = classify_para.gnd_selected;
elseif numel(ta) == 3
    gnd_selected = [100, 200, 400];
else
    error('dfsd');
end
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
classify_para.gnd_selected = gnd_selected;
['complete loading classify gnd']

tic;
all_mus = 0;
all_lambda = [0.1 0.05 0.01];

clear all_rhos;

clear all_zero_bias;
all_zero_bias = [1];

i = 1;

all_rate0 = 10 .^ [-1];

num_rhos = size(ta, 1);
num_mus = numel(all_mus);
% matlabpool open
% for i = 1 : numel(all_rhos)
gamma = zeros(numel(ta), 2);
gamma(:) = 1.0 / m;
classify_para.gamma = gamma;

classify_para.momentum = 0.9;
classify_para.is_learning_rate_fixed = 1;

%%

val_num = Ntraining * 0.05;
val_num = round(val_num);

val_idx_test = randperm(Ntraining, val_num);
val_idx_train = setdiff([1 : Ntraining], val_idx_test);
val_test = Xtraining(:, val_idx_test);
val_train = Xtraining(:, val_idx_train);

X = [val_train; ones(1, numel(val_idx_train))];
I2 = cell(numel(I), 1);
for i = 1 : numel(I)
    I2{i} = I{i}(val_idx_train, val_idx_train);
end
StestBase = I{1}(val_idx_test, val_idx_train);
StestBase2 = cell(numel(val_idx_test), 1);

for i = 1 : numel(val_idx_test)
    StestBase2{i} = find(StestBase(i, :));
end

%
initW = [randn(size(Xtraining, 1), m); zeros(1, m)];
sw = sum(initW .^ 2, 1);
sw = sqrt(sw);
initW = bsxfun(@rdivide, initW, sw);


%%
% generate parameter set
p = 0;
i = 1; mu = all_mus(i);
i2 = 1; alpha = all_alpha(i2, :);
idx_rate0 = 1; rate0 = all_rate0(idx_rate0);
idx_zero_bias = 1; is_bias = all_zero_bias(idx_zero_bias);
classify_para.mu = mu;
classify_para.alpha = alpha;
classify_para.type_train = 10;
classify_para.max_iter = 500;
classify_para.is_zero_bias = is_bias;
classify_para.beta = 40;
classify_para.max_search_mu_times = 0;
classify_para.is_learning_rate_fixed = 2;
classify_para.learning_rate = rate0;
classify_para.num_center_sample = 200;
classify_para.need_add_1_data = 0;
classify_para.dk = ta;
classify_para.max_time = 3600 * 24;
%%
clear best_result2 
p = 0;

best_score = 0;
best_lambda = classify_para.lambda;
best_dk = ta;
best_W = initW;

clear all_dk
st = m / 32;
st = round(st);
i = 1;
all_dk(i, :) = ta + st; i = i + 1;
all_dk(i, :) = ta - st; i = i + 1;
all_dk(i, :) = ta; i = i + 1;

initW = best_W;
for k = 1 : size(all_dk, 1)
    para = classify_para;
    lambda = best_lambda;
    dk = all_dk(k, :);
   
    para.lambda = lambda;
    para.dk = dk;
       
    [W para_out] = jf_train_cbh_ranking(X, [], I2, para, initW);
    p = p + 1;
    best_result2{p}.W = W;
    best_result2{p}.para_out = para_out;
    
    curr_eval = eval_hash7(size(W, 2), 'linear', W, ...
        val_test, val_train, ...
        StestBase2', [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    score = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    best_result2{p}.curr_eval = curr_eval;
    
    if score > best_score
        best_score = score;
        best_W = W;
        best_para_out = para_out;
        best_lambda = lambda;
        best_dk = all_dk(k);
    end
end

save(['best_result2_' type '_' num2str(m)], ...
    'best_result2');

%%
all_lambda = [0.1, 0.05, 0.01];
initW = best_W;
for k = 1 : numel(all_lambda)
    para = classify_para;
    lambda = all_lambda(k);
    dk = best_dk;
    
    para.lambda = lambda;
    para.dk = dk;
       
    [W para_out] = jf_train_cbh_ranking(X, [], I2, para, initW);
    p = p + 1;
    best_result2{p}.W = W;
    best_result2{p}.para_out = para_out;
    
    curr_eval = eval_hash7(size(W, 2), 'linear', W, ...
        val_test, val_train, ...
        StestBase2', [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    score = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    best_result2{p}.curr_eval = curr_eval;
    
    if score > best_score
        best_score = score;
        best_W = W;
        
        best_para_out = para_out;
        best_lambda = lambda;
        best_dk = all_dk(k);
    end
end
save(['best_result2_' type '_' num2str(m)], ...
    'best_result2');

%%

W = best_W;
para_out = best_para_out;

Xtest = read_mat(src_file.c_test, 'double');
StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

eval_classification = cell(numel(all_topks), 1);
for k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    eval_classification{k} = curr_eval;
end

save([type '_' num2str(m)], ...
    'best_result2', 'W', 'eval_classification', 'para_out');

%% find the best result in best_result

mlhs = load(save_file.test_mlh);
cbh = load(save_file.test_classification);
color_type{1} = 'r-o';
color_type{2} = 'k-d';
color_type{3} = 'g-<';
color_type{4} = 'c->';

color_type{1} = 'r-';
color_type{2} = 'k-';
color_type{3} = 'g-';
color_type{4} = 'c-';

for k = 1 : numel(all_topks)
    figure;
    
    best_v = 0;
    best_idx = -1;
    
    semilogx(cbh.eval_classification{k}.avg_retrieved, cbh.eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
    hold on;
    xlim([10, 1000]); 
    semilogx(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 'k-d', 'LineWidth', 2);
    semilogx(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2);
    grid on;

    legend('OPH', 'OPH\_val', 'MLH', 'Location', 'Best');
    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    saveas(gca, [num2str(all_topks(k)) '.eps'], 'psc2');
    
end
pause;
close all;


x = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_8_14_20_train_classification24_0.mat');

idx = randperm(24, 16);
W = x.W(:, idx);

%%


%%
ours = cell(numel(all_topks), 1);
for k = 1 : numel(all_topks)

    curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    
    ours{k} = curr_eval;
end
%%
for k = 1 : numel(all_topks)
    mlhs = load(save_file.test_mlh);
    figure;
    plot(mlhs.all_mlhs{k}.eval.r_code, 'b'); hold on;
    plot(ours{k}.r_code, 'r');
    axis([0, 1000, 0, 1]);
end
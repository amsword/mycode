all_eta = [0.1 0.125 0.2 0.3 0.4 0.5 0.6 0.7 0.8];


all_usplh = cell(numel(all_eta), 1);
parfor idx_eta = 1 : numel(all_eta)
    nb = m;
    USPLHparam.nbits = nb;
    USPLHparam.eta=all_eta(idx_eta);
    USPLHparam.c_num=100;
    
    fprintf('start USPLH training\n');
    USPLHparam = trainUSPLH(Xtraining', USPLHparam);
    
    W_usplh = [USPLHparam.w; USPLHparam.b];
    para_usplh = USPLHparam;

    all_usplh{idx_eta}.W_usplh = W_usplh;
    all_usplh{idx_eta}.para_usplh = para_usplh;
end
%
parfor idx_eta = 1 : numel(all_eta)
    W_usplh = all_usplh{idx_eta}.W_usplh;
    for k = 1 : numel(all_topks)
        all_usplh{idx_eta}.eval{k} = eval_hash5(size(W_usplh, 2), 'linear', ...
            W_usplh, Xtest, Xtraining, StestBase2', ...
            all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    end
end

%%
load(save_file.test_lsh, 'eval_lsh');
load(save_file.test_classification, 'eval_classification');
load(save_file.test_usplh, 'eval_usplh');
for k = 1 : numel(all_topks)
    figure;
    plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'b'); hold on;
    plot(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 'g'); 
    
    b = 0;
    idx = -1;
    for i = 1 : numel(all_usplh)
        tmp = jf_calc_map(all_usplh{i}.eval{k}.avg_retrieved, all_usplh{i}.eval{k}.rec);
        if (tmp > b)
            b = tmp;
            idx = i;
        end
%         plot(all_usplh{i}.eval{k}.avg_retrieved, all_usplh{idx}.eval{k}.rec, 'r');
    end
% idx = 2;
% idx = 2;
    idx = 1;
%     plot(all_usplh{idx}.eval{k}.avg_retrieved, all_usplh{idx}.eval{k}.rec, 'r');
%     
    plot(eval_usplh{k}.avg_retrieved, eval_usplh{k}.rec, 'k-o');
    title([num2str(all_topks(k)) '-NN']);
    axis([0 1000 0 1]);
    grid on;
end
pause;
close all;
%%
for k = 1 : numel(all_topks)
    figure;
    plot(eval_lsh{k}.r_code, 'b'); 
    hold on;
     plot(eval_classification{k}.r_code, 'g'); 
    axis([0 1000 0 1]);
    for i = 1 : numel(all_usplh)
        plot(all_usplh{i}.eval{k}.r_code, 'r');
    end
    axis([0 1000 0 1]);
end
%%
idx = 3;
W_usplh = all_usplh{idx}.W_usplh;
para_usplh = all_usplh{idx}.para_usplh;
eval_usplh = all_usplh{idx}.eval;
save(save_file.train_usplh, 'W_usplh', 'para_usplh');
save(save_file.test_usplh, 'eval_usplh');
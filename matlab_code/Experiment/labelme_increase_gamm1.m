% clear result;
% load('result_labelme24_(5,6,8)');
% W = best_result{1}.W;
W = cbhs.W;

dk = [8 9 11];
gamma = zeros(length(dk), 2);
gamma(:, 1) = -2 * log(10^-3) ./ (2);
gamma(:, 2) = 1.5434 ./ ((2 + dk) / 2);
% gamma(:, 2) = -2 * log(0.2) ./ (2 * rd_dk + 1 - rd_dk2);
% para.gamma = gamma;

for k = 1 : 10
    k
    lambda = 1;
    cbh_para2 = cbh_para;
    cbh_para2.gamma = gamma;
    cbh_para2.beta = cbh_para.beta;
    cbh_para2.dk = dk;
    cbh_para2.max_iter = 25;
    cbh_para2.lambda = 10 ^ -3;
    cbh_para2.is_learning_rate_fixed = 0;



        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            cbh_para2, W);
 
    
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
    
    save('result_labelme_24_8_9_11_0.001_from_cbh.mat', 'result');
end
diary(['result_' type num2str(m) '_(5,7,8).txt']);
for k = 1 : 20
    ['k = ' num2str(k) '/20']
    best_para_out = cbh_para;
    best_para_out.max_iter = 100;
    best_para_out.is_learning_rate_fixed = 0;
    ['start best case']
    if (k == 1)
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            best_para_out);
    else
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            best_para_out, W);
    end
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    best_result{k}.eval = curr_eval;
    best_result{k}.W = W;
    best_result{k}.para_out = para_out;
    best_result{k}.ap_recal = jf_calc_map(1 : 10^4, curr_eval.r_code(1 : 10^4));
    save(['result_' type num2str(m) '_(5,7,8)_divide_m.mat'], 'best_result');
end
diary off;
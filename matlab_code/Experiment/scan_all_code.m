mlh12 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_7_35_17_test_mlh12_0.mat');
mlh16 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_7_54_56_test_mlh16_0.mat');
mlh24 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_8_14_20_test_mlh24_0.mat');

%%
cls12 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_7_35_17_test_classification12_0topk400.mat');
cls16 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_7_54_56_test_classification16_0.mat');
cls24 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_18_8_14_20_test_classification24_0topk400.mat');

%%
idx = 500;
plot([12 16 24], [mlh12.eval_mlh.r_code(idx), mlh16.eval_mlh.r_code(idx), mlh24.eval_mlh.r_code(idx)], 'b', ...
    [12 16 24], [cls12.curr_eval.r_code(idx), cls16.result.eval{5}.r_code(idx), cls24.curr_eval.r_code(idx)], 'r');

%%
plot([12 16 24], [mean(mlh12.eval_mlh.r_code(1 : 1000)), mean(mlh16.eval_mlh.r_code(1 : 1000)), mean(mlh24.eval_mlh.r_code(1 : 1000))], 'b', ...
    [12 16 24], [mean(cls12.curr_eval.r_code(1 : 1000)), mean(cls16.result.eval{5}.r_code(1 : 1000)), mean(cls24.curr_eval.r_code(1 : 1000))], 'r');


%%
plot(mlh12.eval_mlh.r_code, 'b'); 
hold on;
plot(cls12.curr_eval.r_code, 'r');
axis([0 1000 0 1]);
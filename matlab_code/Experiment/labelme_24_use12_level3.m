clear result;

k = 1;
best_coef = 1;
best_ap = -1;
best_lambda = 1;
best_W = [];
best_para_out = [];
best_eval = [];
cbh_para.max_iter = 75;

%%
diary([type num2str(m) '_3.txt']);
dks = [6; 8]
for i = 1 : size(dks, 1)
    cbh_para2 = cbh_para;
    cbh_para2.dk = dks(i, :);
    cbh_para2.lambda = best_lambda;
    
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        cbh_para2);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    k = k + 1;
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
    ap = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    result{k}.ap = ap;
    
    if (ap > best_ap)
        best_ap = ap;
        best_W = W;
        best_para_out = para_out;
        best_eval = curr_eval;
    end
    
    save(['labelme_24_use12_level3.mat'], 'result');
end
diary off;
%%
for lambda = [10^-2 0.5 10^-1 1]
    lambda
    cbh_para2 = cbh_para;
    cbh_para2.dk = cbh_para.dk * best_coef;
    cbh_para2.lambda = lambda;
    
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        cbh_para2);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
    ap = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    result{k}.ap = ap;
    if (ap > best_ap)
        best_ap = ap;
        best_lambda = lambda;
        best_W = W;
        best_para_out = para_out;
        best_eval = curr_eval;
    end
    k = k + 1;
    save(['labelme_24_use12_level2.mat'], 'result');
end
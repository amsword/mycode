function result = jf_sum_DCG(Rank, relevance)

[num, Ntrain] = size(Rank);

seq = bsxfun(@plus, (Rank - 1) * num, [1 : num]');

rel = relevance(seq);

% rel = reshape(rel, num, Ntrain);

dcg_1 = zeros(num, Ntrain);
dcg_1(:, 1) = rel(:, 1);
dcg_1(:, 2 : Ntrain) = rel(:, 2 : Ntrain) ./ log2(repmat(2 : Ntrain, num, 1));

dcg_2 = (2 .^ rel - 1) ./ log2(1 + repmat([1 : Ntrain], num, 1));


dcg_1 = cumsum(dcg_1, 2);
dcg_2 = cumsum(dcg_2, 2);


result.s_dcg1 = sum(dcg_1, 1);
result.s_dcg2 = sum(dcg_2, 1);
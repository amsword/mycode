diary(['result_' type num2str(m) '.txt']);
 load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\result\peekaboom\2012_5_19_16_45_5_train_cbh12_0.mat',...
     'W');
for k = 1 : 10
    ['k = ' num2str(k) '/20']
    best_para_out = cbh_para;
    best_para_out.dk = rhos
    best_para_out.lambda = 0.01;
    best_para_out.max_iter = 50;
    best_para_out.is_learning_rate_fixed = 0;
    ['start best case']
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        best_para_out, W);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
        
    result{k}.eval = curr_eval;
    result{k}.W = W;
    result{k}.para_out = para_out;
    save(['result_' type num2str(m) 'one_peek.mat'], 'result');
end
diary off;
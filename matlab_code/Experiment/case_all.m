%%
% clear;

eval_types.is_hash_lookup = 0;
eval_types.is_hamming_ranking = 1;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;
pca_dim = 64;
is_init_orth = 0;

is_plot = 0;
is_plot_lookup = 0;
is_plot_ranking = 1;

% for gnd_percent = [0.02]
%     gnd_percent
    for m = [64]
        type = 'Labelme'; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
% CIFAR10, Labelme, sift_1m, peekaboom, GIST1M3, 
% BRISK10M, SIFT1M3, Tiny80M, SIFT1B
        file_pre_date = '';
        %       
% file_pre_date = [];
        % m = 64 % code length
        is_cbh = 0;
        is_bsd = 0;
        
        is_regression = 0;
        is_classification = 0;
        is_bre = 0;
        is_mlh = 0;
        is_lsh = 0;
        is_sh = 1;
        is_ch = 0;
        is_usplh = 0;
        is_itq = 0;
        is_mbq = 0;
        is_agh = 0;
        is_ksh = 0;
        is_isohash = 0;
        
        is_train = 1;
        is_test = 1;
        
        is_bre_all = false;
        
        jf_compare;
    end
% end
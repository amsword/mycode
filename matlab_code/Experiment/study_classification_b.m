num_center_sample = 200;
classify_para.m = m;
classify_para.epsilon = 10 ^ -6;
classify_para.beta = -2 * log(classify_para.epsilon) / sqrt(2);
% classify_para.beta = 1;
classify_para.mu = 1 / m;
classify_para.epsilon_gama = 10 ^ -3;
classify_para.max_search_mu_times = 0; % 15
classify_para.start_mu = classify_para.mu;
classify_para.lambda = 1;
classify_para.type_train = 10;
classify_para.alpha = ones(3, 1);
classify_para.is_smart_set_lambda = 0;
classify_para.num_center_sample = num_center_sample; % stochastic sampling
classify_para.max_iter = log(10 ^ -2) / log((Ntraining - num_center_sample) / Ntraining); % maximum cycles
classify_para.max_iter = floor(classify_para.max_iter);
classify_para.max_iter = min(75, classify_para.max_iter);
classify_para.min_abs_deltaW = 0.001; % about the stopping criterium

classify_para.dk = [1 2 3];

gamma = zeros(length(classify_para.dk), 2);
gamma(:, 1) = -2 * log(classify_para.epsilon) ./ (m - classify_para.dk);
gamma(:, 2) = -2 * log(classify_para.epsilon) ./ (classify_para.dk + 1);
classify_para.gamma = gamma;

% gamma =  [6.9078    1.4631; ...
%     6.9078    0.9467; ...
%     6.9078    0.6998];
gamma(:) = 1;
classify_para.gamma = gamma;

classify_para.is_set_dk_smart = 0;

classify_para.is_learning_rate_fixed = 0;

% convert_para(classify_para, 'para.txt');

%%
['loading classify gnd']
I = jf_load_gnd(gnd_file.ClassifyStrainingtrainingAll);
% I = jf_load_gnd('ClassifyStrainingtraining_16');
['complete loading classify gnd']

%%
diary(['result_' type num2str(m) '.txt']);
tic;
% all_mus = 10 .^ [-2 : 1 : 2];
all_mus = 10 .^ [-3];
% all_mus = 0.01;
% all_lambda = [0.1 0.5];
all_lambda = [0.001 0.01 0.1];
% all_lambda = 0.1;

clear all_alpha;
i = 1;
all_alpha(i, :) = [1 1 1]; i = i + 1;
% all_alpha(i, :) = [1 1 0.5]; i = i + 1;
% all_alpha(i, :) = [1 0.8 0.5]; i = i + 1;

clear all_rhos;

clear all_zero_bias;
all_zero_bias = [1];

i = 1;
% all_rhos(i, :) = [1 2 3] * 0.5; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 0.6; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 0.7; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 0.8; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 0.9; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 1; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 1.1; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 1.2; i = i + 1;
all_rhos(i, :) = [1 2 3] * 1.3; i = i + 1;
all_rhos(i, :) = [1 2 3] * 1.4; i = i + 1;
all_rhos(i, :) = [1 2 3] * 1.5; i = i + 1;
all_rhos(i, :) = [1 2 3] * 1.6; i = i + 1;

% all_rhos(i, :) = [1 2 3] * 1.7; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 1.8; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 1.9; i = i + 1;
% all_rhos(i, :) = [1 2 3] * 2.0; i = i + 1;

% all_rhos(i, :) = [1 2 4] * 1.4; i = i + 1;
% all_rhos(i, :) = [1 2 4] * 1.5; i = i + 1;
% all_rhos(i, :) = [1 2 4] * 1.6; i = i + 1;
% all_rhos(i, :) = [1 2 4] * 1.7; i = i + 1;
% all_rhos(i, :) = [1 2 4] * 1.8; i = i + 1;
% all_rhos(i, :) = [1 2 4] * 1.9; i = i + 1;
% 
% all_rhos(i, :) = [1 3 4] * 1.3; i = i + 1;
% all_rhos(i, :) = [1 3 4] * 1.4; i = i + 1;
% all_rhos(i, :) = [1 3 4] * 1.5; i = i + 1;



all_rate0 = 10 .^ [-5 : -1];

num_rhos = size(all_rhos, 1);
num_mus = numel(all_mus);
% matlabpool open
% for i = 1 : numel(all_rhos)

classify_para.momentum = 0;
classify_para.is_learning_rate_fixed = 0;
% generate parameter set
p = 0;
clear para_set;
for i = 1 : numel(all_mus)
    mu = all_mus(i);
    for j = 1 : num_rhos
        rhos = all_rhos(j, :);
        for k = 1 : numel(all_lambda)
            lambda = all_lambda(k);
            for i2 = 1 : size(all_alpha, 1)
                alpha = all_alpha(i2, :);
                
%                 for idx_rate0 = 1 : numel(all_rate0)
%                     rate0 = all_rate0(idx_rate0);
                    
                    for idx_zero_bias = 1 : numel(all_zero_bias)
                        is_bias = all_zero_bias(idx_zero_bias);
                        
                        p = p + 1;
                        para_set{p} = classify_para;

                        para_set{p}.mu = mu;
                        para_set{p}.alpha = alpha;
                        para_set{p}.dk = rhos;
                        para_set{p}.lambda = lambda;
                        para_set{p}.max_iter = 50;
                        para_set{p}.beta = 20;
                        para_set{p}.max_search_mu_times = 0;
                        para_set{p}.is_learning_rate_fixed = 0;
%                         para_set{p}.learning_rate = rate0;
                        para_set{p}.is_zero_bias = is_bias;
                    end
%                 end
            end
        end
    end
end
numel(para_set)
%% scanning all possible parameters

assert(sum(mean(Xtraining, 2)) < 0.001)
X = [Xtraining; ones(1, Ntraining)];
%%
% initW = best_result2{idx}.W;
% best_result2 = cell(numel(para_set), 1);
W_cbh = load(save_file.train_cbh);
% W_cbh = load(save_file.train_classification);
% W_cbh = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\SIFT\working\2012_5_21_1_27_19_train_classification24_0.mat');
% W_cbh = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_6_14_34_1_train_cbh32_0.mat');
idx = randperm(12, m);
W_cbh = W_cbh.W(:, idx);

%%
W_cbh = load(save_file.train_classification);
W_cbh = W_cbh.W;
init_perf = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W_cbh, 2), 'linear', W_cbh, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    
    init_perf{k} = curr_eval;
end
eval_classification = init_perf;
save(save_file.test_classification, 'eval_classification');
%%
W_backup = best_result2{22}.W;
W_backup2 = best_result2{35}.W;
W_cbh = W_backup;
%%
best_result2 = cell(numel(para_set), 1);
tic
parfor i = 1 : numel(para_set)
% for i = 1 : 1
    para = para_set{i};
%     para = best_result2{i}.para_out;
%     [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, para, initW);
%     para.max_iter = 100;
%     para.max_search_mu_times = 2;
    
    [W para_out] = train_cbh_inc_variable(Xtraining, [], I, ...
        para, W_cbh);
%     [W para_out] = train_cbh_inc_variable(Xtraining, [], I, ...
%         para);
%     [W para_out] = train_cbh_inc_variable(Xtraining, [], I, ...
%         para, best_result2{i}.W);
    best_result2{i}.W = W;
    best_result2{i}.para_out = para_out;
    
end
toc;
 save(['02_sift_b_' num2str(m)], 'best_result2');

parfor  i = 1 : numel(para_set)
    
    W = best_result2{i}.W;
    para_out = best_result2{i}.para_out;
    
    for k = 1 : numel(all_topks)
        curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
        
        best_result2{i}.eval{k} = curr_eval;
    end
    
%     [obj obj_content] = cbh_true_obj(W, X, rhos, para.alpha, para_out.lambda, I);
    
%     best_result2{i}.obj = obj;
%     best_result2{i}.obj_content = obj_content;
   
end
 toc;
 save(['02_sift_b_' num2str(m)], 'best_result2');

%% mlh all test 
W_mlh = load(save_file.train_mlh, 'W_mlh');
W_mlh = W_mlh.W_mlh';

all_mlhs = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W_mlh, 2), 'linear', W_mlh, Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    
    all_mlhs{k}.eval = curr_eval;
end
eval_mlh = all_mlhs;
save(save_file.test_mlh, 'eval_mlh', '-append');

%% find the best result in best_result
mlhs = load(save_file.test_mlh);
% mlhs = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\mean_removed_test_mlh16_0.mat');


for k = 1 : numel(all_topks)
    figure;
    plot(mlhs.eval_mlh{k}.r_code, 'b'); hold on;
    
    b = 0;
    idx = -1;
    for i = 1 : numel(best_result2)
        tmp = sum(best_result2{i}.eval{k}.r_code(1 : 1000));
        if (tmp > b)
            b = tmp;
            idx = i;
        end
    end
% idx = 8
idx
%     all_W{k} = best_result2{idx}.W;
%     all_para_out{k} = best_result2{idx}.para_out;
%     eval_classification{k} = best_result2{idx}.eval{k};
    plot(best_result2{idx}.eval{k}.r_code, 'r');
%     plot(init_perf{k}.r_code, 'r');
    
    title([num2str(all_topks(k)) '-NN']);
    axis([0 1000 0 1]);
    grid on;
%     saveas(gca, ['C:\Users\v-jianfw\Desktop\r\peek\18-' num2str(all_topks(k)) '.eps'], 'psc2');
%     close;
end
% save('C:\Users\v-jianfw\Desktop\r\peek\figure_related', 'best_result2');
% close all;
%%
for k = 1 : 4;
    figure;
    plot(all_mlhs{k}.eval.r_code, 'b'); hold on;
    
    plot(best_result2{k}.eval{1}.r_code, 'r');
    title([num2str(all_topks(k)) '-NN']);
    axis([0 1000 0 1]);
    saveas(gca, ['C:\Users\v-jianfw\Desktop\r\12-' num2str(all_topks(k)) '.eps'], 'psc2');
    close;
end


%% convergence of relaxed objective value

for i = 1 : numel(best_result2)
    figure;
    plot(best_result2{i}.para_out.relaxed_obj, 'b');
    hold on;
    plot(best_result2{i}.para_out.exact_obj, 'r');
    title([num2str(best_result2{i}.para_out.dk(1)) ' ' ...
        num2str(best_result2{i}.para_out.alpha(2)) ' ' ...
        num2str(best_result2{i}.para_out.mu)]);
end



%%
parfor i = 1 : numel(best_result2)
   [obj obj_content] = cbh_true_obj(W_mlh, X, best_result2{i}.para_out.alpha, ...
       best_result2{i}.para_out.dk, best_result2{i}.para_out.lambda, I);
   
   mlhs_obj{i}.obj = obj;
   mlhs_obj{i}.obj_content = obj_content;
end
%% compare relaxed objective function values
relaxed_obj_mlh = zeros(numel(best_result2), 1);
relaxed_obj_ours = zeros(numel(best_result2), 1);
parfor i = 1 : numel(best_result2)
% for i = 1 : numel(best_result)
   relaxed_obj_mlh(i) = relaxed_obj_total(W_mlh, X, ...
       best_result2{i}.para_out.alpha,...
       best_result2{i}.para_out.beta, ...
       best_result2{i}.para_out.gamma, ...
       best_result2{i}.para_out.lambda,...
       best_result2{i}.para_out.mu,...
       best_result2{i}.para_out.rd_dk,...
       best_result2{i}.para_out.rd_dk2,...
       best_result2{i}.para_out.rd_dk3, ...
       1,...
       I);
   
   relaxed_obj_ours(i) = relaxed_obj_total(best_result2{i}.W, X, ...
       best_result2{i}.para_out.alpha,...
       best_result2{i}.para_out.beta, ...
       best_result2{i}.para_out.gamma, ...
       best_result2{i}.para_out.lambda,...
       best_result2{i}.para_out.mu,...
       best_result2{i}.para_out.rd_dk,...
       best_result2{i}.para_out.rd_dk2,...
       best_result2{i}.para_out.rd_dk3, ...
       1,...
       I);
    fprintf('%f\n', relaxed_obj_ours(i) - relaxed_obj_mlh(i))
end

%% performance checking by plot recall

for k = 1 : numel(all_topks)
    figure;
    plot(mlhs.eval_mlh{k}.r_code, 'b'); 
    hold on;
    axis([0 1000 0 1]);
    for i = 1 : numel(best_result2)
        
%         if ~isequal(best_result2{i}.para_out.alpha, [1 1 1]) || ...
%                  ~isequal(best_result2{i}.para_out.dk, [1 2 4] * 1.1)
%             continue;
%         end
        
        if (best_result2{i}.para_out.lambda == 0.01)
            color = 'r';
        elseif (best_result2{i}.para_out.lambda == 0.1)
            color = 'k';
        elseif (best_result2{i}.para_out.lambda == 1)
            color = 'g';
        else
            color = 'y';
        end
        
        plot(best_result2{i}.eval{k}.r_code, color);
        title(best_result2{i}.para_out.alpha');
%         pause;
    end
    axis([0 1000 0 1]);
end




%%
plot(mlhs.eval_mlh.avg_retrieved, mlhs.eval_mlh.rec, 'b');
hold on;
for i = 1 : numel(best_result2)
    plot(best_result2{i}.eval.avg_retrieved, best_result2{i}.eval.rec, 'r');
end
grid on;
% axis([0 1000 0 1]);

%% compare obj between ours and mlhs
for i = 1 : numel(all_rhos)
    fprintf('%f\n', scan_mu_result{i}.obj - mlhs_obj{i}.obj);
end
%% check obj scan mu
for i = 1 : numel(best_result2)
    best_result2{i}.para_out.dk
 
    fprintf('%f\n', (best_result2{i}.obj - mlhs_obj{i}.obj) / best_result2{i}.obj );
end
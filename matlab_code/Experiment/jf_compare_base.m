global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

global gl_is_multi_thread;
gl_is_multi_thread = false;

% get the file name, almost return directly
[src_file] = jf_src_data_file_name(type);

metric_info.type = 0;
% seperate data to training, test
% jf_gen_src_data(type, src_file, false);
%

% Xtraining = jf_load_origin_training(src_file.train);
Xtraining = read_mat(src_file.c_train, 'double');
% Xtest = jf_load_origin_test(src_file.test);
%     Xbase = jf_load_origin_base(src_file.base);

Ntraining = size(Xtraining, 2);
% Ntest = size(Xtest, 2);

% topK = 50;
% topK = floor(Ntraining * 0.02);
% topK = 1;
%
% topK_train = Ntraining * 0.02; %only used for mlh
% topK_train = floor(topK_train);
topK_train = 400;
% N_base = size(Xbase, 2);
% topK_base = floor(N_base * gnd_percent);

% gnd_file = jf_gnd_file_name(type, m, topK_train, topK_base);

all_topks = [1 10 100 200 400 50];

gnd_file = jf_gnd_file_name(type, m, topK_train);

if (~exist('file_pre_date', 'var'))
    'random generate save file name'
    % [save_file save_figure] = jf_train_save_file(type, m, topK, is_pca);
%     [save_file save_figure] = train_save_file(type, m, topK, is_pca);
    [save_file save_figure] = train_save_file2(type, m, 0);
else
    'dedicated save file name'
    %     [save_file save_figure] = jf_train_save_file(type, m, topK, is_pca, file_pre_date);
%     [save_file save_figure] = train_save_file(type, m, topK, is_pca, file_pre_date);
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
end

% exeExaustiveKNN(src_file.c_test, src_file.c_train, gnd_file.StestTrainingBin, 1000);
% StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

% exeExaustiveKNN(src_file.c_train, src_file.c_train, gnd_file.SortedPartTrainTrain, 1000);

% calc_large_distance2(Xtest, Xtraining,gnd_file.DistanceTestTrainBin); 
% calc_large_sorted_distance(gnd_file.DistanceTestTrainBin, gnd_file.SortedDistanceTestTrainBin);
% epsilon = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, 50);
% [StestBase3 avg_num] = load_gnd_epsilon(gnd_file.DistanceTestTrainBin, epsilon);

return;
%%
% num_neighbors = jf_calc_avg_num_neighbor(dk, m, N);
% jf_calc_rho(m, percent)
if (is_mlh)
    if (~exist(gnd_file.MLH_Strainingtraining, 'file'))
        'start gnd of mlh'
        jf_gnd_train_mlh(src_file.train, topK, gnd_file.MLH_Strainingtraining);
    else
        'no need for mlh'
    end
end

%%
if (is_regression)
    if (~exist(gnd_file.RankStrainingtraining, 'file'))
        'start proposed gnd'
        [I] = ...
            jf_gnd_train_ranking(src_file.train, m);
        save(gnd_file.RankStrainingtraining, ...
            'I', '-v7.3');
    else
        'no need'
    end
    clear I;
end

%%
if (is_classification)
    if (is_train && ~exist(gnd_file.ClassifyStrainingtraining, 'file'))
        ['loading regression gnd']
        regI = jf_load_gnd(gnd_file.RankStrainingtraining);
        ['complete loading gnd']
        
        if (m == 12 && strcmp(type, 'labelme'))
            rhos = [1, 2, 3];
        elseif (m == 18)
            rhos = [2 3 4 5];
        elseif (m == 32)
            rhos = [8 9 10];
        end
        
        I = jf_gnd_classify_cbh(regI, rhos);
        clear regI;
        jf_save_gnd(I, gnd_file.ClassifyStrainingtraining);
        save(gnd_file.ClassifyStrainingtraining, 'rhos', '-append');
    end
end



%%
% I{1} =  jf_load_gnd(gnd_file.MLH_Strainingtraining);
% rhos = jf_calc_rho(m, topK / Ntraining * 99);
% save(gnd_file.ClassifyStrainingtraining, 'I', 'rhos', '-v7.3');
%%
% I = jf_gnd_test_rankingNDCG(Ntest, Ntraining, ...
%     gnd_file.TestBaseSeed);

%%
% jf_gnd_test(src_file.test, src_file.train, gnd_file.TestBaseSeed, 'knn');
%
%
% Ntest = size(Xtest, 2);
% for percent = [0.02]
%     percent
%     topK_train = floor(Ntraining * percent);
%     gnd_file = jf_gnd_file_name(type, m, topK, topK);
%
%     jf_gnd_knn_test(Ntraining, Ntest, ...
%         topK, gnd_file.StestTraining, ...
%         gnd_file.TestBaseSeed, 'knn');
% end

%%


%%
if (is_bsd)
    [trained_bre] = jf_train_bre(Xtraining(:, 1 : 3000), m, bre_gnd_info);
    
    Xtraining2 = Xtraining(:, 1 : 3000);
    hash_inds = trained_bre.hash_inds;
    %     Ktest = Xbase' * para.Xtraining; % it should be Kbase, but we consider the memory
    B_base = false(m, 3000);
    for b = 1 : m
        Ktest = Xtraining2' * trained_bre.Xtraining(:, hash_inds(:,b));
        %         B_base(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
        B_base(b, :) = Ktest * trained_bre.W(:,b) > 0;
    end
    
    inds = sub2ind([3000, 3000], bre_gnd_info.is, bre_gnd_info.js);
    Dist = sparse(is, js, ...
        bre_gnd_info.Dist(inds), ...
        3000, 3000);
    
    B_test = false(2000, m);
    for b = 1 : m
        Ktest = Xtest' * trained_bre.Xtraining(:, hash_inds(:,b));
        %         B_base(:,b) = Ktest(:, hash_inds(:,b))*W(:,b) > 0;
        B_test(:,b) = Ktest * trained_bre.W(:,b) > 0;
    end
    
    metric_info1 = learn_metric(B_base, Dist, 1);
    
    metric_info2 = learn_metric(B_base, m * Dist, 2);
    
    eval_metric0 = jf_eval_hash3(size(trained_bre.W, 2), 'bre', trained_bre, ...
        Xtest, Xtraining, StestBase, topK, gnd_file.TestBaseSeed, [], metric_info0);
    
    eval_metric2 = jf_eval_hash3(size(trained_bre.W, 2), 'bre', trained_bre, ...
        Xtest, Xtraining, StestBase, topK, gnd_file.TestBaseSeed, [], metric_info2);
end

%% regression
if (is_regression)
    if (is_train)
        if (strcmp(type, 'labelme'))
            num_center_sample = 200;
        elseif (strcmp(type, 'peekaboom'))
            num_center_sample = 500;
        elseif (strcmp(type, 'sift_1m') || strcmp(type, 'notre') || strcmp(type, 'tiny'))
            num_center_sample = 500;
        end
        
        regression_para.m = m; % code length
        regression_para.num_center_sample = num_center_sample; % stochastic sampling
        regression_para.max_iter = log(10 ^ -2) / log((Ntraining - num_center_sample) / Ntraining); % maximum cycles
        regression_para.max_iter = floor(regression_para.max_iter);
        regression_para.max_iter = min(300, regression_para.max_iter);
        regression_para.min_abs_deltaW = 0.001; % about the stopping criterium
        
        regression_para.type_train = 0; % 0: i is randomly sample, j is all
        % 1: i is randomly sample, j is sampled so that every exp_dist has equal
        % points
        regression_para.epsilon = 10 ^ -6;
        
        regression_para.beta = -2 * log(regression_para.epsilon) / sqrt(2);
        %         regression_para.beta = 10;
        regression_para.pd = 1;
        regression_para.po = 2;
        regression_para.mu = 1 / m / 10 ^ 3;
        %          cbh_para.mu = 0;
        regression_para.start_mu = regression_para.mu;
        regression_para.is_relax_dk = false;
        %     cbh_para.candidate_idx = 1 : N_train;
        regression_para.max_search_mu_times = 0; % 15
        regression_para.gnd_file_name = gnd_file.RankStrainingtraining;
        regression_para.sift_need_split = false;
        regression_para.sift_fix = '_22_51_36.76_';
        regression_para.is_force_normalize_w = 0;
        regression_para.is_set_weight = 0;
        
        cbh_para2 = regression_para;
        
        ['loading gnd']
        I = jf_load_gnd(gnd_file.RankStrainingtraining);
        ['complete loading gnd']
        
        
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            cbh_para2);
        %         bucketCap = jf_how_average(W, Xtraining);
        if (para_out.type_train == 10)
            save([save_file.train_classification], 'W', 'para_out', '-v7.3');
        else
            save([save_file.train_cbh], 'W', 'para_out', '-v7.3');
        end
    else
        load(save_file.train_cbh, 'W', 'para_out');
    end
    
    if (is_test)
        if (~exist('StestBase', 'var'))
            StestBase2 = load_gnd2(gnd_file.StestTrainingBin, topK);
%             nDCG_irrelevance = jf_load_gnd(gnd_file.RankStestTrainingNDCG);
        end
        metric_info.type = 0;
        curr_eval = eval_hash5(size(W, 2), 'linear', W, Xtest, Xtraining, ...
            StestBase2', topK, gnd_file.TestBaseSeed, [], metric_info, eval_types);
%         [bucketCap en] = jf_how_average(W, Xtraining);
        eval_cbh = curr_eval;
        save(save_file.test_cbh, 'eval_cbh', 'curr_eval', '-v7.3');
    end
end

%% classification
if (is_classification)
    if (is_train)
        %         if (strcmp(type, 'labelme'))
        %             num_center_sample = 200;
        %         elseif (strcmp(type, 'peekaboom'))
        %             num_center_sample = 200;
        %         elseif (strcmp(type, 'sift_1m') || strcmp(type, 'notre') || strcmp(type, 'tiny'))
        %             num_center_sample = 200;
        %         end
        num_center_sample = 200;
        cbh_para.m = m;
        cbh_para.epsilon = 10 ^ -6;
        cbh_para.beta = -2 * log(cbh_para.epsilon) / sqrt(2);
        cbh_para.mu = 1 / m;
        cbh_para.epsilon_gama = 10 ^ -3;
        cbh_para.max_search_mu_times = 0; % 15
        cbh_para.start_mu = cbh_para.mu;
        cbh_para.lambda = 1;
        cbh_para.type_train = 10;
        cbh_para.alpha = ones(3, 1);
        cbh_para.is_smart_set_lambda = 0;
        cbh_p2ara.type_train = 10;
        cbh_para.num_center_sample = num_center_sample; % stochastic sampling
        cbh_para.max_iter = log(10 ^ -2) / log((Ntraining - num_center_sample) / Ntraining); % maximum cycles
        cbh_para.max_iter = floor(cbh_para.max_iter);
        cbh_para.max_iter = min(75, cbh_para.max_iter);
        cbh_para.min_abs_deltaW = 0.001; % about the stopping criterium
        
        ['loading classify gnd']
        I = jf_load_gnd(gnd_file.ClassifyStrainingtraining);
        ['complete loading classify gnd']
        
        load(gnd_file.ClassifyStrainingtraining, 'rhos');
        cbh_para.is_set_dk_smart = 0;
        
        cbh_para.is_learning_rate_fixed = 0;
        
        n_sample = Ntraining * 0.9;
        n_sample = floor(n_sample);
        rp = randperm(Ntraining);
        
        idx_train = rp(1 : n_sample);
        idx_test = rp(n_sample + 1 : Ntraining);
        
        sub_xtraining = Xtraining(:, idx_train);
        sub_xtest = Xtraining(:, idx_test);
        
        sub_I{1} = I{1}(idx_train, idx_train);
        sub_stest_base = I{1}(idx_test, idx_train);
        
        diary(['result_' type num2str(m) '.txt']);
        for coef = [1 1.2]
            best_para_out = cbh_para;
            best_para_out.max_iter = 200;
            best_para_out.dk = rhos * coef;
            best_para_out.momentum = 0;
            best_para_out.is_learning_rate_fixed = 0;
            ['start best case']
            [W para_out] = jf_train_cbh_ranking(sub_xtraining, [], sub_I, ...
                best_para_out);
            
            curr_eval = jf_eval_hash3(size(W, 2), 'linear', ...
                W, sub_xtest, sub_xtraining, sub_stest_base, ...
                topK, [], [], metric0);
            
            best_result{k}.eval = curr_eval;
            best_result{k}.W = W;
            best_result{k}.para_out = para_out;
            save(['result_' type num2str(m) '1_relax_region.mat'], 'best_result');
        end
        diary off;
        save([save_file.train_classification], 'W', 'para_out', '-v7.3');
    else
        load(save_file.train_classification, 'W', 'para_out');
    end
    
    if (is_test)
        eval_classification = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            eval_classification{k} = eval_hash5(m, 'linear', ...
                W, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        
        save(save_file.test_classification, 'eval_classification', 'all_topks');
    end
end

%% BRE
if (is_bre)
    if (is_train)
        tic
        rp = randperm(Ntraining);
        idx = rp(1 : 5000);
        
        [Ktrain, Dist, is, js, nns, nns2] = gnd_bre(Xtraining(:, idx));
        bre_gnd_info.Ktrain = Ktrain;
        bre_gnd_info.Dist = Dist;
        bre_gnd_info.is = is;
        bre_gnd_info.js = js;
        bre_gnd_info.nns = nns;
        bre_gnd_info.nns2 = nns2;
        
        trained_bre = jf_train_bre(Xtraining(:, idx), m,  bre_gnd_info);
        time_cost = toc;
        save(save_file.train_bre, 'trained_bre', 'time_cost');
    else
        load(save_file.train_bre, 'trained_bre_all', 'trained_bre');
    end
    
    if (is_test)
        eval_bre = cell(numel(all_topks), 1);
        for k = 1 : numel(all_topks)
            eval_bre{k} = eval_hash5(size(trained_bre.W, 2), ...
                'bre', trained_bre, ...
                Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_bre,  'eval_bre');
    end
end

%% linear bre
run_cbre;
%% CH
if (is_ch)
    if (is_train)
        trained_ch = jf_train_ch(Xtraining, m, 1);
        save(save_file.train_ch, 'trained_ch');
    else
        load(save_file.train_ch, 'trained_ch');
    end
    
    if (is_test)
        eval_ch = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            eval_ch{k} = eval_hash5(trained_ch.nbits, 'ch', trained_ch, ...
                Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        
        save(save_file.test_ch, 'eval_ch', 'all_topks');
    end
end
%% LSH
if (is_lsh)
    if (is_train)
        [W_lsh para_lsh] = jf_train_lsh(Xtraining, m);
        save(save_file.train_lsh, 'W_lsh', 'para_lsh');
    else
        load(save_file.train_lsh, 'W_lsh', 'para_lsh');
    end
    if (is_test)
        eval_lsh = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            eval_lsh{k} = eval_hash5(m, 'linear', ...
                W_lsh, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
         assert(~isempty(strfind(save_file.test_lsh, num2str(m))));
        
        save(save_file.test_lsh, 'eval_lsh', 'all_topks');
    end
end
%% MLH
if (is_mlh)
    if (is_train)
        mlh_Strainingtraining = read_classification_gnd(gnd_file.SortedPartTrainTrain, 400);
        mlh_Strainingtraining = mlh_Strainingtraining{1};
        tic;
        [W_mlh para_mlh] = jf_train_mlh(Xtraining, ...
            mlh_Strainingtraining,...
            m);
        time_cost = toc;
        save(save_file.train_mlh, 'W_mlh', 'para_mlh', 'type', 'time_cost');
        clear mlh_Strainingtraining;
    else
        load(save_file.train_mlh, 'W_mlh');
    end
    Xtest = read_mat(src_file.c_test, 'double');
    Xbase = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
    if (is_test)
        all_eval = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            all_eval{k} = eval_hash5(size(W_mlh, 1), 'linear', ...
                W_mlh', Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        eval_mlh = all_eval;
        assert(~isempty(strfind(save_file.test_mlh, num2str(m))));
        
        save(save_file.test_mlh, 'all_eval', 'eval_mlh', 'type');
    end
end


%%
mlh_Strainingtraining = read_classification_gnd(gnd_file.SortedPartTrainTrain, 400);
mlh_Strainingtraining = mlh_Strainingtraining{1};
%
tic;
[W_mlh para_mlh] = jf_train_mlh2(Xtraining, ...
    mlh_Strainingtraining,...
    m);
time_cost = toc;
save(save_file.train_mlh2, 'W_mlh', 'para_mlh', 'type', 'time_cost');

all_eval = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    all_eval{k} = eval_hash5(size(W_mlh, 1), 'linear', ...
        W_mlh', Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end
eval_mlh = all_eval;
save(save_file.test_mlh2, 'all_eval', 'eval_mlh', 'type');
    

%%
mlh_Strainingtraining = read_classification_gnd(gnd_file.SortedPartTrainTrain, 1000);
mlh_Strainingtraining = mlh_Strainingtraining{1};

for m = [64, 128, 12, 18, 24, 16]
    [save_file save_figure] = train_save_file2(type, m, 0, 't');
    tic;
    [W_mlh para_mlh] = jf_train_mlh(Xtraining, ...
        mlh_Strainingtraining,...
        m);
    time_cost = toc;
    save(save_file.train_mlh1k, 'W_mlh', 'para_mlh', 'type', 'time_cost');
    
    all_eval = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        all_eval{k} = eval_hash5(size(W_mlh, 1), 'linear', ...
            W_mlh', Xtest, Xtraining, StestBase2', ...
            all_topks(k), [], [], metric_info, eval_types);
    end
    eval_mlh = all_eval;
    save(save_file.test_mlh1k, 'all_eval', 'eval_mlh', 'type');
end

%% mbq
if (is_mbq)
    if is_train
        [W_mbq aux] = trainMBQ(Xtraining, m);
        save(save_file.train_mbq, 'W_mbq', 'aux');
    else
        load(save_file.train_mbq, 'W_mbq');
    end
    if (is_test)
        eval_mbq = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            eval_mbq{k} = eval_hash5(size(W_mbq, 2), 'linear', ...
                W_mbq, Xtest, Xtraining, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        save(save_file.test_mbq, 'eval_mbq');
    end
end

%% itq
if (is_itq)
    if (is_train)
        W_itq = trainITQ(Xtraining, m);
        assert(~isempty(strfind(save_file.train_itq, num2str(m))));
        
        save(save_file.train_itq, 'W_itq');
    else
        load(save_file.train_itq, 'W_itq');
    end
    if (is_test)
        all_eval = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            all_eval{k} = eval_hash5(size(W_itq, 2), 'linear', ...
                W_itq, Xtest, Xbase, StestBase2', ...
                all_topks(k), [], [], metric_info, eval_types);
        end
        eval_itq = all_eval;
        assert(~isempty(strfind(save_file.train_itq, num2str(m))));
        
        save(save_file.test_itq, 'eval_itq');
    end
end

%% sh
if (is_sh)
    if (is_train)
        SHparam = jf_train_sh(Xtraining, m);
         assert(~isempty(strfind(save_file.train_sh, num2str(m))));
        
        save(save_file.train_sh, 'SHparam');
    else
        load(save_file.train_sh, 'SHparam');
    end
    
    if (is_test)
        eval_sh = cell(numel(all_topks), 1);
        for k = 1 : numel(all_topks)
            eval_sh{k} = eval_hash5(SHparam.nbits, 'sh', SHparam, ...
                Xtest, Xbase, StestBase2', ...
                all_topks(k), gnd_file.TestBaseSeed, ...
                [], metric_info, eval_types);
        end
        assert(~isempty(strfind(save_file.train_sh, num2str(m))));
        
        save(save_file.test_sh, 'eval_sh', 'all_topks');
    end
end
%% usplh
if (is_usplh)
    if (is_train)
        [W_usplh para_usplh] = jf_train_usplh(Xtraining, m);
        assert(~isempty(strfind(save_file.train_usplh, num2str(m))));
        
        save(save_file.train_usplh, 'W_usplh', 'para_usplh');
    else
        load(save_file.train_usplh, 'W_usplh', 'para_usplh');
    end
    
    if (is_test)
        eval_usplh = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            eval_usplh{k} = eval_hash5(size(W_usplh, 2), 'linear', ...
                W_usplh, Xtest, Xbase, StestBase2', ...
                all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
        end
         assert(~isempty(strfind(save_file.train_usplh, num2str(m))));
        
        save(save_file.test_usplh, 'eval_usplh');
    end
end

return;
%%
if (is_plot)
    load(save_file.test_sh, 'eval_sh');
    load(save_file.test_mlh, 'eval_mlh');
    load(save_file.test_usplh, 'eval_usplh');
    load(save_file.test_lsh, 'eval_lsh');
    load(save_file.test_ch, 'eval_ch');
    load(save_file.test_itq, 'eval_itq');
    load(save_file.test_bre, 'eval_bre');
    %     load(save_file.test_cbh, 'eval_cbh');
    load(save_file.test_classification, 'eval_classification');
%     eval_classify = eval_classify.curr_eval;
    %     load(save_file.test_classification_one_level, 'eval_classification_one');
    
    is_marker = true;
   
    curve_colors =  [[255 0 0] / 255; ...
        [220 50 176] / 255; ...
        [148 30 249] / 255; ...
        [20 4 229] / 255; ...
        [3 151 219] / 255; ...
        [1 189 37] / 255; ...
        [0 0 0] / 255; ...
        [80 136 170] / 255; ...
        [200 137 30] / 255];
    markers = {'s', 'v', 'o', '.', '<', 'diamond', 'hexagram', '+', 'x'};
    %% check bre
    % load(save_file.test_bre, 'eval_bre_all');
    %
    % plot(eval_bre_all{1}.rec, eval_bre_all{1}.pre,...
    %     eval_bre_all{2}.rec, eval_bre_all{2}.pre,...
    %     eval_bre_all{3}.rec, eval_bre_all{3}.pre,...
    %     eval_bre_all{4}.rec, eval_bre_all{4}.pre,...
    %     eval_bre_all{5}.rec, eval_bre_all{5}.pre);
    
    %% precision (hash lookup)
    if (is_plot_lookup)
        figure
        plot1 = semilogx(eval_cbh.avg_retrieved, eval_cbh.pre,...
            eval_classify.avg_retrieved, eval_classify.pre,...
            eval_classification_one.avg_retrieved, eval_classification_one.pre, ...
            eval_sh.avg_retrieved, eval_sh.pre,...
            eval_mlh.avg_retrieved, eval_mlh.pre,...
            eval_usplh.avg_retrieved, eval_usplh.pre, ...
            eval_lsh.avg_retrieved, eval_lsh.pre,...
            eval_ch.avg_retrieved, eval_ch.pre, ...
            eval_bre.avg_retrieved, eval_bre.pre);
        
        if (is_marker)
            for k = 1 : numel(plot1)
                set(plot1(k), 'Marker', markers{k}, 'Color', curve_colors(k, :));
            end
        else
            for k = 1 : numel(plot1)
                set(plot1(k), 'Color', curve_colors(k, :));
            end
        end
        
        legend([plot1(1), plot1(2), plot1(3)], 'OPH_{r}', 'OPH_{m}', 'OPH_{o}', ...
            'Location', 'NorthWest');
        legend boxoff;
        ah=axes('position',get(gca,'position'),...
            'visible','off');
        legend(ah, [plot1(4), plot1(5), plot1(6), plot1(7), plot1(8), plot1(9)],  'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'NorthEast');
        legend boxoff;
        
        % Create xlabel
        xlabel('Number of retrieved points','FontSize',14);
        
        % Create ylabel
        ylabel('Precision','FontSize',14);
        set(gca, 'FontSize', 14);
        
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_pre.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_pre.eps'], 'psc2');
        
        %% recall (hash lookup)
        figure
        plot1 = plot(eval_classify.avg_retrieved, eval_classify.rec,...
            eval_sh.avg_retrieved, eval_sh.rec,...
            eval_mlh.avg_retrieved, eval_mlh.rec,...
            eval_usplh.avg_retrieved, eval_usplh.rec, ...
            eval_lsh.avg_retrieved, eval_lsh.rec,...
            eval_ch.avg_retrieved, eval_ch.rec, ...
            eval_bre.avg_retrieved, eval_bre.rec);
        
     
        if (is_marker)
            for k = 1 : numel(plot1)
                set(plot1(k), 'Marker', markers{k}, 'Color', curve_colors(k, :));
            end
        else
            for k = 1 : numel(plot1)
                set(plot1(k), 'Color', curve_colors(k, :));
            end
        end
        
         legend( 'OPH', ...
            'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'Best');
       
        
        % Create xlabel
        xlabel('Number of retrieved points','FontSize',14);
        
        % Create ylabel
        ylabel('Recall','FontSize',14);
        set(gca, 'FontSize', 14);
        
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_rec.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_rec.eps'], 'psc2');
        
        %% p-r (lookup)
        figure
        plot1 = plot(eval_classify.rec, eval_classify.pre,...
            eval_sh.rec, eval_sh.pre,...
            eval_mlh.rec, eval_mlh.pre,...
            eval_usplh.rec, eval_usplh.pre, ...
            eval_lsh.rec, eval_lsh.pre,...
            eval_ch.rec, eval_ch.pre, ...
            eval_bre.rec, eval_bre.pre);
        
        if (is_marker)
            for k = 1 : numel(plot1)
                set(plot1(k), 'Marker', markers{k}, 'Color', curve_colors(k, :));
            end
        else
            for k = 1 : numel(plot1)
                set(plot1(k), 'Color', curve_colors(k, :));
            end
        end
        
        legend( 'OPH', ...
            'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'Best');
        
        % Create xlabel
        xlabel('Recall','FontSize',14);
        
        % Create ylabel
        ylabel('Precision','FontSize',14);
        set(gca, 'FontSize', 14);
        
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_pr.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_lookup_pr.eps'], 'psc2');
        
    end
    %%
    if (is_plot_ranking)
        figure
        plot1 = plot(1 : numel(eval_cbh.p_code), eval_cbh.p_code,...
            1 : numel(eval_cbh.p_code), eval_classify.p_code,...
            1 : numel(eval_cbh.p_code), eval_sh.p_code,...
            1 : numel(eval_cbh.p_code), eval_mlh.p_code,...
            1 : numel(eval_cbh.p_code), eval_usplh.p_code, ...
            1 : numel(eval_cbh.p_code), eval_lsh.p_code,...
            1 : numel(eval_cbh.p_code), eval_ch.p_code, ...
            1 : numel(eval_cbh.p_code), eval_bre.p_code);
        
        
        for k = 1 : numel(plot1)
            set(plot1(k), 'Color', curve_colors(k, :));
        end
        
        if (strcmp(type, 'labelme') && m == 12)
            axis([0, 1000, 0, 0.6]);
        elseif (strcmp(type, 'labelme') && m == 24)
            axis([0, 1000, 0, 0.7]);
        end
        
        legend([plot1(1), plot1(2), plot1(3)], 'OPH_{r}', 'OPH_{m}', 'OPH_{o}', ...
            'Location', 'NorthWest');
        legend boxoff;
        ah=axes('position',get(gca,'position'),...
            'visible','off');
        legend(ah, [plot1(4), plot1(5), plot1(6), plot1(7), plot1(8), plot1(9)],  'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'NorthEast');
        legend boxoff;
                
        % Create xlabel
        xlabel('Number of retrieved points','FontSize',14);
        
        % Create ylabel
        ylabel('Precision','FontSize',14);
        set(gca, 'FontSize', 14);
        
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_ranking_pre.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_ranking_pre.eps'], 'psc2');
        
        %%
        for k = 1 : numel(all_topks)
            figure
            plot1 = plot(...
                1 : numel(eval_classification{1}.r_code), eval_classification{k}.r_code,...
                1 : numel(eval_classification{1}.r_code), eval_sh{k}.r_code,...
                1 : numel(eval_classification{1}.r_code), eval_mlh{k}.r_code,...
                1 : numel(eval_classification{1}.r_code), eval_usplh{k}.r_code, ...
                1 : numel(eval_classification{1}.r_code), eval_lsh{k}.r_code,...
                1 : numel(eval_classification{1}.r_code), eval_ch{k}.r_code, ...
                1 : numel(eval_classification{1}.r_code), eval_bre{k}.r_code);
            hold on;
            axis([0, 1000, 0, 1]);
            
            for i = 1 : numel(plot1)
                set(plot1(i), 'Color', curve_colors(i, :));
            end
            
            legend( 'OPH', ...
                'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
                'Location', 'Best');
            
            % Create xlabel
            xlabel('Number of retrieved points','FontSize',14);
            
            % Create ylabel
            ylabel('Recall','FontSize',14);
            set(gca, 'FontSize', 14);
            
            saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(all_topks(k)) '_ranking_rec.fig']);
            saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(all_topks(k)) '_ranking_rec.eps'], 'psc2');
        end
        %%
        figure
        plot1 = plot(eval_cbh.r_code, eval_cbh.p_code,...
            eval_classify.r_code, eval_classify.p_code,...
            eval_classification_one.r_code, eval_classification_one.p_code,...
            eval_sh.r_code, eval_sh.p_code,...
            eval_mlh.r_code, eval_mlh.p_code,...
            eval_usplh.r_code, eval_usplh.p_code, ...
            eval_lsh.r_code, eval_lsh.p_code,...
            eval_ch.r_code, eval_ch.p_code, ...
            eval_bre.r_code, eval_bre.p_code);
        
        for k = 1 : numel(plot1)
            set(plot1(k), 'Color', curve_colors(k, :));
        end
        
        legend( 'OPH_{r}', 'OPH_{m}', 'OPH_{o}', ...
            'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'Best');
        legend boxoff;
        
        % Create xlabel
        xlabel('Recall','FontSize',14);
        
        % Create ylabel
        ylabel('Precision','FontSize',14);
        set(gca, 'FontSize', 14);
        
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_ranking_pr.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_ranking_pr.eps'], 'psc2');
        
        %%
        figure
        plot1 = plot(1 : numel(eval_cbh.avg_precision_topK), eval_cbh.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_classify.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_classification_one.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_sh.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_mlh.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_usplh.avg_precision_topK, ...
            1 : numel(eval_cbh.avg_precision_topK), eval_lsh.avg_precision_topK,...
            1 : numel(eval_cbh.avg_precision_topK), eval_ch.avg_precision_topK, ...
            1 : numel(eval_cbh.avg_precision_topK), eval_bre.avg_precision_topK);
        
        for k = 1 : numel(plot1)
            set(plot1(k), 'Color', curve_colors(k, :));
        end
        
        if (strcmp(type, 'labelme'))
            axis([0, 1000, 0, 0.45]);
        end
        
        legend([plot1(1), plot1(2), plot1(3)], 'OPH_{r}', 'OPH_{m}', 'OPH_{o}', ...
            'Location', 'NorthWest');
        legend boxoff;
        ah=axes('position',get(gca,'position'),...
            'visible','off');
        legend(ah, [plot1(4), plot1(5), plot1(6), plot1(7), plot1(8), plot1(9)],  'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
            'Location', 'SouthEast');
        legend boxoff;
        %         legend('OPH_{r}', 'OPH_{m}', 'OPH_{o}', 'SH',  'MLH', 'USPLH', 'LSH', 'CH', 'BRE', ...
        %             'Location', 'Best');
        % Create xlabel
        xlabel('Number of retrieved points','FontSize',14);
        
        % Create ylabel
        ylabel('Precision','FontSize',14);
        
        set(gca, 'FontSize', 14);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_topk.fig']);
        saveas(gca, [save_figure type '_' num2str(m) 'topK' num2str(topK) '_topk.eps'], 'psc2');
    end
end
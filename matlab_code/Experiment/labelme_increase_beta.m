clear result;
for k = 1 : 4
    lambda = 1;
    cbh_para2 = cbh_para;
    cbh_para2.beta = cbh_para.beta;
    cbh_para2.dk = [5 6 8];
    cbh_para2.max_iter = 25;
    cbh_para2.lambda = lambda;
    cbh_para2.is_learning_rate_fixed = 0;
    if (k == 1)
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            cbh_para2, cbhs.W);
    else
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            cbh_para2, W);
    end
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
    
    save('result_labelme_24_from_cbh.mat', 'result');
end
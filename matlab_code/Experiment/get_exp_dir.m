function exp_dir = get_exp_dir()
machine_name = getComputerName();
machine_name(end) = [];
if strcmp(machine_name, 'dewberry')
    exp_dir = '\\dragon\Data\Jianfeng\Data_HashCode\';
elseif strcmp(machine_name, 'dragon')
    exp_dir = 'E:\Data\Jianfeng\Data_HashCode\';
else
    error('df');
end
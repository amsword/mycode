clear result;
for k = 1 : 10
    lambda
    cbh_para2 = para_out;
    cbh_para2.mu = para_out.mu * 2;
    cbh_para2.dk = [5 6 8];
    cbh_para2.max_iter = 50;
    cbh_para2.lambda = 1;
    cbh_para2.max_search_mu_times = 4;
    cbh_para2.is_learning_rate_fixed = 0;
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        cbh_para2, W);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
    ap = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    result{k}.ap = ap;
   
    k = k + 1;
    save('result_labelme_24_multi_mu.mat', 'result');
end

%% clear best_result
diary(['result_' type num2str(m) '_(5,6,8).txt']);
for k = 2 : 4
    ['k = ' num2str(k) '/4']
    best_para_out = cbh_para;
    best_para_out.dk = [5 6 8];
    best_para_out.max_iter = 50;
    best_para_out.is_learning_rate_fixed = 0;
    ['start best case']
    if (k == 1)
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            best_para_out);
    else
        [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
            best_para_out, W);
    end
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    best_result{k}.eval = curr_eval;
    best_result{k}.W = W;
    best_result{k}.para_out = para_out;
    best_result{k}.ap_recal = jf_calc_map(1 : 10^4, curr_eval.r_code(1 : 10^4));
    save(['result_' type num2str(m) '_(5,6,8)_10_5_gamma1.mat'], 'best_result');
end
diary off;
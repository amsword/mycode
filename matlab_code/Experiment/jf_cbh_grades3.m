clear;
clc;
%%
% dk = [0, 1, 2]';

global gl_data_parent_folder;
gl_data_parent_folder = 'F:\v-jianfw\Data_HashCode\';
curr_time = clock;
save_sub_folder = [num2str(curr_time(1)) '_' ...
    num2str(curr_time(2)) '_' ...
    num2str(curr_time(3)) '_' ...
    num2str(curr_time(4)) '_' ...
    num2str(curr_time(5)) '_' ...
    num2str(floor(curr_time(6))) 'final_candidate'];
%%

type = 'labelme'; % peekaboom; labelme;
m = 10; % code length

% rho1 = jf_calc_rho(m, 2)
rho = 3;
dk = 0 : rho;

% get the file name, almost return directly
[src_file] = jf_src_data_file_name(type);
[save_file] = jf_train_save_file(type, m);

%% seperate data to training, base, test
% jf_gen_src_data(type, src_file);

%%
N = size(jf_load_training(src_file.train), 2);
topK = floor(N * 0.02);
gnd_file = jf_gnd_file_name(type, m, rho, topK);
num_neighbors = jf_calc_avg_num_neighbor(dk, m, N);

if (exist(gnd_file.Strainingtraining, 'file') && ...
        exist(gnd_file.Strainingtraining, 'file'))
    'no need to calculate the training file'
else
    'start calculate the gnd of training file'
    jf_gnd_train(src_file.train, num_neighbors, ...
        gnd_file.Strainingtraining,...
        topK, gnd_file.MLH_Strainingtraining);
end

if (exist(gnd_file.StestTraining, 'file'))
    'no need to calculate the test gnd'
else
     'start calculate the gnd of test'
    jf_gnd_test(src_file.test, src_file.base, topK, ...
        gnd_file.StestTraining, gnd_file.TestBaseSeed, 'knn');
end

%%
Xtraining = jf_load_training(src_file.train);
I = jf_load_gnd(gnd_file.Strainingtraining);
OriginXtraining = jf_load_origin_training(src_file.train);
OriginXbase = jf_load_origin_base(src_file.base);
OriginXtest = jf_load_origin_test(src_file.test);

Xbase = jf_load_base(src_file.base);
Xtest = jf_load_test(src_file.test);
StestBase = jf_load_gnd([gnd_file.StestTraining]);

%%
cbh_para.m = m; % code length
cbh_para.max_iter = 20; % maximum cycles
cbh_para.min_abs_deltaW = 0.001; % about the stopping criterium
cbh_para.num_center_sample = 200; % stochastic sampling
cbh_para.num_neighbors = num_neighbors;

cbh_para.is_smart_set_lambda = false;
cbh_para.alpha = ones(rho + 1, 1);
% cbh_para.alpha = num_neighbors(numel(num_neighbors)) ./ num_neighbors;
cbh_para.lambda = 1;

cbh_para.epsilon = 0.01;
cbh_para.epsilon_gama = 0.01;

cbh_para.max_search_mu_times = 15; % 15
tic;
eval_cbh = cell(rho + 1, 1);
result = cell(rho + 1, 1);
jf_start_log('multi_grade_012', save_sub_folder);
%%
for i = 0 : rho
    %%
    iter = 0;
    min_value = 10^20;
%     while(iter < 5)
    i = 2;
    iter = iter + 1;
    cbh_para2 = cbh_para;
    cbh_para2.dk = dk(1 : (i + 1));
    cbh_para2.num_neighbors = num_neighbors(1 : (i + 1));
    
    [W para_out] = jf_train_cbh(Xtraining, I(1 : (i + 1)), ...
        cbh_para2);
    
     curr_eval  = jf_test_cbh(W, Xtest, Xbase, ...
        StestBase, topK, gnd_file.TestBaseSeed);
   
    result{i + 1}.W = W;
    result{i + 1}.para = para_out;
    eval_cbh{i + 1} = curr_eval;
    file_name = jf_gen_u_full_file_name(['multi_grade_rho' num2str(i)],...
        save_sub_folder);
    save(file_name, 'W', 'para_out', 'curr_eval');
%     end
    %%
    
    %%
end

eval_cbh_one = cell(rho + 1, 1);
result_one = cell(rho + 1, 1);
for i = 1 : rho
    %%
    i = 2;
    cbh_para2 = cbh_para;
    cbh_para2.dk = dk(i + 1);
    cbh_para2.num_neighbors = num_neighbors(i + 1);
    [W para_out] = jf_train_cbh(Xtraining, I(i + 1), ...
        cbh_para2);
    
    curr_eval = jf_test_cbh(W, Xtest, Xbase, StestBase, topK, gnd_file.TestBaseSeed);
    result_one{i + 1}.W = W; 
    result_one{i + 1}.para  = para_out;
    
    eval_cbh_one{i + 1} = curr_eval;
    file_name = jf_gen_u_full_file_name(['one_grade_rho' num2str(i)],...
        save_sub_folder);
    save(file_name, 'W', 'para_out', 'curr_eval');
    %%
end

%%
% tic;
% mlh_Strainingtraining =  jf_load_gnd(gnd_file.MLH_Strainingtraining);
% W_mlh = jf_train_mlh2(Xtraining, ...
%     mlh_Strainingtraining,...
%     m);
% file_name = jf_gen_u_full_file_name('trained_mlh', save_sub_folder);
% save(file_name, 'W_mlh');
% eval_mlh = jf_test_mlh(W_mlh, Xtest, Xbase, StestBase, topK, gnd_file.TestBaseSeed);
% toc;
% file_name = jf_gen_u_full_file_name('test_mlh', save_sub_folder);
% save(file_name, 'eval_mlh');
% 
% toc;


%%
% input = result;
% input{rho + 2} = result_one{2};
% input{rho + 3} = result_one{3};
% input{rho + 4} = result_one{4};
input{1}.W = W;
input{1}.para = para_out;
input{2}.W = o2.W;
input{2}.para = o2.para_out;
mat_obj = jf_check_optW(Xtraining, input);
%%
eval_mlh = load('F:\v-jianfw\Data_HashCode\2012_4_5_22_14_17_sample_300\result\2012_4_6_7_31_27_test_mlh');
eval_mlh = eval_mlh.eval_mlh;
%%

Nbase = length(eval_cbh{2}.avg_precision_topK);
% expected = (1 : Nbase) / Nbase;

semilogx(1 : Nbase, eval_cbh_one{2}.avg_precision_topK, ...
    1 : Nbase, eval_cbh_one{3}.avg_precision_topK, ...
    1 : Nbase, eval_cbh_one{4}.avg_precision_topK, ...
    1 : Nbase, eval_cbh{2}.avg_precision_topK, ...
    1 : Nbase, eval_cbh{3}.avg_precision_topK, ...
    1 : Nbase, eval_cbh{4}.avg_precision_topK,...
    1 : Nbase, eval_mlh.avg_precision_topK);
legend('one2', 'one3', 'one4', 'multi2', 'multi3', 'multi4', 'mlh');
% axis([0 10000 0 1]);

diary off;
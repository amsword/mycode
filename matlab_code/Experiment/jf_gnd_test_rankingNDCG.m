function [exp_dist] = jf_gnd_test_rankingNDCG(Ntest, Ntraining, ...
    save_gnd_seed)
%% I: for cbh
%   mlh_I, for mlh

max_size = 0.1 * 1024^3 / 8;
batchsize = min(floor(max_size / Ntraining), Ntest);
nbath = ceil(Ntest / batchsize);

exp_dist = zeros(Ntest, Ntraining, 'int8');

m = 10;
%% prepare the number of neighbors in different grades.
avg_num_neighbour = jf_calc_avg_num_neighbor([0 : m], m, Ntraining);
avg_num_neighbour(m + 1) = Ntraining;
%

for i = 1 : nbath
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, Ntest);
     
    file_name = jf_gen_gnd_file(save_gnd_seed, idx_start, idx_end);
    x = load(file_name, 'idx', 'idx_start', 'idx_end');
    
    if (x.idx_start ~= idx_start || ...
            x.idx_end ~= idx_end)
        error('error');
    end
    idx = double(x.idx);
    clear x;
	for k = 1 : (m + 1)
		if (k == 1)
			pre = 1;
		else
			pre = avg_num_neighbour(k - 1) + 1;
        end
        
        if (avg_num_neighbour(k) < pre)
            continue;
        end
        
		tmp_idx = bsxfun(@plus, (idx(:, pre : avg_num_neighbour(k)) - 1) * Ntest, ...
            [idx_start: idx_end]');
		exp_dist(tmp_idx(:)) = k - 1;
    end
    ['bath ' num2str(i) '/' num2str(nbath)]
end
for k = 1 : numel(all_topks)
    figure;
    plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'b*-');
    hold on;
    plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'gd-');
    plot(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 'ro-');
    xlim([0, 5000]);
    legend('LSH', 'ITQ', 'MLH', 'Location', 'Best');
    set(gca, 'FontSize', 14);
    xlabel('Number of retrieved points');
    ylabel('Recall');
    grid on;
    saveas(gca, [num2str(all_topks(k)) '.eps'], 'psc2');
end

%%
lambda = [0.01, 0.05, 0.1, 1];
x = [1.8, 3.0, 3.6, 6.4];
loglog(lambda, x, '*-');
hold on;
x(:) = 3.1;
loglog(lambda, x, 'ro-');

x = [8.9, 14.8, 18.5, 34.2];
loglog(lambda, x, '*-');

x(:) = 15.5;
loglog(lambda, x, 'ro-');

x = [34.4, 59.0, 74.4, 138.3];
loglog(lambda, x, '*-');

x(:) = 61.8;
loglog(lambda, x, 'ro-');

xlabel('lambda', 'FontSize', 14);
ylabel('True negative', 'FontSize', 14);
set(gca, 'FontSize', 14);

saveas(gca, 'tn.eps', 'psc2');

%%
lambda = [0.01, 0.05, 0.1, 1];
x = [427.8, 193.4, 132.0, 32.6];
loglog(lambda, x, '*-');
hold on;
x(:) = 196.4;
loglog(lambda, x, 'ro-');

x = [733.1, 369.2, 265.7, 77.7];
loglog(lambda, x, '*-');

x(:) = 361.0;
loglog(lambda, x, 'ro-');

x = [1098.0, 601.7, 452.8, 158.3];
loglog(lambda, x, '*-');

x(:) = 574.3;
loglog(lambda, x, 'ro-');

xlabel('lambda', 'FontSize', 14);
ylabel('False Positive', 'FontSize', 14);
set(gca, 'FontSize', 14);
saveas(gca, 'fp.eps', 'psc2');
function [src_file] = jf_src_data_file_name(type)

global gl_data_parent_folder;

if (strcmp(type, 'Labelme'))
    data_path = [gl_data_parent_folder 'Labelme\data\'];
elseif (strcmp(type, 'LabelmeOrigin'))
    data_path = [gl_data_parent_folder 'LabelmeOrigin\data\'];
elseif (strcmp(type, 'peekaboom'))
    data_path = [gl_data_parent_folder 'Peekaboom\data\'];
elseif strcmp(type, 'PeekaboomOrigin')
    data_path = [gl_data_parent_folder 'PeekaboomOrigin\data\'];
elseif (strcmp(type, 'sift_1m'))
    data_path = [gl_data_parent_folder 'SIFT\data\'];
elseif strcmp(type, 'SIFTOrigin')
    data_path = [gl_data_parent_folder 'SIFTOrigin\data\'];
elseif strcmp(type, 'SIFT100K')
    data_path = [gl_data_parent_folder 'SIFT100K\data\'];
elseif (strcmp(type, 'notre'))
    data_path = [gl_data_parent_folder 'NotreDame\data\'];
elseif (strcmp(type, 'tiny'))
    data_path = [gl_data_parent_folder 'tiny1M\data\'];
elseif (strcmp(type, 'ForQuantize'))
    data_path = [gl_data_parent_folder 'ForQuantize\data\'];
elseif strcmp(type, 'CIFAR10')
    data_path = [gl_data_parent_folder 'CIFAR10\data\'];
elseif strcmp(type, 'GIST1M')
    data_path = [gl_data_parent_folder 'GIST1M\data\'];
elseif strcmp(type, 'GIST1M3')
    data_path = [gl_data_parent_folder 'GIST1M3\data\'];
elseif strcmp(type, 'SIFT1M3')
    data_path = [gl_data_parent_folder 'SIFT1M3\data\'];
elseif strcmp(type, 'Tiny80M')
    data_path = [gl_data_parent_folder 'Tiny80M\data\'];
else
    type
    data_path = [gl_data_parent_folder type '\data\'];
end

src_file.train = [data_path 'prepared_train_data'];
src_file.c_train = [data_path 'c_data\OriginXtraining.double.bin'];
src_file.c_val_train = [data_path 'c_data\OriginValidationXtraining.double.bin'];
src_file.c_learn_train = [data_path 'c_data\OriginLearnXtraining.double.bin'];
src_file.idx_val_learn_in_train = [data_path 'idx_validation_in_xtraining'];



src_file.pca_c_train = [data_path 'c_data\pcaXtraining.double.bin'];
src_file.pca_mat = [data_path 'c_data\pcaMatrix.double.bin'];

src_file.c_folder = [data_path 'c_data\'];
src_file.train_query = [data_path 'preprared_train_query_data'];
src_file.base = [data_path 'prepared_base_data'];
src_file.c_base =[data_path 'c_data\OriginXBase.double.bin'];

if strcmp(type, 'Tiny80M')
    src_file.c_base =[data_path 'c_data\OriginXBase.float.bin'];
end

src_file.test = [data_path 'prepared_test_data'];
src_file.c_test =[data_path 'c_data\OriginXTest.double.bin'];

if strcmp(type, 'Tiny80M')
    src_file.c_test =[data_path 'c_data\OriginXTest.float.bin'];
end

src_file.other_info = [data_path 'other_info'];
src_file.cluster_center = [data_path type '_cluster_center'];

src_file.eigendecomp = [data_path 'eigndecomp'];
src_file.eigendecomp_kernel_linear = [data_path 'eigndecomp_kernel_linear'];
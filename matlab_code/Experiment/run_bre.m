clc
clear;
eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = true;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;

is_plot = 1;
is_plot_lookup = 0;
is_plot_ranking = 1;

global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

global gl_is_multi_thread;
gl_is_multi_thread = false;

type = 'peekaboom';

% get the file name, almost return directly
[src_file] = jf_src_data_file_name(type);

metric_info.type = 0;

if (is_pca)
    error('not expected, forever');
else
    Xtraining = jf_load_origin_training(src_file.train);
    Xtest = jf_load_origin_test(src_file.test);
    
%     mean_value = mean(Xtraining, 2);
%     Xtraining = bsxfun(@minus, Xtraining, mean_value);
%     Xtest = bsxfun(@minus, Xtest, mean_value);
end

Ntraining = size(Xtraining, 2);
Ntest = size(Xtest, 2);


topK_train = 400; %only used for mlh

% N_base = size(Xbase, 2);
% topK_base = floor(N_base * gnd_percent);

% gnd_file = jf_gnd_file_name(type, m, topK_train, topK_base);

all_topks = [1 10 100 200 400];


gnd_file = gnd_file_name(type, 0, topK_train);
StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);


 rp = randperm(Ntraining);
 
[Ktrain, Dist, is, js, nns, nns2] = gnd_bre(Xtraining(:, rp(1 : 5000)));
bre_gnd_info.Ktrain = Ktrain;
bre_gnd_info.Dist = Dist;
bre_gnd_info.is = is;
bre_gnd_info.js = js;
bre_gnd_info.nns = nns;
bre_gnd_info.nns2 = nns2;

%%
all_m = [18];
all_train = cell(5, 1);
all_test = cell(5, 1);
for i = 1 : numel(all_m)
    m = all_m(i);
    file_pre_date = 'mean_removed_';
    gnd_file = gnd_file_name(type, m, topK_train);

    [save_file save_figure] = train_save_file2(type, m, is_pca, file_pre_date);
   
    trained_bre = jf_train_bre(Xtraining(:, rp(1 : 5000)), m, bre_gnd_info);
    all_train{i} = trained_bre;
    
    eval_bre = cell(numel(all_topks), 1);
    for k = 1 : numel(all_topks)
        eval_bre{k} = eval_hash5(size(trained_bre.W, 2), ...
            'bre', trained_bre, ...
            Xtest, Xtraining, StestBase2', ...
            all_topks(k), [], [], metric_info, eval_types);
    end
    all_test{i} = eval_bre;
end
for i = 1 : numel(all_m)
    m = all_m(i);
    file_pre_date = [];
    gnd_file = gnd_file_name(type, m, topK_train);

    [save_file save_figure] = train_save_file2(type, m, is_pca, file_pre_date);
   
    trained_bre = all_train{i};
    save(save_file.train_bre, 'trained_bre');
    
    eval_bre = all_test{i};
    save(save_file.test_bre,  'eval_bre');
end
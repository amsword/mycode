%%change lambda to less than 1
for k = 11 : 40    
    lambda = 10^-3;
    cbh_para2 = para_out;
    cbh_para2.dk = [5 6 8] * 1.5;
    cbh_para2.max_iter = 50;
    cbh_para2.lambda = lambda;
    cbh_para2.is_learning_rate_fixed = 0;
    [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, ...
        cbh_para2, W);
    curr_eval  = jf_test_cbh(W, Xtest, Xtraining, ...
        StestBase, topK, gnd_file.TestBaseSeed, []);
    
    result{k}.W = W;
    result{k}.para_out = para_out;
    result{k}.curr_eval = curr_eval;
  
    save('result_labelme_24_lambda0.001.mat', 'result');
end
clc
clear;
eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = true;

eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

is_pca = 0;

is_plot = 1;
is_plot_lookup = 0;
is_plot_ranking = 1;

global gl_data_parent_folder;
gl_data_parent_folder = get_data_parent_folder();

global gl_is_multi_thread;
gl_is_multi_thread = false;

type = 'peekaboom';

% get the file name, almost return directly
[src_file] = jf_src_data_file_name(type);

metric_info.type = 0;

if (is_pca)
    error('not expected, forever');
else
    Xtraining = jf_load_origin_training(src_file.train);
    Xtest = jf_load_origin_test(src_file.test);
end

Ntraining = size(Xtraining, 2);
Ntest = size(Xtest, 2);


topK_train = Ntraining * 0.02; %only used for mlh
topK_train = floor(topK_train);

% N_base = size(Xbase, 2);
% topK_base = floor(N_base * gnd_percent);

% gnd_file = jf_gnd_file_name(type, m, topK_train, topK_base);

all_topks = [1 10 100 200 400];


gnd_file = gnd_file_name(type, 0, topK_train);
StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);

mlh_Strainingtraining =  jf_load_gnd(gnd_file.MLH_Strainingtraining);
%%
for m = [18]
    
    file_pre_date = '2012_6_1_12_27_8_';
    gnd_file = gnd_file_name(type, m, topK_train);

    if (~exist('file_pre_date', 'var'))
        'random generate save file name'
        [save_file save_figure] = train_save_file2(type, m, is_pca);
    else
        'dedicated save file name'
        [save_file save_figure] = train_save_file2(type, m, is_pca, file_pre_date);
    end


    [W_mlh para_mlh] = jf_train_mlh(Xtraining, ...
        mlh_Strainingtraining,...
        m);
    save(save_file.train_mlh, 'W_mlh', 'para_mlh', 'type');
           
    eval_mlh = cell(numel(all_topks), 1);
    for k = 1 : numel(all_topks)
        eval_mlh{k} = eval_hash5(size(W_mlh, 1), 'linear', ...
            W_mlh', Xtest, Xtraining, StestBase2', ...
            all_topks(k), [], [], metric_info, eval_types);
        
    end
   
    save(save_file.test_mlh, 'eval_mlh', 'type');
end
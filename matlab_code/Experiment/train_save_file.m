function [save_file folder_figure] = train_save_file(type, m, topK, suffix, prefix)

global gl_data_parent_folder;

if (strcmp(type, 'labelme'))
    path_working = [gl_data_parent_folder ...
        'labelme\working\'];
    path_dir = [gl_data_parent_folder ...
        'labelme\result\'];
elseif (strcmp(type, 'labelme_origin'))
    path_working = [gl_data_parent_folder ...
        'labelmeOrigin\working\'];
    path_dir = [gl_data_parent_folder ...
        'labelmeOrigin\result\'];
elseif (strcmp(type, 'peekaboom'))
    path_working = [gl_data_parent_folder ...
        'peekaboom\working\'];
    path_dir = [gl_data_parent_folder ...
        'peekaboom\result\'];
elseif (strcmp(type, 'sift_1m'))
    path_dir = [gl_data_parent_folder ...
        'result\sift_1m\'];
elseif (strcmp(type, 'notre'))
    path_dir = [gl_data_parent_folder 'result\Notre\'];
elseif (strcmp(type, 'tiny'))
    path_dir = [gl_data_parent_folder 'result\tiny\'];
elseif (strcmp(type, 'ForQuantize'))
    path_dir = [gl_data_parent_folder 'ForQuantize\result\'];
else
    error('no valid type');
end

folder_figure = [path_dir 'figure\'];

if (exist('prefix', 'var'))
 
    path_working = [path_working prefix];
    
else
    curr_time = clock;
   
    path_working = [path_working ...
        num2str(curr_time(1)) '_' ...
        num2str(curr_time(2)) '_' ...
        num2str(curr_time(3)) '_' ...
        num2str(curr_time(4)) '_' ...
        num2str(curr_time(5)) '_' ...
        num2str(floor(curr_time(6))) '_'];
end

save_file.train_cbh = [path_working 'train_cbh' num2str(m) '_' num2str(suffix)];
save_file.test_cbh = [path_working 'test_cbh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_classification = [path_working 'train_classification' num2str(m) '_' num2str(suffix)];
save_file.test_classification = [path_working 'test_classification' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_classification_one_level = [path_working 'train_classification_one_level' num2str(m) '_' num2str(suffix)];
save_file.test_classification_one_level = [path_working 'test_classification_one_level' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_bre = [path_working 'train_bre' num2str(m) '_' num2str(suffix)];
save_file.test_bre = [path_working 'test_bre' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_ch = [path_working 'train_ch' num2str(m) '_' num2str(suffix)];
save_file.test_ch = [path_working 'test_ch' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_lsh = [path_working 'train_lsh' num2str(m) '_' num2str(suffix)];
save_file.test_lsh = [path_working 'test_lsh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

% save_file.train_mlh = [path_dir 'train_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
save_file.train_mlh = [path_working 'train_mlh' num2str(m) '_' num2str(suffix)];
save_file.test_mlh = [path_working 'test_mlh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_sh = [path_working 'train_sh' num2str(m) '_' num2str(suffix)];
save_file.test_sh = [path_working 'test_sh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];

save_file.train_usplh = [path_working 'train_usplh' num2str(m) '_' num2str(suffix)];
save_file.test_usplh = [path_working 'test_usplh' num2str(m) '_' num2str(suffix) 'topk' num2str(topK)];
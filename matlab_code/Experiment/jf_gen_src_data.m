function jf_gen_src_data(type, src_file, is_random)
if (strcmp(type, 'labelme'))
    jf_prepare_labelme(src_file, is_random);
elseif (strcmp(type, 'LabelmeOrigin'))
    prepare_labelme_origin(src_file, is_random);
elseif (strcmp(type, 'peekaboom'))
    jf_prepare_peekaboom(src_file);
elseif strcmp(type, 'PeekaboomOrigin')
    prepare_peekaboom_origin(src_file);
elseif (strcmp(type, 'sift_1m'))
    jf_prepare_sift_1m(src_file);
elseif strcmp(type, 'SIFTOrigin')
    prepare_SIFTOrigin(src_file)
elseif (strcmp(type, 'tiny'))
    jf_prepare_tiny_1m(src_file);
elseif (strcmp(type, 'ForQuantize'))
    jf_prepare_ForQuantize(src_file);
else
    error('type is not valid');
end
function W = read_homo(str, m);
W = read_mat(str, 'double');
W = [W; zeros(1, m)];
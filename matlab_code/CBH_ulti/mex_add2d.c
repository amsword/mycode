#include "mex.h"
#include <assert.h>
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

void parse_double_matrix(const mxArray* prhs,
				  int *m, int *n, double** ptr)
{
	assert(mxIsDouble(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (double*)mxGetPr(prhs);
}

// result = mex_add(original, idx, value)
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double* p_original;
	double* p_idx;
	double* p_value;
	int num_idx;
	int num_ori;
	int m, n;
	double* p_result;
	int i;
    int num_code_length;
    int num_points;
	
	parse_double_matrix(prhs[0], &num_code_length, &num_points, &p_original);
		
	parse_double_matrix(prhs[1], &m, &n, &p_idx);
	num_idx = m * n;    
	
    parse_double_matrix(prhs[2], &m, &n, &p_value);
    mxAssert(m == num_code_length, "dimension is not correct");
    
	plhs[0] = mxCreateDoubleMatrix(num_code_length, num_points, mxREAL);
	p_result = mxGetPr(plhs[0]);
	
	memcpy(p_result, p_original, sizeof(double) * num_code_length * num_points);
	
	for (i = 0; i < num_idx; i++)
	{
		int sub_idx = *p_idx++;
        double* pr;
        double* pv;
        int k;
        
		sub_idx--; // c is zeros-based.
		pr = p_result + sub_idx * num_code_length;

        for (k = 0; k < num_code_length; k++)
        {
            *pr =  *pr + (*p_value++); 
            pr++;
        }
	}
}
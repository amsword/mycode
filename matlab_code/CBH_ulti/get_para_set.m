function para_set = get_para_set(m, all_rhos, all_lambda, gnd_selected, all_mr, input_all_para)
clear classify_para

if nargin >= 4
    classify_para.gnd_selected = gnd_selected;
end

if nargin ~= 5
    all_mr = 0;
end

all_beta = 40;

gamma = zeros(numel(all_rhos(1, :)), 2);
gamma(:) = 1.0 / m;
all_gamma{1} = gamma;

if nargin == 6
    if isfield(input_all_para, 'all_beta')
        all_beta = input_all_para.all_beta;
    end
    if isfield(input_all_para, 'all_gamma')
        all_gamma = input_all_para.all_gamma;
    end
end

num_center_sample = 200;
classify_para.m = m;
classify_para.epsilon = 10 ^ -6;
% classify_para.beta = -2 * log(classify_para.epsilon) / sqrt(2);
classify_para.mu = 1 / m;
classify_para.epsilon_gama = 10 ^ -3;
classify_para.max_search_mu_times = 0; % 15
classify_para.start_mu = classify_para.mu;
classify_para.lambda = 1;
classify_para.type_train = 10;
classify_para.alpha = ones(3, 1);
classify_para.is_smart_set_lambda = 0;
classify_para.num_center_sample = num_center_sample; % stochastic sampling

classify_para.min_abs_deltaW = 0.001; % about the stopping criterium
classify_para.dk = [5 7 9];
% gamma = zeros(length(classify_para.dk), 2);
% gamma(:, 1) = -2 * log(classify_para.epsilon) ./ (m - classify_para.dk);
% gamma(:, 2) = -2 * log(classify_para.epsilon) ./ (classify_para.dk + 1);
% classify_para.gamma = gamma;
classify_para.max_time = 3600 * 72;
classify_para.is_set_dk_smart = 0;
classify_para.is_learning_rate_fixed = 0;
classify_para.need_add_1_data = 0;
tic;
all_mus = 0;

clear all_alpha;
i = 1;
all_alpha(i, :) = [1, 1, 1, 1]; i = i + 1;

clear all_zero_bias;
all_zero_bias = [1];
i = 1;
s = m / 32;

all_rate0 = 10 .^ [-1];
num_rhos = size(all_rhos, 1);
num_mus = numel(all_mus);
% gamma = zeros(numel(all_rhos(1, :)), 2);
% gamma(:) = 1.0 / m;
% classify_para.gamma = gamma;
classify_para.momentum = 0.9;
classify_para.is_learning_rate_fixed = 1;

p = 0;
clear para_set;
for idx_mr = 1 : numel(all_mr)
    mr = all_mr(idx_mr);
    for i = 1 : numel(all_mus)
        mu = all_mus(i);
        for j = 1 : num_rhos
            rhos = all_rhos(j, :);
            for k = 1 : numel(all_lambda)
                lambda = all_lambda(k);
                for i2 = 1 : size(all_alpha, 1)
                    alpha = all_alpha(i2, :);
                    
                    for idx_rate0 = 1 : numel(all_rate0)
                        rate0 = all_rate0(idx_rate0);
                        
                        for idx_zero_bias = 1 : numel(all_zero_bias)
                            is_bias = all_zero_bias(idx_zero_bias);
                            
                            for idx_beta = 1 : numel(all_beta)
                                beta = all_beta(idx_beta);
                                
                                for idx_gamma = 1 : numel(all_gamma)
                                    gamma = all_gamma{idx_gamma};
                                    
                                    p = p + 1;
                                    para_set{p} = classify_para;
                                    para_set{p}.mr = mr;
                                    para_set{p}.mu = mu;
                                    para_set{p}.alpha = alpha;
                                    para_set{p}.dk = rhos;
                                    para_set{p}.type_train = 10;
                                    para_set{p}.lambda = lambda;
                                    para_set{p}.max_iter = 500;
                                    para_set{p}.is_zero_bias = is_bias;
                                    para_set{p}.gamma = gamma;
                                    para_set{p}.beta = beta;
                                    para_set{p}.max_search_mu_times = 0;
                                    para_set{p}.is_learning_rate_fixed = 2;
                                    para_set{p}.learning_rate = rate0;
                                    para_set{p}.num_center_sample = 200;
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
numel(para_set)
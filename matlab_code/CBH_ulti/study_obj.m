idx_i = 4;
idx_j = 1;

all_W = best_result2{idx_i, idx_j}.para_out.all_W;
pe = zeros(numel(all_W), 1);
for i = 1 : numel(all_W)
    W = all_W{i};
    wt = diag(W' * W - 1);
    pe(i) = 1 / 4 * mu * (wt' * wt);
end

best_result2{idx_i, idx_j}.obj
best_result2{idx_i, idx_j}.obj_content

plot(pe, 'r-o');
figure;
plot(best_result2{idx_i, idx_j}.para_out.exact_obj - pe, '*-');
pause;
close all;

%%
for k = 1 : 6
    figure;
    plot(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*');
    hold on;
    plot(best_result2{idx_i, idx_j}.eval{k}.avg_retrieved, best_result2{idx_i, idx_j}.eval{k}.rec, 'r-o');
    xlim([0, 1000]);
end
pause;
close all;
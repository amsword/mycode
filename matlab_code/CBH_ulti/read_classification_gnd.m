function I = read_classification_gnd(str_file, topks)

%%
m_topks = max(topks);
fp = fopen(str_file, 'rb');
m = fread(fp, 1, 'int32');
fread(fp, 1, 'int32');
selected = zeros(m, m_topks);
for i = 1 : m
    n = fread(fp, 1, 'int32');
    
    idx = fread(fp, n, 'int32');
    fread(fp, n, 'double');
    selected(i, :) = idx(1 : m_topks);
end
selected = selected + 1;
fclose(fp);

%%
I = cell(numel(topks), 1);
for i = 1 : numel(topks)
    t_idx = selected(:, 1 : topks(i));
    t_idx = bsxfun(@plus, (t_idx - 1) * m, [1 : m]');
    [is, js] = ind2sub([m, m], t_idx(:));
    I{i} = sparse(is, js, true, m, m);
end

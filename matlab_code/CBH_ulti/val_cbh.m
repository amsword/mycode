% function [W para_out] = val_cbh(Xtraining, I, m)

val_num = Ntraining * 0.05;
val_num = round(val_num);

val_idx_test = randperm(Ntraining, val_num);
val_idx_train = setdiff([1 : Ntraining], val_idx_test);
val_test = Xtraining(:, val_idx_test);
val_train = Xtraining(:, val_idx_train);

X = [val_train; ones(1, numel(val_idx_train))];
%% one similarity layer
I2 = cell(1, 1);
for i = 1 : 1
    I2{1} = I{end}(val_idx_train, val_idx_train);
end
StestBase = I{end}(val_idx_test, val_idx_train);
StestBase2 = cell(numel(val_idx_test), 1);

for i = 1 : numel(val_idx_test)
    StestBase2{i} = find(StestBase(i, :));
end

sparse_eff = sum(StestBase(:)) / size(StestBase(:), 1) / size(StestBase(:), 2);

%% init hash matrix
initW = [randn(size(Xtraining, 1), m); zeros(1, m)];
sw = sum(initW .^ 2, 1);
sw = sqrt(sw);
initW = bsxfun(@rdivide, initW, sw);

%% estimate dk
curr_eval = eval_hash7(size(initW, 2), 'linear', initW, ...
    val_test, val_train, ...
    StestBase2', [], gnd_file.TestBaseSeed, [], ...
    metric_info, eval_types);

t1 = sum(curr_eval.rec < 0.3);
t2 = jf_calc_rho(m, full(sparse_eff));

ta = ceil((t1 + t2) / 2);
%%
% generate parameter set
clear all_dk
st = m / 48;
st = round(st);
i = 1;
all_dk(i, :) = ta + 2 * st; i = i + 1;
all_dk(i, :) = ta + st; i = i + 1;
all_dk(i, :) = ta - st; i = i + 1;
all_dk(i, :) = ta; i = i + 1;
all_lambda = [0.1 0.05 0.01];
para_set = get_para_set(m, all_dk, all_lambda);
%

%% single layer
clear best_result2 
best_result2 = cell(numel(para_set), 1);
for k = 1 : numel(para_set)
    para = para_set{k};
    [W para_out] = jf_train_cbh_ranking(X, [], I2, para, initW);
    best_result2{k}.W = W;
    best_result2{k}.para_out = para_out;
end

for k = 1 : numel(para_set)
    W = best_result2{k}.W;
    curr_eval = eval_hash7(size(W, 2), 'linear', W, ...
        val_test, val_train, ...
        StestBase2', [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    best_result2{k}.curr_eval = curr_eval;
end

best_score = -1;
for k = 1 : numel(para_set)
    curr_eval = best_result2{k}.curr_eval;
    score = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    if score > best_score
        best_score = score;
        best_W = W;
        best_para_out = best_result2{k}.para_out;
        best_lambda = best_result2{k}.para_out.lambda;
        best_dk = best_result2{k}.para_out.dk;
    end
end

if numel(I) == 1
    W = best_W;
    para_out = best_para_out;
    return;
end

%% multi layer
init_dk = [best_dk - 3 * s, ...
    best_dk - 2 * s, ...
    best_dk - s, ...
    best_dk];
i = 1;
clear all_rhos;
all_rhos(i, :) = init_dk; i = i + 1;
all_rhos(i, :) = init_dk - st; i = i + 1;
all_rhos(i, :) = init_dk - 2 * st; i = i + 1;
all_lambda = best_lambda;
para_set = get_para_set(m, all_rhos, all_lambda);

I2 = cell(numel(I), 1);
for i = 1 : numel(I)
    I2{i} = I{i}(val_idx_train, val_idx_train);
end
initW = best_W;
clear best_result2 
best_result2 = cell(numel(para_set), 1);
for k = 1 : numel(para_set)
    para = para_set{k};
    [W para_out] = jf_train_cbh_ranking(X, [], I2, para, initW);
    best_result2{k}.W = W;
    best_result2{k}.para_out = para_out;
end

for k = 1 : numel(para_set)
    W = best_result2{k}.W;
    curr_eval = eval_hash7(size(W, 2), 'linear', W, ...
        val_test, val_train, ...
        StestBase2', [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    best_result2{k}.curr_eval = curr_eval;
end

best_score = -1;
for k = 1 : numel(para_set)
    curr_eval = best_result2{k}.curr_eval;
    score = jf_calc_map(curr_eval.avg_retrieved, curr_eval.rec);
    if score > best_score
        best_score = score;
        best_W = W;
        best_para_out = best_result2{k}.para_out;
    end
end

W = best_W;
para_out = best_para_out;
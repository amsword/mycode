function [loss loss_negative loss_positive] = relaxed_sub_loss(W, X, center_index, alpha, ...
    beta, gamma, ...
    lambda, ...
    dk, dk2, dk3, ...
    pd, subI)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
offset = 1;
N = size(X, 2);
num_center = length(center_index);

m = size(W, 2);

WX = W' * X;
% bWX = double(WX > 0);
% hash_dist = jf_c_hash_distMP(bWX(:, center_index), bWX, 32);

byte_base = compactbit(WX > 0);
hash_dist = jf_hammingDist3(byte_base(:, center_index), byte_base);
hash_dist = double(hash_dist);

rho = length(dk);

inter = cell(rho, 1);
QueryI = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
	
	QueryI3{k} = sparse(hash_dist <= dk3(k));
    inter3{k} = QueryI3{k} & subI{k};
end

loss = 0;
loss_negative = 0;
loss_positive = 0;

contri_obj_grades = zeros(rho, 2);

for k = 1 : rho
%     [idx_i idx_j] =find(subI{k} ~= inter3{k});
    
    [idx_i idx_j] = find(subI{k} ~= inter{k});
      
    s = numel(idx_i);
    contri_obj_grades(k, 1) = s * alpha(k);
    loss_negative = loss_negative + s * alpha(k);
    loss = loss + s * alpha(k);
    
    [idx_i idx_j] =find(QueryI3{k} ~= inter3{k});
    s = numel(idx_i);
    loss_positive = loss_positive + s * alpha(k);
    loss = loss + s * alpha(k) * lambda;
end

loss = loss / num_center;
loss_negative = loss_negative / num_center;
loss_positive = loss_positive / num_center;
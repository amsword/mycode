function [W, para, t] = train_cbh(data, I, para, initW, start_t)
%% data: every column is a point in a high-dimensional space. size: [D N]
%% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
%% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
%% W: the mapping function is W' * X;
%% initW must be [] or the real matrix

%% parameters

% beta = para.beta;
[D, N] = size(data);
pd = 1;
%% init W.
W = get_initW(initW, D, para.m);

X = [data; ones(1, N)];

if (para.is_set_dk_smart)
    error('not checked');
    para.dk = smart_set_dk();
end
num_center_sample = min(para.num_center_sample, N);

%% calculate alpha_k, the parameters are not defined in the original paper
rho = length(para.dk);

subI = cell(1, rho);
pre_deltaW = zeros(size(W));

obj_para.alpha = para.alpha;
obj_para.beta = para.beta;
obj_para.gamma = para.gamma;
obj_para.lambda = para.lambda;
obj_para.mu = para.mu;
obj_para.rd_dk = para.rd_dk;
obj_para.rd_dk2 = para.rd_dk2;
obj_para.rd_dk3 = para.rd_dk3;

num_batch = floor(N / num_center_sample);
%% determine the parameters of learning rate
learning_rate_info.type_learning_rate = para.type_learning_rate;
if (strcmp(para.type_learning_rate, 'reciprocal_t'))
    % determine step0 and b; step(t) = step0 / (1 + b * t)
    rp = randperm(N);
    step0 = determin_step0(W, X, rp, I, ...
        obj_para, para.min_abs_deltaW, ...
        pre_deltaW, para.momentum, num_center_sample);
%     step0 = 2.5 * 10 ^ -4;
    % b = 10 ^ 3 / num_batch / para.max_iter; % intuitively only
	b = 1 / D / para.m;
    para.step0 = step0;
    para.b = b;
end
%%
t = 1;
if (~isempty(start_t))
	t = start_t;
end
for iter = 1 : para.max_iter
    rp = randperm(N);
    
    for batch_id = 1 : num_batch
        [num2str(iter) '/' num2str(para.max_iter) '. batch: '  num2str(batch_id) '/' num2str(num_batch)]
        center_index = rp([1 + (batch_id - 1) *  num_center_sample : batch_id * num_center_sample]);
        
		if (strcmp(learning_rate_info.type_learning_rate, 'reciprocal_t'))
			learning_rate_info.learning_rate = step0 / (1 + b * t)
			t = t + 1
		end
        [W1, deltaW] = sample_train_min_batch(W, X, center_index, I, ...
            obj_para, para.min_abs_deltaW, ...
            learning_rate_info, pre_deltaW, para.momentum);
        pre_deltaW = deltaW;
        W = W1;
    end
end



function dk = smart_set_dk()
WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

center_index = randperm(N);
center_index = center_index([1 : 10 ^ 3]);

if (pd == 2)
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, center_index)';
    hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
        2 * sigX(:, center_index)' * sigX;
elseif (pd == 1)
    hash_dist = jf_c_hash_distMP(sigX(:, center_index), sigX, 32);
end

hash_dist = sort(hash_dist, 2);

dk = zeros(numel(I), 1);
for i = 1 : numel(I)
    tmpI = I{i};
    num = sum(tmpI(1, :));
    dk(i) = mean(hash_dist(:, num));
end

% update W-->W1
% obj_para: the parameters in the objective function
function [W1, deltaW] = train_min_batch_by_pairs(W, X, is, js, I, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum)

W1 = W; % in case the function returns in some special cases
[deltaW pre] = jf_deltaW_by_pairs(...
        W, X, is, js, ...
        obj_para.alpha, ...
        obj_para.beta, ...
        obj_para.gamma, ...
		obj_para.lambda, ...
        obj_para.mu, ...
		obj_para.rd_dk, ... 
		obj_para.rd_dk2, ...
		obj_para.rd_dk3, ...
		1, ...
        I);

deltaW = momentum * pre_deltaW + ...
 	(1 - momentum) * deltaW;
    
length_deltaW = norm(deltaW(:));
if (length_deltaW < min_abs_deltaW)
	return;
end
   
if (strcmp(learning_rate_info.type_learning_rate, 'wolfe'))
	error('not finished');
    c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
    c2 = 0.5; % change of the step
    max_step_percent = 0.1;
    max_iter_find_step = 50;
	length_W = norm(W(:));
	step = max_step_percent * length_W  / length_deltaW;
	squared_length_deltaW = length_deltaW * length_deltaW;
	for i = 1 : max_iter_find_step
		W1 = W - step * deltaW;
		after = jf_objective_8(W1, X, center_index, ...
			alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, 1, ...
			subI);
		target = pre - (c1 * step) * squared_length_deltaW;
		if (target <= 0)
			target = pre;
		end
		if (after <= target)
			break;
		else
			step = step * c2;
		end
	end
else % reciprocal_t or const
	W1 = W - learning_rate_info.learning_rate * deltaW;
	after = jf_objective_by_pairs(W1, X, is, js, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, obj_para.rd_dk, ...
        obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		I);
end

W_changed = norm(W1(:) - W(:)) / norm(W(:));
str = ['obj: ' num2str(pre) '->' num2str(after) ...
	'. W decent: ' num2str(W_changed) ...
	'. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
	'. time: ' num2str(toc) ...
	'\n'];

fprintf(str);

function [W1, deltaW] = sample_train_min_batch(W, X, center_index, I, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum)


rho = numel(I);
subI = cell(rho, 1);
for i_rho = 1 : rho
    tmp_subIk = I{i_rho};
    subI{i_rho} = tmp_subIk(center_index, :);
end

[W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum);
    
% update W-->W1
% obj_para: the parameters in the objective function
function [W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum)
tic;
W1 = W; % in case the function returns in some special cases
[deltaW pre] = jf_deltaW_8(...
        W, X, center_index, ...
        obj_para.alpha, ...
        obj_para.beta, ...
        obj_para.gamma, ...
		obj_para.lambda, ...
        obj_para.mu, ...
		obj_para.rd_dk, ... 
		obj_para.rd_dk2, ...
		obj_para.rd_dk3, ...
		1, ...
        subI);

deltaW = momentum * pre_deltaW + ...
 	(1 - momentum) * deltaW;
    
length_deltaW = norm(deltaW(:));
if (length_deltaW < min_abs_deltaW)
    toc;
	return;
end
   
if (strcmp(learning_rate_info.type_learning_rate, 'wolfe'))
    c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
    c2 = 0.5; % change of the step
    max_step_percent = 0.1;
    max_iter_find_step = 50;
	length_W = norm(W(:));
	step = max_step_percent * length_W  / length_deltaW;
	squared_length_deltaW = length_deltaW * length_deltaW;
	for i = 1 : max_iter_find_step
		W1 = W - step * deltaW;
		after = jf_objective_8(W1, X, center_index, ...
			obj_para.alpha, obj_para.beta, obj_para.gamma, obj_para.lambda, obj_para.mu, ...
			obj_para.rd_dk, obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
			subI);
		target = pre - (c1 * step) * squared_length_deltaW;
		if (target <= 0)
			target = pre;
		end
		if (after <= target)
			break;
		else
			step = step * c2;
		end
	end
else % reciprocal_t or const
	W1 = W - learning_rate_info.learning_rate * deltaW;
	after = jf_objective_8(W1, X, center_index, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, obj_para.rd_dk, ...
        obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		subI);
end

W_changed = norm(W1(:) - W(:)) / norm(W(:));
str = ['obj: ' num2str(pre) '->' num2str(after) ...
	'. W decent: ' num2str(W_changed) ...
	'. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
	'. time: ' num2str(toc) ...
	'\n'];

fprintf(str);

function step0 = determin_step0(W, X, rp, I, ...
    obj_para, min_abs_deltaW, pre_deltaW, momentum, num_center_sample)

scale = 2;
step0 = 10^-3;

center_index = randperm(size(X, 2), num_center_sample);
subI = cell(numel(I), 1);
for i = 1 : numel(I)
    subI{i} = I{i}(center_index, :);
end
[deltaW] = jf_deltaW_8(...
        W, X, center_index, ...
        obj_para.alpha, ...
        obj_para.beta, ...
        obj_para.gamma, ...
		obj_para.lambda, ...
        obj_para.mu, ...
		obj_para.rd_dk, ... 
		obj_para.rd_dk2, ...
		obj_para.rd_dk3, ...
		1, ...
        subI);
step0 = norm(W(:), 1) / norm(deltaW(:), 1) * 0.3;

cost0 = NaN;
while(isnan(cost0))
    cost0 = evaluate_step(W, X, rp, I, ...
        obj_para, min_abs_deltaW, step0, pre_deltaW, momentum, num_center_sample);
    step0 = step0 / scale^2;
end

step1 = step0 * scale;
cost1 = evaluate_step(W, X, rp, I, ...
    obj_para, min_abs_deltaW, step1, pre_deltaW, momentum, num_center_sample);

if (isnan(cost1))
    error('check the parameters');
end

if (cost0 < cost1)
    t = cost1;
    cost1 = cost0;
    cost0 = t;
    
    t = step1;
    step1 = step0;
    step0 = t;
    scale = 1 / scale;
end

while (cost0 >= cost1)
    cost0 = cost1;
    step0 = step1;
    
    step1 = step1 * scale;
    cost1 = evaluate_step(W, X, rp, I, ...
        obj_para, min_abs_deltaW, step1, pre_deltaW, momentum, num_center_sample);
end

function cost = evaluate_step(W, X, rp, I, ...
    obj_para, min_abs_deltaW, step, pre_deltaW, momentum, num_center_sample)

n_iter = 10;
learning_rate_info.type_learning_rate = 'const';
learning_rate_info.learning_rate = step;
rho = length(I);
subI = cell(rho, 1);
for i = 1 : n_iter
    % calculate the deltaW
    center_index = rp((i - 1) * num_center_sample + 1 : i * num_center_sample);
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end
    
    [W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
        obj_para, min_abs_deltaW, ...
        learning_rate_info, pre_deltaW, momentum);
    pre_deltaW = deltaW;
    W = W1;    
end

cost = 0;
for i = 1 : n_iter
    % calculate the deltaW
    center_index = rp((i - 1) * num_center_sample + 1 : i * num_center_sample);
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end
    
    cost = cost + jf_objective_8(W, X, center_index, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, ...
        obj_para.rd_dk, obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		subI);
end
cost = cost / n_iter;

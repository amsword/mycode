function [deltaW obj] = relaxed_deltaW_by_pairs(W, X, is, js, alpha, beta, gamma, ...
    lambda, mu, ...
    dk, dk2, dk3, ...
    pd, I)
%% X and W: the aumented one.
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
num_pairs = length(is);

m = size(W, 2);

WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

if (pd == 2)
    error('not finished');
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, center_index)';
    
    hash_dist = repmat(left, 1, N) + repmat(right, num_pairs, 1) - ...
        2 * sigX(:, center_index)' * sigX;
elseif (pd == 1)
    diff = sigX(:, is) - sigX(:, js);
    abs_diff = abs(diff);
    hash_dist = sum(abs_diff, 1);
end

rho = length(dk);

inter2 = cell(rho, 1);
QueryI2 = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
idx = sub2ind([N, N], is, js);
gnd = cell(rho, 1);
for k = 1 : rho
    QueryI2{k} = hash_dist <= dk2(k);
    gnd{k} = I{k}(idx);
    inter2{k} = QueryI2{k} & gnd{k};
    
    QueryI3{k} = hash_dist <= dk3(k);
    inter3{k} = QueryI3{k} & gnd{k};
end

deltaW = zeros(size(W));
obj = 0;

contri_obj_grades = zeros(rho, 2);
contri_delta_grades = zeros(rho, 2);

for k = 1 : rho
    %     [idx_i idx_j] =find(subI{k} ~= inter3{k});
    
    idx_one = (gnd{k} ~= inter2{k});
    
    dist_diff = hash_dist(idx_one) - dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    s = sum(dist_diff);
    contri_obj_grades(k, 1) = s * alpha(k);
    obj = obj + s * alpha(k);
    dist_diff = (alpha(k) * gamma(k, 1)) * dist_diff .* (1 - dist_diff);
    
    left_diff(:) = 0;
    left_diff(idx_one) = dist_diff;
    
    idx_one = (QueryI3{k} ~= inter3{k});
    
    dist_diff = (1 + dk(k)) - hash_dist(idx_one) ;
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
    s = sum(dist_diff);
    contri_obj_grades(k, 2) = s * alpha(k) * lambda;
    obj = obj + s * alpha(k) * lambda;
    dist_diff = (alpha(k) * lambda * gamma(k, 2)) * dist_diff .* (1 - dist_diff);
    
    right_diff(:) = 0;
    right_diff(idx_one) = dist_diff;
    
    all_diff = left_diff - right_diff;
    
    contri_delta_grades(k, 1) = sum(abs(left_diff(:)));
    contri_delta_grades(k, 2) =  sum(abs(right_diff(:)));
    
    for t = 1 : m
        P = diff(t, :);
        if (pd == 1)
            P = sign(P);
        elseif (pd == 2)
            P = 2 * P;
        end
        P = P .* all_diff;
        
        diff_sigX = sigX(t, :);
        diff_sigX = beta * diff_sigX .* (1 - diff_sigX);
        all_diff_sigX = bsxfun(@times, X, diff_sigX);
        
        diff_sigX2 = all_diff_sigX(:, is) - all_diff_sigX(:, js);
        
        multi_diff = bsxfun(@times, diff_sigX2, P);
        
        deltaW(:, t) = deltaW(:, t) + sum(multi_diff, 2); 
    end
end

obj = obj / num_pairs;
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;

deltaW = deltaW / num_pairs;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW + deltaW2;

%---------------------------------------------
contri_obj_grades = contri_obj_grades / sum(contri_obj_grades(:));
contri_delta_grades = contri_delta_grades / sum(contri_delta_grades(:));

['penalty: obj: ' num2str(p_sum / obj) '\n']
['penalty: delta: ' num2str(sum(abs(deltaW2(:))) / sum(abs(deltaW(:)))) '\n']

fprintf('obj: grades * (AC)\n');
contri_obj_grades
fprintf('delta: grades * (AC)\n');
contri_delta_grades
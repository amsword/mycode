is_pca = 0;

is_kernel = 1;

is_reg = 0;

if strcmp(type, 'sift_1m') || strcmp(type, 'SIFT1M3')
    is_pca = 1;
    numpca = 50;
elseif strcmp(type, 'GIST1M3')
    is_pca = 1;
    numpca = 150;
end

%%
['loading classify gnd']

% gnd_selected = jf_calc_avg_num_neighbor([22], m, Ntraining)
gnd_selected = 400;
% exeExaustiveKNN(cxtest, cxtraining, gnd_file.SortedPartTrainTrain, 400);
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
% I = jf_load_gnd(gnd_file.ClassifyStrainingtrainingAll);
% I = jf_load_gnd('ClassifyStrainingtraining_16');
classify_para.gnd_selected = gnd_selected;
['complete loading classify gnd']

%%
all_lambda = [0.01 0.05 0.1];

clear all_rhos;
x = load(save_file.train_classification);
ta = x.para_out.dk(end)

all_mr = 10 .^ (-6 : -1);

%
step = m / 32;
step = round(step);
i = 1;
all_rhos(i, :) = [ta + step]; i = i + 1;
all_rhos(i, :) = [ta]; i = i + 1;
all_rhos(i, :) = [ta - step]; i = i + 1;
all_rhos(i, :) = [ta - 2 * step]; i = i + 1;
if is_reg
    'is_reg'
    para_set = get_para_set(m, all_rhos, all_lambda, gnd_selected, all_mr);
else
    'no_reg'
    para_set = get_para_set(m, all_rhos, all_lambda, gnd_selected);
end

%%
all_lambda = [0.01 0.05 0.1];

clear all_rhos;
x = load(save_file.train_classification_k);
% x = load('../one/best_train');
ta = x.para_out.dk(end);

step = m / 32;
step = round(step);
s = max(step - 1, 1);
i = 1;
all_rhos(i, :) = [ta + step - 3 * s, ta + step - 2 * s, ta + step - s, ta + step]; i = i + 1;
all_rhos(i, :) = [ta - 3 * s, ta - 2 * s, ta - s, ta]; i = i + 1;
all_rhos(i, :) = [ta - step - 3 * s, ta - step - 2 * s, ta - step - s, ta - step]; i = i + 1;
all_rhos(i, :) = [ta - 4 * s, ta - 3 * s, ta - 2 * s, ta]; i = i + 1;
para_set = get_para_set(m, all_rhos, all_lambda, gnd_selected);

%% scanning all possible parameters

Xtraining = read_mat(src_file.c_train, 'double');
if is_kernel
    'is_kernel'
    anchor = read_mat([src_file.cluster_center '1000'], 'double');
    [para_kernel, Xtraining] = calc_kernel_linear_para(Xtraining, anchor);
else
    'no kernel'
end


%%
% [V, D] = eig(cov(Xtraining'));
% 
% save(src_file.eigendecomp_kernel_linear, 'V', 'D');

%%
if is_pca
    'pca'
    if is_kernel
        x = load(src_file.eigendecomp_kernel_linear, 'V', 'D');
    else
        x = load(src_file.eigendecomp, 'V', 'D');
    end
    V = x.V;
    D = x.D;
    %
    x = diag(D);
    kl_dim = size(D, 1);
    x = x(kl_dim - numpca + 1 : kl_dim);
    fprintf('\n');
    fprintf('Energy used: %f\n', sum(x) / sum(diag(D)));
    %
    zer = x .^ (-0.5);
    Z = diag(zer);
    fprintf('Max scaler: %f\n', max(zer));
    fprintf('Min scalar: %f\n', min(zer));
    
    %%
    pcaXtraining = V(:, kl_dim - numpca + 1  : kl_dim)' * Xtraining;
    zpcaXtrain = Z * pcaXtraining;
    Xtraining = zpcaXtrain;
   
    clear zpcaXtrain pcaXtraining
else
    'no pca'
end

s = sqrt(mean(sum(Xtraining .^ 2, 1)));
Xtraining = Xtraining ./ s;

assert(mean(mean(Xtraining, 2)) < 0.001)
X = [Xtraining; ones(1, size(Xtraining, 2))];
% clear Xtraining;
%%
initW = [randn(size(X, 1) - 1, m); zeros(1, m)];
sw = sum(initW .^ 2, 1);
sw = sqrt(sw);
initW = bsxfun(@rdivide, initW, sw);
% x = load(save_file.train_classification_k);
% x = load('../one/best_train');
% initW = x.W;
%%
% for tk = [50, 100, 400]
%     e10 = eval_hash5(size(initW, 2), 'linear', initW, ...
%         Xtest, Xtraining, ...
%         StestBase2', tk, gnd_file.TestBaseSeed, [], metric_info, eval_types);
%     sum(e10.rec < 0.3)
% end
%%
% x = load(save_file.train_classification);
% initW = x.W;
% clear x;
%%
diary(['result_' type num2str(m) '.txt']);
best_result2 = cell(numel(para_set), 1);

if matlabpool('size') == 0
    matlabpool;
end

parfor i = 1 : numel(best_result2)
% for i = 1 : 1
    para = para_set{i};
    
%     para = best_result2{i}.para_out;
%     para.max_iter = 10;
    tic;
    [W para_out] = jf_train_cbh_ranking(X, [], I, para, initW);
    time_cost = toc;
    %     [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, para, best_result2{i}.W);
    
%     [W para_out] = train_cbh_inc_variable(Xtraining, [], I, ...
%         para, W_cbh);
%     [W para_out] = train_cbh_inc_variable(Xtraining, [], I, ...
%         para);
    best_result2{i}.W = W;
    best_result2{i}.para_out = para_out;
    best_result2{i}.time_cost = time_cost
end
diary off;
%
save([type '_' num2str(m)], 'best_result2', '-v7.3');
%
% parfor i = 1 : numel(best_result2)
%     para = best_result2{i}.para_out;
%     para.max_time = 3600 * 36;
%     [W para_out] = jf_train_cbh_ranking(Xtraining, [], I, para, best_result2{i}.W);
% 
%     best_result2{i}.W = W;
%     best_result2{i}.para_out = para_out;
% end
% 
%  save([type '_' num2str(m)], 'best_result2');
Xtest = read_mat(src_file.c_test, 'double');
if is_kernel
    Xtest = kernel_transform(Xtest, para_kernel);
end
if strcmp(type, 'SIFT1M3')
    Xbase = read_mat(src_file.c_base, 'double');
    Xtraining = Xbase;
    if is_kernel
        Xtraining = kernel_transform(Xtraining, para_kernel);
    end
    if is_pca
        Xtraining = Z * V(:, kl_dim - numpca + 1 : kl_dim)' * Xtraining;
    end
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
else
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);
end

if is_pca
    Xtest = Z * V(:, kl_dim - numpca + 1 : kl_dim)' * Xtest;
end

%
parfor  i = 1 : numel(best_result2)
    W = best_result2{i}.W;
    all_eval = eval_hash_all_topk5(size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks, gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);

    best_result2{i}.eval = all_eval;
end
 save([type '_' num2str(m)], 'best_result2', '-v7.3');
 
 %
%  parfor i = 1 : numel(best_result2)
%      W = best_result2{i}.W;
%      para = best_result2{i}.para_out;
%      [obj obj_content] = cbh_true_obj(W, X, para.dk, para.alpha, para.lambda, I);
%      
%      best_result2{i}.obj = obj;
%      best_result2{i}.obj_content = obj_content;
%  end
%  save([type '_' num2str(m)], 'best_result2', '-v7.3');
 
 %%
%  Straintrain = load_gnd2(gnd_file.SortedPartTrainTrain, 400);
 %%
%  parfor  i = 1 : numel(best_result2)
%     
%     W = best_result2{i}.W;
%     para_out = best_result2{i}.para_out;
%     
%     all_eval = cell(numel(all_topks), 1);
%    for k = 1 : numel(all_topks)
%         curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
%             Xtraining, Xtraining, ...
%             Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
%         all_eval{k} = curr_eval;
%     end
%     best_result2{i}.eval_train = all_eval;
% end
%  save([type '_' num2str(m)], 'best_result2');
 %%
 
 %
% W = W_mlh';
%  all_eval = cell(numel(all_topks), 1);
%  parfor k = 1 : numel(all_topks)
%      curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
%          Xtest, Xtraining, ...
%          StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
%          metric_info, eval_types);
%      all_eval{k} = curr_eval;
%  end
% eval_mlh = all_eval;
% 
% save(save_file.test_mlh, 'eval_mlh');
 
 %%
%  Straintrain = load_gnd2(gnd_file.SortedPartTrainTrain, 400);
%%
%  load(save_file.train_mlh);
%  W = W_mlh';
%  
%  all_eval = cell(numel(all_topks), 1);
%  parfor k = 1 : numel(all_topks)
%      curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
%          Xtraining, Xtraining, ...
%          Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
%      all_eval{k} = curr_eval;
%  end
%  eval_mlh_train = all_eval;
% save(save_file.test_mlh, 'eval_mlh_train', '-append');
%%
 
 %

 
 %%
%  W_mlh = load(save_file.train_mlh, 'W_mlh');
%  W_mlh = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\working\2012_5_6_14_34_1_train_mlh32_0.mat');
%  file_pre_date = '2012_5_4_20_26_16_';
%  [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
 %%
%  load(save_file.train_mlh);
%  
% W_mlh = W_mlh';
% num = numel(best_result2);
%  mlhs_obj = cell(num, 1);
% parfor i = 1 : 4
%     para = best_result2{1, i}.para_out;
%     
%    [obj obj_content] = cbh_true_obj(W_mlh, X, ...
%        para.dk, para.alpha, ...
%        para.lambda, I);
%    
%    mlhs_obj{i}.obj = obj;
%    mlhs_obj{i}.obj_content = obj_content;
% end
% 
% save('obj_obj_content', 'mlhs_obj');

%%
% for k = 1 : numel(all_topks)
%     figure;
%     hold on;
%     plot(eval_mlh_train{k}.rec, eval_mlh_train{k}.pre, 'b-');
%     for i = 1 : numel(best_result2)
%         plot(best_result2{i}.eval_train{k}.rec, best_result2{i}.eval_train{k}.pre, 'r');
%     end
% %     xlim([0, 1000]);
% end

%%

% for i = 1 : numel(best_result2)
%     W = best_result2{i}.W;
%     para = best_result2{i}.para_out;
%     all_W = best_result2{i}.para_out.all_W;
%     all_obj = zeros(1, 20);
%     parfor j = 1 : 20
%         W = all_W{j};
%         [obj obj_content] = cbh_true_obj(W, X, ...
%             para.dk, para.alpha, ...
%             para.lambda, I);
%         
%         all_obj(j) = obj;
%     end
%     best_result2{i}.para_out.exact_obj(1 : 20) = all_obj;
% end


%% find the best result in best_result

mlhs = load(save_file.test_mlh);
% bre = load(save_file.test_bre);
% lsh = load(save_file.test_lsh);
% usplh = load(save_file.test_usplh);
% sh = load(save_file.test_sh);
% load(save_file.test_itq);
load(save_file.test_classification);
x = load(save_file.test_classification_k);
eval_classification_k = x.eval_classification_k;
color_type{1} = 'r-o';
color_type{2} = 'k-d';
color_type{3} = 'g-<';
color_type{4} = 'c->';

color_type{1} = 'r-';
color_type{2} = 'k-';
color_type{3} = 'g-';
color_type{4} = 'c-';

for k = 1 : numel(all_topks)
    figure;
%     hold on;
%         plot(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*'); hold on;
%     xlim([0, 1000]); 
    best_v = 0;
    best_idx = -1;
    for i = 1 : numel(best_result2)
%     for i = 1 : 4
            v = jf_calc_map(best_result2{i}.eval{k}.avg_retrieved, ...
                best_result2{i}.eval{k}.rec);
            if v > best_v
                best_v = v;
                best_idx = i;
            end
            
%         plot(best_result2{i, 1}.eval{k}.avg_retrieved, best_result2{i, 1}.eval{k}.rec, color_type{i});
%         figure
%         hold on;
%          plot(y.best_result2{i}.eval{k}.avg_retrieved, ...
%             y.best_result2{i}.eval{k}.rec, 'k-<');
%         plot(x.best_result2{i}.eval{k}.avg_retrieved, ...
%             x.best_result2{i}.eval{k}.rec, 'g-<');
        semilogx(best_result2{i}.eval{k}.avg_retrieved, ...
            best_result2{i}.eval{k}.rec, 'r-o'); 
%         title([num2str(all_topks(k)) '\_' ...
%             num2str(best_result2{i}.para_out.dk(end)) '\_' ...
%             num2str(best_result2{i}.para_out.lambda)]);
% %         i
%         pause
            hold on;
%         close;
    end
%     continue;
    best_idx 
%     plot(x1.best_result2{best_idx}.eval{k}.avg_retrieved, ...
%         x1.best_result2{best_idx}.eval{k}.rec, 'k-<', 'LineWidth', 2);
%     plot(x2.best_result2{best_idx}.eval{k}.avg_retrieved, ...
%         x2.best_result2{best_idx}.eval{k}.rec, 'g-<', 'LineWidth', 2);
    plot(best_result2{best_idx}.eval{k}.avg_retrieved, ...
        best_result2{best_idx}.eval{k}.rec, 'k-o', 'LineWidth', 2);

%     plot(best_result2{best_idx}.eval{k}.avg_retrieved, ...
%         best_result2{best_idx}.eval{k}.rec, ...
%         'r-o', 'LineWidth', 2);
% 
%     plot(x.best_result2{best_idx}.eval{k}.avg_retrieved, ...
%         x.best_result2{best_idx}.eval{k}.rec, ...
%         'k-o', 'LineWidth', 2);

%     plot(x.best_result2{best_idx}.eval{k}.r_code, ...
%         'k-o', 'LineWidth', 2);
%     
%     plot(best_result2{best_idx}.eval{k}.r_code, ...
%         'r-o', 'LineWidth', 2);
    
%     plot(best_result2{17}.eval_train{k}.avg_retrieved, best_result2{17}.eval_train{k}.rec, 'r-o', 'LineWidth', 2);
    
plot(eval_classification{k}.avg_retrieved, eval_classification{k}.rec, 'g-<', 'LineWidth', 2); 
plot(eval_classification_k{k}.avg_retrieved, eval_classification_k{k}.rec, 'c-<', 'LineWidth', 2); 

%     plot(mlhs.eval_mlh{k}.r_code, 'b-', 'LineWidth', 2); 
    plot(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2); 
%     plot(mlhs.eval_mlh_train{k}.avg_retrieved, mlhs.eval_mlh_train{k}.rec, 'b-'); 
%     legend('0.01','0.1', 'MLH', 'Location', 'Best');
    %     plot(bre.eval_bre{k}.r_code, 'k-', 'LineWidth', 2);
%     plot(bre.eval_bre{k}.avg_retrieved, bre.eval_bre{k}.rec, 'k-', 'LineWidth', 2);
%     plot(sh.eval_sh{k}.r_code, 'g-', 'LineWidth', 2);
%     plot(sh.eval_sh{k}.avg_retrieved, sh.eval_sh{k}.rec, 'g-', 'LineWidth', 2);
%     plot(usplh.eval_usplh{k}.r_code, 'y', 'LineWidth', 2);
%     plot(usplh.eval_usplh{k}.avg_retrieved, usplh.eval_usplh{k}.rec, 'y', 'LineWidth', 2);

%     plot(lsh.eval_lsh{k}.r_code, 'c', 'LineWidth', 2);
%     plot(lsh.eval_lsh{k}.avg_retrieved, lsh.eval_lsh{k}.rec, 'c', 'LineWidth', 2);
%     legend('One', 'Two', 'Four', 'MLH', 'Location', 'Best');
%     axis([0 1000 0 1]);
    grid on;
%     legend(['\lambda=' num2str(best_result2{1, 1}.para_out.lambda)], ...
%         ['\lambda=' num2str(best_result2{2, 1}.para_out.lambda)], ...
%          ['\lambda=' num2str(best_result2{3, 1}.para_out.lambda)], ...
%          ['\lambda=' num2str(best_result2{4, 1}.para_out.lambda)], ...
%         'MLH', 'Location', 'Best');
%     legend('MLH', ['\lambda=' num2str(best_result2{1}.para_out.lambda)], ...
%         ['\lambda=' num2str(best_result2{2}.para_out.lambda)], ...
%         ['\lambda=' num2str(best_result2{3}.para_out.lambda)], ...
%         'Location', 'Best');
    xlim([10, 1000]);
    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
%     saveas(gca, [num2str(all_topks(k)) '-NN.bmp']);
%     close;
end
% save('C:\Users\v-jianfw\Desktop\labelme32\oph.mat', 'best_result2');
pause;
close all;

%%
% for k = 1 : numel(all_topks)
%     figure;
%    
%     plot(mlhs.eval_mlh_train{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*'); hold on;
%     
%     for i = 1 : numel(best_result2)
%         plot(best_result2{i}.eval_train{k}.avg_retrieved, best_result2{i}.eval{k}.rec, 'r-o');
%     end
%     axis([0 1000 0 1]);
%     grid on;
% %     saveas(gca, ['C:\Users\v-jianfw\Desktop\labelme32\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
% %     close;
% end

%%
% mlhs = load(save_file.test_mlh);
% for k = 1 : numel(all_topks)
%     figure;
%     hold on;
%     plot(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*'); 
%     plot(x1.eval_classification{k}.avg_retrieved, ...
%         x1.eval_classification{k}.rec, 'k-<');
%     plot(x2.eval_classification{k}.avg_retrieved, ...
%         x2.eval_classification{k}.rec, 'r-o');
%     xlim([0, 1000]);
%     grid on;
% end
% pause;
% close all;
%%
% for k = 1 : 4;
%     figure;
%     plot(all_mlhs{k}.eval.r_code, 'b'); hold on;
%     
%     plot(best_result2{k}.eval{1}.r_code, 'r');
%     title([num2str(all_topks(k)) '-NN']);
%     axis([0 1000 0 1]);
%     saveas(gca, ['C:\Users\v-jianfw\Desktop\r\12-' num2str(all_topks(k)) '.eps'], 'psc2');
%     close;
% end



%% convergence of relaxed objective value

% for i = 1 : numel(best_result2)
%     figure;
%     plot(best_result2{i}.para_out.relaxed_obj, 'b');
%     hold on;
%     plot(best_result2{i}.para_out.exact_obj, 'r');
%     title([num2str(best_result2{i}.para_out.dk(1)) ' ' ...
%         num2str(best_result2{i}.para_out.alpha(2)) ' ' ...
%         num2str(best_result2{i}.para_out.mu)]);
% end
%%
% for i = 1 : numel(best_result2)
%     figure;
%     plot(best2{i}.para_out.relaxed_obj, 'b');
%     hold on;
%     plot(best2{i}.para_out.exact_obj, 'r');
%     title([num2str(best2{i}.para_out.dk(1)) ' ' ...
%         num2str(best2{i}.para_out.alpha(2)) ' ' ...
%         num2str(best2{i}.para_out.mu)]);
% end

%%
% mlhs_obj = cell(numel(best_result2), 1);
% parfor i = 1 : numel(best_result2)
%    [obj obj_content] = cbh_true_obj(W_mlh, X, best_result2{i}.para_out.alpha, ...
%        best_result2{i}.para_out.dk, best_result2{i}.para_out.lambda, I);
%    
%    mlhs_obj{i}.obj = obj;
%    mlhs_obj{i}.obj_content = obj_content;
% end

%% performance checking by plot recall

% for k = 1 : numel(all_topks)
%     figure;
%     plot(mlhs.eval_mlh{k}.r_code, 'b'); 
%     hold on;
%     axis([0 1000 0 1]);
%     for i = 1 : numel(best_result2)
%         
% %         if ~isequal(best_result2{i}.para_out.alpha, [1 1 1]) || ...
% %                  ~isequal(best_result2{i}.para_out.dk, [1 2 4] * 1.1)
% %             continue;
% %         end
%         
%         if (best_result2{i}.para_out.lambda == 0.01)
%             color = 'r';
%         elseif (best_result2{i}.para_out.lambda == 0.1)
%             color = 'k';
%         elseif (best_result2{i}.para_out.lambda == 1)
%             color = 'g';
%         else
%             color = 'y';
%         end
%         best_result2{i}.para_out.dk
%         plot(best_result2{i}.eval{k}.r_code, color);
%         title(best_result2{i}.para_out.alpha');
%         pause;
%     end
%     axis([0 1000 0 1]);
% end

%%
% plot(mlhs.eval_mlh.avg_retrieved, mlhs.eval_mlh.rec, 'b');
% hold on;
% for i = 1 : numel(best_result2)
%     plot(best_result2{i}.eval.avg_retrieved, best_result2{i}.eval.rec, 'r');
% end
% grid on;
% axis([0 1000 0 1]);

%% compare obj between ours and mlhs
% for i = 1 : numel(all_rhos)
%     fprintf('%f\n', scan_mu_result{i}.obj - mlhs_obj{i}.obj);
% end
%% check obj scan mu
% for i = 1 : numel(best2)
% %     best2{i}.para_out.dk
%      fprintf('%f\t%f\n', (best2{i}.obj - mlhs_obj{i}.obj) / best2{i}.obj, ...
%         best2{i}.obj - mlhs_obj{i}.obj);
% end
%%
% for i = 1 : 4
%     best_result2{i, 1}.para_out.dk
% end
%%
% sh = load(save_file.train_sh);
% SHparam = sh.SHparam;
% 
% eval = cell(numel(all_topks), 1);
% parfor k = 1 : numel(all_topks)
%     eval{k} =  eval_hash5(SHparam.nbits, 'sh', SHparam, ...
%                 Xtest, Xtraining, StestBase2', ...
%                 all_topks(k), gnd_file.TestBaseSeed, ...
%                 [], metric_info, eval_types);
% end
% eval_sh = eval;
% save(save_file.test_sh, 'eval_sh');
%%
% clc;
% for i = 1 : size(best_result2, 1)
%     for j = 1 : size(best_result2, 2)
%         best_result2{i, j}.para_out.learning_rate
%         best_result2{i, j}.para_out.lambda
%         best_result2{i, j}.obj
%         best_result2{i, j}.obj_content
%         pause;
%     end
%     fprintf('\n');
% end
%%

% jf_train_cbh_ranking(Xtraining, [], I, para, initW);
%%
idx = 17;
W = best_result2{idx}.W;
para_out = best_result2{idx}.para_out;
save('best_train', 'W', 'para_out');
eval_classification_k = best_result2{idx}.eval;
save('best_test', 'eval_classification_k');
%%
idx = 21;
W = best_result2{idx}.W;
para_out = best_result2{idx}.para_out;

if is_kernel
    save(save_file.train_classification_k, 'W', 'para_out');
    eval_classification_k = best_result2{idx}.eval;
    save(save_file.test_classification_k, 'eval_classification_k');
else
    save(save_file.train_classification, 'W', 'para_out');
    eval_classification = best_result2{idx}.eval;
    save(save_file.test_classification, 'eval_classification');
end
%%
% x = load('best_train');
% W = x.W;
% para_out = x.para_out;
% x = load('best_test');
% eval_classification_k = x.eval_classification;
% save(save_file.train_classification, 'W', 'para_out');
% save(save_file.test_classification, 'eval_classification');

%%
% mlhs = load(save_file.test_mlh);
% oph = load(save_file.test_classification);
% ophk = load(['best_' type '_' num2str(m) ]);
% ophk_o = load(save_file.test_classification_k);
% 
% for k = 1 : numel(all_topks)
%     figure;
%     best_v = 0;
%     best_idx = -1;
%     
%     semilogx(oph.eval_classification{k}.avg_retrieved, oph.eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
%     hold on;
%     semilogx(ophk.eval_classification_k{k}.avg_retrieved, ophk.eval_classification_k{k}.rec, 'k-o', 'LineWidth', 2);
%     semilogx(ophk_o.eval_classification_k{k}.avg_retrieved, ophk_o.eval_classification_k{k}.rec, 'g-*', 'LineWidth', 2);
%     semilogx(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2);
%     
%     legend('OPH', 'OPH\_kernel', 'OPH\_kernel_o', 'MLH', 'Location', 'Best');
%     grid on;
%     xlim([10, 1000000]);
%     xlabel('Number of retrieved points', 'FontSize', 14);
%     ylabel('Recall', 'FontSize', 14);
%     set(gca, 'FontSize', 14);
% %     saveas(gca, [num2str(m) '_' num2str(all_topks(k)) '.eps'], 'psc2');
% end
% pause;
% close all;
% %%
% para_out = ophk.para_out;
% W = ophk.W;
% eval_classification_k = ophk.eval_classification_k;
% 
% save(save_file.train_classification_k, 'para_out', 'W');
% save(save_file.test_classification_k, 'eval_classification_k');
% 
% %%
% mlhs = load(save_file.test_mlh);
% oph = load(save_file.test_classification);
% ophk = load(['best_' type '_' num2str(m) ]);
% 
% for k = 1 : numel(all_topks)
%     figure;
%     best_v = 0;
%     best_idx = -1;
%     
%     semilogx(oph.eval_classification{k}.avg_retrieved, oph.eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
%     hold on;
%     semilogx(ophk.eval_classification_k{k}.avg_retrieved, ophk.eval_classification_k{k}.rec, 'k-o', 'LineWidth', 2);
%     semilogx(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2);
%     
%     legend('OPH', 'OPH\_kernel', 'MLH', 'Location', 'Best');
%     grid on;
%     xlim([100, 100000]);
%     xlabel('Number of retrieved points', 'FontSize', 14);
%     ylabel('Recall', 'FontSize', 14);
%     set(gca, 'FontSize', 14);
%     saveas(gca, [num2str(m) '_' num2str(all_topks(k)) '.eps'], 'psc2');
% end
% pause;
% close all;
% 
% %%
% x = load([type '_' num2str(m)]);
% eval_classification_k = x.result.eval;
% para_out = x.result.para_out;
% W = x.result.W;
% 
% save(save_file.train_classification_k, 'para_out', 'W');
% save(save_file.test_classification_k, 'eval_classification_k');

function [deltaW obj how_beta_large] = deltaW_14(W, X, center_index, alpha, beta, gamma, ...
    lambda, mu, ...
    dk, dk2, dk3, ...
    pd, subI)
%% X and W: the aumented one.
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair

offset = 0; % whether plus 1

N = size(X, 2);
num_center = length(center_index);

m = size(W, 2);

WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

how_beta_large = sum(sigX(:) < 0.1 | sigX(:) > 0.9);

how_beta_large = how_beta_large / N / m;

% hash_dist = jf_c_hash_distMP(bWX(:, center_index), bWX, 32);
byte_base = compactbit(WX > 0);
hash_dist = jf_hammingDist3(byte_base(:, center_index), byte_base);
hash_dist = double(hash_dist);

rho = length(dk);

inter = cell(rho, 1);
QueryI = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
    
    QueryI3{k} = sparse(hash_dist <= dk3(k));
    inter3{k} = QueryI3{k} & subI{k};
end

% deltaW = zeros(size(W));
obj = 0;

contri_obj_grades = zeros(rho, 2);
contri_delta_grades = zeros(rho, 2);

weight = zeros(m, N);

for k = 1 : rho
    [idx_i idx_j] = find(subI{k} ~= inter{k});
    idx_one = (idx_j - 1) * num_center + idx_i;
    
    dist_diff = hash_dist(idx_one) - dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    s = numel(idx_one);
    contri_obj_grades(k, 1) = s * alpha(k);
    obj = obj + s * alpha(k);
    dist_diff = (alpha(k) * gamma(k, 1)) * dist_diff .* (1 - dist_diff);
    
    idx_i_rel_all = center_index(idx_i);
     left_sigx = sigX(:, idx_i_rel_all);
    right_sigx = sigX(:, idx_j);
    P = left_sigx - right_sigx;
   
    if (pd == 1)
        P = sign(P);
    elseif (pd == 2)
        P = 2 * P;
    end
    P = bsxfun(@times, P, dist_diff');
    coef = beta * left_sigx .* (1 - left_sigx) .* P;
    weight = mex_add2d(weight, idx_i_rel_all, coef);
        
    coef = -beta * right_sigx .* (1 - right_sigx) .* P;
    weight = mex_add2d(weight, idx_j, coef);
    
    [idx_i idx_j] =find(QueryI3{k} ~= inter3{k});
    idx_one = (idx_j - 1) * num_center + idx_i;
    
    dist_diff = (offset + dk(k)) - hash_dist(idx_one) ;
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
    
    s = numel(idx_one);
    contri_obj_grades(k, 2) = s * alpha(k);
    
    obj = obj + s * alpha(k) * lambda;
    dist_diff = -(alpha(k) * lambda * gamma(k, 2)) * dist_diff .* (1 - dist_diff);
    
    idx_i_rel_all = center_index(idx_i);
    for t = 1 : m
        left_sigx = sigX(t, idx_i_rel_all);
        right_sigx = sigX(t, idx_j);
        P = left_sigx - right_sigx;
        if (pd == 1)
            P = sign(P);
        elseif (pd == 2)
            P = 2 * P;
        end
        P = P .* dist_diff';
        coef = beta * left_sigx .* (1 - left_sigx) .* P;
        
        weight(t, :) = mex_add(weight(t, :), idx_i_rel_all, coef);
        
        coef = -beta * right_sigx .* (1 - right_sigx) .* P;
        weight(t, :) = mex_add(weight(t, :), idx_j, coef);
    end
end

% for t = 1 : m
%     cx = bsxfun(@times, X, weight(t, :));
%     deltaW(:, t) = sum(cx, 2);
% end

deltaW = X * weight';




obj = obj / num_center;
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;

deltaW = deltaW / num_center;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW + deltaW2;

%---------------------------------------------
% contri_obj_grades = contri_obj_grades / sum(contri_obj_grades(:));
% contri_delta_grades = contri_delta_grades / sum(contri_delta_grades(:));

% ['penalty: obj: ' num2str(p_sum / obj) '\n']
% ['penalty: delta: ' num2str(sum(abs(deltaW2(:))) / sum(abs(deltaW(:)))) '\n']

% fprintf('obj: grades * (AC)\n');
% contri_obj_grades
function [y loss_negative loss_positive] = relaxed_loss(W, X, alpha, beta, gamma, ...
				lambda, dk, dk2, dk3, I)

batch_size = 400;
N = size(X, 2);
num_batch = ceil(N / batch_size);

y = 0;
loss_negative = 0;
loss_positive = 0;
rho = numel(I);
subI = cell(rho, 1);
for i = 1 : num_batch
    idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    idx_end = min(N, idx_end);
    center_index = idx_start : idx_end;
   
    [num2str(i) '/' num2str(num_batch)]
    
    for k = 1 : rho
        subI{k} = I{k}(idx_start : idx_end, :);
    end    
    [sub_loss sub_loss_negative sub_loss_positive] = ...
        relaxed_sub_loss(W, X, center_index, alpha, beta, gamma, ...
				lambda, dk, dk2, dk3, 1, subI);
    y = y  + length(center_index) * sub_loss;
    loss_negative = loss_negative + length(center_index) * sub_loss_negative;
    loss_positive = loss_positive + length(center_index) * sub_loss_positive;
end

y = y / N;
loss_negative = loss_negative / N;
loss_positive = loss_positive / N;      
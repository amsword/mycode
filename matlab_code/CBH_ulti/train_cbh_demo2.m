clear cbh_para;
num_center_sample = 10;
cbh_para.m = m;
cbh_para.epsilon = 10 ^ -6;
cbh_para.beta = -2 * log(cbh_para.epsilon) / sqrt(2);
cbh_para.mu = 1 / m;
cbh_para.epsilon_gama = 10 ^ -3;
cbh_para.start_mu = cbh_para.mu;
cbh_para.lambda = 1;
cbh_para.type_train = 10;
cbh_para.alpha = ones(3, 1);
cbh_para.is_smart_set_lambda = 0;
cbh_para.type_train = 10;
cbh_para.num_center_sample = num_center_sample; % stochastic sampling
cbh_para.max_iter = log(10 ^ -2) / log((Ntraining - num_center_sample) / Ntraining); % maximum cycles
cbh_para.max_iter = floor(cbh_para.max_iter);
cbh_para.max_iter = min(2000, cbh_para.max_iter);
cbh_para.min_abs_deltaW = 0.001; % about the stopping criterium


['loading classify gnd']
I = jf_load_gnd(gnd_file.ClassifyStrainingtraining);
['complete loading classify gnd']


load(gnd_file.ClassifyStrainingtraining, 'rhos');
cbh_para.dk = rhos;
cbh_para.is_set_dk_smart = 0;

cbh_para.rd_dk = cbh_para.dk;
cbh_para.rd_dk(cbh_para.rd_dk <= 0) = 0.3;
cbh_para.rd_dk2 = cbh_para.rd_dk; % the threshold used for I_{ijk} and J_{ijk}, or be taken as the threshold in smoothed objective domain
% dk2 = dk;
cbh_para.rd_dk2(cbh_para.rd_dk2 <= 0) = 0.3;
cbh_para.rd_dk3 = cbh_para.rd_dk;
cbh_para.rd_dk3(cbh_para.rd_dk3 <= 0) = 0.01;

cbh_para.gamma = zeros(length(cbh_para.rd_dk), 2);
cbh_para.gamma(:, 1) = -2 * log(10^-3) ./ (2);
cbh_para.gamma(:, 2) = 1.5434 ./ ((2 + cbh_para.rd_dk) / 2);

cbh_para.type_learning_rate = 'reciprocal_t';
cbh_para.momentum = 0;

% convert_para(cbh_para, 'para_small_dk.txt');

return;
%%
load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\result\2012_5_6_14_34_1_train_cbh32_0.mat', 'W');
%%
center_index = [10, 11, 12];
center_index = randperm(Ntraining, 400);
subI = cell(3, 1);
for i = 1 : 3
    subI{i} = I{i}(center_index, :);
end
tic
[deltaW pre] = jf_deltaW_8(...
        W, [Xtraining; ones(1, Ntraining)], center_index, ...
        cbh_para.alpha, ...
        cbh_para.beta, ...
        cbh_para.gamma, ...
		cbh_para.lambda, ...
        cbh_para.mu, ...
		cbh_para.rd_dk, ... 
		cbh_para.rd_dk2, ...
		cbh_para.rd_dk3, ...
		1, ...
        subI);
   pre
   toc
% save_mat(deltaW, 'test_gnd_deltaW_9_10_from_0.bin', 'double');
% save_mat(W, 'test_w.bin', 'double');
%%
k = 1;
for lambda = [10^2 10^3]
    cbh_para.lambda = lambda;
    [result{k}.W, result{k}.para_out, result{k}.end_iter] = train_cbh(Xtraining, I, cbh_para, [], []);
    result{k}.eval_cbh =  jf_eval_hash3(m, 'linear', ...
            W, Xtest, Xtraining, StestBase, topK, gnd_file.TestBaseSeed, [], metric_info0);
    k = k + 1;
end
%%
for k = 1 : numel(result)
    if (result{k}.our_relaxed_loss > result{k}.mlh_relaxed_loss)
        [result{k}.W, result{k}.para_out, result{k}.end_iter] = train_cbh(Xtraining, I, cbh_para, result{k}.W, result{k}.end_iter);
        result{k}.eval_cbh =  jf_eval_hash3(m, 'linear', ...
            W, Xtest, Xtraining, StestBase, topK, gnd_file.TestBaseSeed, [], metric_info0);
    end
end

%%
cbh_para.max_iter = cbh_para.max_iter * 10;
[W, para_out] = train_cbh(Xtraining, I, cbh_para, W, para_out.max_iter + 1);
%%
cbh_para.max_iter = 20;
cbh_para.num_center_sample = 400;
cbh_para.lambda = 0.5;
cbh_para.type_learning_rate = 'wolfe';
[W, para_out] = train_cbh(Xtraining, I, cbh_para, [], []);
%%
for k = 1 : numel(result)
    para_out = result{k}.para_out;
    W = result{k}.W;
    result{k}.our_relaxed_loss = relaxed_loss(W, [Xtraining; ones(1, Ntraining)], ...
        para_out.alpha, para_out.beta, para_out.gamma, ...
        para_out.lambda, ...
        para_out.rd_dk, para_out.rd_dk2, para_out.rd_dk3, I);
    result{k}.mlh_relaxed_loss = relaxed_loss(mlhs.W_mlh', [Xtraining; ones(1, Ntraining)], ...
        para_out.alpha, para_out.beta, para_out.gamma, ...
        para_out.lambda, ...
        para_out.rd_dk, para_out.rd_dk2, para_out.rd_dk3, I);
end
%%
para_out = cbh_para;
tic
[loss loss_negative loss_positive] = relaxed_loss(W, [Xtraining; ones(1, Ntraining)], ...
        para_out.alpha, para_out.beta, para_out.gamma, ...
        1000, ...
        para_out.rd_dk, para_out.rd_dk2, para_out.rd_dk3, I)
p =  penalty_value(W, para_out.mu)
p + loss
toc
%%
para_out = cbh_para;
[loss loss_negative loss_positive]  = relaxed_loss(mlhs.W_mlh', [Xtraining; ones(1, Ntraining)], ...
    para_out.alpha, para_out.beta, para_out.gamma, ...
    0.5, ...
    para_out.rd_dk, para_out.rd_dk2, para_out.rd_dk3, I) 
penalty_value(mlhs.W_mlh', para_out.mu)

%%
i = 1;
for lambda = [0.001 0.01 0.1 0.5 1 10 100 1000]
all_lambda(i) = lambda;
lambda
W = read_mat(['C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\data\c_data\w_lambda_' num2str(lambda) '.bin'], 'double');
eval_cbh(i) =  jf_eval_hash3(m, 'linear', W, Xtest, Xtraining, StestBase, topK, gnd_file.TestBaseSeed, [], metric_info0);
i = i + 1;
end

%%
idx = 1 : 10 : 2000;
plot(eval_mlh.eval_mlh.r_code(idx), 'b'); hold on;
k = 1;
plot(eval_cbh(k).r_code(idx), 'r'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'k'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'g'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'c'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'y'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'm'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'r*'); k = k + 1;
plot(eval_cbh(k).r_code(idx), 'bo'); k = k + 1;

legend('mlh', num2str(all_lambda(1)), ...
    num2str(all_lambda(2)), num2str(all_lambda(3)), ...
    num2str(all_lambda(4)), num2str(all_lambda(5)), ...
    num2str(all_lambda(6)), num2str(all_lambda(7)), ...
    num2str(all_lambda(8)));

%%

n_sample = Ntraining * 0.9;
n_sample = floor(n_sample);
rp = randperm(Ntraining);

idx_train = rp(1 : n_sample);
idx_test = rp(n_sample + 1 : Ntraining);

sub_xtraining = Xtraining(:, idx_train);
sub_xtest = Xtraining(:, idx_test);

sub_I{1} = I{1}(idx_train, idx_train);
sub_stest_base = I{1}(idx_test, idx_train);
%%
diary(['result_' type num2str(m) '.txt']);
for coef = [1 1.2]
    best_para_out = cbh_para;
    best_para_out.max_iter = 200;
    best_para_out.dk = rhos * coef;
    best_para_out.momentum = 0;
    best_para_out.is_learning_rate_fixed = 0;
    ['start best case']
    [W para_out] = jf_train_cbh_ranking(sub_xtraining, [], sub_I, ...
        best_para_out);
    
    curr_eval = jf_eval_hash3(size(W, 2), 'linear', ...
        W, sub_xtest, sub_xtraining, sub_stest_base, ...
        topK, [], [], metric0);
    
    best_result{k}.eval = curr_eval;
    best_result{k}.W = W;
    best_result{k}.para_out = para_out;
    save(['result_' type num2str(m) '1_relax_region.mat'], 'best_result');
end
diary off;
[W, cbh_para] = train_cbh(data, I, cbh_para, []);

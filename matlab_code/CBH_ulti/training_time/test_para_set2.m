%%
clear best_result2;
best_result2 = cell(numel(para_set), 1);
tic;
parfor i = 1 : numel(para_set)
    para = para_set{i};
    para.gnd_selected = gnd_selected;
    
    [W para_out] = jf_train_cbh_ranking(X, [], I, para, initW);
    best_result2{i}.W = W;
    best_result2{i}.para_out = para_out;
end
toc;

tic;
for i = 1 : numel(best_result2)
    all_eval = cell(numel(all_topks), 1);
    W = best_result2{i}.W;
    for k = 6 : 6
        curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
            metric_info, eval_types);
        all_eval{k} = curr_eval;
    end
    best_result2{i}.eval = all_eval;
end

best_score = 0;

for i = 1 : numel(best_result2)
    score = jf_calc_map(best_result2{i}.eval{6}.avg_retrieved, ...
        best_result2{i}.eval{6}.rec);
    if score > best_score
        best_para_out = best_result2{i}.para_out;
        best_W = best_result2{i}.W;
        best_eval = best_result2{i}.eval;
        best_score = score;
    end
end
toc;
%%
m = 32;
eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = 0;
eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

type = 'labelme'; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
file_pre_date = '';
jf_compare;
is_pca = 0;

Xtraining = read_mat(src_file.c_train, 'double');
Ntraining = size(Xtraining, 2);
%

x  = load(save_file.train_classification);

classify_para = x.para_out;
if isfield(classify_para, 'all_W')
    classify_para = rmfield(classify_para, 'all_W');
end
if isfield(classify_para, 'exact_obj')
    classify_para = rmfield(classify_para, 'exact_obj');
end
if isfield(classify_para, 'pre_deltaW')
    classify_para = rmfield(classify_para, 'pre_deltaW');
end

if isfield(classify_para, 'gnd_selected')
    gnd_selected = classify_para.gnd_selected(end);
else
    gnd_selected = [400];
end

['loading classify gnd']
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
['complete loading classify gnd']

x = mean(sum(Xtraining .^ 2, 1));
Xtraining = Xtraining / sqrt(x);
%
% assert(sum(mean(Xtraining, 2)) < 0.001)
X = [Xtraining; ones(1, Ntraining)];
clear Xtraining;

%
initW = [randn(size(X, 1) - 1, m); zeros(1, m)];
sw = sum(initW .^ 2, 1);
sw = sqrt(sw);
initW = bsxfun(@rdivide, initW, sw);

x = load(save_file.train_classification);
best_up_dk = x.para_out.dk(end);
s = m / 32;
clear all_rhos
i = 1;
all_rhos(i, :) = best_up_dk + s; i = i + 1;
all_rhos(i, :) = best_up_dk; i = i + 1;
all_rhos(i, :) = best_up_dk - s; i = i + 1;
all_rhos(i, :) = best_up_dk - 2 * s; i = i + 1;
all_lambda = [0.1, 0.05, 0.01];

para_set = get_para_set(m, all_rhos, all_lambda);

%
tic;
test_para_set2;


% find the best parameters
one_best_W = best_W;
one_best_para_out = best_para_out;
one_best_eval = best_eval;
one_best_score = best_score;

initW = best_W;

%
best_up_dk = one_best_para_out.dk;
s = m / 32;
init_dk = [best_up_dk - 3 * s, best_up_dk - 2 * s, best_up_dk - s, best_up_dk];
clear all_rhos
i = 1;
clear all_rhos;
all_rhos(i, :) = init_dk; i = i + 1;
all_rhos(i, :) = init_dk - s; i = i + 1;
all_rhos(i, :) = init_dk - 2 * s; i = i + 1;
all_lambda = best_para_out.lambda;
para_set = get_para_set(m, all_rhos, all_lambda);
x1 = toc;

gnd_selected = [50, 100, 200, 400];
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
%
tic;
test_para_set2;
x2 = toc;
x = x1 + x2;

save('oph', 'x');
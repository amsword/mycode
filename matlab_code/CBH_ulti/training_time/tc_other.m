tic;
[W_lsh para_lsh] = jf_train_lsh(Xtraining, m);
x =  toc;
save('lsh', 'x');

%%
tic;
para = prepare_ksh(Xtraining, src_file.c_train);
[para_out] = train_ksh(para, m);
x = toc;
save('ksh', 'x');

%%
tic;
W_itq = trainITQ(Xtraining, m);
x = toc;
save('itq', 'x');

%%
tic;
[W_isohash_lp] = jf_train_isohash(Xtraining, m, 'lp');
x = toc;
save('iso_lp', 'x');

tic;
[W_isohash_gf] = jf_train_isohash(Xtraining, m, 'gf');
x = toc;
save('iso_gf', 'x');

%
tic;
SHparam = jf_train_sh(Xtraining, m);
x = toc;
save('sh', 'x');

%%
tic;
[W_usplh para_usplh] = jf_train_usplh(Xtraining, m);
x = toc;
save('usplh', 'x');

%%
anchor = read_mat(['\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\HashCode\Anchor_Graph_Hash\Anchor_Graph_Hash\Anchor_Graph_Hash\' type '_cluster_center300'], 'double');

tic;
[para_out] = train_agh(Xtraining, anchor, m, 'one');
x = toc;
save('agh_one', 'x');

tic;
[para_out] = train_agh(Xtraining, anchor, m, 'two');
x = toc;
save('agh_two', 'x');


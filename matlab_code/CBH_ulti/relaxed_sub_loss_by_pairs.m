function [deltaW obj] = relaxed_sub_loss_by_pairs(...
    W, X, is, js, alpha, beta, gamma, ...
    lambda, ...
    dk, dk2, dk3, ...
    pd, I)
%% X and W: the aumented one.
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair
num_pairs = length(is);

m = size(W, 2);

WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

if (pd == 2)
    error('not finished');
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, center_index)';
    
    hash_dist = repmat(left, 1, N) + repmat(right, num_pairs, 1) - ...
        2 * sigX(:, center_index)' * sigX;
elseif (pd == 1)
    diff = sigX(:, is) - sigX(:, js);
    abs_diff = abs(diff);
    hash_dist = sum(abs_diff, 1);
end

rho = length(dk);

inter2 = cell(rho, 1);
QueryI2 = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
idx = sub2ind([N, N], is, js);
gnd = cell(rho, 1);
for k = 1 : rho
    QueryI2{k} = hash_dist <= dk2(k);
    gnd{k} = I{k}(idx);
    inter2{k} = QueryI2{k} & gnd{k};
    
    QueryI3{k} = hash_dist <= dk3(k);
    inter3{k} = QueryI3{k} & gnd{k};
end

deltaW = zeros(size(W));
obj = 0;

for k = 1 : rho
     idx_one = (gnd{k} ~= inter2{k});
    
    dist_diff = hash_dist(idx_one) - dk(k);
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    s = sum(dist_diff);
    obj = obj + s * alpha(k);
    
    idx_one = (QueryI3{k} ~= inter3{k});
    
    dist_diff = (1 + dk(k)) - hash_dist(idx_one) ;
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);
    s = sum(dist_diff);
    obj = obj + s * alpha(k) * lambda;
end

obj = obj / num_pairs;
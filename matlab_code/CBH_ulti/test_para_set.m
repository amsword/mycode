%%
clear best_result2;
best_result2 = cell(numel(para_set), 1);

if matlabpool('size') == 0
    matlabpool 12;
end

parfor i = 1 : numel(para_set)
    para = para_set{i};
    para.gnd_selected = gnd_selected;
    
    [W para_out] = jf_train_cbh_ranking(X, [], I, para, initW);
    best_result2{i}.W = W;
    best_result2{i}.para_out = para_out;
end


Xtest = read_mat(src_file.c_test, 'double');
Xtest = kernel_transform(Xtest, para_kernel);
if is_pca
    Xtest = Z * V(:, kl_dim - numpca + 1 : kl_dim)' * Xtest;
end
%
if strcmp(type, 'SIFT1M3') || strcmp(type, 'GIST1M3')
    Xtraining = read_mat(src_file.c_base, 'double');
    StestBase2 = load_gnd2(gnd_file.STestBase, 400);
else
    Xtraining = read_mat(src_file.c_train, 'double');
    StestBase2 = load_gnd2(gnd_file.StestTrainingBin, 400);
end
Xtraining = kernel_transform(Xtraining, para_kernel);
if is_pca
    Xtraining = Z * V(:, kl_dim - numpca + 1 : kl_dim)' * Xtraining;
end
%
for i = 1 : numel(best_result2)
    all_eval = cell(numel(all_topks), 1);
    W = best_result2{i}.W;
    for k = 1 : numel(all_topks)
        curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
            metric_info, eval_types);
        all_eval{k} = curr_eval;
    end
    best_result2{i}.eval = all_eval;
end

clear Xtest Xtraining;

best_score = 0;

for i = 1 : numel(best_result2)
    score = jf_calc_map(best_result2{i}.eval{6}.avg_retrieved, ...
        best_result2{i}.eval{6}.rec);
    if score > best_score
        best_para_out = best_result2{i}.para_out;
        best_W = best_result2{i}.W;
        best_eval = best_result2{i}.eval;
        best_score = score;
    end
end

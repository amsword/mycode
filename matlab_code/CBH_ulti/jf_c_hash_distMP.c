#include "mex.h"
#include <assert.h>
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

void parse_double_matrix(const mxArray* prhs,
				  int *m, int *n, double** ptr)
{
	assert(mxIsDouble(prhs));
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (double*)mxGetPr(prhs);
}

 //[hash_dist] = jf_deltaW_rankAbs3(...
 //           sig_x_center, ...
 //           sig_x);
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int int_tmp;
	double* p_beta;
	double* p_mu;
	int m;
	double* p_obj;
	clock_t start, finish;
	char buf[256];
	int N;
	int num_center;
	double* p_sigx_center;
	double* p_sigx;
	double* p_hash_dist;
	int j;
	int t;	
    double* p_num_thread;

	parse_double_matrix(prhs[0], &m, &num_center, &p_sigx_center);
	parse_double_matrix(prhs[1], &m, &N, &p_sigx);
    
    parse_double_matrix(prhs[2], &int_tmp, &int_tmp, &p_num_thread);
    

	plhs[0] = mxCreateDoubleMatrix(num_center, N, mxREAL);
	p_hash_dist = mxGetPr(plhs[0]);
	
    omp_set_num_threads((int)(*p_num_thread));
    
    #pragma omp parallel for shared(num_center, \
		p_sigx_center, p_sigx, m, p_hash_dist)
	for (j = 0; j < N; j++)
	{
        int i;
        
		for (i = 0; i < num_center; i++)
		{
            double s;
            int t;
			s = 0;
			for (t = 0; t < m; t++)
			{
				s += fabs(p_sigx_center[i * m + t] - 
					p_sigx[j * m + t]);
			}
			p_hash_dist[j * num_center + i] = s;
		}
	}
}

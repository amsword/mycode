function [deltaW obj] = jf_deltaW_8(W, X, center_index, alpha, beta, gamma, ...
						lambda, mu, ...
						dk, dk2, dk3, ...
                        pd, subI)
%% X and W: the aumented one. 
% type_active_set:	0:	no active set is applied
%					1:	the same as the paper describes
%					2: 	another type	
% subI: a cell array, subJ{1 : rho}

% hash_dist_pair

offset = 1; % whether plus 1

N = size(X, 2);
num_center = length(center_index);

m = size(W, 2);

WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

if (pd == 2)
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, center_index)';
    
    hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
        2 * sigX(:, center_index)' * sigX;
elseif (pd == 1)
    hash_dist = jf_c_hash_distMP(sigX(:, center_index), sigX, 32);
end

rho = length(dk);

inter = cell(rho, 1);
QueryI = cell(rho, 1);
QueryI3 = cell(rho, 1);
inter3 = cell(rho, 1);
for k = 1 : rho
    QueryI{k} = sparse(hash_dist <= dk2(k));
    inter{k} = QueryI{k} & subI{k};
	
	QueryI3{k} = sparse(hash_dist <= dk3(k));
    inter3{k} = QueryI3{k} & subI{k};
end

left_diff = zeros(num_center, N);
right_diff = zeros(num_center, N);

deltaW = zeros(size(W));
obj = 0;

contri_obj_grades = zeros(rho, 2);
contri_delta_grades = zeros(rho, 2);

for k = 1 : rho
%     [idx_i idx_j] =find(subI{k} ~= inter3{k});
    
    [idx_i idx_j] = find(subI{k} ~= inter{k});
      
    idx_one = (idx_j - 1) * num_center + idx_i;
    
	dist_diff = hash_dist(idx_one) - dk(k);	
    dist_diff = sigmf(dist_diff, [gamma(k, 1), 0]);
    s = sum(dist_diff);
    contri_obj_grades(k, 1) = s * alpha(k);
    obj = obj + s * alpha(k);
	dist_diff = (alpha(k) * gamma(k, 1)) * dist_diff .* (1 - dist_diff);

    left_diff(:) = 0;
    left_diff(idx_one) = dist_diff;

    [idx_i idx_j] =find(QueryI3{k} ~= inter3{k});
    idx_one = (idx_j - 1) * num_center + idx_i;

	dist_diff = (offset + dk(k)) - hash_dist(idx_one) ;	
    dist_diff = sigmf(dist_diff, [gamma(k, 2), 0]);

    s = sum(dist_diff);
    contri_obj_grades(k, 2) = s * alpha(k) * lambda;

    obj = obj + s * alpha(k) * lambda;
	dist_diff = (alpha(k) * lambda * gamma(k, 2)) * dist_diff .* (1 - dist_diff);

    right_diff(:) = 0;
    right_diff(idx_one) = dist_diff;

    all_diff = left_diff - right_diff;

    contri_delta_grades(k, 1) = sum(abs(left_diff(:)));
    contri_delta_grades(k, 2) =  sum(abs(right_diff(:)));

	for t = 1 : m
		P = bsxfun(@minus, sigX(t, center_index)', sigX(t, :));
        if (pd == 1)
           P = sign(P);
        elseif (pd == 2)
            P = 2 * P;
        end
        diff_sigX = sigX(t, :);
        diff_sigX = beta * diff_sigX .* (1 - diff_sigX);
        j_diff_sigX = bsxfun(@times, X, diff_sigX);
        
        i_diff_sigX = j_diff_sigX(:, center_index);
        
        P1 = all_diff .* P;
        
        P2 = sum(P1, 2);
        P2 = bsxfun(@times, i_diff_sigX, P2');
        deltaW1 = sum(P2, 2);
        
        P3 = sum(P1, 1);
        P3 = bsxfun(@times, j_diff_sigX, P3);
        deltaW2 = sum(P3, 2);
        
        deltaW(:, t) = deltaW(:, t) + deltaW1 - deltaW2;
    end

end

obj = obj / num_center;
wt = diag(W' * W - 1);
p_sum = 1 / 4 * mu * (wt' * wt);
obj = obj + p_sum;

deltaW = deltaW / num_center;
deltaW2 = mu * W * diag(diag(W' * W) - 1);
deltaW = deltaW + deltaW2;

%---------------------------------------------
contri_obj_grades = contri_obj_grades / sum(contri_obj_grades(:));
contri_delta_grades = contri_delta_grades / sum(contri_delta_grades(:));

['penalty: obj: ' num2str(p_sum / obj) '\n']
['penalty: delta: ' num2str(sum(abs(deltaW2(:))) / sum(abs(deltaW(:)))) '\n']

fprintf('obj: grades * (AC)\n');
contri_obj_grades
fprintf('delta: grades * (AC)\n');
contri_delta_grades
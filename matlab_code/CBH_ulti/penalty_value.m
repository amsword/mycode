function y = penalty_value(W, mu)

wt = diag(W' * W - 1);
y = 1 / 4 * mu * (wt' * wt);
%%

eval_types.is_hash_lookup = true;
eval_types.is_hamming_ranking = 0;
eval_types.is_topk = false;
eval_types.is_ndcg = false;
eval_types.is_success_rate = false;

type = 'labelme'; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
file_pre_date = '';
jf_compare;
is_pca = 0;
if strcmp(type, 'labelme')
    numpca = 50
end

%%
diary('tracking.txt');
x  = load(save_file.train_classification);

classify_para = x.para_out;
if isfield(classify_para, 'all_W')
    classify_para = rmfield(classify_para, 'all_W');
end
if isfield(classify_para, 'exact_obj')
    classify_para = rmfield(classify_para, 'exact_obj');
end
if isfield(classify_para, 'pre_deltaW')
    classify_para = rmfield(classify_para, 'pre_deltaW');
end

if isfield(classify_para, 'gnd_selected')
    gnd_selected = classify_para.gnd_selected(end);
else
    gnd_selected = [400];
end

['loading classify gnd']
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
['complete loading classify gnd']

%%
anchor = read_mat(['\\msra-msm-03\F$\v-jianfw\hash\v-jianfw\HashCode\Anchor_Graph_Hash\Anchor_Graph_Hash\Anchor_Graph_Hash\' type '_cluster_center300'], 'double');
%
[para_kernel, Xtraining] = calc_kernel_linear_para(Xtraining, anchor);


%%
% [V, D] = eig(cov(Xtraining'));
% 
% save(src_file.eigendecomp_kernel_linear, 'V', 'D');

%%
if is_pca
    x = load(src_file.eigendecomp_kernel_linear, 'V', 'D');
    V = x.V;
    D = x.D;
    %
    x = diag(D);
    kl_dim = size(D, 1);
    x = x(kl_dim - numpca + 1 : kl_dim);
    fprintf('\n');
    fprintf('Energy used: %f\n', sum(x) / sum(diag(D)));
    %
    zer = x .^ (-0.5);
    Z = diag(zer);
    fprintf('Max scaler: %f\n', max(zer));
    fprintf('Min scalar: %f\n', min(x));
    
    %
    pcaXtraining = V(:, kl_dim - numpca + 1  : kl_dim)' * Xtraining;
    zpcaXtrain = Z * pcaXtraining;
    Xtraining = zpcaXtrain;
    x = mean(sum(Xtraining .^ 2, 1));
    Xtraining = Xtraining / sqrt(x);
    clear zpcaXtrain pcaXtraining
end
%%
% assert(sum(mean(Xtraining, 2)) < 0.001)
X = [Xtraining; ones(1, Ntraining)];
clear Xtraining;

%%
initW = [randn(size(X, 1) - 1, m); zeros(1, m)];
sw = sum(initW .^ 2, 1);
sw = sqrt(sw);
initW = bsxfun(@rdivide, initW, sw);

x = load(save_file.train_classification);
best_up_dk = x.para_out.dk(end);
s = m / 32;
clear all_rhos
i = 1;
all_rhos(i, :) = best_up_dk + s; i = i + 1;
all_rhos(i, :) = best_up_dk; i = i + 1;
all_rhos(i, :) = best_up_dk - s; i = i + 1;
all_rhos(i, :) = best_up_dk - 2 * s; i = i + 1;
all_lambda = [0.1, 0.05, 0.01];

para_set = get_para_set(m, all_rhos, all_lambda);

%%
test_para_set;
save([type '_' num2str(m) '_one'], 'best_result2');

%% find the best parameters
one_best_W = best_W;
one_best_para_out = best_para_out;
one_best_eval = best_eval;
one_best_score = best_score;

initW = best_W;

%%
best_up_dk = one_best_para_out.dk;
s = m / 32;
init_dk = [best_up_dk - 3 * s, best_up_dk - 2 * s, best_up_dk - s, best_up_dk];
clear all_rhos
i = 1;
clear all_rhos;
all_rhos(i, :) = init_dk; i = i + 1;
all_rhos(i, :) = init_dk - s; i = i + 1;
all_rhos(i, :) = init_dk - 2 * s; i = i + 1;
all_lambda = best_para_out.lambda;
para_set = get_para_set(m, all_rhos, all_lambda);

gnd_selected = [50, 100, 200, 400];
I = read_classification_gnd(gnd_file.SortedPartTrainTrain, gnd_selected);
%%
test_para_set;
save([type '_' num2str(m) '_multi'], 'best_result2');

if one_best_score < best_score;
    W = best_W;
    para_out = best_para_out;
    eval_classification_k = best_eval;
else
    W = one_best_W;
    para_out = one_best_para_out;
    eval_classification_k = one_best_eval;
end

save(['best_' type '_' num2str(m) ], 'W', 'para_out', 'eval_classification_k');
%% find the best result in best_result
mlhs = load(save_file.test_mlh);
oph = load(save_file.test_classification);
ophk = load(['best_' type '_' num2str(m) ]);
for k = 1 : numel(all_topks)
    figure;
    best_v = 0;
    best_idx = -1;
    
    semilogx(oph.eval_classification{k}.avg_retrieved, oph.eval_classification{k}.rec, 'r-o', 'LineWidth', 2);
    hold on;
    semilogx(ophk.eval_classification_k{k}.avg_retrieved, ophk.eval_classification_k{k}.rec, 'k-o', 'LineWidth', 2);
    
    semilogx(mlhs.eval_mlh{k}.avg_retrieved, mlhs.eval_mlh{k}.rec, 'b-*', 'LineWidth', 2);
    
    legend('OPH', 'OPH\_kernel', 'MLH', 'Location', 'Best');
    grid on;
    xlim([10, 1000]);
    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    saveas(gca, [num2str(all_topks(k)) '.eps'], 'psc2');
end
matlabpool close
close all;
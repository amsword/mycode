function [W, para] = train_cbh_p(data, I, para, initW)
%% data: every column is a point in a high-dimensional space. size: [D N]
%% I: I(i, j, k) = 1, if x_j\in{N_k(x_i)}; 0 otherwise. The ground truth neighbourhood relationship
%% para: parameter, that can be changed. length of para.dk must be equal to the third dimension of I
%% W: the mapping function is W' * X;
%% initW must be [] or the real matrix
%% _p: sampling by pairs is supported
%% parameters

% beta = para.beta;
[D, N] = size(data);
pd = 1;
%% init W.
W = get_initW(initW, D, para.m);

X = [data; ones(1, N)];

if (para.is_set_dk_smart)
    para.dk = smart_set_dk();
end

para.rd_dk = para.dk;
para.rd_dk(para.rd_dk <= 0) = 0.3;
para.rd_dk2 = para.rd_dk - 1; % the threshold used for I_{ijk} and J_{ijk}, or be taken as the threshold in smoothed objective domain
% dk2 = dk;
para.rd_dk2(para.rd_dk2 <= 0) = 0.3;
para.rd_dk3 = para.rd_dk + 1;
para.rd_dk3(para.rd_dk3 <= 0) = 0.01;

% mu = para.mu;
% max_iter = para.max_iter;
% min_abs_deltaW = para.min_abs_deltaW;
num_center_sample = min(para.num_center_sample, N);

% para.I = I;

% beta and gamma, which is determined by dk and m

para.gamma = zeros(length(para.rd_dk), 2);
para.gamma(:, 1) = -2 * log(10^-3) ./ (2);
para.gamma(:, 2) = 1.5434 ./ ((2 + para.rd_dk) / 2);
% gamma(:, 2) = -2 * log(0.2) ./ (2 * rd_dk + 1 - rd_dk2);
%% calculate alpha_k, the parameters are not defined in the original paper
rho = length(para.dk);

% lambda = para.lambda;
% alpha = para.alpha;

subI = cell(1, rho);
pre_deltaW = zeros(size(W));

obj_para.alpha = para.alpha;
obj_para.beta = para.beta;
obj_para.gamma = para.gamma;
obj_para.lambda = para.lambda;
obj_para.mu = para.mu;
obj_para.rd_dk = para.rd_dk;
obj_para.rd_dk2 = para.rd_dk2;
obj_para.rd_dk3 = para.rd_dk3;

%% determine the parameters of learning rate
learning_rate_info.type_learning_rate = para.type_learning_rate;
if (strcmp(para.type_learning_rate, 'reciprocal_t'))
    % determine step0 and b; step(t) = step0 / (1 + b * t)
    rp = randperm(N);
%     step0 = determin_step0(W, X, rp, I, ...
%         obj_para, para.min_abs_deltaW, ...
%         pre_deltaW, para.momentum, num_center_sample)
    step0 = 1.25 * 10 ^ -4;
     b = 1 / D / para.m; % intuitively only
end
%%

t = 1;
for iter = 1 : para.max_iter
	if (strcmp(para.type_sample_scheme, 'center'))
		rp = randperm(N);
		num_batch = floor(N / num_center_sample);
		for batch_id = 1 : num_batch
			[num2str(iter) '/' num2str(para.max_iter) '. batch: '  num2str(batch_id) '/' num2str(num_batch)]
			center_index = rp([1 + (batch_id - 1) *  num_center_sample : batch_id * num_center_sample]);
			
			if (strcmp(learning_rate_info.type_learning_rate, 'reciprocal_t'))
				learning_rate_info.learning_rate = step0 / (1 + b * t);
				t = t + 1;
			end
			[W1, deltaW] = sample_train_min_batch(W, X, center_index, I, ...
				obj_para, para.min_abs_deltaW, ...
				learning_rate_info, pre_deltaW, para.momentum, num_center_sample);
			pre_deltaW = deltaW;
			W = W1;
		end
	elseif (strcmp(para.type_sample_scheme, 'pairs'))
		is = randperm(N);
		js = randperm(N);
		if (strcmp(learning_rate_info.type_learning_rate, 'reciprocal_t'))
			learning_rate_info.learning_rate = step0 / (1 + b * t);
			t = t + 1;
		end
		[W1, deltaW] = train_min_batch_by_pairs(W, X, is, js, I, ...
					obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum);
		pre_deltaW = deltaW;
		W = W1;
	end
end

% update W-->W1
% obj_para: the parameters in the objective function
function [W1, deltaW] = train_min_batch_by_pairs(W, X, is, js, I, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum)

W1 = W; % in case the function returns in some special cases
[deltaW pre] = relaxed_deltaW_by_pairs(...
        W, X, is, js, ...
        obj_para.alpha, ...
        obj_para.beta, ...
        obj_para.gamma, ...
		obj_para.lambda, ...
        obj_para.mu, ...
		obj_para.rd_dk, ... 
		obj_para.rd_dk2, ...
		obj_para.rd_dk3, ...
		1, ...
        I);

deltaW = momentum * pre_deltaW + ...
 	(1 - momentum) * deltaW;
    
length_deltaW = norm(deltaW(:));
if (length_deltaW < min_abs_deltaW)
	return;
end
   
if (strcmp(learning_rate_info.type_learning_rate, 'wolfe'))
	error('not finished');
    c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
    c2 = 0.5; % change of the step
    max_step_percent = 0.1;
    max_iter_find_step = 50;
	length_W = norm(W(:));
	step = max_step_percent * length_W  / length_deltaW;
	squared_length_deltaW = length_deltaW * length_deltaW;
	for i = 1 : max_iter_find_step
		W1 = W - step * deltaW;
		after = jf_objective_8(W1, X, center_index, ...
			alpha, beta, gamma, lambda, mu, rd_dk, rd_dk2, rd_dk3, 1, ...
			subI);
		target = pre - (c1 * step) * squared_length_deltaW;
		if (target <= 0)
			target = pre;
		end
		if (after <= target)
			break;
		else
			step = step * c2;
		end
	end
else % reciprocal_t or const
	W1 = W - learning_rate_info.learning_rate * deltaW;
	after = jf_objective_by_pairs(W1, X, is, js, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, obj_para.rd_dk, ...
        obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		I);
end

W_changed = norm(W1(:) - W(:)) / norm(W(:));
str = ['obj: ' num2str(pre) '->' num2str(after) ...
	'. W decent: ' num2str(W_changed) ...
	'. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
	'. time: ' num2str(toc) ...
	'\n'];

fprintf(str);

function W = get_initW(initW, D, m)
if (isempty(initW))
    'random generate W'
    W = [randn(D, m); zeros(1, m)]; % initialize
    normW = sqrt(sum(W .* W, 1));
    W = bsxfun(@rdivide, W, normW);
else
    'W has been initilized'
    W = initW;
end

function dk = smart_set_dk()
WX = W' * X;

sigX = sigmf(WX, [beta, 0]);

center_index = randperm(N);
center_index = center_index([1 : 10 ^ 3]);

if (pd == 2)
    s_sigX = sigX .* sigX;
    right = sum(s_sigX, 1);
    left = right(1, center_index)';
    hash_dist = repmat(left, 1, N) + repmat(right, num_center, 1) - ...
        2 * sigX(:, center_index)' * sigX;
elseif (pd == 1)
    hash_dist = jf_c_hash_distMP(sigX(:, center_index), sigX, 32);
end

hash_dist = sort(hash_dist, 2);

dk = zeros(numel(I), 1);
for i = 1 : numel(I)
    tmpI = I{i};
    num = sum(tmpI(1, :));
    dk(i) = mean(hash_dist(:, num));
end



function [W1, deltaW] = sample_train_min_batch(W, X, center_index, I, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum,...
    num_center_sample)


rho = numel(I);
subI = cell(rho, 1);
for i_rho = 1 : rho
    tmp_subIk = I{i_rho};
    subI{i_rho} = tmp_subIk(center_index, :);
end

[W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum);
    
% update W-->W1
% obj_para: the parameters in the objective function
function [W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
    obj_para, min_abs_deltaW, learning_rate_info, pre_deltaW, momentum)

tic;
W1 = W; % in case the function returns in some special cases
[deltaW pre] = jf_deltaW_8(...
        W, X, center_index, ...
        obj_para.alpha, ...
        obj_para.beta, ...
        obj_para.gamma, ...
		obj_para.lambda, ...
        obj_para.mu, ...
		obj_para.rd_dk, ... 
		obj_para.rd_dk2, ...
		obj_para.rd_dk3, ...
		1, ...
        subI);

deltaW = momentum * pre_deltaW + ...
 	(1 - momentum) * deltaW;
    
length_deltaW = norm(deltaW(:));
if (length_deltaW < min_abs_deltaW)
	return;
end
   
if (strcmp(learning_rate_info.type_learning_rate, 'wolfe'))
    c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
    c2 = 0.5; % change of the step
    max_step_percent = 0.1;
    max_iter_find_step = 50;
	length_W = norm(W(:));
	step = max_step_percent * length_W  / length_deltaW;
	squared_length_deltaW = length_deltaW * length_deltaW;
	for i = 1 : max_iter_find_step
		W1 = W - step * deltaW;
		after = jf_objective_8(W1, X, center_index, ...
			obj_para.alpha, obj_para.beta, obj_para.gamma, obj_para.lambda, obj_para.mu, ...
			obj_para.rd_dk, obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
			subI);
		target = pre - (c1 * step) * squared_length_deltaW;
		if (target <= 0)
			target = pre;
		end
		if (after <= target)
			break;
		else
			step = step * c2;
		end
	end
else % reciprocal_t or const
	W1 = W - learning_rate_info.learning_rate * deltaW;
	after = jf_objective_8(W1, X, center_index, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, obj_para.rd_dk, ...
        obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		subI);
end

W_changed = norm(W1(:) - W(:)) / norm(W(:));
str = ['obj: ' num2str(pre) '->' num2str(after) ...
	'. W decent: ' num2str(W_changed) ...
	'. w^2-1: ' num2str(mean(diag(abs(W1' * W1 - 1)))) ...
	'. time: ' num2str(toc) ...
	'\n'];

fprintf(str);

function step0 = determin_step0(W, X, rp, subI, ...
    obj_para, min_abs_deltaW, pre_deltaW, momentum, num_center_sample)

scale = 2;
step0 = 10^-3;

step1 = step0 * scale;

cost0 = evaluate_step(W, X, rp, subI, ...
    obj_para, min_abs_deltaW, step0, pre_deltaW, momentum, num_center_sample);

cost1 = evaluate_step(W, X, rp, subI, ...
    obj_para, min_abs_deltaW, step1, pre_deltaW, momentum, num_center_sample);

if (cost0 < cost1)
    t = cost1;
    cost1 = cost0;
    cost0 = t;
    
    t = step1;
    step1 = step0;
    step0 = t;
    scale = 1 / scale;
end

while (cost0 >= cost1)
    cost0 = cost1;
    step0 = step1;
    
    step1 = step1 * scale;
    cost1 = evaluate_step(W, X, rp, subI, ...
        obj_para, min_abs_deltaW, step1, pre_deltaW, momentum, num_center_sample);
end

function cost = evaluate_step(W, X, rp, I, ...
    obj_para, min_abs_deltaW, step, pre_deltaW, momentum, num_center_sample)

n_iter = 10;
learning_rate_info.type_learning_rate = 'const';
learning_rate_info.learning_rate = step;
rho = length(I);
subI = cell(rho, 1);
for i = 1 : n_iter
    % calculate the deltaW
    center_index = rp((i - 1) * num_center_sample + 1 : i * num_center_sample);
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end
    
    [W1, deltaW] = train_min_batch(W, X, center_index, subI, ...
        obj_para, min_abs_deltaW, ...
        learning_rate_info, pre_deltaW, momentum);
    pre_deltaW = deltaW;
    W = W1;    
end

cost = 0;
for i = 1 : n_iter
    % calculate the deltaW
    center_index = rp((i - 1) * num_center_sample + 1 : i * num_center_sample);
    
    % prepare the subI, since it is a cell array
    for i_rho = 1 : rho
        tmp_subIk = I{i_rho};
        subI{i_rho} = tmp_subIk(center_index, :);
    end
    
    cost = cost + jf_objective_8(W, X, center_index, ...
		obj_para.alpha, obj_para.beta, obj_para.gamma, ...
        obj_para.lambda, obj_para.mu, ...
        obj_para.rd_dk, obj_para.rd_dk2, obj_para.rd_dk3, 1, ...
		subI);
end
cost = cost / n_iter;

function W = train_rsh_support_sparse(data, I_plus1, para, initW )
%TRAINRSH Summary of this function goes here
%   Detailed explanation goes here

[D, N] = size(data);
num_center_sample = para.num_center_sample;

%% init W.
W = get_initW(initW, D, para.m);
X = [data; ones(1, N)];

c1 = 0; % the decent should be large enough, it controls how large. it should be (0, 0.5)
iter = 0;
max_step_percent = 0.3;
max_iter_find_step = 50;

bad_case = 0;
Q = zeros(1, max_iter_find_step);
step = zeros(1, max_iter_find_step + 1);

num_center_sample = para.num_center_sample;
num_center_sample = min(N, num_center_sample);
num_batch = floor(N / num_center_sample);

fprintf('beta = %f\n', para.beta);
fprintf('epsilon = %f\n', para.epsilon);
fprintf('num_center_sample = %d\n', num_center_sample);
fprintf('max_iter = %d\n', para.max_iter);

global gl_is_multi_thread;
if (isempty(gl_is_multi_thread))
    gl_is_multi_thread = false;
end
para

for iter = 1 : para.max_iter
    ThroughOneEpoch();
end
    function ThroughOneEpoch()
        rp = randperm(N);
        for i_batch = 1 : num_batch
            center_index = rp((i_batch - 1) * num_center_sample + 1 : ...
                i_batch * num_center_sample);
            
            subI = double(I_plus1(center_index, :));
            
            [idx_row, idx_col, exp_dist] = find(subI);
            
            exp_dist = exp_dist - 1;
            
            ThroughOneBatch();
        end
        
        function ThroughOneBatch()
            [deltaW pre] = deltaW_rsh();
            P = -deltaW;
            
            length_deltaW = norm(deltaW(:));
            squared_length_deltaW = length_deltaW * length_deltaW;
            
            if (length_deltaW < para.min_abs_deltaW)
                str = ['iter: ' num2str(iter) '. deltaW = 0\n'];
                fprintf(str);
                return;
            end
            
            step(1) = 0;
            Q(1) = pre;
            length_W = norm(W(:));
            
            diff_Q1 = sum(deltaW(:) .* P(:));
            step(2) = max_step_percent * length_W  / length_deltaW;
            is_bad_dir = false;
            for i = 2 : max_iter_find_step
                W1 = W + step(i) * P;
                after = objective_rankAbs3(W1);
                Q(i) = after;
                target = pre - (c1 * step(i)) * squared_length_deltaW;
                if (target <= 0)
                    target = pre;
                end
                if (after <= target)
                    break;
                else
                    next_step = jf_next_step(diff_Q1, step(1 : i), Q(1 : i));
                    if (isnan(next_step))
                        is_bad_dir = true;
                        break;
                    end
                    step(i + 1) = next_step;
                end
            end
            
            if (is_bad_dir || i == max_iter_find_step)
                return;
            end
            
            W_changed = norm(W1(:) - W(:)) / norm(W(:));
            obj_changed = (pre - after) / pre;
            str = ['iter: ' num2str(iter) '/' num2str(para.max_iter) ...
                '. batch_id: ' num2str(i_batch) '/' num2str(num_batch) ...
                '. obj: ' num2str(pre) ...
                '->' num2str(after) ...
                '. W decent: ' num2str(W_changed) ...
                '. w^2-1: ' num2str(mean(diag((W1' * W1 - 1)))) ...
                '\n'];
            fprintf(str);
            
            if (obj_changed < 10^-5)
                bad_case = bad_case + 1;
                if (bad_case >= 10)
                    return;
                end
            else
                bad_case = 0;
            end
            
            W = W1;
            
            if (para.is_force_normalize_w)
                error('erro');
                normW = sqrt(sum(W .^ 2, 1));
                W = W ./ repmat(normW, size(W, 1), 1);
            end
            
            function [deltaW, obj] = deltaW_rsh()
                [obj hash_dist subX sigX sig_idx_row, sig_idx_col] = ...
                    objective_rankAbs4(W);
                %% diff
                deltaW1 = zeros(size(W));
                
                % calculate the partial derivative of the objective function part
                if (para.po == 1)
                    diff_o = sign(hash_dist - exp_dist);
                elseif (para.po == 2)
                    diff_o = 2 * (hash_dist - exp_dist);
                end
                
                diff_sigX = para.beta * sigX .* (1 - sigX);
                
                for t = 1 : m
                    tmp_dij = sigX(t, sig_idx_row) - sigX(t, sig_idx_col);
                    if (para.pd == 1)
                        diff_d = sign(tmp_dij);
                    elseif (para.pd == 2)
                        diff_d = 2 * tmp_dij;
                    end
                    PQ = diff_o .* diff_d';
                    diff_x = bsxfun(@times, subX, diff_sigX(t, :));
                    
                    PQ = bsxfun(@times, diff_x(:, sig_idx_row) - diff_x(:, sig_idx_col), PQ');
                    deltaW1(:, t) = sum(PQ, 2);
                end
                
                deltaW1 = deltaW1 / numel(idx_row);
                deltaW2 = para.mu * W * diag(diag(W' * W) - 1);
                deltaW = deltaW1 + deltaW2;
            end
            function [obj hash_dist subX sigX sig_idx_row, sig_idx_col] = objective_rankAbs4(W)
                num = numel(idx_row);
                %% hash dist: get the all involved X that should be multiplied by W
                is_involved = false(N, 1);
                is_involved(idx_row) = true;
                is_involved(idx_col) = true;
                %
                subX = X(:, is_involved);
                sigX = W' * subX;
                sigX = sigmf(sigX, [para.beta 0]);
                
                map_idx = zeros(N, 1);
                map_idx(is_involved) = 1 : sum(is_involved);
                sig_idx_row = map_idx(idx_row);
                sig_idx_col = map_idx(idx_col);
                
                hash_dist = zeros(num, 1);
                for t = 1 : m
                    tmp_diff = sigX(t, sig_idx_row) - sigX(t, sig_idx_col);
                    if (para.pd == 1)
                        tmp_diff = abs(tmp_diff);
                    elseif (para.pd == 2)
                        tmp_diff = tmp_diff .* tmp_diff;
                    end
                    hash_dist = hash_dist + tmp_diff';
                end
                
                tmp_diff = hash_dist - exp_dist;
                if (para.po == 1)
                    tmp_diff = abs(tmp_diff);
                elseif (para.po == 2)
                    tmp_diff = tmp_diff .* tmp_diff;
                end
                obj = sum(tmp_diff) / num;
                
                wt = diag(W' * W - 1);
                p_sum = 1 / 4 * para.mu * (wt' * wt);
                obj = obj + p_sum;
            end
        end
    end
end

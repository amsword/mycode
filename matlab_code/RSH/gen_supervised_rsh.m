% function I = gen_supervised_rsh(Xtraining, m)
%: distance ~ N(mu, sigma^2), x
%: binomial ~ N(m / 2, m / 4), y
% y = sqrt(m) / 2 / sigma * (x - mu) + 0.5 * m

N = size(Xtraining, 2);

[~, ~, dist] = read_gnd('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\Labelme\data\c_data\random_sample_NxN.bin', 'float64');

%%
% [s_dist] = sort(dist(:));
% edges = zeros(m + 2);
% total_count = numel(s_dist);
% for i = 1 : m
%     tmp_idx = binocdf(i - 1, m, 0.5) * total_count;
%     tmp_idx = round(tmp_idx);
%     tmp_idx(tmp_idx <= 0) = 1;
%     tmp_idx(tmp_idx > total_count) = total_count;
%     edges(i + 1) = s_dist(tmp_idx);
% end
% edges(m + 2) = Inf;
% n = histc(s_dist(:), edges);
% plot(n);

%%
m = 32;
mu = mean(dist(:));
s_sigma = var(dist(:));

exp_dist = sqrt(m) / 2 / sqrt(s_sigma) * (dist - mu) + 0.5 * m;
exp_dist = round(exp_dist);
% exp_dist(exp_dist < 0) = 0;
% exp_dist(exp_dist > m) = m;

figure
hist(exp_dist(:));
function e = RSHFunc(alpha, hash_code, exp_dist)
[is js v] = find(exp_dist + 1);
v = double(v - 1);

n = numel(v);
batch_size = 2000;
batch_num = ceil(n / batch_size);

e = 0;
for i = 1 : batch_num
    if (mod(i, 1000) == 0)
        [num2str(i) '/' num2str(batch_num)]
    end
    idx_start = (i - 1) * batch_size + 1;
    idx_end = i * batch_size;
    if (idx_end > n)
        idx_end  = n;
    end
    
    sub_is = is(idx_start : idx_end);
    sub_js = js(idx_start : idx_end);
    sub_v = v(idx_start : idx_end);
    
    left = hash_code(: , sub_is) - hash_code(:, sub_js);
    left = abs(left);
    calc_v = alpha' * left;
    calc_v = calc_v';
    e = e + sum((sub_v - calc_v) .^ 2);
end
e = e / n;
    
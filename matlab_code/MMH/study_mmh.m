
% [W_mmh, objs, deltas] = trainMMH(1, 1);
[W_mmh, obj_left, obj_right, deltas] = trainMMH(Xtraining, m, A);

subplot(2, 1, 1)
semilogy(abs(obj_left), 'r');
subplot(2, 1, 2);
plot(obj_left, 'r');

figure
subplot(2, 1, 1);
semilogy(abs(obj_right), 'b');
subplot(2, 1, 2);
plot(obj_right, 'b');

figure
subplot(2, 1, 1);
semilogy(abs(obj_left + obj_right), 'g');
subplot(2, 1, 2);
plot(obj_left + obj_right, 'g');
% title('obj');
% figure;
% plot(deltas);

%
true_margin(Xtraining, W_mmh(1 : end - 1, :))
x = load(save_file.train_mlh);
true_margin(Xtraining, x.W_mlh(:, 1 : end - 1)')

%%
eval_mmh = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    eval_mmh{k} = eval_hash5(m, 'linear', ...
        W_mmh, Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end

%
mlhs = load(save_file.test_mlh);

for k = 1 : numel(all_topks)
    figure;
    plot(mlhs.eval_mlh{k}.r_code, 'b'); hold on;
    plot(eval_mmh{k}.r_code, 'r');

    axis([0 1000 0 1]);
    grid on;
%     saveas(gca, ['C:\Users\v-jianfw\Desktop\labelme32\' num2str(all_topks(k)) '-NN.eps'], 'psc2');
%     close;
end
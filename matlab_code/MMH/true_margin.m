function y = true_margin(Xtraining, A)

y = A' * Xtraining;
b = sum(A .^ 2, 1)';

y = bsxfun(@rdivide, abs(y), b);
y = sum(y(:));
function [W obj_left obj_right deltas] = trainMMH(Xtraining, m, A)
% maximum margin hashing, in fact, it is the same with pca


t = sum(A .^ 2, 1);
t = t / m;
A = bsxfun(@rdivide, A, t);

XXT = Xtraining * Xtraining';
N = size(Xtraining, 2);
XXT = XXT / N;

L = ones(m, m);
L(:) = 10;
for i = 1 : m
    L(i, i) = 1;
end

for i = 1 : 200
    delta = delta_value(XXT, A, L);
    
    [y_old, obj_left(i) obj_right(i)] = obj_value(XXT, A, L);
    deltas(i) = norm(delta);
    
    step = norm(A) / norm(delta);
    is_good = false;
    for j = 1 : 15
        A_new = A - delta * step;
        y_new = obj_value(XXT, A_new, L);
        if (y_new < y_old)
            is_good = true;
            break;
        else
            step = 0.1 * step;
        end
    end
    if (~is_good)
        assert(0);
        break;
    end

    A = A_new;
%     L = L * 10;
end

W = [A; zeros(1, m)];

end


function [y left right] = obj_value(XXT, A, L)
left = -trace(A' * XXT * A);
m = size(A, 2);
right = (trace(L * (A' * A - m * eye(size(A, 2))))) ^ 2;
y = left + right ;
end

function delta = delta_value(XXT, A, L)
m = size(A, 2);
delta = -2 * XXT * A + 2 * trace(L * (A' * A - m * eye(size(A, 2)))) * A * (L + L');
end
~~~~~~~~ Data
-- LabelMe and Peekaboom: downloaded from http://cs.nyu.edu/~fergus/research/tfw_cvpr08_code.zip, dataset is in the archive. Put under directory as data/LabelMe_gist.mat and data/Peekaboom_gist.mat

-- SIFT-1M downloaded from http://corpus-texmex.irisa.fr/, ANN_SIFT1M dataset. 'learn', 'query', 'base' are obtained after using the routine provided from http://corpus-texmex.irisa.fr/fvecs_read.m by the author, put under data/sift-1m.mat

~~~~~~~~ Usage
-- LabelMe experiment: use run_labelme.m to train the model, results stored in /result, and use eval_labelme_src.m to get the visualized results.

-- Peekaboom experiment: use run_peekaboom.m in the Peekaboom directory, results stored in /result, and use eval_peekaboom_src.m to get the visualized results.

-- SIFT-1M experiment: 1) run run_gnd.m to get ground information of the training data
						2) run run_sift.m to train the model
						3) run get_base_gnd.m to get the testing groundtruth of the data
						4) run eval_sift_src.m to get evaluation results and plots
						
						
~~~~~~~~~ Directory
-- utlis: utilitis for computing hamming distance, euclidean distance and evaluation	 (adopted from MLH code)
-- Labelme: scripts for labelme experiments
-- Peekaboom: scripts for peekaboom experiments
-- SIFT: scripts for SIFT-1M experiments
-- CBH: implementation of proposed algorithm
	- SIG.m : wrapper for SIFT-1M data to sequentially loading data for training
	- trainSIG.m: implementation of CBH
	- trainSIG_SIFT.m: implentation of CBH, differs in sampling methods (designed for SIFT dataset)
-- MLH: slightly modified, excluded the validation procedure. original code from http://www.cs.toronto.edu/~norouzi/research/mlh/
-- CH, LSH, SH, USPLH: implementations provided by authors
s
----------------------------------------------------------------------------------------------------------------
File name convention
every folder must contain a script, named, 'foldername_main' or main_foldername. 
This should contain all the utilities provided from the folder
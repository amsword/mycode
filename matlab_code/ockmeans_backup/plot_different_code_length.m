%% dist ratio


all_code_length = [32 64 128];

if strcmp(type, 'SIFT1B')
    used = 100;
    all_code_length = [32 64 128];
elseif strcmp(type, 'SIFT1M3')
    all_code_length = [32 64 128];
    used = 100;
elseif strcmp(type, 'GIST1M3')
    all_code_length = [32 64 128];
    used = 100;
end


num_code_length = numel(all_code_length);
all_ock_sym = zeros(1, num_code_length);
all_jck_sym = zeros(1, num_code_length);
all_ck_sym = zeros(1, num_code_length);
all_pq_sym = zeros(1, num_code_length);
all_ock = zeros(1, num_code_length);
all_jck = zeros(1, num_code_length);
all_ck = zeros(1, num_code_length);
all_pq = zeros(1, num_code_length);
for k_code_length  = 1 : numel(all_code_length)
    code_length = all_code_length(k_code_length);
    for is_sym = [0 1];
        % all_dic_num = [4 8 16];
        clear pq ck jck ock
        var_name = 'rec';
        if is_sym
            var_name = [var_name '_sym'];
        end
        
        dic_num = code_length / 8;
        
        pq = load(['pq_' num2str(dic_num) '_256'], var_name);
        ck = load(['ck_' num2str(dic_num) '_256'], var_name);
        
        dic_num = dic_num / 2;
        jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
        ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
        
        if is_sym
            all_ock_sym(k_code_length) = ock.rec_sym(used);
            all_jck_sym(k_code_length) = jck.rec_sym(used);
            all_ck_sym(k_code_length) = ck.rec_sym(used);
            all_pq_sym(k_code_length) = pq.rec_sym(used);
        else
            all_ock(k_code_length) = ock.rec(used);
            all_jck(k_code_length) = jck.rec(used);
            all_ck(k_code_length) = ck.rec(used);
            all_pq(k_code_length) = pq.rec(used);
        end
    end
end

marker_size = 9;
figure;
semilogx(all_code_length, all_ock, 'r-', ...
    'LineWidth', 2, ...
    'marker', marker_ock, ...
    'markersize', marker_size);
hold on;
plot(all_code_length, all_jck, 'g-', ...
    'LineWidth', 2, ...
    'marker', marker_eck, ...
    'markersize', marker_size);
plot(all_code_length, all_ck, 'k-', ...
    'LineWidth', 2, ...
    'marker', marker_ck, ...
    'markersize', marker_size);
plot(all_code_length, all_pq, 'b-', ...
    'LineWidth', 2, ...
    'marker', marker_pq, ...
    'markersize', marker_size);

plot(all_code_length, all_ock_sym, 'r--', ...
    'LineWidth', 2, ...
    'marker', marker_ock, ...
    'markersize', marker_size);
plot(all_code_length, all_jck_sym, 'g--', ...
    'LineWidth', 2, ...
    'marker', marker_eck, ...
    'markersize', marker_size);
plot(all_code_length, all_ck_sym, 'k--', ...
    'LineWidth', 2, ...
    'marker', marker_ck, ...
    'markersize', marker_size);
plot(all_code_length, all_pq_sym, 'b--', ...
    'LineWidth', 2, ...
    'marker', marker_pq, ...
    'markersize', marker_size);

xlabel('Code length', 'FontSize', 14);
ylabel(['Recall@' num2str(used) '-NN'], 'FontSize', 14);

lo = 'SouthEast';
legend('ock-means-A', ...
    'eck-means-A', ...
    'ck-means-A', ...
    'PQ-A', ...
    'ock-means-D', ...
    'eck-means-D', ...
    'ck-means-D', ...
    'PQ-D', ...
    'Location', lo);

set(gca, 'XTick', all_code_length);
xlim([2^(log(min(all_code_length)) / log(2) - 0.5), ...
    2^(log(max(all_code_length)) / log(2) + 0.5)]);
set(gca, 'FontSize', gca_font_size);
grid on;
set(gcf, 'PaperPositionMode', 'auto');
saveas(gca, ['figs\' type '_rec_code' num2str(used) '.eps'],'psc2');

function all_D = UpdateDictionary(Xtraining, R, all_B, dic_dim, num_split)

Z = R' * Xtraining;

dim = size(Xtraining, 1);

num_dic = dim / dic_dim;

all_D = cell(num_dic, 1);

for i = 1 : num_dic
    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    %     idx_b_start = (i - 1) * dic_size + 1;
    %     idx_b_end = i * dic_size;
    
    subB = double(all_B{i});
    
    %     all_D{i} = (subZ * subB')  / (subB * subB');
    all_D{i} = solve_Z_DB(subZ, subB, num_split);
    
end

end

function D = solve_Z_DB(subZ, subB, num_split)

n = size(subB, 1);
lambda = 10^(-5);
D = (subZ * subB')  / (subB * subB' + lambda * eye(n));

return;

[U S V] = svd(full(subB * subB'));

diag_value = diag(S);
ind = diag_value > 10^-5;

if ~find(~ind)
    D = (subZ * subB')  / (subB * subB');
else
    selected_diag_value = diag_value(ind);
    non_zero_rhs = subZ * subB' * V;
    hat_D1 = non_zero_rhs(:, ind) * ...
        diag(selected_diag_value .^ -1);
    
    D = subZ(:, randperm(size(subZ, 2), size(subB, 1)));
    hat_D = D * U;
    hat_D2 = hat_D(:, ~ind);
    hat_D(:, ind) = hat_D1;
    hat_D(:, ~ind) = hat_D2;
    D = hat_D * U';
end

return;
s = sum(subB, 2);
ind = full((s ~= 0));

D = zeros(size(subZ, 1), size(subB, 1));

size_split = size(subB, 1) / num_split;


D(:, ~ind) = subZ(:, randperm(size(subZ, 2), sum(ind == 0)));


for idx_split = 1 : num_split
    ind_empty = false(size(subB, 1), 1);
    idx_start = (idx_split - 1) * size_split + 1;
    idx_end = idx_split * size_split;
    
    ind_empty(idx_start : idx_end) = ~ind(idx_start : idx_end);
    
    empty_pos = (ind_empty == 1);
    empty_count = sum(empty_pos);
    if empty_count > 0
        X = subZ(:, randperm(size(subZ, 2), empty_count));
        for i = 1 : (idx_split - 1)
            idx_d_start = (i - 1) * size_split + 1;
            idx_d_end = i * size_split;
            
            distance_2_dic = sqdist(X, D(:, idx_d_start : idx_d_end));
            [~, idx] = min(distance_2_dic, [], 2);
            X = X - D(:, idx + idx_d_start - 1);
        end        
        D(:, ind_empty) = X;
    end
end
end

function all_D = RandomInitDictionary3(Xtraining, dic_dim, dic_size, num_split)
%% k-means clustering to init


dim = size(Xtraining, 1);

num_dic = dim / dic_dim;

all_D = cell(num_dic, 1);

size_split = dic_size / num_split;
assert(mod(dic_size, num_split) == 0);

for i = 1 : numel(all_D)
    
    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    X = Xtraining(idx_dim_start : idx_dim_end, :);
    
    subD = zeros(dic_dim, dic_size);
    
    for idx_split = 1 : num_split
        [C, A] = vl_kmeans(X, size_split);
        subD(:, ((idx_split - 1) * size_split + 1): (idx_split * size_split)) = C;
        X = X - C(:, A);
    end
    all_D{i} = subD;
end
end
function [mat_compact_B] = UpdateRepresentationCompact(...
    Xtraining, R, all_D, s0, method)

Z = R' * Xtraining;

dic_dim = size(all_D{1}, 1);

num_words_each = s0;
if s0 == -1
    num_words_each = 1;
end

num_point = size(Xtraining, 2);

mat_compact_B = zeros(numel(all_D) * num_words_each, num_point, 'int16');

for i = 1 : numel(all_D)
    subD = all_D{i};

    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    new_compact_SubB = mexMatchingPersuit(subZ, subD, s0, method);

    mat_compact_B((i - 1)* num_words_each + 1 : i * num_words_each, : ) = ...
        new_compact_SubB;
    
end
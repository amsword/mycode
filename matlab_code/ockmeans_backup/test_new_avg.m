 % Xtraining = read_mat(src_file.c_train, 'double');

cd('C:\Users\uqjwan34\Desktop\codes2\HashCode');
jf_conf;
clear;
%%
m = [64]
 type = 'SIFT1B'; % PeekaboomOrigin; LabelmeOrigin; SIFTOrigin; SIFT100K; sift_1m; ForQuantize
% CIFAR10, Labelme, sift_1m, peekaboom, GIST1M3, 
% BRISK10M, SIFT1M3, Tiny80M, SIFT1B, Tiny80MNN
file_pre_date = '';

jf_compare;

working_dir = [gl_data_parent_folder type '\working_sckmeans\non_full_training'];
cd(working_dir)
%% get the training data
if strcmp(type, 'Tiny80MNN')
    Xtraining = read_tiny_gist_binary(...
        1001 : 1001000);
    Xtraining = double(Xtraining);
elseif strcmp(type, 'SIFT1B')
    file_name = [gl_data_parent_folder type '\data\learn.bvecs'];
    Xtraining = bvecs_read(file_name, ...
         [1, 1000000]);
        Xtraining = double(Xtraining);
else
    Xtraining = read_mat(src_file.c_train, 'double');
end



%% test and database
if strcmp(type, 'Tiny80MNN')
    Xbase = 1
    Xtest = read_tiny_gist_binary(1 : 1000);
    Xtest = double(Xtest);
elseif strcmp(type, 'SIFT1B')
    Xbase = 2
    file_name = [gl_data_parent_folder type '\data\queries.bvecs'];
    Xtest = bvecs_read(file_name);
    Xtest = double(Xtest);
else
    Xbase = read_mat(src_file.c_base, 'double');
    Xtest = read_mat(src_file.c_test, 'double');

end
ground_truth_file = gnd_file.STestBase;
    
%% get the para_set
% method_type = 0: PQ
%                           = 1: ck
%                           = 2 : jck
%                           = 3 : ock;

dic_size = 256;
dic_num = m / log(dic_size) * log(2) / 2
para_idx = 1;
clear para_set;

% ck
para_set(para_idx).method_type = 1;
para_set(para_idx).method = 0;
para_set(para_idx).s0 = -1;
para_idx = para_idx + 1;

% pq
para_set(para_idx).method_type = 0;
para_set(para_idx).method = 0;
para_set(para_idx).s0 = -1;
para_idx = para_idx + 1;



% jck
% para_set(para_idx).method_type = 2;
% para_set(para_idx).method = 1;
% para_set(para_idx).s0 = 2;
% para_idx = para_idx + 1;

% all_s0 = [-1, 1, 2, 3, 4, 5, 6];
% 
% % ock
% for method = [102]
%     para_set(para_idx).method_type = 3;
%     para_set(para_idx).method = method;
%     para_set(para_idx).s0 = 2;
%     para_idx = para_idx + 1;
% end

numel(para_set)
return;
%% run 
% x = load('ock_4_32_2_102.mat', 'all_D', 'R', 'all_B');
% init_all_D = x.all_D;
% 
% z = zeros(size(init_all_D{1}, 1), dic_size);
% for i = 1 : dic_num
%     init_all_D{i} = [init_all_D{i}, z];
% end
% init_B = x.all_B;
% Ntraining = size(Xtraining, 2);
% s = sparse(ones(1, Ntraining), 1 : Ntraining, ...
%     1, dic_size, Ntraining);
% for i = 1 : dic_num
%     init_B{i} = [init_B{i}; s];
% end
% init_R = x.R;

%%
init_all_D = [];
init_R = [];
init_B = [];

for idx = 1 : numel(para_set)
    s0 = para_set(idx).s0;
    method = para_set(idx).method;
    method_type = para_set(idx).method_type;
    
  run_training_test_sift1b(Xtraining, ...
        Xtest, ...
        Xbase, ...
        dic_num, ...
        s0, ...
        method, ...
        method_type, ...
        ground_truth_file, ...
        working_dir, ...
        dic_size, ...
        init_all_D, ...
        init_R, ...
        init_B);
%     run_training_test_sift1b;
end

%%
x = load('ck_8_256', 'retrieval_result');
 retrieval_result = x.retrieval_result;
 %%
 avg = mexResultDistanceRatio(retrieval_result, ...
     Xtest, ...
     Xbase, ...
     ground_truth_file);
%%

function all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size)

dim = size(Xtraining, 1);
num_point = size(Xtraining, 2);

num_dic = dim / dic_dim;

all_D = cell(num_dic, 1);

rng(0);

for i = 1 : numel(all_D)
    rp = randperm(num_point, dic_size);
    
    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    all_D{i} = Xtraining(idx_dim_start : idx_dim_end, rp);
end
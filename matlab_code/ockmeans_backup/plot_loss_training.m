clear x;
x = zeros(4, 4);
all_dic_num = [2 4 8 16];
for idx_dic_num = 1 : 4
    dic_num = all_dic_num(idx_dic_num);
    pq = load(['pq_' num2str(dic_num) '_256'], 'obj');
    ck = load(['ck_' num2str(dic_num) '_256'], 'obj');
    jck = load(['jck_' num2str(dic_num) '_256_2'], 'obj');
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], 'obj');
    
    x(1, idx_dic_num) = pq.obj(end);
    x(2, idx_dic_num) = ck.obj(end);
    x(3, idx_dic_num) = jck.obj(end);
    x(4, idx_dic_num) = ock.obj(end);
end

% x(:, 3) = x(:, 2) / 2;
figure;
semilogx(all_dic_num, x(4, :), 'r-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_ock);
hold on;
plot(all_dic_num, x(3, :), 'G-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_eck);
plot(all_dic_num, x(2, :), 'k-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_ck);
plot(all_dic_num, x(1, :), 'b-', 'LineWidth', 1.5, ...
    'MarkerSize', 10, ...
    'Marker', marker_pq);

lo = 'NorthEast';
if strcmp(type, 'GIST1M3')
    ylim([1, 6] * 10^5);
    lo = 'SouthWest';
end

legend('ock-means', ...
    'eck-means', ...
    'ck-means', ...
    'PQ', ...
    'Location', lo);

xlabel('M', 'FontSize', 16);
ylabel('Distortion', 'FontSize', 16);
set(gca, 'XTick', all_dic_num);
set(gca, 'FontSize', 16);
xlim([2^0.7, 2^4.3]);

grid on;
set(gcf, 'PaperPositionMode', 'auto');
saveas(gca, ['figs\' type '_loss_train.eps'],'psc2');
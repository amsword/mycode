function [cell_sparse_B] = UpdateRepresentation(...
    Xtraining, R, all_D, old_cell_sparse_B, s0, method)


Z = R' * Xtraining;

dic_dim = size(all_D{1}, 1);
cell_sparse_B = cell(numel(all_D), 1);

num_point = size(Xtraining, 2);

for i = 1 : numel(all_D)
    subD = all_D{i};

    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    new_compact_SubB = mexMatchingPersuit(subZ, subD, s0, method);

    new_full_SubB = Compact2Binary(new_compact_SubB, size(subD, 2), ...
        mod(method, 10));
    
    new_error = sum((subZ - subD * new_full_SubB) .^ 2, 1);
    
    if s0 >= 0 && ~isempty(old_cell_sparse_B)
        oldSubB = old_cell_sparse_B{i};
        original_error = sum((subZ - subD * oldSubB) .^ 2, 1);
        no_need_update = new_error > original_error;
        sum(no_need_update);
        new_full_SubB(:, no_need_update) = oldSubB(:, no_need_update);
    end
    cell_sparse_B{i} = sparse(new_full_SubB);
end








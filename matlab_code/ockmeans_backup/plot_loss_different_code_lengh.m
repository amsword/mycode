%% dist ratio

used = 10;
all_code_length = [16 32 64 128];

if strcmp(type, 'SIFT1B')
    used = 100;
    all_code_length = [32 64 128];
elseif strcmp(type, 'SIFT1M3')
    all_code_length = [32 64 128];
end


num_code_length = numel(all_code_length);
all_ock_sym = zeros(1, num_code_length);
all_jck_sym = zeros(1, num_code_length);
all_ck_sym = zeros(1, num_code_length);
all_pq_sym = zeros(1, num_code_length);
all_ock = zeros(1, num_code_length);
all_jck = zeros(1, num_code_length);
all_ck = zeros(1, num_code_length);
all_pq = zeros(1, num_code_length);
for k_code_length  = 1 : numel(all_code_length)
    code_length = all_code_length(k_code_length);
    % all_dic_num = [4 8 16];
    clear pq ck jck ock
    var_name = 'loss_base';
    
    dic_num = code_length / 8;
    
    pq = load(['pq_' num2str(dic_num) '_256'], var_name);
    ck = load(['ck_' num2str(dic_num) '_256'], var_name);
    
    dic_num = dic_num / 2;
    jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
    ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
    
    all_ock(k_code_length) = ock.loss_base;
    all_jck(k_code_length) = jck.loss_base;
    all_ck(k_code_length) = ck.loss_base;
    all_pq(k_code_length) = pq.loss_base;
    
end

marker_size = 9;
figure;
semilogx(all_code_length, all_ock, 'r-', ...
    'LineWidth', 2, ...
    'Marker', marker_ock, ...
    'markersize', marker_size);
hold on;
plot(all_code_length, all_jck, 'g-', 'LineWidth', 2, ...
    'Marker', marker_eck, ...
    'markersize', marker_size);
plot(all_code_length, all_ck, 'k-', 'LineWidth', 2, ...
    'Marker', marker_ck, ...
    'markersize', marker_size);
plot(all_code_length, all_pq, 'b-', 'LineWidth', 2, ...
    'Marker', marker_pq, ...
    'markersize', marker_size);

xlabel('Code length', 'FontSize', 14);
ylabel('Distortion', 'FontSize', 14);

lo = 'Best';
legend('ock-means', ...
    'eck-means', ...
    'ck-means', ...
    'PQ', ...
    'Location', lo);

set(gca, 'XTick', all_code_length);
xlim([2^(log(min(all_code_length)) / log(2) - 0.5), ...
    2^(log(max(all_code_length)) / log(2) + 0.5)]);
set(gca, 'FontSize', gca_font_size);
grid on;
set(gcf, 'PaperPositionMode', 'auto');
saveas(gca, ['figs\' type '_loss_base_code.eps'],'psc2');

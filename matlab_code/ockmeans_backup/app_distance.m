function dist = app_distance(Z, all_D, B)

num_code_word = size(all_D{1}, 2);

num_point = size(B, 2);

dim = size(Z, 1);

base = zeros(dim, num_point);

num_table = numel(all_D);

for i = 1 : num_table
    idx_dim_start = (i - 1) * num_code_word + 1;
    idx_dim_end = i * num_code_word;
    
    base(idx_dim_start : idx_dim_end, :) = all_D{i} * B(idx_dim_start : idx_dim_end, :);
end

dist = sqdist(Z, base);

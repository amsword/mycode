if code_length == 32
    if strcmp(type, 'GIST1M3')
        used = int32(10 .^ [2 : 0.1 : 4]);
    elseif strcmp(type, 'SIFT1M3')
        used = int32(10 .^ [1 : 0.1 : 4]);

    elseif strcmp(type, 'SIFT1B')
        used = int32(10 .^ [1 : 0.1 : 4]);
    end
elseif code_length == 64
    if strcmp(type, 'GIST1M3')
        used = int32(10 .^ [0 : 0.1 : 2]);
    elseif strcmp(type, 'SIFT1M3')
        lo = 'SouthEast';
    elseif strcmp(type, 'SIFT1B')
        used = 1 : 10^4;
    end
elseif code_length == 128
    if strcmp(type, 'GIST1M3')
        used = int32(10 .^ [1 : 0.1 : 3]);
    elseif strcmp(type, 'SIFT1M3')
        used = 1 : 100;
    elseif strcmp(type, 'SIFT1B')
        used = int32(10 .^ [0 : 0.1 : 3]);
    end
elseif code_length == 16
    used = int32(10 .^ [0 : 0.1 : 4]);
end
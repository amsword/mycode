all_type = {'SIFT1M3', 'GIST1M3', 'SIFT1B'};

% all_type = all_type(1 : 2);

for k_type = 1 : numel(all_type)

type = 'GIST1M3'; % SIFT1B; SIFT1M3; GIST1M3
type = all_type{k_type};

machine_name = getComputerName();
machine_name(end) = [];
if strcmp(machine_name, 'dewberry')
    gl_data_parent_folder = '\\dragon\Data\Jianfeng\Data_HashCode\';
elseif strcmp(machine_name, 'dragon')
    gl_data_parent_folder = 'E:\Data\Jianfeng\Data_HashCode\';
elseif strcmp(machine_name, 'gs624-0145')
    gl_data_parent_folder = 'E:\Data_HashCode\';
elseif strcmp(machine_name, 'j-pc')
    gl_data_parent_folder = 'H:\Data_HashCode\';
end

working_dir = [gl_data_parent_folder type '\working_sckmeans\non_full_training'];
cd(working_dir)

gca_font_size = 14;

marker_ock = 's';
marker_eck = 'd';
marker_ck = 'o';
marker_pq = '*';

plot_loss_base;
plot_loss_training;
end
%% loss on base
plot_loss_base;
%% 
plot_loss_different_code_lengh;
%% loss on training
plot_loss_training;

%% recall with different M 
plot_rec_M;
%%
%% MOR ->M
plot_mor_M
%% rec with candidate point
plot_rec
%% rec, sym and asym in one figure
plot_rec_one
%%
plot_mor
%%
plot_mor_one

%% rec
plot_different_code_length
%% different T
clear x;
all_dic_num = [4 8 16];
idx_dic_num = 2;
dic_num = all_dic_num(idx_dic_num);

for i = 1 : 15
    ock = load(['ock_' num2str(dic_num) ...
        '_256_2_' num2str(i) '2'], 'obj');
    
    x(i) = ock.obj(end);
end
% x(:, 3) = x(:, 2) / 2;

plot(x, 'ro-', 'LineWidth', 2);
set(gca, 'FontSize', 16);
xlabel('T', 'FontSize', 14);
ylabel('Distortion', 'FontSize', 14);
set(gcf, 'PaperPositionMode', 'auto');
grid on;

saveas(gca, ['figs\' type '_diff_T.eps'],'psc2');
%%
x = load('convergence\ock_8_256_2_102', 'obj');
t = x.obj(3 : 3 : 900);
 plot(1 : 10 : 300, t(1 : 10 : 300), 'ro-', 'LineWidth', 2);
 set(gca, 'FontSize', 16);
 xlabel('Number of Iterations', 'FontSize', 14);
 ylabel('Distortion', 'FontSize', 14);
 grid on;
 set(gcf, 'PaperPositionMode', 'auto');
 saveas(gca, ['figs\ock_8_256_2_102' '_convergence.eps'],'psc2');
 
 %% different C with dic_num fixed
dic_num = 4;
dic_size = 32;

clear x;
file_name = ['ck_' num2str(dic_num) ...
    '_' num2str(dic_size) '.mat'];
x = load(file_name, 'rec');
ck = x.rec;
semilogx(ck, 'LineWidth', 2, 'Color', 'k');


clear x;
hold on;
file_name = ['ock_' num2str(dic_num) ...
    '_' num2str(dic_size) '_2_102.mat'];
x = load(file_name, 'rec');
rec = x.rec;
semilogx(rec, 'LineWidth', 2, 'Color', 'b');

%

clear x;
hold on;
file_name = ['ock_' num2str(dic_num) ...
    '_' num2str(dic_size) '_3_102.mat'];
x = load(file_name, 'rec');
rec = x.rec;
semilogx(rec, 'LineWidth', 2, 'Color', 'g');

%%
clear x;
file_name = ['ock_' num2str(dic_num) ...
    '_' num2str(dic_size) '_4_102.mat'];
x = load(file_name, 'rec');
rec = x.rec;
semilogx(rec, 'LineWidth', 2, 'Color', 'r');
 
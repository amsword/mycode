function run_training_test_sift1b(Xtraining, ...
        Xtest, ...
        Xbase, ...
        dic_num, ...
        s0, ...
        method, ...
        method_type, ...
        ground_truth_file, ...
        working_dir, ...
        dic_size, ...
        init_all_D, ...
        init_R, ...
        init_B)


line = ['dic_num: ' num2str(dic_num)];
fprintf('%s\n', line);
line = ['s0: ' num2str(s0)];
fprintf('%s\n', line);
line = ['method: ' num2str(method)];
fprintf('%s\n', line);
line = ['method_type: ' num2str(method_type)];
fprintf('%s\n', line);
line = ['working_dir: ' num2str(working_dir)];
fprintf('%s\n', line);
 
dim = size(Xtraining, 1);
dic_dim = dim / dic_num;
assert(mod(dim, dic_num) == 0);
% dic_size = 256;
max_iter = 100;

file_name = get_file_name(...
    dic_num, dic_size, ...
    s0, method, method_type);

file_name = [working_dir '\' file_name]

%% training
if method_type == 0
    is_opt_R = 0;
else
    is_opt_R = 1;
end
is_run = 1;
if exist(file_name, 'file')
    x = load(file_name, 'R', 'all_D', 'all_B', 'obj');
    if isfield(x, 'R') && ...
            isfield(x, 'all_D') && ...
            isfield(x, 'all_B') && ...
            isfield(x, 'obj')
       R = x.R;
       all_D = x.all_D;
       all_B = x.all_B;
       obj = x.obj;
        is_run = 0;
    end
end
if is_run
    dic_size2 = dic_size;
    tmp_method = mod(method, 10);
    if (tmp_method == 2 || tmp_method == 5) && s0 ~= -1
        dic_size2 = dic_size * s0;
    end   
    'start training...'
    tic;
    [R, all_D, all_B, obj]  = sckmeans(Xtraining, ...
        dic_dim, dic_size2, s0, method, max_iter, ...
        init_all_D, init_R, init_B, is_opt_R);
    time_cost_training = toc;
    save(file_name, ...
        'R', 'all_D', 'all_B', 'obj', 's0', 'method', ...
        'time_cost_training', '-v7.3');
end
% encode the data base
x = load(file_name, 'mat_compact_B');
if isfield(x, 'mat_compact_B') && ~isempty(x.mat_compact_B)
    mat_compact_B = x.mat_compact_B;
else
    tic;
    if numel(Xbase) == 1 && Xbase == 1
        mat_compact_B = UpdateRepresentationCompactTiny80M(...
            1001, 79302017, R, all_D, s0, method);
    elseif numel(Xbase) == 1 && Xbase == 2
        mat_compact_B = UpdateRepresentationCompactSIFT1B(...
            1, 10^9, R, all_D, s0, method);
        
    else
        mat_compact_B = UpdateRepresentationCompact(...
            Xbase, R, all_D, s0, method);
        
    end
    time_encode_b = toc;
    save(file_name, ...
        'mat_compact_B', '-v7.3', '-append');
    save(file_name, ...
        'time_encode_b', '-v7.3', '-append');
end
NBase = size(mat_compact_B, 2);
%% encode the query set
x = load(file_name, 'cell_code_Q');
if isfield(x, 'cell_code_Q')
    cell_code_Q = x.cell_code_Q;
else
    cell_code_Q = UpdateRepresentation(...
        Xtest, R, all_D, [], s0, method);
    save(file_name, ...
        'cell_code_Q', '-append', '-v7.3');
end

% recover the query set
x = load(file_name, 'RecoveredXtest');
if isfield(x, 'RecoveredXtest')
    RecoveredXtest = x.RecoveredXtest;
else
    RecoveredXtest = Recovered2(R, all_D, cell_code_Q);
    save(file_name, ...
        'RecoveredXtest', '-append', '-v7.3');
end

% retrieval and save
x = load(file_name, 'retrieval_result');
if ~isfield(x, 'retrieval_result')
    tic;
    retrieval_result = mexRetrievalSave(mat_compact_B, ...
        all_D, ...
        R' * Xtest, ...
        10000);
    time_retrieval = toc;
    
    save(file_name, ...
        'retrieval_result', 'time_retrieval', '-append', '-v7.3');
else
    retrieval_result = x.retrieval_result;
end

% recall
x = load(file_name, 'rec');
if ~isfield(x, 'rec')
    tic;
    rec = mexResultRecall(retrieval_result, ...
        ground_truth_file, ...
        int32(1), ...
        NBase);
   time_cost_recall = toc;
   save(file_name, ...
        'rec', 'time_cost_recall', '-append', '-v7.3');
end


%
x = load(file_name, 'retrieval_result_sym');
if ~isfield(x, 'retrieval_result_sym')
    tic;
    retrieval_result_sym = ...
        mexRetrievalSave(mat_compact_B, ...
        all_D, ...
        R' * RecoveredXtest, ...
        10000);
    time_retrieval_sym = toc;
    save(file_name, ...
        'retrieval_result_sym', ...
        'time_retrieval_sym', ...
        '-append', '-v7.3');
end

%%
x = load(file_name, 'rec_sym');
if ~isfield(x, 'rec_sym')
    tic;
    rec_sym  = mexResultRecall(retrieval_result_sym, ...
        ground_truth_file, ...
        int32(1), ...
        NBase);
    time_cost_recall_sym = toc;
    
    save(file_name, ...
        'rec_sym', 'time_cost_recall_sym', ...
        '-append', '-v7.3');
end

x = load(file_name, 'dist_ratio');
if ~isfield(x, 'dist_ratio')
    x = load(file_name, 'retrieval_result');
    retrieval_result = x.retrieval_result;
    tic;
    dist_ratio = mexResultDistanceRatio(...
        retrieval_result, ...
        Xtest, ...
        Xbase, ...
        ground_truth_file);
    time_dist_ratio = toc;
    save(file_name, ...
        'dist_ratio', 'time_dist_ratio', ...
        '-append', '-v7.3');
end

x = load(file_name, 'dist_ratio_sym');
if ~isfield(x, 'dist_ratio_sym')
    x = load(file_name, 'retrieval_result_sym');
    retrieval_result_sym = x.retrieval_result_sym;
    dist_ratio_sym = mexResultDistanceRatio(...
        retrieval_result_sym, ...
        Xtest, ...
        Xbase, ...
        ground_truth_file);
    save(file_name, ...
        'dist_ratio_sym', 'time_dist_ratio', ...
        '-append', '-v7.3');
end



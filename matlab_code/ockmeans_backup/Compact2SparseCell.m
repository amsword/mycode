function [cell_sparse_B] = Compact2SparseCell(...
    mat_compact_B, all_D, s0, method)


% Z = R' * mat_compact_B;

% dic_dim = size(all_D{1}, 1);
cell_sparse_B = cell(numel(all_D), 1);

num_words_each = s0;
if s0 == -1
    num_words_each = 1;
end

% num_point = size(mat_compact_B, 2);

for i = 1 : numel(all_D)
    subD = all_D{i};

    idx_dim_start = (i - 1) * num_words_each + 1;
    idx_dim_end = i * num_words_each;

    new_compact_SubB = mat_compact_B(idx_dim_start : idx_dim_end, :);
    
    new_full_SubB = Compact2Binary(new_compact_SubB, size(subD, 2), ...
        mod(method, 10));
    
    
    cell_sparse_B{i} = sparse(new_full_SubB);
end
function all_details = collect_detailed_time_cost(file_name, is_excluded_first)
fp = fopen(file_name, 'r');
k = 1;
clear all_details;

while ~feof(fp)
    line = fgetl(fp);
    x = strsplit(line);
    all_details(k).name = x{1};
    count = str2double(x{2});
    all_details(k).all_time = zeros(count, 1);
    line = fgetl(fp);
    all_details(k).all_time = sscanf(line, '%f');
    assert(count == numel(all_details(k).all_time));
    
    if is_excluded_first && count > 1
        all_details(k).all_time = all_details(k).all_time(2 : end);
    end
    all_details(k).time_mean = mean(all_details(k).all_time);
    all_details(k).time_std = std(all_details(k).all_time);

    k = k + 1;
end

function y = read_cuda_fft(file_name)

fp = fopen(file_name);

num_images = fread(fp, 1, 'int32');
num_channels = fread(fp, 1, 'int32');
num_height = fread(fp, 1, 'int32');
num_width = fread(fp, 1, 'int32');

num_count = num_images * num_channels * num_height * num_width;

real_part = fread(fp, num_count, 'float');
image_part = fread(fp, num_count, 'float');
data = real_part + i * image_part;

y = reshape(data, num_width * num_height, num_channels, num_images);

fclose(fp);
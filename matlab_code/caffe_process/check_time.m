log_dir = 'C:\Users\t0908482\Desktop\mine\working\Caffe\';
save_dir = 'D:\OneDrive\Documents\Research\OnGoing\active\cnn_time_cost\';

clear all_logs;
% all_logs(1).input = [log_dir 'train_net.bin.TT2.jianfeng.log.INFO.20140717-165845.15041-jpeg'];
% all_logs(1).output = 'jpeg';

% all_logs(2).input = [log_dir 'train_net.bin.TT2.jianfeng.log.INFO.20140717-165831.15009-raw'];
% all_logs(2).output = 'raw';

all_logs(1).input = [log_dir 'TimeTracker-20140806-144830.14863'];
all_logs(1).output = '1';
all_logs(2).input = [log_dir 'TimeTracker-20140806-143945.11907'];
all_logs(2).output = '2';
for i = 1 : numel(all_logs)
    file_name = all_logs(i).input;
    tex_name = all_logs(i).output;
    for is_exclude_first = [1]
        save_file = [save_dir tex_name num2str(is_exclude_first) '.tex'];
        
%         all_result = report_time_cost(file_name);
        all_result = collect_detailed_time_cost(file_name, is_exclude_first);
        
        r_result = rearrange_result(all_result);
        write_time_cost(save_file, r_result);
    end
end
%%
clc;
fprintf('forward\n');
for i = 1 : 5
    x = find_exact_time(['forward_CaffeNet_conv' num2str(i)], all_result);
    total = mean(x.all_time) * 10^3;
    x = find_exact_time(['conv' num2str(i) '_forward_iterate_all_images'], all_result);
    one_iteration = mean(x.all_time) * 10^3;
    x = find_exact_time(['conv' num2str(i) '_forward_create_matrix'], all_result);
    create_matrix = mean(x.all_time) * 128 * 10^3;
    create_matrix_std = std(x.all_time) * 128 * 10^3;
    x = find_exact_time(['conv' num2str(i) '_forward_multiply'], all_result);
    multiply = mean(x.all_time) * 128 * 10^3;
    multiply_std = std(x.all_time) * 128 * 10^3;
    x = find_exact_time(['conv' num2str(i) '_forward_add_bias'], all_result);
    add_bias = mean(x.all_time) * 128 * 10^3;
    add_bias_std = std(x.all_time) * 128 * 10^3;
    pat = ['conv' num2str(i) ', total: %.2f; create_matrix: %.2f(%.1f%%); ' ...
        'matrix_multiply: %.2f(%.1f%%); add_bias: %.2f(%.1f%%); ' ...
        '\n'];
    fprintf(pat, total, create_matrix, create_matrix / total * 100, ...
        multiply, multiply / total * 100, ...
        add_bias, add_bias / total * 100);
end

%
fprintf('backward\n');
for i = 1 : 5
    x = find_exact_time(['back_CaffeNet_conv' num2str(i)], all_result);
    total = mean(x.all_time) * 10^3;
    x = find_exact_time(['conv' num2str(i) '_backward_create_matrix'], all_result);
    create_matrix = mean(x.all_time) * 128* 10^3;
        x = find_exact_time(['conv' num2str(i) '_backward_bias_gradient'], all_result);
        
    bias_gradient = mean(x.all_time) * 10^3;
%     creatstd(conv1_for_create.all_time)
    x = find_exact_time(['conv' num2str(i) '_backward_matrix_multiply'], all_result);
    multiply = mean(x.all_time) * 128* 10^3;
    x = find_exact_time(['conv' num2str(i) '_backward_derive_propagation'], all_result);
    propagation = mean(x.all_time) * 128* 10^3;
    % std(conv1_matrix_multiply.all_time)
    pat = ['conv' num2str(i) ', total: %.2f; create_matrix: %.2f(%.1f%%); ' ...
        'matrix_multiply: %.2f(%.1f%%); bias_gradient: %.2f(%.1f%%); ' ...
        'back_propagation: %.2f(%.1f%%)\n'];
    fprintf(pat, total, create_matrix, create_matrix / total * 100, ...
        multiply, multiply / total * 100, ...
        bias_gradient, bias_gradient / total * 100, ...
        propagation, propagation / total * 100);
end


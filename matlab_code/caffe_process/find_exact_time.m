function y = find_exact_time(name, all_result)

num_result = numel(all_result);

y = [];
for i = 1 : num_result
	one_result = all_result(i);
    
    if strcmp(one_result.name, name)
        y = one_result;
    end
end

assert(~isempty(y));

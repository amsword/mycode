function y = read_blob_data(file_name)

fp = fopen(file_name, 'rb');

num_images = fread(fp, 1, 'int32');
num_channels = fread(fp, 1, 'int32');
num_height = fread(fp, 1, 'int32');
num_width = fread(fp, 1, 'int32');

num_count = num_images * num_channels * num_height * num_width;

data = fread(fp, num_count, 'float');

data = reshape(data, num_width, num_height, num_channels, num_images);
y.data = zeros(num_height, num_width, num_channels, num_images);
for i = 1 : num_images
    for j = 1 : num_channels
        z = data(:, :, j, i);
        y.data(:, :, j, i) = z';
    end
end
% assert(feof(fp));
fclose(fp);
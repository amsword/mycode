function curr_weight_resized =  resize_weight(curr_weight, width, height)
curr_weight_resized = zeros(height, width);
fliped_weight = fliplr(flipud(curr_weight));

curr_weight_resized(1, 1) = fliped_weight(end, end);
curr_weight_resized(1, end - size(fliped_weight, 2) + 2 : end) = ...
    fliped_weight(end, 1 : end - 1);
curr_weight_resized(end - size(fliped_weight, 1) + 2 : end, 1) = ...
    fliped_weight(1 : end - 1, end);
curr_weight_resized(end - size(fliped_weight, 1) + 2 : end, ...
    end - size(fliped_weight, 2) + 2: end) = fliped_weight(1 : end - 1, 1 : end - 1);

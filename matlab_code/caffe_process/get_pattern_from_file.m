function acc = get_pattern_from_file(file_name, pattern, read_pattern1, read_pattern2)
% if read_pattern2 exists, we remove read_pattern1 and then find the 
% string with read_pattern1.

fp = fopen(file_name, 'r');

is_rm = false;
if nargin == 4
    is_rm = true;
end

idx_to_be = 1;
while ~feof(fp)
    line = fgetl(fp);
    matched = regexp(line, pattern, 'match');
    if isempty(matched)
        continue;
    end
    if numel(matched) ~= 1
        error('confused');
    end
    if is_rm
        matched{1} = strrep(matched{1}, read_pattern1, '');
        acc{idx_to_be} = sscanf(matched{1}, read_pattern2);
    else
        acc{idx_to_be} =  sscanf(matched{1}, read_pattern1);
    end
    
    idx_to_be = idx_to_be + 1;
end

fclose(fp);
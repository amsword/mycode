collect_acc_from_log;

check_time;


%% time of active
file_name = 'active_set.txt';
all = get_pattern_from_file(file_name, ...
    'write time to file .*$', 'write time to file /home/jianfeng/glogs/', '%s');
file_name_time_tracker = all{1};

all_result = collect_detailed_time_cost(file_name_time_tracker, false);
active_time = find_exact_time('one_training_iteration', all_result);
% time of base
clc;
file_name = 'base.txt';
all = get_pattern_from_file(file_name, ...
    'write time to file .*$', 'write time to file /home/jianfeng/glogs/', '%s');
file_name_time_tracker = all{1};

all_result = collect_detailed_time_cost(file_name_time_tracker, false);
base_time = find_exact_time('one_training_iteration', all_result);

% acc of active
file_name = 'active_set.txt';
all = get_pattern_from_file(file_name, ...
    'Test score #0: .*$', 'Test score #0: ', '%f'); 
all = cell2mat(all);
active_acc = all(2 : end);

% acc of base
file_name = 'base.txt';
all = get_pattern_from_file(file_name, ...
    'Test score #0: .*$', 'Test score #0: ', '%f'); 
all = cell2mat(all);
base_acc = all(2 : end);
%% plot x-epoch
save_dir = 'D:\OneDrive\Documents\Research\OnGoing\active\cnn_hard_sample_training\images';
line_width = 1.4;
font_size = 16;
figure;
plot(active_acc, 'r-o', 'linewidth', line_width);
hold on;
plot(base_acc, 'b-*', 'linewidth', line_width);
legend('Proposed', 'Baseline');
grid on;
set(gca, 'FontSize', font_size);
xlabel('epochs');
ylabel('accuracy');
saveas(gca, [save_dir '\epoch.eps'], 'psc2');
% plot x-time
active_cum_sum = cumsum(active_time.all_time);
base_cum_sum = cumsum(base_time.all_time);
figure;
plot(active_cum_sum(500 : 500 : end), active_acc, 'r-o', 'linewidth', line_width);
hold on;
plot(base_cum_sum(500 : 500 : end), base_acc, 'b-*', 'linewidth', line_width);
grid on;
legend('Proposed', 'Baseline');
set(gca, 'FontSize', font_size);
xlabel('time (sec)');
ylabel('accuracy');
saveas(gca, [save_dir '\time.eps'], 'psc2');
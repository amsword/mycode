function collect_file_name(dir_name, save_file_name);
% dir_name = 'D:\OneDrive\BitBucketCode\home\jianfeng\caffe - Copy\build\MSVC\';
% save_file_name = 'a.txt';
sub_dir_info = dir(dir_name);
num_sub_dir = numel(sub_dir_info);

fp = fopen(save_file_name, 'w');
label = 0;
for i = 1 : num_sub_dir
    fprintf('%s\n', [num2str(i) '/' num2str(num_sub_dir)]);
    sub_dir = sub_dir_info(i);
    if sub_dir.isdir == 0
        continue;
    end
    if sub_dir.isdir == 1
        if strcmp(sub_dir.name, '.') || ...
                strcmp(sub_dir.name, '..')
            continue;
        end
    end
    sub_dir_full_name = [dir_name sub_dir.name '/'];
    all_sub_files = dir(sub_dir_full_name);
    for j = 1 : numel(all_sub_files)
        sub_file = all_sub_files(j);
        if sub_file.isdir == 1
            continue;
        end
        fprintf(fp, '%s %d\n', [sub_dir.name '/' sub_file.name], label);
    end
    label = label + 1;
end
fclose(fp);
function r_result = rearrange_result(all_result)
k = 1;
layer_names{k} = 'data'; k = k + 1;
layer_names{k} = 'conv1'; k = k + 1;
layer_names{k} = 'relu1'; k = k + 1;
layer_names{k} = 'pool1'; k = k + 1;
layer_names{k} = 'norm1'; k = k + 1;
layer_names{k} = 'conv2'; k = k + 1;
layer_names{k} = 'relu2'; k = k + 1;
layer_names{k} = 'pool2'; k = k + 1;
layer_names{k} = 'norm2'; k = k + 1;
layer_names{k} = 'conv3'; k = k + 1;
layer_names{k} = 'relu3'; k = k + 1;
layer_names{k} = 'conv4'; k = k + 1;
layer_names{k} = 'relu4'; k = k + 1;
layer_names{k} = 'conv5'; k = k + 1;
layer_names{k} = 'relu5'; k = k + 1;
layer_names{k} = 'pool5'; k = k + 1;
layer_names{k} = 'fc6'; k = k + 1;
layer_names{k} = 'relu6'; k = k + 1;
layer_names{k} = 'drop6'; k = k + 1;
layer_names{k} = 'fc7'; k = k + 1;
layer_names{k} = 'relu7'; k = k + 1;
layer_names{k} = 'drop7'; k = k + 1;
layer_names{k} = 'fc8'; k = k + 1;
layer_names{k} = 'loss'; k = k + 1;
layer_names{k} = 'cross_relu'; k = k + 1;

summed_time_cost = 0;
clear table_result
for k = 1 : numel(layer_names)
    name = layer_names{k};
    one_result = find_time_cost(name, all_result);
    one_result.name = name;
    if isfield(one_result, 'forward')
        summed_time_cost = summed_time_cost + one_result.forward.time_mean;
    end
    if isfield(one_result, 'backward')
        summed_time_cost = summed_time_cost + one_result.backward.time_mean;
    end
    table_result{k} = one_result; 
end
r_result.table_result = table_result;
r_result.summed_time_cost = summed_time_cost;
r_result.pre_fetch = find_exact_time('Data_prefetch', all_result);
r_result.read_info = find_exact_time('read_data', all_result);
r_result.decode_info = find_exact_time('decode_datum', all_result);
r_result.next_read_info = find_exact_time('go_to_next_iter', all_result);
r_result.join_thread = find_exact_time('join_thread', all_result);
r_result.copy_data = find_exact_time('data_layer_copy', all_result);
r_result.one_training_iteration = find_exact_time('one_training_iteration', all_result);
r_result.crop_and_norm = find_exact_time('crop_data_and_normalization', all_result);
r_result.data_layer_copy = find_exact_time('data_layer_copy', all_result);
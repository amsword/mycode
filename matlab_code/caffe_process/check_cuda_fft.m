log_dir = 'C:\Users\t0908482\Desktop\mine\working\Caffe\';

file_name_cuda_fft = [log_dir 'fft'];
file_name_inversed = [log_dir 'reversed'];
file_name_cuda_fft_again = [log_dir 'forwarded_again'];

input_blob = read_blob_data([log_dir  'input']);
weight_blob = read_blob_data([log_dir 'weight']);
output_blob = read_blob_data([log_dir 'output']);

output_blob2 = read_blob_data([log_dir 'output2']);
resized_weight = read_blob_data([log_dir 'resized_weight']);
cuda_fft_input = read_cuda_fft([log_dir 'fft_input']);
cuda_fft_weight = read_cuda_fft([log_dir 'fft_weight']);
cuda_fft_output = read_cuda_fft([log_dir 'fft_output']);
% cuda_fft = read_cuda_fft(file_name_cuda_fft);
% cuda_reversed = read_blob_data(file_name_inversed);
% cuda_fft_again = read_cuda_fft(file_name_cuda_fft_again);
%
%%
% input_blob(:) = 1;
% weight_blob(:) = 1;
x = 0;

for idx_image = 1 : size(input_blob, 4)
    one_input = input_blob(:, :, :, idx_image);
    for idx_out = 1 : size(weight_blob, 4)
        s = [];
        r = [];
        matlab_fft = zeros(256, 256);
        for idx_in_channel = 1 : size(one_input, 3)
%         for idx_in_channel = 1 : 1
            curr_image = one_input(:, :, idx_in_channel);
            curr_weight = weight_blob(:, :, idx_in_channel, idx_out);
            curr_image_fft = fft2(curr_image);
            
            curr_weight = weight_blob(:, :, idx_out, idx_image);
            curr_weight_resized = resize_weight(curr_weight, 256, 256);
            curr_weight_resized_fft = fft2(curr_weight_resized);
            
            matlab_fft = matlab_fft + curr_image_fft .* curr_weight_resized_fft;
            if isempty(s)
                s = conv2(curr_image, fliplr(flipud(curr_weight)), 'valid');
            else
                s = s + conv2(curr_image, fliplr(flipud(curr_weight)), 'valid');
            end
        end
        t = matlab_fft(:, 1 : 129);
        t = t .';
        norm(cuda_fft_output(:, idx_out, idx_image) * 256 * 256 - t(:))
%         norm(norm((s - output_blob(:, :, idx_out, idx_image)))) / norm(norm(s))
        error('df');
    end
end
%% fft of the input image
for i = 1 : size(input_blob, 4)
    for j = 1 : size(input_blob, 3)
        curr_image = input_blob(:, :, j, i);
        fft_image = fft2(curr_image);
        t = fft_image(:, 1 : 129);
        t = t .';
        norm(t(:) - cuda_fft_input(:, j, i)) / norm(t(:))
    end
end

%% fft of the resized kernel
for i = 1 : size(weight_blob, 4)
    for j = 1 : size(weight_blob, 3)
        curr_weight = weight_blob(:, :, j, i);
        curr_weight_resized = resize_weight(curr_weight, 256, 256);
        curr_weight_resized_fft = fft2(curr_weight_resized);
        t = curr_weight_resized_fft(:, 1 : 129);
        t = t .';
        norm(t(:) - cuda_fft_weight(:, j, i))
    end
end

%% fft of the output







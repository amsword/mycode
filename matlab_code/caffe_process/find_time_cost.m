function y = find_time_cost(name, all_result)

forward_name = ['forward_CaffeNet_' name];
backward_name = ['back_CaffeNet_' name];
num_result = numel(all_result);

y = [];
for i = 1 : num_result
    one_result = all_result(i);
    if strcmp(forward_name, one_result.name)
        y.forward = one_result;
    elseif strcmp(backward_name, one_result.name)
        y.backward = one_result;
    end
end
function all_result = report_time_cost(file_name)

fp = fopen(file_name, 'r');

pat = 'TimeTracker.cpp:50]';
k = 1;
clear all_result;
while ~feof(fp)
    line = fgetl(fp);
    start = strfind(line, pat);
    if isempty(start)
        continue;
    end
    sub_line = line(start : end);
    start_right = strfind(sub_line, ']');
    start_colon = strfind(sub_line, ': count');
    name = sub_line(start_right + 2 : start_colon - 1);
    start_mean = strfind(sub_line, 'mean: ');
    start_operator = strfind(sub_line, '+-');
    start_operator = start_operator(1);
    str_time_mean = sub_line(start_mean + 6 : start_operator - 2);
    time_mean = str2double(str_time_mean);
    
    str_time_std = sub_line(start_operator + 3 : end);
    time_std = str2double(str_time_std);
    all_result(k).name = name;
    all_result(k).time_mean = time_mean;
    all_result(k).time_std = time_std;
    k = k + 1;
end

fclose(fp);
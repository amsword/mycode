collect_acc_from_log;

check_time;


%% time of active
file_name = 'active_set.txt';
all = get_pattern_from_file(file_name, ...
    'write time to file .*$', 'write time to file /home/jianfeng/glogs/', '%s');
file_name_time_tracker = all{1};

all_result = collect_detailed_time_cost(file_name_time_tracker, false);
active_one_iter = find_exact_time('one_training_iteration', all_result);
%% time of base
clc;
file_name = 'base.txt';
all = get_pattern_from_file(file_name, ...
    'write time to file .*$', 'write time to file /home/jianfeng/glogs/', '%s');
file_name_time_tracker = all{1};

all_result = collect_detailed_time_cost(file_name_time_tracker, false);
origin_one_iter = find_exact_time('one_training_iteration', all_result);

%% acc of active
file_name = 'active_set.txt';
all = get_pattern_from_file(file_name, ...
    'Test score #0: .*$', 'Test score #0: ', '%f'); 
all = cell2mat(all);
all = all(2 : end);

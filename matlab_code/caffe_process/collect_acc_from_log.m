function acc = collect_acc_from_log(file_name)

fp = fopen(file_name, 'r');

clear pattern;
pattern{1} = 'Test score #0: .*';
clear acc;
idx_to_be = 1;
while ~feof(fp)
    line = fgetl(fp);
    
    idx_start = regexp(line, pattern{1}, 'start');
    if isempty(idx_start)
        continue;
    end
    matched = line(idx_start : end);
    num = sscanf(matched, 'Test score #0: %f');
    acc(idx_to_be) = num;
    idx_to_be = idx_to_be + 1;
    
end

fclose(fp);
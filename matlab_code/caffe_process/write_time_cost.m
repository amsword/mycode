function write_time_cost(save_file, r_result)

batch_size = 128;

total_time_cost = r_result.summed_time_cost;
table_result = r_result.table_result;
one_iteration = r_result.one_training_iteration;

idx_slash = strfind(save_file, '\');
sub_file_name = save_file(idx_slash(end) + 1 : end);
sub_file_name = sub_file_name(1 : end - 4); 
upper_file_name = upper(sub_file_name);

fp = fopen(save_file, 'w');

line = '\begin{table}[t]';
fprintf(fp, '%s\n', line);
line = '\centering';
fprintf(fp, '%s\n', line);
fprintf(fp, ['\\caption{' upper_file_name(1 : end - 1) '. Total time of one iteration: %.1f $\\pm$ %.1f; ' ...
    'Summmed time of the following: %.1f; ' ...
    'Prefetch time: %.1f $\\pm$ %.1f; ' ...
    'Decode time: %.1f $\\pm$ %.1f; ' ...
    'Read data: %.1f $\\pm$ %.1f; ' ...
    'Crop and normalize: %.1f $\\pm$ %.1f; ' ...
    'Data layer copy: %.1f $\\pm$ %.1f; ' ...
    '}' ], ...
    one_iteration.time_mean * 10^3, ...
    one_iteration.time_std * 10^3, ...
    r_result.summed_time_cost * 10^3, ...
    r_result.pre_fetch.time_mean * 10^3, ...
    r_result.pre_fetch.time_std * 10^3, ...
    r_result.decode_info.time_mean * 10^3 * batch_size, ...
    r_result.decode_info.time_std * 10^3 * batch_size, ...
    mean(r_result.read_info.all_time + r_result.next_read_info.all_time) * 10^3 * batch_size, ...
    std(r_result.read_info.all_time + r_result.next_read_info.all_time) * 10^3 * batch_size, ...
    r_result.crop_and_norm.time_mean * 10^3 * batch_size, ...
    r_result.crop_and_norm.time_std * 10^3 * batch_size, ...
    r_result.data_layer_copy.time_mean * 10^3, ...
    r_result.data_layer_copy.time_std * 10^3); 
line = ['\label{' sub_file_name '}'];
fprintf(fp, '%s\n', line);
line = '\begin{tabular}{ccccccc}';
fprintf(fp, '%s\n', line);
line = '\toprule';
fprintf(fp, '%s\n', line);
line = '\multirow{2}{*}{Layer name} & \multicolumn{2}{c}{Forward} & \multicolumn{2}{c}{Backward} & \multicolumn{2}{c}{Total} \\';
fprintf(fp, '%s\n', line);
line = '\cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7}';
fprintf(fp, '%s\n', line);
line = '& Time & Proportion & Time & Proportion & Time & Proportion \\';
fprintf(fp, '%s\n', line);
line = '\midrule';
fprintf(fp, '%s\n', line);

for i = 1 : numel(table_result)
    one_result = table_result{i};
    line = one_result.name;
    fprintf(fp, '%s & ', line);
    fm = 0; fs = 0; bm = 0; bs = 0;
    if isfield(one_result, 'forward')
        fm = one_result.forward.time_mean * 10^3;
        fs = one_result.forward.time_std * 10^3;
    end
    if isfield(one_result, 'backward');
        bm = one_result.backward.time_mean * 10^3;
        bs = one_result.backward.time_std * 10^3;
    end
    fprintf(fp, '%.1f $\\pm$ %.1f & %.1f & %.1f $\\pm$ %.1f & %.1f & %.1f  & %.1f \\\\ \n', ...
        fm, fs, fm / total_time_cost / 10^3 * 100, ...
        bm, bs, bm / total_time_cost / 10^3 * 100, ...
        fm + bm, (fm + bm) / total_time_cost / 10^3 * 100);
end
line = '\bottomrule';
fprintf(fp, '%s \n', line);
line = '\end{tabular}';
fprintf(fp, '%s \n', line);
line = '\end{table}';
fprintf(fp, '%s \n', line);

fclose(fp);

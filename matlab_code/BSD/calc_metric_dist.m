function dist = calc_metric_dist(H1, H2, metric_info)
if (metric_info.type == 0)
    byte_2 = compactbit(H2);
    byte_1 = compactbit(H1);
    dist = jf_hammingDist2(byte_1, byte_2);
    dist = double(dist);
elseif (metric_info.type == 1)
    m = size(H1, 1);
    dist = 0.5 * (m - (2 * H1 - 1)' * metric_info.A * (2 * H2 - 1));
elseif (metric_info.type == 2)
    a1 = diag(H1' * metric_info.A * H1);
    a2 = diag(H2' * metric_info.A * H2);
    dist = a1 * ones(1, size(H2, 2)) + ...
        ones(size(H1, 2), 1) * a2' - ...
        H1' * (metric_info.A + metric_info.A') * H2;
else
    error('no support');
end
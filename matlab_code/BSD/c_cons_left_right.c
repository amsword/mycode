#include <mex.h>
#include "h_aux.h"

#define	MAT_HASH_CODE   	prhs[0]  // Uint8 vector of size n x 1
#define MAT_IS    prhs[1]  // Uint8 matrix of size n x m
#define MAT_JS     prhs[2]
#define MAT_V       prhs[3]
#define MAT_TYPE prhs[4]

#define MAT_LEFT plhs[0]
#define MAT_RIGHT plhs[1]


void parse_matrix_double(const mxArray* prhs,
				  int * m, int* n, double** ptr)
{
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (double*)mxGetPr(prhs);
}

void parse_matrix_int32(const mxArray* prhs,
				  int * m, int* n, int** ptr)
{
	*m = mxGetM(prhs);
	*n = mxGetN(prhs);
	*ptr = (int*)mxGetData(prhs);
}

void verify_type(int nrhs, const mxArray*prhs[])
{
    if (nrhs != 5)
    {
        mexErrMsgTxt("5 inputs are required");
    }
    
    if (!mxIsInt32(MAT_HASH_CODE))
    {
        mexErrMsgTxt("hash code should be int32");
    }
    
    if (!mxIsInt32(MAT_IS) || 
            !mxIsInt32(MAT_JS))
    {
        mexErrMsgTxt("is, js should be int32");
    }
    
    if (!mxIsDouble(MAT_V))
    {
        mexErrMsgTxt("v should be double");
    }
    
    if (!mxIsInt32(MAT_TYPE))
    {
        mexErrMsgTxt("type should be int32");
    }
    
}

void parse_matrix_intput( int nrhs, const mxArray*prhs[], 
        int** pp_hash_code, int** pp_is, int** pp_js, double** pp_v,
        int* p_code_length, int* p_num_point, int* p_length_v,
        int* p_type)
{
    int tmp1;
    int tmp2;
    int** pp_type;
    int* p2_type;
    
    verify_type(nrhs, prhs);
    
     parse_matrix_int32(MAT_HASH_CODE, p_code_length, p_num_point,
            pp_hash_code);
    
    parse_matrix_int32(MAT_IS, &tmp1, &tmp2, pp_is);
    *p_length_v = tmp1 * tmp2;
    
    parse_matrix_int32(MAT_JS, &tmp1, &tmp2, pp_js);
    if (*p_length_v  != tmp1 * tmp2)
    {
        mexErrMsgTxt("is, and js, not equal length");
    }
    
    parse_matrix_double(MAT_V, &tmp1, &tmp2, pp_v);
    if (*p_length_v  != tmp1 * tmp2)
    {
        mexErrMsgTxt("is, and js, v not equal length");
    }
    
    pp_type = &p2_type;
    parse_matrix_int32(MAT_TYPE, &tmp1, &tmp2, pp_type);
    if (tmp1 != 1 || tmp2 != 1)
    {
        mexErrMsgTxt("type should be 1 x 1");
    }
    
    *p_type = *p2_type;
}



void mat_add(double* p1, double* p2, int n, double* pout)
{
    int i;
    
    for (i = 0; i < n; i++)
    {
        (*pout++) = (*p1++) + (*p2++);
    }
}

void mat_scalemultiple_add(double* p1, double scale, double*  p2, int n, double* pout)
{
    int i;
    
    for (i = 0; i < n; i++)
    {
        (*pout++) = (*p1++) * scale + (*p2++);
    }
}

void cons_left_right_type1(double* p_left, double* p_right, 
        int* p_hash_code, int* p_is, int* p_js, double* p_v, 
        int code_length, int length_v)
{
    int i;
    int* p_code1;
    int* p_code2;
    
    for (i = 0; i < length_v; i++)
    {
        p_code1 = p_hash_code + code_length * (p_is[i] - 1);
        p_code2 = p_hash_code + code_length * (p_js[i] - 1);
        
    }
}

void cons_left_right_type2(double* p_left, double* p_right, 
        int* p_hash_code, int* p_is, int* p_js, double* p_v, 
        int code_length, int length_v)
{
    int i;
    int j;
    int* p_code1;
    int* p_code2;

    double* p_diff;
    double* p_vec_m2;
    double* p_vec_m4;
    
    int code_length2 = code_length * code_length;
    int code_length4 = code_length2 * code_length2;
    
    mexPrintf("allocating %d double\n", code_length);
    p_diff = (double*)malloc(sizeof(double) * code_length);
    
    mexPrintf("allocating %d double\n", code_length2);
    p_vec_m2 = (double*)malloc(sizeof(double) * code_length2);
    
    mexPrintf("allocating %d double\n", code_length4);
    p_vec_m4 = (double*)malloc(sizeof(double) * code_length4);
    
    for (i = 0; i < length_v; i++)
    {
        p_code1 = p_hash_code + code_length * (p_is[i] - 1);
        p_code2 = p_hash_code + code_length * (p_js[i] - 1);
        
        for (j = 0; j < code_length; j++)
        {
            p_diff[j] = p_code1[j] - p_code2[j];
        }
        mat_multiple_double(p_diff, p_diff, code_length, p_vec_m2);
        mat_multiple_double(p_vec_m2, p_vec_m2, code_length2, p_vec_m4);
        mat_add(p_left, p_vec_m4, code_length4, p_left);
        mat_scalemultiple_add(p_vec_m2, p_v[i], p_right, code_length2, p_right);       
    }
    
    free(p_diff);
    free(p_vec_m2);
    free(p_vec_m4);
}


// [left right] = c_cons_left_right(hash_code, is, js, v, type)
// hash_code: int32
// is: int32
// js: int32
//  v: double
// type: int32 1/2
void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[])
{
    int* p_hash_code;
    int* p_is;
    int* p_js;
    double* p_v;
    int type;
  
    int length_v;
    int code_length;
    int num_point;
    
    double* p_left;
    double* p_right;
    int rows_left;
           
    parse_matrix_intput( nrhs, prhs, 
        &p_hash_code, &p_is, &p_js, &p_v,
        &code_length, &num_point, &length_v, &type);
    
    rows_left = code_length * code_length;
    
    MAT_LEFT = mxCreateDoubleMatrix(rows_left, rows_left, mxREAL);
    p_left = mxGetPr(MAT_LEFT);
    
    MAT_RIGHT = mxCreateDoubleMatrix(rows_left, 1, mxREAL);
    p_right = mxGetPr(MAT_RIGHT);
    
    memset(p_left, sizeof(double) * rows_left * rows_left, 0);
    memset(p_right, sizeof(double) * rows_left, 0);
    
    if (type == 1)
    {
        mexErrMsgTxt("type is not supported now");
//         cons_left_right_type1();
    }
    else if(type == 2)
    {
        cons_left_right_type2(p_left, p_right,
                p_hash_code, p_is, p_js, p_v,
                code_length, length_v);
    }
    else
    {
        mexErrMsgTxt("type is not correct");
    }   
}
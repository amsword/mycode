function eval = jf_test_bre(param, Xtest, Xbase, StestBase, topK, trueRank_fileseed)
%% form hash keys over queries
% Perhaps: H can be calculated from W
eval = jf_eval_hash3(size(param.W, 2), 'bre', param, Xtest, Xbase, StestBase, topK, trueRank_fileseed);
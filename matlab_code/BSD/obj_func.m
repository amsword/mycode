function [f, grad] = obj_func(W, K, hash_inds, Dist, inds, beta)
n = size(K, 1);
numbits = size(W, 2);

KW = zeros(size(K, 1), numbits);
for i = 1:numbits
    KW(:,i) = K(:,hash_inds(:,i))*W(:,i);
end

H = KW > 0;
len = sum(H,2);
D = (1/numbits)*(len*ones(1,n) + ones(n,1)*len' - 2*H*H');

Ddiff = D - Dist;
f = .5*sum(Ddiff(inds).^2);

if (nargout == 2)
    [is js] = ind2sub([n n], inds);
    v1 = Ddiff(inds);
    
    grad = zeros(size(W));
    for t = 1 : numbits
        v2 = KW(is, t) - KW(js, t);
        v2 = sign(v2);
        left_coef = beta * sigmf(KW(is, t), [beta 0]) .* (1 - sigmf(KW(is, t), [beta 0]));
        left = bsxfun(@times, K(is, hash_inds(:, t)), left_coef);
        
        right_coef = beta * sigmf(KW(js, t), [beta 0]) .* (1 - sigmf(KW(js, t), [beta 0]));
        right = bsxfun(@times, K(js, hash_inds(:, t)), right_coef);
        
        left = bsxfun(@times, left - right, v2 .* v1);
        grad(:, t) = sum(left, 1)';
    end
end
function metric_info = learn_metric(hash_code, dist, type)
% dist: ground truth dist
% Xtraining: the data
% hash_func: handle of the hash bit

[is js v] = find(dist + 1);
v = v - 1;

n = length(v);

m = size(hash_code, 1);

if (type == 2)
%     diff_code = hash_code(:, is) - hash_code(:, js);
%     
%     left2 = zeros(m * m, m * m);
%     right2 = zeros(m * m, 1);
%     tic
%     for j = 1 : n
%         mat = diff_code(:, j) * diff_code(:, j)';
%         rmat = reshape(mat, m * m, 1);
%         left2 = left2 + rmat * rmat';
%         right2 = right2 + rmat * v(j);
%     end
    [left right] = c_cons_left_right(...
            int32(hash_code), int32(is), int32(js), v, int32(type));
%     cvx_begin
%         variable x(m * m, 1)
%         minimize( x' * left * x - 2 * right' * x )
%     cvx_end

    epsilon = 10^-7;
    t1 = mean(diag(left));
    if (epsilon / t1 > epsilon)
        left = left * 1 / t1;
    end
    x = (left + epsilon * eye(m^2, m^2)) \ right;

    metric_info.type = 2;
    metric_info.notes = '(h1 - h2)A(h1 - h2)';
    
    %         metric_info.A = (left * left') \ (left * v);
    metric_info.A = reshape(x, m, m);
    
elseif (type == 1)
    v = m - 2 * v * m;
    
    left = zeros(m * m, n);
    
    hash_code = 2 * hash_code - 1; % transfer to {-1, 1};
    
    for j = 1 : n
        mat = hash_code(:, is(j)) * hash_code(:, js(j))';
        left(:, j) = reshape(mat, m * m, 1);
    end
    metric_info.type = type;
    metric_info.notes = '1 / 2 (m - h1Ah2)';
    metric_info.A = (left * left') \ (left * v);
    metric_info.A = reshape(metric_info.A, m, m);
elseif (type == 3)
    batch_size = 2000;
    num_batch = ceil(length(is) / batch_size);
    all_left = zeros(m, m);
    all_right = zeros(m, 1);
    for i = 1 : num_batch
        if (mod(i, 1000) == 0)
            [num2str(i) '/' num2str(num_batch)]
        end
        idx_start = (i - 1) * batch_size + 1;
        idx_end = i * batch_size;
        if (idx_end > numel(is))
            idx_end = numel(is);
        end
        sub_left = hash_code(:, is(idx_start : idx_end)) - ...
            hash_code(:, js(idx_start : idx_end));
        sub_left = abs(sub_left);
        all_left = all_left + sub_left * sub_left';
        all_right = all_right + sub_left * double(v(idx_start : idx_end));
    end
    metric_info.alpha = all_left \ all_right;
    metric_info.type = type;
    metric_info.notes = 'alpha * h1^h2';
else
    error('wrong');
end

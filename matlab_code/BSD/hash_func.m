function code = hash_func(para, Xtraining, type)
if (strcmp(type, 'linear'))
    W = para;
    clear para;
    n = size(Xtraining, 2);
    Xtraining = [Xtraining; ones(1, n)];
    code = W' * Xtraining;
    code = code > 0;
else
    error('wrong type');
end
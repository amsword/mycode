#ifndef _H_AUX
#define _H_AUX

void mat_multiple_int(int* p1, int* p2, int n, double* pout)
{
    int k;
    int i;
    int j;
    int t;
    
    k = 0;
    for (j = 0; j < n; j++)
    {
        t = p2[j];
        for (i = 0; i < n; i++)
        {
            (*pout++) = p1[i] * t;
        }
    }
}

void mat_multiple_double(double* p1, double* p2, int n, double* pout)
{
    int k;
    int i;
    int j;
    double t;
    
    k = 0;
    for (j = 0; j < n; j++)
    {
        t = p2[j];
        for (i = 0; i < n; i++)
        {
            (*pout++) = p1[i] * t;
        }
    }
}

#endif
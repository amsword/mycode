%%
confidence_score = read_mat(file_confidence_score, 'float');
[s_confidence] = sort(confidence_score, 1, 'descend');

true_label = read_mat(file_true_label, 'int32') + 1;
[~, predicted_label] = max(confidence_score, [], 1);
bad_samples = predicted_label ~= true_label;
%%
confuse_samples = false(1, size(confidence_score, 2));
for i = 1 : size(confidence_score, 2)
    if s_confidence(1, i) - s_confidence(2, i) < confuse_th
        confuse_samples(i) = 1;
    end
end
num_confuse_samples = sum(confuse_samples);
idx0_confuse_samples = find(confuse_samples) - 1;
save_mat(idx0_confuse_samples, file_confuse_sample, 'int32');
num_bad_confuse = sum(confuse_samples .* bad_samples);
err = num_bad_confuse / num_confuse_samples;
pre = 1 - err;

%%
file_confuse_new_confidence = 'confuse_samples_cs_2_20_150_0.01.float';
confuse_new_confidence = read_mat(file_confuse_new_confidence, 'float');
[~, confuse_new_label] = max(confuse_new_confidence, [], 1); 
confuse_old_label = predicted_label(confuse_samples);
confuse_true_label = true_label(confuse_samples);
confuse_good_good = (confuse_old_label == confuse_true_label) & ...
    (confuse_new_label == confuse_true_label);
confuse_good_bad = (confuse_old_label == confuse_true_label) & ...
    (confuse_new_label ~= confuse_true_label);
confuse_bad_good = (confuse_old_label ~= confuse_true_label) & ...
    (confuse_new_label == confuse_true_label);
confuse_bad_bad = (confuse_old_label ~= confuse_true_label) & ...
    (confuse_new_label ~= confuse_true_label);

fprintf('& %d & %d & %d & %d \\\\\n', ...
    sum(confuse_good_good), ...
    sum(confuse_good_bad), ...
    sum(confuse_bad_good), ...
    sum(confuse_bad_bad));




% file_name
feature_label = 'pool3';
file_exact_knn = ['cifar10_iter_120001_90_' feature_label '_exact_l2_knn_10000_index'];
file_confidence_score = 'cifar10_iter_120001_90_feature_test_softmax';
file_true_label = '/home/wangjianfeng/data/cifar10/preprocessed/cifar10-lmdb/cifar-test-lmdb_label';
file_train_label = '/home/wangjianfeng/data/cifar10/preprocessed/cifar10-lmdb/cifar-train-lmdb_label';
code_dir = '/home/wangjianfeng/code/mycode/matlab_code/knncnn/';
file_transfer = [code_dir feature_label '_bad_exact_quality'];
file_train_feature = ['cifar10_iter_120001_90_feature_train_' feature_label];
confuse_th = 0.3;
file_confuse_sample = ['model90_confuse_sample_' num2str(confuse_th)];
file_train_confidence = 'cifar10_iter_120001_90_feature_train_softmax';
%%
check_bad_samples;
%%
generate_confuse_samples;
%%
check_train_loss;

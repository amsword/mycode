if strcmp(data_type, 'RANDOM')
    Xtraining = rand(64, 1000);
    num_coarse_cluster = 10;
else
    file_training = [working_folder data_type '/knncnn/feature_mat_float'];
    file_test = [working_folder data_type '/knncnn/feature_test_mat_float'];
    Xtraining = read_mat(file_training, 'float');
    num_coarse_cluster = 1024;
end
cd([working_folder data_type '/knncnn']);

%%
file_model_coarse = [working_folder data_type '/knncnn/coarse_' ...
    num2str(num_coarse_cluster) '.mat'];
file_model_finer = [working_folder data_type '/knncnn/finer_' ...
    num2str(num_coarse_cluster) '_' ...
    num2str(num_partitions) '_' ...
    num2str(num_sub_dic_each_partition) '.mat'];
x = load(file_model_coarse, 'coarse_centers');
if ~isfield(x, 'coarse_centers')
    coarse_centers = w_kmeans(Xtraining, num_coarse_cluster);
    save(file_model_coarse, 'coarse_centers');
else
    coarse_centers = x.coarse_centers;
    clear x;
end
%% load database
x = load(file_model_coarse, 'coarse_idx');
if isfield(x, 'coarse_idx')
    coarse_idx = x.coarse_idx;
else
    coarse_idx = w_assignment(Xtraining, coarse_centers);
    save(file_model_coarse, 'coarse_idx', '-append');
end
%% residual and ock_training
assert(strcmp(data_type, 'cifar10') == 1);

rXtraining = Xtraining - coarse_centers(:, coarse_idx);

is_run = true;
if exist(file_model_finer, 'file')
    x = load(file_model_finer, 'ock_model');
    if isfield(x, 'ock_model')
        ock_model = x.ock_model;
        is_run = false;
    end
end
if is_run
    clear opt_input_ock;
    opt_input_ock.num_sub_dic_each_partition = num_sub_dic_each_partition;
    opt_input_ock.sub_dic_size_each_partition = 256;
    opt_input_ock.num_partitions = num_partitions;
    opt_input_ock.num_grouped = 1;
    [ock_model, compactB, sub_ock_models] = ...
        ock_hierarchy_trianing(Xtraining, opt_input_ock);
    save(file_model_finer, 'ock_model', 'compactB', 'sub_ock_models', 'opt_input_ock');
end
%
Xtest = read_mat(file_test, 'float');
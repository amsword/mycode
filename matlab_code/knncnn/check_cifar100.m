% file_name

file_true_label = [data_folder  data_type ...
    '/preprocessed/' data_type '-lmdb/' 'cifar-test-lmdb_label'];

feature_label = 'pool3';
file_exact_knn = ['cifar100_yuncode_iter_200000_' feature_label '_exact_l2_knn_10000_index'];
file_confidence_score = 'cifar100_yuncode_iter_200000_feature_test_softmax';
file_train_confidence = 'cifar100_yuncode_iter_200000_feature_train_softmax';
file_train_label = [data_folder  data_type ...
    '/preprocessed/' data_type '-lmdb/' 'cifar-train-lmdb_label'];
code_dir = '/home/wangjianfeng/code/mycode/matlab_code/knncnn/';
file_transfer = [code_dir feature_label '_bad_exact_quality'];
file_train_feature = ['cifar100_yuncode_iter_200000_feature_train_' feature_label];
file_test_feature = ['cifar100_yuncode_iter_200000_feature_test_' feature_label];
confuse_th = 0.3;
file_confuse_sample = ['model90_confuse_sample_' num2str(confuse_th)];


%%
true_label = read_mat(file_true_label, 'int32') + 1;
%%
check_bad_samples;
%%
generate_confuse_samples;

check_train_loss;
%%
x = cumsum(predicted_label == true_label);
n = 6187;  x(n) / n

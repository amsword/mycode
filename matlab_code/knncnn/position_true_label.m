bad_confidence_score = confidence_score(:, bad_samples);
v = zeros(size(bad_confidence_score, 2), 1);
for i = 1 : size(bad_confidence_score, 2)
    conf = bad_confidence_score(:, i);
    t = bad_true_label(i);
    [~, s_idx] = sort(conf, 'descend');
    for j = 1 : size(s_idx)
        if s_idx(j) == t
            v(i) = j;
            break;
        end
    end
end

%%
bad_confidence_score = confidence_score(:, bad_samples);
v = zeros(size(bad_confidence_score, 2), 1);
for i = 1 : size(bad_confidence_score, 2)
    conf = bad_confidence_score(:, i);
    t = bad_true_label(i);
    v(i) = max(conf) - conf(t);
end

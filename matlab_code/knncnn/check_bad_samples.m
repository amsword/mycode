%% check the position of bad samples
confidence_score = read_mat(file_confidence_score, 'float');
[~, predicted_label] = max(confidence_score, [], 1);
true_label = read_mat(file_true_label, 'int32') + 1;
bad_samples = predicted_label ~= true_label;
bad_true_label = true_label(bad_samples);

bad_confidence_score = confidence_score(:, bad_samples);
v = zeros(size(bad_confidence_score, 2), 1);
for i = 1 : size(bad_confidence_score, 2)
    conf = bad_confidence_score(:, i);
    t = bad_true_label(i);
    [~, s_idx] = sort(conf, 'descend');
    for j = 1 : size(s_idx)
        if s_idx(j) == t
            v(i) = j;
            break;
        end
    end
end
mean(v)
std(v)
%% check the difference between the largest and 
% the true largest
bad_confidence_score = confidence_score(:, bad_samples);
v = zeros(size(bad_confidence_score, 2), 1);
for i = 1 : size(bad_confidence_score, 2)
    conf = bad_confidence_score(:, i);
    t = bad_true_label(i);
    v(i) = max(conf) - conf(t);
end
mean(v)
std(v)
numel(find(v < 0.4)) / numel(v)
%%


train_feature = read_mat(file_train_feature, 'float');
train_label = read_mat(file_train_label, 'int32') + 1;
test_feature = read_mat(file_test_feature, 'float');


bad_test_feature = test_feature(:, bad_samples);

% [eigenvectors, eigenvalues] = dengLDA(train_label', [], train_feature');
options = [];
options.intraK = 5;
options.interK = 40;
options.Regu = 1;
[~, ~, eigenvectors, eigenvalues] = MFA(train_label', options, train_feature');



% position_true_label;
%%
for selected = size(eigenvectors, 2) : -1 : 1
    reduced_train_feature = eigenvectors(:, 1 : selected)' * train_feature;
    reduced_test_feature = eigenvectors(:, 1 : selected)' * test_feature;
%     reduced_train_feature = train_feature;
%     reduced_test_feature = test_feature;
    %
    reduced_bad_test_feature = reduced_test_feature(:, bad_samples);
    %
    e2_dist = distMat(reduced_bad_test_feature, reduced_train_feature);
    [~, bad_exact_knn_full] = sort(e2_dist, 2);
    bad_exact_knn = bad_exact_knn_full(:, 1 : 10000)';
    
    bad_predicted_label = predicted_label(bad_samples);
    bad_true_label = true_label(bad_samples);
    bad_knn_true_label_quality = compute_knn(bad_exact_knn, train_label, bad_true_label);
    bad_knn_predicted_label_quality = compute_knn(bad_exact_knn, train_label, bad_predicted_label);
    
    fprintf('%f\n', selected);
    fprintf('%f\t', bad_knn_true_label_quality([10 100 500 1000 2000]))
    fprintf('\n');
    fprintf('%f\t', bad_knn_predicted_label_quality([10 100 500 1000 2000]))
    fprintf('\n');
end
return;
%%
confidence_score = read_mat(file_confidence_score, 'float');
true_label = read_mat(file_true_label, 'int32') + 1;
train_label = read_mat(file_train_label, 'int32') + 1;
[~, predicted_label] = max(confidence_score, [], 1);
accuracy = sum(predicted_label == true_label) / numel(true_label);
bad_samples = predicted_label ~= true_label;
exact_knn = read_mat(file_exact_knn, 'int32') + 1;
bad_exact_knn = exact_knn(:, bad_samples);
bad_predicted_label = predicted_label(bad_samples);
bad_true_label = true_label(bad_samples);

%%
bad_knn_true_label_quality = compute_knn(bad_exact_knn, train_label, bad_true_label);
bad_knn_predicted_label_quality = compute_knn(bad_exact_knn, train_label, bad_predicted_label);
save(file_transfer, 'bad_knn_true_label_quality', 'bad_knn_predicted_label_quality');

%%
bad_confidence_score = confidence_score(:, bad_samples);
mean(bad_confidence_score(sub2ind([10 sum(bad_samples)], bad_predicted_label, 1 : sum(bad_samples))))
std(bad_confidence_score(sub2ind([10 sum(bad_samples)], bad_predicted_label, 1 : sum(bad_samples))))

mean(bad_confidence_score(sub2ind([10 sum(bad_samples)], bad_true_label, 1 : sum(bad_samples))))
std(bad_confidence_score(sub2ind([10 sum(bad_samples)], bad_true_label, 1 : sum(bad_samples))))
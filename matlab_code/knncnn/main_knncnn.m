[ret, name] = system('hostname');
if ret ~= 0,
    if ispc
        name = getenv('COMPUTERNAME');
    else
        name = getenv('HOSTNAME');
    end
end
machine_name = lower(name);
machine_name = strtrim(machine_name);
if strcmp(machine_name, 'evdell18')
    conf_dir = 'D:\OneDrive\BitBucketCode\mycode\matlab_code';
    data_folder = 'D:\research\data\';
    working_folder = 'D:\research\working\';
elseif strcmp(machine_name(1 : 2), 'lv')
    data_folder = '/home/jianfeng/data/';
    working_folder = '/home/jianfeng/working/';
    conf_dir = '/home/wangjianfeng/code/mycode/matlab_code';
elseif strcmp(machine_name(1 : 4), 'deep')
    data_folder = '/home/wangjianfeng/data/';
    working_folder = '/home/wangjianfeng/working/';
    conf_dir = '/home/wangjianfeng/code/mycode/matlab_code';
else
    error('dfsf');
end
cd(conf_dir);
jf_conf;

%
% data_type = 'RANDOM';
% data_type = 'cifar10';
% data_type = 'imagenet2012';
data_type = 'cifar100';
cd([working_folder data_type '/knncnn/cccp_model']);
num_partitions = 1;
num_sub_dic_each_partition = 8;

%%


%% all file name
generate_all_files_knncnn;

%% remove the training data with very good predictions. 

train_confidence = read_mat(file_train_confidence, 'float=>float');
train_label = read_mat(file_train_label, 'int32') + 1;
train_confidence_true = train_confidence(...
    sub2ind(size(train_confidence), train_label, 1 : size(train_confidence, 2)));

if strcmp(data_type, 'cifar10')
    th = 0.999;
elseif strcmp(data_type, 'cifar100')
    th = 0.99;
end
idx = find(train_confidence_true < th) - 1;
numel(idx) / numel(train_confidence_true)
save_mat(idx, [model_name num2str(th) '_selected_fine_tune.int'], 'int32'); 

idx = idx + 1;
selected_origin_confidence_true = train_confidence_true(:, idx);
selected_current_confidence = read_mat('cifar10_iter_120001_90_feature_train_softmax_selected', 'float=>float');
selected_current_confidence = selected_current_confidence(:, 1 : numel(selected_origin_confidence_true));
selected_current_confidence_true = selected_current_confidence(...
    sub2ind(size(selected_current_confidence), train_label(idx), 1 : size(selected_current_confidence, 2)));


%% split the training feature into multiple files
feature_label = 'fc2';
file_test_feature = [model_name 'feature_test_' feature_label];
test_feature = read_mat(file_test_feature, 'float=>float');

num_split = 5;
num_test = size(test_feature, 2);
split_size = num_test / num_split;
assert(mod(num_test, num_split) == 0);

for i = 1 : num_split
    idx_start = (i - 1) * split_size + 1;
    idx_end = i * split_size;
    save_mat(test_feature(:, idx_start : idx_end), ...
        [file_test_feature '_' num2str(num_split) '.' num2str(i)], 'float');
end


%% check 
query_feature = read_mat(file_query_feature, 'float=>float');
query_label = read_mat(file_query_label, 'int32=>in32');
[tmp, idx1] = max(query_feature, [], 1);
selected = 1 : 100;
idx0 = idx1 - 1;
sum(double(query_label(selected)) == idx0(selected)) / numel(idx0(selected))

%% the acc of the base model. 
num_selected = 2000;
confidence_score = read_mat2(file_confidence_score, 'float', 1, num_selected);
[~, predicted_label] = max(confidence_score, [], 1);
true_label = read_mat(file_true_label, 'int32') + 1;
true_label = true_label(1 : num_selected);
acc_base = sum(true_label == predicted_label) / num_selected;
% check_cifar10
check_cifar100


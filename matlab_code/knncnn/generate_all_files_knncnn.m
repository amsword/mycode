file_query_label = [working_folder data_type '/knncnn/' data_type '-test-lmdb.labels'];
file_database_label = [working_folder data_type '/knncnn/' data_type '-train-lmdb.labels'];
feature_blob_name = 'softmax';
file_query_feature = [working_folder data_type '/knncnn/' 'features_test_' ...
    feature_blob_name '.matrix.float'];
file_database_feature = [working_folder data_type '/knncnn/' 'features_train_' ...
    feature_blob_name '.matrix.float'];
file_bruteforce_knn = [working_folder data_type '/knncnn/' 'bruteforce_l2_' ...
    feature_blob_name '_knn.float'];
file_bruteforce_knn_int = [working_folder data_type '/knncnn/' 'bruteforce_l2_' ...
    feature_blob_name '_knn.int'];
file_bruteforce_knn_quality = [working_folder data_type '/knncnn/' 'bruteforce_l2_' ...
    feature_blob_name '_knn.quality.txt'];

para_folder = ['/home/wangjianfeng/code/caffe/examples/'];
if strcmp(data_type, 'cifar10')
    file_knn_proto = [para_folder data_type '/knncnn/cifar10_full_solver_knn.prototxt'] ;
end
file_model = [working_folder data_type '/active_set/cifar10_full_iter_50000'];

topk = 1000;
my_tools_dir = '/home/wangjianfeng/code/mycode/c_code/bin';
caffe_tools_dir = '/home/wangjianfeng/code/caffe/.build_release/tools';
% app_knn;


if strcmp(data_type, 'cifar10')
    model_name = 'cifar10_iter_120001_90_';
    file_train_label = [data_folder data_type ...
        '/preprocessed/cifar10-lmdb/cifar-train-lmdb_label'];
    file_true_label = [data_folder data_type ...
        '/preprocessed/cifar10-lmdb/cifar-test-lmdb_label'];
elseif strcmp(data_type, 'cifar100')
    model_name = 'cifar100_yuncode_iter_200000_';
    file_train_label = [data_folder data_type ...
        '/preprocessed/cifar100-lmdb/cifar-train-lmdb_label'];
elseif strcmp(data_type, 'imagenet2012')
    model_name = 'nizf4_iter_710710_';
    file_train_label = [data_folder  data_type ...
        '/lmdb_256xN/' 'imagenet_train_lmdb_256xN_label'];
    file_true_label = [data_folder data_type ...
        '/lmdb_256xN/imagenet_val_lmdb_256xN_label'];
else
    error('sdfs');
end
file_train_confidence = [model_name 'feature_train_softmax'];
file_confidence_score = [model_name 'feature_test_softmax'];


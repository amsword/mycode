num = 1281167;

for i = 2 : 500
    if mod(num, i) == 0
        break;
    end
end
i
%%

% file_name

file_true_label = [data_folder  data_type ...
    '/preprocessed/' data_type '-lmdb/' 'cifar-test-lmdb_label'];

feature_label = 'pool3';
file_exact_knn = ['nizf4_iter_710710_' feature_label '_exact_l2_knn_10000_index'];
file_confidence_score = 'nizf4_iter_710710_feature_test_softmax';
file_train_confidence = 'nizf4_iter_710710_feature_train_softmax';
file_train_label = [data_folder  data_type ...
    '/lmdb_256xN/' 'imagenet_train_lmdb_256xN_label'];
code_dir = '/home/wangjianfeng/code/mycode/matlab_code/knncnn/';
file_transfer = [code_dir feature_label '_bad_exact_quality'];
file_train_feature = ['nizf4_iter_710710_feature_train_' feature_label];
file_test_feature = ['nizf4_iter_710710_feature_test_' feature_label];
confuse_th = 0.3;
file_confuse_sample = ['model90_confuse_sample_' num2str(confuse_th)];


%%
true_label = read_mat(file_true_label, 'int32') + 1;
%%
check_bad_samples;
%%
generate_confuse_samples;

check_train_loss;
%%

function y = compute_knn(knn_result, train_label, test_label)
knn_result_label = train_label(knn_result);
test_label = reshape(test_label, 1, numel(test_label));
y = bsxfun(@eq, knn_result_label, test_label);
y = double(y);
y = cumsum(y, 1);
y = mean(y, 2);
y = y ./ [1 : numel(y)]';

function opt_input_ock = get_opt_params(opt_input_ock)

default_num_partitions = 4;
default_sub_dic_size_each_partition = 256;
if isfield(opt_input_ock, 'all_D')
    all_D = opt_input_ock.all_D;
    default_num_partitions = numel(all_D);
    default_sub_dic_size_each_partition = size(all_D{1}, 2);
end

if isfield(opt_input_ock, 'num_sub_dic_each_partition')
    num_sub_dic_each_partition = opt_input_ock.num_sub_dic_each_partition;
else
    num_sub_dic_each_partition = 1;
end

if isfield(opt_input_ock, 'func_tol')
    func_tol = opt_input_ock.func_tol;
else
    func_tol = 10^-4;
end

if isfield(opt_input_ock, 'sub_dic_size_each_partition')
    sub_dic_size_each_partition = opt_input_ock.sub_dic_size_each_partition;
else
    sub_dic_size_each_partition = default_sub_dic_size_each_partition;
end

if isfield(opt_input_ock, 'num_partitions')
    num_partitions = opt_input_ock.num_partitions;
else
    num_partitions = default_num_partitions;
end

if isfield(opt_input_ock, 'max_iter')
    max_iter = opt_input_ock.max_iter;
else
    max_iter = 100;
end

if isfield(opt_input_ock, 'is_optimize_R')
    is_optimize_R = opt_input_ock.is_optimize_R;
else
    is_optimize_R = true;
end

if num_partitions == 1
    is_optimize_R = false;
end
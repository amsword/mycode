function [ock_output_model]  = mck_training(...
    Xtraining, opt_input_ock)

%%
opt_input_ock = get_opt_params(opt_input_ock);

dim = size(Xtraining, 1);
vec_2_mat_size = opt_input_ock.vec_2_mat_size;
num_partitions = opt_input_ock.num_partitions;
assert(prod(vec_2_mat_size) == dim);

is_left_R = opt_input_ock.is_left_R;
is_right_R = opt_input_ock.is_right_R;

if vec_2_mat_size(1) == 1
    is_left_R = false;
end
if vec_2_mat_size(2) == 1
    is_right_R = false;
end
%% set partition information
sub_dim_start_idx = cell(2, 1);
sub_dim_start_idx{1} = dim_split(vec_2_mat_size(1), num_partitions(1)) - 1;
sub_dim_start_idx{2} = dim_split(vec_2_mat_size(2), num_partitions(2)) - 1;

mck_model.sub_dim_start_idx = sub_dim_start_idx;

%% intialize R
% if isfield(opt_input_ock, 'init_R') && is_optimize_R
%     R = opt_input_ock.init_R;
% else
%     R = eye(dim, dim);
% end

if is_left_R
    mck_model.left_R = eye(vec_2_mat_size(1), vec_2_mat_size(1));
else
    mck_model.left_R = [];
end
if is_right_R
    mck_model.right_R = eye(vec_2_mat_size(2), vec_2_mat_size(2));
else
    mck_model.right_R = [];
end

%% initialize D

if ~isfield(opt_input_ock, 'init_all_D')
    mck_model.all_D = mckRandomInitDictionary(...
        Xtraining, sub_dim_start_idx, ...
        num_sub_dic_each_partition, sub_dic_size_each_partition);
else
    mck_model.all_D = opt_input_ock.init_all_D;
end
ock_output_model.init_all_D = mck_model.all_D;

%% initialize B
clear para_encode;
para_encode.is_initialize = 1;
para_encode.num_grouped = opt_input_ock.num_grouped;

if isfield(opt_input_ock, 'is_ock')
    para_encode.is_ock = opt_input_ock.is_ock;
end

if ~para_encode.is_initialize
    para_encode.old_codes = old_compact_subB;
end

compactB = mck_encoding(Xtraining, mck_model, para_encode);
mck_model.all_D = mck_update_dictionary(Xtraining, mck_model); % without code.

para_encode.is_initialize = 0;

%%

obj = zeros(3 * max_iter, 1);
i = 1;
total_time = 0;
if ~is_optimize_R
    assert(sum(sum(abs(R - eye(dim, dim)))) < 10^-9);
end

%%
for iter = 1 : max_iter
    [num2str(iter) '/' num2str(max_iter)]
    
    tic;
    if is_optimize_R
        R = UpdateRotation(Xtraining, all_D, compactB);
        Z = R' * Xtraining;
    else
        Z = Xtraining;
    end
    
    obj(i) = DistortionLoss(Z, all_D, compactB);
    i = i + 1;
  
    all_D = UpdateDictionary(Z, compactB, sub_dim_start_idx, ...
        num_sub_dic_each_partition, sub_dic_size_each_partition);
  
    obj(i) = DistortionLoss(Z, all_D, compactB);
    
    i = i + 1;
    
    compactB = UpdateRepresentation(...
        Z, all_D, compactB, num_sub_dic_each_partition, para_encode);
    
    obj(i) = DistortionLoss(Z, all_D, compactB);
    i = i + 1;

    time_iter = toc;
    total_time = total_time + time_iter;
    fprintf('time/iter: %f. Left: %f. obj: %f\n', ...
        time_iter,  time_iter * (max_iter - iter), ...
        obj(i - 1));
    
    if iter > 1
        if abs(obj(i - 1) - obj(i - 4)) < func_tol
            break;
        end
    end
end

ock_output_model.is_optimize_R = is_optimize_R;
ock_output_model.R = R;
ock_output_model.all_D = all_D;
ock_output_model.obj = obj(1 : 3 * iter);
ock_output_model.num_sub_dic_each_partition = num_sub_dic_each_partition;
ock_output_model.sub_dim_start_idx = sub_dim_start_idx;
ock_output_model.num_grouped = opt_input_ock.num_grouped;
if isfield(opt_input_ock, 'is_ock')
    ock_output_model.is_ock = opt_input_ock.is_ock;
end

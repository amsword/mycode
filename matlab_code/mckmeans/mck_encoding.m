function new_compactB = mck_encoding(...
    Xtraining, mck_model, para_encode)

new_compactB = mexmck_encoding(Xtraining, mck_model, para_encode);

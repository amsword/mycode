function all_D = mckRandomInitDictionary(...
    Xtraining, sub_dim_start_idx, num_sub_dic_each_partition, sub_dic_size_each_partition)

% note: the strategy of initializing the dictionary is different from that
% described in the paper of group k-means
num_point = size(Xtraining, 2);

row_size = sub_dim_start_idx{1}(end) - 1;
col_size = sub_dim_start_idx{2}(end) - 1;
num_row_partition = numel(sub_dim_start_idx{1}) - 1;
num_col_partition = numel(sub_dim_start_idx{2}) - 1;

rp = randperm(num_point);
rp = rp(1 : sub_dic_size_each_partition);
selected_data = Xtraining(:, rp);
selected_mat = reshape(selected_data, [row_size, col_size, sub_dic_size_each_partition]);

all_D = cell(num_row_partition, num_col_partition);
for i = 1 : num_row_partition
    for j = 1 : num_col_partition

        sub_row_start = sub_dim_start_idx{1}(i);
        sub_row_end = sub_dim_start_idx{1}(i + 1) - 1;
        sub_col_start = sub_dim_start_idx{2}(j);
        sub_col_end = sub_dim_start_idx{2}(j + 1) - 1;
        sub_size = (sub_row_end + 1 - sub_row_start) * (sub_col_end + 1 - sub_col_start);
        sub_data = selected_mat(sub_row_start : sub_row_end, ...
             sub_col_start : sub_col_end , :);
         sub_data = reshape(sub_data, sub_size, sub_dic_size_each_partition);
        
         init_scheme = dim_split(sub_size, num_sub_dic_each_partition);
        
         subD = cell(num_sub_dic_each_partition, 1);         
        for k = 1 : num_sub_dic_each_partition
            sub_sub_d = zeros(sub_size, sub_dic_size_each_partition);
            sub_sub_d(init_scheme(k) : init_scheme(k + 1) - 1, :) = ...
                sub_data(init_scheme(k) : init_scheme(k + 1) - 1, :);
            subD{k} = sub_sub_d;
        end
        all_D{i, j} = subD;
    end
end
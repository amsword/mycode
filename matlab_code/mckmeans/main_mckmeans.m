function main_mckmeans(...
     num_sub_dic_each_partition, ...
    num_grouped)


idx_initialize_type = 1;
num_grouped = 2; % only valid for gk-means.
% data_type = 'SIFT1M' %%
data_type = 'RANDOM'; % only for debugging.
% data_type = 'GIST1M' 
% data_type = 'MNIST';
% encoding_method = 'gkmeans';
% encoding_method = 'ock';
encoding_method = 'mck';
vec_2_mat_size = [8 16];
num_partitions = [2 2];
num_sub_dic_each_partition = 8;
iter_mnist = 100;

is_test_encoding_time = 0;

all_initialize_type = ...
    {'random', 'kmeans', 'ockmeans', 'h_ockmeans'};
initialize_type = all_initialize_type{idx_initialize_type}

% almost not changed.
h_ockmeans_initi_num_iter = 30
h_ockmeans_final_num_iter = 100
max_num_sub_dic_each_partition = 16;
topks = [1, 10, 100];
sub_dic_size_each_partition = 256;
data_type
num_grouped
num_sub_dic_each_partition
encoding_method

if ispc()
    conf_dir = 'D:\OneDrive\BitBucketCode\mycode\matlab_code';
elseif isunix()
    conf_dir = '/home/jianfeng/mycode/matlab_code';
end
cd(conf_dir);
jf_conf;
prepare_folder_name;
cd([working_folder data_type '/mckmeans/non_full_training']);

if strcmp(initialize_type, 'h_ockmeans')
    file_output = [encoding_method '_' num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_grouped' num2str(num_grouped) '_' ...
        'initialize_type' initialize_type '_' ...
        num2str(h_ockmeans_initi_num_iter) '_' ...
        num2str(h_ockmeans_final_num_iter) '.mat'];
else
    %
    file_output = [encoding_method '_' ...
        num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_grouped' num2str(num_grouped) '_' ...
        'initialize_type' initialize_type '.mat'];
end

if strcmp(encoding_method, 'ock')
    if num_sub_dic_each_partition == 1
        file_output = ['../../sckmeans/non_full_training/ck_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) ...
            '.mat'];
    else
        file_output = ['../../sckmeans/non_full_training/ock_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) '_' ...
            num2str(num_sub_dic_each_partition) '_102.mat'];
    end
end

if strcmp(data_type, 'MNIST') && ...
        iter_mnist ~= 100 && ...
        strcmp(encoding_method, 'ock')
    if num_sub_dic_each_partition == 1
        file_output = ['../../sckmeans/non_full_training/ck_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) '_iter_' ...
            num2str(iter_mnist) '.mat'];
    else
        file_output = ['../../sckmeans/non_full_training/ock_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) '_' ...
            num2str(num_sub_dic_each_partition) '_' ...
            'iter_' num2str(iter_mnist) '_102.mat'];
    end
end




%%
if is_test_encoding_time
    main_gkmeans_test_encoding_time;
else
    main_gkmeans_one_case;
end

% main_gkmeans_trying
% gkmeans_plot_result


%% dist ratio
for code_length  = [32 64 128]
    
    figure;
   
    used = 0 : 50 : 10^3;
    used(1) = 1;
%     used = 1 : 10^3;
    for is_sym = [0 1];
        % all_dic_num = [4 8 16];
        clear pq ck jck ock
      
        var_name = 'dist_ratio';
        if is_sym
            var_name = [var_name '_sym'];
        end
        
        dic_num = code_length / 8;
        pq = load(['pq_' num2str(dic_num) '_256'], var_name);
        ck = load(['ck_' num2str(dic_num) '_256'], var_name);
        
        dic_num = dic_num / 2;
        jck = load(['jck_' num2str(dic_num) '_256_2'], var_name);
        ock = load(['ock_' num2str(dic_num) '_256_2_102'], var_name);
        
        if is_sym
            plot(used, ock.dist_ratio_sym(used), 'r--', ...
                'LineWidth', 2, ...
                'marker', marker_ock);
            hold on;
            plot(used, jck.dist_ratio_sym(used), 'g--', ...
                'LineWidth', 2, ...
                'marker', marker_eck);
            plot(used, ck.dist_ratio_sym(used), 'k--', ...
                'LineWidth', 2, ...
                'marker', marker_ck);
            plot(used, pq.dist_ratio_sym(used), 'b--', ...
                'LineWidth', 2, ...
                'marker', marker_pq);
        else
            plot(used, ock.dist_ratio(used), 'r', ...
                'LineWidth', 2, ...
                'marker', marker_ock);
            hold on;
            plot(used, jck.dist_ratio(used), 'g', ...
                'LineWidth', 2, ...
                'marker', marker_eck);
            plot(used, ck.dist_ratio(used), 'k', ...
                'LineWidth', 2, ...
                'marker', marker_ck);
            plot(used, pq.dist_ratio(used), 'b', ...
                'LineWidth', 2, ...
                'marker', marker_pq);
        end
    end
    
    lo = 'NorthEast';
    
    if code_length == 32
        if strcmp(type, 'GIST1M3')
            ylim([1.07, 1.2]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([1.07, 1.22]);
        end
    elseif code_length == 64
        if strcmp(type, 'GIST1M3')
            ylim([1.04, 1.15]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([1.03, 1.12]);
        end
    elseif code_length == 128
        if strcmp(type, 'GIST1M3')
            ylim([1.024, 1.09]);
        elseif strcmp(type, 'SIFT1M3')
            ylim([1.01, 1.05]);
        end
    end 
    
    
    xlabel('Number of Retrieved Points', 'FontSize', 14);
    ylabel('Mean Overall Ratio', 'FontSize', 14);
    
    legend('ock-means-A', ...
        'eck-means-A', ...
        'ck-means-A', ...
        'PQ-A', ...
        'ock-means-D', ...
        'eck-means-D', ...
        'ck-means-D', ...
        'PQ-D', ...
        'Location', lo);
    
    set(gca, 'FontSize', 14);
    grid on;
    set(gcf, 'PaperPositionMode', 'auto');
    saveas(gca, ['figs\' type '_' num2str(code_length) '_dist_ratio_both.eps'],'psc2');

end

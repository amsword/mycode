function [mat_compact_B] = UpdateRepresentationCompact(...
    Xtraining, R, all_D, num_sub_dic_each_partition)

Z = R' * Xtraining;

if num_sub_dic_each_partition == 1
    method = 0;
else
    method = 102;
end

num_point = size(Xtraining, 2);

mat_compact_B = zeros(numel(all_D) * num_sub_dic_each_partition, num_point, 'int16');

idx_dim_start = 1;
for i = 1 : numel(all_D)
    subD = all_D{i};
    sub_dim = size(subD, 1);
    
    idx_dim_end = idx_dim_start + sub_dim - 1;
    subZ = Z(idx_dim_start : idx_dim_end, :);
    idx_dim_start = idx_dim_end + 1;
    
    new_compact_SubB = mexMatchingPersuit(subZ, subD, num_sub_dic_each_partition, method);

    mat_compact_B((i - 1)* num_sub_dic_each_partition + 1 : i * num_sub_dic_each_partition, : ) = ...
        new_compact_SubB;
end
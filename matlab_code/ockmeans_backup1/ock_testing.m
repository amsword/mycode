function [result_idx, time_cost] = ock_testing(...
    query, database, gamma, max_expanded, ...
    database_code, distance_info, ranking_info)

if ranking_info.is_re_ranking
    ranking_info2.num_candidate = max_expanded * 10;
    ranking_info2.gamma = gamma / 2;
end

[idx_knn, time_cost] = mexAbsInnerSearch(...
    query, ...
    database_code, ...
    distance_info, ...
    ranking_info2);

if ranking_info.is_re_ranking
    increased = double(idx_knn) + 1;
    selected_database = database(:, increased);
    nn_idx = abs_nn_brute_force(query, selected_database, gamma, max_expanded);
    result_idx = increased(nn_idx);
else
    result_idx = double(idx_knn) + 1;
end

end

function matlab_compute_check(query, database, gamma, topk, ...
    database_code, ock_model, opt_testing)

%%
tic;
rotated = distance_info.R' * query;
num_partitions = numel(distance_info.all_D);
lookups = cell(num_partitions, 1);
idx_start = 1;
for i = 1 : num_partitions
    subD = distance_info.all_D{i};
    sub_dim = size(subD, 1);
    idx_end = idx_start + sub_dim - 1;
    lookups{i} = rotated(idx_start : idx_end)' * subD;
    idx_start = idx_end + 1;
end
%
num_point = size(database_code, 2);
distance = zeros(1, num_point);
for i = 1 : num_partitions
    distance = distance + lookups{i}(double(database_code(i, :)) + 1);
end
distance = abs(distance);
idx_selected = find(distance > gamma);
[s, idx] = sort(distance(idx_selected), 'descend');
result = idx_selected(idx(1 : max_expanded));
toc;
end

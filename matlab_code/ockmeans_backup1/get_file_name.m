function file_name = get_file_name(dic_num, dic_size, ...
    s0, method, is_opt)

file_name = [num2str(dic_num) '_' num2str(dic_size) ...
    ];

if is_opt == 0
    file_name = ['pq_' file_name];
elseif s0 <= 0
    file_name = ['ck_' file_name];
elseif method == 1
    file_name = ['jck_' file_name '_' num2str(s0)];
else
    file_name = ['ock_' file_name '_' num2str(s0) '_' num2str(method)];
end

file_name = [file_name '.mat'];

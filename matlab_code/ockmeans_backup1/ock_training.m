function [ock_output_model]  = ock_training(...
    Xtraining, opt_input_ock)
%%

default_num_partitions = 4;
default_sub_dic_size_each_partition = 256;
if isfield(opt_input_ock, 'all_D')
    all_D = opt_input_ock.all_D;
    default_num_partitions = numel(all_D);
    default_sub_dic_size_each_partition = size(all_D{1}, 2);
    
end

if isfield(opt_input_ock, 'num_sub_dic_each_partition')
    num_sub_dic_each_partition = opt_input_ock.num_sub_dic_each_partition;
else
    num_sub_dic_each_partition = 1;
end

if isfield(opt_input_ock, 'func_tol')
    func_tol = opt_input_ock.func_tol;
else
    func_tol = 10^-4;
end

if isfield(opt_input_ock, 'sub_dic_size_each_partition')
    sub_dic_size_each_partition = opt_input_ock.sub_dic_size_each_partition;
else
    sub_dic_size_each_partition = default_sub_dic_size_each_partition;
end

if isfield(opt_input_ock, 'num_partitions')
    num_partitions = opt_input_ock.num_partitions;
else
    num_partitions = default_num_partitions;
end

if isfield(opt_input_ock, 'max_iter')
    max_iter = opt_input_ock.max_iter;
else
    max_iter = 100;
end

if isfield(opt_input_ock, 'is_optimize_R')
    is_optimize_R = opt_input_ock.is_optimize_R;
else
    is_optimize_R = true;
end

dic_size_each_partitions = num_sub_dic_each_partition * sub_dic_size_each_partition;

%% set partition information
dim = size(Xtraining, 1);
[sub_dim_start_idx, sub_dim_lengths] = dim_split(dim, num_partitions);

%% intialize R
if isfield(opt_input_ock, 'init_R') && is_optimize_R
    R = opt_input_ock.init_R;
else
    R = eye(dim, dim);
end
%% initialize D
if ~isfield(opt_input_ock, 'init_all_D')
    all_D = RandomInitDictionary(Xtraining, sub_dim_start_idx, dic_size_each_partitions);
    %         all_D = RandomInitDictionary3(Xtraining, dic_dim, dic_size, num_split);
else
    all_D = opt_input_ock.init_all_D;
end

%% initialize B
all_B = [];
%
all_B = UpdateRepresentation(...
    R' * Xtraining, all_D, all_B, num_sub_dic_each_partition, 0, ...
    sub_dim_start_idx);

%%
obj = zeros(3 * max_iter, 1);
i = 1;
total_time = 0;


if ~is_optimize_R
    assert(sum(sum(abs(R - eye(dim, dim)))) < 10^-9);
end
%%
for iter = 1 : max_iter
    [num2str(iter) '/' num2str(max_iter)]
    
    tic;
    if is_optimize_R
        R = UpdateRotation(Xtraining, all_D, all_B);
        Z = R' * Xtraining;
    else
        Z = Xtraining;
    end
    
    obj(i) = DistortionLoss(Z, all_D, all_B);
    i = i + 1;
    
    all_D = UpdateDictionary(Z, all_B, sub_dim_start_idx);
    
    obj(i) = DistortionLoss(Z, all_D, all_B);
    
    i = i + 1;
    
    all_B = UpdateRepresentation(...
        Z, all_D, all_B, num_sub_dic_each_partition, 0, ...
        sub_dim_start_idx);
    
    obj(i) = DistortionLoss(Z, all_D, all_B);
    i = i + 1;
    
    time_iter = toc;
    total_time = total_time + time_iter;
    fprintf('time/iter: %f. Left: %f\n', total_time / iter, total_time / iter * (max_iter - iter));
    
    if iter > 1
        if abs(obj(i - 1) - obj(i - 4)) < func_tol
            break;
        end
    end

end

ock_output_model.is_optimize_R = is_optimize_R;
ock_output_model.R = R;
ock_output_model.all_D = all_D;
ock_output_model.obj = obj(1 : 3 * iter);
ock_output_model.num_sub_dic_each_partition = num_sub_dic_each_partition;
ock_output_model.sub_dim_start_idx = sub_dim_start_idx;

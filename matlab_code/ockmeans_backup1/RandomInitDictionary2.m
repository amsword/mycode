function all_D = RandomInitDictionary2(Xtraining, dic_dim, dic_size, num_split)

dim = size(Xtraining, 1);

num_dic = dim / dic_dim;

all_D = cell(num_dic, 1);

size_split = dic_size / num_split;
assert(mod(dic_size, num_split) == 0);

for i = 1 : numel(all_D)
    
    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    all_D{i} = BestErrorGreedyRandom(...
        Xtraining, idx_dim_start, idx_dim_end, num_split, size_split);
end
end

function subD = BestErrorGreedyRandom(Xtraining, idx_dim_start, idx_dim_end, ...
    num_split, size_split)

num_point = size(Xtraining, 2);

Ds = cell(num_split, 1);
for i = 1 : num_split
    rp = randperm(num_point, size_split);
    X = Xtraining(idx_dim_start : idx_dim_end, rp);
    for j = 1 : (i - 1)
        D = Ds{j};
        distance_2_dic = sqdist(X, D);
        [~, idx] = min(distance_2_dic, [], 2);
        X = X - D(:, idx);
    end
    Ds{i} = X;
end

subD = zeros(idx_dim_end - idx_dim_start + 1, num_split * size_split);
for i = 1 : num_split
    idx_start = (i - 1) * size_split + 1;
    idx_end = i * size_split;
    subD(:, idx_start : idx_end) = Ds{i};
end

end
function  all_D = InitFromFinerD(init_all_D, dic_dim)
ratio = dic_dim / size(init_all_D{1}, 1);

dic_size = size(init_all_D{1}, 2) * ratio;

num_dic = numel(init_all_D) / ratio;

all_D = cell(num_dic, 1);

old_dic_dim = size(init_all_D{1}, 1);
old_dic_size = size(init_all_D{1}, 2);

for i = 1 : num_dic
    subD = zeros(dic_dim, dic_size);
    idx_start = (i - 1) * ratio;
    for j = 1 : ratio
        idx_row_start = (j - 1) * old_dic_dim + 1;
        idx_col_start = (j - 1) * old_dic_size +1;
        
        subD(idx_row_start : idx_row_start + old_dic_dim - 1, ...
            idx_col_start : idx_col_start + old_dic_size - 1) = init_all_D{j + idx_start};
    end
    all_D{i} = subD;
end
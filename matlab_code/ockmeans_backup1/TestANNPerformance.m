function rec = TestANNPerformance(Xbase, Xtest, R, all_D, s0, method, ...
    ground_truth_file_name, topks, num_candidate)

% R = result{3}.R;
% all_D = result{3}.all_D;
% s0 = result{3}.s0;
% method = result{3}.method;
% Xbase = Xtraining;
% topks = 1;
% num_candidate = 1000;
% ground_truth_file_name = gnd_file.StestTrainingBin;

mat_compact_B = UpdateRepresentationCompact(Xbase, R, all_D, s0, method);

rec = ANNPerformance(mat_compact_B, ...
        all_D, ...
        R' * Xtest, ...
        ground_truth_file_name, ...
        int32(topks), ...
        num_candidate);
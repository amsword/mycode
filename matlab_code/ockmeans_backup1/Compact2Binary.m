function binary = Compact2Binary(comB, dic_size, method)

s = size(comB, 1);
num_point = size(comB, 2);
vs = comB +1;
vs = double(vs);

if method == 0 || method == 2
    ind = (vs ~= 0);
    is = vs(ind);
    
    js = repmat(1 :num_point, s, 1);
    js = js(ind);
  
%     binary = sparse(is(ind), js(ind), 1, s * dic_size, num_point);   
    
    binary = zeros(dic_size, num_point);   
    t = sub2ind([dic_size, num_point], is, js);
    binary(t) = 1;
    
elseif method == 1
    binary = zeros(dic_size, num_point);
    
    for idx_s = 1 : s
        is = vs(idx_s, :);
        ind = (is ~= 0);
        is = is(ind);
        
        js = repmat(1 :num_point, 1, 1);
        js = js(ind);
          
        t = sub2ind([dic_size, num_point], is, js);
        binary(t) = binary(t) + 1;
    end
end
function W = MultipleAllAll(all_D, all_B)

num_partitions = numel(all_D);

dim = 0;
for i = 1 : num_partitions
    dim = dim + size(all_D{i}, 1);
end

num_point = size(all_B{1}, 2);

W = zeros(dim, num_point);

idx_start = 1;
for i = 1 : numel(all_D)
    dic_dim = size(all_D{i}, 1);
    
    idx_end = dic_dim + idx_start - 1;
    
    W(idx_start : idx_end, :) = all_D{i} * full(all_B{i});
    
    idx_start = idx_end + 1;
end

% dic_dim = size(all_D{1}, 1);
% dic_num = numel(all_D);
% 
% dim = dic_num * dic_dim;
% 
% num_point = size(all_B{1}, 2);
% 
% W = zeros(dim, num_point);
% 
% for i = 1 : numel(all_D)
%     idx_start = (i - 1) * dic_dim + 1;
%     idx_end = i * dic_dim;
%     
%     W(idx_start : idx_end, :) = all_D{i} * full(all_B{i});
% end
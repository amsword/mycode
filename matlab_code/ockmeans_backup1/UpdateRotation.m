function R = UpdateRotation(Xtraining, all_D, all_B)


% D = ConstructD(all_D);
% 
% W = D * B;

W = MultipleAllAll(all_D, all_B);

[svd_U, svd_Sigma, svd_V] = svd(Xtraining * W');
R = svd_U * svd_V';

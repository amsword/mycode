function code_length = AvgCodeLength(all_B, dic_size)

n = 0;
for i = 1 : numel(all_B)
    n = n + sum(all_B{i}(:));
end

num_point = size(all_B{1}, 2);

code_length = n * log2(dic_size) / num_point;

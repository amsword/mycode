function accelerate_optimization_main(...
    instruction_code, idx_start_batch, idx_end_batch)

% instruction_code = 10;
% idx_start_batch = 1;
% idx_end_batch = 2;

file_dir = pwd;
cd ..;
jf_conf;
cd(file_dir);

prepare_data;
%% compute the results by original leastR approach.

is_origin_least = mod(instruction_code, 10);
if is_origin_least
    'origin_least'
    by_origin_leastr;
end
instruction_code2 = floor(instruction_code / 10);

is_siso = mod(instruction_code2, 10);
if is_siso
    %% original SISO
    'siso'
    by_siso;
end

is_ck_siso = mod(floor(instruction_code / 100), 10);
if is_ck_siso
    by_ck_siso;
end

function [nn_idx, nn_time_details] = abs_nn_brute_force(...
    query, data_base, threshold, topk)

[nn_idx] = LinearScanAbs(query, data_base, threshold, topk);
nn_time_details = -1;
end

function [nn_idx] = check(query, data_base, threshold, topk)
zeros_threshold = 10^-10;

indicator = query' * data_base;
indicator = abs(indicator);
expanded_idx = find(indicator > threshold + zeros_threshold);
num_full_expanded_point = numel(expanded_idx);
% in theory, the intersection of expanded_idx and
% shrinked_active_set is empty. However, it is not in practice
if num_full_expanded_point <= topk
    nn_idx = expanded_idx;
else
    expanded_strength = indicator(expanded_idx);
    [tmp, sorted_idx] = sort(expanded_strength, 'descend');
    topk_idx_in_full_expanded = sorted_idx(1 : topk);
    nn_idx = expanded_idx(topk_idx_in_full_expanded);
end
nn_idx = nn_idx';
end
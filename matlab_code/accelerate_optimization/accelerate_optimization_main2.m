function accelerate_optimization_main(...
    instruction_code, idx_start_batch, idx_end_batch)

% instruction_code = 10;
% idx_start_batch = 1;
% idx_end_batch = 2;

file_dir = pwd;
cd ..;
jf_conf;
cd(file_dir);

prepare_data;
%% compute the results by original leastR approach.

problem.dictionary = dictionary;
problem.all_target = all_target;
problem.all_gamma = all_gamma;

is_origin_least = mod(instruction_code, 10);
if is_origin_least
    'origin_least'
    file_name = [working_folder data_type ...
        '/SLEP_' ...
        num2str(idx_start_batch) ...
        '_' ...
        num2str(idx_end_batch)];
    make_valid(file_name);

    result = evaluate_lasso(problem, @direct_solve);
    save_evaluated(file_name, result);
end

% l1ls_featuresign

%%
is_siso = mod(floor(instruction_code / 10), 10);
if is_siso
    %% original SISO
    'SISO'
     file_name = [working_folder data_type ...
        '/SISO_' ...
        num2str(idx_start_batch) ...
        '_' ...
        num2str(idx_end_batch)];
    make_valid(file_name);

    result = evaluate_lasso(problem, ...
        @(para1, para2, para3)lasso_siso(para1, para2, para3, @abs_nn_brute_force));
    save_evaluated(file_name, result);
end

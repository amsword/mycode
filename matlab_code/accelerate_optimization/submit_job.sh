#!/bin/bash
# cd /home/jianfeng/mycode/matlab_code/accelerate_optimization

all_para1=(0);
length1=${#all_para1[@]};
all_machine=(10);
all_para2=(0);
length2=${#all_para2[@]};
m=0;
for ((i=0; i<${length1}; i++))
do
#para1=${all_para1[i]};
	for ((j=0; j<${length2}; j++))
	do
		para2=${all_para2[j]};
		job_name=job${para1}_${para2};
		out_file=result${para1}_${para2}_${all_machine[m]}.out
		rm ${out_file}
		qsub -N ${job_name} -cwd -l hostname=lv-c${all_machine[m]} -o ${out_file} -j y start_matlab.sh ${para1} ${para2}
		let m=${m}+1;
	done
done

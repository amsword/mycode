addpath('C:\Users\a0092558\Documents\MATLAB');
root = 'D:\Projects\SLEP';
addpath(genpath([root '/SLEP']));

cd D:/Projects/SparseProblem;
% -----------------------------options---------------------
% max number of expansion set
K = 100;
% weight of threshold
alpha = 0.002;
lamda = 0.08;
root = 'D:\DataSet\NUS_WID_Low_Level_Features\Low_Level_Features\';
% ----------------------Read trainging data----------------------
% fprintf('read training data...\n');
A = [];
trainFileList = dir(strcat(root,'Train*.dat'));
for fileIndex = 1:size(trainFileList)
    file = trainFileList(fileIndex);
    fprintf('file name: %s\n', file.name);
    data = load(strcat(root,file.name));
    A = [A; data'];
end
normalization
for i=1:size(A,2)
    A(:,i) = A(:,i) / norm(A(:,i));
end
% ----------------------Read testing data-------------------------
% fprintf('read testing data...\n');
% testY = [];
% testFileList = dir(strcat(root,'Test*.dat'));
% for fileIndex = 1:size(testFileList)
%     file = testFileList(fileIndex);
%     fprintf('file name: %s\n', file.name);
%     data = load(strcat(root,file.name));
%     testY = [testY; data'];
% end
% testNumb = size(testY,2);
% fprintf('size: %d\n', testNumb);
% normalization
% for i = 1:testNumb
%     testY(:,i) = testY(:,i) / norm(testY(:,i));
% end
% ---------------------Read Training Tags---------------------
% fprintf('read training tags data...\n');
root = 'D:\DataSet\Groundtruth\TrainTestLabels\';
trainTags = [];
trainFileList = dir(strcat(root,'*Train.txt'));
for fileIndex = 1:size(trainFileList)
    file = trainFileList(fileIndex);
    fprintf('file name: %s\n', file.name);
    data = load(strcat(root,file.name));
    trainTags = [trainTags data];
end
trainTags = trainTags';
% ----------------------Create Hash-----------------------------
fprintf('prepare for hash table...\n');
trainFile = 'D:\DataSet\NUS-WIDE-Lite\NUS-WIDE-Lite_features\train.txt';
testFile = 'D:\DataSet\NUS-WIDE-Lite\NUS-WIDE-Lite_features\test.txt';
% dlmwrite(trainFile, A', '\t');
% dlmwrite(testFile, testY', '\t');
fprintf('create hash table...\n');
% hash = LSHMain(size(A,2), 100, size(A,1), 0.90, 1.04, 5368709120, trainFile, testFile);
%----------------------Run SISO----------------------------------
tol = 1e-4;
resultHashSISO = zeros(81,testNumb);
valueHashSISO = zeros(1,testNumb);
timeHashSISO = zeros(1,testNumb);
ReCoff = cell(1,testNumb);

total = tic;
for i = 1:testNumb
    if mod(i, 100) == 0
        fprintf('image %d\n', i);
    end
    %------------------------Generate target----------------------------
    y = testY(:,i);
    %------------------------options for Lasso--------------------------
    opts=[];
    % Starting point
    opts.init= 1;        % x0 are defined
    % termination criterion
    opts.tFlag = 1;       % until the relative difference is below a threhold
    % normalization
    opts.nFlag=0;       % without normalization
    % regularization
    opts.rFlag=0;       % equals the input value
    opts.mFlag=0;       % treating it as compositive function
    opts.lFlag=0;       % Nemirovski's line search
    opts.tFlag = 1;
    opts.tol = tol;
    %calculate \gamma
    g0 = abs(A'*y);
    gamma = alpha*max(g0);
    %--------------------------Hash SISO------------------------------
    %--------------------------Active Variable Set---------------------
    tag = tic;
    VS = [];                      %the active variable set
    x = zeros(size(A,2),1);               % start from the zero point

        if size(VS,1) == 0
            r = y;
        else
            r = A(:,VS)*x(VS)-y;
        end
        r = r / norm(r);

        if mod(i, 100) == 0
            nnlsh = LSHSearch(hash, r, 11);
        else
            nnlsh = LSHSearch(hash, r, 1);
        end

        nnlshI = find(nnlsh ~= 0);
        nnlsh = nnlsh(nnlshI);

        g = A(:,nnlsh)'*r;

        ag = abs(g);
        [Y,I] = sort(ag,1,'descend');
        %already the solution of original problem
%         if Y(1)<= gamma
%             break;
%         end;

        % select active variables
        Index = find(Y>=gamma); %expansion set
        pn = size(Index,1);
        if pn>K
            pn = K;
        end;
        NV = nnlsh(I(1:pn));

        % current working set
        oldVS = VS;
        n1 = size(VS,2);
        VS = union(VS,NV);
        n2 = size(VS,2);
%         if n1==n2
%             break;
%         end;

        
        % solve subproblem
        tx0 = x(VS);
        tA  = A(:,VS);
        opts.x0 = tx0;
        [tx, tfunVal]= LeastR(tA, y, lamda , opts);
    % end;
    
    sn = size(VS,2);
     RC = zeros(sn,2);
     if sn > 0
     RC(:,1) = VS;
     RC(:,2) = x(VS);
     end
     ReCoff{i} = RC;
    resultHashSISO(:,i) = trainTags(:,VS) * tx;
    valueHashSISO(i) = tfunVal(end);
    timeHashSISO(i) = toc(tag);
    if mod(i, 100) == 0
        fprintf('\tHash-SISO time: %d\n', sum(timeHashSISO)/i);
        fprintf('\tCost function: %f\n', tfunVal(end));
    end   
end;
totalTime = toc(total);
% ---------------------Read Testing Tags----------------------
fprintf('read testing tags data...\n');
fprintf('read training tags data...\n');
root = 'D:\DataSet\Groundtruth\TrainTestLabels\';
testTags = [];
testFileList = dir(strcat(root,'*Test.txt'));
for fileIndex = 1:size(testFileList)
    file = testFileList(fileIndex);
    fprintf('file name: %s\n', file.name);
    data = load(strcat(root,file.name));
    testTags = [testTags data];
end
% -------------------Evaluate the Result----------------------
prehash = [];
for i = 1:size(testTags,2)
    gt = testTags(1:testNumb,i);
    prehash(i)=eval_ap(resultHashSISO(i,:),gt,1);
end
disp(mean(prehash));
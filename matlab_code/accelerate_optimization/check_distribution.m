function y = check_distribution(idx_start, idx_end, ...
    all_query, query_gamma, ock_model, ock_codes, ...
    bound, dictionary)

sub_query = all_query(:, idx_start : idx_end);
if (ock_model.is_optimize_R)
    sub_query = ock_model.R' * sub_query;
end

[sub_query, how_long] = normalize_squared_l2(sub_query);
sub_gamma = query_gamma(idx_start : idx_end);
num_partitions = numel(ock_model.all_D);
s_app = 0;
idx_dim_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    idx_dim_end = size(subD, 1) + idx_dim_start - 1;
    lookups = sub_query(idx_dim_start : idx_dim_end, :)' * subD;
    s_app = s_app + lookups(:, double(ock_codes(i, :)) + 1);
    idx_dim_start = idx_dim_end + 1;
end
s_real = sub_query' * dictionary;
s_diff = s_real - s_app;
y = bsxfun(@rdivide, s_diff, bound);




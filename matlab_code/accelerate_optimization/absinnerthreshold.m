function y = absinnerthreshold(...
    idx_start, idx_end, all_query, query_gamma, dictionary)

r = all_query(:, idx_start : idx_end)' * dictionary;
r = abs(r);
y = bsxfun(@ge, r, query_gamma(idx_start : idx_end));
y = sparse(y);
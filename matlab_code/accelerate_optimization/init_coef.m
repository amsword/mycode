function [active_index, curr_coef] = init_coef(num_words, percent_non_zeros)

num_non_zeros = ceil(num_words * percent_non_zeros);
active_index = randperm(num_words, num_non_zeros)';
curr_coef = randn(num_non_zeros, 1);

curr_coef = curr_coef / norm(curr_coef);
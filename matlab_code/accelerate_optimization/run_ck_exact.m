opt_input = [];
tol = 10^-4;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;

file_name_ock_model = generate_task_save_file('ock_model', ...
    {opt_input_ock.num_partitions, opt_input_ock.sub_dic_size_each_partition});
x = load(file_name_ock_model);
distance_info = x.ock_model;
distance_info.ock_codes = x.ock_codes;
distance_info.ock_bounds = x.ock_bounds;


file_name_bound_ck_linear_exact = generate_task_save_file('bound_exact', ...
    {opt_input_ock.num_partitions, opt_input_ock.sub_dic_size_each_partition, ...
    idx_start_batch, idx_end_batch});

%%
[nn_idx, info] = bound_exact(problem.all_target, problem.dictionary, distance_info);

% clear_dll;

%%
save(file_name_bound_ck_linear_exact, ...
    'nn_idx', 'info', ...
    'machine_name', ...
    '-v7.3');



function y = ockabsinner(...
    idx_start, idx_end, all_query, ock_codes, ock_model, gnd, database)

sub_query = all_query(:, idx_start : idx_end);
sub_gnd = gnd(idx_start : idx_end, :);
if ock_model.is_optimize_R
    sub_query = ock_model.R' * sub_query;
end

num_query = size(sub_query, 2);
num_database_point = size(ock_codes, 2);

num_partitions = numel(ock_model.all_D);

s_app = 0;
idx_dim_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    idx_dim_end = size(subD, 1) + idx_dim_start - 1;
    lookups = sub_query(idx_dim_start : idx_dim_end, :)' * subD;
    s_app = s_app + lookups(:, double(ock_codes(i, :)) + 1);
    idx_dim_start = idx_dim_end + 1;
end

s_true = (all_query(:, idx_start : idx_end))' * database;
s_diff = s_true - s_app;
s_sqdiff = s_diff .^ 2;
relative_error = s_sqdiff ./ s_true .^ 2;
y.error = sum(relative_error(:)) / size(s_true, 2);


s_app = abs(s_app);
[s2, s_idx] = sort(s_app, 2, 'descend');

all_indicator = (s_idx - 1) * num_query + repmat((1 : num_query)', 1, num_database_point);
all_indicator = sub_gnd(all_indicator);


all_ap = zeros(num_query, 1);
for i = 1 : num_query
    d = find(all_indicator(i, :));
    n = numel(d);
    ap = sum((1 : n) ./ d);
    all_ap(i) = ap / n;
end
y.all_ap = all_ap;



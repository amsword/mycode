file_name_global_ock_model = generate_task_save_file(...
    'global_ock_model', {num_coarse_cluster, ...
    is_optimize_R, ...
    num_partitions, ...
    sub_dic_size_each_partition});

if exist(file_name_global_ock_model, 'file')
    warning(['no need ' file_name_global_ock_model]);
%     return;
end


file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);

x = load(file_name_coarse_k_means);
if strcmp(data_type, 'SIFT1M') || ...
        strcmp(data_type, 'GIST1M') || ...
        strcmp(data_type, 'ImageNet')
    coarse_centers = x.coarse_centers;
    coarse_idx_training = x.coarse_idx;
    coarse_idx = x.coarse_idx;
elseif strcmp(data_type, 'Tiny80M')
    coarse_centers = x.coarse_centers;
    coarse_idx = x.coarse_idx;
    coarse_idx_training = x.coarse_idx_training;
else
    error('not supported');
end
%%


clear opt_input_ock;
opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;
opt_input_ock.is_optimize_R = is_optimize_R;

%% training
x = [];
if exist(file_name_global_ock_model, 'file')
    x = load(file_name_global_ock_model);
end

prepare_training;

if isfield(x, 'global_ock_model')
    global_ock_model = x.global_ock_model;
else
    Xtraining = bsxfun(@minus, Xtraining, coarse_centers(:, coarse_idx_training));

    global_ock_model = ock_training(Xtraining, opt_input_ock);
    save(file_name_global_ock_model, 'global_ock_model', '-v7.3');
end

clear coarse_idx_training
clear x;

%% indexing
if strcmp(data_type, 'SIFT1M') || ...
        strcmp(data_type, 'GIST1M') || ...
        strcmp(data_type, 'ImageNet')
    Xbase = Xtraining;
elseif strcmp(data_type, 'Tiny80M')
    prepare_base;
    coarse_idx = load(file_name_coarse_k_means);
    coarse_idx = coarse_idx.coarse_idx;
    num_base = size(Xbase, 2);
    assert(num_base == 10^7);
    batch_size = 10^6;
    batch_num = num_base / batch_size;
    for idx_batch = 1 : batch_num
        idx_start = 1 + (idx_batch - 1) * batch_size;
        idx_end = idx_start + batch_size - 1;
        Xbase(:, idx_start : idx_end) = bsxfun(...
            @minus, Xbase(:, idx_start : idx_end), coarse_centers(:, coarse_idx(idx_start : idx_end)));
    end
else
    error('dfs');
end

global_ock_codes = ock_encoding(global_ock_model, Xbase);
global_ock_bounds = ock_sq_errors(global_ock_model, Xbase, global_ock_codes);
global_ock_bounds = sqrt(global_ock_bounds);

assert(isfloat(coarse_idx));
all_ock_codes = cell(num_coarse_cluster, 1);
all_ock_bound = cell(num_coarse_cluster, 1);

for i = 1 : num_coarse_cluster
    idx = (coarse_idx == i);
    all_ock_codes{i} = global_ock_codes(:, idx);
    all_ock_bound{i} = global_ock_bounds(idx);
end
file_name_global_ock_model
save(file_name_global_ock_model, ...
    'global_ock_codes', 'global_ock_bounds', ...
    'all_ock_bound', 'all_ock_codes', '-v7.3', '-append');

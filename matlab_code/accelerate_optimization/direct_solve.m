function [active_idx, active_coef, obj, opts_origin] = ...
    direct_solve(dictionary, target, gamma, opt_direct_solve)

if isfield(opt_direct_solve, 'tol')
    tol = opt_direct_solve.tol;
else
    tol = 10^-4;
end

opts_origin = [];
opts_origin.init = 2; % all zeros
opts_origin.tFlag = 1;
opts_origin.tol = tol;
% percent_non_zeros = 0.1;
zeros_threshold = 10^-10;

% num_words = size(dictionary, 2);
% [active_index, curr_coef] = init_coef(num_words, percent_non_zeros);

% opts_origin.x0 = zeros(num_words, 1);
% opts_origin.x0(active_index) = curr_coef;

[coef, obj] = LeastR(dictionary, target, gamma, opts_origin);
opts_origin.iter = numel(obj);
obj = obj(end);
active_idx = find(coef > zeros_threshold);
active_coef = coef(active_idx);

function all_gamma = compute_gamma(...
    dictionary, all_target, alpha)

max_num_point = 10^7;

num_target = size(all_target, 2);

batch_size = max_num_point / size(dictionary, 2);
batch_size = round(batch_size);
batch_size = min(batch_size, num_target);
batch_size = max(1, batch_size);

batch_num = ceil(num_target / batch_size) + 1;

all_gamma = zeros(num_target, 1);

for i = 1 : batch_num
    idx_start = 1 + (i - 1) * batch_size;
    if idx_start > num_target
        break;
    end
    idx_end = i * batch_size;
    idx_end = min(idx_end, num_target);
    
    sub_all_target = all_target(:, idx_start : idx_end);
    all_gamma(idx_start : idx_end) = alpha * max(abs(dictionary' * sub_all_target), [], 1);
end
function r = load_siso(file_name)

siso = load(file_name, 'all_time', 'all_obj', 'all_opt_info');

num_samples = numel(siso.all_opt_info);
ratio = zeros(num_samples, 1);
ratio_all = zeros(num_samples, 1);
for i = 1 : num_samples
    ratio(i) = sum(siso.all_opt_info{i}.time_k_nn) / sum(siso.all_opt_info{i}.time_leastR);
    ratio_all(i) = sum(siso.all_opt_info{i}.time_k_nn) / siso.all_time(i);
end

r.ratio = ratio;
r.ratio_all = ratio_all;
r.all_obj = siso.all_obj;
r.all_time = siso.all_time;
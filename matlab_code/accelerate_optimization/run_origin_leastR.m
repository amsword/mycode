opt_input = [];
tol = 10^-4;
opt_input.tol = tol;

prepare_test;
prepare_base;
all_gamma = zeros(size(Xtest, 2), 1); 
all_gamma(:) = 0.1;

file_name = generate_task_save_file('origin_leastR', ...
    {[working_folder data_type], tol, idx_start_batch, idx_end_batch});

make_valid(file_name);

problem.dictionary = Xbase;
problem.all_target = Xtest;
problem.all_gamma = all_gamma;

result = evaluate_lasso(problem, ...
    @(p1, p2, p3)direct_solve(p1, p2, p3, opt_input));
result.opt_input = opt_input;
save_evaluated(file_name, result);

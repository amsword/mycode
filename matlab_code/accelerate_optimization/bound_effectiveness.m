function y = bound_effectiveness(idx_start, idx_end, ...
    all_query, query_gamma, ock_model, ock_codes, bound)

sub_query = all_query(:, idx_start : idx_end);
if (ock_model.is_optimize_R)
    sub_query = ock_model.R' * sub_query;
end

[sub_query, how_long] = normalize_squared_l2(sub_query);
sub_gamma = query_gamma(idx_start : idx_end);
sub_gamma = sub_gamma ./ how_long';
num_partitions = numel(ock_model.all_D);
s_app = 0;
idx_dim_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    idx_dim_end = size(subD, 1) + idx_dim_start - 1;
    lookups = sub_query(idx_dim_start : idx_dim_end, :)' * subD;
    s_app = s_app + lookups(:, double(ock_codes(i, :)) + 1);
    idx_dim_start = idx_dim_end + 1;
end
s_app = abs(s_app);

real_bound = bsxfun(@minus, s_app, bound); 
real_good = bsxfun(@gt, real_bound, sub_gamma);
y = sum(real_good, 2);

real_bound = bsxfun(@plus, s_app, bound);
real_good = bsxfun(@lt, real_bound, sub_gamma);
y = y + sum(real_good, 2);



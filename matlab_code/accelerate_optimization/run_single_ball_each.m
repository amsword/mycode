clear distance_info;
distance_info.max_leaf_size = max_leaf_size;
distance_info.is_approximate = is_approximate;
distance_info.num_max_real_computed = tree_num_max_real_computed;

%
[nn_idx, info] = single_ball_exact(...
    Xtest, Xbase, distance_info);

if is_approximate
    file_name = generate_task_save_file('linear_largest_inner_product', ...
        {idx_start_batch, idx_end_batch});
    x = load(file_name);
    acc = sum(x.nn_idx == nn_idx) / numel(nn_idx);
end

%
file_name_inverted_list_exact = generate_task_save_file(...
    'single_ball_exact', ...
    {max_leaf_size, idx_start_batch, idx_end_batch, ...
    is_approximate, tree_num_max_real_computed});

save(file_name_inverted_list_exact, ...
    'nn_idx', 'info', 'machine_name', ...
    '-v7.3');

if is_approximate
    evaluate_top1;
    save(file_name_inverted_list_exact, ...
        'acc', '-v7.3', '-append');
end
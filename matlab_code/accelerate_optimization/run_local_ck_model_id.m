if process_id == 0
    idx_start = 1;
else
    idx_start = (process_id - 1) * process_size + 1;
end

if idx_start > num_coarse_cluster
    return;
end
if process_id == 0
    idx_end = num_coarse_cluster;
else
    idx_end = idx_start + process_size - 1;
    if idx_end > num_coarse_cluster
        idx_end = num_coarse_cluster;
    end
end

%%
file_name_batch = generate_task_save_file('batch', ...
    {process_id, file_name_local_ock_model});

all_ock_model = cell(num_coarse_cluster, 1);
all_ock_codes = cell(num_coarse_cluster, 1);
all_ock_bound = cell(num_coarse_cluster, 1);

is_model_ready = false;
is_codes_ready = false;
if exist(file_name_batch, 'file')
    x = load(file_name_batch);
    if isfield(x, 'all_ock_model')
        is_model_ready = true;
        all_ock_model = x.all_ock_model;
    end
    if isfield(x, 'all_ock_codes')
        is_codes_ready = true;
        all_ock_codes = x.all_ock_codes;
    end
end
%%
for i = idx_start : idx_end
    sub_idx = double(all_inverted_idx{i}) + 1;
    subXtraining = Xtraining(:, sub_idx);
    subXtraining = bsxfun(@minus, subXtraining, coarse_centers(:, i));
    if ~is_model_ready
        all_ock_model{i} = ock_training(subXtraining, opt_input_ock);
    end
%     if ~is_codes_ready
        all_ock_codes{i} = ock_encoding(all_ock_model{i}, subXtraining);
%     end
    all_ock_bound{i} = ock_sq_errors(all_ock_model{i}, subXtraining, all_ock_codes{i});
    assert(abs(all_ock_model{i}.obj(end) / numel(sub_idx) - mean(all_ock_bound{i})) < 10^-4);
    all_ock_bound{i} = sqrt(all_ock_bound{i});
end

save(file_name_batch, 'all_ock_model', 'all_ock_codes', 'all_ock_bound');


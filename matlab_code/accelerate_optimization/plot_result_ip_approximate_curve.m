clear inverted_time
clear inverted_acc
k = 1;

for num_real_calculated = [[1 : 9], [10 : 10 : 40]]
    file_name = ['inverted_list_exact_1024_1_16_256_approximate' ...
        num2str(num_real_calculated) '_1_10000.mat'];
    if ~exist(file_name, 'file')
        continue;
    end
    x = load(file_name);
    assert(x.info{4}.id == 0);
    inverted_time(k) = x.info{4}.time_cost / numel(x.nn_idx);
    inverted_acc(k) = x.acc;
    k = k + 1;
end

semilogy(inverted_acc, inverted_time, 'r-o', ...
    'linewidth', line_width);
hold on;

% plot
clear tree_time
clear tree_acc
k = 1;

for num_real_calculated = [10^4 * [1 : 2: 10], 10^5 * [2 : 3]]
    file_name = ['single_ball_exact_20_' num2str(num_real_calculated) ...
        '_1_10000.mat'];
    if ~exist(file_name, 'file')
        continue;
    end
    x = load(file_name);
    assert(x.info{2}.id == 0);
    tree_time(k) = x.info{2}.time_cost / numel(x.nn_idx);
    tree_acc(k) = x.acc;
    k = k + 1;
end

plot(tree_acc, tree_time, 'b-*', ...
    'linewidth', line_width);

legend('QIP', 'SB', 'Location', 'Best');

grid on;
set(gca, 'FontSize', gca_font_size);

xlabel('Recall', 'FontSize', gca_font_size);
ylabel('Query time (second)', 'fontsize', gca_font_size);

saveas(gca, [result_folder '/' data_type '_app.eps'], 'psc2');

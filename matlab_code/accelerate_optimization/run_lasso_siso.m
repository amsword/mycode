 
opt_input = [];
tol = 10^-4;
num_init_non_zeros = 100;

opt_input.tol = tol;
opt_input.num_init_non_zeros = num_init_non_zeros;
opt_input.is_save_query = 1;

file_name = generate_task_save_file(...
    'lasso_siso', ...
    {[working_folder data_type], tol, idx_start_batch, idx_end_batch});

file_collect = generate_task_save_file('collect_query', ...
    {[working_folder data_type], tol, idx_start_batch, idx_end_batch});

make_valid(file_name);

func_siso = @(para1, para2, para3)lasso_siso(...
    para1, ...
    para2, ...
    para3, ...
    @abs_nn_brute_force, ...
    opt_input);

prepare_test;
prepare_base;
problem.dictionary = Xbase;
problem.all_target = Xtest(:, idx_start_batch : idx_end_batch);
all_gamma = zeros(size(problem.all_target, 2), 1);
all_gamma(:) = 0.1;
problem.all_gamma = all_gamma;

% [Xout, obj] = w_l1ls_featuresign(Xbase, Xtest(:, 1), 0.1);

result = evaluate_lasso(problem, func_siso);
result.opt_input = opt_input;

save_evaluated(file_name, result);


% save the query;

num_query = numel(result.all_time);
dim = size(result.all_opt_info{1}.all_query, 1);

s = 0;
for i = 1 : num_query
    s = s + size(result.all_opt_info{i}.all_query, 2);
end
all_query = zeros(dim, s);
query_gamma = zeros(s, 1);

idx_start = 1;
for i = 1 : num_query
    t = result.all_opt_info{i}.all_query;
    n = size(t, 2);
    idx_end = idx_start + n - 1;
    all_query(:, idx_start : idx_end) = t;
    query_gamma(idx_start : idx_end) = all_gamma(i);
    idx_start = idx_end + 1;
end

%
save(file_collect, 'query_gamma', 'all_query', '-v7.3');

evaluate_imagenet;
save(file_name, 'scemantic_acc', '-append');

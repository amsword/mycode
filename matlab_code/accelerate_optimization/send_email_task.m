fs = fieldnames(all_task);

num_fs = numel(fs);

str = [];
for i = 1 : num_fs
    v = getfield(all_task, fs{i});
    if v
        str = [str fs{i}];
    end
end

sendmail_mine(str);
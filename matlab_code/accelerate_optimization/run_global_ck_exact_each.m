file_name_inverted_list_exact = generate_task_save_file('inverted_list_exact', ...
    {num_coarse_cluster, is_optimize_R, num_partitions, ...
    sub_dic_size_each_partition, ...
    is_approximate, max_num_inverted_list, ...
    idx_start_batch, idx_end_batch});

if exist(file_name_inverted_list_exact, 'file')
    warning(['file exist ' file_name_inverted_list_exact]);
    return;
end


opt_input = [];
tol = 10^-4;
num_init_non_zeros = 100;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;
opt_input.num_init_non_zeros = num_init_non_zeros;

%% load coarse quantization
file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);

coarse_model = load(file_name_coarse_k_means, 'coarse_centers', ...
    'all_inverted_idx', 'bound_center');

%% load fine quantization
file_name_local_ock_model = generate_task_save_file(...
    'global_ock_model', ...
    {num_coarse_cluster, is_optimize_R, num_partitions, sub_dic_size_each_partition});

fine_model = load(file_name_local_ock_model, ...
    'global_ock_model', 'all_ock_codes', 'all_ock_bound');

clear distance_info;
distance_info.coarse_model = coarse_model;
distance_info.fine_model = fine_model;
distance_info.is_approximate = is_approximate;
distance_info.max_num_inverted_list = max_num_inverted_list;
%%
[nn_idx, info] = inverted_list_exact(...
    Xtest, Xbase, distance_info);
%
save(file_name_inverted_list_exact, 'nn_idx', 'info', 'machine_name', '-v7.3');

if is_approximate
    evaluate_top1;
   
    save(file_name_inverted_list_exact, ...
        'acc', '-v7.3', '-append');
end

function [nn_idx, nn_time_details] = global_ock_one_query(...
    query, database, gamma, max_expanded, ...
    distance_info)

distance_info.gamma = gamma;
distance_info.top_num = max_expanded;

[nn_idx, nn_time_details] = ...
    mexBoundBasedExact(4, query, database, distance_info);

end

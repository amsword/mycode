line_width = 2;
gca_font_size = 14;
result_folder = 'D:\OneDrive\Documents\Research\OnGoing\NUS\IEEEtran\figs';

%%
plot_result_ip_approximate_curve;

%%

for num_coarse_cluster = [1024 2048 4096]
    for sub_dic_size_each_partition = [256 512 1024 2048]
        
        file_name_inverted_list_exact = generate_task_save_file(...
            'inverted_list_exact', ...
            {num_coarse_cluster, is_optimize_R, num_partitions, ...
            sub_dic_size_each_partition, ...
            is_approximate, max_num_inverted_list, ...
            idx_start_batch, idx_end_batch});
        if ~exist(file_name_inverted_list_exact, 'file')
            warning(file_name_inverted_list_exact);
            continue;
        end
%         x = load(file_name_inverted_list_exact);
    end
end


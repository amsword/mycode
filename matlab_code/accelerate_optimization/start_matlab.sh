#!/bin/bash
 
cd /home/jianfeng/mycode/matlab_code/accelerate_optimization
pwd


export LD_LIBRARY_PATH=/home/jianfeng/mycode/c_code/bin

matlab -nodisplay -nodesktop -nosplash -r "accelerate_optimization_main(${1}, ${2})"

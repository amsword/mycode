function [final_active_set, final_active_coef, obj, opt_info] ...
    = lasso_siso(...
    dictionary, ...
    target, gamma, ...
    func_abs_nn, ...
    opt_input)

if isfield(opt_input, 'num_init_non_zeros')
    num_init_non_zeros = opt_input.num_init_non_zeros;
else
    num_init_non_zeros = 0.1 * size(dictionary, 2);
end
if isfield(opt_input, 'max_expanded')
    max_expanded = opt_input.max_expanded;
else
    max_expanded = 150;
end
if isfield(opt_input, 'max_iter')
    max_iter = opt_input.max_iter;
else
    max_iter = 100;
end

if isfield(opt_input, 'tol')
    tol = opt_input.tol;
else
    tol = 10^-4;
end

if isfield(opt_input, 'init')
    init = opt_input.init;
else
    init = 0; % random based
end

if isfield(opt_input, 'is_save_query')
    is_save_query = opt_input.is_save_query;
else
    is_save_query = 0;
end

%%

zeros_threshold = 10^-10;
% opts
opts_sisoLeastR=[];
opts_sisoLeastR.init = 1;        % starting from previous results
opts_sisoLeastR.tFlag = 1;       % until the relative difference is below a threhold
opts_sisoLeastR.tol = tol;

opt_info.num_init_non_zeros = num_init_non_zeros;
opt_info.max_expanded = max_expanded;
opt_info.zeros_threshold = zeros_threshold;
opt_info.max_iter = max_iter;
opt_info.opts_sisoLeastR = opts_sisoLeastR;

num_words = size(dictionary, 2);

time_leastR = zeros(max_iter, 1);
time_shrink = 0;
 time_k_nn = zeros(max_iter, 1);
time_generate_query = 0;

assert(init == 2);

tic;
if init == 0
    [active_index, curr_coef] = init_coef_abs(num_words, num_init_non_zeros);
elseif init == 2
    [active_index] = func_abs_nn(target, ...
        dictionary, gamma, max_expanded);
    curr_coef = zeros(numel(active_index), 1);
else
    error('not supported');
end
time_init = toc;

if is_save_query
    all_query = zeros(size(dictionary, 1), max_iter);
end

%% solve the sub problem
stop_type = 'max_iter_reached';
all_obj = zeros(max_iter, 1);
for iter = 1 : max_iter
    sub_dictionary = dictionary(:, active_index);
    
    tic;
    opts_sisoLeastR.x0 = curr_coef;
    [sub_opt_coef, sub_opt_obj] = LeastR(...
        sub_dictionary, target, gamma, opts_sisoLeastR);
    x = toc;
    time_leastR(iter) = x;
    
    all_obj(iter) = sub_opt_obj(end);
    
    tic;
    non_zeros_idx_in_active_set = abs(sub_opt_coef) > zeros_threshold;
    shrinked_active_index = active_index(non_zeros_idx_in_active_set);
    shrinked_curr_coef = sub_opt_coef(non_zeros_idx_in_active_set);
    time_shrink = time_shrink + toc;

    tic;
    obj_diff = sub_dictionary * sub_opt_coef - target;
    time_generate_query = time_generate_query + toc;
    
    all_query(:, iter) = obj_diff;
    
    tic;
    [adopted_expanded_idx] = func_abs_nn(obj_diff, dictionary, gamma, max_expanded);
    time_k_nn(iter) = toc;
    robust_expanded_idx = setdiff(adopted_expanded_idx, shrinked_active_index);
    
    if isempty(robust_expanded_idx)
        stop_type = 'no_expanded';
        break;
    end
    active_index = [robust_expanded_idx; shrinked_active_index];
    curr_coef = [zeros(numel(robust_expanded_idx), 1); shrinked_curr_coef];
end
obj = all_obj(1 : iter);

non_zeros_idx_in_active_set = abs(sub_opt_coef) > zeros_threshold;
final_active_set = active_index(non_zeros_idx_in_active_set);
final_active_coef = curr_coef(non_zeros_idx_in_active_set);

opt_info.stop_type = stop_type;
opt_info.iter = iter;

opt_info.time_init = time_init;
opt_info.time_leastR = time_leastR(1 : iter);
opt_info.time_shrink = time_shrink;
opt_info.time_k_nn = time_k_nn(1 : iter);
opt_info.time_generate_query = time_generate_query;
if is_save_query
    opt_info.all_query = all_query(:, 1 : iter);
end

if exist('Xtest', 'var')
    return;
end

clear data_required;
if strcmp(data_type, 'Tiny80M')
    data_required.idx_start_test = tiny80m_idx_start_test;
    data_required.idx_end_test = tiny80m_idx_end_test;
elseif strcmp(data_type, 'SIFT1M') || ...
        strcmp(data_type, 'GIST1M') || ...
        strcmp(data_type, 'ImageNet')

    data_required.is_test = 1;
    data_required.is_base = 0;
    data_required.is_training = 0;
    data_required.is_data_normalize = is_data_normalize;
    if strcmp(data_type, 'ImageNet')
        data_required.is_data_normalize = 1;
    end
else
    error('dfs');
end

loaded_data = load_data(data_folder, data_type,data_required);
Xtest = loaded_data.test;
clear loaded_data;
function y = reduce_absinnerthreshold(all_indicator)

num_batch = numel(all_indicator);

num_non_zeros = 0;

num_rows = 0;
for i = 1 : num_batch
    ind = all_indicator{i};
    num_rows = num_rows + size(ind, 1);
    num_non_zeros = num_non_zeros + sum(ind(:));
end
num_cols = size(ind, 2);

all_is = zeros(num_non_zeros, 1);
all_js = zeros(num_non_zeros, 1);

idx_first_row = 1;
idx1 = 1;
for i = 1 : num_batch
    ind = all_indicator{i};
    [is, js] = find(ind);
    n = numel(is);
    idx2 = idx1 + n - 1;
    
    all_is(idx1 : idx2) = is + idx_first_row - 1;
    all_js(idx1 : idx2) = js;
    idx1 = idx2 + 1;
    idx_first_row = idx_first_row + size(ind, 1);
end

y = sparse(all_is, all_js, true, num_rows, num_cols);


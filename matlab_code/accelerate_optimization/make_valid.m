function make_valid(file_name)

if ~exist(file_name, 'file')
    x1 = strfind(file_name, '/');
    if isempty(x1)
        x1 = -1;
    end
    x2 = strfind(file_name, '\');
    if isempty(x2)
        x2 = -1;
    end
    x = max(x1(end), x2(end));
    
    target_dir = file_name(1 : x);
    
    if ~exist(target_dir, 'dir')
        mkdir(target_dir);
    end
    empty_matrix = [];
    save(file_name, 'empty_matrix');
end
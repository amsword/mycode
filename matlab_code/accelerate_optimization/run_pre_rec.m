tol = 10^-4;
num_init_non_zeros = 100;
init = 0; % 0: random based, 2: nn-search and then all zeros.
working_folder;
data_type;
idx_start_batch;
idx_end_batch;

file_name_pre_rec = generate_task_save_file('pre_rec', ...
    {num_partitions, sub_dic_size_each_partition});

% if exist(file_name_pre_rec, 'file')
%     x = load(file_name_pre_rec);
%     if isfield(x, 'all_ap') && ...
%             isfield(x, 'mean_ap') && ...
%             isfield(x, 'query_error')
%         return;
%     end
% end

file_collect = generate_task_save_file('collect_query', ...
    {[working_folder data_type], tol, init, num_init_non_zeros, idx_start_batch, idx_end_batch});
x = load(file_collect);
all_query = x.all_query;
query_gamma = x.query_gamma;

num_query = size(all_query, 2);
% ground truth
file_name_gnd = generate_task_save_file('gnd');
if ~exist(file_name_gnd, 'file')
    gnd = batch_map(1, num_query, 100, ...
        @(idx1, idx2)absinnerthreshold(idx1, idx2, all_query, query_gamma, problem.dictionary));
    gnd = reduce_absinnerthreshold(gnd);
    save(file_name_gnd, 'gnd');
else
    x = load(file_name_gnd);
    gnd = x.gnd;
end
%
file_name_ock_model = generate_task_save_file('ock_model', ...
    {num_partitions, sub_dic_size_each_partition});
x = load(file_name_ock_model);
ock_model = x.ock_model;
ock_codes = x.ock_codes;
%
y = batch_map(1, num_query, 100, ...
    @(idx1, idx2)ockabsinner(idx1, idx2, all_query, ...
    ock_codes, ock_model, gnd, problem.dictionary));

[all_ap, query_error] = reduce_ockabsinner(y);
mean_ap = mean(all_ap);

save(file_name_pre_rec, 'all_ap', 'mean_ap', 'query_error');
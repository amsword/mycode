function accelerate_optimization_main(...
    all_num_coarse_cluster, all_sub_dic_size_each_partition)
all_num_coarse_cluster = 2048;
all_sub_dic_size_each_partition = 1024;

all_num_partitions = 20;
num_partitions = all_num_partitions(1);
process_id = 0;
num_coarse_cluster = all_num_coarse_cluster(1);
sub_dic_size_each_partition = all_sub_dic_size_each_partition(1);

omega = 8;

num_ock_candidate = 5000;

ck_linear_type = 2; % 0, 1

is_optimize_R = true;

all_task.is_pre_rec = 0;
all_task.is_ck_model = 0;
all_task.is_local_ck_model = 0;

all_task.is_origin_least = 0;
all_task.is_ck_siso = 0;
all_task.is_siso = 0;
all_task.is_inverted_global_siso = 1;
all_task.is_local_ck_siso = 0;

all_task.is_coarse_clustering = 0;
all_task.is_global_ockmeans = 0;
all_task.is_debug = 0;

all_task.is_ck_exact = 0;
all_task.is_exhaustive_exact = 0;
all_task.is_global_ck_exact = 0;
all_task.is_single_ball = 0
is_approximate = 0;
all_tree_num_max_real_computed = [[1 : 9] * 10^5, [1 : 9] * 10^4]
tree_num_max_real_computed = all_tree_num_max_real_computed(1);
max_leaf_size = 20

all_max_num_inverted_list = [[10 : 10 : 100], 200, 400, 600]
max_num_inverted_list = all_max_num_inverted_list(1);
process_size = 10;
% debug_num_selected = 256;

max_iter_ock = 100;

idx_start_batch = 1;
idx_end_batch = 50;

alpha = 0.02;
% 
% data_type = 'SIFT1M'
% data_type = 'GIST1M'
% data_type = 'Tiny80M'
data_type = 'ImageNet';
tiny80m_idx_start_training = 1;
tiny80m_idx_end_training = 10^6;
tiny80m_idx_start_base = 1;
tiny80m_idx_end_base = 10^7;
tiny80m_idx_start_test = 10^7 + 1;
tiny80m_idx_end_test = 10^7 + 1000;
is_data_normalize = 1;
% data_type = 'NUS-WIDE-LITE';
% data_type = 'NUS-WIDE';
% data_type = 'DEBUG_DATA';
%
initialize_setup;
prepare_folder_name;

if strcmp(data_type, 'SIFT1M') && is_data_normalize
    cd([working_folder data_type '/normalize']);
else
    if strcmp(data_type, 'SIFT1M')
        cd([working_folder data_type '/inner_product']);
    else
        cd([working_folder data_type]);
    end
end
%%
prepare_data;

% 'finished loading data'



%% model

if all_task.is_coarse_clustering
    run_coarse_clustering;
end

if all_task.is_ck_model
    run_ck_model;
end

if all_task.is_local_ck_model
    run_local_ck_model;
end

if all_task.is_global_ockmeans
    run_global_ockmeans;
end


%% siso

if all_task.is_origin_least
    'origin_least'
    run_origin_leastR;
end

if all_task.is_siso
    'SISO'
    run_lasso_siso;
end

if all_task.is_inverted_global_siso
    run_inverted_global_siso;
end

if all_task.is_ck_siso
    % ck SISO
    'ck-means SISO'
    run_ck_siso;
end

if all_task.is_local_ck_siso
    run_local_ck_siso;
end

%%
if all_task.is_pre_rec
    run_pre_rec
end

if all_task.is_debug
    run_debug;
end


%% exact performance

if all_task.is_exhaustive_exact
    run_exhaustive_exact;
end
%
if all_task.is_ck_exact
    run_ck_exact;
end

if all_task.is_global_ck_exact
    run_global_ck_exact;
end

if all_task.is_single_ball
    run_single_ball;
end



%%
% plot_result_ip;
% analysis_result
% send_email_task;

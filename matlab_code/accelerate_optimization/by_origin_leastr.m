file_name = [working_folder data_type ...
    '/SLEP_' ...
    num2str(idx_start_batch) ...
    '_' ...
    num2str(idx_end_batch)];

make_valid(file_name);

num_test = size(all_target, 2);

last_time = smart_meassge_init();

num_words = size(dictionary, 2);

all_obj = zeros(num_test, 1);
all_active_coef = cell(num_test, 1);
all_active_idx = cell(num_test, 1);
all_opt_info = cell(num_test, 1);
all_time = zeros(num_test, 1);

for i = 1 : num_test
    last_time = smart_message(last_time, num2str(i));
    
    target = all_target(:, i);
    gamma = all_gamma(i);
    
    tic;
    [active_idx, active_coef, obj, opts_origin] = direct_solve(dictionary, target, gamma);
    curr_time = toc;
    
    all_obj(i) = obj(end);
    all_active_coef{i} = active_coef;
    all_active_idx{i} = active_idx;
    all_opt_info{i} = opts_origin;
    all_time(i) = curr_time;
end
save(file_name, 'all_obj', 'all_active_coef', 'all_active_idx', ...
    'all_time', 'all_opt_info', '-append');
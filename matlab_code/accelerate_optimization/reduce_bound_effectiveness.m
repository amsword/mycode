function y = reduce_bound_effectiveness(y)

num_batch = numel(y);

s = 0;
c = 0;
for i = 1 : num_batch
    c = c + numel(y{i});
    s = sum(y{i}) + s;
end
y = s / c;


%%
subZ = Z(:, 1 : 100);
save('matlab.mat', 'subZ', '-append');

%%
for k = 1 : 100
best_s = 10000;
k
for i = 1 : 256
    for j = 1 : 256
        word = sub_all_D{1}(:, i) + sub_all_D{1}(:, j + 256);
        s = norm(subZ(:, k) - word);
        if s < best_s
            best_s = s;
            best_i = i;
            best_j = j + 256;
        end
    end
end
assert(best_i - x(1, k) - 1 == 0);
assert(best_j - x(2, k) - 1 == 0);
end
%%
idx1 = 1;
idx2 = 1;
clear count;
k= 1;
for idx1 = 1 : numel(x.all_opt_info)
    opt_info = x.all_opt_info{idx1};
    for idx2 = 1 : numel(opt_info.time_k_nn_details)
        count(k) = opt_info.time_k_nn_details{idx2}{5};
        k = k + 1;
    end
end


%%
x = load('ours.mat');
y = load('base.mat');

find(x.obj_diff ~= y.obj_diff)
% numel(find(x.adopted_expanded_idx ~= y.adopted_expanded_idx))
% find(x.target ~= y.target)
% find(x.sub_dictionary ~= y.sub_dictionary)
% x.adopted_expanded_idx(123 : 124)
% y.adopted_expanded_idx(123 : 124)
% find(x.sub_opt_coef ~= y.sub_opt_coef)

%%
x = load('ours', 'obj_diff');
query = x.obj_diff;
database = Xbase;
distance_info;
save('debug_data2.mat', 'query', 'database', 'distance_info');

%%
% dictionary = Xbase;
% obj_diff = x.obj_diff;
tmp = obj_diff' * dictionary;
tmp = abs(tmp);
[s_tmp, s_idx] = sort(tmp, 'descend');

load('debug_data2.mat', 'obj_diff');
[t1, t2] = abs_nn_brute_force(obj_diff, Xbase, 0.1, 150);

non_equal_idx = find(adopted_expanded_idx' ~= s_idx(1 : numel(adopted_expanded_idx)))
if ~isempty(non_equal_idx)
d = dictionary(:, adopted_expanded_idx) - ...
    dictionary(:, s_idx(1 : numel(adopted_expanded_idx)));
norm(d(:))
end
%%
clear inverted_time
clear inverted_acc
k = 1;

for num_real_calculated = [1 : 1 : 50]
    file_name = ['inverted_list_exact_1024_1_16_256_approximate' ...
        num2str(num_real_calculated) '_1_10000.mat'];
    if ~exist(file_name, 'file')
        continue;
    end
    x = load(file_name);
    assert(x.info{4}.id == 0);
    inverted_time(k) = x.info{4}.time_cost / numel(x.nn_idx);
    inverted_acc(k) = x.acc;
    k = k + 1;
end

semilogy(inverted_acc, inverted_time, 'r-o');
hold on;

%% plot
clear tree_time
clear tree_acc
k = 1;

for num_real_calculated = [10^4 * [1 : 2: 10], 10^5 * [2 : 3]]
    file_name = ['single_ball_exact_20_' num2str(num_real_calculated) ...
        '_1_10000.mat'];
    if ~exist(file_name, 'file')
        continue;
    end
    x = load(file_name);
    assert(x.info{2}.id == 0);
    tree_time(k) = x.info{2}.time_cost / numel(x.nn_idx);
    tree_acc(k) = x.acc;
    k = k + 1;
end

plot(tree_acc, tree_time, '-o');


%%
Z = R' * Xtraining;
save_mat(Z, 'z.double.bin', 'double');
for i = 0 : numel(all_D) - 1
    file_name = ['all_d_' num2str(i)];
    save_mat(all_D{i + 1}, file_name, 'double');
end

save_mat(compactB, 'code', 'int16');


%%
x = load('linear_largest_inner_product_1_10000');
mean(x.info.each_cost)
std(x.info.each_cost)
%%
m1 = 4096;
k = 256;
file_name = ['inverted_list_exact_' num2str(m1) ...
    '_1_16_' num2str(k) '_1_10000'];
x = load(file_name);

x.info{3}.time_cost / numel(x.nn_idx)
std(x.info{1}.time_cost + x.info{2}.time_cost)

%% check whether the result is rightr

clc;
% x1 = load('bound_exact_16_256_1_10000');
x0 = load('linear_largest_inner_product_1_10000');
x2 = load('inverted_list_exact_1024_1_16_256_1_10000');

non_equal = find(x0.nn_idx ~= x2.nn_idx);
for i = 1 : numel(non_equal)
    idx = non_equal(i);
    idx0 = x0.nn_idx(idx);
    idx0 = double(idx0) + 1;
    
    idx2 = x2.nn_idx(idx);
    idx2 = double(idx2) + 1;
    
    n = norm(problem.dictionary(:, idx0) - problem.dictionary(:, idx2));
    assert(n < 10^-4);
end



% % mean(x0.info.each_cost)
% x1.info{3}.time_cost / numel(x1.nn_idx)
% x2.info{3}.time_cost / numel(x2.nn_idx)

%% save data to run main function by c++

query = problem.all_target;
database = problem.dictionary;

save('debug_data.mat', 'query', 'database', 'distance_info');


%%
x = load('local_ock_model_256_0_8_256.mat');
%
c = 0;
for i = 1 : 256
    c = c + x.all_ock_batch{i}.obj(end);
end
c / 161789

%%
s = 0;
for i = 1 : numel(local_ock_model.all_ock_model)
s = s + local_ock_model.all_ock_model{i}.obj(end)
end
s / 27087
%%
rng(0);
dim = 100;
num = 10000;
sparsity = 10;
A = rand(dim, num);
x2 = rand(num, 1);
rp = randperm(num, sparsity);
x = zeros(num, 1);
x(rp) = x2(rp);

y = A * x + randn(dim, 1) * 0.1;

gamma = 20;


tic;
Xout3 = w_l1ls_featuresign(A, y, gamma);
x3 = toc;

tic;
Xout1 = l1ls_featuresign(A, y, gamma);
x1 = toc;


tic;
Xout2 = LeastR(A, y, gamma);
x2 = toc;

fprintf('fs1 : %d, %f, %f\n', numel(find(Xout1)), ...
    0.5 * norm(A * Xout1 - y) ^ 2 + gamma * norm(Xout1, 1), ...
    x1);
fprintf('fs3 : %d, %f, %f\n', numel(find(Xout3)), ...
    0.5 * norm(A * Xout3 - y) ^ 2 + gamma * norm(Xout3, 1), ...
    x3);
fprintf('slep : %d, %f, %f\n', numel(find(Xout2)), ...
    0.5 * norm(A * Xout2 - y) ^ 2 + gamma * norm(Xout2, 1), ...
    x2);


%%
demo_fast_sc(2);

%%
training = read_mat('training', 'double');
centers = read_mat('centers', 'double');
centers = w_kmeans(training, 256, centers);
%% multi-layer k-means
Xtraining = problem.dictionary;
%%
num_point = size(problem.dictionary, 2);

rp = randperm(num_point);
rp = rp(1 : 255);
centers = Xtraining(:, rp);
centers = [centers, zeros(426, 1)];

centers = w_kmeans(Xtraining, ...
    256, centers);

sq_error = sqdist(Xtraining, centers);
[sq_error, code] = min(sq_error, [], 2);

Xtraining = Xtraining - centers(:, code);
mean(sq_error)

%% check the pronability of the bound
num_partitions = 16;
sub_dic_size_each_partition = 256;
file_name_ock_model = generate_task_save_file('ock_model', ...
    {num_partitions, sub_dic_size_each_partition});
x = load(file_name_ock_model);
ock_model = x.ock_model;
ock_codes = x.ock_codes;
% compute the bound;
bound = 0;
Z = ock_model.R' *  problem.dictionary;
idx_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    sub_dim = size(subD, 1);
    idx_end = idx_start + sub_dim - 1;
    x = Z(idx_start : idx_end, :) - subD(:, double(ock_codes(i, :)) + 1);
    x = x.^ 2;
    x = sum(x, 1);
    bound = bound + x;
    idx_start = idx_end + 1;
end
mean(bound(:))
bound = sqrt(bound);
mean(bound(:))

tol = 10^-4;
num_init_non_zeros = 100;
init = 0; % 0: random based, 2: nn-search and then all zeros.
file_collect = generate_task_save_file('collect_query', ...
    {[working_folder data_type], tol, init, num_init_non_zeros, 1, 100});

x = load(file_collect);
query_gamma = x.query_gamma;
all_query = x.all_query;
num_query = size(all_query, 2);

%
clc;
y = batch_map(1, num_query, 100, ...
    @(p1, p2)check_distribution(p1, p2, all_query, query_gamma, ...
    ock_model, ock_codes, bound, dictionary));
y = reduce_bound_effectiveness(y);


%% check the pronability of the bound
num_partitions = 1;
sub_dic_size_each_partition = 27807;
file_name_ock_model = generate_task_save_file('ock_model', ...
    {num_partitions, sub_dic_size_each_partition});
x = load(file_name_ock_model);
ock_model = x.ock_model;
ock_codes = x.ock_codes;
% compute the bound;
bound = 0;
Z = ock_model.R' *  problem.dictionary;
idx_start = 1;
for i = 1 : num_partitions
    subD = ock_model.all_D{i};
    sub_dim = size(subD, 1);
    idx_end = idx_start + sub_dim - 1;
    x = Z(idx_start : idx_end, :) - subD(:, double(ock_codes(i, :)) + 1);
    x = x.^ 2;
    x = sum(x, 1);
    bound = bound + x;
    idx_start = idx_end + 1;
end
mean(bound(:))
bound = sqrt(bound);
mean(bound(:))

tol = 10^-4;
num_init_non_zeros = 100;
init = 0; % 0: random based, 2: nn-search and then all zeros.
file_collect = generate_task_save_file('collect_query', ...
    {[working_folder data_type], tol, init, num_init_non_zeros, 1, 100});

x = load(file_collect);
query_gamma = x.query_gamma;
all_query = x.all_query;
num_query = size(all_query, 2);

%
clc;
y = batch_map(1, num_query, 100, ...
    @(p1, p2)bound_effectiveness(p1, p2, all_query, query_gamma, ...
    ock_model, ock_codes, bound));
y = reduce_bound_effectiveness(y);

%%
x1 = load('pre_rec_1_16384.mat');

x16 = load('pre_rec_16_16384.mat');

%%

file_name_ock_model = generate_task_save_file('ock_model', ...
    {1, 16384});
x = load(file_name_ock_model);
ock_model1 = x.ock_model;
ock_codes1 = x.ock_codes;
file_name_ock_model = generate_task_save_file('ock_model', ...
    {16, 16384});
x = load(file_name_ock_model);
ock_model16 = x.ock_model;
ock_codes16 = x.ock_codes;
%%
[~, idx] = max(x1.all_ap - x16.all_ap);

y1 = batch_map(idx, idx, 1, ...
    @(idx1, idx2)ockabsinner(idx1, idx2, all_query, ...
    ock_codes1, ock_model1, gnd, problem.dictionary));

y16 = batch_map(idx, idx, 1, ...
    @(idx1, idx2)ockabsinner(idx1, idx2, all_query, ...
    ock_codes16, ock_model16, gnd, problem.dictionary));

return;

%%
slep = load('SLEP_tol0.0001_1_100.mat');
%
mean(slep.all_time(1 : 100))
std(slep.all_time(1 : 100))

%%
% siso = load('SISO_tol0.0001_1_1000.mat');
siso = load('SISO_tol0.0001_numInitNonZeros100_1_100.mat');
%
mean(siso.all_time(1 : 100))
std(siso.all_time(1 : 100))
%%
ratio_leastR = zeros(100, 1);
for i = 1 : 100
    ratio_leastR(i) = sum(siso.all_opt_info{i}.time_leastR) / siso.all_time(i);
    ratio_nn(i) = sum(siso.all_opt_info{i}.time_k_nn) / siso.all_time(i);
end
%%
mean(ratio_leastR)
std(ratio_leastR)

mean(ratio_nn)
std(ratio_nn)
mean(x.all_obj(1 : 100))
std(x.all_obj(1 : 100))

%%
figure;
[~, idx] = sort(slep.all_obj);
plot(slep.all_obj(idx), 'b', 'LineWidth', 2);
hold on;
plot(siso.all_obj(idx), 'r', 'LineWidth', 2);
legend('SLEP', 'SISO', 'Location', 'Best');
set(gca, 'FontSize', 16);
grid on;
xlabel('sorted index of the query', 'FontSize', 16);
ylabel('function value', 'FontSize', 16);
saveas(gca, 'obj.eps', 'psc2');

%%
siso = load('SISO_tol0.0001_init0_numInitNonZeros100_1_100.mat');
s = 0;
for i = 1 : 100
    s = s + size(siso.all_opt_info{i}.all_query, 2);
end
%%
all_query = zeros(426, s);
idx_start = 1;
for i = 1 : 100
    num_query = size(siso.all_opt_info{i}.all_query, 2);
    idx_end = idx_start + num_query - 1;
    all_query(:, idx_start : idx_end) = siso.all_opt_info{i}.all_query;
    idx_start = idx_end + 1;
end
all_query = normalize_squared_l2(all_query);

%% plot the principle component
num_query = size(all_query, 2);
num_dic = size(problem.dictionary, 2);
rp = randperm(num_dic, num_query);

[mapped, pc, center] = do_pca([problem.dictionary, all_query], 2);
%
if size(mapped, 1) == 3
    figure;
    plot3(mapped(1, 1 : num_dic), mapped(2, 1 : num_dic), mapped(3, 1 : num_dic), 'b.');
    hold on;
    plot3(mapped(1, num_dic + 1 : end), mapped(2, num_dic + 1 : end), mapped(3, num_dic + 1 : end), 'r');
elseif size(mapped, 1) == 2
    figure
    plot(mapped(1, 1 : num_dic), mapped(2, 1 : num_dic), 'b.', 'markersize', 3);
    hold on;
    plot(mapped(1, num_dic + 1 : end), mapped(2, num_dic + 1 : end), 'r.', 'markersize', 3);
    set(gca, 'FontSize', 16);
    grid on;
    saveas(gca, 'TrainingQueryDistribution.eps', 'psc2');
end

%% mean ap
clc;
fprintf(' & ');
for i = [256 512 1024 2048 4096 8192 16384]
    fprintf(' %d & ', i);
end
fprintf('\\\\\n');
for j = [1 2 4 8 16]
    fprintf(' %d &', j);
    for i = [256 512 1024 2048 4096 8192 16384]
        file_name = ['pre_rec_' num2str(j) '_' num2str(i) '.mat'];
        if ~exist(file_name, 'file')
            fprintf(' * & ');
        else
            x = load(file_name);
            fprintf(' %.4f &', x.mean_ap);
        end
    end
    fprintf('\\\\\n');
end

%%
clc;
fprintf(' & ');
for i = [256 512 1024 2048 4096 8192 16384]
    fprintf(' %d & ', i);
end
fprintf('\\\\\n');
for j = [1 2 4 8 16]
    fprintf(' %d &', j);
    for i = [256 512 1024 2048 4096 8192 16384]
        file_name = ['pre_rec_' num2str(j) '_' num2str(i) '.mat'];
        if ~exist(file_name, 'file')
            fprintf(' * & ');
        else
            x = load(file_name);
            fprintf(' %.4f &', x.query_error);
        end
    end
    fprintf('\\\\\n');
end

%% objective function value
clc;
fprintf(' & ');
for i = [256 512 1024 2048 4096 8192 16384]
    fprintf(' %d & ', i);
end
fprintf('\\\\\n');
for j = [1 2 4 8 16]
    fprintf(' %d &', j);
    for i = [256 512 1024 2048 4096 8192 16384]
        file_name = ['' num2str(j) '_' num2str(i) '.mat'];
        if ~exist(file_name, 'file')
            fprintf(' * & ');
        else
            x = load(file_name);
            fprintf(' %.4f &', (x.ock_model.obj(end) / 161789));
        end
    end
    fprintf('\\\\\n');
end

%% number of non-zeros
file_name = 'SISO_tol0.0001_init0_numInitNonZeros100_1_100.mat';
file_name = 'ckSISO_tol0.0001_numInitNonZeros100_1_100.mat';
x = load(file_name);
s = 0;
for i = 1 :  numel(x.all_active_idx)
    s = s + numel(x.all_active_idx{i});
end
s = s / numel(x.all_active_idx)

%%
x = load('SISO_tol0.0001_1_100.mat');

s = 0;
for i = 1 : 100
    s = s + numel(x.all_opt_info{i}.time_leastR)
end
s / 100

%%

x = load('ck_siso_16_256_20000_0.0001_2_1_100.mat');
%
t = 0;
r1 = 0;
r2 = 0;
r_scan = 0;
r_reranking = 0;
num_iter = 0;
for i = 1 : numel(x.all_active_idx)
    t = t + numel(x.all_active_idx{i});
    r1 = r1 + sum(x.all_opt_info{i}.time_leastR) / x.all_time(i);
    r2 = r2 + sum(x.all_opt_info{i}.time_k_nn) / x.all_time(i);
    
    scan = 0;
    reranking = 0;
    knn_details = x.all_opt_info{i}.time_k_nn_details;
    for k = 1 : numel(knn_details)
            scan = scan + knn_details{k}.scan;
            reranking = reranking + knn_details{k}.reranking;
    end
    r_scan = r_scan + scan / x.all_time(i);
    r_reranking = r_reranking + reranking / x.all_time(i);
    num_iter = num_iter + numel(x.all_opt_info{i}.time_leastR);
end
fprintf('%.3f & %.4f & %.3f & %.3f & %.3f & %.3f & %.3f \n', ...
    mean(x.all_time), ...
    mean(x.all_obj), ...
    num_iter / numel(x.all_active_idx), ...
    t / numel(x.all_active_idx), ...
    r1 / numel(x.all_active_idx), ...
    r_scan / numel(x.all_active_idx), ...
    r_reranking / numel(x.all_active_idx));

prepare_test;
prepare_base;

for num_partitions = all_num_partitions
    for sub_dic_size_each_partition = all_sub_dic_size_each_partition
        if is_approximate
            for max_num_inverted_list = all_max_num_inverted_list
                run_global_ck_exact_each;
            end
        else
            run_global_ck_exact_each
        end
    end
end



prepare_test;
prepare_base;


file_name = generate_task_save_file('linear_largest_inner_product', ...
    {idx_start_batch, idx_end_batch});

[nn_idx, info] = linear_largest_inner_product(Xtest, Xbase);

save(file_name, 'nn_idx', 'info', 'machine_name', '-v7.3');
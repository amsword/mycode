file_name = [working_folder data_type '/siso' ...
    num2str(idx_start_batch) ...
    '_' ...
    num2str(idx_end_batch)];

make_valid(file_name);

num_test = size(all_target, 2);
num_words = size(dictionary, 2);
all_obj = zeros(num_test, 1);
all_active_coef = cell(num_test, 1);
all_active_idx = cell(num_test, 1);
all_opt_info = cell(num_test, 1);
all_time = zeros(num_test, 1);

%%
last_time = smart_meassge_init();
for i = 1 : num_test
    last_time = smart_message(last_time, num2str(i));
    
    target = all_target(:, i);
    gamma = all_gamma(i);
    
    stamp = tic;
    [active_idx, active_coef, obj, opt_info] = lasso_siso(dictionary, target, gamma);
    curr_time = toc(stamp);
    
    all_obj(i) = obj(end);
    all_active_coef{i} = active_coef;
    all_active_idx{i} = active_idx;
    all_opt_info{i} = opt_info;
    all_time(i) = curr_time;
end
%%

save(file_name, 'all_active_coef', 'all_active_idx', ...
    'all_obj',  'all_opt_info', 'all_time', '-append');
function [all_ap, query_error] = reduce_ockabsinner(y)


num_batch = numel(y);

count = 0;
sum_ap = 0;
for i = 1 : num_batch
    count = count + numel(y{i}.all_ap);
end
all_ap = zeros(count, 1);

query_error = 0;

idx_start = 1;
for i = 1 : num_batch
    sub_all_ap = y{i}.all_ap;
    n = numel(sub_all_ap);
    idx_end = idx_start + n - 1;
    all_ap(idx_start : idx_end) = sub_all_ap;
    idx_start = idx_end + 1;
    
    query_error = query_error + y{i}.error;
end

query_error = query_error / count;

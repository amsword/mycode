function result = evaluate_lasso(problem, func_lasso)

dictionary = problem.dictionary;
all_target = problem.all_target;
all_gamma = problem.all_gamma;

num_test = size(all_target, 2);

all_obj = zeros(num_test, 1);
all_active_coef = cell(num_test, 1);
all_active_idx = cell(num_test, 1);
all_opt_info = cell(num_test, 1);
all_time = zeros(num_test, 1);

%%
last_time = smart_meassge_init();
for i = 1 : num_test
    last_time = smart_message(last_time, [num2str(i) ': ' num2str(num_test)]);
    
    target = all_target(:, i);
    gamma = all_gamma(i);
    
    stamp = tic;
    [active_idx, active_coef, obj, opt_info] = ...
        func_lasso(dictionary, target, gamma);
    curr_time = toc(stamp);
    
    all_obj(i) = obj(end);
    all_active_coef{i} = active_coef;
    all_active_idx{i} = active_idx;
    all_opt_info{i} = opt_info;
    all_time(i) = curr_time;
end

result.all_obj = all_obj;
result.all_active_coef = all_active_coef;
result.all_active_idx = all_active_idx;
result.all_opt_info = all_opt_info; 
result.all_time = all_time;


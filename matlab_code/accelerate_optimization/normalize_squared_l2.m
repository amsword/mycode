
function [y, s] = normalize_squared_l2(x)
s = sum(x .^ 2, 1);
s = sqrt(s);
y = bsxfun(@rdivide, x, s);
end
opt_input = [];
tol = 10^-4;
num_init_non_zeros = 100;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;
opt_input.num_init_non_zeros = num_init_non_zeros;

%% load coarse quantization
file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);

coarse_model = load(file_name_coarse_k_means, 'coarse_centers', ...
    'all_inverted_idx', 'bound_center');

% load fine quantization
file_name_global_ock_model = generate_task_save_file(...
    'global_ock_model', ...
    {num_coarse_cluster, is_optimize_R, num_partitions, sub_dic_size_each_partition});

fine_model = load(file_name_global_ock_model, ...
    'global_ock_model', 'all_ock_codes', 'all_ock_bound');

clear distance_info;
distance_info.coarse_model = coarse_model;
distance_info.fine_model = fine_model;
distance_info.is_approximate = is_approximate;
distance_info.max_num_inverted_list = max_num_inverted_list;

%%
opt_input = [];
tol = 10^-4;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;

func_nn_search = @(p1, p2, p3, p4)global_ock_one_query(...
    p1, p2, p3, p4, distance_info);

func_siso = @(para1, para2, para3)lasso_siso(...
    para1, ...
    para2, ...
    para3, ...
    func_nn_search, ...
    opt_input);

prepare_test;
prepare_base;
clear problem;
problem.dictionary = Xbase;
problem.all_target = Xtest(:, idx_start_batch : idx_end_batch);
all_gamma = zeros(size(problem.all_target, 2), 1);
all_gamma(:) = 0.1;
problem.all_gamma = all_gamma;

%%
result = evaluate_lasso(problem, func_siso);

%%
result.opt_input = opt_input;

file_name_ck_siso = generate_task_save_file('inverted_global_siso', ...
    {num_partitions, sub_dic_size_each_partition, ...
    num_ock_candidate, ...
    tol, idx_start_batch, idx_end_batch, ck_linear_type});

save_evaluated(file_name_ck_siso, result);
nn_idx;

file_name = generate_task_save_file('linear_largest_inner_product', ...
    {idx_start_batch, idx_end_batch});
x = load(file_name);
 acc = sum(x.nn_idx == nn_idx) / numel(nn_idx);
 
 t = Xtest - Xbase(:, double(x.nn_idx) + 1);
 t = t .^ 2;
 gnd_dist = sum(t, 1);

 t = Xtest - Xbase(:, double(nn_idx) + 1);
 t = t .^ 2;
 curr_dist = sum(t, 1);
 
 num_right = sum(abs(gnd_dist - curr_dist) < 10^-5 * mean(curr_dist));
 acc2 = num_right / numel(x.nn_idx);
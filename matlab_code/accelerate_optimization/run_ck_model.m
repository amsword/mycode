opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;
opt_input_ock.max_iter = max_iter_ock;

file_name_ock_model = generate_task_save_file('ock_model', ...
    {opt_input_ock.num_partitions, opt_input_ock.sub_dic_size_each_partition});

%%
is_model_ready = 0;
if exist(file_name_ock_model, 'file')
    x = load(file_name_ock_model);
    if isfield(x, 'ock_model')
        is_model_ready = 1;
        ock_model = x.ock_model;
    end
end

if ~is_model_ready
    ock_model = ock_training(problem.dictionary, opt_input_ock);
end
ock_codes = ock_encoding(ock_model, problem.dictionary);
ock_bounds = ock_sq_errors(ock_model, problem.dictionary, ock_codes);
ock_bounds = sqrt(ock_bounds);
%%
save(file_name_ock_model, 'ock_model', '-v7.3');
save(file_name_ock_model, 'ock_codes', '-append', '-v7.3');
save(file_name_ock_model, 'ock_bounds', '-append', '-v7.3');
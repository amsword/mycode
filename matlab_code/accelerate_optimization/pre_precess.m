function [dictionary, all_target] = pre_precess(loaded_data, data_type)

data_type_nus_wide_lite = 'NUS-WIDE-LITE';
data_type_nus_wide = 'NUS-WIDE';
data_type_sift1m = 'SIFT1M';
data_type_debug_data = 'DEBUG_DATA';
if strcmp(data_type, data_type_nus_wide_lite) || ...
        strcmp(data_type, data_type_nus_wide)
    dictionary = [normalize_squared_l2(loaded_data.wt_train); ...
        normalize_squared_l2(loaded_data.edh_train); ...
        normalize_squared_l2(loaded_data.cm55_train)];
    
    dictionary = normalize_squared_l2(dictionary);
    
    all_target = [normalize_squared_l2(loaded_data.wt_test); ...
        normalize_squared_l2(loaded_data.edh_test); ...
        normalize_squared_l2(loaded_data.cm55_test)];
    
    all_target = normalize_squared_l2(all_target);
    
elseif strcmp(data_type, data_type_sift1m)
    assert(~isfield(loaded_data, 'training'));
%     loaded_data.training = normalize_squared_l2(loaded_data.training);
    dictionary = normalize_squared_l2(loaded_data.base);
    all_target = normalize_squared_l2(loaded_data.test);
end

end

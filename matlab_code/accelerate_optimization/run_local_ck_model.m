file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);
file_name_coarse_k_means
x = load(file_name_coarse_k_means);
coarse_centers = x.coarse_centers;
all_inverted_idx = x.all_inverted_idx;

%%
Xtraining = problem.dictionary;
file_name_local_ock_model = generate_task_save_file(...
    'local_ock_model', ...
    {num_coarse_cluster, is_optimize_R, num_partitions, sub_dic_size_each_partition});
file_name_local_ock_model
is_model_ready = false;
is_code_ready = false;

if exist(file_name_local_ock_model, 'file')
    x = load(file_name_local_ock_model);
    if isfield(x, 'all_ock_model')
        is_model_ready = true;
    end
    if isfield(x, 'local_ock_codes')
        is_code_ready = true;
    end
end
%%
opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;
opt_input_ock.is_optimize_R = is_optimize_R;


%%
run_local_ck_model_id;

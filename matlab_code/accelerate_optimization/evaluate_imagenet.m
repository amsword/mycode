result;

query_label_file = [data_folder data_type '/query_label.double'];
database_label_file = [data_folder data_type '/database_label.double'];

query_label = read_mat(query_label_file, 'double');
database_label = read_mat(database_label_file, 'double');

num_query = numel(result.all_obj);

good = 1;
for idx_query = 1 : num_query
    curr_coef = result.all_active_coef{idx_query};
    curr_active_idx = result.all_active_idx{idx_query};
    
    curr_indicator = zeros(1000, 1);
    for idx_idx = 1 : numel(curr_active_idx)
        idx = curr_active_idx(idx_idx);
        coef = curr_coef(idx_idx);
        idx_label = database_label(idx);
        curr_indicator(idx_label) = curr_indicator(idx_label) + coef;
    end
    [tmp, best_idx] = max(curr_indicator);
    if best_idx == query_label(idx_query)
        good = good + 1;
    end
end

scemantic_acc = good / num_query;
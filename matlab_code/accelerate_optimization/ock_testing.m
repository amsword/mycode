function [result_idx, details] = ock_testing(...
    query, database, gamma, max_expanded, ...
    database_code, distance_info)

distance_info.gamma = gamma;
distance_info.max_expanded = max_expanded;
distance_info.raw_database = database;

if distance_info.distance_type == 1 || ...
        distance_info.distance_type == 2
    query_length = norm(query);
    query = query / query_length;
    distance_info.gamma = gamma / query_length;
end

[result_idx, details] = mexAbsInnerSearch(...
    query, ...
    database_code, ...
    distance_info);


end

function matlab_compute_check(query, database, gamma, topk, ...
    database_code, ock_model, opt_testing)

%%
tic;
rotated = distance_info.R' * query;
num_partitions = numel(distance_info.all_D);
lookups = cell(num_partitions, 1);
idx_start = 1;
for i = 1 : num_partitions
    subD = distance_info.all_D{i};
    sub_dim = size(subD, 1);
    idx_end = idx_start + sub_dim - 1;
    lookups{i} = rotated(idx_start : idx_end)' * subD;
    idx_start = idx_end + 1;
end
%
num_point = size(database_code, 2);
distance = zeros(1, num_point);
for i = 1 : num_partitions
    distance = distance + lookups{i}(double(database_code(i, :)) + 1);
end
distance = abs(distance);
[s, idx] = sort(distance, 'descend');
result = idx_selected(idx(1 : max_expanded));
toc;
end

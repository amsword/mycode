%% load original data
data_required.idx_start_test = idx_start_batch;
data_required.idx_end_test = idx_end_batch;

data_required.is_data_normalize = is_data_normalize;

if strcmp(data_type, 'Tiny80M')
    data_required.idx_start_training = tiny80m_idx_start_training;
    data_required.idx_end_training = tiny80m_idx_end_training;
    data_required.idx_start_base = tiny80m_idx_start_base;
    data_required.idx_end_base = tiny80m_idx_end_base;
    data_required.idx_start_test = tiny80m_idx_start_test;
    data_required.idx_end_test = tiny80m_idx_end_test;
else
    data_required.idx_start_test = idx_start_batch;
    data_required.idx_end_test = idx_end_batch;
end

data_required.is_data_normalize = is_data_normalize;
loaded_data = load_data(data_folder, data_type,data_required);

if strcmp(data_type, 'Tiny80M')
    if isfield(loaded_data, 'base')
        problem.dictionary = loaded_data.base;
        Xbase = problem.dictionary;
    end
    if isfield(loaded_data, 'test')
        problem.all_target = loaded_data.test;
        Xtest = problem.all_target;
    end
    
    if isfield(loaded_data, 'training')
        problem.training = loaded_data.training;
        Xtraining = problem.training;
    end
    
    clear loaded_data;
    
elseif strcmp(data_type, 'SIFT1M') || strcmp(data_type, 'GIST1M')
    if isfield(loaded_data, 'base')
        problem.dictionary = loaded_data.base;
        Xbase = problem.dictionary;
        Xtraining = Xbase;
    end
    if isfield(loaded_data, 'test')
        problem.all_target = loaded_data.test;
        Xtest = problem.all_target;
    end
else
    error('no longer supported');
    [dictionary, all_target] = pre_precess(loaded_data, data_type);
    problem.dictionary = dictionary;
    problem.all_target = all_target;
    all_gamma = compute_gamma(dictionary, all_target, alpha);
    problem.all_gamma = all_gamma;
end
clear loaded_data
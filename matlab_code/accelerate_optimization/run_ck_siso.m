opt_input = [];
tol = 10^-4;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;

file_name_ock_model = generate_task_save_file('ock_model', ...
    {opt_input_ock.num_partitions, opt_input_ock.sub_dic_size_each_partition});
x = load(file_name_ock_model);
distance_info = x.ock_model;
ock_codes = x.ock_codes;

distance_info.distance_type = ck_linear_type;

if distance_info.distance_type == 1 || ...
        distance_info.distance_type == 2
       distance_info.ock_bounds = x.ock_bounds;
end

distance_info.num_ock_candidate = num_ock_candidate;

func_nn_search = @(p1, p2, p3, p4)ock_testing(p1, ...
    p2, p3, p4, ock_codes, distance_info);

func_siso = @(para1, para2, para3)lasso_siso(...
    para1, ...
    para2, ...
    para3, ...
    func_nn_search, ...
    opt_input);

result = evaluate_lasso(problem, func_siso);

result.opt_input = opt_input;

file_name_ck_siso = generate_task_save_file('ck_siso', ...
    {num_partitions, sub_dic_size_each_partition, ...
    num_ock_candidate, ...
    tol, idx_start_batch, idx_end_batch, ck_linear_type});

save_evaluated(file_name_ck_siso, result);
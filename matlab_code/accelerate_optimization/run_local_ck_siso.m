opt_input = [];
tol = 10^-4;
num_init_non_zeros = 100;

opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;

opt_input.tol = tol;
opt_input.num_init_non_zeros = num_init_non_zeros;

%% load coarse quantization
file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);

x = load(file_name_coarse_k_means, 'coarse_centers', ...
    'coarse_idx', 'all_inverted_idx');
coarse_centers = x.coarse_centers;
all_inverted_idx = x.all_inverted_idx;

%% load fine quantization
file_name_local_ock_model = generate_task_save_file(...
    'local_ock_model', ...
    {num_coarse_cluster, is_optimize_R, num_partitions, sub_dic_size_each_partition});

x = load(file_name_local_ock_model);

all_ock_model = x.all_ock_model;
all_ock_codes = x.all_ock_codes;
all_ock_bound = x.all_ock_bound;

%
clear distance_info;
distance_info.coarse_centers = coarse_centers;
distance_info.all_inverted_idx = all_inverted_idx;

distance_info.all_ock_model = all_ock_model;
distance_info.all_ock_codes = all_ock_codes;
distance_info.all_ock_bound = all_ock_bound;
distance_info.omega = omega;
%
func_nn_search = @(p1, p2, p3, p4)local_ock_testing(p1, p2, p3, p4, ...
    distance_info);
%
func_siso = @(para1, para2, para3)lasso_siso(...
    para1, ...
    para2, ...
    para3, ...
    func_nn_search, ...
    opt_input);

result = evaluate_lasso(problem, func_siso);

result.opt_input = opt_input;
save_evaluated(file_name, result);


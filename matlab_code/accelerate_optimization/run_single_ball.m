prepare_base;
prepare_test;

% if ~is_approximate
%     assert(numel(all_tree_num_max_real_computed) == 1);
% end

if is_approximate
    for tree_num_max_real_computed = all_tree_num_max_real_computed
        run_single_ball_each;
    end
else
    run_single_ball_each;
end
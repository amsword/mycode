% clear all;
% addpath('D:\Projects\SLEP\SLEP\functions');
% cd D:/Projects;

%% The data matrix is of size m x n
m=size(dictionary, 1);
n=size(dictionary, 2);
rho=gamma;            % the regularization parameter

% ---------------------- Generate random data ----------------------
% for reproducibility
randNum = rand(1,1)*17*37;
randn('state',(randNum-1)*3+1);
A= dictionary;

% normalization
% for i=1:n
%     A(:,i)=A(:,i)/norm(A(:,i));
% end

%iterNumb = [];
T = 10;
ReCoff = [];
alpha = 0.2;
K = 100;

oldtime = 0;
newtime = 0;

time1 = 0;
time2 = 0;
time3 = 0;
time4 = 0;
time5 = 0;

i = 1;
% for i = 1:T
%------------------------Generate target----------------------------
randn('state',(randNum-1)*3+2);
xOrin=randn(n,1);

randn('state',(randNum-1)*3+3);
noise=randn(m,1);
y = target;

tic;
%calculate \gamma
g0 = A'*y;
g0 = abs(g0);
%gamma = max(g0);
gamma

time = toc;
time1 = time1 + time;

%------------------------options for Lasso--------------------------
opts=[];
% Starting point
opts.init=2;        % starting from a zero point
% termination criterion
opts.tFlag = 1;       % until the relative difference is below a threhold
%opts.tol   = tol;
% normalization
opts.nFlag=0;       % without normalization
% regularization
opts.rFlag=0;       % equals the input value
opts.mFlag=0;       % treating it as compositive function
opts.lFlag=0;       % Nemirovski's line search
opts.tFlag = 1;
opts.tol = 0.0001;

%--------------------------compare with Lasso------------------
tic;
opts.x0 = zeros(n, 1);
[x1, funVal1, ValueL1]= LeastR(A, y, gamma, opts);
time = toc;
%% oldtime = oldtime + time;


%--------------------------Active Variable Set---------------------
VS = [];                      %the active variable set
VSArray = [];
%---------------------------constrained sparse problem--------------
% randomly select 2000 atoms and assign them values randomly drawn
% from N(0,1)
%-------------------------------------------------------------------
x = zeros(n,1);               % start from the zero point
index = (1:n);
VS = index(randperm(length(index)))';
VS = VS(1:2000);
x(VS) = randn(1, 2000);
bate = 0.5;
sigmma = bate * max(abs(x));
%----------------------------------end-----------------------------
%tic;
Iter = 0;
fValue = [];
opts.tol = 0.00001;

%%
while Iter<100
    Iter = Iter + 1;
    tic;
    % compute gradient of the whole set
    g = A'*(A*x - y);
%     index = find(x < sigmma);
%     index = intersect(VS, index);
%     g(index) = g(index) + gamma;
    
    time = toc;
    time2 = time2 + time;
    
    tic;
    ag = abs(g);
    [Y,I] = sort(ag,1,'descend');
    if Y(1)<= gamma     %already the solution of original problem
        break;
    end;
    
    % select active variables
    Index = find(Y>=gamma); %expansion set
    pn = size(Index,1);
    if pn>K
        pn = K;
    end;
    NV = I(1:pn);
    % current working set
    n1 = size(VS,1);
    VS = union(VS,NV);
    n2 = size(VS,1);
    VSArray{Iter} = VS;
    if n1==n2
        break;
    end;
    time = toc;
    time3 = time3 + time;
    
    tx0 = x(VS);
    tA  = A(:,VS);
    
    opts.x0 = tx0;
    opts.init = 1; %initilized by the solution of previous subproblem
    tic;
    [tx, tfunVal, tValueL]= LeastR(tA, y, gamma, opts);
    time = toc;
    time4 = time4 + time;
    %opts.x0 = x;
    %[tx, tfunVal, tValueL]= LeastR(A, y, rho, opts);
    fValue{Iter} = tfunVal;
    
    if tfunVal(end)< funVal1(end)
        break;
    end;
    
    x(VS) = tx;
    
    Index = find(abs(tx)>0.0000000001);
    NVS = VS(Index);
    VS = NVS;
end;

sn = size(VS,1);
RC = zeros(sn,2);
RC(:,1) = VS;
RC(:,2) = x(VS);
ReCoff{i} = RC;
iterNumb{i} = Iter;

% end;
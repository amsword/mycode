function [final_active_set, final_active_coef, obj, opt_info] ...
    = lasso_siso1(dictionary, target, gamma)

percent_non_zeros = 0.1;
max_expanded = 150;
zeros_threshold = 10^-10;
max_iter = 100;
rng('default')
rng(1);
% opts
opts_sisoLeastR=[];
opts_sisoLeastR.init = 1;        % starting from previous results
opts_sisoLeastR.tFlag = 1;       % until the relative difference is below a threhold
opts_sisoLeastR.tol = 10^-4;

opt_info.percent_non_zeros = percent_non_zeros;
opt_info.max_expanded = max_expanded;
opt_info.zeros_threshold = zeros_threshold;
opt_info.max_iter = max_iter;
opt_info.rand_seed = 1;
opt_info.opts_sisoLeastR = opts_sisoLeastR;

num_words = size(dictionary, 2);

time_init = 0;
time_leastR = 0;
time_shrink = 0;
 time_k_nn = 0;


tic;
[active_index, curr_coef] = init_coef(num_words, percent_non_zeros);
time_init = toc;

%% solve the sub problem
last_time = smart_meassge_init();
stop_type = 'max_iter_reached';
all_obj = zeros(max_iter, 1);
for iter = 1 : max_iter
    sub_dictionary = dictionary(:, active_index);
    
    tic;
    opts_sisoLeastR.x0 = curr_coef;
    [sub_opt_coef, sub_opt_obj] = LeastR(...
        sub_dictionary, target, gamma, opts_sisoLeastR);
    x = toc;
    time_leastR = time_leastR + x;
    
    
    all_obj(iter) = sub_opt_obj(end);
    last_time = smart_message(last_time, [num2str(iter) ': ' num2str(all_obj(iter))]);
    
    tic;
    non_zeros_idx_in_active_set = abs(sub_opt_coef) > zeros_threshold;
    shrinked_active_index = active_index(non_zeros_idx_in_active_set);
    shrinked_curr_coef = sub_opt_coef(non_zeros_idx_in_active_set);
    time_shrink = time_shrink + toc;
    
    tic;
    obj_diff = sub_dictionary * sub_opt_coef - target;
    indicator = dictionary' * obj_diff;
    indicator = abs(indicator);
    expanded_idx = find(indicator > gamma + zeros_threshold);
    num_full_expanded_point = numel(expanded_idx);
    % in theory, the intersection of expanded_idx and 
    % shrinked_active_set is empty. However, it is not in practice
    if num_full_expanded_point <= max_expanded
        adopted_expanded_idx = expanded_idx;
    else
        expanded_strength = indicator(expanded_idx);
        [~, sorted_idx] = sort(expanded_strength, 'descend');
        topk_idx_in_full_expanded = sorted_idx(1 : max_expanded);
        adopted_expanded_idx = expanded_idx(topk_idx_in_full_expanded);
    end
    time_k_nn = time_k_nn + toc;
    robust_expanded_idx = setdiff(adopted_expanded_idx, shrinked_active_index);
    
    if isempty(robust_expanded_idx)
        stop_type = 'no_expanded';
        break;
    end
    active_index = [robust_expanded_idx; shrinked_active_index];
    curr_coef = [zeros(numel(robust_expanded_idx), 1); shrinked_curr_coef];
end
obj = all_obj(1 : iter);

non_zeros_idx_in_active_set = abs(sub_opt_coef) > zeros_threshold;
final_active_set = active_index(non_zeros_idx_in_active_set);
final_active_coef = curr_coef(non_zeros_idx_in_active_set);

opt_info.stop_type = stop_type;
opt_info.iter = iter;

opt_info.time_init = time_init;
opt_info.time_leastR = time_leastR;
opt_info.time_shrink = time_shrink;
opt_info.time_k_nn = time_k_nn;

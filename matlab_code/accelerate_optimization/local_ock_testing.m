function [result_idx, time_cost] = local_ock_testing(...
    query, database, gamma, max_expanded, ...
    distance_info)

s = norm(query);
query = query / s;
gamma = gamma / s;

distance_info.raw_database = database;
distance_info.gamma = gamma;
distance_info.max_expanded = max_expanded;

[result_idx, time_cost] = mexLocalOckTesting(...
    query, ...
    distance_info);

end

function [idx_good] = LocalOckTesting(...
    query, ...
    distance_info)

inner_to_coarse_center = query' * distance_info.coarse_centers;
abs_inner_to_coarse_center = abs(inner_to_coarse_center);

[s_abs_inner_to_coarse_center, s_idx_coarse_center] = ...
    sort(abs_inner_to_coarse_center, 'descend');
s_idx_coarse_center = s_idx_coarse_center(1 : distance_info.omega);

idx_good = [];
for i = 1 : distance_info.omega
    idx_coarse_center = s_idx_coarse_center(i);
    ock_model = distance_info.all_ock_model{idx_coarse_center};
    
    if ock_model.is_optimize_R
        t_query = ock_model.R' * query;
    else
        t_query = query;
    end
    num_partitions = numel(ock_model.all_D);
    idx_dim_start = 1;
    lookups = cell(num_partitions, 1);
    for j = 1 : num_partitions
       subD = ock_model.all_D{j};
        sub_dim = size(subD, 1);
        idx_dim_end = idx_dim_start + sub_dim - 1;
        lookups{j} = t_query(idx_dim_start : idx_dim_end)' * subD;
        idx_dim_start = idx_dim_end + 1;
    end
    ock_codes = distance_info.all_ock_codes{idx_coarse_center};
    
    m = 1;
    dist = 0;
    for j = 1 : num_partitions
        for k = 1 : ock_model.num_sub_dic_each_partition
            dist = dist + lookups{j}(double(ock_codes(m, :)) + 1);
            m = m + 1;
        end
    end
    dist = dist + inner_to_coarse_center(idx_coarse_center);
    dist = abs(dist);
    
    ind = find(dist - distance_info.all_ock_bound{idx_coarse_center} > distance_info.gamma);
    
    idx_good = [idx_good; distance_info.all_inverted_idx{idx_coarse_center}(ind)];
    
end

end

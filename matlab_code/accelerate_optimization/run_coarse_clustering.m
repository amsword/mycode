%%
file_name_coarse_k_means = generate_task_save_file(...
    'coarse_k_means', num_coarse_cluster);
file_name_coarse_k_means

is_inverted_idx = true;
is_kmeans_training = true;
if exist(file_name_coarse_k_means, 'file')
    x = load(file_name_coarse_k_means);
    if isfield(x, 'coarse_centers')
        is_kmeans_training = false;
        coarse_centers = x.coarse_centers;
    end
end

prepare_training;
%% training
if is_kmeans_training
    
    [coarse_centers] = w_kmeans(Xtraining, num_coarse_cluster);
    save(file_name_coarse_k_means, 'coarse_centers');
    if strcmp(data_type, 'Tiny80M')
        [coarse_idx_training] = w_assignment(Xtraining, coarse_centers);
        save(file_name_coarse_k_means, 'coarse_idx_training', '-append');
    end
end

%% indexing
if strcmp(data_type, 'SIFT1M') || ...
    strcmp(data_type, 'GIST1M') || ...
    strcmp(data_type, 'ImageNet');
    Xbase = Xtraining;
elseif strcmp(data_type, 'Tiny80M')
    clear Xtraining;
    prepare_base;
else
    error('dfs');
end

[coarse_idx,  distortions] = w_assignment(...
    Xbase, coarse_centers);

distortions = sqrt(distortions);

bound_center = zeros(num_coarse_cluster, 1);
for i = 1 : num_coarse_cluster
    idx_one = coarse_idx == i;
    x = distortions(idx_one);
    if isempty(x)
        bound_center(i) = -1;
    end
    bound_center(i) = max(x);
end

%
all_inverted_idx = cell(num_coarse_cluster, 1);
for i = 1 : num_coarse_cluster
    all_inverted_idx{i} = int32(find(coarse_idx == i) - 1);
end

save(file_name_coarse_k_means, 'coarse_centers', ...
    'coarse_idx', 'all_inverted_idx', 'bound_center', '-append', '-v7.3');

s1 = 0;

for c1 = 1 : num_dic
    for c2 = 1 : dic_size
        if c2 == c1
            continue;
        end
        for c3 = 1 : num_dic
            if c3 == c1 || c3 == c2
                continue;
            end
            for c4 = 1 : num_dic
                if c4 == c3 || c4 == c2 || c4 == c1
                    continue;
                end
                s1 = s1 + (original_D{c1} * ones(dic_size, 1))' * ...
                    (original_D{c2} * ones(dic_size, 1)) * ...
                    (original_D{c3} * ones(dic_size, 1))' * ...
                    (original_D{c4} * ones(dic_size, 1));
            end
        end
    end
end

s2 = 0;

for c1 = 1 : num_dic
    for c2 = 1 : num_dic
        if c2 == c1
            continue;
        end
        for c4 = 1 : num_dic
            if c4 == c2 || c4 == c1
                continue;
            end
            s2 = s2 + (original_D{c1} * ones(dic_size, 1))' * ...
                (original_D{c2})* ...
                (original_D{c2})' * ...
                (original_D{c4} * ones(dic_size, 1));
        end
    end
end


s3 = 0;
for c1 = 1 : num_dic
    for c2 = 1 : num_dic
        if c2 == c1
            continue;
        end
        s3 = s3 + trace(original_D{c1} * original_D{c1}' * ...
            (original_D{c2}) * (original_D{c2})');
    end
end

ours = 0.25 * dic_size ^ (num_dic - 4) * s1 + ...
    dic_size ^ (num_dic - 3) * s2 + ...
    0.5 * dic_size ^ (num_dic - 2) * s3;
ours = ours / 


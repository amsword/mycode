dim = 5;

P = rand(dim, dim);
P = (P + P') / 2;
A = rand(dim - 3, dim);
b = rand(size(A, 1), 1);
q = rand(dim, 1);
%%
true_x = [P A'; A zeros(size(A, 1), size(A, 1))] \ [-q; b];
true_x = true_x(1 : dim);

k = 1;
clear v;
clear diff_x;
t2 = inv(P) - P \ A' * inv(A * inv(P) * A') * A * inv(P);
app_x2 = t2 * (-q);
for mu = 10 .^ [3 : 10]
    t1 =  inv(P + mu * (A' * A));
    app_x = (P + mu * (A' * A)) \ (mu * A' * b - q);
    
    v(k) = norm(t1 - t2, 'fro');
    diff_x(k) = norm(app_x - true_x, 'fro');
    k = k + 1;
end
figure;
plot(diff_x);

% figure;
% plot(v);
%%



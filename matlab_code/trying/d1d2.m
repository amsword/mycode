for i = 1 : 10000;
dim = 4;

D1 = rand(dim, dim);
d1 = rand(dim, 1);
d2 = rand(dim, 1);
right = d1 * ones(1, numel(d2)) + ones(numel(d1), 1) * d2';
D2 = D1' \ right;
%%
func = @(a)(norm((D1 + a * ones(1, dim))' * (D2 - a * ones(1, dim)), 'fro'));
% func = @(a)(-trace((D1 + a * ones(1, dim))' * (D2 - a * ones(1, dim))));
clear options
options.MaxFunEvals = 10^9;
options.TolX = 10^-9;
options.MaxIter = 10^5;
options.TolFun = 10^-9;
options.Display = 'iter';
[astar, obj, r] = fminsearch(func, rand(dim, 1) - 0.5, options);
% [astar, obj, r] = fsolve(func, rand(dim, 1), options);
if obj > 0.001
['obj' num2str(func(astar))]
break;
end
end

%%
a = astar;
(D1 + a * ones(1, dim))' * (D2 - a * ones(1, dim))
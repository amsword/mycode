#include <mex.h>
#include <time.h>

void throw_excep()
{
    const int num_dic = 32;
    const int dic_size = 256 * 256;
    double** pp = new double*[num_dic];
    for (int i = 0; i < num_dic; i++)
    {
        pp[i] = new double[dic_size];
        for (int j = 0; j < dic_size; j++)
        {
            pp[i][j] = rand();
        }
    }
    
    clock_t begin = clock();
    double* result = new double[dic_size];
    for (int i = 0; i < dic_size; i++)
    {
        result[i] = 0;
    }
    for (int i = 0; i < num_dic; i++)
    {
        for (int j = 0; j < dic_size; j++)
        {
            result[j] += pp[i][j];
        }
    }
    
    clock_t end = clock();
    printf("%f\n", (end - begin) / (double)CLOCKS_PER_SEC);
    
    
    
}
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, 
  const mxArray *prhs[])
{
    try
    {
        throw_excep();
    }
    catch( int e)
    {
        printf("%d\n", e);
    }
    
}
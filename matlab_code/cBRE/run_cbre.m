%% distance calculation
str_distance = gnd_file.DistanceTrainTrainBin;
if ~exist(str_distance, 'file')
    calc_large_distance(Xtraining, str_distance);
end

str_sorted = gnd_file.SortedDistanceTrainTrainBin;
if ~exist(str_sorted, 'file')
    calc_large_sorted_distance(str_distance, str_sorted);
end
%%
if is_train

    train_cbre;
    save(save_file.train_linear_bre, 'W_bre');
end

eval_linear_bre = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    eval_linear_bre{k} = eval_hash5(m, 'linear', ...
        W_bre, Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end

save(save_file.test_linear_bre, 'eval_linear_bre');

return;

%% performance on the training set
Straintrain = load_gnd2(gnd_file.SortedDistanceTrainTrainBin, 400);
W = W_bre;

all_eval = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
        Xtraining, Xtraining, ...
        Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    all_eval{k} = curr_eval;
end
eval_linear_bre_train = all_eval;
save(save_file.test_linear_bre, 'eval_linear_bre_train', '-append');

%% objective value on all of the points
% squared_dist = read_mat('\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\DistanceTrainTrain.double.bin', 'double');
squared_dist = read_mat('\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\PeekaboomOrigin\data\c_data\DistanceTrainTrain.double.bin', 'double');
squared_dist = squared_dist .^ 2;

X = [Xtraining; ones(1, size(Xtraining, 2))];
W = W_bre;
[obj_bre, ratio_bre] = bre_obj_all(W, X, squared_dist);

save('info', 'obj_bre', 'ratio_bre');
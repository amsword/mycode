function calc_large_sorted_distance(str_distance, str_sorted)
%% I: for cbh
%   mlh_I, for mlh

fpread = fopen(str_distance, 'rb');
fpwrite = fopen(str_sorted, 'wb');

N1 = fread(fpread, [1 1], 'int32');
N2 = fread(fpread, [1 1], 'int32');
fwrite(fpwrite, N1, 'int32');
fwrite(fpwrite, N2, 'int32');

max_size = 0.1 * 1024^3 / 8;
batchsize = min(floor(max_size / N2), N1);
nbath = ceil(N1 / batchsize);

for i = 1 : nbath
    ['i: ' num2str(i) ' total: ' num2str(nbath)]
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, N1);

    dist = fread(fpread, [N2, idx_end - idx_start + 1], 'double');
    [s_dist, s_idx] = sort(dist, 1);
	s_idx = s_idx - 1;
    
    for j = 1 : (idx_end - idx_start + 1)
        fwrite(fpwrite, N2, 'int32');
        fwrite(fpwrite, s_idx(:, j), 'int32');
        fwrite(fpwrite, s_dist(:, j), 'double');
    end
end
fclose(fpread);
fclose(fpwrite);
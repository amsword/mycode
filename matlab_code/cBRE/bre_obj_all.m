function [y r] = bre_obj_all(W, X, squared_distance)

wx = W' * X;
bwx = wx > 0;
bwx = double(bwx);

N = size(X, 2);

batch_size = 4010;
batch_num = ceil(N / batch_size);

coef = max(squared_distance(:)) / size(W, 2);

r = 0;
y = 0;
count = 0;
for k = 1 : batch_num
    [num2str(k) '/' num2str(batch_num)]
    idx_begin = (k - 1) * batch_size + 1;
    idx_end = idx_begin + batch_size - 1;
    idx_end = min(idx_end, N);
    
    query = bwx(:, idx_begin : idx_end);
    hamm = jf_distMat(query, bwx);
    hamm = hamm .^ 2;
    
    euc = squared_distance(idx_begin : idx_end, :);
    
    diff = coef * hamm(:) - euc(:);
    
    selected = euc(:) > 0.001;
        
    ratio = abs(diff(selected) ./ euc(selected));
    
    count = count + numel(ratio);
    r = r + sum(ratio);
    y = y + sum(diff .^ 2);
    
end
y = sqrt(y /  N / N);
r = r / count;

function [obj, a, b] = calculate_objective(Xtraining, Btraining, xquery, bquery, type)


dist = jf_distMat(xquery, Xtraining);
dist = dist .^ 2;

hamming_dist = jf_distMat(bquery, Btraining);
hamming_dist = hamming_dist .^ 2;

if strcmp(type, 'constant')
    a = max(dist) / 32;
    a = 0.269688;
    a = 4.3150 / size(Btraining, 1);
    b = 0;
elseif strcmp(type, 'slope')
    a = sum(hamming_dist .* dist) / sum(hamming_dist .^ 2);
    b = 0;
elseif strcmp(type, 'slope_displacement')
    l11 = sum(hamming_dist .^ 2);
    l12 = sum(hamming_dist);
    l22 = numel(hamming_dist);
    right1 = sum(hamming_dist .* dist);
    right2 = sum(dist);
    x = [l11 l12; l12 l22] \ [right1; right2];
    a = x(1);
    b = x(2);
end
% obj_l1 = sum(abs(a * hamming_dist + b - dist));
% ['l1-obj: ' num2str(obj_l1)]

obj = sum((a * hamming_dist + b - dist) .^ 2) / size(Xtraining, 2);
obj = sqrt(obj);
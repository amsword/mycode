function obj = plot_objecitve(str_log)
fp = fopen(str_log, 'r');

t = 'BinaryReconstructHashing.cpp:Line:914';

count  = numel(t);

idx_obj = 1;
while ~feof(fp)
    str = fgetl(fp);
    if numel(str) > count && ...
            strcmp(str(1 : count), t)
        obj(idx_obj) = sscanf(str, '%*s%*f%*f%f');
        idx_obj = idx_obj + 1;
    end
end

plot(obj);
fclose(fp);
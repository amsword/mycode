function B = F_distance_alignment(Xtraining, m, para)

str_distance = para.str_distance;
str_sorted = para.str_sorted;
k1 = para.k1;
k2 = para.k2;
k3 = para.k3;
str_selected = para.selected;
str_sym = para.str_sym;
str_binary = para.str_binary;
epsilon2 = para.epsilon2;

calc_large_distance(Xtraining, ...
    str_distance);
% 
calc_large_sorted_distance(str_distance, ...
    str_sorted);
% 
select_dist(str_sorted, ...
    str_selected, k1, k2, k3);
% 
system(['exeSymDistance ' str_selected ' ' str_sym]);

system(['exeDistanceAlignment ' str_sym ' ' ...
    num2str(m) ' ' num2str(epsilon2) ' ' str_binary]);

B = read_mat(str_binary, 'uint8');
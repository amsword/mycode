%%
k = 5;
i = 1;
plot(all_result_power2{i}.eval{k}.avg_retrieved, all_result_power2{i}.eval{k}.rec, 'b'); hold on;
plot(all_result{i}.eval{k}.avg_retrieved, all_result{i}.eval{k}.rec, 'r'); i = i + 1;
% plot(all_result{idx}.eval{k}.avg_retrieved, all_result{idx}.eval{k}.rec, 'g'); idx = idx + 1;
% plot(all_result{idx}.eval{k}.avg_retrieved, all_result{idx}.eval{k}.rec, 'k'); idx = idx + 1;
% plot(all_result{idx}.eval{k}.avg_retrieved, all_result{idx}.eval{k}.rec, 'c'); idx = idx + 1;
% plot(all_result{idx}.eval{k}.avg_retrieved, all_result{idx}.eval{k}.rec, 'y'); idx = idx + 1;
grid on;

axis([0 1000 0 1]);

%%
Btraining = cell(numel(all_result), 1);
Btest = cell(numel(all_result), 1);
for k = 1 : numel(all_result)
    Btraining{k} = all_result{k}.W' * [Xtraining; ones(1, size(Xtraining, 2))] > 0;
    Btraining{k} = double(Btraining{k});
    Btest{k} = all_result{k}.W' * [Xtest; ones(1, size(Xtest, 2))] > 0;
    Btest{k} = double(Btest{k});
end
%%
s = 2000;
plot_diff_dist(Xtraining, Btraining{1}, Xtraining(:, s), Btraining{1}(:, s));
plot_diff_dist(Xtraining, Btraining{2}, Xtraining(:, s), Btraining{2}(:, s));
plot_diff_dist(Xtraining, Btraining{3}, Xtraining(:, s), Btraining{3}(:, s));
pause;
close all;

clc;
calculate_objective(Xtraining, Btraining{1}, Xtraining(:, s), Btraining{1}(:, s), 'constant')
calculate_objective(Xtraining, Btraining{2}, Xtraining(:, s), Btraining{2}(:, s), 'slope')
calculate_objective(Xtraining, Btraining{3}, Xtraining(:, s), Btraining{3}(:, s), 'slope_displacement')


%%
s = 1000;
plot_diff_dist(Xtraining, Btraining{1}, Xtest(:, s), Btest{1}(:, s));
plot_diff_dist(Xtraining, Btraining{2}, Xtest(:, s), Btest{2}(:, s));
plot_diff_dist(Xtraining, Btraining{3}, Xtest(:, s), Btest{3}(:, s));
pause;
close all;
clc;
calculate_objective(Xtraining, Btraining{1}, Xtest(:, s), Btest{1}(:, s), 'constant')
calculate_objective(Xtraining, Btraining{2}, Xtest(:, s), Btest{2}(:, s), 'slope')
calculate_objective(Xtraining, Btraining{3}, Xtest(:, s), Btest{3}(:, s), 'slope_displacement')
%%
clc;
s = 200;
plot_diff_obj(Xtraining, Btraining{1}, Xtest(:, s), Btest{1}(:, s), 'constant')
plot_diff_obj(Xtraining, Btraining{2}, Xtest(:, s), Btest{2}(:, s), 'slope')
plot_diff_obj(Xtraining, Btraining{3}, Xtest(:, s), Btest{3}(:, s), 'slope_displacement')

pause;
close all;

%%
clc;
s = 3;

plot_diff_dist(Xtraining, Btraining{1}, Xtraining(:, s), Btraining{1}(:, s));
plot_diff_dist(Xtraining, Btraining{2}, Xtraining(:, s), Btraining{2}(:, s));
plot_diff_dist(Xtraining, Btraining{3}, Xtraining(:, s), Btraining{3}(:, s));

plot_diff_obj(Xtraining, Btraining{1}, Xtraining(:, s), Btraining{1}(:, s), 'constant')
plot_diff_obj(Xtraining, Btraining{2}, Xtraining(:, s), Btraining{2}(:, s), 'slope')
plot_diff_obj(Xtraining, Btraining{3}, Xtraining(:, s), Btraining{3}(:, s), 'slope_displacement')

pause;
close all;
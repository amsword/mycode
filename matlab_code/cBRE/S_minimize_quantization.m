function W = S_minimize_quantization(Xtraining, B)
% Xtraining: d * N
% B: m * N

W = (Xtraining * Xtraining') \ (Xtraining * B');

W = [W; zeros(1, size(B, 1))];
function obj = plot_diff_dist(Xtraining, Btraining, xquery, bquery)

dist = jf_distMat(xquery, Xtraining);
dist = dist .^ 2;

[s_dist s_idx] = sort(dist, 2);

s_dist = mean(s_dist, 1);

% weight = exp(-dist / epsilon2);

hamming_dist = jf_distMat(bquery, Btraining);
hamming_dist = hamming_dist .^ 2;

avg = zeros(1, size(hamming_dist, 2));
for i = 1 : size(hamming_dist, 1)
    avg = avg + hamming_dist(i, s_idx(i, :));
end
avg = avg / size(hamming_dist, 1);


total = numel(s_dist);
max_used = 10^4;
if (total > max_used)
    inter = floor(total / max_used);
    selected = 1 : inter : total;
else
    selected = 1 : total;
end

figure
% 
% subplot(2, 1, 1);
% plot(s_dist(selected), 'b'); 
% 
% subplot(2, 1, 2);
plot(hamming_dist(s_idx(selected)), 'r.');
ylim([0, 16]);

calculate_objective(Xtraining, Btraining, xquery, bquery, 'constant')

% error = (dist - lambda * hamming_dist) .^ 2;
% plot(error(s_idx), 'k.');
% error = error .* weight;
% obj = sum(error);

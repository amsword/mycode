function cObjective(para)

str_training = para.str_training;
str_sym_dist = para.str_sym_dist;
code_length = para.code_length;
dist_power = para.dist_power;
epsilon_coef = para.epsilon_coef;
is_slope = para.is_slope;
is_displacement = para.is_displacement;
sim_type = para.sim_type;
str_w_in = para.str_w_in;
str_w_out = para.str_w_out;
max_iter = para.max_iter;
max_inner_iter = para.max_inner_iter;
max_second = para.max_second;
obj_power = para.obj_power;
is_kernel = para.is_kernel;
kernel_coef = para.kernel_coef;

cmd = 'C:\Users\v-jianfw\Desktop\v-jianfw\HashCode\cBRE\exeObjectiveValue ';
cmd = [cmd str_training ' '];
cmd = [cmd str_sym_dist ' '];
cmd = [cmd num2str(code_length) ' '];
cmd = [cmd num2str(dist_power) ' '];
cmd = [cmd num2str(epsilon_coef) ' '];
cmd = [cmd num2str(is_slope) ' '];
cmd = [cmd num2str(is_displacement) ' '];
cmd = [cmd num2str(sim_type) ' '];
cmd = [cmd str_w_in ' '];
cmd = [cmd str_w_out ' '];
cmd = [cmd num2str(max_iter) ' '];
cmd = [cmd num2str(max_inner_iter) ' '];
cmd = [cmd num2str(max_second) ' '];
cmd = [cmd num2str(obj_power) ' '];
cmd = [cmd num2str(is_kernel) ' '];
cmd = [cmd num2str(kernel_coef)];

system(cmd);
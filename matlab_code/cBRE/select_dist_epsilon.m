function select_dist_epsilon(str_sorted, str_selected, e1, e3)
% k1, k2, k3
fpread = fopen(str_sorted, 'rb');

N = fread(fpread, [1 1], 'int32');
fread(fpread, [1 1], 'int32');

fpwrite = fopen(str_selected, 'wb');
fwrite(fpwrite, N, 'int32');
fwrite(fpwrite, N, 'int32');

for i = 1 : N
    if (mod(i, 1000) == 0)
        [num2str(i) '/' num2str(N)]
    end
    
    fread(fpread, [1 1], 'int32');
    sub_idx = fread(fpread, N, 'int32');
    sub_dist = fread(fpread, N, 'double');
    
    assert(isempty(find(sub_idx < 0 | sub_idx >= N)));
    
    selected = sub_dist < e1 | sub_dist > e3;
    idx_se = sub_idx(selected);
    dist_se = sub_dist(selected);
    
    fwrite(fpwrite, sum(selected), 'int32');
    fwrite(fpwrite, idx_se, 'int32');
    fwrite(fpwrite, dist_se, 'double');
end
fclose(fpread);
fclose(fpwrite);
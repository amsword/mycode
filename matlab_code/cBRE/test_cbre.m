all_num = 0;
all_value = 0;
for i = 1 : numel(mat)
    all_num = all_num + numel(mat{i}.idx);
    all_value = all_value + sum(mat{i}.dist);
end

all_value / all_num
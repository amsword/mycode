function obj = plot_diff_obj(Xtraining, Btraining, xquery, bquery, type)

dist = jf_distMat(xquery, Xtraining);
dist = dist .^ 2;

[s_dist s_idx] = sort(dist, 2);

hamming_dist = jf_distMat(bquery, Btraining);
hamming_dist = hamming_dist .^ 2;

[s_hamming s_idx_hamming] = sort(hamming_dist);

[inner] = intersect(s_idx(1 : 50), s_idx_hamming(1 : 1000));
% ['recall: ' num2str(numel(inner) / 50)]

[~, ~, idx_in_sh] = intersect(s_idx(1 : 50), s_idx_hamming);



total = numel(s_dist);
max_used = 10^4;
if (total > max_used)
    inter = floor(total / max_used);
    selected = 1 : inter : total;
else
    selected = 1 : total;
end

selected = 1 : total;

[obj, a, b] = calculate_objective(Xtraining, Btraining, xquery, bquery, type);

% figure;
% plot(hamming_dist(s_idx(selected)), 'g.');

figure
plot(a * hamming_dist(s_idx(selected)) + b, 'r.');
hold on;
plot(s_dist(selected), 'b.');
% plot((a * hamming_dist(s_idx(selected)) + b - s_dist(selected)) .^ 2, 'k.');

% figure
% plot(1 - exp(-s_dist / 1), 'k');

% figure;
% hold on;
% plot(s_hamming, 'b.');
% plot(idx_in_sh, s_hamming(idx_in_sh), 'ro');

% figure; hold on;
% plot(dist(s_idx_hamming), 'g.');
% plot(idx_in_sh, dist(s_idx_hamming(idx_in_sh)), 'b.');
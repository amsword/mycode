%%
str_distance = [src_file.c_folder 'DistanceTrainTrain.double.bin'];
str_sorted = [src_file.c_folder 'SortedDistance.double.bin'];
k1 = 50;
k2 = 50;
k3 = Ntraining - 50;
k3 = 50;
str_selected = src_file.c_folder;

is_redistance = 0; % 0:none; 1 : 0:N-1; 2 : the first is set 0

% k1 = 100; k2 = 100; k3 = 99;
% k1 = 0; k2 = 150; k3 = 0;
% k1 = 75; k2 = 0; k3 = 75;
% k1 = 100; k2 = 0; k3 = 40;

if is_redistance == 0
    str_selected = [str_selected 'selected.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
    str_sym = src_file.c_folder;
    str_sym = [str_sym 'sym.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
elseif is_redistance == 1
    str_selected = [str_selected '1_selected.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
    str_sym = src_file.c_folder;
    str_sym = [str_sym '1_sym.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
elseif is_redistance == 2
    str_selected = [str_selected '2_selected.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
    str_sym = src_file.c_folder;
    str_sym = [str_sym '2_sym.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
end
% e1 = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, k1);
% e3 = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, k3);
% str_selected = [str_selected 'selected.' num2str(e1) '.' num2str(e3) '.double.bin'];
% str_sym = src_file.c_folder;
% str_sym = [str_sym 'sym.' num2str(e1) '.' num2str(e3) '.double.bin'];

%% calculate the distance
calc_large_distance(Xtraining, str_distance);

% sort the distance
calc_large_sorted_distance(str_distance, str_sorted);
%%
% select part of the distance, not all of them is used
if ~is_redistance
    select_dist(str_sorted, str_selected, k1, k2, k3);
    % select_dist_epsilon(str_sorted, str_selected, e1, e3);
elseif is_redistance == 1
    re_distance_select(str_sorted, str_selected, k1, k2, k3);
elseif is_redistance == 2
    re_distance_select(str_sorted, str_selected, k1, k2, k3, 2);
end
% symmetric it
system(['\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\HashCode\cBRE\exeSymDistance ' str_selected ' ' str_sym]);

%%
if is_pca
    if ~exist([src_file.pca_mat num2str(pca_dim)])
        'generate reduced training data'
        [V, D] = eig(cov(Xtraining'));
        W_pca = V(:, end - pca_dim + 1 : end);
        
        % random matrix
        U = randn(pca_dim, pca_dim);
        [U, ~, ~] = svd(U);
        W_pca = W_pca * U;
        
        save_mat(W_pca, [src_file.pca_mat num2str(pca_dim)], 'double');
        
        pcaXtraining = W_pca' * Xtraining;
        save_mat(pcaXtraining, [src_file.pca_c_train num2str(pca_dim)], 'double');
    else
        'nothing'
    end
else
    'nothing'
end


%%
if is_pca
    'is-pca'
    pca_dim
    para.str_training = [src_file.pca_c_train num2str(pca_dim)];
else
    'no-pca'
    para.str_training = src_file.c_train;
end

str_init_w = ['initw'];

para.str_sym_dist = str_sym;
para.code_length = m;
para.dist_power = 2;
para.epsilon_coef = 2;
para.is_slope = 0;
para.is_displacement = 0;
para.sim_type = 0;
para.str_w_in = [str_init_w];
para.str_w_out = [pwd '\' save_file.train_c_bre_2000];
para.max_iter = 10;
para.max_inner_iter = 200;
para.max_second = 3600 * 10;
para.obj_power = 2;
para.is_kernel = 0;
para.kernel_coef = 0;

if ~exist([str_init_w], 'file')
    'random generate init value'
    if is_pca
        W = randn(pca_dim, m);
    else
        D = size(Xtraining, 1);
        W = randn(D, m);
    end
    if is_init_orth
        'orth'
        [U, ~, ~] = svd(W);
        W = U(:, 1 : m);
    end
    save_mat(W, str_init_w, 'double');
end
%%

clear all_para;
idx = 1; % base line
all_para{idx} = para;
if is_pca
    all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2000 'pca' num2str(pca_dim) num2str(k1) '_' num2str(k2) '_' num2str(k3)];
else
    all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2000 num2str(k1) '_' num2str(k2) '_' num2str(k3)];
end
all_epsilon_coef = [0.2 0.4 0.6 0.8 1 1.2 1.4 1.6]; % for labelme
all_epsilon_coef = [0.6 0.8 1 1.2 1.4 1.6 1.8 2.0 2.2];
idx = idx + 1; % sim_type = 2
for i = 1 : numel(all_epsilon_coef)
    all_para{idx} = para;
    all_para{idx}.sim_type = 2;
    all_para{idx}.epsilon_coef = all_epsilon_coef(i);
    if is_pca
        all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2002 num2str(all_epsilon_coef(i)) 'pca' num2str(pca_dim) num2str(k1) '_' num2str(k2) '_' num2str(k3)];
    else
        all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2002 num2str(all_epsilon_coef(i)) '_' num2str(k1) '_' num2str(k2) '_' num2str(k3)];
    end
    idx = idx + 1;
end

numel(all_para)

%%
clear all_para;

all_kernel_coef = [0.2 0.4 0.6 0.8 1 1.2 1.4 1.6 1.8 2];
all_para = cell(numel(all_kernel_coef), 1);
for idx = 1 : numel(all_kernel_coef)
    all_para{idx} = para;
    all_para{idx}.is_kernel = 1;
    all_para{idx}.sim_type = 0;
    all_para{idx}.kernel_coef = all_kernel_coef(idx);
    if is_pca
        all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2002 'kernel' num2str(all_kernel_coef(idx)) 'pca' num2str(pca_dim) num2str(k1) '_' num2str(k2) '_' num2str(k3)];
    else
        all_para{idx}.str_w_out = [pwd '\' save_file.train_c_bre_2002 'kernel' num2str(all_kernel_coef(idx)) num2str(k1) '_' num2str(k2) '_' num2str(k3)];
    end
end

numel(all_para)

%%
clear all_para;

for idx = 1 : 2
    all_para{idx} = para;
    all_para{idx}.is_kernel = 0;
    all_para{idx}.sim_type = 0;
    all_para{idx}.dist_power = idx;
    all_para{idx}.str_w_out = [pwd '\' num2str(idx)];
    all_para{idx}.max_second = 3600 * 1;
end

numel(all_para)



%%

all_result = cell(numel(all_para), 1);
parfor idx = 1 : numel(all_result)
    elaps = rand() * 20;
    pause(elaps);
    
    curr_para = all_para{idx};
    all_result{idx}.para = curr_para;
    if exist(curr_para.str_w_out, 'file')
        W = read_mat(curr_para.str_w_out, 'double');
        all_result{idx}.W = [W; zeros(1, size(W, 2))];
        error('dfsd');
    else
        if is_pca
            all_result{idx}.W = cBRE(curr_para, [src_file.pca_mat num2str(pca_dim)]);
        else
            all_result{idx}.W = cBRE(curr_para);
        end
    end
    
    curr_para = all_result{idx}.para;
    
    curr_para.str_sym_dist = ...
        convert_file_name_2_02(curr_para.str_sym_dist);
    curr_para.str_training = ...
        convert_file_name_2_02(curr_para.str_training);
    curr_para.str_w_out = ...
        convert_file_name_2_02(curr_para.str_w_out);
    
    curr_para.max_iter = 50;
    curr_para.max_inner_iter = 50;
    
    curr_para.max_second = 3600 * 5;
    curr_para.is_slope = 1;
    curr_para.str_w_in = curr_para.str_w_out;
    curr_para.str_w_out = [curr_para.str_w_out '_1_0'];
    all_result{idx}.slope.para = curr_para;
    
    if exist(curr_para.str_w_out, 'file')
        W = read_mat(curr_para.str_w_out, 'double');
        all_result{idx}.slope.W = [W; zeros(1, size(W, 2))];
        error('dfsd');
    else
        if is_pca
            all_result{idx}.slope.W = cBRE(curr_para, [src_file.pca_mat num2str(pca_dim)]);
        else
            all_result{idx}.slope.W = cBRE(curr_para);
        end
    end
    
    curr_para.max_second = 3600 * 5;
    curr_para.is_displacement = 1;
    curr_para.str_w_in = curr_para.str_w_out;
    curr_para.str_w_out = [curr_para.str_w_out '_1_1'];
    all_result{idx}.slope_dis.para = curr_para;
    if exist(curr_para.str_w_out, 'file')
        W = read_mat(curr_para.str_w_out, 'double');
        all_result{idx}.slope_dis.W = [W; zeros(1, size(W, 2))];
        error('dfsd');
    else
        if is_pca
            all_result{idx}.slope_dis.W = cBRE(curr_para, [src_file.pca_mat num2str(pca_dim)]);
        else
            all_result{idx}.slope_dis.W = cBRE(curr_para);
        end
    end
    
end

save('tmp', 'all_result');

%%
all_para{1} = para;
all_result = cell(numel(all_para), 1);
parfor idx = 1 : numel(all_result)
       
    curr_para = all_para{idx};
    all_result{idx}.para = curr_para;
    all_result{idx}.W = cBRE(curr_para);
  
end

save('tmp', 'all_result');


%%
for i = 1 : numel(all_result)
    all_result{i} = rmfield(all_result{i}, 'slope');
    all_result{i} = rmfield(all_result{i}, 'slope_dis');
end
%%
can_cared = 1000;
parfor idx = 1 : numel(all_result)
    if ~isfield(all_result{idx}, 'eval')
        W = all_result{idx}.W;
        curr_eval = cell(numel(all_topks), 1);
        for k = 1 : numel(all_topks)
            curr_eval{k} = eval_hash10(...
                size(W, 2), 'linear', W, ...
                Xtest, Xtraining, ...
                StestBase2', all_topks(k), ...
                [], [], metric_info, eval_types, can_cared);
        end
        all_result{idx}.eval = curr_eval;
    end
    if isfield(all_result{idx}, 'slope')
        W = all_result{idx}.slope.W;
        curr_eval = cell(numel(all_topks), 1);
        for k = 1 : numel(all_topks)
            curr_eval{k} = eval_hash10(...
                size(W, 2), 'linear', W, ...
                Xtest, Xtraining, ...
                StestBase2', all_topks(k), ...
                [], [], metric_info, eval_types, can_cared);
        end
        all_result{idx}.slope.eval = curr_eval;
    end
    
    if isfield(all_result{idx}, 'slope_dis')
        W = all_result{idx}.slope_dis.W;
        curr_eval = cell(numel(all_topks), 1);
        for k = 1 : numel(all_topks)
            curr_eval{k} = eval_hash10(...
                size(W, 2), 'linear', W, ...
                Xtest, Xtraining, ...
                StestBase2', all_topks(k), ...
                [], [], metric_info, eval_types, can_cared);
        end
        all_result{idx}.slope_dis.eval = curr_eval;
    end
    
end

save([type num2str(m)], 'all_result');


%%
can_cared = 1000;
for idx = 1 : numel(all_result)
    if ~isfield(all_result{idx}, 'eval')
        W = all_result{idx}.W;
        curr_eval = cell(numel(all_topks), 1);
        parfor k = 1 : numel(all_topks)
            curr_eval{k} = eval_hash10(...
                size(W, 2), 'linear', W, ...
                Xtest, Xtraining, ...
                StestBase2', all_topks(k), ...
                [], [], metric_info, eval_types, can_cared);
        end
        all_result{idx}.eval = curr_eval;
    end
end

save([type num2str(m)], 'all_result');

%
% Straintrain = load_gnd2('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\SortedDistance.double.bin', 400);
% Straintrain = load_gnd2('\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\PeekaboomOrigin\data\c_data\SortedDistance.double.bin', 400);
% 
cared_num = 1000;
for  i = 1 : numel(all_result)
    
    W = all_result{i}.W;
    
    all_eval = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        curr_eval = eval_hash10(size(W, 2), 'linear', W, ...
            Xtraining, Xtraining, ...
            Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], ...
            metric_info, eval_types, cared_num);
        all_eval{k} = curr_eval;
        
    end
    all_result{i}.eval_train = all_eval;
end
save([type num2str(m)], 'all_result');


%%

load(save_file.train_itq);
load(save_file.test_itq);

load(save_file.train_lsh);
load(save_file.test_lsh);
W = W_lsh;

% W = W_itq;

cared_num = 1000;

all_eval = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash10(size(W, 2), 'linear', W, ...
        Xtraining, Xtraining, ...
        Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types, cared_num);
    all_eval{k} = curr_eval;
end
% eval_itq_train = all_eval;
eval_lsh_train = all_eval;

% curr_eval = cell(numel(all_topks), 1);
% parfor k = 1 : numel(all_topks)
%     curr_eval{k} = eval_hash10(...
%         size(W, 2), 'linear', W, ...
%         Xtest, Xtraining, ...
%         StestBase2', all_topks(k), ...
%         [], [], metric_info, eval_types, can_cared);
% end
% eval_lsh = curr_eval;

save(save_file.test_lsh, 'eval_lsh', 'eval_lsh_train');
% save(save_file.test_itq, 'eval_itq', 'eval_itq_train');


%%
load(save_file.test_itq);
load(save_file.test_lsh);
for k = 1 : 6
    figure;
    for idx = 1 : numel(all_result)
        hold on;
%         plot(all_result{idx}.eval{k}.rec, all_result{idx}.eval{k}.pre, 'ro-');
%             plot(all_result{idx}.eval{k}.avg_retrieved, all_result{idx}.eval{k}.rec, 'ro-');
%             plot(all_result{idx}.eval_train{k}.avg_retrieved, all_result{idx}.eval_train{k}.rec, 'ro-');
        plot(all_result{idx}.eval_train{k}.rec, all_result{idx}.eval_train{k}.pre, 'ro-');
    end
%     plot(eval_itq{k}.r_code, 'b*-');
    assert(numel(eval_itq{k}.rec) == m + 1);
%     plot(eval_itq{k}.rec, eval_itq{k}.pre, 'b*-'); 
%     plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'b*-');
%     plot(eval_itq_train{k}.avg_retrieved, eval_itq_train{k}.rec, 'b*-');
    plot(eval_itq_train{k}.rec, eval_itq_train{k}.pre, 'b*-');

    assert(numel(eval_lsh{k}.rec) == m + 1);
%     plot(eval_lsh{k}.rec, eval_lsh{k}.pre, 'kd-');
%     plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'kd-');
%     plot(eval_lsh_train{k}.avg_retrieved, eval_lsh_train{k}.rec, 'kd-');
    plot(eval_lsh_train{k}.rec, eval_lsh_train{k}.pre, 'kd-');
%     title(num2str(all_topks(k)));
%     xlim([0 1000]);
    legend('BRE', 'ITQ', 'LSH', 'Location', 'Best');

%     xlabel('Number of retrieved points', 'FontSize', 14);
    xlabel('Recall', 'FontSize', 14);
    ylabel('Precision', 'FontSize', 14);
%     ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    grid on;
%     saveas(gca, ['pr_' num2str(all_topks(k)) '.bmp']);
%     saveas(gca, ['avg_rec_' num2str(all_topks(k)) '.bmp']);
%     saveas(gca, ['train_train_avg_rec_' num2str(all_topks(k)) '.bmp']);
        saveas(gca, ['train_train_pr' num2str(all_topks(k)) '.bmp']);
end

pause;
close all;


%%
x = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_5_20_8_20_5_train_itq64_0.mat');
save_mat(x.W_itq(1 : 512, :), 'itq_w64', 'double');

itq_obj_para = curr_para;
itq_obj_para.str_w_out = 'itq_w64';
itq_obj_para.str_sym_dist = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\SortedDistance.double.bin';
cObjective(itq_obj_para);

%%
[save_file save_figure] = train_save_file2(type, m, is_pca, file_pre_date);

eval_lsh = load(save_file.test_lsh, 'eval_lsh');
eval_lsh = eval_lsh.eval_lsh;

%%
all_topks = [1 10 100 200 400 50];
for k = 1 : 6
    figure
    hold on;
    idx = 1;
    
    best_v = 0;
    best_idx = 0;
    
    for idx = 1 : numel(all_result)
        v = sum(all_result{idx}.eval{k}.r_code(1 : 2000));
        if (v > best_v)
            best_v = v;
            best_idx = idx;
        end
        v = sum(all_result{idx}.slope.eval{k}.r_code(1 : 2000));
        if (v > best_v)
            best_v = v;
            best_idx = idx;
        end
        v = sum(all_result{idx}.slope_dis.eval{k}.r_code(1 : 2000));
        if (v > best_v)
            best_v = v;
            best_idx = idx;
        end
    end
%     best_idx = 2;
    
    plot(all_result{1}.eval{k}.avg_retrieved, all_result{1}.eval{k}.rec, 'b');
    
    plot(all_result{best_idx}.eval{k}.avg_retrieved, all_result{best_idx}.eval{k}.rec, 'm');
    plot(all_result{best_idx}.slope.eval{k}.avg_retrieved, all_result{best_idx}.slope.eval{k}.rec, 'k');
    plot(all_result{best_idx}.slope_dis.eval{k}.avg_retrieved, all_result{best_idx}.slope_dis.eval{k}.rec, 'r');
    
    plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'g');
     
% plot(nopca_best.result_best.eval{k}.r_code, 'b');
% plot(pca_best.result_best.eval{k}.r_code, 'r');
% 
    legend('ori', 'wbre', 'slope', 'sd', ...
        'itq', ...
        'Location', 'Best');
        xlim([0 2000]);
    xlabel('Number of Retrieved Points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    
    grid on;
    set(gca, 'FontSize', 14);
%     saveas(gca, ['all_result{2}_avg_rec' num2str(all_topks(k)) '.eps'], 'psc2');
    
end

pause;
close all;

%%
result_base = all_result{1};
result_best = all_result{7};
save('result_base', 'result_base');
save('result_best', 'result_best');
%%
save_mat(all_result{1}.slope.W(1 : end - 1, :), [all_result{1}.slope.para.str_w_out  '.full'], 'double');
cErrorContribution([all_result{1}.slope.para.str_w_out  '.full']);

%%
curr_para = all_result{1}.para;
curr_para.str_sym_dist = str_sym;
curr_para.obj_power = 2;
curr_para.is_kernel = 0;
curr_para.kernel_coef = 0;
% curr_para.str_w_out = 'itq';
cObjective(curr_para);
%%
cAllPairObjective(src_file.c_train, all_result{1}.para.str_w_out, str_distance)

cAllPairObjective(src_file.c_train, 'C:\Users\v-jianfw\Desktop\v-jianfw\HashCode\cBRE\itq', str_distance)
%%
cErrorContribution(all_result{1}.para.str_w_out)
%%
cErrorContribution('itq')

%%
cErrorContribution(x.all_result{1}.para.str_w_out)

%%
for k = 1 : 6
    figure;
    hold on;
    plot(all_result{1}.slope.eval{k}.rec, 'r');
    plot(all_result{1}.eval{k}.rec, 'k');
    plot(eval_itq{k}.rec, 'b');
    saveas(gca, ['rec-code' num2str(all_topks(k)) '.eps'], 'psc2');
end

pause;
close all;
%%
for k = 1 : 6
    figure;
    hold on;
    plot(all_result{1}.slope.eval{k}.avg_retrieved, all_result{1}.slope.eval{k}.rec, 'r');
    plot(all_result{1}.eval{k}.avg_retrieved, all_result{1}.eval{k}.rec, 'k');
    plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'b');
    xlim([0, 1000]);
    
    legend('s', 'o', 'itq');
    
    saveas(gca, ['avg-rec' num2str(all_topks(k)) '.eps'], 'psc2');
end

pause;
close all;
%%


%%
figure;
hold on;
[y orth] = analysis_energy(Xtraining, all_result{1}.slope.W(1 : end - 1, :));

plot(y, 'r');

[y_itq orth_itq] = analysis_energy(Xtraining, W_itq(1 : end - 1, :));
plot(y_itq, 'b');
%%
% diff = eval_itq{6}.every_r_code - all_result{1}.eval{6}.every_r_code;
diff = eval_itq_train{6}.every_r_code - all_result{1}.eval_train{6}.every_r_code;
[s_diff s_idx] = sort(diff);
plot(s_diff);
grid on;
%%
for k = 20000 : 20019
    clc;
%     tmp = s_idx(k);
    tmp = round(rand() * 20019);
    W = all_result{1}.W;
    Btraining = W(1 : end - 1, :)' * Xtraining > 0;
    Btraining = double(Btraining);
    Btest = W(1 : end - 1, :)' * Xtest > 0;
    Btest = double(Btest);
%     plot_diff_obj(Xtraining, Btraining, Xtest(:, tmp), Btest(:, tmp), 'constant')
    plot_diff_obj(Xtraining, Btraining, Xtraining(:, tmp), Btraining(:, tmp), 'constant')
    ylim([0, 4]);
    
%     eval_hash10(...
%         size(W, 2), 'linear', W, ...
%         Xtraining(:, tmp), Xtraining, ...
%         Straintrain(:, tmp)', all_topks(k), ...
%         [], [], metric_info, eval_types, can_cared);
    
%     pause;
    
    W = W_itq;
    Btraining = W(1 : end - 1, :)' * Xtraining > 0;
    Btraining = double(Btraining);
    Btest = W(1 : end - 1, :)' * Xtest > 0;
    Btest = double(Btest);
%     plot_diff_obj(Xtraining, Btraining, Xtest(:, tmp), Btest(:, tmp), 'constant')
    plot_diff_obj(Xtraining, Btraining, Xtraining(:, tmp), Btraining(:, tmp), 'constant')
    ylim([0, 4]);
    
    pause;
    close all;
end
%%
clc;
W = all_result{1}.W;
Btraining = W(1 : end - 1, :)' * Xtraining > 0;
Btraining = double(Btraining);
Btest = W(1 : end - 1, :)' * Xtest > 0;
Btest = double(Btest);
multi_point_obj(Xtraining, Btraining, Xtest, Btest, 'constant')

W = W_itq;
Btraining = W(1 : end - 1, :)' * Xtraining > 0;
Btraining = double(Btraining);
Btest = W(1 : end - 1, :)' * Xtest > 0;
Btest = double(Btest);
multi_point_obj(Xtraining, Btraining, Xtest, Btest, 'constant')

%%

for k = 1 : 6
    figure
    plot(all_result{1}.eval{k}.avg_retrieved, all_result{1}.eval{k}.rec, 'r*-');
    hold on;
    plot(all_result{2}.eval{k}.avg_retrieved, all_result{2}.eval{k}.rec, 'r*-');
    % plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'b*-');
    xlim([0, 5000]);
end

%%
% squared_dist = read_mat('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\DistanceTrainTrain.double.bin', 'double');
squared_dist = read_mat('\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\PeekaboomOrigin\data\c_data\DistanceTrainTrain.double.bin', 'double');

squared_dist = squared_dist .^ 2;
%%
X = [Xtraining; ones(1, size(Xtraining, 2))];
%%
[obj0, ratio0] = bre_obj_all(all_result{1}.W, X, squared_dist)
%
[obj1, ratio1] = bre_obj_all(W_itq, X, squared_dist)
%

[obj2, ratio2] = bre_obj_all(W_lsh, X, squared_dist)

%%
wx = W' * X;
bwx = wx > 0;
bwx = double(bwx);

N = size(X, 2);

batch_size = 200;
batch_num = ceil(N / batch_size);

coef = max(squared_dist(:)) / size(W, 2);

ham = mean(squared_dist(:));
ham = round(ham);
ham = 0;

y = sum((coef * ham(:) - squared_dist(:)) .^ 2);

%%
k = 1;
clear obj  ratio;
for m = [16, 32, 64]
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    load(save_file.train_linear_bre);
    W = W_bre;
    [obj(k), ratio(k)] = bre_obj_all(W, X, squared_dist);
    k = k + 1;
end

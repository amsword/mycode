function select_dist(str_sorted, str_selected, k1, k2, k3)
% k1, k2, k3
fpread = fopen(str_sorted, 'rb');

N = fread(fpread, [1 1], 'int32');
fread(fpread, [1 1], 'int32');

fpwrite = fopen(str_selected, 'wb');
fwrite(fpwrite, N, 'int32');
fwrite(fpwrite, k1 + k2 + k3, 'int32');

for i = 1 : N
    if (mod(i, 1000) == 0)
        [num2str(i) '/' num2str(N)]
    end
    
    fread(fpread, [1 1], 'int32');
    sub_idx = fread(fpread, N, 'int32');
    sub_dist = fread(fpread, N, 'double');
    
    assert(isempty(find(sub_idx < 0 | sub_idx >= N)));
    
    rp = randperm(N - k1 - k3, k2);
    rp = rp + k1;
    selected = [1 : k1, N - k3 + 1 : N, rp];
    idx_se = sub_idx(selected);
    dist_se = sub_dist(selected);
    
    fwrite(fpwrite, k1 + k2 + k3, 'int32');
    fwrite(fpwrite, idx_se, 'int32');
    fwrite(fpwrite, dist_se, 'double');
end
fclose(fpread);
fclose(fpwrite);
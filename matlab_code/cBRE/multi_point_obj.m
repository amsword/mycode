function [obj, a, b] = multi_point_obj(Xtraining, Btraining, xquery, bquery, type)

dist = jf_distMat(xquery, Xtraining);
dist = dist .^ 2;
[dist, s_idx] = sort(dist, 2);

hamming_dist = jf_distMat(bquery, Btraining);
hamming_dist = hamming_dist .^ 2;
for i = 1 : size(hamming_dist, 1)
    hamming_dist(i, :) = hamming_dist(i, s_idx(1, :));
end

if strcmp(type, 'constant')
    a = max(dist) / 32;
    a = 0.269688;
    b = 0;
elseif strcmp(type, 'slope')
    a = sum(hamming_dist .* dist) / sum(hamming_dist .^ 2);
    b = 0;
elseif strcmp(type, 'slope_displacement')
    l11 = sum(hamming_dist .^ 2);
    l12 = sum(hamming_dist);
    l22 = numel(hamming_dist);
    right1 = sum(hamming_dist .* dist);
    right2 = sum(dist);
    x = [l11 l12; l12 l22] \ [right1; right2];
    a = x(1);
    b = x(2);
end
% obj_l1 = sum(abs(a * hamming_dist + b - dist));
% ['l1-obj: ' num2str(obj_l1)]
selected = 1 : 20019;
obj = sum(sum((a * hamming_dist(:, selected) + b - dist(:, selected)) .^ 2));
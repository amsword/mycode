function calc_large_distance2(X1, X2, str_file)
%% I: for cbh, squared_distance

%   mlh_I, for mlh
N2 = size(X2, 2);
N1 = size(X1, 2);

max_size = 0.1 * 1024^3 / 8;
batchsize = min(floor(max_size / N2), N2);
nbath = ceil(N1 / batchsize);

fp = fopen(str_file, 'wb');
fwrite(fp, N1, 'int32');
fwrite(fp, N2, 'int32');

for i = 1 : nbath
    ['i: ' num2str(i) ' total: ' num2str(nbath)]
    idx_start = 1 + (i - 1) * batchsize;
    idx_end = idx_start + batchsize - 1;
    idx_end = min(idx_end, N1);
    dist = jf_distMat(X1(:, idx_start : idx_end), X2);

    fwrite(fp, dist', 'double');
end
fclose(fp);
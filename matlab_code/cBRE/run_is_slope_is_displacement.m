str_result{1} = '\\msra-msm-02\c$\users\v-jianfw\desktop\v-jianfw\hashcode\cBRE\16\labelme16.mat';

str_result{2} = '\\msra-msm-02\c$\users\v-jianfw\desktop\v-jianfw\hashcode\cBRE\24\labelme_24.mat';

str_result{3} = '\\msra-msm-02\c$\users\v-jianfw\desktop\v-jianfw\hashcode\cBRE\32\labelme_32.mat';

% s: slope; d: displacement
all_sd_result = cell(numel(str_result), 1);
i = 2;
x = load(str_result{i});

all_result = x.all_result;
all_result(10 : end) = [];
%%
all_result{1}.para.code_length
parfor idx = 1 : numel(all_result)
    t = rand() * 10;
    pause(t);
    
    curr_para = all_result{idx}.para;
    
    curr_para.str_sym_dist = ...
        convert_file_name_2_02(curr_para.str_sym_dist);
    curr_para.str_training = ...
        convert_file_name_2_02(curr_para.str_training);
    curr_para.str_w_out = ...
        convert_file_name_2_02(curr_para.str_w_out);
   
    curr_para.max_inner_iter = 200;
    
    curr_para.max_second = 3600 * 5;
    curr_para.is_kernel = 0;
    curr_para.kernel_coef = 0;
    curr_para.is_slope = 1;
    curr_para.str_w_in = curr_para.str_w_out;
    curr_para.str_w_out = [curr_para.str_w_out '_1_0'];
    all_result{idx}.slope.para = curr_para;
    
    if exist(curr_para.str_w_out, 'file')
        w = read_mat(curr_para.str_w_out, 'double');
        all_result{idx}.slope.W = [w; zeros(1, size(w, 2))];
    else
        all_result{idx}.slope.W = cBRE(curr_para);
    end
        
    curr_para.is_displacement = 1;
    curr_para.str_w_in = curr_para.str_w_out;
    curr_para.str_w_out = [curr_para.str_w_out '_1_1'];
    all_result{idx}.slope_dis.para = curr_para;
    if exist(curr_para.str_w_out, 'file')
        w = read_mat(curr_para.str_w_out, 'double');
        all_result{idx}.slope_dis.W = [w; zeros(1, size(w, 2))];
    else
        all_result{idx}.slope_dis.W = cBRE(curr_para);
    end
end

save('tmp', 'all_result');

parfor i = 1 : numel(all_result)
    W = all_result{i}.slope.W;
    curr_eval = cell(numel(all_topks), 1);
    for k = 1 : numel(all_topks)
        curr_eval{k} = eval_hash5(...
            size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), ...
            [], [], metric_info, eval_types);
    end
    all_result{i}.slope.eval = curr_eval;
    
    W = all_result{i}.slope_dis.W;
    curr_eval = cell(numel(all_topks), 1);
    for k = 1 : numel(all_topks)
        curr_eval{k} = eval_hash5(...
            size(W, 2), 'linear', W, ...
            Xtest, Xtraining, ...
            StestBase2', all_topks(k), ...
            [], [], metric_info, eval_types);
    end
    all_result{i}.slope_dis.eval = curr_eval;
    
end

save('labelme_16', 'all_result');

%%
for i = 1 : numel(all_result)
    w = read_mat(all_result{i}.slope.para.str_w_out, 'double');
    all_result{i}.slope.W = [w; zeros(1, m)];
end

%%


%%
for k = 1 : 6
    best_v = 0;
    best_idx = 0;
    best_sub_idx = 0;
    for i = 1 : numel(all_result)
%         figure;
%         plot(all_result{i}.eval{k}.r_code, 'b');
%         hold on;
%         plot(all_result{i}.slope.eval{k}.r_code, 'k');
%         plot(all_result{i}.slope_dis.eval{k}.r_code, 'r');
        
        v1 = sum(all_result{i}.eval{k}.r_code(1 : 2000));
        if (best_v < v1)
            best_v = v1; best_idx = i; best_sub_idx = 1;
        end
        
        v2 = sum(all_result{i}.slope.eval{k}.r_code(1 : 2000));
        if (best_v < v2)
            best_v = v2; best_idx = i; best_sub_idx = 2;
        end
        
        v3 = sum(all_result{i}.slope_dis.eval{k}.r_code(1 : 2000));
        if best_v < v3
            best_v = v3; best_idx = i; best_sub_idx = 3;
        end
        
%         xlim([0 2000]);
%         title([num2str(all_result{i}.para.sim_type) '\_' ...
%             num2str(all_result{i}.para.epsilon_coef) '\_' ...
%             num2str(all_topks(k))]);
        
%         pause;
%         close; 
    end
    
    figure;
    plot(all_result{1}.eval{k}.r_code, 'b'); hold on;
    plot(all_result{1}.slope.eval{k}.r_code, 'g');
    plot(all_result{1}.slope_dis.eval{k}.r_code, 'c');
    
    plot(all_result{best_idx}.eval{k}.r_code, 'k');
    plot(all_result{best_idx}.slope.eval{k}.r_code, 'y');
    plot(all_result{best_idx}.slope_dis.eval{k}.r_code, 'r');
    
    plot(eval_itq{k}.r_code, 'm');
    plot(eval_lsh{k}.r_code, 'g');
    
    legend('BRE', 'BRE\_s', 'BRE\_sd', ...
        'WBRE', 'WBRE\_s', 'WBRE\_sd', ...
        'ITQ', 'LSH', 'Location', 'Best');
    
    xlim([0 2000]);
    saveas(gca, [num2str(all_topks(k)) '.eps'], 'psc2');
    
%     pause;
%     close;
end
pause;
close all;
% close all;

str_distance = [src_file.c_folder 'DistanceTrainTrain.double.bin'];
str_sorted = [src_file.c_folder 'SortedDistanceTrainTrain.double.bin'];
k1 = 50;
k2 = 50;
k3 = 50;
str_selected = src_file.c_folder;

is_redistance = 0; % 0:none; 1 : 0:N-1; 2 : the first is set 0

str_selected = [str_selected 'selected.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
str_sym = src_file.c_folder;
str_sym = [str_sym 'sym.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
% e1 = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, k1);
% e3 = avg_epsilon(gnd_file.SortedDistanceTestTrainBin, k3);
% str_selected = [str_selected 'selected.' num2str(e1) '.' num2str(e3) '.double.bin'];
% str_sym = src_file.c_folder;
% str_sym = [str_sym 'sym.' num2str(e1) '.' num2str(e3) '.double.bin'];

% calculate the distance
% calc_large_distance(Xtraining, str_distance);

% sort the distance
% calc_large_sorted_distance(str_distance, str_sorted);

%     select_dist(str_sorted, str_selected, k1, k2, k3);
% system(['\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\HashCode\cBRE\exeSymDistance ' str_selected ' ' str_sym]);

para.str_training = src_file.c_train;

str_init_w = ['initw'];

para.str_sym_dist = str_sym;
para.code_length = m;
para.dist_power = 2;
para.epsilon_coef = 2;
para.is_slope = 0;
para.is_displacement = 0;
para.sim_type = 0;
para.str_w_in = [str_init_w];
para.str_w_out = [pwd '\' save_file.train_c_bre_2000];
para.max_iter = 10;
para.max_inner_iter = 200;
para.max_second = 3600 * 10;
para.obj_power = 2;
para.is_kernel = 0;
para.kernel_coef = 0;

if ~exist([str_init_w], 'file')
    'random generate init value'
    D = size(Xtraining, 1);
    W = randn(D, m);
    save_mat(W, str_init_w, 'double');
end

W_bre = cBRE(para);
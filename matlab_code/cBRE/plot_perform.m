
for m = [16, 32, 64, 128]
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    
    load(save_file.test_linear_bre);
    load(save_file.test_lsh);
    load(save_file.test_itq);
    load(save_file.test_mlh);
    load(save_file.test_sh);
    
    for k = 1 : numel(all_topks)
        
        figure;
        hold on;
%         plot(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 'r-o', 'LineWidth', 2);
%         plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'b-*', 'LineWidth', 2);
%         plot(eval_linear_bre{k}.avg_retrieved, eval_linear_bre{k}.rec, 'c-d', 'LineWidth', 2);
%         plot(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, 'k-<', 'LineWidth', 2);
%         plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'g->', 'LineWidth', 2);
%         
        plot(eval_mlh_train{k}.avg_retrieved, eval_mlh_train{k}.rec, 'r-o', 'LineWidth', 2);
        plot(eval_itq_train{k}.avg_retrieved, eval_itq_train{k}.rec, 'b-*', 'LineWidth', 2);
        plot(eval_linear_bre_train{k}.avg_retrieved, eval_linear_bre_train{k}.rec, 'c-d', 'LineWidth', 2);
        plot(eval_sh_train{k}.avg_retrieved, eval_sh_train{k}.rec, 'k-<', 'LineWidth', 2);
        plot(eval_lsh_train{k}.avg_retrieved, eval_lsh_train{k}.rec, 'g->', 'LineWidth', 2);
        
        xlim([0, 5000]);
        legend('MLH', 'ITQ', 'BRE', 'SH', 'LSH', 'Location', 'Best');
        xlabel('Number of retrieved points', 'FontSize', 16);
        ylabel('Recall', 'FontSize', 16);
        grid on;
        set(gca, 'FontSize', 16);
        saveas(gca, ['tt_' num2str(m) '_' num2str(all_topks(k)) '_avg_rec.eps'], 'psc2');
    end
end
%%

%%

for m = [16, 32, 64, 128]
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    
    load(save_file.test_linear_bre);
    load(save_file.test_lsh);
    load(save_file.test_itq);
    load(save_file.test_mlh);
    load(save_file.test_sh);
    
    for k = 1 : numel(all_topks)
        
        figure;
        hold on;
        plot(eval_mlh{k}.rec, eval_mlh{k}.pre, 'r-o', 'LineWidth', 2);
        plot(eval_itq{k}.rec, eval_itq{k}.pre, 'b-*', 'LineWidth', 2);
        plot(eval_linear_bre{k}.rec, eval_linear_bre{k}.pre, 'c-d', 'LineWidth', 2);
        plot(eval_sh{k}.rec, eval_sh{k}.pre, 'k-<', 'LineWidth', 2);
        plot(eval_lsh{k}.rec, eval_lsh{k}.pre, 'g->', 'LineWidth', 2);
        
%         plot(eval_mlh_train{k}.rec, eval_mlh_train{k}.pre, 'r-o', 'LineWidth', 2);
%         plot(eval_itq_train{k}.rec, eval_itq_train{k}.pre, 'b-*', 'LineWidth', 2);
%         plot(eval_linear_bre_train{k}.rec, eval_linear_bre_train{k}.pre, 'c-d', 'LineWidth', 2);
%         plot(eval_sh_train{k}.rec, eval_sh_train{k}.pre, 'k-<', 'LineWidth', 2);
%         plot(eval_lsh_train{k}.rec, eval_lsh_train{k}.pre, 'g->', 'LineWidth', 2);
%         
        
        legend('MLH', 'ITQ', 'BRE', 'SH', 'LSH', 'Location', 'Best');
        xlabel('Recall', 'FontSize', 16);
        ylabel('Precision', 'FontSize', 16);
        grid on;
        set(gca, 'FontSize', 16);
        saveas(gca, [num2str(m) '_' num2str(all_topks(k)) '_pr.eps'], 'psc2');
    end
end

%%
Straintrain = load_gnd2('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\SortedDistanceTraintrain.double.bin', 400);

assert(size(Straintrain, 2) == size(Xtraining, 2));
%%
for m = [16, 32, 64, 128]
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    
    load(save_file.train_itq);
    W = W_itq;
    all_eval = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
            Xtraining, Xtraining, ...
            Straintrain', all_topks(k), gnd_file.TestBaseSeed, [], ...
            metric_info, eval_types);
        all_eval{k} = curr_eval;
    end
    eval_itq_train = all_eval;
    save(save_file.test_itq, 'eval_itq_train', '-append');
end


%%
for m = [16, 32, 64, 128]
    [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    
    SHparam = jf_train_sh(Xtraining, m);
    save(save_file.train_sh, 'SHparam');
    
    eval_sh = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        eval_sh{k} = eval_hash5(SHparam.nbits, 'sh', SHparam, ...
            Xtest, Xtraining, StestBase2', ...
            all_topks(k), gnd_file.TestBaseSeed, ...
            [], metric_info, eval_types);
    end
    
    save(save_file.test_sh, 'eval_sh', 'all_topks');
    
    
    load(save_file.train_sh);
    all_eval = cell(numel(all_topks), 1);
    parfor k = 1 : numel(all_topks)
        curr_eval = eval_hash5(SHparam.nbits, 'sh', SHparam, ...
                Xtraining, Xtraining, Straintrain', ...
                all_topks(k), gnd_file.TestBaseSeed, ...
                [], metric_info, eval_types);
        all_eval{k} = curr_eval;
    end
    eval_sh_train = all_eval;
    save(save_file.test_sh, 'eval_sh_train', '-append');
end

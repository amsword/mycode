function file_name = convert_file_name_2_02(file_name)

if file_name(1) == 'C' && file_name(2) == ':'
    file_name(2) = '$';
    file_name = ['\\msra-msm-02\' file_name];
end
para_sr.similarity_epsilon2 = 2;
para_sr.str_distance = 'all_distance.double.bin';
para_sr.str_similariity = 'all_similarity.double.bin';
para_sr.str_laplace = 'all_laplace.double.bin';
para_sr.str_eigenvector = 'all_eigenvector.double.bin';
para_sr.str_eigenvalue = 'all_eigenvalue.double.bin';
% ---------------------------------------------------
para_da.str_distance = [gl_data_parent_folder type '\all_distance.double.bin'];
para_da.str_sorted = [gl_data_parent_folder type '\all_sorted.double.bin'];;
para_da.k1 = 200;
para_da.k2 = 200;
para_da.k3 = 200;
para_da.epsilon2 = 14;
para_da.selected = [gl_data_parent_folder type '\all_selected_' num2str(para_da.k1) '_' ...
    num2str(para_da.k2) '_' num2str(para_da.k3) '.double.bin'];
para_da.str_sym = [gl_data_parent_folder type '\all_sym_' num2str(para_da.k1) '_' ...
    num2str(para_da.k2) '_' num2str(para_da.k3) '.double.bin'];
para_da.str_binary = [gl_data_parent_folder type '\all_binary_' num2str(para_da.k1) '_' ...
    num2str(para_da.k2) '_' num2str(para_da.k3) '_' num2str(m) '.double.bin'];

%% step 1
B = F_spectral_relax([Xtraining, Xtest], ...
    m,...
    para_sr.similarity_epsilon2,...
    para_sr.str_distance, ...
    para_sr.str_similariity, ...
    para_sr.str_laplace,...
    para_sr.str_eigenvector, ...
    para_sr.str_eigenvalue);

%% step 1
B = F_distance_alignment([Xtraining, Xtest], m, para_da);
%%
system(['exeObjectiveValue ' para_da.str_binary ' ' para_da.str_sym ' ' num2str(para_da.epsilon2)]);

system(['exeObjectiveValue ' 'mlh_binary_code_all' ' ' para_da.str_sym ' ' num2str(para_da.epsilon2)]);

%%
all_red = 0;
all_blue = 0;

for k = 21 : 90
    if (mod(k, 100) == 0)
    k
    end
ored = plot_diff_dist(Xtraining, B(:, 1 : Ntraining), Xtest(:, k), B(:, Ntraining + k), para_da.epsilon2);
figure;
oblue = plot_diff_dist(Xtraining, double(mlh_B_train), Xtest(:, k), double(mlh_B_test(:, k)), para_da.epsilon2);

(oblue - ored) / oblue

pause;
close all;

end

%%
perf = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    perf{k} = curr_eval;
end
%%
load(save_file.train_mlh);
mlh_B_train = W_mlh * [Xtraining; ones(1, Ntraining)] > 0;
mlh_B_test = W_mlh * [Xtest; ones(1, Ntest)] > 0;
%%
perf = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash6(m, 'linear', [], ...
        B(:, Ntraining + 1 : end), B(:, 1 : Ntraining), ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    perf{k} = curr_eval;
end

%%
perf = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash6(m, 'linear', [], ...
        B(:, 1 : Ntraining), B(:, 1 : Ntraining), ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    perf{k} = curr_eval;
end
%%
mlhs = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(m, 'linear', W_mlh', ...
        Xtraining, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
    mlhs{k} = curr_eval;
end

%%
curr_eval = eval_hash7(m, 'linear', W_mlh', ...
        Xtest, Xtraining, ...
        StestBase3, [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
%%
    curr_eval_da = eval_hash8(m, 'linear', [], ...
        B(:, Ntraining + 1 : end), B(:, 1 : Ntraining), ...
        StestBase3, [], gnd_file.TestBaseSeed, [], ...
        metric_info, eval_types);
    
% save('epsilon_1', 'perf');

%%
mlhs = load(save_file.test_mlh);
x = load('epsilon_1', 'perf');
for k = 1 : numel(all_topks)
    figure;
    plot(mlhs.eval_mlh{k}.r_code, 'b'); hold on;
    plot(perf{k}.r_code, 'r');
%     plot(x.perf{k}.r_code, 'g');
    axis([0 1000 0 1]);
    grid on;
end
function [y orth] = analysis_energy(Xtraining, W)
normal = W .^ 2;
normal = sum(normal, 1);
normal = sqrt(normal);
W = bsxfun(@rdivide, W, normal);

orth = W' * W;

Y = W' * Xtraining;
Y = Y .^ 2;
y = sum(Y, 2);
y = sort(y);
function B = F_spectral_relax(Xtraining, m, ...
    epsilon2, ...
    str_distance, str_similarity, str_laplace, str_eigenvector, str_eigenvalue)

% epsilon2 = 2;
% str_distance = 'distance.double.bin';
% str_similarity = 'similarity.double.bin';
% str_laplace = 'laplace.double.bin';
% str_eigenvalue = 'eigenvalue.double.bin';
% str_eigenvector = 'eigenvector.double.bin';

% lmat = mexLaplaceMatrix(Xtraining, epsilon2, ...
%     str_distance, str_similarity, str_laplace);
lmat = change_epsilon(Xtraining, epsilon2, ...
    str_distance, str_similarity, str_laplace);

[V D] = eig(lmat);
save_mat(V, str_eigenvector, 'double');
D = diag(D);
save_mat(D, str_eigenvalue, 'double');

B = V(:, 2 : m + 1);
B = B > 0;
Xbase = fvecs_read('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\SIFTOrigin\data\sift_base.fvecs');
Xbase = double(Xbase);
mean_value = mean(Xbase, 2);
mean_removed = bsxfun(@minus, Xbase, mean_value);

%%
test_idx = randperm(1000000, 100000);
Xtest = mean_removed(test_idx);
train_idx = setdiff([1 : 1000000], test_idx);
Xtraining = mean_removed(train_idx);

%%
str_distance = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\\SIFTOrigin\data\c_data\DistanceTrainTrain.double.bin';
str_sorted = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\\SIFTOrigin\data\c_data\SortedDistance.double.bin';
k1 = 50;
k2 = 50;
k3 = 50;
str_selected = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\\SIFTOrigin\data\c_data\';
str_selected = [str_selected 'selected.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
str_sym = 'C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\\SIFTOrigin\data\c_data\';
str_sym = [str_sym 'sym.' num2str(k1) '.' num2str(k2) '.' num2str(k3) '.double.bin'];
%% calculate the distance
calc_large_distance(Xtraining, str_distance);

% sort the distance
calc_large_sorted_distance(str_distance, str_sorted);

% select part of the distance, not all of them is used
select_dist(str_sorted, str_selected, k1, k2, k3);

% symmetric it
tic
system(['exeSymDistance ' str_selected ' ' str_sym]);
toc
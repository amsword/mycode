function epsilon = avg_epsilon(file_name, topk)

fp = fopen(file_name, 'rb');

num_test = fread(fp, 1, 'int32');
num_base = fread(fp, 1, 'int32');

vec = zeros(1, num_test);
for i = 1 : num_test
    num = fread(fp, 1, 'int32');
    fseek(fp, num * 4 + (topk - 1) * 8, 0);
    vec(i) = fread(fp, 1, 'double');
    fseek(fp, (num - topk) * 8, 0);
end

epsilon = mean(sqrt(vec));
epsilon = epsilon^2;

fclose(fp);
function y = load_tiny80M(folder_name, required)

file_name = 'tinygist80million.bin';
file_name = [folder_name '/' file_name];

if isfield(required, 'idx_start_base')
    idx_start_base = required.idx_start_base;
    idx_end_base = required.idx_end_base;
    
    y.base = double(read_tiny_gist_binary(...
        file_name, ...
        idx_start_base : idx_end_base));
end

if isfield(required, 'idx_start_training')
    idx_start_training = required.idx_start_training;
    idx_end_training = required.idx_end_training;
    
    y.training = double(read_tiny_gist_binary(...
        file_name, ...
        idx_start_training : idx_end_training));
end

if isfield(required, 'idx_start_test')
    idx_start_test = required.idx_start_test;
    idx_end_test = required.idx_end_test;
    
    y.test = double(read_tiny_gist_binary(...
        file_name, ...
        idx_start_test : idx_end_test));
end
function y = load_nus_wide(folder_name, required)
%%  run convert_dat_bin before calling this function
sub_folder_name_feature = 'NUS_WID_Low_Level_Features/Low_Level_Features';
file_name_wt_train = 'Train_Normalized_WT.dat.bin';
file_name_wt_test = 'Test_Normalized_WT.dat.bin';
file_name_edh_train = 'Train_Normalized_EDH.dat.bin';
file_name_edh_test = 'Test_Normalized_EDH.dat.bin';
file_name_cm55_train = 'Train_Normalized_CM55.dat.bin';
file_name_cm55_test = 'Test_Normalized_CM55.dat.bin';
is_wt_train = 1;
is_wt_test = 1;
is_edh_train = 1;
is_edh_test = 1;
is_cm55_train = 1;
is_cm55_test = 1;
idx_start_test = [];
idx_end_test = [];
    
if isfield(required, 'idx_start_test')
    idx_start_test = required.idx_start_test;
    idx_end_test = required.idx_end_test;
end
%%
if is_wt_train
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_wt_train];
    y.wt_train = read_mat(file_name, 'double');
end
%%
if is_wt_test
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_wt_test];
    if isempty(idx_start_test) || isempty(idx_end_test)
        y.wt_test = read_mat(file_name, 'double');
    else
        y.wt_test = read_mat2(file_name, 'double', ...
            idx_start_test, idx_end_test);
    end
end

if is_edh_train
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_edh_train];
    y.edh_train = read_mat(file_name, 'double');
end

if is_edh_test
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_edh_test];
    if isempty(idx_start_test) || ...
            isempty(idx_end_test)
        y.edh_test = read_mat(file_name, 'double');
    else
        y.edh_test = read_mat2(file_name, 'double', ...
            idx_start_test, idx_end_test);
    end
end

if is_cm55_train
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_cm55_train];
    y.cm55_train = read_mat(file_name, 'double');
end

if is_cm55_test
    file_name = ...
        [folder_name sub_folder_name_feature '/' file_name_cm55_test];
    if isempty(idx_start_test) || ...
            isempty(idx_end_test)
        y.cm55_test = read_mat(file_name);
    else
        y.cm55_test = read_mat2(file_name, 'double', ...
            idx_start_test, idx_end_test);
    end
end
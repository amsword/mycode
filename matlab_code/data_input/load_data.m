function y = load_data(data_folder, data_type, required)

data_type_nus_wide_lite = 'NUS-WIDE-LITE';
data_type_nus_wide = 'NUS-WIDE';
data_type_sift1m = 'SIFT1M';
data_type_gist1m = 'GIST1M';
data_type_debug_data = 'DEBUG_DATA';
data_type_tiny80m = 'Tiny80M';
data_type_imagenet = 'ImageNet';

if nargin == 2
    required = [];
end
if strcmp(data_type, data_type_nus_wide_lite)
    y = load_nus_wide_lite(...
        [data_folder data_type_nus_wide_lite '/'], ...
        required);
elseif strcmp(data_type, data_type_nus_wide)
    y = load_nus_wide(...
        [data_folder data_type_nus_wide '/'], ...
        required);
elseif strcmp(data_type, data_type_debug_data)
    t = 128 * 1024;
    y = 1 : t;
    y = reshape(128, 1024);
elseif strcmp(data_type, data_type_sift1m) || ...
        strcmp(data_type, data_type_gist1m)
    y = load_sift1m_gist1m(...
        [data_folder data_type '/'], ...
        required);
elseif strcmp(data_type, data_type_imagenet)
    y = load_imagenet([data_folder data_type '/'], required);
elseif strcmp(data_type, data_type_tiny80m) 
    y = load_tiny80M([data_folder data_type], required);
end
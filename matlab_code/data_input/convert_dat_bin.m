function convert_dat_bin(folder_name)

%%

files = dir([folder_name '/*.dat']);

num_files = numel(files);
for i = 1 : num_files
    file_name = [folder_name '/' files(i).name];
    
    files(i).name
    x = load(file_name);
    save_mat(x', [file_name '.bin'], 'double');
end


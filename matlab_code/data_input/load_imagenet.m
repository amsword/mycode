function y = load_imagenet(folder_name, required)

file_name_test = 'bog_query.double';
file_name_base = 'bog_database.double';

is_training = 0;
is_test = 1;
is_base = 1;
idx_start_test = [];
idx_end_test = [];
is_data_normalize = 0;

if ~isempty(required)
    if isfield(required, 'is_data_normalize')
        is_data_normalize = required.is_data_normalize;
    end
    if isfield(required, 'is_training')
        is_training = required.is_training;
    end
    if isfield(required, 'is_test')
        is_test = required.is_test;
    end
    if isfield(required, 'is_base')
        is_base = required.is_base;
    end
    if isfield(required, 'idx_start_test')
        idx_start_test = required.idx_start_test;
    end
    if isfield(required, 'idx_end_test')
        idx_end_test = required.idx_end_test;
    end
end

if is_data_normalize
    pre_fix = 'normalize_';
    file_name_test = [pre_fix file_name_test];
    file_name_base = [pre_fix file_name_base];
end

if is_test
    file_name = [folder_name '/' file_name_test];
    if isempty(idx_start_test) || isempty(idx_end_test)
        y.test = read_mat(file_name, 'double');
    else
        y.test = read_mat2(file_name, 'double', idx_start_test, idx_end_test);
    end
end

if is_base
    file_name = [folder_name '/' file_name_base];
    y.base = read_mat(file_name, 'double');
end

if is_training
    file_name = [folder_name '/' file_name_training];
    y.training = read_mat(file_name, 'double');
end


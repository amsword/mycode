function gnd_idx = read_epsilon_gnd(file_name, type, epsilon)
fp = fopen(file_name, 'rb');

rows = fread(fp, 1, 'int32');
cols = fread(fp, 1, 'int32');

gnd_idx = cell(rows, 1);

aux = 1 : cols;
for i = 1 : rows
    dist = fread(fp, cols, type);
    gnd_idx{i} = double(aux(dist < epsilon));
end

fclose(fp);
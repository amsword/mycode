function mat = read_sp_mat(file_name, type)
fp = fopen(file_name, 'rb');

rows = fread(fp, 1, 'int32');
tmp = fread(fp, 1, 'int32');

mat = cell(rows, 1);

for i = 1 : rows
    num = fread(fp, 1, 'int32');
    mat{i}.idx = fread(fp, num, 'int32');
    mat{i}.dist = fread(fp, num, type);
end

fclose(fp);
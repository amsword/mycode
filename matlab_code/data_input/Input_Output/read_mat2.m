function y = read_mat2(...
    file_name, type, idx_start, idx_end)


fp = fopen(file_name, 'r');
m = fread(fp, 1, 'int32');
n = fread(fp, 1, 'int32');

if strcmp(type(1 : 5), 'int32')
    elem = 4;
elseif strcmp(type(1 : 5), 'float')
    elem = 4;
elseif strcmp(type(1 : 5), 'uint8')
    elem = 1;
elseif strcmp(type(1 : 6), 'double')
    elem = 8;
else
    error('unsupported');
end

if idx_start > 1
    fseek(fp, (idx_start - 1) * elem * n, 0);
end

if idx_end > m
    idx_end = m;
end

y = fread(fp, [n,(idx_end - idx_start + 1)], type);

fclose(fp);
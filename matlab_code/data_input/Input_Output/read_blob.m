function y = read_blob(file_name, type)
fp = fopen(file_name, 'r');
assert(fp ~= 0);

num = fread(fp, 1, 'int32')
channels = fread(fp, 1, 'int32')
height = fread(fp, 1, 'int32');
width = fread(fp, 1, 'int32');

y = fread(fp, [width * height * channels * num], type);
y = reshape(y, [width, height, channels, num]);

fclose(fp);

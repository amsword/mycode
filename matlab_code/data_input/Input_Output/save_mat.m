function save_mat(data, file_name, type)
fp = fopen(file_name, 'w');

[dim, m] = size(data);
fwrite(fp, m, 'int32');
fwrite(fp, dim, 'int32');

fwrite(fp, data, type);

fclose(fp);
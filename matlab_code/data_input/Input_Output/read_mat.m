function y = read_mat(file_name, type)
fp = fopen(file_name, 'rb');
assert(fp ~= -1);
m = fread(fp, 1, 'int32');
n = fread(fp, 1, 'int32');

if m == 0 || n == 0
    y = [];
else
    y = fread(fp, [n,m], type);
end

fclose(fp);

function [W_mlh best_params] = jf_train_mlh4(Xtraining, Straining, nb, rho, shrink_w)

data2.Xtraining = Xtraining;
data2.MODE = 'euc-22K-labelme';
data2.Ntraining = size(data2.Xtraining, 2);
data2.Straining = Straining; % only one largest grade is needed.


% verbose flag for validation is set to 0 (off)
% to see the details during validation, set val_verbose to 15 (debug info every 15th iteration)
val_verbose = 25;

% ----- validation -----

% A heuristic for initial selection of rho

fprintf('automatic estimation of rho suggested rho = %d.\n', rho);
clear data3;

% best_params is a data structure that stores the most reasonable parameter setting
% initial setting of parameters
best_params.size_batches = 100;
best_params.eta = .1;
best_params.shrink_w = shrink_w;
best_params.lambda = .5;
best_params.rho = rho;


% ---- training on the train+val set -----
% below 500 learning iterations are used, using more might provide slightly better results
% in the paper we used 2000
train_iter = 500;
train_zerobias = 1;
Wmlh = MLH(data2, {'hinge', best_params.rho, best_params.lambda}, nb, ...
    [best_params.eta], .9, [best_params.size_batches], 'trainval', train_iter, train_zerobias, ...
    5, 0, 50, [best_params.shrink_w], 1);
 
W_mlh = Wmlh.W;
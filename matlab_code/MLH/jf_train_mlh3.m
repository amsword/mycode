function [W_mlh best_params] = jf_train_mlh2(Xtraining, Straining, nb)

data2.Xtraining = Xtraining;
data2.MODE = 'euc-22K-labelme';
data2.Ntraining = size(data2.Xtraining, 2);
data2.Straining = Straining; % only one largest grade is needed.


% verbose flag for validation is set to 0 (off)
% to see the details during validation, set val_verbose to 15 (debug info every 15th iteration)
val_verbose = 25;

% ----- validation -----

% A heuristic for initial selection of rho
 data3 = create_training(data2, 'train', 1);
 [p0 r0] = eval_LSH(nb, data3);
 rho = sum(r0 < .3);			% rho with 30% recall (nothing deep; just a heuristic)
fprintf('automatic estimation of rho suggested rho = %d.\n', rho);
clear data3;

% best_params is a data structure that stores the most reasonable parameter setting
% initial setting of parameters
best_params.size_batches = 100;
best_params.eta = .1;
best_params.shrink_w = 1e-4;
best_params.lambda = .5;
best_params.rho = rho;
best_params.shrink_w = 0;


val_iter = 75;
val_zerobias = 1;

fprintf('validation for rho in hinge loss\n');
step = round(nb / 32);
step(step < 1) = 1;

rho_set = rho + [-2 -1 0 +1 +2] * step;
rho_set(rho_set < 1) = [];
Wtmp_rho = MLH(data2, {'hinge', rho_set, best_params.lambda}, nb, [best_params.eta], ...
    .9, 100, 'train', val_iter, val_zerobias, 0, 5, val_verbose, best_params.shrink_w, 0);
best_ap = -1;
for j = 1:numel(Wtmp_rho)
    if (Wtmp_rho(j).ap > best_ap)
        best_ap = Wtmp_rho(j).ap;
        best_params.rho = Wtmp_rho(j).params.loss.rho;
    end
    if (val_verbose)
        fprintf('%.3f %d\n', Wtmp_rho(j).ap, Wtmp_rho(j).params.loss.rho);
    end
end
fprintf('Best rho (%d bits) = %d\n', nb, best_params.rho);



% ---- training on the train+val set -----
% below 500 learning iterations are used, using more might provide slightly better results
% in the paper we used 2000
train_iter = 500;
train_zerobias = 1;
Wmlh = MLH(data2, {'hinge', best_params.rho, best_params.lambda}, nb, ...
    [best_params.eta], .9, [best_params.size_batches], 'trainval', train_iter, train_zerobias, ...
    5, 0, 50, [best_params.shrink_w], 1);
 
W_mlh = Wmlh.W;
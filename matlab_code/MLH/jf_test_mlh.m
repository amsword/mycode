function eval = jf_test_mlh(W, Xtest, Xbase, StestBase, topK, TestBaseSeed, irrelevance)
eval = jf_eval_hash3(size(W, 1), 'linear', W', Xtest, Xbase, StestBase, topK, TestBaseSeed, irrelevance);
function W1 = min_clustering_error(Xtraining, cluster_center, cluster_idx, code_length)

diff = Xtraining' - cluster_center(cluster_idx, :);

mat = diff' * diff;

[V D] = eig(mat);

W1 = V(:, 1 : code_length);

end
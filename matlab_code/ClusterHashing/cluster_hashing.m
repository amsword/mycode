% function [W, sw, sb] = cluster_hashing(Xtraining, para)

para.str_cluster = '\\msra-msm-02\c$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\cluster_';

code_length = para.code_length;
num_cluster = para.num_cluster;
str_cluster_idx = [para.str_cluster 'idx_' num2str(num_cluster)];
str_cluster_center = [para.str_cluster 'center_' num2str(num_cluster)];

% [cluster_center, cluster_idx] = vl_kmeans(Xtraining, num_cluster, ...
%     'Initialization', 'PLUSPLUS');
% cluster_center = cluster_center';

% [cluster_idx, cluster_center] = kmeans(Xtraining', num_cluster, 'emptyaction', 'singleton');
% 
% save_mat(cluster_idx - 1, str_cluster_idx, 'int32');
% save_mat(cluster_center', str_cluster_center, 'double');

cluster_idx = read_mat(str_cluster_idx, 'int32');
cluster_idx = cluster_idx + 1;
cluster_center = read_mat(str_cluster_center, 'double');
cluster_center = cluster_center';
% return;

% W1_min_cluster = min_clustering_error(Xtraining, cluster_center, cluster_idx, code_length);
[W1_lda, sw, sb] = lda(Xtraining, cluster_idx, code_length);

W1 = W1_lda;

W2 = mbq(Xtraining' * W1, code_length);
W12 = W1 * W2;
% W12 = W1;
W = W12;
% W = [W12; zeros(1, code_length)];
% end
eval_cth = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    eval_cth{k} = eval_hash5(m, 'linear', ...
        W, Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end

%%
all_num_cluster = [100 2000 500 1000];
parfor i = 1 : numel(all_num_cluster)
    curr_para = cell(1, 1);
    curr_para.num_cluster = all_num_cluster(i);
    cluster_hashing(Xtraining, curr_para);
end

%%
para.code_length = m;
para.num_cluster = 24;

% W = cluster_hashing2(Xtraining, para);
cluster_hashing;
% [W, sw, sb] = cluster_hashing(Xtraining, para);

% save('labelme_cluster_hashing_16_1000', 'W');

%%
toy = zeros(2, 500);
v = 0.1;
toy(:, 1 : 100) = bsxfun(@plus, randn(2, 100) * v, [1; 0]);
toy(:, 101 : 200) = bsxfun(@plus, randn(2, 100) * v, [-1; 0]);
toy(:, 201 : 300) = bsxfun(@plus, randn(2, 100) * v, [0; 2]);
toy(:, 301 : 500) = bsxfun(@plus, randn(2, 200) * v, [0; -2]);

figure;
hold on;
plot(toy(1, 1 : 100), toy(2, 1 : 100), 'r.');
plot(toy(1, 101 : 200), toy(2, 101 : 200), 'b.');
plot(toy(1, 201 : 300), toy(2, 201 : 300), 'g.');
plot(toy(1, 301 : 500), toy(2, 301 : 500), 'k.');


W = trainITQ(toy, 1);

cluster_idx = zeros(1, 500);
cluster_idx(101 : 200)  = 1;
cluster_idx(201 : 300)  = 2;
cluster_idx(301 : 500)  = 3;
cluster_idx = cluster_idx + 1;
[W1_lda, sw, sb] = lda(toy, cluster_idx, 1);
W_lda = [W1_lda; zeros(1, 1)];

W_itq = W;
plot_normal(W_itq, 'b');

plot_normal(W_lda, 'r');

axis square;

%%
energy_total = sum(Xtraining(:) .^ 2);
proj_itq = W_itq(1 : end - 1, :)' * Xtraining;

energy_itq = sum(proj_itq(:) .^ 2);

energy_itq / energy_total

proj_cluster = W(1 : 512, :)' * Xtraining;
energy_cluster = sum(proj_cluster(:) .^ 2);
energy_cluster / energy_total

%%
eval_cth = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    eval_cth{k} = eval_hash5(m, 'linear', ...
        W, Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end
%%
for k = 1 : 6
    figure;
    hold on;
    plot(eval_cth{k}.r_code, 'r');
    plot(eval_itq{k}.r_code, 'b');
     xlim([0, 2000]);
end

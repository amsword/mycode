% function W = cluster_hashing2(Xtraining, para)
% cluster in hamming space

para.code_length = m;
para.num_cluster = 100;

code_length = para.code_length;
num_cluster = para.num_cluster;

dim = size(Xtraining, 1);

W = randn(dim, code_length);

for iter = 1 : 10
    B = W' * Xtraining > 0;
    B = double(B);
    
    [~, cluster_idx] = vl_kmeans(B, num_cluster, ...
        'Initialization', 'PLUSPLUS', 'distance', 'l1');
    
    num_point = size(Xtraining, 2);
    
    cluster_center = zeros(num_cluster, dim);
    cluster_size = zeros(num_cluster, 1);
    for i = 1 : num_point
        cluster_center(cluster_idx(i), :) = ...
            cluster_center(cluster_idx(i), :) + Xtraining(:, i)';
        cluster_size(cluster_idx(i)) = cluster_size(cluster_idx(i)) + 1;
    end
    cluster_center = bsxfun(@rdivide, cluster_center, cluster_size);
    
%     W1_min_cluster = min_clustering_error(Xtraining, cluster_center, cluster_idx, code_length);
    
    [W1, sw, sb] = lda(Xtraining, cluster_idx, code_length);
    
%     trace(W1' * sw * W1)
%     trace(W1' * sb * W1)
%     trace((W1' * sw * W1) \ (W1' * sb * W1) )
    
%     W1 = W1_min_cluster;
    
    W2 = mbq(Xtraining' * W1, code_length);
    W = W1 * W2;
end

% W = [W; zeros(1, code_length)];
% end
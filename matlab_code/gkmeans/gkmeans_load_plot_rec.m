

file_name = ['../../sckmeans/non_full_training/ock_' ...
    num2str(num_sub_dic_each_partition / 2) ...
    '_256_2_102.mat'];

if strcmp(data_type, 'MNIST')
    ock = load(file_name, 'rec', 'loss_base');
else
    ock = load(file_name, 'obj', 'rec', 'loss_base', 'rec_o');
end

file_name = ['../../sckmeans/non_full_training/ck_' num2str(num_sub_dic_each_partition) '_256.mat'];
% end
if strcmp(data_type, 'MNIST')
    ck = load(file_name, 'ock_model', 'rec', 'loss_base');
    ck.obj = ck.ock_model.obj;
else
    ck = load(file_name, 'obj', 'rec', 'loss_base', 'rec_o');
end
%
file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped2_initialize_typerandom'];
ours_r = load(file_name, 'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped2_initialize_typekmeans'];
ours_k = load(file_name, 'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped2_initialize_typeockmeans'];
% ours_o = load(file_name, 'ock_model', 'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped2_initialize_typeh_ockmeans_30_100'];
ours_h = load(file_name, 'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped1_initialize_typeh_ockmeans_30_100'];
ours_h1 = load(file_name, 'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped1_initialize_typerandom'];
ours_r1 = load(file_name,  'rec', 'loss_base');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped1_initialize_typekmeans'];
ours_k1 = load(file_name,  'rec', 'loss_base');

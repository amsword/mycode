#!/bin/bash
 
cd /home/jianfeng/mycode/matlab_code/gkmeans
pwd


export LD_LIBRARY_PATH=/home/jianfeng/mycode/c_code/bin

matlab -nodisplay -nodesktop -nosplash -r "main_gkmeans(${1}, ${2})"

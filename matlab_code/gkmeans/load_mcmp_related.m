x = load('energy.mat');
base_energy = x.base_energy;

file_name = ['../../sckmeans/non_full_training/ock_' ...
    '1' ...
    '_256_' num2str(num_sub_dic_each_partition) '_102.mat'];
if num_sub_dic_each_partition == 8
    clear ock;
    ock.time_encoding = 2589.66 * 10^6 / 240;
else
    ock = load(file_name, 'loss_base', 'time_encoding');
end

% file_name = ['../../sckmeans/non_full_training/ck_' ...
%     num2str(num_sub_dic_each_partition) '_256.mat'];
% ck = load(file_name, 'loss_base', 'time_encoding');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped2_initialize_typeh_ockmeans_30_100'];
ours_h = load(file_name, 'loss_base', 'time_encoding');

file_name = ['gkmeans_1_' num2str(num_sub_dic_each_partition) '_256_num_grouped1_initialize_typeh_ockmeans_30_100'];
ours_h1 = load(file_name,  'loss_base', 'time_encoding');
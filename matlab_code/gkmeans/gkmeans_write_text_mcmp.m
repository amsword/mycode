file_name = [save_pair_folder 'mcmcp.tex'];

fp = fopen(file_name, 'w');

line = '\begin{table}'; fprintf(fp, '%s\n', line); 
line = '\centering'; fprintf(fp, '%s\n', line);
line = '\caption{Comparison of encoding time and the relative distortion (R.d) on the database of SIFT1M.}';
fprintf(fp, '%s\n', line);
line = '\label{fig:comp_mcmp}';
fprintf(fp, '%s\n', line);
line = '\begin{tabular}{cccccccc}'; fprintf(fp, '%s\n', line);
line = '\toprule'; fprintf(fp, '%s\n', line);
line = '\multicolumn{2}{c}{} & \multicolumn{2}{c}{2} & \multicolumn{2}{c}{4} & \multicolumn{2}{c}{8} \\'; fprintf(fp, '%s\n', line);
line = '\cmidrule(lr){3-4}  \cmidrule(lr){5-6} \cmidrule(lr){7-8}'; fprintf(fp, '%s\n', line);
line = '\multicolumn{2}{c}{} & Time (s) & R.d. & Time  (s) & R.d. & Time (s) & R.d \\'; fprintf(fp, '%s\n', line);
line = '\midrule'; fprintf(fp, '%s\n', line);
line = '\multirow{2}{*}{gk-means} & 1 '; fprintf(fp, '%s\n', line); 
for i = 1 : 3
    line = sprintf(' & %.1f ', time_gkmeans_1(i)); fprintf(fp, '%s\n', line); 
    line = sprintf(' & %.2f', all_gkmeans_1(i)); fprintf(fp, '%s\n', line); 
end
line = '\\'; fprintf(fp, '%s\n', line); 
line = ' & 2 '; fprintf(fp, '%s\n', line); 
for i = 1 : 3
    line = sprintf(' & %.1f ', time_gkmeans_2(i)); fprintf(fp, '%s\n', line); 
    line = sprintf(' & %.2f', all_gkmeans_2(i)); fprintf(fp, '%s\n', line); 
end
line = '\\'; fprintf(fp, '%s\n', line); 
line = '\midrule'; fprintf(fp, '%s\n', line); 
line = '\multicolumn{2}{c}{MCMP} '; fprintf(fp, '%s\n', line); 
for i = 1 : 3
    line = sprintf(' & %.1f ', time_ock(i)); fprintf(fp, '%s\n', line); 
    if i == 3
        line = ' & --'; fprintf(fp, '%s\n', line); 
    else
        line = sprintf(' & %.2f', all_ock(i)); fprintf(fp, '%s\n', line); 
    end
end

line = '\\'; fprintf(fp, '%s\n', line); 
line = '\bottomrule'; fprintf(fp, '%s\n', line); 
line = '\end{tabular}'; fprintf(fp, '%s\n', line); 
line = '\end{table}'; fprintf(fp, '%s\n', line); 

fclose(fp);


all_data_type = {'SIFT1M', 'GIST1M', 'MNIST', 'cifar10_nin_cccp6'};
idx_data_type = 3;
all_num_sub_dic_each_partition = [4 8 16];
% all_num_sub_dic_each_partition = [8];

name_gkmeans_2_h = 'gk-means(2, h)';
name_gkmeans_2_k = 'gk-means(2, k)';
name_gkmeans_2_r = 'gk-means(2, r)';
name_gkmeans_1_h = 'gk-means(1, h)';
name_gkmeans_1_k = 'gk-means(1, k)';
name_gkmeans_1_r = 'gk-means(1, r)';
name_ock = 'ock-means [6]';
name_ck = 'ck-means [5]';

name_gkmeans_2_h_all_rec = 'gk-means(2, h)';
name_gkmeans_2_k_all_rec = 'gk-means';
name_gkmeans_2_r_all_rec = 'gk-means';
name_gkmeans_1_h_all_rec = 'gk-means(1, h)';
name_gkmeans_1_k_all_rec = 'gk-means';
name_gkmeans_1_r_all_rec = 'gk-means';
name_ock_all_rec = 'ock-means';
name_ck_all_rec = 'ck-means';


fontsize = 14;
linewidth = 2;

style_gkmeans_2_h = 'rp-';
% marker_gkmeans_2_h = 'd';
style_gkmeans_2_k = 'rs-';
style_gkmeans_2_r = 'r*-';
style_gkmeans_1_h = 'bp-';
style_gkmeans_1_k = 'bs-';
style_gkmeans_1_r = 'b*-';
style_ock = 'm';
style_ck = 'k';

save_folder = 'D:\OneDrive\Documents\Research\MyPaper\GroupKMeans\cvpr15\figs\';
save_pair_folder = 'D:\OneDrive\Documents\Research\MyPaper\GroupKMeans\';

idx_num_sub_dic_each_partition = 1;
% for idx_data_type = 1 : numel(all_data_type)

 working_folder = 'E:\research\working\';

h_ockmeans_initi_num_iter = 10;
%
% gkmeans_plot_time_distortion;
gkmeans_plot_acc_codelength;

%%
for idx_data_type = 1 : numel(all_data_type)
    
    data_type = all_data_type{idx_data_type};
    prepare_folder_name;
    cd([working_folder data_type '/gkmeans/non_full_training']);
    
    for idx_num_sub_dic_each_partition = 1 : numel(all_num_sub_dic_each_partition)
        num_sub_dic_each_partition = all_num_sub_dic_each_partition(idx_num_sub_dic_each_partition);
        gkmeans_load_plot;
        gkmeans_train_obj;
%                 gkmeans_plot_rec;
        % gkmeans_base_obj;
    end
end

%%
% for idx_data_type = 1 : numel(all_data_type)
    idx_data_type = 2;
    data_type = all_data_type{idx_data_type};
    prepare_folder_name;
    cd([working_folder data_type '/gkmeans/non_full_training']);
    gkmeans_plot_rec_all_code_length
% end

%%
% for idx_data_type = 1 : numel(all_data_type)
    idx_data_type = 2;
    data_type = all_data_type{idx_data_type};
    prepare_folder_name;
    cd([working_folder data_type '/gkmeans/non_full_training']);
    gkmeans_hist_rec
% end

%%
gkmeans_plot_base_obj
%%
data_type = all_data_type{1};
prepare_folder_name;
cd([working_folder data_type '/gkmeans/non_full_training']);
gkmeans_mcmp

%%

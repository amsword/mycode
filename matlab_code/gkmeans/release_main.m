num_sub_dic_each_partition = 4;
num_partitions = 1;
num_grouped = 2;
data_type = 'MNIST';
encoding_method = 'gkmeans';
num_can = 12;
num_can_encode = 12;
initialize_type = 'h_ockmeans'
h_ockmeans_initi_num_iter = 10; % in the paper, we set it 30;
h_ockmeans_final_num_iter = 100 - log(num_sub_dic_each_partition) / log(2) * h_ockmeans_initi_num_iter; % in the paper, we set it 100
assert(h_ockmeans_final_num_iter == floor(h_ockmeans_final_num_iter));
assert(h_ockmeans_final_num_iter > 0);
topks = [1, 10, 100];
sub_dic_size_each_partition = 256;


data_dim = 128;
num_training = 10000;
Xtraining = rand(data_dim, num_training);

clear opt_input_ock;
opt_input_ock.num_sub_dic_each_partition = num_sub_dic_each_partition;
opt_input_ock.num_partitions = num_partitions;
opt_input_ock.sub_dic_size_each_partition = sub_dic_size_each_partition;
opt_input_ock.num_grouped = num_grouped;
opt_input_ock.encoding_type = encoding_method;
opt_input_ock.num_can_train = num_can;
opt_input_ock.num_can = num_can;

% hirarchical initialization 
num_sub_problem = log(num_sub_dic_each_partition) / log(2);
num_sub_problem = floor(num_sub_problem);
assert(2 .^ (num_sub_problem) == num_sub_dic_each_partition);
clear opt_input_ock2;
opt_input_ock2.num_sub_dic_each_partition = 1;
opt_input_ock2.num_partitions = num_sub_dic_each_partition;
opt_input_ock2.sub_dic_size_each_partition = ...
sub_dic_size_each_partition;
opt_input_ock2.max_iter = h_ockmeans_initi_num_iter;
opt_input_ock2.num_grouped = num_grouped;
opt_input_ock2.encoding_type = encoding_method;
opt_input_ock2.num_can_train = num_can;
opt_input_ock2.num_can = num_can;

[sub_dim_start_idx] = dim_split(size(Xtraining, 1), opt_input_ock2.num_partitions);
init_all_D = RandomInitDictionary(Xtraining, sub_dim_start_idx, ...
    opt_input_ock2.sub_dic_size_each_partition * ...
    opt_input_ock2.num_sub_dic_each_partition);
    opt_input_ock2.init_all_D = init_all_D;

all_ock_model = cell(num_sub_problem, 1);
t1 = tic;
for idx_sub_problem = 1 : num_sub_problem
        t2 = tic;
        ock_model = ock_training(Xtraining, opt_input_ock2);
        time_curr = toc(t2);
        all_ock_model{idx_sub_problem} = ock_model;

        opt_input_ock2.num_sub_dic_each_partition = ...
        opt_input_ock2.num_sub_dic_each_partition * 2;
        opt_input_ock2.num_partitions = opt_input_ock2.num_partitions / 2;
        [opt_input_ock2.init_R, opt_input_ock2.init_all_D] = construct_init_dictionary(...
        ock_model.R, ...
        ock_model.all_D);
end
time_h_init = toc(t1);

opt_input_ock.init_all_D = opt_input_ock2.init_all_D;
opt_input_ock.max_iter = h_ockmeans_final_num_iter;
x = tic;
ock_model = ock_training(Xtraining, opt_input_ock);
time_training = toc(x);
save(file_output, ...
'ock_model', ...
'opt_input_ock', ...
'time_training', ...
'machine_name', ...
'-v7.3');

training_energy = sum(Xtraining(:) .^ 2);
save('energy.mat', 'training_energy');



%% encode
    %%
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    Xbase = read_mat(file_name, 'double');
    
    x = load('energy.mat');
    base_energy = sum(Xbase(:) .^ 2);
    save('energy.mat', 'base_energy', '-append');

    ock_model.para_encode.num_can_encode = num_can_encode;
    ock_model.para_encode.num_can = num_can_encode;
    x = tic;
    mat_compact_B = ock_encoding(ock_model, Xbase);
    time_encoding = toc(x);
    time_encoding
    machine_name_encode = getComputerName();
    save(file_output, 'mat_compact_B', 'time_encoding', 'machine_name_encode', '-append');

% loss on the database
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    if strcmp(data_type, 'MNIST')
        file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    end
    Xbase = read_mat(file_name, 'double');
    if ock_model.is_optimize_R
        Xbase = ock_model.R' * Xbase;
    end

    loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)

    save(file_output, 'loss_base', '-append');
    NBase = size(mat_compact_B, 2);

    is_cal = true;
    if exist('energy.mat', 'file')
        x = load('energy.mat', 'base_energy');
        if isfield(x, 'base_energy')
            is_cal = false;
        end
    end
    if is_cal
        base_energy = sum(Xbase(:) .^ 2);
        save('energy.mat', 'base_energy', '-append');
    end

    clear Xbase;
% 
% file_name = [data_folder data_type '/OriginXBase.double.bin'];
% if strcmp(data_type, 'MNIST')
%     file_name = [data_folder data_type '/OriginXtraining.double.bin'];
% end
% Xbase = read_mat(file_name, 'double');
% 
% if ock_model.is_optimize_R
%     Xbase = ock_model.R' * Xbase;
% end
% 
% loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)
% 
% save(file_output, 'loss_base', '-append');

%% encode
is_run = true;
x = load(file_output, 'mat_compact_B');
if isfield(x, 'mat_compact_B') && isa(x.mat_compact_B, 'uint8')
    is_run = false;
    mat_compact_B = x.mat_compact_B;
    NBase = size(mat_compact_B, 2);
    warning('exist mat_compact_B');
end

% is_run = true;
if is_run
    %%
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    if strcmp(data_type, 'MNIST')
        file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    end
    Xbase = read_mat(file_name, 'double');
    x = load('energy.mat');
    if ~isfield(x, 'base_energy');
        base_energy = sum(Xbase(:) .^ 2);
        save('energy.mat', 'base_energy', '-append');
    end

    ock_model.para_encode.num_can_encode = num_can_encode;
    ock_model.para_encode.num_can = num_can_encode;
    x = tic;
    mat_compact_B = ock_encoding(ock_model, Xbase);
    time_encoding = toc(x);
    time_encoding
    machine_name_encode = getComputerName();
    save(file_output, 'mat_compact_B', 'time_encoding', 'machine_name_encode', '-append');
end

% loss on the database
is_run = true;
x = load(file_output, 'loss_base');
if isfield(x, 'loss_base')
    is_run = false;
end

if is_run
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    if strcmp(data_type, 'MNIST')
        file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    end
    Xbase = read_mat(file_name, 'double');
    if ock_model.is_optimize_R
        Xbase = ock_model.R' * Xbase;
    end

    loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)

    save(file_output, 'loss_base', '-append');
    NBase = size(mat_compact_B, 2);

    is_cal = true;
    if exist('energy.mat', 'file')
        x = load('energy.mat', 'base_energy');
        if isfield(x, 'base_energy')
            is_cal = false;
        end
    end
    if is_cal
        base_energy = sum(Xbase(:) .^ 2);
        save('energy.mat', 'base_energy', '-append');
    end

    clear Xbase;
end
% 
% file_name = [data_folder data_type '/OriginXBase.double.bin'];
% if strcmp(data_type, 'MNIST')
%     file_name = [data_folder data_type '/OriginXtraining.double.bin'];
% end
% Xbase = read_mat(file_name, 'double');
% 
% if ock_model.is_optimize_R
%     Xbase = ock_model.R' * Xbase;
% end
% 
% loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)
% 
% save(file_output, 'loss_base', '-append');


is_run = true;
x = load(file_output, 'retrieval_result');
if isfield(x, 'retrieval_result')
    retrieval_result = x.retrieval_result;
    if ~isempty(retrieval_result)
        is_run = false;
    end
end

if is_run
    file_name = [data_folder data_type '/OriginXTest.double.bin'];
    Xtest = read_mat(file_name, 'double');
    if ock_model.is_optimize_R
        Xtest = ock_model.R' * Xtest;
    end

    %%
    tic;
    retrieval_result = mexRetrievalSave(mat_compact_B, ...
    ock_model.all_D, ...
    Xtest, ...
    10000);
    time_retrieval = toc;
    save(file_output, 'retrieval_result', '-append');
    save(file_output, 'time_retrieval', '-append');
end




is_run = true;
x = load(file_output, 'rec');
if isfield(x, 'rec')
    rec = x.rec;
    if ~isempty(rec)
        is_run = false;
    end
end

if is_run
    ground_truth_file = [data_folder ...
    data_type '/STestBase.double.bin'];
    % recall

    tic;
    rec = mexResultRecall(retrieval_result, ...
    ground_truth_file, ...
    int32(topks), ...
    NBase);
    time_cost_recall = toc;
    save(file_output, 'rec', 'time_cost_recall', '-append', '-v7.3');
end

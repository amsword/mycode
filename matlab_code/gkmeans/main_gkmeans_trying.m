file_name = [data_folder data_type '/OriginXtraining.double.bin'];
Xtraining = read_mat(file_name, 'double');

%%
num_sub_problem = log(num_sub_dic_each_partition) / log(2) + 1;
num_sub_problem = floor(num_sub_problem);
assert(2 .^ (num_sub_problem - 1) == num_sub_dic_each_partition);

%%
clear opt_input_ock;
opt_input_ock.num_sub_dic_each_partition = 1;
opt_input_ock.num_partitions = num_sub_dic_each_partition;
opt_input_ock.sub_dic_size_each_partition = ...
    sub_dic_size_each_partition;
opt_input_ock.max_iter = 100;
opt_input_ock.num_grouped = num_grouped;

for idx_sub_problem = 1 : num_sub_problem
    ock_model = ock_training(Xtraining, opt_input_ock);
    
    if idx_sub_problem == num_sub_problem
        continue;
    end
    opt_input_ock.num_sub_dic_each_partition = ...
        opt_input_ock.num_sub_dic_each_partition * 2;
    opt_input_ock.num_partitions = opt_input_ock.num_partitions / 2;
    [opt_input_ock.init_R, opt_input_ock.init_all_D] = construct_init_dictionary(...
        ock_model.R, ...
        ock_model.all_D);
end
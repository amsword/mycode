
is_run = true;
x = load(file_output, 'retrieval_result');
if isfield(x, 'retrieval_result')
    retrieval_result = x.retrieval_result;
    if ~isempty(retrieval_result)
        is_run = false;
    end
end

if is_run
    file_name = [data_folder data_type '/OriginXTest.double.bin'];
    Xtest = read_mat(file_name, 'double');
    if ock_model.is_optimize_R
        Xtest = ock_model.R' * Xtest;
    end
    
    %%
    tic;
    retrieval_result = mexRetrievalSave(mat_compact_B, ...
        ock_model.all_D, ...
        Xtest, ...
        10000);
    time_retrieval = toc;
    save(file_output, 'retrieval_result', '-append');
    save(file_output, 'time_retrieval', '-append');
end

x = load(file_output, 'ock_model');
if isfield(x, 'ock_model')
    ock_model = x.ock_model;
else
    clear ock_model
    x = load(file_output, 'R', 'all_D');
    ock_model.R = x.R;
    ock_model.all_D = x.all_D;
    ock_model.is_optimize_R = 1;
    ock_model.num_grouped = num_grouped;
    ock_model.num_sub_dic_each_partition = ...
        num_sub_dic_each_partition;
    if strcmp(encoding_method, 'ock')
        ock_model.is_ock = 1;
    end
end

file_name = [data_folder data_type '/OriginXBase.double.bin'];
if strcmp(data_type, 'MNIST')
    file_name = [data_folder data_type '/OriginXtraining.double.bin'];
end


%%
% num_selected = 10000;
Xbase = read_mat(file_name, 'double');
start_encoding = tic;
mat_compact_B = ock_encoding(ock_model, Xbase);
time_encoding = toc(start_encoding);
encoding_machine = machine_name;
save(file_output, 'time_encoding', ...
    'encoding_machine', ...
    '-append');

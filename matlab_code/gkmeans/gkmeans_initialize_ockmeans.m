file_name = ['../../sckmeans/non_full_training/ock_' num2str(num_sub_dic_each_partition / 2) '_256_2_102.mat'];
x = load(file_name, 'all_D', 'R');
x_num_part = numel(x.all_D);
x_num_rows = 0;
x_num_cols = 0;
for i = 1 : x_num_part
    x_num_rows = x_num_rows + size(x.all_D{i}, 1);
    x_num_cols = x_num_cols + size(x.all_D{i}, 2);
end
init_all_D = zeros(x_num_rows, x_num_cols);
x_rows_start = 1;
x_cols_start = 1;
for i = 1 : x_num_part
    x_sub_rows = size(x.all_D{i}, 1);
    x_sub_cols = size(x.all_D{i}, 2);
    init_all_D(x_rows_start : x_rows_start + x_sub_rows - 1, ...
        x_cols_start : x_cols_start + x_sub_cols - 1) = x.all_D{i};
    x_rows_start = x_rows_start + x_sub_rows;
    x_cols_start = x_cols_start + x_sub_cols;
end
init_all_D = x.R * init_all_D;
assert(size(init_all_D, 1) == size(Xtraining, 1) && ...
    size(init_all_D, 2) == num_sub_dic_each_partition * sub_dic_size_each_partition);
opt_input_ock.init_all_D = {init_all_D};

all_time_type = {'training_time', 'encoding_time'};
for idx_time_type = 1 : 2
    time_type = all_time_type{idx_time_type};
    
    figure;
    for num_sub_dic_each_partition = [4 8 16]
        all_num_can = [1 4 8 12 16];
        
        if num_sub_dic_each_partition == 4
            line_style = '-';
        elseif num_sub_dic_each_partition == 8
            line_style = '--';
        elseif num_sub_dic_each_partition == 16
            line_style = '-.';
        end
        
        num_all_num_can = numel(all_num_can);
        
        % data of AQ
        idx_initialize_type = 1;
        all_initialize_type = ...
            {'random', 'kmeans', 'ockmeans', 'h_ockmeans'};
        encoding_method = 'additive_quantization';
        num_partitions = 1;
        
        sub_dic_size_each_partition = 256;
        initialize_type = all_initialize_type{idx_initialize_type};
        base_energy = load('energy.mat');
        base_energy = base_energy.base_energy;
        idx = 1;
        clear aq_num_can;
        clear aq_relative_distortions;
        clear aq_encoding_time;
        for num_grouped = 1 : num_all_num_can
            num_can = all_num_can(num_grouped);
            num_can_encode = num_can;
            file_output = [encoding_method '_' ...
                num2str(num_partitions) '_' ...
                num2str(num_sub_dic_each_partition) '_' ...
                num2str(sub_dic_size_each_partition) '_' ...
                'num_can_train' num2str(num_can) '_' ...
                'num_can_encode' num2str(num_can_encode) '_' ...
                'initialize_type' initialize_type '.mat'];
            if exist(file_output, 'file')
                x = load(file_output, 'loss_base', 'time_encoding', 'ock_model');
                if strcmp(time_type, 'training_time')
                    aq_encoding_time(idx) = x.ock_model.training_time;
                    assert(numel(x.ock_model.obj) == 300);
                elseif strcmp(time_type, 'encoding_time')
                    aq_encoding_time(idx) = x.time_encoding;
                end
                aq_relative_distortions(idx) = x.loss_base / base_energy;
                aq_num_can(idx) = num_can;
                
                idx = idx + 1;
            end
        end
        
        % load gk
        gk_relative_distortion = zeros(2, 1);
        gk_encoding_time = zeros(2, 1);
        encoding_method = 'gkmeans';
        for num_grouped = 1 : 2
            file_output = [encoding_method ...
                '_1_' num2str(num_sub_dic_each_partition) ...
                '_256_num_grouped' ...
                num2str(num_grouped) '_initialize_typeh_ockmeans_10_' ...
                num2str(100 - log(num_sub_dic_each_partition) / log(2) * 10) ...
                '.mat'];
            if exist(file_output, 'file')
                x = load(file_output, 'loss_base', 'time_encoding', 'ock_model');
                if strcmp(time_type, 'training_time')
                    gk_encoding_time(num_grouped) = x.ock_model.training_time;
                    file_name = ['h_ockmeans_init_1' ...
                        num2str(num_sub_dic_each_partition) '_' ...
                        num2str(sub_dic_size_each_partition) '_' ...
                        num2str(10) '_' ...
                        num2str(num_grouped) '.mat'];
                    x1 = load(file_name, 'time_h_init');
                    gk_encoding_time(num_grouped) = gk_encoding_time(num_grouped) + ...
                        x1.time_h_init;
                elseif strcmp(time_type, 'encoding_time')
                    gk_encoding_time(num_grouped) = x.time_encoding;
                end
                gk_relative_distortion(num_grouped) = x.loss_base / base_energy;
            end
        end
        
        %
        semilogy(gk_relative_distortion, gk_encoding_time, 'ro', ...
            'markersize', gk_marker_size, 'markerfacecolor', 'r', ...
            'linewidth', line_width, 'linestyle', line_style);
        if num_sub_dic_each_partition == 16
            for num_grouped = 1 : 2
                text(gk_relative_distortion(num_grouped), gk_encoding_time(num_grouped), ...
                    ['\leftarrow' num2str(num_grouped)], 'fontsize', txt_font_size);
            end
        else
            for num_grouped = 1 : 2
                text(gk_relative_distortion(num_grouped) - 0.01, gk_encoding_time(num_grouped), ...
                    [num2str(num_grouped) '\rightarrow'], 'fontsize', txt_font_size);
            end
        end
        
        hold on;
        semilogy(aq_relative_distortions, aq_encoding_time, '*', ...
            'linewidth', line_width, 'markersize', aq_marker_size, ...
            'linestyle', line_style);
        for num_grouped = 1 : numel(aq_num_can)
            num_can = all_num_can(num_grouped);
            text(aq_relative_distortions(num_grouped), aq_encoding_time(num_grouped), ...
                ['  \leftarrow ' num2str(num_can)], 'fontsize', txt_font_size);
        end
        hold on;
        
        % if num_sub_dic_each_partition == 4
        %     xlim([0.195, 0.235]);
        %     ylim([0, 70]);
        % elseif num_sub_dic_each_partition == 8
        %     xlim([0.11, 0.145]);
        % end
        if strcmp(data_type, 'SIFT1M')
            %         xlim([0.09, 0.25]);
            %         ylim([8, 10^3]);
            if strcmp(time_type, 'encoding_time')
                xlim([0.04, 0.25]);
            end
        elseif strcmp(data_type, 'MNIST')
            xlim([0.05, 0.19]);
            ylim([2, 10^3]);
        elseif strcmp(data_type, 'cifar10_nin_cccp6');
            if strcmp(time_type, 'encoding_time')
            elseif strcmp(time_type, 'training_time')
                ylim([110, 3*10^4]);
            end
        end
        grid on;
        set(gca, 'fontsize', 14);
        xlabel('Relative distortion');
        ylabel('Encoding time (seconds)');
        
    end
    legend([lg_gk '_{32}'], [lg_aq '_{32}'], ...
        [lg_gk '_{64}'], [lg_aq '_{64}'], ...
        [lg_gk '_{128}'], [lg_aq '_{128}']);
    
    
    if strcmp(time_type, 'training_time')
        saveas(gca, ...
            [save_folder data_type '_train_time_distortion.eps'], 'psc2');
    elseif strcmp(time_type, 'training_time')
        saveas(gca, ...
            [save_folder data_type '_encode_time_distortion.eps'], 'psc2');
    end
end

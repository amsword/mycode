
%% encode
is_run = true;
x = load(file_output, 'mat_compact_B');
if isfield(x, 'mat_compact_B') && isa(x.mat_compact_B, 'uint8')
    is_run = false;
    mat_compact_B = x.mat_compact_B;
    NBase = size(mat_compact_B, 2);
    warning('exist mat_compact_B');
end

% is_run = true;
if is_run
    %%
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    if strcmp(data_type, 'MNIST')
        file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    end
    Xbase = read_mat(file_name, 'double');
    x = load('energy.mat');
    if ~isfield(x, 'base_energy');
        base_energy = sum(Xbase(:) .^ 2);
        save('energy.mat', 'base_energy', '-append');
    end

    ock_model.para_encode.num_can_encode = num_can_encode;
    ock_model.para_encode.num_can = num_can_encode;
    x = tic;
    mat_compact_B = ock_encoding(ock_model, Xbase);
    time_encoding = toc(x);
	time_encoding
	machine_name_encode = getComputerName();
    save(file_output, 'mat_compact_B', 'time_encoding', 'machine_name_encode', '-append');
end

% loss on the database
is_run = true;
x = load(file_output, 'loss_base');
if isfield(x, 'loss_base')
    is_run = false;
end

if is_run
    file_name = [data_folder data_type '/OriginXBase.double.bin'];
    if strcmp(data_type, 'MNIST')
        file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    end
    Xbase = read_mat(file_name, 'double');
    if ock_model.is_optimize_R
        Xbase = ock_model.R' * Xbase;
    end
    
    loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)
    
    save(file_output, 'loss_base', '-append');
    NBase = size(mat_compact_B, 2);
    
    is_cal = true;
    if exist('energy.mat', 'file')
        x = load('energy.mat', 'base_energy');
        if isfield(x, 'base_energy')
            is_cal = false;
        end
    end
    if is_cal
        base_energy = sum(Xbase(:) .^ 2);
        save('energy.mat', 'base_energy', '-append');
    end
    
    clear Xbase;
end
% 
% file_name = [data_folder data_type '/OriginXBase.double.bin'];
% if strcmp(data_type, 'MNIST')
%     file_name = [data_folder data_type '/OriginXtraining.double.bin'];
% end
% Xbase = read_mat(file_name, 'double');
% 
% if ock_model.is_optimize_R
%     Xbase = ock_model.R' * Xbase;
% end
% 
% loss_base = DistortionLoss(Xbase, ock_model.all_D, mat_compact_B)
% 
% save(file_output, 'loss_base', '-append');


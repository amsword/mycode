num_sub_dic_each_partition = 8;
figure;
gkmeans_load_plot_rec;

loc = 'Best';
if strcmp(data_type, 'SIFT1M')
    selected = [1 : 9, 10 .^ [1 : 0.2 : 2]];
elseif strcmp(data_type, 'GIST1M')
    selected = [1 : 9, 10 .^ [1 : 0.2 : 2]];
    loc = 'northwest';
elseif strcmp(data_type, 'MNIST');
    selected = [1 : 9, 10 .^ [1 : 0.2 : 2]];
end
selected = round(selected);

clear collected;
choosed = 10;
collected(1) = ours_h.rec(choosed, 1);
collected(2) = ours_k.rec(choosed, 1);
collected(3) = ours_r.rec(choosed, 1);
collected(4) = ours_h1.rec(choosed, 1);
collected(5) = ours_k1.rec(choosed, 1);
collected(6) = ours_r1.rec(choosed, 1);
collected(7) = ock.rec(choosed, 1);
collected(8) = ck.rec(choosed, 1);

bar(collected);

if strcmp(data_type, 'MNIST')
    if num_sub_dic_each_partition == 8
        ylim([0.65, 1]);
    end
    
end


grid on;
% 
% legend([name_gkmeans_2_h_all_rec '-128'], ...
%     [name_ock_all_rec '-128'], ...
%     [name_ck_all_rec '-128'], ...
%     [name_gkmeans_2_h_all_rec '-64'], ...
%     [name_ock_all_rec '-64'], ...
%     [name_ck_all_rec '-64'], ...
%     [name_gkmeans_2_h_all_rec '-32'], ...
%     [name_ock_all_rec '-32'], ...
%     [name_ck_all_rec '-32'], ...
%     'Location', loc);
% 
set(gca, 'FontSize', fontsize);
% 
% xlabel('Number of top-ranked points');
% ylabel('Recall');

% saveas(gca, [save_folder data_type ...
%     '_rec_all_codes.eps'], 'psc2');
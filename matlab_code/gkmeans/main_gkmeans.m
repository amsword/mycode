function main_gkmeans(...
     num_sub_dic_each_partition, ...
    num_can)

num_sub_dic_each_partition = 4;
num_partitions = 1;
idx_initialize_type = 4;
num_grouped = 1;
data_type = 'SIFT1M' %%
% data_type = 'GIST1M' 
% data_type = 'cifar10_nin_cccp6'
% data_type = 'MNIST';
encoding_method = 'gkmeans';
% encoding_method = 'ock';
% encoding_method = 'additive_quantization';
iter_mnist = 100;
num_can = 12;
num_can_encode = 12;
num_can_encode = num_can
is_test_encoding_time = 0;

all_initialize_type = ...
    {'random', 'kmeans', 'ockmeans', 'h_ockmeans'};
initialize_type = all_initialize_type{idx_initialize_type}

% if strcmp(encoding_method, 'additive_quantization')
%     assert(idx_initialize_type == 1);
% end



% almost non-changed.
h_ockmeans_initi_num_iter = 10
h_ockmeans_final_num_iter = 100 - log(num_sub_dic_each_partition) / log(2) * h_ockmeans_initi_num_iter;
assert(h_ockmeans_final_num_iter == floor(h_ockmeans_final_num_iter));
assert(h_ockmeans_final_num_iter > 0);
max_num_sub_dic_each_partition = 16;
topks = [1, 10, 100];
sub_dic_size_each_partition = 256;
data_type
num_grouped
num_sub_dic_each_partition
encoding_method

[ret, machine_name] = system('hostname');   
if ret ~= 0,
   if ispc
      machine_name = getenv('COMPUTERNAME');
   else      
      machine_name = getenv('HOSTNAME');      
   end
end
machine_name = lower(machine_name);
machine_name(end) = []
if strcmp(machine_name, 'evdell18')
    data_folder = 'E:\research\data\';
    working_folder = 'E:\research\working\';
    conf_dir = 'D:\OneDrive\BitBucketCode\mycode\matlab_code\';
elseif strcmp(machine_name, 'psl')
    data_folder = '/home/liuluoqi/jianfeng/data/';
    working_folder = '/home/liuluoqi/jianfeng/working/';
    conf_dir = '/home/liuluoqi/jianfeng/code/mycode/matlab_code/';
elseif strcmp(machine_name(1 : 2), 'lv')
	data_folder = '/home/jianfeng/data/';
    working_folder = '/home/jianfeng/working/';
elseif strcmp(machine_name(1 : 4), 'deep')
    data_folder = '/home/wangjianfeng/data/';
    working_folder = '/home/wangjianfeng/working/';
    conf_dir = '/home/wangjianfeng/code/mycode/matlab_code/';
else
    machine_name
    data_folder = '/home/liuluoqi/jianfeng/data/';
    working_folder = '/home/liuluoqi/jianfeng/working/';
    conf_dir = '/home/liuluoqi/jianfeng/code/mycode/matlab_code/';
end
cd(conf_dir);
jf_conf;
cd([working_folder data_type '/gkmeans/non_full_training']);

if strcmp(initialize_type, 'h_ockmeans')
    file_output = [encoding_method '_' num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_grouped' num2str(num_grouped) '_' ...
        'initialize_type' initialize_type '_' ...
        num2str(h_ockmeans_initi_num_iter) '_' ...
        num2str(h_ockmeans_final_num_iter) '.mat'];
else
    %
    file_output = [encoding_method '_' ...
        num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_grouped' num2str(num_grouped) '_' ...
        'initialize_type' initialize_type '.mat'];
end

if strcmp(encoding_method, 'ock')
%    if num_sub_dic_each_partition == 1
%        file_output = ['../../sckmeans/non_full_training/ck_' ...
%            num2str(num_partitions) '_' ...
%            num2str(sub_dic_size_each_partition) ...
%            '.mat'];
%    else
%        file_output = ['../../sckmeans/non_full_training/ock_' ...
%            num2str(num_partitions) '_' ...
%            num2str(sub_dic_size_each_partition) '_' ...
%            num2str(num_sub_dic_each_partition) '_102.mat'];
%    end
    file_output = [encoding_method '_' ...
        num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_can_train' num2str(num_can) '_' ...
        'num_can_encode' num2str(num_can_encode) '_' ...
        'initialize_type' initialize_type '.mat'];
elseif strcmp(encoding_method, 'additive_quantization')
    file_output = [encoding_method '_' ...
        num2str(num_partitions) '_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '_' ...
        'num_can_train' num2str(num_can) '_' ...
        'num_can_encode' num2str(num_can_encode) '_' ...
        'initialize_type' initialize_type '.mat'];
	if strcmp(initialize_type, 'h_ockmeans')
		 file_output = [encoding_method '_' num2str(num_partitions) '_' ...
   		     num2str(num_sub_dic_each_partition) '_' ...
   		     num2str(sub_dic_size_each_partition) '_' ...
   		     'num_can_train' num2str(num_can) '_' ...
			 'num_can_encode' num2str(num_can_encode) '_' ...
   		     'initialize_type' initialize_type '_' ...
   		     num2str(h_ockmeans_initi_num_iter) '_' ...
   		     num2str(h_ockmeans_final_num_iter) '.mat'];
	end
end

if strcmp(data_type, 'MNIST') && ...
        iter_mnist ~= 100 && ...
        strcmp(encoding_method, 'ock')
    if num_sub_dic_each_partition == 1
        file_output = ['../../sckmeans/non_full_training/ck_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) '_iter_' ...
            num2str(iter_mnist) '.mat'];
    else
        file_output = ['../../sckmeans/non_full_training/ock_' ...
            num2str(num_partitions) '_' ...
            num2str(sub_dic_size_each_partition) '_' ...
            num2str(num_sub_dic_each_partition) '_' ...
            'iter_' num2str(iter_mnist) '_102.mat'];
    end
end


file_output

%%
if is_test_encoding_time
    main_gkmeans_test_encoding_time;
else
    main_gkmeans_one_case;
end

% main_gkmeans_trying
% gkmeans_plot_result

%%
% x = [2 4 8 16];
% ours1 = [8.6 20.3 120.4 871.69];
% ours2 = [45.5 110.3 392.9 1.6726e+03];
% % y2 = [12.2 723.3 10790250];
% semilogy(x, ours1, 'r-o', 'markersize', 15);
% hold on;
% semilogy(x, ours2, 'r-*', 'markersize', 15);
% plot(x(1 : end - 1), y2, 'b-*', 'markersize', 15);

%% check whether  the two codes are the same

% int8_code = mat_compact_B;
% int16_code = x.mat_compact_B;
% for i = 1 : size(int8_code, 2)
%     int8_sub = int8_code(:, i);
%     int16_sub = int16_code(:, i);
%     for j = 1 : size(int8_code, 1)
%         a1 = double(int8_sub(j)) + (j - 1) * 256;
%         a2 = double(int16_sub(j));
%         assert(a1 == a2);
%     end
% end




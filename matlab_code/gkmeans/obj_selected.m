function selected = obj_selected(obj)

selected = [1 : 5 : numel(obj)];

if selected(end) ~= numel(obj)
    selected = [selected, numel(obj)];
end
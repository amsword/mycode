
x = load('energy.mat');
base_energy = x.base_energy;

k = 1;
rloss_base(k) = ours_h.loss_base / base_energy;
k = k + 1;

rloss_base(k) = ours_h1.loss_base / base_energy;
k = k + 1;

rloss_base(k) = ours_k.loss_base / base_energy;
k = k + 1;

rloss_base(k) = ours_r.loss_base / base_energy;
k = k + 1;

rloss_base(k) = ock.loss_base / base_energy;
k = k + 1;

rloss_base(k)  = ck.loss_base / base_energy;
k = k + 1;

figure;
bar(rloss_base);
ylim([0.18, 0.25]);
set(gca, 'fontsize', fontsize');
grid on;
set(gca, 'xticklabel', {name_gkmeans_2_h, name_gkmeans_1_h, ...
    name_gkmeans_2_k, name_gkmeans_2_r, name_ock, ...
    name_ck});

saveas(gca, [save_folder data_type '_' ...
    num2str(num_sub_dic_each_partition) '_base_obj.eps'], 'psc2');






%%

figure;

if strcmp(data_type, 'SIFT1M')
    if num_sub_dic_each_partition == 4
        selected = [1 : 2 : 9, 10 .^ [1 : 0.2 : 3]];
    elseif num_sub_dic_each_partition == 8
        selected = [1 : 2 : 9, 10 .^ [1 : 0.2 : 2]];
    elseif num_sub_dic_each_partition == 16
        selected = [1 : 2 : 9, 10 .^ [1 : 0.2 : 2]];
%         selected = [1 : 1 : 10];
    end
elseif strcmp(data_type, 'MNIST')
        selected = [1 : 9, 10 .^ [1 : 0.2 : 2]];
end

selected = round(selected);

semilogx(selected, ours_h.rec(selected, 1), style_gkmeans_2_h, ...
    'LineWidth', linewidth);
hold on;
semilogx(selected, ours_k.rec(selected, 1), style_gkmeans_2_k, ...
    'LineWidth', linewidth);
semilogx(selected, ours_r.rec(selected, 1), style_gkmeans_2_r, ...
    'LineWidth', linewidth);

semilogx(selected, ours_h1.rec(selected, 1), style_gkmeans_1_h, ...
    'LineWidth', linewidth);
semilogx(selected, ours_k1.rec(selected, 1), style_gkmeans_1_k, ...
    'LineWidth', linewidth);
semilogx(selected, ours_r1.rec(selected, 1), style_gkmeans_1_r, ...
    'LineWidth', linewidth);

plot(selected, ock.rec(selected, 1), style_ock, 'LineWidth', linewidth);
plot(selected, ck.rec(selected, 1), style_ck, 'LineWidth', linewidth);
grid on;
loc = 'Best';

if strcmp(data_type, 'MNIST')
    if num_sub_dic_each_partition == 4
        loc = 'SouthEast';
    end
end

legend(name_gkmeans_2_h, ...
    name_gkmeans_2_k, ...
    name_gkmeans_2_r, ...
    name_gkmeans_1_h, ...
    name_gkmeans_1_k, ...
    name_gkmeans_1_r, ...
    name_ock, ...
    name_ck, ...
    'Location', loc);


% legend(name_gkmeans_2_h, ...
%     name_ock, ...
%     name_ck, ...
%     'Location', loc);




% ylim([0.25, 1]);
set(gca, 'FontSize', fontsize);

xlabel('Number of top-ranked points');
ylabel('Recall');

saveas(gca, [save_folder data_type '_' ...
    num2str(num_sub_dic_each_partition) '_rec.eps'], 'psc2');





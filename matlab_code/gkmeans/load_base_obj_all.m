x = load('energy.mat', 'base_energy');
base_energy = x.base_energy;

num_sub_dic_each_partition = 4;
gkmeans_load_plot;
load_base_obj;
clear all_obj
all_obj(1, :) = rloss_base;

num_sub_dic_each_partition = 8;
gkmeans_load_plot;
load_base_obj;
all_obj(2, :) = rloss_base;

num_sub_dic_each_partition = 16;
gkmeans_load_plot;
load_base_obj;
all_obj(3, :) = rloss_base;
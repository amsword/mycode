num_sub_dic_each_partition = 2;
load_mcmp_related;
time_gkmeans_1(1) = ours_h1.time_encoding;
time_gkmeans_2(1) = ours_h.time_encoding;
time_ock(1) = ock.time_encoding;
all_gkmeans_1(1) = ours_h1.loss_base / base_energy;
all_gkmeans_2(1) = ours_h.loss_base / base_energy;
all_ock(1) = ock.loss_base / base_energy;

num_sub_dic_each_partition = 4;
load_mcmp_related;
time_gkmeans_1(2) = ours_h1.time_encoding;
time_gkmeans_2(2) = ours_h.time_encoding;
time_ock(2) = ock.time_encoding;
all_gkmeans_1(2) = ours_h1.loss_base / base_energy;
all_gkmeans_2(2) = ours_h.loss_base / base_energy;
all_ock(2) = ock.loss_base / base_energy;

num_sub_dic_each_partition = 8;
load_mcmp_related;
time_gkmeans_1(3) = ours_h1.time_encoding;
time_gkmeans_2(3) = ours_h.time_encoding;
time_ock(3) = ock.time_encoding;
all_gkmeans_1(3) = ours_h1.loss_base / base_energy;
all_gkmeans_2(3) = ours_h.loss_base / base_energy;

%%
gkmeans_write_text_mcmp
%%
figure;
semilogy(time_gkmeans_2, 'rd-');
hold on;
semilogy(time_gkmeans_1, 'bd-');
semilogy(time_ock, 'm*-');

%%
figure;
plot(all_gkmeans_2, 'rd-', 'linewidth', linewidth);
hold on;
plot(all_gkmeans_1, 'bd-', 'linewidth', linewidth);
plot(all_ock, 'm*-', 'linewidth', linewidth);
%%


clear all_data_obj
for idx_data_type = 1 : numel(all_data_type)
    data_type = all_data_type{idx_data_type};
    prepare_folder_name;
    cd([working_folder data_type '/gkmeans/non_full_training']);
    load_base_obj_all;
    all_data_obj{idx_data_type} = all_obj;
end


file_output = [save_pair_folder 'table_base_obj.tex'];

fp = fopen(file_output, 'w');

line = '\begin{table}';
fprintf(fp, '%s\n', line);
line = '\centering';
fprintf(fp, '%s\n', line);
line = '\caption{Relative distortion ($\times 10^{-2}$) on the databases with different code lengths.}';
fprintf(fp, '%s\n', line);
line = '\label{tbl:base_loss}';
fprintf(fp, '%s\n', line);

line = '\begin{tabular}{c@{~~}cccccccc@{~~}c}';
fprintf(fp, '%s\n', line);

line = '\toprule'; fprintf(fp, '%s\n', line);
line = '\multicolumn{2}{c}{} &'; fprintf(fp, '%s\n', line);
line = '\multicolumn{6}{c}{gk-means} & '; fprintf(fp, '%s\n', line);
line = '\multirow{2}{*}{ock-means } & '; fprintf(fp, '%s\n', line);
line = '\multirow{2}{*}{ck-means} \\ '; fprintf(fp, '%s\n', line);
line = '\cmidrule{3-8} '; fprintf(fp, '%s\n', line);
line = '\multicolumn{2}{c}{} & (2, h) & (2, k) & (2, r) & (1, h) & (1, k) & (1, r) & & \\ ';
fprintf(fp, '%s\n', line);
line = '\midrule'; fprintf(fp, '%s\n', line);

for k = 1 : 3
    line = ['\multirow{3}{*}{' all_data_type{k} '} '];
    fprintf(fp, '%s', line);
    all_obj = all_data_obj{k};
    for j = 1 : 3
        line = [' & ' num2str(2 ^ (j + 4)) ' '];
        fprintf(fp, '%s', line);
        [~, selected_idx] = min(all_obj(j, :));
        [~, worsest_idx] = max(all_obj(j, :));
        for i = 1 : size(all_obj, 2)
            if i == selected_idx
                line = sprintf(' & $\\mathbf{%.2f}$ ', 100 * all_obj(j, i));
            elseif i == worsest_idx
                line = sprintf(' & $\\textit{%.2f}$ ', 100 * all_obj(j, i));
            else
                line = sprintf(' & %.2f ', 100 * all_obj(j, i));
            end
            fprintf(fp, '%s', line);
        end
        line = '\\';
        fprintf(fp, '%s\n', line);
    end
    if k ~= 3
        line = '\midrule';
        fprintf(fp, '%s\n', line);
    end
end
line = '\bottomrule';
fprintf(fp, '%s\n', line);

line = '\end{tabular}';
fprintf(fp, '%s\n', line);
line = '\end{table}';
fprintf(fp, '%s\n', line);

fclose(fp);
linewidth = 2;

x = load('energy.mat');
training_energy = x.training_energy;

figure;
hold on;
obj = ours_h.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), style_gkmeans_2_h, ...
    'linewidth', linewidth);

obj = ours_k.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), ...
    style_gkmeans_2_k, 'linewidth', linewidth);

obj = ours_r.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), ...
    style_gkmeans_2_r, 'linewidth', linewidth);

obj = ours_h1.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), ...
    style_gkmeans_1_h, 'linewidth', linewidth);

obj = ours_k1.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), ...
    style_gkmeans_1_k, 'linewidth', linewidth);

obj = ours_r1.ock_model.obj(3 : 3 : end) / training_energy;
selected = obj_selected(obj);
plot(selected, obj(selected), ...
    style_gkmeans_1_r, 'linewidth', linewidth);

obj = ock.obj(3 : 3 : end) / training_energy;
% selected = obj_selected(obj);
selected = 1 : numel(obj);
plot(selected, obj(selected), ...
    style_ock, 'linewidth', linewidth);

obj = ck.obj(3 : 3 : end) / training_energy;
selected = 1 : numel(obj);
plot(selected, obj(selected), ...
    style_ck, 'linewidth', linewidth);

loc = 'North';
% 

grid on;
if strcmp(data_type, 'SIFT1M')
    if num_sub_dic_each_partition == 4
        ylim([0.18, 0.28]);
        loc = 'North';
    elseif num_sub_dic_each_partition == 8
        ylim([0.09, 0.24]);
        loc = 'North';
    elseif num_sub_dic_each_partition == 16
        ylim([0.03, 0.25]);
        loc = 'North';
    end
elseif strcmp(data_type, 'GIST1M')
    if num_sub_dic_each_partition == 4
        ylim([0.39, 0.5]);
    elseif num_sub_dic_each_partition == 8
        ylim([0.3, 0.45]);
    elseif num_sub_dic_each_partition == 16
        ylim([0.22, 0.4]);
    end
elseif strcmp(data_type, 'MNIST')
    if num_sub_dic_each_partition == 4
        ylim([0.13, 0.3]);
    elseif num_sub_dic_each_partition == 8
        ylim([0.07, 0.3]);
    elseif num_sub_dic_each_partition == 16
        ylim([0.04, 0.3]);
    end
end

legend(name_gkmeans_2_h, ...
    name_gkmeans_2_k, ...
    name_gkmeans_2_r, ...
    name_gkmeans_1_h, ...
    name_gkmeans_1_k, ...
    name_gkmeans_1_r, ...
    name_ock, ...
    name_ck, 'location', loc);

set(gca, 'fontsize', fontsize);

xlabel('Iteration', 'FontSize', fontsize);
ylabel('Relative distortion', 'fontsize', fontsize');


saveas(gca, [save_folder data_type '_' ...
    num2str(num_sub_dic_each_partition) '_train_obj.eps'], 'psc2');


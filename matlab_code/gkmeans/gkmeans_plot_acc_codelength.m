%% load aq
lg_aq = 'AQ';
lg_gk = 'gk-means';
line_width = 1.5;
txt_font_size = 12;
aq_marker_size = 8;
gk_marker_size = 8;


for idx_data_type = 2 : 2
% for idx_data_type = 2 : numel(all_data_type)
    data_type = all_data_type{idx_data_type}
    cd([working_folder data_type '/gkmeans/non_full_training']);
    
    gk_means_plot_acc_codelength_one;
end


is_run = true;
x = load(file_output, 'rec');
if isfield(x, 'rec')
    rec = x.rec;
    if ~isempty(rec)
        is_run = false;
    end
end

if is_run
    ground_truth_file = [data_folder ...
        data_type '/STestBase.double.bin'];
    % recall
    
    tic;
    rec = mexResultRecall(retrieval_result, ...
        ground_truth_file, ...
        int32(topks), ...
        NBase);
    time_cost_recall = toc;
    save(file_output, 'rec', 'time_cost_recall', '-append', '-v7.3');
end
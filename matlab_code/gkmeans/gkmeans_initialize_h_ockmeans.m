num_sub_problem = log(num_sub_dic_each_partition) / log(2);
num_sub_problem = floor(num_sub_problem);
assert(2 .^ (num_sub_problem) == num_sub_dic_each_partition);

%%

if strcmp(encoding_method, 'gkmeans')
	file_name = ['h_ockmeans_init_1' ...
		num2str(num_sub_dic_each_partition) '_' ...
		num2str(sub_dic_size_each_partition) '_' ...
		num2str(h_ockmeans_initi_num_iter) '_' ...
		num2str(num_grouped) '.mat'];
elseif strcmp(encoding_method, 'additive_quantization')
	file_name = ['h_' encoding_method '_init_1' ...
		num2str(num_sub_dic_each_partition) '_' ...
		num2str(sub_dic_size_each_partition) '_' ...
		num2str(h_ockmeans_initi_num_iter) '_' ...
		num2str(num_can) '.mat'];
elseif strcmp(encoding_method, 'ock')
	file_name = ['h_' encoding_method '_init_1' ...
		num2str(num_sub_dic_each_partition) '_' ...
		num2str(sub_dic_size_each_partition) '_' ...
		num2str(h_ockmeans_initi_num_iter) '_' ...
		num2str(num_can) '.mat'];
else
	error('dfsf');
end
%%
if exist(file_name, 'file')
    x = load(file_name);
    opt_input_ock2 = x.opt_input_ock2;
else
    %%
    clear opt_input_ock2;
    opt_input_ock2.num_sub_dic_each_partition = 1;
    opt_input_ock2.num_partitions = num_sub_dic_each_partition;
    opt_input_ock2.sub_dic_size_each_partition = ...
        sub_dic_size_each_partition;
    opt_input_ock2.max_iter = h_ockmeans_initi_num_iter;
%         opt_input_ock2.max_iter = 30;
    opt_input_ock2.num_grouped = num_grouped;
    
    opt_input_ock2.encoding_type = encoding_method;
    opt_input_ock2.num_can_train = num_can;
    opt_input_ock2.num_can = num_can;

    initial_random_file = ['h_ockmeans_init_first_' ...
        num2str(num_sub_dic_each_partition) '_' ...
        num2str(sub_dic_size_each_partition) '.mat'];
    if exist(initial_random_file, 'file')
        x = load(initial_random_file);
        opt_input_ock2.init_all_D = x.init_all_D;
        'loaded from first'
    else
        [sub_dim_start_idx] = dim_split(size(Xtraining, 1), opt_input_ock2.num_partitions);
        init_all_D = ...
            RandomInitDictionary(Xtraining, sub_dim_start_idx, ...
            opt_input_ock2.sub_dic_size_each_partition * ...
            opt_input_ock2.num_sub_dic_each_partition);
        opt_input_ock2.init_all_D = init_all_D;
        save(initial_random_file, 'init_all_D');
    end
    
    all_ock_model = cell(num_sub_problem, 1);
   
	t1 = tic;
    for idx_sub_problem = 1 : num_sub_problem
		t2 = tic;
        ock_model = ock_training(Xtraining, opt_input_ock2);
		time_curr = toc(t2);
        all_ock_model{idx_sub_problem} = ock_model;
        
        opt_input_ock2.num_sub_dic_each_partition = ...
            opt_input_ock2.num_sub_dic_each_partition * 2;
        opt_input_ock2.num_partitions = opt_input_ock2.num_partitions / 2;
        [opt_input_ock2.init_R, opt_input_ock2.init_all_D] = construct_init_dictionary(...
            ock_model.R, ...
            ock_model.all_D);
    end
	time_h_init = toc(t1);
    save(file_name, 'opt_input_ock2', 'all_ock_model', 'time_h_init');
end
opt_input_ock.init_all_D = opt_input_ock2.init_all_D;
opt_input_ock.max_iter = h_ockmeans_final_num_iter;



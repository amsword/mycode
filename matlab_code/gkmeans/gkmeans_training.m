is_run = true;
if exist(file_output, 'file')
    x = load(file_output, 'ock_model');
    if isfield(x, 'ock_model')
		if isfield(x.ock_model, 'training_time')
			 is_run = false;
       		 ock_model = x.ock_model;
       		 clear x;
		end
    end
end

%%
if is_run
    
    file_name = [data_folder data_type '/OriginXtraining.double.bin'];
    Xtraining = read_mat(file_name, 'double');
	whos Xtraining
    
    file_name = ['k_means_initialize_' num2str(max_num_sub_dic_each_partition) '.mat'];
    if ~exist(file_name, 'file')
        init_all_D = zeros(size(Xtraining, 1), max_num_sub_dic_each_partition * sub_dic_size_each_partition);
        
        X = Xtraining;
        clear para;
		clear k_means_distortions;
		clear kmeans_time;
        para.max_iter = 30;
        for k = 1 : max_num_sub_dic_each_partition
			t1 = tic;
            centers = w_kmeans(X, sub_dic_size_each_partition, para);
            coarse_idx = w_assignment(X, centers);
            X = X - centers(:, coarse_idx);
			kmeans_time(k) = toc(t1); 
		    k_means_distortions(k) = sum(X(:) .^ 2);
            init_all_D(:, (k - 1) * sub_dic_size_each_partition + 1 : k * sub_dic_size_each_partition) = centers;
        end
        save(file_name, 'init_all_D', 'kmeans_time', 'k_means_distortions', 'para');
    end
    file_name = ['k_means_initialize_' num2str(max_num_sub_dic_each_partition) '_50.mat'];
    if ~exist(file_name, 'file')
        init_all_D = zeros(size(Xtraining, 1), max_num_sub_dic_each_partition * sub_dic_size_each_partition);
        
        X = Xtraining;
        clear para;
		clear k_means_distortions;
		clear kmeans_time;
        para.max_iter = 50;
        for k = 1 : max_num_sub_dic_each_partition
			t1 = tic;
            centers = w_kmeans(X, sub_dic_size_each_partition, para);
            coarse_idx = w_assignment(X, centers);
            X = X - centers(:, coarse_idx);
			kmeans_time(k) = toc(t1); 
		    k_means_distortions(k) = sum(X(:) .^ 2);
            init_all_D(:, (k - 1) * sub_dic_size_each_partition + 1 : k * sub_dic_size_each_partition) = centers;
        end
        save(file_name, 'init_all_D', 'kmeans_time', 'k_means_distortions', 'para');
    end
    %
    
    clear opt_input_ock;
    opt_input_ock.num_sub_dic_each_partition = num_sub_dic_each_partition;
    opt_input_ock.num_partitions = num_partitions;
    
    if strcmp(data_type, 'MNIST') && ...
            strcmp(encoding_method, 'ock')
        opt_input_ock.max_iter = iter_mnist;
    end
    opt_input_ock.sub_dic_size_each_partition = ...
        sub_dic_size_each_partition;
    %
    opt_input_ock.num_grouped = num_grouped;
    opt_input_ock.encoding_type = encoding_method;
    opt_input_ock.num_can_train = num_can;
    opt_input_ock.num_can = num_can;
    if ~strcmp(encoding_method, 'ock')
        if strcmp(initialize_type, 'kmeans')
            file_name = ['k_means_initialize_' num2str(max_num_sub_dic_each_partition) '.mat'];
            x = load(file_name);
            opt_input_ock.init_all_D = {x.init_all_D(:, 1 : num_sub_dic_each_partition * sub_dic_size_each_partition)};
        elseif strcmp(initialize_type, 'ockmeans')
            gkmeans_initialize_ockmeans;
        elseif strcmp(initialize_type, 'random')
            gkmeans_initialize_random;
        elseif strcmp(initialize_type, 'h_ockmeans')
            gkmeans_initialize_h_ockmeans;
        else
            error('dfs');
        end
    end
	if strcmp(encoding_method, 'ock')
		if strcmp(initialize_type, 'random')
			gkmeans_initialize_random;
		else
			error('dfs');
		end
	end
    %
    if strcmp(encoding_method, 'ock')
        opt_input_ock.is_ock = 1;
    end
%%
%     if strcmp(encoding_method, 'additive_quantization')
%         opt_input_ock.init_compactB = zeros(4, 10^5, 'uint8');
%         x = floor(rand(4, 10^5) * 256);
%         x = max(0, x);
%         x = min(x, 255);
%         opt_input_ock.init_compactB = uint8(x);
%     end

    x = tic;
    ock_model = ock_training(Xtraining, opt_input_ock);
%     ock_training_for_test_c_code
    time_training = toc(x);
    save(file_output, ...
        'ock_model', ...
        'opt_input_ock', ...
        'time_training', ...
        'machine_name', ...
        '-v7.3');
    
    if ~exist('energy.mat', 'file')
        training_energy = sum(Xtraining(:) .^ 2);
        save('energy.mat', 'training_energy');
    end
    clear Xtraining;
    
end

function [W, W1, W2, obj] = advITQ(X, m, lambda)

D = size(X, 2);

W = rand(D, m);
[U, ~, ~] = svd(W);
W1 = U(:, 1 : m);

W2 = 0.0001 * randn(D, m);

B = X * (W1 + W2);
B = sign(B);

for iter = 1 : 50
    %% update W1
    [U, ~, V] = svd(X' * (B - X * W2));
    W1 = U(:, 1 : m) * V';
    
    %% update W2
    W2 = solveW2(X, B - X * W1, lambda, W2);
    
    %% update B
    B = X * (W1 + W2);
    B = sign(B);
    
    obj(iter) = norm(X * (W1 + W2) - B, 'fro') .^ 2 + lambda * norm(W2(:), 1);
end

W = W1 + W2;

end

function W = solveW2(X, G, lambda, W)

rate = 0.3;
for iter = 1 : 10
    delta = 2 * (X' * X) * W - 2 * X' * G + lambda * sign(W);
    
    r = rate * sqrt(norm(W(:))) / sqrt(norm(delta(:)));
    
    obj1 = norm(X * W - G, 'fro') .^ 2 + lambda * norm(W(:), 1);
    is_decreased = false;
    for t = 1 : 10
        W1 = W - r * delta;
        obj2 = norm(X * W1 - G, 'fro') .^ 2 + lambda * norm(W1(:), 1);
        if (obj2 < obj1)
            is_decreased = true;
            break;
        else
            r = r / 2;
        end
    end
    W = W1;
    if ~is_decreased
        break;
    end
end
end
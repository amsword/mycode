clc;
[W, W1, W2, obj] = advITQ(Xtraining', m, 1);
norm(W2(:)) / norm(W1(:))
%%
W = [W; zeros(1, m)];
%
all_eval = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    curr_eval = eval_hash5(size(W, 2), 'linear', W, ...
        Xtest, Xtraining, ...
        StestBase2', all_topks(k), gnd_file.TestBaseSeed, [], metric_info, eval_types);
    all_eval{k} = curr_eval;
end

% file_pre_date = '2012_5_20_8_20_5_';
% [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
%   
% load(save_file.test_itq);

for k = 1 : numel(all_topks)
    figure;
   
    plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'b-*'); hold on;
%     plot(eval_itq{k}.r_code, 'b-'); hold on;
    
   
        plot(all_eval{k}.avg_retrieved, all_eval{k}.rec, 'r');
%         plot(all_eval{k}.r_code, 'r');
  
    
    axis([0 1000 0 1]);
    grid on;

    xlabel('Number of retrieved points', 'FontSize', 14);
    ylabel('Recall', 'FontSize', 14);
    set(gca, 'FontSize', 14);
    saveas(gca, [num2str(all_topks(k)) '-NN.eps'], 'psc2');
%     close;
end
% save('C:\Users\v-jianfw\Desktop\labelme32\oph.mat', 'best_result2');
pause;
close all;
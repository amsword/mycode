function [y pc center] = do_pca(data, num)
%% data: every column is an observation
%% num: the number of principle component
%% y: the coordinate in principal component space
%% pc: the dominant vector, the number of column is 'num'
%% here, mean(data) must be zeros

center = mean(data, 2);
data = bsxfun(@minus, data, center);

opts.disp = 0;
[pc, tmp1] = eigs(cov(data'), num, 'LM', opts);
y = pc' * data;
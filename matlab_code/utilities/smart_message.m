function last_time = smart_message(last_time, message)

min_time_pass = 2;

now_time = clock;
if etime(now_time, last_time) > min_time_pass
    fprintf('%s\n', message);
    last_time = now_time;
end


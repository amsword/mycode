function y = batch_map(idx_begin, idx_end, max_size, ...
    func_run)

assert(idx_end >= idx_begin);

num_task = idx_end - idx_begin + 1;
num_batch = floor(num_task / max_size);
if mod(num_task, max_size) ~= 0
    num_batch = num_batch + 1;
end

idx1 = idx_begin;
y = cell(num_batch, 1);

last_time = smart_meassge_init();
for i = 1 : num_batch
    idx2 = idx1 + max_size - 1;
    if idx2 > idx_end
        idx2 = idx_end;
    end
    y{i} = func_run(idx1, idx2);
    idx1 = idx2 + 1;
    last_time = smart_message(last_time, num2str(i));
end
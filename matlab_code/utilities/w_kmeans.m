function [centers] = w_kmeans(Xtraining, ...
    num_cluster, ...
    para)

max_iter = 100;
min_error = 10^-5;
init_centers = [];
if exist('para', 'var');
    if isfield(para, 'max_iter')
        max_iter = para.max_iter;
    end
    if isfield(para, 'min_error')
        min_error = para.min_error;
    end
    if isfield(para, 'init_centers')
         init_centers = para.init_centers;
    end
end

if isempty(init_centers)
    rp = randperm(size(Xtraining, 2));
    rp = rp(1 : num_cluster);
    
    init_centers = Xtraining(:, rp);
end


[centers] = mexKMeans(Xtraining, ...
    num_cluster, ...
    init_centers, ...
    max_iter, ...
    min_error);
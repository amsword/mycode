function y = get_free_mem()

if isunix()
    [r,w] = unix('free | grep Mem');
    stats = str2double(regexp(w, '[0-9]*', 'match'));
    memsize = stats(1)/1e6;
    y = (stats(3)+stats(end))/1e6;
else
    y = 100;
end
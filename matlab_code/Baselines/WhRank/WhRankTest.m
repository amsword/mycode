function adapt_weight = WhRankTest(unbinarized, weight_para)

unbinarized = -abs(unbinarized);
code_length = size(weight_para, 2);

[code_length1, num_point] = size(unbinarized);
assert(code_length == code_length1);

adapt_weight = zeros(code_length, num_point);


for i = 1 : code_length
    mu = weight_para(1, i);
    sigma = weight_para(2, i);
    
    prob = normcdf(unbinarized(i, :), mu, sigma);
    
    adapt_weight(i, :) = -log((1 - prob) ./ prob);
end
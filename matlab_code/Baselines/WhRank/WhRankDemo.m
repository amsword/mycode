

x = load(file_name_binary_code, 'W');
W = x.W;

X = Xtraining;
unbinarized = bsxfun(@plus, W(1 : end - 1, :)' * X, W(end, :)');

%%
[~, knn_graph] = read_gnd(str_gnd_train_train, 2000, 'double');
knn_graph = double(knn_graph) + 1;
%
weight_para = WhRankTrain(unbinarized, knn_graph(1 : 1000, :));

%
X = Xtest;
query_unbinarized = bsxfun(@plus, W(1 : end - 1, :)' * X, W(end, :)');

%%
clear all_dist;
k = 1;
all_dist{k} = {5, weight_para};

clear all_cri;
k = 1;
all_cri{k} = {0, int32(topks), gnd}; k = k + 1; 

clear weighted_lei;
for i = 1 : numel(all_dist)
    tic;
    i
    weighted_lei{i} = MmexTestTrueNeighbors(...
        query_unbinarized, ...
        base_binary_code, ...
        num_candidate, ...
        Xtest, ...
        Xbase, ...
        all_dist{i}, ...
        all_cri);
    toc;
end



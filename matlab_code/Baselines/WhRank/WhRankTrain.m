function weight_para= WhRankTrain(unbinarized, knn_graph)

[code_length, num_point] = size(unbinarized);
[num_nn, num_point1] = size(knn_graph);

assert(num_point == num_point1);

weight_para = zeros(2, code_length);

for i = 1 : code_length
    % mean and std
    one_unbinarized = unbinarized(i, :);
    [mu, sigma] = MeanStd();
    weight_para(1, i) = mu;
    weight_para(2, i) = sigma;
end

    function [mu, sigma] = MeanStd()
        aux = (1 : num_point);
        idx_left = ones(num_nn, 1) * aux;
        left = one_unbinarized(idx_left);
        right = one_unbinarized(knn_graph);
        diff_s = left - right;
        mu = mean(diff_s(:));
        sigma = std(diff_s(:));
    end
end
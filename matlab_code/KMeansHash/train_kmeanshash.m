function [W Wpca] = train_kmeanshash(data, m)

[pca_data, pc] = jf_do_pca(data, m);
d = size(data, 1);
W = zeros(d + 1, m);
Wpca = zeros(d + 1, m);
for i = 1 : m
    th = median(pca_data(i, :));
    
    Wpca(1 : d, i) = pc(:, i);
    Wpca(d + 1, i) = -th;
    
    idx_left = pca_data(i, :) < th;
    idx_right = ~idx_left;
    center_left = mean(data(:, idx_left), 2);
    center_right = mean(data(:, idx_right), 2);
    
    pre_idx_left = idx_left;
    
    i
    while(1)
        gen_middle_line();
        idx_left = a' * data + b < 0;
        idx_right = ~idx_left;
        changed = sum(idx_left~= pre_idx_left);
        if (changed == 0)
            break;
        end
        
        pre_idx_left = idx_left;
        center_left = mean(data(:, idx_left), 2);
        center_right = mean(data(:, idx_right), 2);
    end
    
    W(1 : d, i) = a;
    W(d + 1, i) = b;
end

function gen_middle_line()
a = center_right - center_left;
middle = (center_right + center_left) / 2;
b = -a' * middle;
end
end

function [W]= train_lsh2(Xtraining, code_length)

[dim, num_point] = size(Xtraining);
mean_value = mean(Xtraining, 2);

W1T = randn(code_length, dim);
W2T = -W1T * mean_value;

W = [W1T'; W2T'];


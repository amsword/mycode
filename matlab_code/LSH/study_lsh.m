file_pre_date = '2012_5_20_8_20_5_';
[save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);

load(save_file.train_lsh);
%%
X = [Xtraining; ones(1, size(Xtraining, 2))];
%%
W = W_mlh';
W = W_itq;
B = W' * X > 0;
%%
Straintrain = load_gnd2('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\data\c_data\SortedDistance.double.bin', 400);

%%
all_idx = 1 : size(Xtraining, 2);

for k = 1 : size(W_lsh, 2)
    good = 0;
    bad = 0;
    for i = 1 : size(B, 2)
        b = B(k, i);
        
%         b_not_nn = B(k, setdiff(all_idx, Straintrain(:, i)));
        b_nn = B(k, Straintrain(:, i));
        good = good + sum(b_nn == b);
%         bad = bad + sum(b == b_not_nn);
    end
    prob(k) = good / numel(Straintrain);
%     prob_bad(k) = bad / (Ntraining^2 - numel(Straintrain));
end
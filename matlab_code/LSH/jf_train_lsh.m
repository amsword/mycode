function [W_lsh para_lsh]= jf_train_lsh(Xtraining, nb)

LSHparam.nbits = nb;
LSHparam.L = 1;
[Ndim, Ntrain] = size(Xtraining);

fprintf('start LSH training\n');
Wlsh = lshfunc(LSHparam.L, LSHparam.nbits, Ndim);
W_lsh = [Wlsh.A; Wlsh.b];

para_lsh = LSHparam;
function jf_prepare_labelme(save_file, is_random)
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

if (~is_random)
    load('data\LabelMe_gist.mat', ...
        'gist', 'ndxtrain', 'ndxtest');
else
    load('F:\v-jianfw\Data_HashCode\Labelme\data\LabelMe_gist.mat',...
        'gist');
    N = size(gist, 1);
    rp = randperm(N);
    ndxtest = rp(1 : 2000);
    ndxtrain = rp(2001 : N);
    save('F:\v-jianfw\Data_HashCode2\Labelme\data\LabelMe_gist2.mat',...
        'gist', 'ndxtrain', 'ndxtest');
end
% load('data\LabelMe_gist.mat', ...
%     'gist');
% load('labelme1002');


Xtraining = gist(ndxtrain, :);
Xtraining = Xtraining';

Xtest = gist(ndxtest, :);
Xtest = Xtest';

Xbase = Xtraining;

jf_preprocessing(Xtraining, Xbase, Xtest, save_file);
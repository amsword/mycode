%% Main program to run test for LabelMe dataset, 20000 for training and 2000 for testing
%  evaluation script is 'eval_labelme_src.m'
%%

% create data structure
% 200 is the number of average neighbors of testing data
data = create_labelme('euc-22K-labelme', 400,[]);
perform_pca = 1			% whether to perform PCA dimensionality reduction
if (perform_pca)
  data2 = do_pca(data, 40); % 40D PCA
else
  data2 = data;
end
%%
nbs = [10]; % code length
rhos = [2]; % Hamming radius

nb = nbs;
%%
% save result/source_data data data2
load result/source_data;

%% SIG
clear train_params model_params;

model_params.weight = .65; % weight of non-neighbor pairs, 0.65 for 10bits, 0.4 for 20bits
model_params.nb = nb; % code length
model_params.rho = rhos(nbs == nb); % Hamming radius correspondent to graded neighbors
model_params.thresZ = log(1000)/nb * 2; % sigmoid parameter for classification error
model_params.thresCode = log(1000)*2; % sigmoid parameter threshold for code
model_params.shrink_w = 1e-5; % the weight of regularization of W
if (nb == 10)
    model_params.rhos = [0,1,2]'; %[0,1,2] for 10, [3,5,6] for 20, [7,9,11] for 30
elseif (nb == 20)
    model_params.rhos = [3, 5, 6]';
elseif (nb == 30)
    model_params.rhos = [7, 9, 11]';
else
    error('nb: not valide');
end
train_params.nMinibatch = 100; % mini-batch size
train_params.niters = 300; % number of sampling iterations
train_params.lambda = 0.5; % sampling proportion of neighbor pairs, 0.5 for 10bits, 0.2 for 20bits
train_params.nSamples = 10^5; % number of pairs sampled each iteration

verbose = 1; % verbose mode
fprintf('start SIG training\n');
Wsig = trainSIG(data2, model_params, train_params, verbose);

save result/sig_labelme Wsig train_params model_params

%% MLH
%==========================================================================
% MLH and BRE implementation downloaded from 
% http://www.cs.toronto.edu/~norouzi/research/mlh/
% excluded the validation process here
%==========================================================================
clear mlh_params
mlh_params.size_batches = 100;
mlh_params.eta = .1;
mlh_params.shrink_w = 1e-4;
mlh_params.lambda = 0.1;
mlh_params.rho = 6;
train_iter = 100;
train_zerobias = 1;

fprintf('start MLH training\n');
Wmlh = MLH(data2, {'hinge', mlh_params.rho, mlh_params.lambda}, nb, ...
		    [mlh_params.eta], .9, [mlh_params.size_batches], 'trainval', train_iter, train_zerobias, ...
		    5, 1, 50, [mlh_params.shrink_w], 1);
Wmlh = Wmlh.W;

save result/mlh_labelme Wmlh mlh_params



%% BRE
%==========================================================================
% BRE needs storage of distance for all pairs
% will need 'create_labelme_bre' function to get distance matrix, if memory
% cannot fit all the data
%==========================================================================
clear best_params
bre_params.size_batches = 100;
bre_params.eta = .1;
bre_params.shrink_w = 1e-4;

fprintf('start BRE training\n');
data_bre = create_labelme_bre('euc-22K-labelme', 200,[]);
data2_bre = do_pca(data_bre, 40);
data2_bre.Strainings = data2.Strainings;

data2.Dtraining = distMat(data.Xtraining);
Wbre = MLH(data2_bre, {'bre'}, nb, [bre_params.eta], .9, [bre_params.size_batches], ...
    'trainval', 100, 1, 5, 1, 50, [bre_params.shrink_w], 1);
Wbre = Wbre.W;

save result/bre_labelme Wbre bre_params

%% USPLH
USPLHparam.nbits = nb;
USPLHparam.eta=.8;
USPLHparam.lambda=0.25;
USPLHparam.c_num=100;

fprintf('start USPLH training\n');
USPLHparam = trainUSPLH(data.Xtraining', USPLHparam);
Wusplh = [USPLHparam.w; USPLHparam.b]';

save result/usplh_labelme USPLHparam Wusplh

%% SH
SHparam.nbits = nb;

fprintf('start SH training\n');
SHparam = trainSH(data.Xtraining', SHparam);

save result/sh_labelme SHparam

%% LSH
LSHparam.nbits = nb;
LSHparam.L = 1;
[Ndim, Ntrain] = size(data.Xtraining);

fprintf('start LSH training\n');
Wlsh = lshfunc(LSHparam.L, LSHparam.nbits, Ndim);
Wlsh = [Wlsh.A; Wlsh.b]';
save result/lsh_labelme Wlsh LSHparam

%% CH
CHparam.nbits = nb;
CHparam.doMask = 0;
CHparam.eta = .1;
CHparam.L = 3;
CHparam.c_num = 100;
CHparam.epsilon = .005;
CH = trainCH_ext(data.Xtraining', CHparam);
save result/ch_labelme CH

%% SKLSH
% performance of SKLSH is usually bad for short codes, not reported
% RFparam.gamma = 1;
% RFparam.D = size(data.Xtraining, 1); % dim of data
% RFparam.M = nb;
% RFparam = RF_train(RFparam);
% save result/sklsh_labelme RFparam

%% SIG_DIM
% clear train_params model_params;
% 
% model_params.weight = .6; % 0.8 better?
% model_params.nb = nb;
% model_params.rho = rhos(nbs == nb);
% model_params.thresZ = log(1000)/nb * 2;
% model_params.thresCode = log(1000);
% model_params.shrink_w = 1e-5;
% 
% train_params.nMinibatch = 100;
% train_params.niters = 140;
% train_params.lambda = 0.0;
% train_params.nSamples = 10^5;
% 
% verbose = 1;
% fprintf('start SIG training\n');
% for i = rhos(nbs == nb):rhos(nbs == nb)
%     Wsig_dim{i+1} = trainSIG_single(data2, model_params, train_params, verbose, i);
% end
% 
% for i = rhos(nbs == nb):rhos(nbs == nb)
%     sig_eval_dim{i+1} = eval_hash(Wsig_dim{i+1}, data2, 'w_based', [])
% end
% 
% n = [100, 128, 256, 512, 1024, 2048];
% plot1 = plot(n, sig_eval_dim{1}.p_code(n), n, sig_eval_dim{2}.p_code(n), n, sig_eval_dim{3}.p_code(n));

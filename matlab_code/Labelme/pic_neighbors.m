%% Visualize the searching results
% first run run_labelme.m to get hash functions

nb = 10; % number of bits
NUM_PLOT_NEIGH = 15; % number of images retrieved

W = Wsig;
[B1_SIG, B2_SIG] = getCodes(data, W, 'w_based');
D_code_sig = hammingDist(B2_SIG, B1_SIG);

W = Wmlh;
[B1_MLH, B2_MLH] = getCodes(data, W, 'w_based');
D_code_mlh = hammingDist(B2_MLH, B1_MLH);

W = Wbre;
[B1_BRE, B2_BRE] = getCodes(data, W, 'w_based');
D_code_bre = hammingDist(B2_BRE, B1_BRE);

% SH get codes
Xtraining = data.Xtraining';
Xtest = data.Xtest';
SHparam.nbits = nb; % number of bits to code each sample

% training
SHparam = trainSH(Xtraining, SHparam);

[B1_SH,U1] = compressSH(Xtraining, SHparam);
[B2_SH,U2] = compressSH(Xtest, SHparam);
B2_SH = B2_SH'; B1_SH = B1_SH';
D_code_sh = hammingDist(B2_SH, B1_SH);

W = Wlsh;
[B1_LSH, B2_LSH] = getCodes(data, W, 'w_based');
D_code_lsh = hammingDist(B2_LSH, B1_LSH);

W = Wusplh;
[B1_USPLH, B2_USPLH] = getCodes(data, W, 'w_based');
D_code_usplh = hammingDist(B2_USPLH, B1_USPLH);
   
img2 = double(reshape(img,[3072 size(img,4)]));
gist = gist';

clear j_code_sig j_code_mlh j_truth j_gist

code_sig = zeros(length(query), length(ndxtrain));
code_mlh = zeros(length(query), length(ndxtrain));
code_truth = zeros(length(query), length(ndxtrain));
code_gist = zeros(length(query), length(ndxtrain));
code_bre = zeros(length(query), length(ndxtrain));
code_sh = zeros(length(query), length(ndxtrain));
code_lsh = zeros(length(query), length(ndxtrain));
code_usplh = zeros(length(query), length(ndxtrain));
j = 0;

num_methods = 7;
mar = 0.01;
l_space = mar;
image_size = (1 - 2*l_space - (num_methods - 1)*mar) / num_methods

for n = 1:length(ndxtest)
    ndx = ndxtest(n);
    if ~ismember(ndx, query2)
        continue;
    end
    j = j + 1; % count of query imgs
    % compute your distance using SIG
    [foo, j_code_sig] = sort(D_code_sig(n, :), 'ascend'); % I assume that small distance means closer
    j_code_sig = ndxtrain(j_code_sig);
    
    % compute distance using MLH
    [foo, j_code_mlh] = sort(D_code_mlh(n, :), 'ascend'); % I assume that small distance means closer
    j_code_mlh = ndxtrain(j_code_mlh);
    
    % compute distance using SH
    [foo, j_code_sh] = sort(D_code_sh(n, :), 'ascend'); % I assume that small distance means closer
    j_code_sh = ndxtrain(j_code_sh);
    
    % compute distance using BRE
    [foo, j_code_bre] = sort(D_code_bre(n, :), 'ascend'); % I assume that small distance means closer
    j_code_bre = ndxtrain(j_code_bre);
    
    % compute distance using LSH
    [foo, j_code_lsh] = sort(D_code_lsh(n, :), 'ascend'); % I assume that small distance means closer
    j_code_lsh = ndxtrain(j_code_lsh);
    
    % compute distance using LSH
    [foo, j_code_usplh] = sort(D_code_usplh(n, :), 'ascend'); % I assume that small distance means closer
    j_code_usplh = ndxtrain(j_code_usplh);
    
    % get groundtruth neighbors
    D_truth = data.DtestTraining(n,:);
    [foo, j_truth] = sort(D_truth); 
    j_truth = ndxtrain(j_truth);

    % get neighbors using gist
    D_gist = dist_mat(gist(:,ndx)',gist(:,ndxtrain)');
    [foo, j_gist] = sort(D_gist); 
    j_gist = ndxtrain(j_gist);
    
%     % get neighbors using L2 distance on pixel intensities
%     D_pixel = dist_mat(img2(:,ndx)',img2(:,ndxtrain)');
%     [foo, j_pixel] = sort(D_pixel); 
%     j_pixel = ndxtrain(j_pixel);
   
%     figure;
%     subplot(2,3,1);
%     query_img = img(:,:,:,ndx);
%     imagesc(query_img);
%     image_title = sprintf('Query image %d', ndx);
%     title(image_title);
%     
%     subplot(2,3,3);
%     montage(img(:,:,:,j_code_sig(1:NUM_PLOT_NEIGH)));
%     title('SIG');
%     
%     subplot(2,3,2);
%     montage(img(:,:,:,j_truth(1:NUM_PLOT_NEIGH)));
%     title('Ground truth');
% 
%     subplot(2,3,5);
%     montage(img(:,:,:,j_gist(1:NUM_PLOT_NEIGH)));
%     title('Gist (512 dim)');
% 
%     subplot(2,3,6);
%     montage(img(:,:,:,j_code_mlh(1:NUM_PLOT_NEIGH)));
%     title('MLH');

%% new way to arrange images
    figure;
    query_img = img(:,:,:,ndx);
    set(gcf, 'position', [100, 100, 1100, 200]);
    h = subplot(1,7,1);
    set(h,'position',[mar    0.1    image_size    0.8]); 
    montage(cat(4, query_img, img(:,:,:,j_truth(1:NUM_PLOT_NEIGH))));
    image_title = sprintf('Groundtruth %d', ndx);
%     title(image_title);
    
    h = subplot(1,7,2);
    set(h,'position',[2*mar+image_size    0.1    image_size    0.8]); 
    montage(cat(4, query_img, img(:,:,:,j_code_sig(1:NUM_PLOT_NEIGH))));
%     title('SIG');
      
    h = subplot(1,7,3);
    set(h,'position',[3*mar+2*image_size    0.1    image_size    0.8]); 
    montage(cat(4, query_img, img(:,:,:,j_code_mlh(1:NUM_PLOT_NEIGH))));
%     title('MLH');
    
    h = subplot(1,7,4);
    set(h,'position',[4*mar+3*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_sh(1:NUM_PLOT_NEIGH))));
%     title('SH');
    
    h = subplot(1,7,5);
    set(h,'position',[5*mar+4*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_bre(1:NUM_PLOT_NEIGH))));
%     title('BRE');
    
    h = subplot(1,7,6);
    set(h,'position',[6*mar+5*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_lsh(1:NUM_PLOT_NEIGH))));
%     title('LSH');
    
    h = subplot(1,7,7);
    set(h,'position',[7*mar+6*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_usplh(1:NUM_PLOT_NEIGH))));
%     title('USPLH');
    
    h = subplot(1,7,5);
    set(h,'position',[5*mar+4*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_bre(1:NUM_PLOT_NEIGH))));
%     title('BRE');
    
    h = subplot(1,7,6);
    set(h,'position',[6*mar+5*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_lsh(1:NUM_PLOT_NEIGH))));
%     title('LSH');
    
    h = subplot(1,7,5);
    set(h,'position',[5*mar+4*image_size    0.1    image_size    0.8]);
    montage(cat(4, query_img, img(:,:,:,j_code_bre(1:NUM_PLOT_NEIGH))));
%     title('BRE');
    
    code_sig(j, :) = j_code_sig;
    code_mlh(j, :) = j_code_mlh;
    code_truth(j, :) = j_truth;
    code_gist(j, :) = j_gist;
    
    drawnow
    filename = sprintf('./imgs/img_%d',ndx);
    saveas(gcf, filename, 'png');
    saveas(gcf, filename, 'eps');
    if mod(j, 50) == 0
        pause;
    end
end




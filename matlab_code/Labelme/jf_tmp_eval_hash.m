function eval = jf_tmp_eval_hash(W, data, hash_mode, param)
%% evaluation program for linear and spectral hashing

if isfield(data, 'Xbase') % Xbase is for large dataset, in case the distance matrix cannot fit into memory
    ndxtrain = 1:data.Nbase;
else
    ndxtrain = data.ndxtrain;
end
ndxtest = data.ndxtest;
batchsize = 500; % evaluation the testing cases in batches
nbatch = length(ndxtest) / batchsize;
% nbatch = 1;

if (strcmp(hash_mode, 'w_based'))
    if isfield(data, 'Xbase')
        B1 = ((W * [data.Xbase; ones(1, data.Nbase)])>0);
    else
        B1 = ((W * [data.Xtraining; ones(1, data.Ntraining)]) > 0);
    end
    nb = size(W, 1);
    
    [Nbuckets, bucketACap] = getBuckets(B1, nb)
    B1 = compactbit(B1); B1 = uint8(B1);

    
elseif (strcmp(hash_mode, 'sh'))
    Xtraining = data.Xtraining';
    SHparam = param;
    nb = SHparam.nbits;
    
    if isfield(data, 'Xbase')
        [B1, U1] = compressSH(data.Xbase', SHparam);
    else
        [B1,U1] = compressSH(Xtraining, SHparam);
    end
    
elseif (strcmp(hash_mode, 'sklsh'))
    if isfield(data, 'Xbase')
        [B1, U] = RF_compress(data.Xbase', param);
    else
        [B1, U] = RF_compress(data.Xtraining', param);
    end
end

P_CODE = zeros(batchsize, length(ndxtrain));
R_CODE = zeros(size(P_CODE));
PRECRHO = zeros(batchsize, 3);
NRHO = zeros(size(PRECRHO));
P1 = zeros(1, nb+1);
R1 = zeros(size(P1));
AP = 0;

for i = 1:nbatch
    % get the start/end index of the batch testing data
    b_start = 1+(i-1)*batchsize; b_end = i*batchsize;
    if (strcmp(hash_mode, 'w_based'))
        B2 = ((W * [data.Xtest(:, b_start:b_end); ones(1, batchsize)]) > 0);
        nb = size(W, 1);
        
        B2 = compactbit(B2); B2 = uint8(B2);
        % compute Hamming distance values
        D_code = hammingDist(B2, B1);
        
    elseif (strcmp(hash_mode, 'sh'))
        Xtest = data.Xtest(:, b_start:b_end)';
        SHparam = param;
        
        [B2,U2] = compressSH(Xtest, SHparam);
        % example query
        D_code = hammingDist(B2', B1');
        
    elseif (strcmp(hash_mode, 'sklsh'))
        [B2, U] = RF_compress(data.Xtest(:, b_start:b_end)', param);
        D_code = hammingDist(B2, B1);
    end
    
    [p1 r1 rpairs] = evaluation2(data.StestTraining(b_start:b_end, :), D_code, nb);
    p1 = p1';
    P1 = P1+p1;R1 = R1+r1;
    % compute the average precision from hash lookup
    AP = AP + sum([(p1(1:end-1)+p1(2:end))/2].*[(r1(2:end)-r1(1:end-1))]);

    evalParam.rho = 2; % rho_max = 2;
    P_code = zeros(batchsize, length(ndxtrain));
    R_code = zeros(size(P_code));
    PrecRho = zeros(batchsize, evalParam.rho+1);
    NRho = zeros(size(PrecRho));
%     for n = 1:batchsize
%         ind = n-1+b_start;
%         [foo_code, j_code] = sort(D_code(n, :), 'ascend');
%         j_code = ndxtrain(j_code);
%         % the rank of learnt hash codes
%         
%         D_truth = data.DtestTraining(ind, :);
%         [foo_truth, j_truth] = sort(D_truth);
%         j_truth = ndxtrain(j_truth);
%         % the rank of ground-truth Euclidean distance
%         
%         evalParam.foo_code = foo_code;
%         result = eval_small(j_truth', j_code', evalParam);
%         P_code(n, :) = result.p_code; % precision of Hamming ranking
%         R_code(n, :) = result.r_code; % recall of Hamming ranking
%         PrecRho(n, :) = result.prec_at_rho;
%         NRho(n, :) = result.n_points_rho;
%     end
    P_CODE = P_CODE+P_code;
    R_CODE = R_CODE+R_code;
    PRECRHO = PRECRHO+PrecRho;
    NRHO = NRHO+NRho;

    fprintf('batch %d\n', i);
end
eval.p_code = mean(P_CODE, 1)/nbatch;
eval.r_code = mean(R_CODE, 1)/nbatch;
eval.a_code = mean(PRECRHO, 1)/nbatch;
eval.n_code = mean(NRHO, 1)/nbatch;
eval.p1 = P1/nbatch;
eval.r1 = R1/nbatch;
eval.AP = AP/nbatch;

    
    

function prepare_labelme_origin(save_file, is_random)
%% para.m: code length;
%% para.dk: the graded distance threshold in hamming space;

global gl_data_parent_folder;

if (~is_random)
    load([gl_data_parent_folder 'LabelmeOrigin\data\LabelMe_gist.mat'], ...
        'gist', 'ndxtrain', 'ndxtest');
else
    load('F:\v-jianfw\Data_HashCode\Labelme\data\LabelMe_gist.mat',...
        'gist');
    N = size(gist, 1);
    rp = randperm(N);
    ndxtest = rp(1 : 2000);
    ndxtrain = rp(2001 : N);
    save('F:\v-jianfw\Data_HashCode2\Labelme\data\LabelMe_gist2.mat',...
        'gist', 'ndxtrain', 'ndxtest');
end
% load('data\LabelMe_gist.mat', ...
%     'gist');
% load('labelme1002');

gist = double(gist);
mean_value = mean(gist, 1);
gist = bsxfun(@minus, gist, mean_value);

OriginXtraining = gist(ndxtrain, :);
OriginXtraining = OriginXtraining';

OriginXtest = gist(ndxtest, :);
OriginXtest = OriginXtest';

save(save_file.train, 'OriginXtraining');
save(save_file.test, 'OriginXtest');

save_mat(OriginXtraining, save_file.c_train, 'double');
save_mat(OriginXtest, save_file.c_test, 'double');

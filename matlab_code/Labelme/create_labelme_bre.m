function data = create_labelme_bre(MODE, operand1, operand2, operand3)
data.MODE = MODE;

if (strcmp(MODE, 'euc-22K-labelme'))
  fprintf('Creating %s dataset ... ', MODE);
 
  %%only for the visualization
  load('data/LabelMe_gist', 'gist');
  load('labelme1002', 'ndxtrain', 'ndxtest');
  % data-points are stored in columns
  X = gist';
  clear gist;
  
  % center, then normalize data
  gist_mean = mean(X, 2);
  X = bsxfun(@minus, X, gist_mean);
  normX = sqrt(sum(X.^2, 1));
  X = bsxfun(@rdivide, X, normX);
  
%   ndxtest = ceil(rand(2000, 1) * 22019);
 % ndxtrain = setdiff(1:22019, ndxtest);
 
 % only for BRE

  Xtest = X(:, ndxtest);
  Xtraining = X(:, ndxtrain);
  
  data = construct_data(Xtraining, Xtest, [numel(ndxtrain), numel(ndxtest)], operand1, operand2, data);
  % see bottom for construct_data(...)
  data.gist_mean = gist_mean;
  data.ndxtest = ndxtest;
  data.ndxtrain = ndxtrain;
  
else
  error('The given mode is not recognized.\n');
end

fprintf('done\n');


function data = construct_data(Xtraining, Xtest, sizeSets, avgNNeighbors, proportionNeighbors, data)

% either avgNNeighbors or proportionNeighbors should be set. The other value should be empty ie., []
% avgNNeighbors is a number which determines the average number of neighbors for each data point
% proportionNeighbors is between 0 and 1 which determines the fraction of [similar pairs / total pairs]

[Ntraining, Ntest] = deal(sizeSets(1), sizeSets(2));


Dtraining = single(distMat(Xtraining));

NNeighbors = [20, 200, 900]; %m = 10, rho = 2
nRhos = size(NNeighbors, 2);

tic;
sortedD = sort(Dtraining, 2);
toc;
thresDist = mean(sortedD(:, avgNNeighbors));
data.avgNNeighbors = avgNNeighbors;

clear sortedD


data.Xtraining = Xtraining;
data.Xtest = Xtest;
clear Xtraining;
clear Xtest;
data.Straining = Dtraining < thresDist;
data.Dtraining = Dtraining;
% clear Dtraining;

% tiya: Straining and StestTraining is the neighbor-indicator
data.Ntraining = Ntraining;
data.Ntest = Ntest;

data.avgNNeighbors = avgNNeighbors;
data.threshDist = thresDist;
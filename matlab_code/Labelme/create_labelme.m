function data = create_labelme(MODE, operand1, operand2, operand3)
%% Create the dataset, modified from MLH code, 'create_data.m'
%%

data.MODE = MODE;

if (strcmp(MODE, 'euc-22K-labelme'))
    fprintf('Creating %s dataset ... ', MODE);
    
    load('data/LabelMe_gist', 'gist');
    
    % better stored the index of training and test pairs
    % load('labelme1002', 'ndxtrain', 'ndxtest');
    
    % data-points are stored in columns
    X = gist';
    clear gist;
    [D, N] = size(X);
    
    % center, then normalize data
    gist_mean = mean(X, 2);
    X = bsxfun(@minus, X, gist_mean);
    normX = sqrt(sum(X.^2, 1));
    X = bsxfun(@rdivide, X, normX);
    
    % randomly select training and testing pairs
    % duplicate random number maybe exist!!!
%     ndxtest = ceil(rand(2000, 1) * 22019);
%     ndxtrain = setdiff(1:22019, ndxtest);

%     rperm = randperm(N);
%     ndxtest = rperm([1 : 2000])';
%     ndxtrain = rperm([2001 : N])';
    

    load('data/LabelMe_gist', 'ndxtrain', 'ndxtest');
    save('labelme1002', 'ndxtest', 'ndxtrain');
    Xtest = X(:, ndxtest);
    Xtraining = X(:, ndxtrain);
    
    % the neighbors information of the data
    data = construct_data(Xtraining, Xtest, [numel(ndxtrain), numel(ndxtest)], operand1, operand2, data);
    
    % see bottom for construct_data(...)
    data.gist_mean = gist_mean;
    data.ndxtest = ndxtest;
    data.ndxtrain = ndxtrain;
    
else
    error('The given mode is not recognized.\n');
end

fprintf('done\n');


function data = construct_data(Xtraining, Xtest, sizeSets, avgNNeighbors, proportionNeighbors, data)

% either avgNNeighbors or proportionNeighbors should be set. The other value should be empty ie., []
% avgNNeighbors is a number which determines the average number of neighbors for each data point
% proportionNeighbors is between 0 and 1 which determines the fraction of [similar pairs / total pairs]

[Ntraining, Ntest] = deal(sizeSets(1), sizeSets(2));
if size(sizeSets, 2) > 2
    Nbase = sizeSets(3); %the size of base dataset
end

Dtraining = single(distMat(Xtraining));

% define the graded neighbors
% NNeighbors = [20, 200, 900]; %m = 10, rho = 2
NNeighbors = [30, 300, 900]; %m = 10, rho = 2

nRhos = size(NNeighbors, 2);

tic;
sortedD = sort(Dtraining, 2);
toc;
thresDist = mean(sortedD(:, avgNNeighbors)); % the threshold distance for MLH and BRE
data.avgNNeighbors = avgNNeighbors;

thresDists = zeros(Ntraining, nRhos);
for i = 1:nRhos
    thresDists(:, i) = sortedD(:, NNeighbors(i)); % the threshold distance for graded neighbors
end
clear sortedD

for i = 1:nRhos
    tic;
    data.Strainings(i, :, :) = bsxfun(@lt, Dtraining, thresDists(:, i));
    % get the training ground truth for graded neighbors, 1 for neighbors and 0 for
    % non-neighbors
    toc;
end

data.Xtraining = Xtraining;
data.Xtest = Xtest;
clear Xtraining;
clear Xtest;
data.Straining = Dtraining < thresDist;
% data.Dtraining = Dtraining;
clear Dtraining;

% Straining and StestTraining is the neighbor-indicator
data.Ntraining = Ntraining;
data.Ntest = Ntest;

if isfield(data, 'Xbase')
    data.DtestTraining = distMat(data.Xtest, data.Xbase);
else
    data.DtestTraining = distMat(data.Xtest, data.Xtraining); % size = [Ntest x Ntraining]
end

data.avgNNeighbors = avgNNeighbors;
sortedD = sort(data.DtestTraining, 2);
threshDist = (sortedD(:,avgNNeighbors));
clear sortedD
data.StestTraining = bsxfun(@lt, data.DtestTraining, threshDist);
data.threshDist = threshDist;
data.thresDists = mean(thresDists, 1);
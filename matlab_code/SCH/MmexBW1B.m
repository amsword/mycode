function left = MmexBW1B(subsubComB, selected_label, dic_size)

left = mexBW1B(int16(subsubComB), ...
    int32(selected_label), int32(dic_size));
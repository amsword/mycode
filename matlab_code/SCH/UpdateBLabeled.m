function [all_comB2] = UpdateBLabeled(...
    Z, selected_idx, selected_label, lambda, all_comB, all_D)

dic_dim = size(all_D{1}, 1);

num_point = size(Z, 2);

all_comB2 = zeros(numel(all_D), num_point, 'int16');

e_diag = GenerateDiagE(selected_label);
denom = e_diag * lambda + 1;
denom = denom';
num_category = max(selected_label) + 1;
assert(min(selected_label) > - 1);
for i = 1 : numel(all_D)
    subD = all_D{i};

    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    selected_b = all_comB(i, selected_idx);
    right_numerator = sum(subD(:, selected_b), 2);
    left_numberator = MmexLeftNumberator(...
        subD, selected_b, selected_label, num_category);
    numerator = bsxfun(@minus, left_numberator, right_numerator);
    numerator2 = numerator(:, selected_label + 1);
    tmp = subZ(:, selected_idx) + lambda * numerator2;
    subZ2(:, selected_idx) =  bsxfun(@rdivide, tmp, denom);
    
    %%
%     G = subD(:, selected_b);
%     num_labels = numel(selected_label);
%     W = zeros(num_labels, num_labels, 'int32');
%     is_eq = bsxfun(@eq, selected_label, selected_label');
%     W(:) = -1;
%     W(is_eq) = 1;
%     for i = 1 : numel(selected_idx)
%         idx_point = selected_idx(i);
%         t = bsxfun(@times, G, double(W(i, :)));
%         t = sum(t, 2);
%         t = t * lambda;
%         t = t + subZ(:, idx_point);
%         t = t / (1 + lambda * sum(W(i, :)));
%         norm(t - subZ2(:, idx_point)) / norm(t)
%         assert(norm(t - subZ2(:, idx_point)) / norm(t) < 10^-5);
%     end
    %
    all_comB2(i, :) = mexMatchingPersuit(subZ2, subD, -1, 0) + 1;
      
end
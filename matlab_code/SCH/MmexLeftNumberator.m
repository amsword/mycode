function left_numerator = MmexLeftNumberator(...
        subD, selected_b, selected_label, num_category)

    assert(isa(subD, 'double'));
    assert(isa(selected_b, 'int16'));
    assert(isa(selected_label, 'int32'));
    
    left_numerator  = 2 * mexLeftNumberator(subD, ...
        selected_b, selected_label, num_category);
end
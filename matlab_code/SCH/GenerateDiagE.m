function [e_diag, v_diag] = GenerateDiagE(selected_label)

left = min(selected_label);
right = max(selected_label);

num_each_category = histc(selected_label, left : right);
num_labeled = numel(selected_label);

v_diag = -num_labeled + 2 * num_each_category;

e_diag = v_diag((selected_label - left) + 1);

end

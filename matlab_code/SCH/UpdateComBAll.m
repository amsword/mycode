function all_comB = UpdateComBAll(Z, all_D)

dic_num = numel(all_D);

all_comB = cell(dic_num, 1);

dim_start = 1;
for i = 1 : dic_num
    subD = all_D{i};
    dic_dim = size(subD, 1);
    
    dim_end = dim_start + dic_dim - 1;

    all_comB{i} = mexMatchingPersuit(Z(dim_start : dim_end, :), ...
        subD, -1, 0) + 1;

    dim_start = dim_start + dic_dim;
end

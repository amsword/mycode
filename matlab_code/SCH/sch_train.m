function [model] = sch_train(Xtraining, ...
    selected_idx, selected_label, para)

dic_num = para.dic_num;
dic_size = para.dic_size;
max_iter = para.max_iter;
lambda = para.lambda;

if lambda > 0 && numel(selected_label) ~= 0
    lambda = lambda * size(Xtraining, 2) ...
        / numel(selected_label) / numel(selected_label);
end

[dim] = size(Xtraining, 1);

R = eye(dim, dim);

dic_dim = dim / dic_num;

all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size);

Z = R' * Xtraining;

[all_comB] = UpdateBByNearestNeighbor(Z, all_D);

pre_obj = obj_loss(Z, selected_idx, selected_label, lambda, ...
    all_D, all_comB);

all_objs = zeros(1, max_iter);
reflect = zeros(1, max_iter);

for i = 1 : max_iter
    R = UpdateRotationMatrix(Xtraining, all_D, all_comB);
    
    Z = R' * Xtraining;
    
    all_D2 = FindCandidateDictionary(Z, ...
        selected_idx, selected_label, lambda, ...
        all_comB, all_D);
    
    all_comB2 = UpdateBByNearestNeighbor(Z, all_D2);
    
    curr_obj = obj_loss(Z, selected_idx, selected_label, lambda, ...
        all_D2, all_comB2);
    
    for j = 1 : 10
        if(curr_obj > pre_obj)
            all_D2 = Linear_Combine(all_D2, all_D, 0.5);
            
            all_comB2 = UpdateBByNearestNeighbor(Z, all_D2);
            
            curr_obj = obj_loss(Z, selected_idx, selected_label, lambda, ...
                all_D2, all_comB2);
        else
            pre_obj = curr_obj;
            all_D = all_D2;
            all_comB = all_comB2;
            all_objs(i) = pre_obj;
            break;
        end
    end
    reflect(i) = j - 1;
    if j == 10
        break;
    end
end

model.R = R;
model.all_D = all_D;
model.reflect = reflect;
model.lambda = lambda;
model.all_objs = all_objs;

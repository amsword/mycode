function R = UpdateRotationMatrix(Xtraining, all_D, all_comB)

W = MultipleAllDAllComB(all_D, all_comB);

[svd_U, svd_Sigma, svd_V] = svd(Xtraining * W');
R = svd_U * svd_V';
%% training

pre_fix = 'ours_';

for lambda = [0.6 0.4 0.8]
    para.dic_num = 4;
    para.dic_size = 256;
    para.max_iter = 100;
    para.lambda = lambda;
    
    [model] = sch_train2(Xtraining, selected_idx, selected_label, para);
    
    file_name = pre_fix;
    code_length = log(para.dic_size) / log(2) * para.dic_num;
    file_name = [file_name  num2str(code_length) num2str(para.lambda) '.mat'];
    
    save(file_name, 'model');
    
    % testing
    x = load(file_name);
    model = x.model;
    R = model.R;
    Z = R' * Xtest;
    all_D = model.all_D;
    
    query_code = UpdateBByNearestNeighbor(Z, all_D);
    
    Z = R' * Xtraining;
    data_base_code = UpdateBByNearestNeighbor(Z, all_D);
     
    pre = MmexPrecision(query_code, ...
        full_test_label, ...
        data_base_code, ...
        full_train_label, ...
        all_D, ...
        num_candidate);
    
    save(file_name, 'model', 'pre');
end
% save(file_name, 'pre', '-append');
%%

%%
curve_colors =  [[255 0 0] / 255; ...
    [148 30 249] / 255; ...
    [220 50 176] / 255; ...
    [20 4 229] / 255; ...
    [3 151 219] / 255; ...
    [1 189 37] / 255; ...
    [80 136 170] / 255; ...
    [200 137 30] / 255; ...
    [100 70 230] / 255; ...
    [0 0 0] / 255];

x = load('ck_32.mat');
% plot(x.model.all_objs, 'k');

% plot(x.model.reflect);
%
plot(x.pre, 'Color', curve_colors(end, :));
hold on;
all_lambda = [1 0.7 0.5 0.3];

k = 1;

% hold on;
for lambda = all_lambda
    file_name = pre_fix;
    code_length = 32;
    file_name = [file_name  num2str(code_length) num2str(lambda) '.mat'];
    
    if exist(file_name, 'file')
        x = load(file_name);
%         plot(x.model.all_objs, 'r'); 
%         norm(x.model.all_D{1}, 'fro')
        semilogx(x.pre, 'Color', curve_colors(k, :));
        k = k + 1;
    end
%     pause
end

legend('ck-means', num2str(all_lambda(1)), num2str(all_lambda(2)), ...
    num2str(all_lambda(3)), num2str(all_lambda(4)));
 

function [model] = sch_train2(Xtraining, ...
    selected_idx, selected_label, para)

dic_num = para.dic_num;
dic_size = para.dic_size;
max_iter = para.max_iter;
lambda = para.lambda;

if lambda > 0 && numel(selected_label) ~= 0
    lambda = lambda * size(Xtraining, 2) ...
        / numel(selected_label) / numel(selected_label);
end

[dim] = size(Xtraining, 1);

R = eye(dim, dim);

dic_dim = dim / dic_num;

all_D = RandomInitDictionary(Xtraining, dic_dim, dic_size);

Z = R' * Xtraining;

[all_comB] = UpdateBByNearestNeighbor(Z, all_D);

all_objs = zeros(1, max_iter);

for i = 1 : max_iter
    R = UpdateRotationMatrix(Xtraining, all_D, all_comB);
    
    Z = R' * Xtraining;
    
    all_D = FindCandidateDictionary(Z, ...
        selected_idx, selected_label, lambda, ...
        all_comB, all_D);
    
    all_comB = UpdateBLabeled(...
        Z, selected_idx, selected_label, lambda, all_comB, all_D);
   
    curr_obj = obj_loss(Z, selected_idx, selected_label, lambda, ...
        all_D, all_comB);
    
    all_objs(i) = curr_obj;
end

model.R = R;
model.all_D = all_D;
model.lambda = lambda;
model.all_objs = all_objs;
model.all_comB = all_comB;

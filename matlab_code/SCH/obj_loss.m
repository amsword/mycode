function obj = obj_loss(Z, ...
        selected_idx, ...
        selected_label, lambda, all_D, all_comB)

dic_num = numel(all_D);

obj = 0;
dim_start = 1;
for i = 1 : dic_num
    subD = all_D{i};
    subcomB = all_comB(i, :);
    dic_dim = size(subD, 1);
    dim_end = dim_start + dic_dim - 1;

    t = obj_loss_each(Z(dim_start : dim_end, :), ...
           selected_idx, ...
            selected_label, ...
            lambda, ...
            subD, ...
            subcomB);
    obj = obj + t;
    dim_start = dim_end + 1;
end


end

function each_obj = obj_loss_each(subZ, ...
    selected_idx, ...
    selected_label, ...
    lambda, ...
    subD, ...
    subcomB)

if isa(subcomB, 'cell')
    error('here');
end

subG = subD(:, subcomB);
obj1 = norm(subZ - subG, 'fro');
obj1 = obj1 * obj1;

subsubG = subG(:, selected_idx);

E_diag = GenerateDiagE(selected_label);

if lambda > 0
    part1 = sum(subsubG .* subsubG, 1);
    part1 = sum(part1' .* E_diag);
    
    part3 = sum(subsubG, 2);
    part3 = sum(part3 .* part3);
    
    part2 = MmexSumEachCategory(subsubG, selected_label);
    
    part = part1 - 2 * part2 + part3;
    
    %%
%     num_labels = numel(selected_label);
%     btmp_W1 = bsxfun(@eq, selected_label, selected_label');
%  
% %     tmp_W1 = tmp_W1 + 1;
%     for i = 1 : num_labels
%         tmp_W1(i, i) = E_diag(i) - tmp_W1(i);
%     end
%     
%     x1 = subsubG * diag(E_diag) * subsubG';
%     x1 = trace(x1);
%     
%     tmp_W1 = zeros(num_labels, num_labels, 'single');
% %     tmp_W1(:) = -1;
%     tmp_W1(btmp_W1) = 2;
%     x2 = subsubG * tmp_W1 * subsubG';
%     x2 = trace(x2);
%     
%     x3 = ones(num_labels, num_labels, 'single');
%     x3 = subsubG * x3 * subsubG';
%     x3 = trace(x3);
%     
% 
%     x = subsubG * tmp_W1 * subsubG';
%     trace(x)
%         left232 = binary * tmp_W1 * binary';
    
    
else
    part = 0;
end

each_obj = lambda * part + obj1;
end
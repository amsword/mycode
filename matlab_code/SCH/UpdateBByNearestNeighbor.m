function [all_comB] = UpdateBByNearestNeighbor(...
    Z, all_D)

dic_dim = size(all_D{1}, 1);

num_point = size(Z, 2);

all_comB = zeros(numel(all_D), num_point, 'int16');

for i = 1 : numel(all_D)
    subD = all_D{i};

    idx_dim_start = (i - 1) * dic_dim + 1;
    idx_dim_end = i * dic_dim;
    
    subZ = Z(idx_dim_start : idx_dim_end, :);
    
    all_comB(i, :) = mexMatchingPersuit(subZ, subD, -1, 0) + 1;
       
%     dist = sqdist(subD, subZ);
%     [~, tmpB] = min(dist, [], 1);
%     find(tmpB~= all_comB{i})
end
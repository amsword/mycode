function [model] = sch(Xtraining, ...
    selected_idx, selected_label, para)


coef_val = 0.1;
num_labeled = numel(selected_idx);
num_point = size(Xtraining, 2);
num_val = floor(num_labeled * coef_val);

rng(0);
rp = randperm(num_labeled);

idx_test2 = selected_idx(rp(1 : num_val));
idx_training2 = setdiff([1 : num_point], idx_test2);

Xtest2 = Xtraining(:, idx_test2);
test_label = selected

Xtraining2 = Xtraining(:, idx_training2);

model2 = sch_train(Xtraining2, ...
    selected_idx2, selected_label2, para2);
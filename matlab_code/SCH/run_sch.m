run('C:\Users\uqjwan34\Desktop\codes2\HashCode\jf_conf.m');
%%
clear;
clc;
%
type = 'MNIST';
num_labeled = 60000;
num_candidate = 10000;
%
exp_dir = get_exp_dir();
working_dir = [exp_dir type '\working_sch'];

cd(working_dir);

data_dir = [exp_dir type ...
    '\data\'];

file_label_train = [data_dir 'Label.Train.int32.bin'];

full_train_label = read_mat(file_label_train, 'int32=>int32');

num_full_train = numel(full_train_label);

rng(0);
selected_idx = randperm(num_full_train, num_labeled);
selected_label = full_train_label(selected_idx);

[s_selected_label, idx_selected_label] = sort(selected_label);
selected_idx = selected_idx(idx_selected_label);
selected_label = s_selected_label;

file_feature_train = [data_dir 'Xtraining.double.bin'];

Xtraining = read_mat(file_feature_train, 'double');

% sch(Xtraining, selected_idx, selected_label);
Xtraining = Xtraining / 255;

% read test data set
file_feature_test = [data_dir 'Xtest.double.bin'];
Xtest = read_mat(file_feature_test, 'double');
Xtest = Xtest / 255;

% read test label
file_label_test = [data_dir 'Label.Test.int32.bin'];
full_test_label = read_mat(file_label_test, 'int32=>int32');

%% ck-means
run_sch_ck;

% ***Linear scan using raw feature;
run_sch_euclidean_distance;
    

%%
save_mat(query_code - 1, 'feat_test', 'int16');
save_mat(data_base_code - 1, 'feat_training', 'int16');
save_mat(full_test_label, 'test_label', 'int32');
save_mat(full_train_label, 'train_label', 'int32');
save_mat(all_D{1}, 'dic', 'double');




function W = MultipleAllDAllComB(all_D, all_comB)

dic_num = size(all_comB, 1);

dim = 0;
for i = 1 : dic_num
    dim = dim + size(all_D{i}, 1);
end

num_point = size(all_comB, 2);

W = zeros(dim, num_point);

dim_start = 1;

for i = 1 : dic_num
    subD = all_D{i};
    subComB = all_comB(i, :);
    
    dic_dim = size(subD, 1);
    
    W(dim_start : dim_start + dic_dim - 1, :) = ...
        subD(:, subComB);
    
    dim_start = dim_start + dic_dim;
end
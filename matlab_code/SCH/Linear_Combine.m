function all_D2 = Linear_Combine(all_D2, all_D, coef)

dic_num = numel(all_D);

for i = 1 : dic_num
    all_D2{i} = all_D2{i} * coef + all_D{i} * (1 - coef);
end

end

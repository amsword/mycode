%% training
para.dic_num = 4;
para.dic_size = 256;
para.max_iter = 100;
para.lambda = 0;

[model] = sch_train(Xtraining, [], [], para);

if para.lambda == 0
    file_name = 'ck_';
else
    file_name = 'ours_';
end

code_length = log(para.dic_size) / log(2) * para.dic_num;
file_name = [file_name  num2str(code_length)];

save(file_name, 'model');

% testing
x = load(file_name);
model = x.model;
R = model.R;
Z = R' * Xtest;
all_D = model.all_D;

query_code = UpdateBByNearestNeighbor(Z, all_D);

Z = R' * Xtraining;
data_base_code = UpdateBByNearestNeighbor(Z, all_D);


pre = MmexPrecision(query_code, ...
    full_test_label, ...
    data_base_code, ...
    full_train_label, ...
    all_D, ...
    num_candidate);

save(file_name, 'pre', '-append');

function subB = comB2subB(subB, dic_size)


error('dfs');
num_point = size(subB, 2);

subB = zeros(dic_size, num_point);

subB(subB + (0 : num_point - 1) * dic_size) = 1;


end
function all_D2 = FindCandidateDictionary(Z, ...
    selected_idx, selected_label, lambda, ...
    all_comB, all_D)

    num_dic = size(all_comB, 1);

    all_D2 = cell(num_dic, 1);

    dim_start = 1;

    for i = 1 : num_dic
        dic_dim = size(all_D{i}, 1);
        dim_end = dim_start + dic_dim - 1;

        dic_size = size(all_D{i}, 2);

        all_D2{i} = FindCandidateDictionaryEach(...
            Z(dim_start : dim_end, :), ...
            selected_idx, selected_label, ...
            lambda, all_comB(i, :), dic_size);

        dim_start = dim_end + 1;
    end
    
end

function subD = FindCandidateDictionaryEach(...
    subZ, ...
    selected_idx, selected_label, ...
    lambda, subComB, dic_size)

    num_vec = histc(subComB, 1 : dic_size);

    e_diag = GenerateDiagE(selected_label);

    subsubComB = subComB(selected_idx);

    left1 = diag(num_vec);
    if lambda > 0
        left2 = MmexBEB(subsubComB, e_diag, dic_size);
        left3 = MmexBW1B(subsubComB, selected_label, dic_size);
        left4 = num_vec' * num_vec;
   
        left = left1 + lambda * (left2 - left3 + left4);
    else
        left = left1;
    end

    right = MmexZBT(subZ, subComB, dic_size);

    dim = size(left, 1);

    subD = right / (left + 10^-5 * eye(dim, dim));
    
end



function right = MmexZBT(subsubZ, subsubComB, dic_size)

right = mexZBT(subsubZ, ...
    int16(subsubComB), int32(dic_size));


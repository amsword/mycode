
for k = 1 : 6
    
    im = zeros(100, 100);
    im(:) = 0;
    sub = 100 / k;
    sub = ceil(sub / 2);
    
    idx_start = 1;
    idx_end = sub;
     im(:, idx_start : idx_end) = 0.8;
    for i = 4 : 4 : 2 * k
        idx_start = 1 + (i - 1) * sub;
        idx_end = (i + 1)  * sub;
        if (idx_end > 100)
            idx_end = 100;
        end
        im(:, idx_start : idx_end) = 0.8;
    end
    
    imshow(im);
    
    saveas(gca, [num2str(k) '.eps'], 'psc2');
%     imwrite(im, [num2str(k) '.bmp']);
end

return;

%%
for i = 1 : size(SHparam.modes, 2)
    x(i) = find(SHparam.modes(i, :), 1);
end
%%

load(save_file.test_sh);
load(save_file.test_lsh);
load(save_file.test_itq);
load(save_file.test_mlh);
load(save_file.train_sh);
%%
for k = 1 : numel(all_topks)
    figure;
    hold on;
    plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'b>-', 'LineWidth', 2);
    plot(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, 'k<-', 'LineWidth', 2);
    plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'co-', 'LineWidth', 2);
    plot(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 'r*-', 'LineWidth', 2);
    
    
    xlim([0, 1000]);
    grid on;
    xlabel('Number of retrieved points', 'FontSize', 16);
    ylabel('Recall', 'FontSize', 16);
    set(gca, 'FontSize', 16);
    legend('LSH', 'SH', 'ITQ', 'MLH', 'Location', 'Best');
    saveas(gca, [num2str(all_topks(k)) '.eps'], 'psc2'); 
end

pause;
close all;

%%
sh16 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_5_18_7_54_56_train_sh16_0.mat');
sh16 = sh16.SHparam;

sh128 = load('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_11_19_23_53_54_train_sh128_0.mat');
sh128 = sh128.SHparam;

%%
clc;

for m  = [16, 32, 64, 128]
     [save_file save_figure] = train_save_file2(type, m, 0, file_pre_date);
    clear SHparam;
     load(save_file.train_sh);
     clear x;
    for k = 1 : 7 
        x(k) = sum(SHparam.modes(:) == k);
    end
    x
    pause;
end

%%
for i = 1 : 16
    figure
    stem(sh16.modes(i, :), 'o');
    xlabel('Index of principle components', 'FontSize', 16);
    ylabel('k in the eigenfunction', 'FontSize', 16);
    set(gca, 'FontSize', 16);
    saveas(gca, [num2str(i) '.bmp']);
end

pause;
close all;
%%
for i = 1 : 128
    figure
    stem(sh128.modes(i, :), 'o');
    xlabel('Index of principle components', 'FontSize', 16);
    ylabel('k in the eigenfunction', 'FontSize', 16);
    set(gca, 'FontSize', 16);
    saveas(gca, [num2str(i) '.bmp']);
end

pause;
close all;
%%
 y = flipud(x);
plot(y, '.');
xlim([0, 150]);

%%
[V, D] = eig(cov(Xtraining'));
pcaXtraining = V' * Xtraining;
%%

for i =  (512 - 128 + 1) : 512
    figure;
      hist(pcaXtraining(i, :));
      saveas(gca, [num2str(i) '.bmp']);
      close;
end
pause; 
close all;

%%
z = cumsum(y);
plot(z / z(end), '.');


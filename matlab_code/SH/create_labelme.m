function data = create_labelme(rho, Nneighbors)
fprintf('Creating data\n');
load('data/LabelMe_gist', 'gist');

X = gist';
clear gist;

Ndata = size(X, 2);
Ntest = 2000;
Ntrain = Ndata - Ntest;

gist_mean = mean(X, 2);
X = bsxfun(@minus, X, gist_mean);
normX = sqrt(sum(X.^2, 1));
X = bsxfun(@rdivide, X, normX);

ndxtest = unique(ceil(rand(Ntest, 1) * Ndata));
ndxtrain = setdiff(1:Ndata, ndxtest);
Ntest = numel(ndxtest);
Ntrain = numel(ndxtrain);

fprintf('#train data %d, #test data %d\n', Ntrain, Ntest);

Xtraining = X(:, ndxtrain);
Xtest = X(:, ndxtest);

data = construct_data(Xtraining, Xtest, [Ntrain, Ntest], rho, Nneighbors);

data.gist_mean = gist_mean;
data.ndxtest = ndxtest;
data.ndxtrain = ndxtrain;
end

function data = construct_data(Xtraining, Xtest, sizeSets, rho, Nneighbors)
[Ntraining, Ntest] = deal(sizeSets(1), sizeSets(2));
tic;
fprintf('start compute Euclidean training set\n');
Dtraining = distMat(Xtraining);
toc;
fprintf('finish Euclidean training set\n');
rhos = 0:rho;
nRhos = numel(rhos);
if (numel(rhos) ~= numel(Nneighbors))
    fprintf('Wrong Neighbors!\n');
    exit(1);
end

fprintf('start sorting Dtraining...\n');
Strainings = sparse(nRhos, Ntraining, Ntraining);
threshDists = sparse(Ntraining, nRhos);
fprintf('process data...');
for i = 1:Ntraining
    if mod(i, 1000) == 0
        fprintf('%d..', i);
    end
    [sortedD sortedInd] = sort(Dtraining(i, :));
    ind = (sortedInd - 1) * Ntraining + i;
    for j = 1:nRhos
        Nneighbor = Nneighbors(j);
        Strainings(j, ind) = 1;
        threshDists(i, j) = sortedD(Nneighbor);
    end
end
fprintf('\n');
fprintf('finish sorting Dtraining...\n');

data.Ntraining = Ntraining;
data.Ntest = Ntest;
data.Strainings = Strainings;
data.Xtraining = Xtraining;
data.Xtest = Xtest;
data.threshDist = threshDists(end);
data.threshDists = mean(threshDists, 1);
clear Strainings;
fprintf('finish training stats..\n');

% test2train data
fprintf('start sorting DtestTraining...\n');
DtestTraining = distMat(Xtest, Xtraining);
data.DtestTraining = DtestTraining;
clear DtestTraining;
fprintf('finish sorting...\n');
[sortedD sortedInd] = sort(DtestTraining, 2);
clear sortedD;
StestTraining = sparse(Ntest, Ntraining);
index = (sortedInd(:, 1:Nneighbors(end)) - 1) * Ntest + repmat([1:Ntest]', [1, Nneighbors(nRhos)]);
StestTraining(index) = 1;

data.StestTraining = StestTraining;
fprintf('finish test2training stats.\n');
end


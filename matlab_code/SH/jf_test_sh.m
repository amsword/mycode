function eval = jf_test_sh(SHparam, Xtest, Xbase, StestBase, topK, trueRank_fileseed)

eval = jf_eval_hash3(SHparam.nbits, 'sh', SHparam, Xtest, Xbase, StestBase, topK, trueRank_fileseed);
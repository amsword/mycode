
Xtraining = data2.Xtraining';
Xtest = data2.Xtest';
nb = 14;
WtrueTestTraining = data2.StestTraining;
SHparam.nbits = nb; % number of bits to code each sample

% training
SHparam = trainSH(Xtraining, SHparam);

% compress training and test set
[B1,U1] = compressSH(Xtraining, SHparam);
[B2,U2] = compressSH(Xtest, SHparam);

% example query
Dhamm = hammingDist(B2, B1);
%    size(Dhamm) = [Ntest x Ntraining]

% evaluation
score = evaluation(WtrueTestTraining, Dhamm, 1, 'o-', 'color', colors(i));
    
function SHparam = jf_train_sh(Xtraining, nb)

SHparam.nbits = nb;

fprintf('start SH training\n');
SHparam = trainSH(Xtraining', SHparam);
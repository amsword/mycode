%% add path
run('\\msra-msm-02\C$\Users\v-jianfw\Desktop\v-jianfw\HashCode\jf_conf.m');

%% load training data/test data/ground truth
m = 16;
type = 'LabelmeOrigin';
case_all_header;
jf_compare;
%% SH training

SHparam = jf_train_sh(Xtraining, m);

%% SH testing
eval_sh = cell(numel(all_topks), 1);
for k = 1 : numel(all_topks)
    eval_sh{k} = eval_hash5(SHparam.nbits, 'sh', SHparam, ...
        Xtest, Xtraining, StestBase2', ...
        all_topks(k), gnd_file.TestBaseSeed, ...
        [], metric_info, eval_types);
end

return;
%% saved training result for previous running
file_train_16 = '\\msra-msm-02\C$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_5_18_7_54_56_train_sh16_0.mat';
file_train_128 = '\\msra-msm-02\C$\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_11_19_23_53_54_train_sh128_0.mat';

%
sh16 = load(file_train_16);
sh128 = load(file_train_128);
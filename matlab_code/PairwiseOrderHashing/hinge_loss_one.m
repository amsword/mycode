function y = hinge_loss_one(hamming_dist)
% hamming_dist = reshape(hamming_dist, numel(hamming_dist), 1);
% d = bsxfun(@minus, hamming_dist, hamming_dist');
% 
% d = d + 1;
% y = sum(d(d > 0));


d = diff(hamming_dist);
d = -d;
d = d + 1;
y = sum(d(d > 0));
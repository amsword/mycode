idx_point = 17108 + 1;
delta = zeros(512, 64);
all_first = zeros(numel(hamming), 1);
for i = 1 : (numel(hamming) - 1)
    if hamming(i) < hamming(i + 1)
        continue;
    end
    diff = hamming(i) - hamming(i + 1) - 1;
    first = sigmf(diff, [1 / 64, 0]);
    first = first * (1 - first);
    all_first(i) = first;
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, i)) .* sigwx(:, idx_point) .* (1 - sigwx(:, idx_point));
    delta = delta + bsxfun(@times, Xtraining(:, idx_point), coef');
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, i)) .* sigwx(:, sample_idx(i)) .* (1 - sigwx(:, sample_idx(i)));
    delta = delta - bsxfun(@times, Xtraining(:, sample_idx(i)), coef');
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, i + 1)) .* sigwx(:, idx_point) .* (1 - sigwx(:, idx_point));
    delta = delta - bsxfun(@times, Xtraining(:, idx_point), coef');
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, i + 1)) .* sigwx(:, sample_idx(i + 1)) .* (1 - sigwx(:, sample_idx(i + 1)));
    delta = delta + bsxfun(@times, Xtraining(:, sample_idx(i + 1)), coef');
end
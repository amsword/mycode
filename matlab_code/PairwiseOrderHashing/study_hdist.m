
%%
% rng(0);

while(1)
    
a=get(0);
figure('position',a.MonitorPositions);
    
NQuery = size(XQuery, 2);
idx_sel = rand() * NQuery;
idx_sel = ceil(idx_sel);
idx_sel = max(1, idx_sel);
idx_sel = min(NQuery, idx_sel);
idx_sel

one_target = target{idx_sel}.idx;

one_query = XQuery(:, idx_sel);

one_dist = jf_distMat(one_query, Xtraining);
[s_dist, s_idx] = sort(one_dist);

%
W = W_init;

b_query = W' * one_query > 0;
b_query = double(b_query);
b_base = W' * Xtraining > 0;
b_base = double(b_base);

hamming_dist = jf_distMat(b_query, b_base);
hamming_dist = hamming_dist .^ 2;
part_hamming = hamming_dist(one_target);
obj_part_init = hinge_loss_one(part_hamming);
hamming_dist = hamming_dist(s_idx);
obj_full_init = hinge_loss_one(hamming_dist);

part_init = part_hamming;
full_init = hamming_dist;

%
W = W_out;

b_query = W' * one_query > 0;
b_query = double(b_query);
b_base = W' * Xtraining > 0;
b_base = double(b_base);

hamming_dist = jf_distMat(b_query, b_base);
hamming_dist = hamming_dist .^ 2;
part_hamming = hamming_dist(one_target);
obj_part_out = hinge_loss_one(part_hamming);
hamming_dist = hamming_dist(s_idx);
obj_full_out = hinge_loss_one(hamming_dist);
 
part_out = part_hamming;
full_out = hamming_dist;
%
W = W_mlh;

b_query = W' * one_query > 0;
b_query = double(b_query);
b_base = W' * Xtraining > 0;
b_base = double(b_base);

hamming_dist = jf_distMat(b_query, b_base);
hamming_dist = hamming_dist .^ 2;
part_hamming = hamming_dist(one_target);
obj_part_mlh = hinge_loss_one(part_hamming);
hamming_dist = hamming_dist(s_idx);
obj_full_mlh = hinge_loss_one(hamming_dist);

part_mlh = part_hamming;
full_mlh = hamming_dist;
%
subplot(3, 2, 1);
plot(part_init, '.');
title(['part\_init ' num2str(obj_part_init)]);
ylim([0, 128]);
grid on;

subplot(3, 2, 2);
plot(full_init, '.');
title(['full\_init', num2str(obj_full_init)]);
ylim([0, 128]);
grid on;

subplot(3, 2, 3);
plot(part_out, '.');
title(['part\_out ' num2str(obj_part_out)]);
ylim([0, 128]);
grid on;

subplot(3, 2, 4);
plot(full_out, '.');
title(['full\_out' num2str(obj_full_out)]);
ylim([0, 128]);
grid on;

subplot(3, 2, 5);
plot(part_mlh, '.');
title(['part\_mlh', num2str(obj_part_mlh)]);
ylim([0, 128]);
grid on;

subplot(3, 2, 6);
plot(full_mlh, '.');
title(['full\_mlh', num2str(obj_full_mlh)]);
ylim([0, 128]);
grid on;

pause;
close all;

end
return;
%%
XQuery = Xtraining;

%%

target = read_sp_mat('100target', 'double');
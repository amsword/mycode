eval_pw = cell(numel(all_topks), 1);
parfor k = 1 : numel(all_topks)
    eval_pw{k} = eval_hash5(size(W_pw, 2), 'linear', ...
        W_pw, Xtest, Xtraining, StestBase2', ...
        all_topks(k), [], [], metric_info, eval_types);
end

%%
for k = 1 : numel(all_topks)
    figure;
    hold on;
    
    plot(eval_lsh{k}.avg_retrieved, eval_lsh{k}.rec, 'b-*');
%     plot(eval_itq{k}.avg_retrieved, eval_itq{k}.rec, 'r-o');
%     plot(eval_sh{k}.avg_retrieved, eval_sh{k}.rec, 'r-o');
%     plot(eval_usplh{k}.avg_retrieved,  eval_usplh{k}.rec, 'r-o');
%     plot(eval_mbq{k}.avg_retrieved, eval_mbq{k}.rec, 'r-o');
    plot(eval_mlh{k}.avg_retrieved, eval_mlh{k}.rec, 'k-o');
    
    plot(eval_pw{k}.avg_retrieved, eval_pw{k}.rec, 'r-d');
    
    xlim([0, 1000]);
    grid on; 
end

pause;
close all;
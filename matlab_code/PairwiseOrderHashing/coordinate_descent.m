
idx = read_mat('C:\Users\v-jianfw\Desktop\v-jianfw\VSProject2\SGD\sample_idx', 'int32');
idx = idx + 1;
x_query = Xtraining(:, 17109);
x_sorted = Xtraining(:, idx);
W = read_mat('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_5_20_8_20_5_train_c_bre_init64_0', 'double');

x_query;
x_sorted;
W;
%
for iter = 1 : 10
    for j = 1 : size(W, 2)
        i = rand() * size(W, 1);
        i = ceil(i);
        if (i < 0)
            i = 1;
        end
        
        WX = W' * x_sorted;
        WQ = W' * x_query;
        
        wt = W(:, j);
        wij = W(i, j);
        all_th = (wij * x_sorted(i, :) - WX(j, :)) ./ x_sorted(i, :);
        
        all_th = sort(all_th);
        
        all_wij = zeros(numel(all_th) + 1, 1);
        all_wij(1) = all_th(1) - 1e-3;
        all_wij(2 : (numel(all_th) + 1)) = all_th + [diff(all_th) / 2, 1e-3];
        
        best_k = -1;
        best_value1 = 10000;
        best_value2 = 100000;
        for k = 1 : numel(all_wij)
            W(i, j) = all_wij(k);
            
            WX = W' * x_sorted;
            WQ = W' * x_query;
            bWX = WX > 0;
            bWQ = WQ > 0;
            dist_ham = bsxfun(@minus, bWX, bWQ);
            dist_ham = abs(dist_ham);
            dist_ham = sum(dist_ham, 1);
            offset = diff(dist_ham);
            offset = offset - 1;
            num1 = sum(offset <= 0);
            num2 = -sum(offset(offset <= 0));
            
            if num2 < best_value2
                best_k = k;
                best_value1 = num1;
                best_value2 = num2;
            end
        end
        
        W(i, j) = all_wij(best_k);
        
    end
end
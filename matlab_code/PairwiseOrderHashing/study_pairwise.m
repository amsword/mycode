W = read_mat('C:\Users\v-jianfw\Desktop\v-jianfw\Data_HashCode\LabelmeOrigin\working\2012_5_20_8_20_5_train_c_bre_init64_0', 'double');
%%

WX = W' * Xtraining;
bwx = WX > 0;
sigwx = sigmf(WX, [1, 0]);
%%
file = 'C:\Users\v-jianfw\Desktop\v-jianfw\VSProject2\SGD\';

%%
sample_idx = read_mat([file 'tmp_point_idx'], 'int');
sample_idx = sample_idx + 1;
%% hamming dist
bquery = bwx(:, 17109);
bsamples = bwx(:, sample_idx);
diff = bsxfun(@minus, bsamples, bquery);
diff = abs(diff);
hamming = sum(diff, 1);
%%
idx_point = 17108 + 1;
delta = zeros(512, 64);
all_first = zeros(numel(hamming), 1);
for i = 1 : (numel(hamming) - 1)
    if hamming(i) < hamming(i + 1)
        continue;
    end
    diff = hamming(i) - hamming(i + 1) - 1;
    first = sigmf(diff, [1 / 64, 0]);
    first = first * (1 - first);
    all_first(i) = first;
    
    t_idx = sample_idx(i);
    coef = first * (sigwx(:, idx_point) - sigwx(:, t_idx)) .* sigwx(:, idx_point) .* (1 - sigwx(:, idx_point));
    delta = delta + bsxfun(@times, Xtraining(:, idx_point), coef');
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, t_idx)) .* sigwx(:, t_idx) .* (1 - sigwx(:, t_idx));
    delta = delta - bsxfun(@times, Xtraining(:, t_idx), coef');
    
    t_idx = sample_idx(i + 1);
    coef = first * (sigwx(:, idx_point) - sigwx(:, t_idx)) .* sigwx(:, idx_point) .* (1 - sigwx(:, idx_point));
    delta = delta - bsxfun(@times, Xtraining(:, idx_point), coef');
    
    coef = first * (sigwx(:, idx_point) - sigwx(:, t_idx)) .* sigwx(:, t_idx) .* (1 - sigwx(:, t_idx));
    delta = delta + bsxfun(@times, Xtraining(:, t_idx), coef');
end
%%
delta2 = read_mat([file 'tmp_delta'], 'double');
%%
norm(delta(:) - delta2(:))
norm(delta(:))
if isunix()
    setenv('EDITOR', 'vim');
end

src_parent_folder = get_src_parent_folder();
% src_parent_folder = pwd;
set(0,'defaultfigurepaperpositionmode','auto');

addpath([src_parent_folder]);
addpath([src_parent_folder 'accelerate_optimization']);
addpath([src_parent_folder 'ockmeans']);
addpath([src_parent_folder 'LDA']);
addpath([src_parent_folder 'MFA']);
addpath([src_parent_folder 'gkmeans']);
addpath([src_parent_folder 'knncnn']);
addpath_recurse([src_parent_folder 'caffe_process']);
addpath_recurse([src_parent_folder 'utilities']);
addpath_recurse([src_parent_folder 'data_input']);
addpath_recurse([src_parent_folder 'locality_ockmeans']);
addpath_recurse([src_parent_folder 'third_party/SLEP']);
addpath_recurse([src_parent_folder 'third_party/fast_sc/customized']);
addpath_recurse([src_parent_folder 'third_party/fast_sc/code']);
len = numel(src_parent_folder);
if ispc()
    addpath([src_parent_folder(1 : len - 12) 'c_code\x64\Release']);
elseif isunix()
    addpath([src_parent_folder(1 : len - 12) 'c_code/bin']);
else
    error('what it is?')
end
%savepath;
%return;


addpath([src_parent_folder 'CBH']);
addpath([src_parent_folder 'CBH_ulti']);
addpath([src_parent_folder 'MLH']);
addpath([src_parent_folder 'utlis']);
addpath([src_parent_folder 'Experiment']);

addpath([src_parent_folder 'BRE']);
addpath([src_parent_folder 'CH']);
addpath([src_parent_folder 'LSH']);
addpath([src_parent_folder 'SH']);

addpath([src_parent_folder 'BSD']);
addpath([src_parent_folder 'USPLH']);

addpath([src_parent_folder 'ITQ']);
addpath([src_parent_folder 'IsoHash']);

addpath([src_parent_folder 'MBQ']);
addpath([src_parent_folder 'TwoSteps']);
addpath([src_parent_folder 'SIFT']);

addpath([src_parent_folder 'cBRE']);

addpath([src_parent_folder 'ClusterHashing']);

addpath([src_parent_folder 'Anchor_Graph_Hash']);


addpath([src_parent_folder 'LDA']);

addpath([src_parent_folder 'utlis\Input_Output']);

addpath([src_parent_folder 'utlis\tiny_code']);

addpath([src_parent_folder 'kernelhashing']);



addpath([src_parent_folder 'PaperFigure']);

addpath([src_parent_folder 'SCH']);

% addpath_recurse([src_parent_folder 'utlis\OpenOpt'])
<<<<<<< Updated upstream
%addpath_recurse([src_parent_folder 'utlis\myqueue_1.1']);
%addpath_recurse([src_parent_folder 'utlis\sendtoemail']);
%addpath_recurse([src_parent_folder 'utlis\figure_plot_utility']);


%addpath_recurse([src_parent_folder 'utlis\export_fig']);

%addpath_recurse([src_parent_folder 'Baselines']);
=======
% addpath_recurse([src_parent_folder 'utlis\myqueue_1.1']);
% addpath_recurse([src_parent_folder 'utlis\sendtoemail']);
% addpath_recurse([src_parent_folder 'utlis\figure_plot_utility']);
% 
% 
% addpath_recurse([src_parent_folder 'utlis\export_fig']);
% 
% addpath_recurse([src_parent_folder 'Baselines']);
>>>>>>> Stashed changes

addpath([src_parent_folder 'sparse_ckmeans']);

%addpath_recurse([src_parent_folder 'OptLookupTable']);
% run([src_parent_folder 'utlis\cvx\cvx_setup.m']);
%run([src_parent_folder 'utlis\vlfeat\toolbox\vl_setup.m']);
% run([src_parent_folder 'utlis\gurobi\gurobi_setup']);
addpath([src_parent_folder 'sparse_ckmeans']);
addpath([src_parent_folder 'utlis\cplex_win64']);
addpath([src_parent_folder 'utlis\sweep_conf']);
addpath([src_parent_folder 'OptimalBinaryFeatureRetrieval']);

% savepath;
% clear src_parent_folder;


% curve_colors =  [[255 0 0] / 255; ...
%     [148 30 249] / 255; ...
%     [220 50 176] / 255; ...
%     [20 4 229] / 255; ...
%     [3 151 219] / 255; ...
%     [1 189 37] / 255; ...
%     [0 0 0] / 255; ...
%     [80 136 170] / 255; ...
%     [200 137 30] / 255; ...
%     [100 70 230] / 255];
% markers = {'s', 'v', 'o', '>', '<', 'diamond',  'x', '+', 'hexagram', '*'};

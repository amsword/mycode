#!/usr/bin/env sh
#set -x
set +e 

#deep_num=(0 1 2 3 4 5 6 7 8 9 11 12 14 15 16 17 18 19 20)
echo @{deep_num}
rm a.txt
curr_dir=/home/wangjianfeng/code/mycode/tools/
#for ((i=0; i<20; i++))
for i in 00 01 02 03 04 05 07 08 09 10 11 12 13 14 15 16 17 18 19 20
do
	echo 'deep'${i} | tee -a a.txt
#echo "cd ${curr_dir} && nvidia-smi | tee -a a.txt"
	ssh -o ConnectTimeout=5 deep${i} "cd ${curr_dir} && nvidia-smi | grep MiB | tee -a a.txt"
done


import numpy as np
import matplotlib.pyplot as plt
import Image

num_rows = 768;
num_cols = 1024;

im = np.random.random((num_rows, num_cols, 3));
im = im * 255;
rescaled = np.uint8(im);
im = Image.fromarray(rescaled);
im.save('/home/jianfeng/data/image_samples/random_0.jpg');


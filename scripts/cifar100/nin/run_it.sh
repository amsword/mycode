#!/usr/bin/env sh
set -x
set -e
export GOOGLE_LOG_DIR=/home/wangjianfeng/glogs
#TOOLS=/home/wangjianfeng/from_yunchao/caffe_cccp/caffe_cccp_yunchao/build/tools
TOOLS=/home/wangjianfeng/code/caffe/build/tools
NEW_TOOLS=/home/wangjianfeng/code/caffe_new/caffe/build/tools
#NEW_TOOLS=/home/wangjianfeng/code/caffe_official/caffe/build/tools
MY_TOOLS=/home/wangjianfeng/code/mycode/c_code/bin
DATA_FOLDER=/home/wangjianfeng/data/cifar100/
WORKING_FOLDER=/home/wangjianfeng/working/cifar100/nin/
CURR_DIR=/home/wangjianfeng/code/examples/cifar100/nin/


# base model
#$NEW_TOOLS/caffe train --solver=cifar100_solver.prototxt \
								#2>&1 | tee /home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_log.tee
#python best_acc.py /home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_log.tee \
		   #2>&1 | tee -a /home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_log.tee
#exit 0
#########################################################################
#MODEL_BASE=1p1p1p
#MODEL_BASE=1g00
#MODEL_BASE=1g1g0
#MODEL_BASE=1g1g1g
#MODEL_BASE=debug

#MODEL_BASE=4p1p1p
#MODEL_BASE=1p4p1p
#MODEL_BASE=1p1p4p
#MODEL_BASE=001g
#NET_REVISE=339:blobs_lr:0.01,340:blobs_lr:0.1,341:blobs_lr:0.01,342:blobs_lr:0.01 # weight
#NET_REVISE=10:batch_size:1,24:batch_size:1,num_output:1

# for 1g00
#NET_REVISE=69:num_gaussian:100
#NET_REVISE=209:num_gaussian:5
#NET_REVISE=209:num_gaussian:5,348:num_gaussian:2

# for 1p1p1p
#NET_REVISE=62:method:CUBIC,181:method:CUBIC,300:method:CUBIC
#NET_REVISE=64:type:'"'gaussian'"',65:std:0.05,183:type:'"'gaussian'"',184:std:0.05,302:type:'"'gaussian'"',303:std:0.05
#NET_REVISE=$NET_REVISE,297:blobs_lr:10
#NET_REVISE=297:blobs_lr:10
NET_REVISE=59:blobs_lr:0.01
#NET_REVISE=178:blobs_lr:10
DEVICE_ID=0
MACHINE_NAME=deep15
NET_REVISE_R=$(echo $NET_REVISE | sed 's/,/_/g')
#NET_REVISE_R=$(echo $NET_REVISE_R | sed 's/"//g')
SNAP_PREFIX=${WORKING_FOLDER}sf/$MODEL_BASE/a${NET_REVISE_R}

#SNAP_PREFIX=${WORKING_FOLDER}add_lrn/a

NET_BASE=cifar100_train_val_sf_${MODEL_BASE}.prototxt
#NET_BASE=cifar100_train_val_lrn.prototxt
#NET_REVISE=

SOLVER_USE=${SNAP_PREFIX}_s.prototxt
NET_USE=${SNAP_PREFIX}_n.prototxt
python change_field.py \
		   $NET_BASE \
		   ,${NET_REVISE} \
		   $NET_USE
python change_field.py \
		   cifar100_solver.prototxt \
		   19:snapshot_prefix:'"'${SNAP_PREFIX}'"',4:net:'"'${NET_USE}'"',22:device_id:${DEVICE_ID} \
		   $SOLVER_USE
#if [ "$MODEL_BASE"="4p1p1p" ] || [ "$MODEL_BASE"="1p4p1p" ] || [ "$MODEL_BASE"="1p1p4p" ]; then 
		#$NEW_TOOLS/caffe train --solver=$SOLVER_USE \
			#2>&1 | tee ${SNAP_PREFIX}_log.tee
#else
ssh $MACHINE_NAME \
		"export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH && \
		$NEW_TOOLS/caffe train --solver=$SOLVER_USE \
			--weights=/home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_iter_0.caffemodel \
			2>&1 | tee -a ${SNAP_PREFIX}_log.tee"
#fi

#--snapshot=${SNAP_PREFIX}_iter_120000.solverstate \
#--weights=/home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_iter_0.caffemodel \
python best_acc.py ${SNAP_PREFIX}_log.tee 2>&1 | tee -a ${SNAP_PREFIX}_log.tee

#$NEW_TOOLS/caffe test --model=$NET_USE \
			#--weights=${SNAP_PREFIX}_iter_120000.caffemodel \
			#--gpu=$DEVICE_ID \
			#--iterations=1

#$NEW_TOOLS/caffe test --weights="/home/wangjianfeng/working/cifar10/nin/sf/1p00/59:10_iter_120000.caffemodel" \
							   #--model="cifar10_train_val_sf_1p00.prototxt" --iterations=500 --gpu=2
#ssh deep11 \
	#"cd ${CURR_DIR} && 
#$NEW_TOOLS/caffe train --solver=cifar10_solver_sf.prototxt \
	#--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel
#--weights=/home/wangjianfeng/working/cifar10/nin/sf/debug_iter_0.caffemodel
#2>&1 | tee ${WORKING_FOLDER}sf/1p00_lr11.tee
#--weights=/home/wangjianfeng/working/cifar10/nin/sf/sf_iter_120000.caffemodel
#--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel \
#2>&1 | tee ${WORKING_FOLDER}baseline/
# test model as a baseline 
#$TOOLS/test_net.bin ${TEST_PROTO_BASELINE} ${MODEL90} 100 GPU 2
		#2>&1 | tee -a ${WORKING_FOLDER}${MODEL90}_baseline.tee
#$NEW_TOOLS/caffe time \
  #--gpu=3 \
  #--model=cifar10_train_val_sf_100.prototxt \
  #--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel
echo "done"

import sys;
python_tools = "/home/wangjianfeng/code/mycode/python_code";
sys.path.append(python_tools);

from train_revise_solver import train_revise_solver;
from best_acc import *
from com_utility import *

net_base = "cifar100_train_val.prototxt";
solver_base = "cifar100_solver.prototxt";
net_revise = "";
solver_revise = "base_lr:0.001";
machine_name = "deep08";
device_id = 0;
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
snap_prefix = "/home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/ft";
finetune = "/home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/a_iter_120000.caffemodel";

log_file = "/home/wangjianfeng/working/cifar100/nin/baseline_new_caffe/ft_base_lr:0.001_log.tee";
#highest_acc = best_acc(log_file);
#with open(log_file, "a") as fp:
    #fp.writelines("best acc: " + str(highest_acc));
#log_file = train_revise_solver(net_base, solver_base, 
        #net_revise, solver_revise, 
        #machine_name, device_id, caffe_dir, snap_prefix, 
        #finetune);
plot_acc(log_file, "/home/wangjianfeng/a.png");

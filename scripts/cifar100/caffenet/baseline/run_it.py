import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = [""];
all_device_id, all_machine_name = get_free_gpu(['10', '05', '01']);
#all_device_id = [1];
#all_machine_name = ['deep03'];
print len(all_device_id);
num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
model_base = "merge";
#assert False
#model_base = "preprocessed_data"
net_base = "cifar100_full_train_test.prototxt";
solver_base = "cifar100_full_solver.prototxt";
net_revise = "";
solver_revise = "max_iter:350000,base_lr:0.000001";
machine_name = all_machine_name[0];
device_id = all_device_id[0];
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
snap_prefix = "/home/wangjianfeng/working/cifar100/caffenet/baseline/" + model_base + "/a_";
finetune = "" 
continuing = concat_snap_prefix(snap_prefix, "", "max_iter:300000,base_lr:0.00001") + "_iter_300000.solverstate"
all_procs = [None] * 3;
p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    net_revise, solver_revise, 
	machine_name, device_id, caffe_dir, snap_prefix, 
        finetune, continuing);
all_procs[0] = (p, log_file_name);
#p.wait();

#continuing = concate_snap_prefix(snap_prefix, net_revise, solver_revise) + "_iter_60000.solverstate";
#solver_revise = "";
#snap_prefix = "/home/wangjianfeng/working/cifar100/caffenet/baseline/" + model_base + "/b_" 
#solver_base = "cifar100_full_solver_lr1.prototxt";
#p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    #net_revise, solver_revise, 
	#machine_name, device_id, caffe_dir, snap_prefix, 
        #finetune, continuing);
#all_procs[1] = (p, log_file_name);
#p.wait();

#continuing = concate_snap_prefix(snap_prefix, net_revise, solver_revise) + "_iter_65000.solverstate";
#solver_revise = "";
#snap_prefix = "/home/wangjianfeng/working/cifar100/caffenet/baseline/" + model_base + "/c_" 
#solver_base = "cifar100_full_solver_lr2.prototxt";
#p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    #net_revise, solver_revise, 
	#machine_name, device_id, caffe_dir, snap_prefix, 
        #finetune, continuing);
#all_procs[2] = (p, log_file_name);
#p.wait();


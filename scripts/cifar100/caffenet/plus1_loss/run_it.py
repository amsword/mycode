import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import * 


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(6, 11) for up in range(10, 11) if up >= lb]
all_num_conti = [40] * 5;
all_num_conti[4] = 35; 
all_device_id, all_machine_name = get_free_gpu(['10', '12', '05', '01']);

print all_net_revise;
print len(all_device_id)
num = len(all_net_revise);
assert len(all_device_id) == len(all_machine_name), (len(all_device_id), len(all_machine_name));
assert len(all_machine_name) >= num, (len(all_machine_name), num);
assert len(all_device_id) >= num, (num, len(all_device_id));

model_base = "merge";
#model_base = "mirrored";
all_procs = [None] * num;
#assert False
for i in range(num):
    net_base = "cifar100_full_train_test.prototxt";
    solver_base = "cifar100_full_solver.prototxt";
    net_revise = all_net_revise[i];
    #solver_revise = "max_iter:400000,base_lr:0.0001";
    solver_revise = "max_iter:" + str(all_num_conti[i] + 5) + "0000,base_lr:0.000001";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/cifar100/caffenet/plus1_loss/direct_";
    
    finetune = '';
    continuing = '';
    continuing = concat_snap_prefix(snap_prefix, net_revise, 
            'max_iter:' + str(all_num_conti[i]) + '0000,base_lr:0.00001') + \
                "_iter_" + str(all_num_conti[i]) + "0000.solverstate"

    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
            net_revise, solver_revise, 
            machine_name, device_id, caffe_dir, snap_prefix, 
            finetune, continuing) 

    all_procs[i] = (p, log_file_name);

print [p.poll() for p, log_file_name in all_procs];


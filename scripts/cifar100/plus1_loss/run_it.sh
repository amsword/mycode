#!/usr/bin/env sh
set -x
set -e
export GOOGLE_LOG_DIR=/home/wangjianfeng/glogs
#TOOLS=/home/wangjianfeng/from_yunchao/caffe_cccp/caffe_cccp_yunchao/build/tools
TOOLS=/home/wangjianfeng/code/caffe/build/tools
NEW_TOOLS=/home/wangjianfeng/code/caffe_new/caffe/build/tools
#NEW_TOOLS=/home/wangjianfeng/code/caffe_official/caffe/build/tools
MY_TOOLS=/home/wangjianfeng/code/mycode/c_code/bin
WORKING_FOLDER=/home/wangjianfeng/working/cifar100/
CURR_DIR=/home/wangjianfeng/code/examples/cifar100/plus1_loss/
PYTHON_TOOLS=/home/wangjianfeng/code/mycode/python_code/

# protocol
# cccp6, cccp5, pool3, softmax
#$NEW_TOOLS/caffe train \
  #--solver=cifar10_solver_merge.prototxt
#exit 0
#FEATURE_LAYER=cccp6
#########################################################################
# train the model
# gpu 0
#ssh deep19 \
	#"cd /home/wangjianfeng/code/examples/cifar10/nin/plus1_loss && \
	#$NEW_TOOLS/caffe train \
		#--solver=cifar10_solver_merge.prototxt"
#exit 0
#$NEW_TOOLS/caffe time \
  #--gpu=3 \
  #--model=cifar10_train_val_sf_100.prototxt \
  #--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel
#ssh deep11 "source ~/.bashrc && cd ${CURR_DIR} && $NEW_TOOLS/caffe train --solver=cifar10_solver_sf.prototxt 2>&1 | tee ${WORKING_FOLDER}sf/log_41.tee"
# gpu 1
#MODEL_BASE=1p1p1p
#MODEL_BASE=1g00
#MODEL_BASE=1g1g0
#MODEL_BASE=1g1g1g
#MODEL_BASE=debug

#MODEL_BASE=4p1p1p
#MODEL_BASE=1p4p1p
#MODEL_BASE=exclude_relu_cccp5
MODEL_BASE=merge
#NET_REVISE=339:blobs_lr:0.01,340:blobs_lr:0.1,341:blobs_lr:0.01,342:blobs_lr:0.01 # weight
#NET_REVISE=10:batch_size:1,24:batch_size:1,num_output:1

# for 1g1g0 
#NET_REVISE=69:num_gaussian:100
#NET_REVISE=209:num_gaussian:100
#NET_REVISE=349:num_gaussian:100
#NET_REVISE=209:num_gaussian:5,349:num_gaussian:2


# for 1p1p1p
#NET_REVISE=62:method:CUBIC,181:method:CUBIC,300:method:CUBIC
#NET_REVISE=64:type:'"'gaussian'"',65:std:0.05,183:type:'"'gaussian'"',184:std:0.05,302:type:'"'gaussian'"',303:std:0.05
#NET_REVISE=$NET_REVISE,297:blobs_lr:10
#NET_REVISE=297:blobs_lr:100
#NET_REVISE=$NET_REVISE,297:blobs_lr:10
#NET_REVISE=59:blobs_lr:6
SOLVER_REVISE=23:random_seed:1
SOLVER_REVISE=$SOLVER_REVISE,7:base_lr:0.01
#SOLVER_REVISE=$SOLVER_REVISE,11:stepsize:20000,10:gamma:0.7
NET_REVISE=341:lower_bound:0.7,342:upper_bound:1
#NET_REVISE=178:blobs_lr:100
DEVICE_ID=1
MACHINE_NAME=deep08
SOLVER_REVISE_R=$(echo $SOLVER_REVISE | sed 's/,/_/g')
SOLVER_REVISE_R=$(echo $SOLVER_REVISE_R | sed 's/"//g')
NET_REVISE_R=$(echo $NET_REVISE | sed 's/,/_/g')
NET_REVISE_R=$(echo $NET_REVISE_R | sed 's/"//g')
SNAP_PREFIX=${WORKING_FOLDER}plus1_loss/${MODEL_BASE}/fine_tune${NET_REVISE_R}_$SOLVER_REVISE_R
#SNAP_PREFIX=${WORKING_FOLDER}add_lrn/a
SOLVER_USE=${SNAP_PREFIX}_s.prototxt
NET_USE=${SNAP_PREFIX}_n.prototxt

#NET_BASE=cifar100_train_val_ft.prototxt
NET_BASE=cifar100_train_val_${MODEL_BASE}.prototxt
python ${PYTHON_TOOLS}change_field.py \
		   $NET_BASE \
		   ,${NET_REVISE} \
		   $NET_USE
python ${PYTHON_TOOLS}change_field.py \
		   cifar100_solver_merge.prototxt \
		   19:snapshot_prefix:'"'${SNAP_PREFIX}'"',4:net:'"'${NET_USE}'"',22:device_id:${DEVICE_ID},$SOLVER_REVISE,14:max_iter:200000 \
		   $SOLVER_USE
ssh $MACHINE_NAME \
		"export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH && \
		$NEW_TOOLS/caffe train --solver=$SOLVER_USE \
		--weights=${WORKING_FOLDER}plus1_loss/${MODEL_BASE}/direct341:lower_bound:0.9_342:upper_bound:1_23:random_seed:1_7:base_lr:0.1_iter_150000.caffemodel \
		2>&1 | tee -a ${SNAP_PREFIX}_log.tee"

#--snapshot=${SNAP_PREFIX}_iter_150000.solverstate \
#--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_120000.caffemodel \
python best_acc.py ${SNAP_PREFIX}_log.tee 2>&1 | tee -a ${SNAP_PREFIX}_log.tee

#$NEW_TOOLS/caffe test --model=$NET_USE \
			#--weights=${SNAP_PREFIX}_iter_120000.caffemodel \
			#--gpu=$DEVICE_ID \
			#--iterations=1

#$NEW_TOOLS/caffe test --weights="/home/wangjianfeng/working/cifar10/nin/sf/1p00/59:10_iter_120000.caffemodel" \
							   #--model="cifar10_train_val_sf_1p00.prototxt" --iterations=500 --gpu=2
#ssh deep11 \
	#"cd ${CURR_DIR} && 
#$NEW_TOOLS/caffe train --solver=cifar10_solver_sf.prototxt \
	#--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel
#--weights=/home/wangjianfeng/working/cifar10/nin/sf/debug_iter_0.caffemodel
#2>&1 | tee ${WORKING_FOLDER}sf/1p00_lr11.tee
#--weights=/home/wangjianfeng/working/cifar10/nin/sf/sf_iter_120000.caffemodel
#--weights=/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_0.caffemodel \
#2>&1 | tee ${WORKING_FOLDER}baseline/
echo "done"

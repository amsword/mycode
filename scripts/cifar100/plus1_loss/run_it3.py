import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import train_revise_solver_asyc
from com_utility import get_free_gpu;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(8, 10) for up in range(10, 11) if up >= lb]
all_device_id, all_machine_name = get_free_gpu(['10', '12', '15', '05', '04', '03', '01']);

print all_net_revise;
print len(all_device_id)
num = len(all_net_revise);
assert len(all_device_id) == len(all_machine_name), (len(all_device_id), len(all_machine_name));
assert len(all_machine_name) >= num, (len(all_machine_name), num);
assert len(all_device_id) >= num, (num, len(all_device_id));

all_procs = [];
model_base = "merge";
#model_base = "mirrored";
all_procs = [None] * num;
#assert False
for i in range(num):
    net_base = "cifar100_train_val_" + model_base + ".prototxt";
    solver_base = "cifar100_solver_merge.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "max_iter:150000,random_seed:1";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/cifar100/plus1_loss/" + model_base + "/direct_";
    #finetune = r"/home/wangjianfeng/working/cifar100/plus1_loss/merge/direct_lower_bound:0.9_upper_bound:1.0_max_iter:200000_iter_200000.caffemodel";
    #if all_procs[i][0].poll() == None:
        #continue;
    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
            net_revise, solver_revise, 
            machine_name, device_id, caffe_dir, snap_prefix, 
            finetune = "", continuing = "");
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];

#for (p, log_file_name) in all_procs:
    #os.remove(log_file_name);

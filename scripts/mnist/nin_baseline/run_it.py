import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import train_revise_solver_asyc
from com_utility import get_free_gpu;


all_net_revise = [""];
all_solver_revise = [""];
#all_device_id, all_machine_name = get_free_gpu(['10', '12', '15', '05', '04']);

print all_net_revise;
num = len(all_net_revise);
assert len(all_device_id) == len(all_machine_name), (len(all_device_id), len(all_machine_name));
assert len(all_machine_name) >= num, (len(all_machine_name), num);
assert len(all_device_id) >= num, (num, len(all_device_id));
#assert False
all_procs = [None] * num;
for i in range(len(all_solver_revise)):
    model_base = "merge";
    net_base = "mnist_train_val_" + model_base + ".prototxt";
    solver_base = "mnist_solver_merge.prototxt";
    net_revise = "";
    solver_revise = all_solver_revise[i]; 
    machine_name = all_machine_name[0];
    device_id = all_device_id[0];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/mnist/nin_baseline/direct_";
    
    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
            net_revise, solver_revise, 
            machine_name, device_id, caffe_dir, snap_prefix, 
            finetune = "", continuing = "");
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];
#for (p, log_file_name) in all_procs:
    #os.remove(log_file_name);

import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(6, 11) for up in range(10, 11) if up >= lb]
#all_net_revise = [net_revise + ",251:dropout_ratio:0.8" for net_revise in all_net_revise];

all_device_id, all_machine_name = get_free_gpu(['10', '12', '15', '05', '04', '01']);

print len(all_machine_name)
num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#assert False
for i in range(num):
    net_base = "mnist_train_val_merge.prototxt";
    solver_base = "mnist_solver_merge.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "max_iter: 200000,base_lr:0.01";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/mnist/plus1_loss/merge/fine_tune_l0.8u1.0_";
    direct_snap_prefix = "/home/wangjianfeng/working/mnist/plus1_loss/merge/direct_";
    #finetune = concate_snap_prefix(direct_snap_prefix, "lower_bound:0.9,upper_bound:1.0", "") + \
            #"_iter_150000.caffemodel";
    finetune = concate_snap_prefix(direct_snap_prefix, "lower_bound:0.8,upper_bound:1.0", "base_lr:0.1") + \
            "_iter_119500.caffemodel"

    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
        net_revise, solver_revise, 
    	machine_name, device_id, caffe_dir, snap_prefix, 
	    finetune, continuing = "");
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];


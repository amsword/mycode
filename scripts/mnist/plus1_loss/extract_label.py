import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);
from com_utility import * 

caffe_dir = "/home/wangjianfeng/code/caffe/build/tools/"
device_id = 1
machine_name = "deep18";

cmd = caffe_dir + "extract_info.bin" + " " + \
    "-input_type lmdb -input /home/wangjianfeng/data/mnist/mnist-train-lmdb " + \
    "-output /home/wangjianfeng/data/mnist/mnist-train-lmdb.label -info_type label"

p, log_file_name = throw_cmd(machine_name, cmd, 'extract.log');


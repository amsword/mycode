#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/caffe/build
MODEL=

${TOOLS}/extract_features.bin \
	${MODEL} \
	${PROTOCOL} \
	${OUTPUT_LAYER} \
	${OUTPUT_FILE} \
	${BATCH_SIZE}


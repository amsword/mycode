#!/usr/bin/env sh
# Create the imagenet leveldb inputs
# N.B. set the path to the imagenet train + val data dirs
TOOLS=/home/wangjianfeng/code/caffe/build/tools
IMAGE_LIST_DIR=/home/wangjianfeng/data/imagenet2012/image_list
OUTPUT_DIR=/home/wangjianfeng/data/imagenet2012/lmdb

TRAIN_DATA_ROOT=/home/wangjianfeng/data/imagenet2012/images/ILSVRC2012_cls_train_256/
VAL_DATA_ROOT=/home/wangjianfeng/data/imagenet2012/images/ILSVRC2012_cls_test_256/

echo "Creating train lmdb ..."

$TOOLS/convert_imageset.bin \
    $TRAIN_DATA_ROOT \
    $IMAGE_LIST_DIR/train.txt \
    ${OUTPUT_DIR}/imagenet_train_leveldb -rs 1 -bd lmdb\
    -rh $RESIZE_HEIGHT -rw $RESIZE_WIDTH

#echo "Creating val lmdb ..."

#$TOOLS/convert_imageset.bin \
    #$VAL_DATA_ROOT \
    #$IMAGE_LIST_DIR/val.txt \
    #${OUTPUT_DIR}/imagenet_val_lmdb -rs 1 -bd lmdb\
    #-rh $RESIZE_HEIGHT -rw $RESIZE_WIDTH

echo "Done."

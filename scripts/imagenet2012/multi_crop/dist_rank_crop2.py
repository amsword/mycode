python_tools = "/home/wangjianfeng/code/mycode/python_code/";
import sys;
sys.path.append(python_tools);
from com_utility import throw_cmd;

idx_start = 800000;
idx_end = 1300000;

#all_machine_id = ['07', '07', '07', '07', '07', '09', '12', '13', '14'];
all_device_id = [];
all_machine_id = [];
all_machine_id.extend(['07'] * 17);
all_device_id.extend([2, 3]);
all_device_id.extend([-1] * 15);
all_machines = ['deep' + num for num in all_machine_id];

assert len(all_device_id) == len(all_machines);
import time;
#for machine_name in all_machines:
    #cmd = 'pwd'
    #time.sleep(1);
    #throw_cmd(machine_name, cmd, '');

#assert False
num_batch = len(all_machines);
batch_size = (idx_end - idx_start + 1 + num_batch - 1) / num_batch;
batch_size = int(batch_size);

all_procs = [None] * num_batch;
for idx_batch in range(num_batch):
    batch_start = idx_batch * batch_size + idx_start;
    batch_end = batch_start + batch_size - 1;
    batch_end = min(batch_end, idx_end);

    cmd = "/home/weiyunchao/matlabR2013a/bin/matlab -c /home/weiyunchao/license.lic -nodisplay -nodesktop -r " + \
            "'rank_crops_by_superclass(" + str(batch_start) + \
            ',' + str(batch_end) + ', ' + str(all_device_id[idx_batch]) + ")'";

    log_file_name = '/home/wangjianfeng/working/imagenet2012/edgebox/' + \
            all_machines[idx_batch] + str(idx_batch) + "_2";

    time.sleep(1);
    all_procs[idx_batch] = throw_cmd(all_machines[idx_batch], \
            cmd, log_file_name); 

for p in all_procs:
    if p is not None:
        p.communicate();

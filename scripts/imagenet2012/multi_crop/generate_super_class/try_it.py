import sys;
python_tools = '/home/wangjianfeng/code/mycode/python_code/'
sys.path.append(python_tools);
from com_utility import *;
import matplotlib
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
import numpy;

import sys;
import subprocess;
from nltk.corpus import wordnet
import glob;
import sklearn.cluster;

# get all offsets
train_folder = '/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/n*/';
all_folder = glob.glob(train_folder);
idx_begin = len(train_folder) - 2;
idx_end = -1;
offsets = [];
for folder in all_folder:
    str_offset = folder[idx_begin : idx_end];
    offsets.append(int(float(str_offset)));

# all synsets
num_categories = len(offsets);
all_synsets = [None] * num_categories;
for i in range(num_categories):
    i_offset = offsets[i];
    all_synsets[i] = wordnet._synset_from_pos_and_offset('n', i_offset);

# get the discard ids
import glob;
all_discard_ids = [];
all_file_names = glob.glob('./*.txt');
        
#all_file_names = [];
for file_name in all_file_names:
    with open(file_name, 'r') as fp:
        all_lines = fp.readlines();
    discard_ids = [int(float(line)) for line in all_lines[2 : ]];
    assert int(float(all_lines[1])) == len(discard_ids);
    all_discard_ids.extend(discard_ids);

# generate the filtered synsets;
filtered_synset = [];
is_selected = numpy.zeros(num_categories, numpy.bool_);
for i in range(num_categories):
    if not offsets[i] in all_discard_ids:
        filtered_synset.append(all_synsets[i]);
        is_selected[i] = True;

del all_synsets;
del offsets;

# pairwise similarity
sim_file = '/tmp/sim_file.npy';
if os.path.isfile(sim_file):
    sim_mat = numpy.load(sim_file);
else:
    sim_mat = numpy.empty([num_categories, num_categories]);
    for i in range(num_categories):
        i_offset = offsets[i];
        i_synset = all_synsets[i];
        print i
        for j in range(num_categories):
            j_offset = offsets[j];
            j_synset = all_synsets[j];
            sim_mat[i][j] = i_synset.path_similarity(j_synset);
    numpy.save(sim_file, sim_mat);

filtered_sim_mat = sim_mat[is_selected][:, is_selected];
num_super = 10;
sc = sklearn.cluster.SpectralClustering(n_clusters = num_super, affinity = 'precomputed');
labels = sc.fit_predict(filtered_sim_mat);


# the first label 
data_each_super = [];

for selected in range(num_super):
    hyper = None;
    count = 0;
    names = [];
    ids = [];
    for i in range(len(filtered_synset)):
        if labels[i] != selected:
            continue;
        count = count + 1;
        if hyper is None:
            hyper = filtered_synset[i];
        hyper = hyper.lowest_common_hypernyms(filtered_synset[i])[0];
        names.append(filtered_synset[i].name());
        ids.append(filtered_synset[i].offset());

    assert hyper is not None;
    data_each_super.append((hyper.name(), count, names, ids));

idx = 0;
for hyper, count, names, ids in data_each_super:
    print hyper, count, idx;
    idx = idx + 1;
    for i in range(len(names)):
        print '    ', names[i], ids[i];

print len(filtered_synset);


import glob;

all_file_names = glob.glob('./*.txt');

total = 0;
save_file_name = 'summary.text';
fp_save = open(save_file_name, 'w');
for file_name in all_file_names:
    with open(file_name, 'r') as fp:
        all_lines = fp.readlines();
        if len(all_lines) == 0:
            print file_name;
            break;
        name = all_lines[0];
        name = name[0 : name.find('.')];
        num = all_lines[1];
        fp_save.write(name + ": " + num);
        total = total + float(num);

print total;

fp_save.close();

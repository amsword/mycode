from nltk.corpus import wordnet

file_name = 'barrier.txt.name';
with open(file_name, 'r') as fp:
    all_lines = fp.readlines();

save_file_name = 'barrier.txt';
fp_save = open(save_file_name, 'w');
fp_save.write('barrier.\n');
fp_save.write(str(len(all_lines)) + "\n");
for line in all_lines:
    node = wordnet.synset(line);
    fp_save.write(str(node.offset()) + "\n");

fp_save.close();

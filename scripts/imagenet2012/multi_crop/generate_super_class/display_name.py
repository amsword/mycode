import sys;
python_tools = '/home/wangjianfeng/code/mycode/python_code/'
sys.path.append(python_tools);
from com_utility import *;
import matplotlib
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
import numpy;

import sys;
import subprocess;
from nltk.corpus import wordnet
import glob;
import sklearn.cluster;

# get all offsets
train_folder = '/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/n*/';
all_folder = glob.glob(train_folder);
idx_begin = len(train_folder) - 2;
idx_end = -1;
offsets = [];
for folder in all_folder:
    str_offset = folder[idx_begin : idx_end];
    offsets.append(int(float(str_offset)));

# get the discard ids
import glob;
all_file_names = glob.glob('./*.txt');
        
#all_file_names = [];
for file_name in all_file_names:
    with open(file_name, 'r') as fp:
        all_lines = fp.readlines();
    ids = [int(float(line)) for line in all_lines[2 : ]];
    assert int(float(all_lines[1])) == len(ids);
    all_names = [wordnet._synset_from_pos_and_offset('n', n_id).name() for n_id in ids];
    save_file_name = file_name + ".name";
    with open(save_file_name, "w") as fp:
        fp.writelines([name + "\n" for name in all_names]);


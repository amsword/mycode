import sys;
utility_path = '/home/wangjianfeng/code/mycode/python_code/'
sys.path.append(utility_path);

from com_utility import *;
import matplotlib
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
import numpy;

import sys;
import subprocess;
from nltk.corpus import wordnet
import glob;
import sklearn.cluster;

working_dir = '/home/wangjianfeng/working/imagenet2012/generate_super_class/'
# get all offsets
train_folder = '/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/n*/';
all_folder = glob.glob(train_folder);
idx_begin = len(train_folder) - 2;
idx_end = -1;
offsets = [];
for folder in all_folder:
    str_offset = folder[idx_begin : idx_end];
    offsets.append(int(float(str_offset)));

# all synsets
num_categories = len(offsets);
all_synsets = [None] * num_categories;
for i in range(num_categories):
    i_offset = offsets[i];
    all_synsets[i] = wordnet._synset_from_pos_and_offset('n', i_offset);

# pairwise similarity
sim_file = working_dir + 'wup_sim_file.npy';
if os.path.isfile(sim_file):
    sim_mat = numpy.load(sim_file);
else:
    sim_mat = numpy.empty([num_categories, num_categories]);
    for i in range(num_categories):
        i_synset = all_synsets[i];
        for j in range(num_categories):
            j_synset = all_synsets[j];
            sim_mat[i][j] = i_synset.wup_similarity(j_synset);
    numpy.save(sim_file, sim_mat);

num_super = 10;
sc = sklearn.cluster.SpectralClustering(n_clusters = num_super, affinity = 'precomputed');
labels = sc.fit_predict(sim_mat);

data_each_super = [];

for selected in range(num_super):
    hyper = None;
    count = 0;
    names = [];
    ids = [];
    for i in range(len(all_synsets)):
        if labels[i] != selected:
            continue;
        count = count + 1;
        if hyper is None:
            hyper = all_synsets[i];
        hyper = hyper.lowest_common_hypernyms(all_synsets[i])[0];
        names.append(all_synsets[i].name());
        ids.append(all_synsets[i].offset());

    assert hyper is not None;
    data_each_super.append((hyper.name(), count, names, ids));

idx = 0;
for hyper, count, names, ids in data_each_super:
    print hyper, count, idx;
    #idx = idx + 1;
    #for i in range(len(names)):
        #print '    ', names[i], ids[i];


%function ranking_crops_by_superclass(idx_start, idx_end, device_id)
%idx_start, idx_end, device_id
idx_start = 1;
idx_end = 1000;
device_id = 1;
%net_def = 'google_deploy.prototxt';
%pretrained = '/home/wangjianfeng/data/google_model/bvlc_googlenet.caffemodel';
net_def = './alex_deploy.prototxt';
pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';

caffe_input_size = 227;
caffe_batch_size = 64;

image_folder = '~/data/imagenet2012/image_256xN/train/';
backup_image_folder = '/home/wangjianfeng/data/imagenet2012/image_256xN/train_special_images/'
file_list = '~/data/imagenet2012/image_list/train.txt';
edgebox_folder = '/home/wangjianfeng/working/imagenet2012/edgebox/train/';
ranking_folder = '~/working/imagenet2012/edgebox/train_alex_superclass/';
super_class_ids_file = '/home/wangjianfeng/working/imagenet2012/super_class/ids.mat';

% add path of caffe
caffe_matlab_dir = '~/code/caffe_new/caffe/matlab/caffe/';
addpath(caffe_matlab_dir);
run('/home/wangjianfeng/code/mycode/matlab_code/jf_conf.m');

% get file list
list = importdata(file_list);

caffe_blob = zeros(caffe_input_size, caffe_input_size, 3, ...
        caffe_batch_size, 'single');
caffe_blob_idx = 1;

% super class
x = load(super_class_ids_file, 'mat_relation');
super_class_mat = x.mat_relation;
num_super = size(super_class_mat, 1);
% end of super class

% make dir if not exist
%for i = 1 : numel(list.data)
    %file_name = [ranking_folder list.textdata{i}];
    %[pathdir] = fileparts(file_name);
    %if ~exist(pathdir, 'file')
        %mkdir(pathdir);
    %end
%end

caffe('reset');
% initialize the network
if device_id >= 0
    caffe('set_mode_gpu');
    caffe('set_device', device_id);
else
    caffe('set_mode_cpu');
end
caffe('set_phase_test');
caffe('init', net_def, pretrained);
idx_end = min(numel(list.data), idx_end);


tic;
for i = idx_start : idx_end
    assert(caffe_blob_idx == 1);
    im_name = list.textdata{i};
    % get the image 
    edgebox_file = [edgebox_folder im_name '.seq.edgebox.short.bin'];
    final_bb_file = [ranking_folder im_name '.final.bb.short.bin'];
    final_score_file = [ranking_folder im_name '.final.score.float.bin'];
    
    if exist(final_bb_file, 'file')
        continue;
    end

    try
        I = imread([image_folder im_name]);
        is_ok = true;
    catch
        is_ok = false;
    end
    if ~is_ok
        I = imread([backup_image_folder im_name]);
        is_ok = true;
    end

    if size(I, 3) == 1
        I = repmat(I, [1 1 3]);
    end

    if toc > 10
        % get crop region list
        fprintf('%s\t%d/%d, device_id=%d\n', im_name, ...
            i, idx_end, device_id);
        tic;
    end
    boxes = read_mat(edgebox_file, 'int16');
    if isempty(boxes)
        continue;
    end
    assert(size(boxes, 1) == 5);
    assert(isempty(find(boxes(1, :) ~= 0 : (size(boxes, 2) - 1))));

    % filter by the size and ratio
    boxes_size = boxes(4, :) .* boxes(5, :);
    th = size(I, 1) * size(I, 2) * 0.1;
    indicator = boxes_size < th;
    boxes(:, indicator) = [];
    if isempty(boxes)
        continue;
    end
    
    % crop the image and get the region
    num_box = size(boxes, 2);
    all_super_confidence = zeros(num_super, num_box);
    idx_all_super_confidence = 1;
    for j = 1 : num_box 
        % change the box, so that it is a square, and then we resize it 
        bb = boxes(2 : 5, j);
        if bb(3) < bb(4)
            extra = (bb(4) - bb(3)) / 2;
            bb(2) = bb(2) + extra;
            bb(4) = bb(3);
        else
            extra = (bb(3) - bb(4)) / 2;
            bb(1) = bb(1) + extra;
            bb(3) = bb(4);
        end

        % get the crop and resize it to target
        crop = imcrop(I, bb);
        assert(size(crop, 1) == size(crop, 2));
        crop = imresize(crop, [caffe_input_size, caffe_input_size]);
        assert(size(crop, 1) == size(crop, 2) && ...
            size(crop, 1) == caffe_input_size);

        % convert to bgr, which is required by caffe
        crop = single(crop(:, :, [3, 2, 1]));
        % with is the fastest
        crop = permute(crop, [2, 1, 3]);
        caffe_blob(:, :, :, caffe_blob_idx) = crop;
        caffe_blob_idx = caffe_blob_idx + 1;
        if caffe_blob_idx == caffe_batch_size + 1
            caffe_result = caffe('forward', {caffe_blob});
            confidence = squeeze(caffe_result{1});
            super_confidence = super_class_mat * confidence;
            all_super_confidence(:, ...
                idx_all_super_confidence : idx_all_super_confidence + size(super_confidence, 2) - 1) = ...
                super_confidence;
            idx_all_super_confidence = idx_all_super_confidence + size(super_confidence, 2);
            caffe_blob_idx = 1; 
        end
    end

    if caffe_blob_idx ~= 1
        caffe_result = caffe('forward', {caffe_blob});
        confidence = squeeze(caffe_result{1});
        confidence = confidence(:, 1 : caffe_blob_idx - 1);
        super_confidence = super_class_mat * confidence;
        all_super_confidence(:, idx_all_super_confidence : end) = ...
                super_confidence;
        caffe_blob_idx = 1;
    end
    [tmp, idx] = sort(all_super_confidence, 2, 'descend');
    idx_selected = idx(:, 1);
    final_boxes = boxes(:, idx_selected);
    final_score = tmp(:, 1);
    save_mat(final_boxes, final_bb_file, 'int16');
    save_mat(final_score, final_score_file, 'single');
end
'finished'
exit;

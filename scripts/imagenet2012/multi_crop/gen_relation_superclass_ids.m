image_folder = '~/data/imagenet2012_temp/image_256xN/train/';
backup_image_folder = '/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train_special_images/'
file_list = '~/data/imagenet2012_temp/image_list/train.txt';
edgebox_folder = '/home/wangjianfeng/working/imagenet2012/edgebox/train/';
ranking_folder = '~/working/imagenet2012/edgebox/train/train_alex_superclass/';
super_class_ids_file = '/home/wangjianfeng/working/imagenet2012/super_class/ids.mat';

% add path of caffe
caffe_matlab_dir = '~/code/caffe_new/caffe/matlab/caffe/';
addpath(caffe_matlab_dir);


% get file list
list = importdata(file_list);

caffe_blob = zeros(caffe_input_size, caffe_input_size, 3, ...
        caffe_batch_size, 'single');
caffe_blob_idx = 1;

% super class
[labels, idx_selected] = unique(list.data);
names = list.textdata(idx_selected);
all_ids = zeros(numel(labels), 1);
for i = 1 : numel(names)
    str = names{i};
    nid = str(2 : strfind(str, '/') - 1);
    id = str2num(nid);
    all_ids(i) = id;
end
super_class_files = dir('./generate_super_class/*.txt');
super_class_ids = cell(numel(super_class_files), 1);
for i = 1 : numel(super_class_files)
    x = importdata(['./generate_super_class/' super_class_files(i).name]);
    assert(x.data(1) == numel(x.data) - 1);
    all_idx = zeros(numel(x.data) - 1, 1);
    for j = 2 : numel(x.data)
        anchor = x.data(j);
        sub_labels = find(all_ids == anchor);
        sub_labels = labels(sub_labels) + 1;
        assert(numel(sub_labels) == 1);
        all_idx(j - 1) = sub_labels;
    end
    super_class_ids{i} = all_idx; 
end
s = 0;
for i = 1 : numel(super_class_ids)
    s = s + numel(super_class_ids{i});
end
s
save(super_class_ids_file, 'super_class_ids');


mat_relation = zeros(numel(super_class_ids), s);
for i = 1 : numel(super_class_ids)
    mat_relation(i, super_class_ids{i}) = 1;
end
save(super_class_ids_file, 'mat_relation', '-append');

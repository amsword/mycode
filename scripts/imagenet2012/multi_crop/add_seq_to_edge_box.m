function add_seq_to_edge_box(idx_start, idx_end)
%idx_start = 961111;
%idx_end = 961111;

edge_box_path = '~/code/mycode/tools/edge_box/';
file_list = '~/data/imagenet2012_temp/image_list/train.txt';
image_folder = '/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/';
special_image_folder = '/home/wangjianfeng/working/imagenet2012/edgebox/train_special_images/'
out_folder = '~/working/imagenet2012/edgebox/train/';

% add path
addpath(genpath('~/code/mycode/tools/pdollar_toolbox'));
addpath(edge_box_path);
run('~/code/mycode/matlab_code/jf_conf.m');

% file list
list = importdata(file_list);

model = load([edge_box_path 'models/forest/modelBsds']); model=model.model;
model.opts.multiscale=0; model.opts.sharpen=2; model.opts.nThreads=4;
%% set up opts for edgeBoxes (see edgeBoxes.m)
opts = edgeBoxes;
opts.alpha = .65;     % step size of sliding window search
opts.beta  = .75;     % nms threshold for object proposals
opts.minScore = .01;  % min score of boxes to detect
opts.maxBoxes = 1e4;  % max number of boxes to detect

from_idx = max(1, idx_start);
to_idx = min(idx_end, numel(list.data));
numel(list.data)
tic;
%% detect Edge Box bounding box proposals (see edgeBoxes.m)
for i = from_idx : to_idx
    label = list.data(i);

    im_name = list.textdata{i};
    full_im_name = [image_folder im_name];
    save_file_name = [out_folder im_name '.edgebox.txt'];
    binary_save_file_name = [out_folder im_name '.seq.edgebox.short.bin'];
    %if exist(binary_save_file_name, 'file')
        %continue;
    %end
    %fprintf('running %s\n', im_name);
    %if ~exist(save_file_name, 'file')
    is_read = false;
    try
        I = imread(full_im_name);
        is_read = true;
    catch
        is_read = false;
    end
    if ~is_read
        try
            I = imread([special_image_folder im_name]);
            is_read = true;
        catch
            is_read = false;
        end
    end
    if ~is_read
        fprintf('failed read image: %s\n', im_name);
        continue;
    end
    if size(I, 3) == 1
        I = repmat(I, [1, 1, 3]);
    end
    bbs=edgeBoxes(I,model,opts);
    %else
        %fprintf('load\n');
        %boxes = importdata(save_file_name, '\t', 1);
        %if ~isstruct(boxes)
            %bbs = [];
        %else
            %bbs = boxes.data;
        %end
    %end
    if toc > 10
        fprintf('%s\t%d / %d\n', im_name, i, to_idx);
        tic;
    end

    if numel(bbs) == 0
        save_mat([], binary_save_file_name, 'int16');
    else
        save_mat([(0 : size(bbs, 1) - 1)', bbs(:, 1 : 4)]', binary_save_file_name, 'int16');
    end
end
exit;

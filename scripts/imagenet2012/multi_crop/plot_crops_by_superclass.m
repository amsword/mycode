image_folder = '/home/wangjianfeng/data/imagenet2012/image_256xN/train/';
backup_image_folder = '/home/wangjianfeng/data/imagenet2012/image_256xN/train_special_images/'

ranking_folder = '~/working/imagenet2012/edgebox/train_alex_superclass/';
cropped_image_folder_parent = '/home/wangjianfeng/working/imagenet2012/edgebox/train_alex_superclass_visualize/';
file_list = '/home/wangjianfeng/data/imagenet2012/image_list/train.txt';
out_folder = '/home/wangjianfeng/working/imagenet2012/edgebox/train/';

run('/home/wangjianfeng/code/mycode/matlab_code/jf_conf.m');
% get file list
list = importdata(file_list);

%for i = 1 : numel(list.data)
for i = 2000 : 2010 
    % if the label is not the first 5, ignore it. 
    label = list.data(i);

    % read the image
    im_name = list.textdata{i};
    full_im_name = [image_folder im_name];
    try
        I = imread([image_folder im_name]);
        is_ok = true;
    catch
        is_ok = false;
    end
    if ~is_ok
        I = imread([backup_image_folder im_name]);
        is_ok = true;
    end
    assert(is_ok);
    if size(I, 3) == 1
        I = repmat(I, [1 1 3]);
    end

    % read sorted crops
    fprintf('%s\t%d\n', im_name, i);
    final_bb_file = [ranking_folder im_name '.final.bb.short.bin'];
    bbs = read_mat(final_bb_file, 'int16');

    cropped_image_folder = [cropped_image_folder_parent im_name];
    if ~exist(cropped_image_folder, 'file')
        mkdir(cropped_image_folder);
    end

    num_saved = size(bbs, 2);
    for j = 1 : num_saved 
        save_file_name = [cropped_image_folder '/' num2str(j) '.jpg'];
        bb = bbs(2 : end, j);
        cI = I;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1), :) = 0;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1), 1) = 255;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1) + bb(3) - 1, 1) = 255;
        cI(bb(2), bb(1) : bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2), bb(1) : bb(1) + bb(3) - 1, 1) = 255;
        cI(bb(2) + bb(4) - 1, bb(1) : bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2) + bb(4) - 1, bb(1) : bb(1) + bb(3) - 1, 1) = 255;
        imwrite(cI, save_file_name);
    end
end


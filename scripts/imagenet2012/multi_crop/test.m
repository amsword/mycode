%run('~/code/mycode/matlab_code/jf_conf.m');
x = rand(1000, 1000);

txt_file = '/tmp/x.txt';
fp = fopen(txt_file, 'w');
for i = size(x, 1)
    for j = size(x, 2)
        fprintf(fp, '%f\t', x(i, j));
    end
    fprintf(fp, '\n');
end
fclose(fp);

bin_file = '/tmp/x.bin';
save_mat(x, bin_file, 'float');

tic;
x = read_mat(bin_file, 'float');
toc;

tic;
x = importdata(txt_file);
toc;

python_tools = "/home/wangjianfeng/code/mycode/python_code/";
import sys;
sys.path.append(python_tools);
from com_utility import throw_cmd;

idx_start = 710000;
idx_end = 730000;

all_machine_id = ['01', '04', '05', '06', '07', '09', '12', '13', '14'];
all_machines2 = ['deep' + num for num in all_machine_id];
all_machines = [];
for name in all_machines2:
    all_machines.extend([name] * 6);


for machine_name in all_machines:
    cmd = 'pwd'
    throw_cmd(machine_name, cmd, '');

num_batch = len(all_machines);
batch_size = (idx_end - idx_start + 1) / num_batch;
batch_size = int(batch_size + 1);

all_procs = [None] * num_batch;
for idx_batch in range(num_batch):
    batch_start = idx_batch * batch_size + idx_start;
    batch_end = batch_start + batch_size - 1;
    batch_end = min(batch_end, idx_end);

    cmd = "/home/weiyunchao/matlabR2013a/bin/matlab -c /home/weiyunchao/license.lic -nodisplay -nodesktop -r " + \
            "'add_seq_to_edge_box(" + str(batch_start) + \
            ',' + str(batch_end) + ")'";

    log_file_name = '/home/wangjianfeng/working/imagenet2012/edgebox/' + \
            all_machines[idx_batch] + str(idx_batch);

    all_procs[idx_batch] = throw_cmd(all_machines[idx_batch], \
            cmd, log_file_name); 

for p in all_procs:
    if p is not None:
        p.communicate();

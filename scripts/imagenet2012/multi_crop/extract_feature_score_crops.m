idx_start, idx_end, device_id

net_def = 'google_deploy.prototxt';
pretrained = '/home/wangjianfeng/data/google_model/bvlc_googlenet.caffemodel';

caffe_input_size = 224;
caffe_batch_size = 128;
num_label_used = 5;
image_folder = '~/data/imagenet2012/image_256xN/train/';
file_list = '~/data/imagenet2012/image_list/train.txt';
out_folder = '~/working/imagenet2012/edgebox/train/';

% add path of caffe
caffe_matlab_dir = '~/code/caffe_new/caffe/matlab/caffe/';
addpath(caffe_matlab_dir);

caffe('reset');
% initialize the network
caffe('set_mode_gpu');
caffe('set_phase_test');
caffe('set_device', device_id);
caffe('init', net_def, pretrained);

% get file list
list = importdata(file_list);

caffe_blob = zeros(caffe_input_size, caffe_input_size, 3, ...
        caffe_batch_size, 'single');
caffe_blob_idx = 1;

for i = idx_start : min(numel(list.data), idx_end);
    tic;
    assert(caffe_blob_idx == 1);
    % if the label is not the first 5, ignore it. 
    label = list.data(i);
    if label >= num_label_used 
        continue;
    end

    im_name = list.textdata{i};
    % get the image 
    full_im_name = [image_folder im_name];
    edgebox_file = [out_folder im_name '.edgebox.txt'];
    googlenet_score = [edgebox_file '.googlenet.score'];
    if exist(googlenet_score, 'file')
        continue;
    end

    I = imread(full_im_name);
    if size(I, 3) == 1
        I = repmat(I, [1 1 3]);
    end

    % get crop region list
    fprintf('%s\t%d\n', im_name, i);
    boxes = importdata(edgebox_file, '\t', 1);
    if ~isstruct(boxes)
        continue;
    end
    
    % crop the image and get the region
    num_box = size(boxes.data, 1);
    all_entropy = zeros(num_box, 1);
    time_cost.header = toc;
    time_cost.prepare_data = 0;
    time_cost.caffe_forward = 0;
    for j = 1 : num_box 
        tic;
        % change the box, so that it is a square, and then we resize it 
        bb = boxes.data(j, 1 : 4);
        if bb(3) < bb(4)
            extra = (bb(4) - bb(3)) / 2;
            bb(2) = bb(2) + extra;
            bb(4) = bb(3);
        else
            extra = (bb(3) - bb(4)) / 2;
            bb(1) = bb(1) + extra;
            bb(3) = bb(4);
        end

        % get the crop and resize it to target
        crop = imcrop(I, bb);
        assert(size(crop, 1) == size(crop, 2));
        crop = imresize(crop, [caffe_input_size, caffe_input_size]);
        assert(size(crop, 1) == size(crop, 2) && ...
            size(crop, 1) == caffe_input_size);

        % convert to bgr, which is required by caffe
        crop = single(crop(:, :, [3, 2, 1]));
        % with is the fastest
        crop = permute(crop, [2, 1, 3]);
        caffe_blob(:, :, :, caffe_blob_idx) = crop;
        caffe_blob_idx = caffe_blob_idx + 1;
        time_cost.prepare_data = time_cost.prepare_data + toc;
        if caffe_blob_idx == caffe_batch_size + 1
            tic;
            caffe_result = caffe('forward', {caffe_blob});
            time_cost.caffe_forward = time_cost.caffe_forward + toc;

            confidence = squeeze(caffe_result{1});
            confidence = confidence(1 : num_label_used, :);
            confidence = bsxfun(@rdivide, confidence, sum(confidence, 1));
            entropy = -sum(confidence .* log(confidence), 1);
            all_entropy(j - caffe_batch_size + 1 : j) = entropy;
            caffe_blob_idx = 1;
        end
    end

    if caffe_blob_idx ~= 1
        tic;
        caffe_result = caffe('forward', {caffe_blob});
        time_cost.caffe_forward = time_cost.caffe_forward + toc;
        confidence = squeeze(caffe_result{1});
        confidence = confidence(1 : num_label_used, :);
        confidence = bsxfun(@rdivide, confidence, sum(confidence, 1));
        entropy = -sum(confidence .* log(confidence), 1);
        entropy = entropy(1 : caffe_blob_idx - 1);
        all_entropy(end - numel(entropy) + 1 : end) = entropy;
        caffe_blob_idx = 1;
    end

    dlmwrite(googlenet_score, all_entropy);
    time_cost
end


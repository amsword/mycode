function generate_edge_box(idx_start, idx_end)

edge_box_path = '~/code/mycode/tools/edge_box/';
file_list = '~/data/imagenet2012_temp/image_list/train.txt';
image_folder = '~/data/imagenet2012_temp/image_256xN/train/';
out_folder = '~/working/imagenet2012/edgebox/train/';

% add path
addpath(genpath('~/code/mycode/tools/pdollar_toolbox'));
addpath(edge_box_path);

% file list
list = importdata(file_list);

model = load([edge_box_path 'models/forest/modelBsds']); model=model.model;
model.opts.multiscale=0; model.opts.sharpen=2; model.opts.nThreads=4;
%% set up opts for edgeBoxes (see edgeBoxes.m)
opts = edgeBoxes;
opts.alpha = .65;     % step size of sliding window search
opts.beta  = .75;     % nms threshold for object proposals
opts.minScore = .01;  % min score of boxes to detect
opts.maxBoxes = 1e4;  % max number of boxes to detect

from_idx = max(1, idx_start);
to_idx = min(idx_end, numel(list.data));
numel(list.data)
%% detect Edge Box bounding box proposals (see edgeBoxes.m)
matlabpool open 6
parfor i = from_idx : to_idx
    label = list.data(i);

    im_name = list.textdata{i};
    full_im_name = [image_folder im_name];
    save_file_name = [out_folder im_name '.edgebox.txt'];
    is_run = true;
    if exist(save_file_name, 'file')
        boxes = importdata(save_file_name, '\t', 1);
        if ~isstruct(boxes)
            is_run = false;
        else
            textdata = strsplit(boxes.textdata{1});
            if str2num(textdata{1}) == size(boxes.data, 1)
                is_run = false;
            end
        end
    end
    if ~is_run
        continue;
    end
    fprintf('%s\t%d\n', im_name, i);
    try
        I = imread(full_im_name);
        if size(I, 3) == 1
            I = repmat(I, [1, 1, 3]);
        end
        bbs=edgeBoxes(I,model,opts);
        [pathstr] = fileparts(save_file_name);
        if ~exist(pathstr, 'file')
            mkdir(pathstr);
        end
        fp = fopen(save_file_name, 'w');
        s = size(bbs);
        fprintf(fp, '%d\t%d\n', s(1), s(2)); 
        for j = 1 : s(1)
            for k = 1 : s(2) - 1
                fprintf(fp, '%d\t', bbs(j, k));
            end
            fprintf(fp, '%f\n', bbs(j, end));
        end
        fclose(fp);
    end
end
matlabpool close;

python_tools = "/home/wangjianfeng/code/mycode/python_code/";
import sys;
sys.path.append(python_tools);
from com_utility import throw_cmd;
import time;

    # idx_start <= i < idx_end 
def DistributeFor(idx_start, 
        idx_end, 
        all_resources, 
        func):
    assert idx_end > idx_start;
    num_resources = len(all_resources);
    chunck_size = (idx_end - idx_start) / 2 / num_resources;
    chunck_size = int(chunck_size);
    all_procs = [None] * num_resources;
    allocated = 0;
    while True:
        if allocated >= idx_end - idx_start:
            break; 
        for idx_resource in range(num_resources):
            sub_proc = all_procs[idx_resource];
            if sub_proc is not None and sub_proc.poll() is None:
                continue;
            if allocated >= idx_end - idx_start:
                break; 
            sub_idx_start = idx_start + allocated;
            sub_idx_end = sub_idx_start + chunck_size;
            sub_idx_end = min(sub_idx_end, idx_end);
            resourse = all_resources[idx_resource];
            print 'from ', sub_idx_start, \
                    ' to ', sub_idx_end, \
                    ' by ', resourse;
            all_procs[idx_resource] = func(sub_idx_start, sub_idx_end, 
                    resourse);
            time.sleep(1);
            allocated = allocated + sub_idx_end - sub_idx_start;
        time.sleep(60);
    for p in all_procs:
        if p is not None:
            p.communicate();

def run_instance(batch_start, batch_end, resources):
    (machine_name, device_id) = resources;
    cmd = "/home/weiyunchao/matlabR2013a/bin/matlab -c /home/weiyunchao/license.lic -nodisplay -nodesktop -r " + \
            "'rank_crops_by_superclass(" + str(batch_start) + \
            ',' + str(batch_end) + ', ' + str(device_id) + ")'";

    log_file_name = '/home/wangjianfeng/working/imagenet2012/edgebox/' + \
            str(batch_start) + "_" + str(batch_end) + "_3";

    p = throw_cmd(machine_name, cmd, log_file_name); 
    return p;

idx_start = 974970;# from 974970 to 1267463
idx_end = 1267463;

all_device_id = [];
all_machine_id = [];
all_machine_id.extend(['07'] * 8);

#all_device_id.extend([-1] * 12);
all_machines = ['deep' + num for num in all_machine_id];
all_resources = [(all_machines[i], all_device_id[i]) for i in range(len(all_machines))]
assert len(all_device_id) == len(all_machines);
DistributeFor(idx_start, idx_end, all_resources, run_instance);


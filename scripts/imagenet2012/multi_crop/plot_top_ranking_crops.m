% run
num_label_used = 5;
image_folder = '~/data/imagenet2012/image_256xN/train/';
file_list = '~/data/imagenet2012/image_list/train.txt';
out_folder = '~/working/imagenet2012/edgebox/train/';

% get file list
list = importdata(file_list);

for i = 1 : numel(list.data)
    % if the label is not the first 5, ignore it. 
    label = list.data(i);
    if label >= num_label_used 
        continue;
    end

    im_name = list.textdata{i};
    % get the image 
    full_im_name = [image_folder im_name];

    % get crop region list
    im_name = list.textdata{i};
    fprintf('%s\t%d\n', im_name, i);
    edgebox_file = [out_folder im_name '.edgebox.txt'];
    googlenet_score = [edgebox_file '.googlenet.score'];
    cropped_image_folder = [googlenet_score '.crop_im/'];
    saved_entropy_file = [cropped_image_folder 'entropy.txt'];
    if exist(saved_entropy_file, 'file')
        continue;
    end

    if ~exist(edgebox_file, 'file')
        continue;
    end
    boxes = importdata(edgebox_file, '\t', 1);
    if ~isstruct(boxes)
        continue;
    end
    
    try
        all_entropy = importdata(googlenet_score);
    catch
        continue;
    end
    [~, s_idx] = sort(all_entropy, 'ascend');

    if ~exist(cropped_image_folder, 'file')
        mkdir(cropped_image_folder);
    end

    num_saved = min(size(boxes.data, 1), 10);
    saved_entropy = zeros(num_saved, 1);
    I = imread(full_im_name);
    if size(I, 3) == 1
        I = repmat(I, [1 1 3]);
    end
    for j = 1 : num_saved 
        s = all_entropy(s_idx(j));
        saved_entropy(j) = s;
        save_file_name = [cropped_image_folder num2str(j) '.jpg'];
        bb = boxes.data(s_idx(j), 1 : 4);
        cI = I;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1), :) = 0;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1), 1) = 255;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2) : bb(2) + bb(4) - 1, bb(1) + bb(3) - 1, 1) = 255;
        cI(bb(2), bb(1) : bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2), bb(1) : bb(1) + bb(3) - 1, 1) = 255;
        cI(bb(2) + bb(4) - 1, bb(1) : bb(1) + bb(3) - 1, :) = 0;
        cI(bb(2) + bb(4) - 1, bb(1) : bb(1) + bb(3) - 1, 1) = 255;
        imwrite(cI, save_file_name);
    end
    dlmwrite(saved_entropy_file, saved_entropy);


end


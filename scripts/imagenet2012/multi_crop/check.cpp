#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <omp.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace std;
template <class type>
class Matrix {
public:
    type* operator [] (size_t idx) {
       return vec_data_.data() + idx * num_cols_; 
    }
    int Rows() {
        return num_rows_;
    }
    int Cols() {
        return num_cols_;
    }
    void Load(const string &file_name) {
        FILE* fp = fopen(file_name.c_str(), "rb");
        if (!fp) {
            vec_data_.clear();
            num_rows_ = -1;
            num_cols_ = -1;
            return;
        }
        fread(&num_rows_, sizeof(int), 1, fp);
        fread(&num_cols_, sizeof(int), 1, fp);
        if (num_rows_ == 0 || num_cols_ == 0) {
            vec_data_.clear();
            num_rows_ = 0;
            num_cols_ = 0;
            fclose(fp);
            return;
        }

        vec_data_.resize(num_rows_ * (long long)num_cols_);
        fread(vec_data_.data(), sizeof(type), vec_data_.size(), fp);
        fclose(fp);
    }

private:
    vector<type> vec_data_;
    int num_rows_;
    int num_cols_;
};
// check which image has no edge box file
int which_has_no_edge_box() {
    string str_image_list = "/home/wangjianfeng/data/imagenet2012_temp/image_list/train.txt";
    string str_edge_box_folder = "/home/wangjianfeng/working/imagenet2012/edgebox/train/";

    // read image list;
    vector<string> vec_image_files;
    ifstream fin(str_image_list.c_str());
    string file_name; 
    int label;
    while(fin >> file_name >> label) {
        vec_image_files.push_back(file_name);
    }
    fin.close();
    
    cout << "finished loading file names\n";

    clock_t begin = clock();
    // check each image;
    for (int i = 0; i < vec_image_files.size(); i++) {
        //string full_file_name = str_edge_box_folder + 
            //vec_image_files[i] + ".seq.edgebox.short.bin";
        string full_file_name = str_edge_box_folder + 
            vec_image_files[i] + ".edgebox.txt";
        ifstream fin(full_file_name.c_str());
        if (!fin.good()) {
            cout << vec_image_files[i] << "\n";
        }
        clock_t end = clock();
        if (end - begin > 2 * CLOCKS_PER_SEC) {
            cout << "checked: " << i << "\n";
            begin = end;
        }
    }
    return 0;
}
// check whether opencv can read some files;
int main2()
{
    string image_file_name = "/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/n03062245/n03062245_4620.JPEG";
    cv::Mat im = cv::imread(image_file_name, CV_LOAD_IMAGE_COLOR);
    cout << im.rows << "\t" << im.cols << "\n";
    return 0;
}

int main1()
{

    // check whether opencv can read some files;
    
    
    return 0;
}

void generate_readable_image() {
    string str_folder_org = "/home/wangjianfeng/data/imagenet2012/image_256xN/train/";
    string str_folder_new = "/home/wangjianfeng/working/imagenet2012/edgebox/train_special_images/";
    string str_image_list = "/home/wangjianfeng/working/imagenet2012/edgebox/special_images.txt";

    // read image list;
    vector<string> vec_image_list;
    ifstream fin(str_image_list.c_str());
    string line;
    while (fin >> line) {
        vec_image_list.push_back(line);
    }

    for (size_t i = 0; i < vec_image_list.size(); i++) {
        string name = vec_image_list[i];
        string file_name_org = str_folder_org + name;
        string file_name_new = str_folder_new + name;
        cv::Mat im = cv::imread(file_name_org, CV_LOAD_IMAGE_COLOR);
        if (im.rows <= 0 || im.cols <= 0) {
            cout << "error\n";
            break;
        }
        cv::imwrite(file_name_new, im);
    }
}

void generate_image_size() {
    string str_image_folder = "/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/";
    string str_image_list = "/home/wangjianfeng/data/imagenet2012_temp/image_list/train.txt";
    string str_save_file = "/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train_file_size.int.bin";

    // read image list;
    vector<string> vec_image_list;
    ifstream fin(str_image_list.c_str());
    string line;
    int label;
    while (fin >> line >> label) {
        vec_image_list.push_back(line);
    }

    cout << vec_image_list.size() << "\n";
    vector<pair<int, int> > vec_pair(vec_image_list.size());
    int count = 0;
#pragma omp parallel for
    for (int i = 0; i < vec_image_list.size(); i++) {
#pragma omp atomic
        count++;

        if ((count % 10000) == 0) {
            cout << count << "\t" << omp_get_thread_num() << "\n";
        }
        string name = vec_image_list[i];
        string file_name = str_image_folder + name;
        cv::Mat im = cv::imread(file_name);
        if (im.rows <= 0 || im.cols <= 0) {
            cerr << "error\n";
            exit(0);
        }
        vec_pair[i] = pair<int, int>(im.rows, im.cols);
    }

    FILE* fp = fopen(str_save_file.c_str(), "wb");
    int num_rows = vec_pair.size();
    int num_cols = 2;
    fwrite(&num_rows, sizeof(int), 1, fp);
    fwrite(&num_cols, sizeof(int), 1, fp);
    for (int i = 0; i < num_rows; i++) {
        pair<int, int> &p = vec_pair[i];
        fwrite(&p.first, sizeof(int), 1, fp);
        fwrite(&p.second, sizeof(int), 1, fp);
    }
    fclose(fp);
}

void crop_size_info() {
    string str_image_folder = "/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train/";
    string str_image_list = "/home/wangjianfeng/data/imagenet2012_temp/image_list/train.txt";
    string str_save_file = "/home/wangjianfeng/data/imagenet2012_temp/image_256xN/train_file_size.int.bin";
    string str_edge_box_folder = "/home/wangjianfeng/working/imagenet2012/edgebox/train/";

    // read image list;
    vector<string> vec_image_list;
    ifstream fin(str_image_list.c_str());
    string line;
    int label;
    while (fin >> line >> label) {
        vec_image_list.push_back(line);
    }
    
    // read image size;
    vector<pair<int, int> > vec_pair;
    FILE* fp = fopen(str_save_file.c_str(), "rb");
    int num_rows = 0;
    int num_cols = 2;
    fread(&num_rows, sizeof(int), 1, fp);
    fread(&num_cols, sizeof(int), 1, fp);
    vec_pair.resize(num_rows);
    for (int i = 0; i < num_rows; i++) {
        pair<int, int> &p = vec_pair[i];
        fread(&p.first, sizeof(int), 1, fp);
        fread(&p.second, sizeof(int), 1, fp);
    }
    fclose(fp);

    vector<vector<size_t> > vecvec_count(vec_image_list.size());
    for (size_t i = 0; i < vec_image_list.size(); i++) {
        vecvec_count[i].resize(100, 0);
    }
    vector<vector<size_t> > vecvec_ratio_count(vec_image_list.size());
    for (size_t i = 0; i < vecvec_ratio_count.size(); i++) {
        vecvec_ratio_count[i].resize(100, 0);
    }
    int processed = 0;
    int no_file = 0;
    int no_edge_box = 0;
    int uncorrect_size = 0;
#pragma omp parallel for
    for (int i = 0; i < vec_image_list.size(); i++) {
        string &base_name = vec_image_list[i];
        string edge_box_name = str_edge_box_folder + base_name + ".seq.edgebox.short.bin" ;
        Matrix<short> mat;
        mat.Load(edge_box_name);
        if (mat.Rows() == -1) {
            cout << "no edge box" << base_name << "\t" << i << "\t" << omp_get_thread_num() << "\n";
#pragma omp atomic
            no_file++;
            continue;
        } else if (mat.Rows() == 0) {
#pragma omp atomic
            no_edge_box++;

            continue;
        }
        if (mat.Cols() != 5) {
            cout << "wrong colum number: " << base_name << "\t" << i << "\t"
                << mat.Cols() << "\n";
#pragma omp atomic
            uncorrect_size++;
        }
        //cout << mat.Rows() << "\t";
        int image_size = vec_pair[i].first * vec_pair[i].second;
#pragma omp atomic
        processed++;

        if ((processed % 100000) == 0) {
            cout << processed << "\t" << uncorrect_size << "\n";
        }

        vector<size_t> &vec_count = vecvec_count[i];
        vector<size_t> &vec_ratio_count = vecvec_ratio_count[i];
        for (int j = 0; j < mat.Rows(); j++) {
            if (mat[j][0] != j) {
                cout << "uncorrect id: " << base_name << "\t" 
                    << i << "\t" << j << "\n";
            }
            double coef = mat[j][3] * mat[j][4] / (double)image_size;
            if (coef > 1) {
                cout << "coef: " << coef << "\n";
            }
            coef *= 100;
            int idx = (int)coef;
            idx = min(99, idx);
            vec_count[idx]++;

            coef = min(mat[j][3], mat[j][4]) / (double)max(mat[j][3], mat[j][4]);
            coef *= 100;
            idx = (int)coef;
            idx = min(99, idx);
            vec_ratio_count[idx]++;
        }
    }
    cout << "no file: " << no_file << "\t"
        << "no edge box: " << no_edge_box << "\n";
    vector<size_t> vec_count(100, 0);
    for (size_t i =  0; i < 100; i++) {
        for (size_t j = 0; j < vec_image_list.size(); j++) {
            vec_count[i] += vecvec_count[j][i]; 
        }
    }

    vector<size_t> vec_ratio_count(100, 0);
    for (size_t i = 0; i < 100; i++) {
        for (size_t j = 0; j < vec_image_list.size(); j++) {
            vec_ratio_count[i] += vecvec_ratio_count[j][i];
        }
    }
    cout << "size ratio:\n";
    for (size_t i = 0; i < 100; i++) {
        cout << i << "\t" << vec_count[i] << "\n";
    }
    cout << "aspect ratio: \n";
    for (size_t i = 0; i < 100; i++) {
        cout << i << "\t" << vec_ratio_count[i] << "\n";
    }

    cout << "\n";
}

int main()
{
    //generate_readable_image();
    //generate_image_size();
    crop_size_info();
    return 0;
}

I0812 20:00:08.011317 25722 train_net.cpp:25] Starting Optimization
I0812 20:00:08.011553 25722 solver.cpp:38] Initializing solver from parameters: 
train_net: "imagenet_train_raw_cross_relu.prototxt"
base_lr: 0.01
display: 20
max_iter: 10
lr_policy: "step"
gamma: 0.1
momentum: 0.9
weight_decay: 0.0005
stepsize: 100000
snapshot: 10000
snapshot_prefix: "caffe_imagenet_train_raw"
device_id: 2
random_seed: 1
I0812 20:00:08.295384 25722 solver.cpp:58] Creating training net from file: imagenet_train_raw_cross_relu.prototxt
I0812 20:00:08.295935 25722 net.cpp:39] Initializing net from parameters: 
name: "CaffeNet"
layers {
  top: "data"
  top: "label"
  name: "data"
  type: DATA
  data_param {
    source: "/home/jianfeng/data/imagenet2012/imagenet_val_lmdb"
    mean_file: "/DATA/laihanj/imagenet-train-mean"
    batch_size: 128
    crop_size: 227
    backend: LMDB
  }
}
layers {
  bottom: "data"
  top: "conv1"
  name: "conv1"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 96
    kernel_size: 11
    stride: 4
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "conv1"
  top: "conv1"
  name: "relu1"
  type: RELU
}
layers {
  bottom: "conv1"
  top: "pool1"
  name: "pool1"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool1"
  top: "norm1"
  name: "norm1"
  type: LRN
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layers {
  bottom: "norm1"
  top: "conv2"
  name: "conv2"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 256
    pad: 2
    kernel_size: 5
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv2"
  top: "conv2"
  name: "relu2"
  type: RELU
}
layers {
  bottom: "conv2"
  top: "pool2"
  name: "pool2"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool2"
  top: "norm2"
  name: "norm2"
  type: LRN
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layers {
  bottom: "norm2"
  top: "conv3"
  name: "conv3"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "conv3"
  top: "conv3"
  name: "relu3"
  type: RELU
}
layers {
  bottom: "conv3"
  top: "conv4"
  name: "conv4"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv4"
  top: "conv4"
  name: "relu4"
  type: RELU
}
layers {
  bottom: "conv4"
  top: "conv5"
  name: "conv5"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv5"
  top: "conv5"
  name: "relu5"
  type: RELU
}
layers {
  bottom: "conv5"
  top: "pool5"
  name: "pool5"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool5"
  top: "fc6"
  name: "fc6"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "fc6"
  top: "fc6"
  name: "relu6"
  type: RELU
}
layers {
  bottom: "fc6"
  top: "fc6"
  name: "drop6"
  type: DROPOUT
  dropout_param {
    dropout_ratio: 0.5
  }
}
layers {
  bottom: "fc6"
  top: "fc7"
  name: "fc7"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "fc7"
  top: "fc7"
  name: "relu7"
  type: RELU
}
layers {
  bottom: "fc7"
  top: "fc7"
  name: "drop7"
  type: DROPOUT
  dropout_param {
    dropout_ratio: 0.5
  }
}
layers {
  bottom: "fc7"
  top: "fc8"
  name: "fc8"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 1000
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "fc8"
  bottom: "label"
  top: "each_loss"
  name: "loss"
  type: SOFTMAX_LOSS
}
I0812 20:00:08.296064 25722 data_layer.cpp:530] NO decoding
I0812 20:00:08.296075 25722 net.cpp:67] Creating Layer data
I0812 20:00:08.296082 25722 net.cpp:291] data -> data
I0812 20:00:08.296098 25722 net.cpp:291] data -> label
I0812 20:00:08.296185 25722 data_layer.cpp:268] Opening lmdb /home/jianfeng/data/imagenet2012/imagenet_val_lmdb
I0812 20:00:08.296308 25722 data_layer.cpp:318] start decode: 196608
I0812 20:00:08.296316 25722 data_layer.cpp:320] end decode: 196608
I0812 20:00:08.296324 25722 data_layer.cpp:360] output data size: 128,3,227,227
I0812 20:00:08.296330 25722 data_layer.cpp:388] Loading mean file from/DATA/laihanj/imagenet-train-mean
I0812 20:00:08.454049 25722 net.cpp:84] Top shape: 128 3 227 227 (19787136)
I0812 20:00:08.454092 25722 net.cpp:84] Top shape: 128 1 1 1 (128)
I0812 20:00:08.454099 25722 net.cpp:126] data does not need backward computation.
I0812 20:00:08.454114 25722 net.cpp:67] Creating Layer conv1
I0812 20:00:08.454123 25722 net.cpp:330] conv1 <- data
I0812 20:00:08.454139 25722 net.cpp:291] conv1 -> conv1
I0812 20:00:08.455741 25722 net.cpp:84] Top shape: 128 96 55 55 (37171200)
I0812 20:00:08.455759 25722 net.cpp:121] conv1 needs backward computation.
I0812 20:00:08.455765 25722 net.cpp:67] Creating Layer relu1
I0812 20:00:08.455770 25722 net.cpp:330] relu1 <- conv1
I0812 20:00:08.455775 25722 net.cpp:281] relu1 -> conv1 (in-place)
I0812 20:00:08.455782 25722 net.cpp:84] Top shape: 128 96 55 55 (37171200)
I0812 20:00:08.455787 25722 net.cpp:121] relu1 needs backward computation.
I0812 20:00:08.455792 25722 net.cpp:67] Creating Layer pool1
I0812 20:00:08.455796 25722 net.cpp:330] pool1 <- conv1
I0812 20:00:08.455801 25722 net.cpp:291] pool1 -> pool1
I0812 20:00:08.455814 25722 net.cpp:84] Top shape: 128 96 27 27 (8957952)
I0812 20:00:08.455818 25722 net.cpp:121] pool1 needs backward computation.
I0812 20:00:08.455826 25722 net.cpp:67] Creating Layer norm1
I0812 20:00:08.455829 25722 net.cpp:330] norm1 <- pool1
I0812 20:00:08.455834 25722 net.cpp:291] norm1 -> norm1
I0812 20:00:08.455842 25722 net.cpp:84] Top shape: 128 96 27 27 (8957952)
I0812 20:00:08.455847 25722 net.cpp:121] norm1 needs backward computation.
I0812 20:00:08.455853 25722 net.cpp:67] Creating Layer conv2
I0812 20:00:08.455857 25722 net.cpp:330] conv2 <- norm1
I0812 20:00:08.455863 25722 net.cpp:291] conv2 -> conv2
I0812 20:00:08.464851 25722 net.cpp:84] Top shape: 128 256 27 27 (23887872)
I0812 20:00:08.464877 25722 net.cpp:121] conv2 needs backward computation.
I0812 20:00:08.464886 25722 net.cpp:67] Creating Layer relu2
I0812 20:00:08.464891 25722 net.cpp:330] relu2 <- conv2
I0812 20:00:08.464900 25722 net.cpp:281] relu2 -> conv2 (in-place)
I0812 20:00:08.464908 25722 net.cpp:84] Top shape: 128 256 27 27 (23887872)
I0812 20:00:08.464912 25722 net.cpp:121] relu2 needs backward computation.
I0812 20:00:08.464917 25722 net.cpp:67] Creating Layer pool2
I0812 20:00:08.464921 25722 net.cpp:330] pool2 <- conv2
I0812 20:00:08.464926 25722 net.cpp:291] pool2 -> pool2
I0812 20:00:08.464936 25722 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0812 20:00:08.464939 25722 net.cpp:121] pool2 needs backward computation.
I0812 20:00:08.464965 25722 net.cpp:67] Creating Layer norm2
I0812 20:00:08.464970 25722 net.cpp:330] norm2 <- pool2
I0812 20:00:08.464975 25722 net.cpp:291] norm2 -> norm2
I0812 20:00:08.464983 25722 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0812 20:00:08.464987 25722 net.cpp:121] norm2 needs backward computation.
I0812 20:00:08.464995 25722 net.cpp:67] Creating Layer conv3
I0812 20:00:08.464999 25722 net.cpp:330] conv3 <- norm2
I0812 20:00:08.465005 25722 net.cpp:291] conv3 -> conv3
I0812 20:00:08.489555 25722 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0812 20:00:08.489585 25722 net.cpp:121] conv3 needs backward computation.
I0812 20:00:08.489593 25722 net.cpp:67] Creating Layer relu3
I0812 20:00:08.489599 25722 net.cpp:330] relu3 <- conv3
I0812 20:00:08.489608 25722 net.cpp:281] relu3 -> conv3 (in-place)
I0812 20:00:08.489615 25722 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0812 20:00:08.489620 25722 net.cpp:121] relu3 needs backward computation.
I0812 20:00:08.489629 25722 net.cpp:67] Creating Layer conv4
I0812 20:00:08.489632 25722 net.cpp:330] conv4 <- conv3
I0812 20:00:08.489639 25722 net.cpp:291] conv4 -> conv4
I0812 20:00:08.508185 25722 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0812 20:00:08.508210 25722 net.cpp:121] conv4 needs backward computation.
I0812 20:00:08.508219 25722 net.cpp:67] Creating Layer relu4
I0812 20:00:08.508224 25722 net.cpp:330] relu4 <- conv4
I0812 20:00:08.508232 25722 net.cpp:281] relu4 -> conv4 (in-place)
I0812 20:00:08.508240 25722 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0812 20:00:08.508244 25722 net.cpp:121] relu4 needs backward computation.
I0812 20:00:08.508252 25722 net.cpp:67] Creating Layer conv5
I0812 20:00:08.508256 25722 net.cpp:330] conv5 <- conv4
I0812 20:00:08.508262 25722 net.cpp:291] conv5 -> conv5
I0812 20:00:08.520714 25722 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0812 20:00:08.520743 25722 net.cpp:121] conv5 needs backward computation.
I0812 20:00:08.520751 25722 net.cpp:67] Creating Layer relu5
I0812 20:00:08.520756 25722 net.cpp:330] relu5 <- conv5
I0812 20:00:08.520764 25722 net.cpp:281] relu5 -> conv5 (in-place)
I0812 20:00:08.520772 25722 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0812 20:00:08.520776 25722 net.cpp:121] relu5 needs backward computation.
I0812 20:00:08.520782 25722 net.cpp:67] Creating Layer pool5
I0812 20:00:08.520786 25722 net.cpp:330] pool5 <- conv5
I0812 20:00:08.520792 25722 net.cpp:291] pool5 -> pool5
I0812 20:00:08.520800 25722 net.cpp:84] Top shape: 128 256 6 6 (1179648)
I0812 20:00:08.520804 25722 net.cpp:121] pool5 needs backward computation.
I0812 20:00:08.520817 25722 net.cpp:67] Creating Layer fc6
I0812 20:00:08.520820 25722 net.cpp:330] fc6 <- pool5
I0812 20:00:08.520827 25722 net.cpp:291] fc6 -> fc6
I0812 20:00:09.534863 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.534909 25722 net.cpp:121] fc6 needs backward computation.
I0812 20:00:09.534919 25722 net.cpp:67] Creating Layer relu6
I0812 20:00:09.534924 25722 net.cpp:330] relu6 <- fc6
I0812 20:00:09.534931 25722 net.cpp:281] relu6 -> fc6 (in-place)
I0812 20:00:09.534940 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.534943 25722 net.cpp:121] relu6 needs backward computation.
I0812 20:00:09.534948 25722 net.cpp:67] Creating Layer drop6
I0812 20:00:09.534951 25722 net.cpp:330] drop6 <- fc6
I0812 20:00:09.534956 25722 net.cpp:281] drop6 -> fc6 (in-place)
I0812 20:00:09.534966 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.534970 25722 net.cpp:121] drop6 needs backward computation.
I0812 20:00:09.534976 25722 net.cpp:67] Creating Layer fc7
I0812 20:00:09.534979 25722 net.cpp:330] fc7 <- fc6
I0812 20:00:09.534984 25722 net.cpp:291] fc7 -> fc7
I0812 20:00:09.940529 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.940575 25722 net.cpp:121] fc7 needs backward computation.
I0812 20:00:09.940584 25722 net.cpp:67] Creating Layer relu7
I0812 20:00:09.940589 25722 net.cpp:330] relu7 <- fc7
I0812 20:00:09.940598 25722 net.cpp:281] relu7 -> fc7 (in-place)
I0812 20:00:09.940634 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.940639 25722 net.cpp:121] relu7 needs backward computation.
I0812 20:00:09.940644 25722 net.cpp:67] Creating Layer drop7
I0812 20:00:09.940647 25722 net.cpp:330] drop7 <- fc7
I0812 20:00:09.940652 25722 net.cpp:281] drop7 -> fc7 (in-place)
I0812 20:00:09.940659 25722 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0812 20:00:09.940662 25722 net.cpp:121] drop7 needs backward computation.
I0812 20:00:09.940667 25722 net.cpp:67] Creating Layer fc8
I0812 20:00:09.940671 25722 net.cpp:330] fc8 <- fc7
I0812 20:00:09.940676 25722 net.cpp:291] fc8 -> fc8
I0812 20:00:10.042954 25722 net.cpp:84] Top shape: 128 1000 1 1 (128000)
I0812 20:00:10.042980 25722 net.cpp:121] fc8 needs backward computation.
I0812 20:00:10.042990 25722 net.cpp:67] Creating Layer loss
I0812 20:00:10.042995 25722 net.cpp:330] loss <- fc8
I0812 20:00:10.043002 25722 net.cpp:330] loss <- label
I0812 20:00:10.043009 25722 net.cpp:291] loss -> each_loss
I0812 20:00:10.043047 25722 net.cpp:84] Top shape: 128 1 1 1 (128)
I0812 20:00:10.043053 25722 net.cpp:121] loss needs backward computation.
I0812 20:00:10.043056 25722 net.cpp:148] This network produces output each_loss
I0812 20:00:10.043069 25722 net.cpp:442] Collecting Learning Rate and Weight Decay.
I0812 20:00:10.043076 25722 net.cpp:159] Network initialization done.
I0812 20:00:10.043079 25722 net.cpp:160] Memory required for data: 0
I0812 20:00:10.043114 25722 solver.cpp:81] Solver scaffolding done.
I0812 20:00:10.043125 25722 solver.cpp:88] Solving CaffeNet
I0812 20:00:17.298179 25722 solver.cpp:214] Snapshotting to caffe_imagenet_train_raw_iter_10
I0812 20:00:18.856493 25722 solver.cpp:221] Snapshotting solver state to caffe_imagenet_train_raw_iter_10.solverstate
I0812 20:00:20.182591 25722 TimeTracker.cpp:42] report the time cost
I0812 20:00:20.182677 25722 TimeTracker.cpp:74] write time to file /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184151 25722 TimeTracker.cpp:107] detailed time information is in /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184166 25722 TimeTracker.cpp:59] Data_prefetch: count: 11; mean: 0.0671497 +- 0.0017688
I0812 20:00:20.184188 25722 TimeTracker.cpp:59] crop_data_and_normalization: count: 1408; mean: 0.000464784 +- 1.23608e-05
I0812 20:00:20.184195 25722 TimeTracker.cpp:59] decode_datum: count: 1408; mean: 4.84375e-07 +- 5.01353e-07
I0812 20:00:20.184201 25722 TimeTracker.cpp:59] go_to_next_iter: count: 1408; mean: 6.71165e-07 +- 5.49439e-07
I0812 20:00:20.184207 25722 TimeTracker.cpp:59] read_data: count: 1408; mean: 5.46151e-05 +- 2.32983e-06
I0812 20:00:20.184213 25722 TimeTracker.cpp:42] report the time cost
I0812 20:00:20.184216 25722 TimeTracker.cpp:74] write time to file /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184244 25722 TimeTracker.cpp:107] detailed time information is in /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184252 25722 TimeTracker.cpp:59] data_layer_copy: count: 10; mean: 0.0790926 +- 0.0230497
I0812 20:00:20.184258 25722 TimeTracker.cpp:59] join_thread: count: 10; mean: 1.5e-06 +- 7.07107e-07
I0812 20:00:20.184273 25722 TimeTracker.cpp:42] report the time cost
I0812 20:00:20.184278 25722 TimeTracker.cpp:74] write time to file /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184478 25722 TimeTracker.cpp:107] detailed time information is in /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184494 25722 TimeTracker.cpp:59] ForwardBackward: count: 10; mean: 0.689322 +- 0.00429551
I0812 20:00:20.184501 25722 TimeTracker.cpp:59] back_CaffeNet_conv1: count: 10; mean: 0.0014759 +- 9.54631e-05
I0812 20:00:20.184506 25722 TimeTracker.cpp:59] back_CaffeNet_conv2: count: 10; mean: 0.18284 +- 0.00265923
I0812 20:00:20.184511 25722 TimeTracker.cpp:59] back_CaffeNet_conv3: count: 10; mean: 0.0965238 +- 0.000884713
I0812 20:00:20.184517 25722 TimeTracker.cpp:59] back_CaffeNet_conv4: count: 10; mean: 0.0922271 +- 0.000819161
I0812 20:00:20.184532 25722 TimeTracker.cpp:59] back_CaffeNet_conv5: count: 10; mean: 0.109081 +- 0.0204806
I0812 20:00:20.184538 25722 TimeTracker.cpp:59] back_CaffeNet_drop6: count: 10; mean: 1.24e-05 +- 1.89737e-06
I0812 20:00:20.184543 25722 TimeTracker.cpp:59] back_CaffeNet_drop7: count: 10; mean: 1.4e-05 +- 2.16025e-06
I0812 20:00:20.184551 25722 TimeTracker.cpp:59] back_CaffeNet_fc6: count: 10; mean: 0.0002793 +- 2.52149e-05
I0812 20:00:20.184556 25722 TimeTracker.cpp:59] back_CaffeNet_fc7: count: 10; mean: 0.000264 +- 6.9282e-06
I0812 20:00:20.184561 25722 TimeTracker.cpp:59] back_CaffeNet_fc8: count: 10; mean: 0.000248 +- 1.0924e-05
I0812 20:00:20.184566 25722 TimeTracker.cpp:59] back_CaffeNet_loss: count: 10; mean: 0.0002581 +- 3.23709e-05
I0812 20:00:20.184571 25722 TimeTracker.cpp:59] back_CaffeNet_norm1: count: 10; mean: 2.09e-05 +- 4.04487e-05
I0812 20:00:20.184576 25722 TimeTracker.cpp:59] back_CaffeNet_norm2: count: 10; mean: 2.85e-05 +- 5.17693e-05
I0812 20:00:20.184581 25722 TimeTracker.cpp:59] back_CaffeNet_pool1: count: 10; mean: 3.2e-05 +- 6.43031e-05
I0812 20:00:20.184587 25722 TimeTracker.cpp:59] back_CaffeNet_pool2: count: 10; mean: 2.54e-05 +- 4.34388e-05
I0812 20:00:20.184591 25722 TimeTracker.cpp:59] back_CaffeNet_pool5: count: 10; mean: 0.0001402 +- 1.57254e-05
I0812 20:00:20.184595 25722 TimeTracker.cpp:59] back_CaffeNet_relu1: count: 10; mean: 5.9e-06 +- 1.19722e-06
I0812 20:00:20.184600 25722 TimeTracker.cpp:59] back_CaffeNet_relu2: count: 10; mean: 5.3e-06 +- 4.83046e-07
I0812 20:00:20.184605 25722 TimeTracker.cpp:59] back_CaffeNet_relu3: count: 10; mean: 7.8e-06 +- 4.21637e-07
I0812 20:00:20.184612 25722 TimeTracker.cpp:59] back_CaffeNet_relu4: count: 10; mean: 2.31e-05 +- 6.65749e-06
I0812 20:00:20.184617 25722 TimeTracker.cpp:59] back_CaffeNet_relu5: count: 10; mean: 1.16e-05 +- 2.01108e-06
I0812 20:00:20.184626 25722 TimeTracker.cpp:59] back_CaffeNet_relu6: count: 10; mean: 0.0001096 +- 3.68094e-05
I0812 20:00:20.184633 25722 TimeTracker.cpp:59] back_CaffeNet_relu7: count: 10; mean: 7.2e-06 +- 6.32456e-07
I0812 20:00:20.184638 25722 TimeTracker.cpp:59] forward_CaffeNet_conv1: count: 10; mean: 0.0016747 +- 0.00037118
I0812 20:00:20.184644 25722 TimeTracker.cpp:59] forward_CaffeNet_conv2: count: 10; mean: 0.0055657 +- 0.011183
I0812 20:00:20.184649 25722 TimeTracker.cpp:59] forward_CaffeNet_conv3: count: 10; mean: 0.0193323 +- 0.0105438
I0812 20:00:20.184655 25722 TimeTracker.cpp:59] forward_CaffeNet_conv4: count: 10; mean: 0.0436332 +- 0.0015744
I0812 20:00:20.184660 25722 TimeTracker.cpp:59] forward_CaffeNet_conv5: count: 10; mean: 0.0459225 +- 0.00181147
I0812 20:00:20.184666 25722 TimeTracker.cpp:59] forward_CaffeNet_data: count: 10; mean: 0.0791186 +- 0.0230462
I0812 20:00:20.184672 25722 TimeTracker.cpp:59] forward_CaffeNet_drop6: count: 10; mean: 0.0009195 +- 0.00213296
I0812 20:00:20.184679 25722 TimeTracker.cpp:59] forward_CaffeNet_drop7: count: 10; mean: 0.0001312 +- 2.61619e-06
I0812 20:00:20.184684 25722 TimeTracker.cpp:59] forward_CaffeNet_fc6: count: 10; mean: 0.0064342 +- 0.0199465
I0812 20:00:20.184694 25722 TimeTracker.cpp:59] forward_CaffeNet_fc7: count: 10; mean: 0.0013622 +- 0.00423176
I0812 20:00:20.184700 25722 TimeTracker.cpp:59] forward_CaffeNet_fc8: count: 10; mean: 0.0004854 +- 0.00110103
I0812 20:00:20.184706 25722 TimeTracker.cpp:59] forward_CaffeNet_loss: count: 10; mean: 0.0004253 +- 6.34298e-06
I0812 20:00:20.184711 25722 TimeTracker.cpp:59] forward_CaffeNet_norm1: count: 10; mean: 3.93e-05 +- 8.98444e-05
I0812 20:00:20.184716 25722 TimeTracker.cpp:59] forward_CaffeNet_norm2: count: 10; mean: 3.21e-05 +- 7.12935e-05
I0812 20:00:20.184722 25722 TimeTracker.cpp:59] forward_CaffeNet_pool1: count: 10; mean: 3.63e-05 +- 9.44129e-05
I0812 20:00:20.184727 25722 TimeTracker.cpp:59] forward_CaffeNet_pool2: count: 10; mean: 3.53e-05 +- 9.16019e-05
I0812 20:00:20.184732 25722 TimeTracker.cpp:59] forward_CaffeNet_pool5: count: 10; mean: 0.000133 +- 3.47882e-05
I0812 20:00:20.184739 25722 TimeTracker.cpp:59] forward_CaffeNet_relu1: count: 10; mean: 7.3e-06 +- 1.1595e-06
I0812 20:00:20.184751 25722 TimeTracker.cpp:59] forward_CaffeNet_relu2: count: 10; mean: 6.3e-06 +- 1.05935e-06
I0812 20:00:20.184757 25722 TimeTracker.cpp:59] forward_CaffeNet_relu3: count: 10; mean: 5.29e-05 +- 1.58075e-05
I0812 20:00:20.184762 25722 TimeTracker.cpp:59] forward_CaffeNet_relu4: count: 10; mean: 3.53e-05 +- 1.035e-05
I0812 20:00:20.184768 25722 TimeTracker.cpp:59] forward_CaffeNet_relu5: count: 10; mean: 1.57e-05 +- 3.26769e-06
I0812 20:00:20.184773 25722 TimeTracker.cpp:59] forward_CaffeNet_relu6: count: 10; mean: 1.68e-05 +- 3.55278e-06
I0812 20:00:20.184778 25722 TimeTracker.cpp:59] forward_CaffeNet_relu7: count: 10; mean: 0.0001094 +- 3.59852e-05
I0812 20:00:20.184787 25722 TimeTracker.cpp:42] report the time cost
I0812 20:00:20.184789 25722 TimeTracker.cpp:74] write time to file /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184821 25722 TimeTracker.cpp:107] detailed time information is in /home/jianfeng/glogs/TimeTracker-20140812-200020.25722
I0812 20:00:20.184828 25722 TimeTracker.cpp:59] compute_update_value: count: 10; mean: 0.000347 +- 0.000347054
I0812 20:00:20.184835 25722 TimeTracker.cpp:59] forward_backward: count: 10; mean: 0.689323 +- 0.00429561
I0812 20:00:20.184841 25722 TimeTracker.cpp:59] one_training_iteration: count: 10; mean: 0.689732 +- 0.0044945
I0812 20:00:20.184854 25722 TimeTracker.cpp:59] presolve(): count: 1; mean: 9e-06 +- -nan
I0812 20:00:20.184859 25722 TimeTracker.cpp:59] resume_file: count: 1; mean: 0 +- -nan
I0812 20:00:20.184865 25722 TimeTracker.cpp:59] update_value: count: 10; mean: 5.42e-05 +- 9.84096e-06
I0812 20:00:20.184871 25722 solver.cpp:145] Optimization Done.
I0812 20:00:20.184877 25722 train_net.cpp:33] Optimization Done.
/home/jianfeng/glogs

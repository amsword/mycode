#!/bin/bash

# TOOLS=../../build/tools
export LD_LIBRARY_PATH=/home/jianfeng/liblmdb:$LD_LIBRARY_PATH
TOOLS=../../.build_release/tools
export GOOGLE_LOG_DIR=/home/jianfeng/glogs
VAL_DATA_ROOT=/home/jianfeng/data/imagenet2012/ILSVRC2012_cls_val_256/
#rm -r -f  /home/jianfeng/data/imagenet2012/imagenet_val_lmdb-jpeg-90

#$TOOLS/convert_to_database.bin -input_type lmdb -output_type lmdb \
		#-input /home/jianfeng/data/imagenet2012/imagenet_val_lmdb \
		#-output /home/jianfeng/data/imagenet2012/imagenet_val_lmdb \
		#-JPEG 50
out_prefix=/home/jianfeng/data/imagenet2012/imagenet_val_lmdb
out_dir=/home/jianfeng/data/imagenet2012/imagenet_val_lmdb-jpeg
#all_quality=(10 20 30 40 50 60 70 80 90);
all_quality=(20);
num_all_quality=${#all_quality[@]};
for ((i=0; i<${num_all_quality}; i++))
do
quality=${all_quality[i]};

out_lmdb=${out_dir}-${quality}
out_info=${out_lmdb}.distortion.txt
echo ${out_lmdb}

#$TOOLS/compute_jpeg_distortion.bin -input_lmdb_raw /home/jianfeng/data/imagenet2012/imagenet_val_lmdb \
		#-input_lmdb_jpeg ${out_lmdb} 2>&1 | tee -a ${out_info}

$TOOLS/compute_jpeg_distortion.bin -input_lmdb_raw /home/jianfeng/data/imagenet2012/imagenet_val_lmdb \
		-input_lmdb_jpeg ${out_lmdb} 2>&1 | tee a.txt 
done

		

echo "Done."

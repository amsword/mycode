#!/usr/bin/env sh

# TOOLS=../../build/tools
export LD_LIBRARY_PATH=/home/jianfeng/liblmdb:$LD_LIBRARY_PATH
TOOLS=../../.build_release/tools
export GOOGLE_LOG_DIR=/home/jianfeng/glogs

$TOOLS/train_net.bin imagenet_solver_raw.prototxt 2>&1 | tee tmp.txt
# mv tmp.txt $GOOGLE_LOG_DIR/time_raw.txt

# $TOOLS/train_net.bin imagenet_solver_jpeg.prototxt 2>&1 | tee a.txt
# mv tmp.txt $GOOGLE_LOG_DIR/time_jpeg.txt
echo "Done."

#!/usr/bin/env sh
set -x
set -e
export GOOGLE_LOG_DIR=/home/wangjianfeng/glogs
#TOOLS=/home/wangjianfeng/from_yunchao/caffe_cccp/caffe_cccp_yunchao/build/tools
TOOLS=/home/wangjianfeng/code/caffe/build/tools
MY_TOOLS=/home/wangjianfeng/code/mycode/c_code/bin
DATASET=imagenet2012
DATA_FOLDER=/home/wangjianfeng/data/${DATASET}/
WORKING_FOLDER=/home/wangjianfeng/working/cifar100/knncnn/cccp_model/

TRAIN_LEVELDB=${DATA_FOLDER}preprocessed/${DATASET}-leveldb/cifar-train-leveldb
TRAIN_LMDB=${DATA_FOLDER}preprocessed/${DATASET}-lmdb/cifar-train-lmdb
TRAIN_LMDB_LABEL=${TRAIN_LMDB}_label
TRAIN_LMDB_KEYS=${TRAIN_LMDB}_keys
TEST_LEVELDB=${DATA_FOLDER}preprocessed/${DATASET}-leveldb/cifar-test-leveldb
TEST_LMDB=${DATA_FOLDER}preprocessed/${DATASET}-lmdb/cifar-test-lmdb
TEST_LMDB_KEYS=${TEST_LMDB}_keys
TEST_LMDB_LABEL=${TEST_LMDB}_label

SOLVER_KNN=cifar100_solver_knn.prototxt

MODEL90_SUB=cifar100_yuncode_iter_200000
#MODEL=${WORKING_FOLDER}cifar10
MODEL90=${WORKING_FOLDER}${MODEL90_SUB}

TEST_PROTO_BASELINE=cifar100_val.prototxt
TRAIN_PROTO_BASELINE=cifar10_train.prototxt
TEST_PROTO_EXTRACT=cifar100_val_extract.prototxt
TRAIN_PROTO_EXTRACT=cifar100_train_extract.prototxt
# cccp6, cccp5, pool3, softmax
#FEATURE_LAYER=cccp6
FEATURE_LAYER=loss
TRAIN_FEATURE_MAT=${WORKING_FOLDER}${MODEL90_SUB}_feature_train_${FEATURE_LAYER}
TEST_FEATURE_MAT=${WORKING_FOLDER}${MODEL90_SUB}_feature_test_${FEATURE_LAYER}
FILE_CONFIDENCE_SCORE=${WORKING_FOLDER}${MODEL90_SUB}_feature_test_softmax
NUM_MAX_SELECTED_LABELS=50
NUM_CANDIDATE_EACH=500
BATCH_SIZE_EACH_LABEL=5
NUM_SELECTED_LABELS=20
BATCH_NUM_ONE_TEST=20
INTER_MULTILABEL_KNN=${WORKING_FOLDER}${MODEL90_SUB}_inter_multilabel_knn_${FEATURE_LAYER}_${NUM_MAX_SELECTED_LABELS}${NUM_CANDIDATE_EACH}
MULTILABEL_KNN=${WORKING_FOLDER}${MODEL90_SUB}_multilabel_knn_${FEATURE_LAYER}_${NUM_SELECTED_LABELS}_${BATCH_SIZE_EACH_LABEL}_${BATCH_NUM_ONE_TEST}

KNN=10000
EXACT_L2_NN=${WORKING_FOLDER}${MODEL90_SUB}_${FEATURE_LAYER}_exact_l2_knn_${KNN}
EXACT_L2_NN_IDX=${EXACT_L2_NN}_index
EXACT_L2_NN_IDX_QUALITY=${EXACT_L2_NN_IDX}_quality.txt

LABEL_L2_NN=${WORKING_FOLDER}${MODEL90_SUB}_${FEATURE_LAYER}_label_l2_knn_${KNN}
LABEL_L2_NN_IDX=${LABEL_L2_NN}_index
LABEL_L2_NN_IDX_QUALITY=${LABEL_L2_NN_IDX}_quality.txt
#########################################################################
# convert it to lmdb
#$TOOLS/convert_to_database.bin -input_type leveldb -input ${TRAIN_LEVELDB} \
		#-output_type lmdb -output ${TRAIN_LMDB}
#$TOOLS/convert_to_database.bin -input_type leveldb -input ${TEST_LEVELDB} \
		#-output_type lmdb -output ${TEST_LMDB}
# convert to lmdb from images
#$TOOLS/convert_imageset.bin ${DATA_FOLDER}images/ILSVRC2012_cls_train_256/ \
		#${DATA_FOLDER}image_list/train.txt ${DATA_FOLDER}lmdb/imagenet_train_lmdb_256x256 \
		#-bd lmdb

#$TOOLS/convert_imageset.bin ${DATA_FOLDER}images/ILSVRC2012_cls_train_256/ \
		#${DATA_FOLDER}image_list/train.txt ${DATA_FOLDER}lmdb/imagenet_train_lmdb_256x256_tmp \
		#-bd lmdb -read_raw_jpeg 1

# train the model
#$TOOLS/train_net.bin ${DATASET}_solver.prototxt 2>&1 | tee -a train2.tee
# continue the training
#$TOOLS/train_net.bin ${DATASET}_solver.prototxt \
		#/home/wangjianfeng/working/cifar100/knncnn/cccp_model/cifar100_yuncode_iter_120000.solverstate \
		#2>&1 | tee -a train.tee

# test model as a baseline
# $TOOLS/test_net.bin ${TEST_PROTO_BASELINE} ${MODEL90} 100 GPU 1 \
# 		2>&1 | tee -a ${WORKING_FOLDER}${MODEL90}_baseline.tee

# extract features of test data
# ${TOOLS}/extract_features.bin \
# 	${MODEL90} \
# 	${TEST_PROTO_EXTRACT} \
# 	${FEATURE_LAYER} \
# 	${TEST_FEATURE_MAT} \
# 	100 \
# 	GPU DEVICE_ID=2 2>&1 | tee -a ${TEST_FEATURE_MAT}.tee
# ${TOOLS}/extract_features.bin \
# 	${MODEL90} \
# 	${TRAIN_PROTO_EXTRACT} \
# 	${FEATURE_LAYER} \
# 	${TRAIN_FEATURE_MAT} \
# 	500 \
# 	GPU DEVICE_ID=2 2>&1 | tee -a ${TRAIN_FEATURE_MAT}.tee

# compute the k-nn
#${MY_TOOLS}/BruteForceNNUsingDll.bin ${TEST_FEATURE_MAT} ${TRAIN_FEATURE_MAT} \
	#${EXACT_L2_NN} ${KNN} 2>&1 | tee -a ${EXACT_L2_NN}.tee
#${MY_TOOLS}/ConvertGNDToMatrixIndex.bin ${EXACT_L2_NN} ${EXACT_L2_NN_IDX}

# extract labels
#${TOOLS}/extract_labels.bin -input_type lmdb -input ${TRAIN_LMDB} -output ${TRAIN_LMDB_LABEL}
#${TOOLS}/extract_labels.bin -input_type lmdb -input ${TEST_LMDB} -output ${TEST_LMDB_LABEL}

# generate the best k-nn result by using the labels
#${MY_TOOLS}/LabelFirstBruteForceNN.bin ${TEST_FEATURE_MAT} ${TRAIN_FEATURE_MAT} \
	#${LABEL_L2_NN} ${KNN} ${TEST_LMDB_LABEL} ${TRAIN_LMDB_LABEL} 2>&1 | tee -a ${EXACT_L2_NN}.tee
#${MY_TOOLS}/ConvertGNDToMatrixIndex.bin ${LABEL_L2_NN} ${LABEL_L2_NN_IDX}
#${MY_TOOLS}/SemanticLabel ${TEST_LMDB_LABEL} ${TRAIN_LMDB_LABEL} ${LABEL_L2_NN_IDX} ${LABEL_L2_NN_IDX_QUALITY}
# compute the quality of the k-nn
#${MY_TOOLS}/SemanticLabel ${TEST_LMDB_LABEL} ${TRAIN_LMDB_LABEL} ${EXACT_L2_NN_IDX} ${EXACT_L2_NN_IDX_QUALITY}

# extract keys
#${TOOLS}/extract_keys.bin \
		#-input_type lmdb -input ${TRAIN_LMDB} \
		#-output ${TRAIN_LMDB_KEYS}
#${TOOLS}/extract_keys.bin \
		#-input_type lmdb -input ${TEST_LMDB} \
		#-output ${TEST_LMDB_KEYS}

# generate multilabelknn
#${MY_TOOLS}/MultiLabelKNN.bin ${FILE_CONFIDENCE_SCORE} ${NUM_MAX_SELECTED_LABELS} \
		#${TRAIN_LMDB_LABEL} ${TEST_FEATURE_MAT} ${TRAIN_FEATURE_MAT} \
		#${NUM_CANDIDATE_EACH} ${INTER_MULTILABEL_KNN}
#${MY_TOOLS}/ConvertInterMultiLabelKNN.bin ${INTER_MULTILABEL_KNN} \
		#${NUM_SELECTED_LABELS} ${BATCH_SIZE_EACH_LABEL} ${BATCH_NUM_ONE_TEST} ${MULTILABEL_KNN}

# run proposed testing

#$TOOLS/test_net_knn.bin \
	#${SOLVER_KNN} \
	#${MODEL90}
#2>&1 | tee -a ${WORKING_FOLDER}test_net_knn_base_lr0_test.tee

echo "done"

#reduce learning rate by fctor of 10 after 8 epochs
#$TOOLS/train_net.bin cifar10_quick_solver_lr1.prototxt cifar10_quick_iter_4000.solverstate

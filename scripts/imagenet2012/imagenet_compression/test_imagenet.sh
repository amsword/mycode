#!/usr/bin/env sh

# TOOLS=../../build/tools
export LD_LIBRARY_PATH=/home/jianfeng/liblmdb:$LD_LIBRARY_PATH
TOOLS=../../.build_release/tools
export GOOGLE_LOG_DIR=/home/jianfeng/glogs

#all_quality=(10 20 30 40 50 60 70 80 90);
all_quality=(50);
num_all_quality=${#all_quality[@]};
for ((i=0; i<num_all_quality; i++))
do
quality=${all_quality[i]};
#$TOOLS/test_net.bin imagenet_val_${quality}.prototxt caffe_reference_imagenet_model 1000 GPU 1 \
		#2>&1 | tee -a /home/jianfeng/data/imagenet2012/imagenet_val_lmdb-jpeg-${quality}.test.txt

$TOOLS/test_net.bin imagenet_val_${quality}.prototxt \
		caffe_reference_imagenet_model 1000 GPU 1 \
		2>&1 | tee a.txt
		
# mv tmp.txt $GOOGLE_LOG_DIR/time_raw.txt
done
# $TOOLS/train_net.bin imagenet_solver_jpeg.prototxt
# mv tmp.txt $GOOGLE_LOG_DIR/time_jpeg.txt
echo "Done."


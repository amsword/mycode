I0801 11:36:17.771432 12779 train_net.cpp:25] Starting Optimization
I0801 11:36:17.771663 12779 solver.cpp:38] Initializing solver from parameters: 
train_net: "imagenet_train_raw_cross_relu.prototxt"
base_lr: 0.01
display: 20
max_iter: 10
lr_policy: "step"
gamma: 0.1
momentum: 0.9
weight_decay: 0.0005
stepsize: 100000
snapshot: 10000
snapshot_prefix: "caffe_imagenet_train_raw"
device_id: 2
random_seed: 1
I0801 11:36:18.052252 12779 solver.cpp:58] Creating training net from file: imagenet_train_raw_cross_relu.prototxt
I0801 11:36:18.052786 12779 net.cpp:39] Initializing net from parameters: 
name: "CaffeNet"
layers {
  top: "data"
  top: "label"
  name: "data"
  type: DATA
  data_param {
    source: "/home/jianfeng/data/imagenet2012/imagenet_val_lmdb"
    mean_file: "/DATA/laihanj/imagenet-train-mean"
    batch_size: 128
    crop_size: 227
    backend: LMDB
  }
}
layers {
  bottom: "data"
  top: "conv1"
  name: "conv1"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 96
    kernel_size: 11
    stride: 4
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "conv1"
  top: "conv1"
  name: "relu1"
  type: RELU
}
layers {
  bottom: "conv1"
  top: "pool1"
  name: "pool1"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool1"
  top: "norm1"
  name: "norm1"
  type: LRN
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layers {
  bottom: "norm1"
  top: "conv2"
  name: "conv2"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 256
    pad: 2
    kernel_size: 5
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv2"
  top: "conv2"
  name: "relu2"
  type: RELU
}
layers {
  bottom: "conv2"
  top: "pool2"
  name: "pool2"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool2"
  top: "norm2"
  name: "norm2"
  type: LRN
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layers {
  bottom: "norm2"
  top: "conv3"
  name: "conv3"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "conv3"
  top: "conv3"
  name: "relu3"
  type: RELU
}
layers {
  bottom: "conv3"
  top: "conv4"
  name: "conv4"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv4"
  top: "conv4"
  name: "relu4"
  type: RELU
}
layers {
  bottom: "conv4"
  top: "conv5"
  name: "conv5"
  type: CONVOLUTION
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "conv5"
  top: "conv5"
  name: "relu5"
  type: RELU
}
layers {
  bottom: "conv5"
  top: "pool5"
  name: "pool5"
  type: POOLING
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layers {
  bottom: "pool5"
  top: "fc6"
  name: "fc6"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "fc6"
  top: "fc6"
  name: "relu6"
  type: RELU
}
layers {
  bottom: "fc6"
  top: "fc6"
  name: "drop6"
  type: DROPOUT
  dropout_param {
    dropout_ratio: 0.5
  }
}
layers {
  bottom: "fc6"
  top: "fc7"
  name: "fc7"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layers {
  bottom: "fc7"
  top: "fc7"
  name: "relu7"
  type: RELU
}
layers {
  bottom: "fc7"
  top: "fc7"
  name: "drop7"
  type: DROPOUT
  dropout_param {
    dropout_ratio: 0.5
  }
}
layers {
  bottom: "fc7"
  top: "fc8"
  name: "fc8"
  type: INNER_PRODUCT
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {
    num_output: 1000
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layers {
  bottom: "fc8"
  bottom: "label"
  top: "raw_loss"
  name: "loss"
  type: SOFTMAX_LOSS
}
I0801 11:36:18.052923 12779 data_layer.cpp:523] NO decoding
I0801 11:36:18.052933 12779 net.cpp:67] Creating Layer data
I0801 11:36:18.052940 12779 net.cpp:253] data -> data
I0801 11:36:18.052958 12779 net.cpp:253] data -> label
I0801 11:36:18.053050 12779 data_layer.cpp:264] Opening lmdb /home/jianfeng/data/imagenet2012/imagenet_val_lmdb
I0801 11:36:18.053181 12779 data_layer.cpp:314] start decode: 196608
I0801 11:36:18.053191 12779 data_layer.cpp:316] end decode: 196608
I0801 11:36:18.053200 12779 data_layer.cpp:356] output data size: 128,3,227,227
I0801 11:36:18.053207 12779 data_layer.cpp:384] Loading mean file from/DATA/laihanj/imagenet-train-mean
I0801 11:36:18.083258 12779 net.cpp:84] Top shape: 128 3 227 227 (19787136)
I0801 11:36:18.083276 12779 net.cpp:84] Top shape: 128 1 1 1 (128)
I0801 11:36:18.083281 12779 net.cpp:126] data does not need backward computation.
I0801 11:36:18.083291 12779 net.cpp:67] Creating Layer conv1
I0801 11:36:18.083295 12779 net.cpp:292] conv1 <- data
I0801 11:36:18.083308 12779 net.cpp:253] conv1 -> conv1
I0801 11:36:18.084277 12779 net.cpp:84] Top shape: 128 96 55 55 (37171200)
I0801 11:36:18.084290 12779 net.cpp:121] conv1 needs backward computation.
I0801 11:36:18.084296 12779 net.cpp:67] Creating Layer relu1
I0801 11:36:18.084301 12779 net.cpp:292] relu1 <- conv1
I0801 11:36:18.084306 12779 net.cpp:243] relu1 -> conv1 (in-place)
I0801 11:36:18.084313 12779 net.cpp:84] Top shape: 128 96 55 55 (37171200)
I0801 11:36:18.084317 12779 net.cpp:121] relu1 needs backward computation.
I0801 11:36:18.084322 12779 net.cpp:67] Creating Layer pool1
I0801 11:36:18.084326 12779 net.cpp:292] pool1 <- conv1
I0801 11:36:18.084331 12779 net.cpp:253] pool1 -> pool1
I0801 11:36:18.084342 12779 net.cpp:84] Top shape: 128 96 27 27 (8957952)
I0801 11:36:18.084348 12779 net.cpp:121] pool1 needs backward computation.
I0801 11:36:18.084354 12779 net.cpp:67] Creating Layer norm1
I0801 11:36:18.084358 12779 net.cpp:292] norm1 <- pool1
I0801 11:36:18.084363 12779 net.cpp:253] norm1 -> norm1
I0801 11:36:18.084370 12779 net.cpp:84] Top shape: 128 96 27 27 (8957952)
I0801 11:36:18.084375 12779 net.cpp:121] norm1 needs backward computation.
I0801 11:36:18.084381 12779 net.cpp:67] Creating Layer conv2
I0801 11:36:18.084384 12779 net.cpp:292] conv2 <- norm1
I0801 11:36:18.084390 12779 net.cpp:253] conv2 -> conv2
I0801 11:36:18.093011 12779 net.cpp:84] Top shape: 128 256 27 27 (23887872)
I0801 11:36:18.093034 12779 net.cpp:121] conv2 needs backward computation.
I0801 11:36:18.093041 12779 net.cpp:67] Creating Layer relu2
I0801 11:36:18.093045 12779 net.cpp:292] relu2 <- conv2
I0801 11:36:18.093053 12779 net.cpp:243] relu2 -> conv2 (in-place)
I0801 11:36:18.093061 12779 net.cpp:84] Top shape: 128 256 27 27 (23887872)
I0801 11:36:18.093065 12779 net.cpp:121] relu2 needs backward computation.
I0801 11:36:18.093070 12779 net.cpp:67] Creating Layer pool2
I0801 11:36:18.093073 12779 net.cpp:292] pool2 <- conv2
I0801 11:36:18.093078 12779 net.cpp:253] pool2 -> pool2
I0801 11:36:18.093086 12779 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0801 11:36:18.093093 12779 net.cpp:121] pool2 needs backward computation.
I0801 11:36:18.093119 12779 net.cpp:67] Creating Layer norm2
I0801 11:36:18.093124 12779 net.cpp:292] norm2 <- pool2
I0801 11:36:18.093129 12779 net.cpp:253] norm2 -> norm2
I0801 11:36:18.093137 12779 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0801 11:36:18.093140 12779 net.cpp:121] norm2 needs backward computation.
I0801 11:36:18.093148 12779 net.cpp:67] Creating Layer conv3
I0801 11:36:18.093152 12779 net.cpp:292] conv3 <- norm2
I0801 11:36:18.093157 12779 net.cpp:253] conv3 -> conv3
I0801 11:36:18.117890 12779 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0801 11:36:18.117918 12779 net.cpp:121] conv3 needs backward computation.
I0801 11:36:18.117928 12779 net.cpp:67] Creating Layer relu3
I0801 11:36:18.117931 12779 net.cpp:292] relu3 <- conv3
I0801 11:36:18.117939 12779 net.cpp:243] relu3 -> conv3 (in-place)
I0801 11:36:18.117946 12779 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0801 11:36:18.117950 12779 net.cpp:121] relu3 needs backward computation.
I0801 11:36:18.117957 12779 net.cpp:67] Creating Layer conv4
I0801 11:36:18.117961 12779 net.cpp:292] conv4 <- conv3
I0801 11:36:18.117966 12779 net.cpp:253] conv4 -> conv4
I0801 11:36:18.136438 12779 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0801 11:36:18.136463 12779 net.cpp:121] conv4 needs backward computation.
I0801 11:36:18.136472 12779 net.cpp:67] Creating Layer relu4
I0801 11:36:18.136476 12779 net.cpp:292] relu4 <- conv4
I0801 11:36:18.136484 12779 net.cpp:243] relu4 -> conv4 (in-place)
I0801 11:36:18.136492 12779 net.cpp:84] Top shape: 128 384 13 13 (8306688)
I0801 11:36:18.136497 12779 net.cpp:121] relu4 needs backward computation.
I0801 11:36:18.136504 12779 net.cpp:67] Creating Layer conv5
I0801 11:36:18.136508 12779 net.cpp:292] conv5 <- conv4
I0801 11:36:18.136513 12779 net.cpp:253] conv5 -> conv5
I0801 11:36:18.148836 12779 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0801 11:36:18.148862 12779 net.cpp:121] conv5 needs backward computation.
I0801 11:36:18.148870 12779 net.cpp:67] Creating Layer relu5
I0801 11:36:18.148874 12779 net.cpp:292] relu5 <- conv5
I0801 11:36:18.148882 12779 net.cpp:243] relu5 -> conv5 (in-place)
I0801 11:36:18.148890 12779 net.cpp:84] Top shape: 128 256 13 13 (5537792)
I0801 11:36:18.148895 12779 net.cpp:121] relu5 needs backward computation.
I0801 11:36:18.148898 12779 net.cpp:67] Creating Layer pool5
I0801 11:36:18.148902 12779 net.cpp:292] pool5 <- conv5
I0801 11:36:18.148907 12779 net.cpp:253] pool5 -> pool5
I0801 11:36:18.148916 12779 net.cpp:84] Top shape: 128 256 6 6 (1179648)
I0801 11:36:18.148919 12779 net.cpp:121] pool5 needs backward computation.
I0801 11:36:18.148926 12779 net.cpp:67] Creating Layer fc6
I0801 11:36:18.148929 12779 net.cpp:292] fc6 <- pool5
I0801 11:36:18.148936 12779 net.cpp:253] fc6 -> fc6
I0801 11:36:19.197191 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.197222 12779 net.cpp:121] fc6 needs backward computation.
I0801 11:36:19.197232 12779 net.cpp:67] Creating Layer relu6
I0801 11:36:19.197237 12779 net.cpp:292] relu6 <- fc6
I0801 11:36:19.197244 12779 net.cpp:243] relu6 -> fc6 (in-place)
I0801 11:36:19.197265 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.197269 12779 net.cpp:121] relu6 needs backward computation.
I0801 11:36:19.197274 12779 net.cpp:67] Creating Layer drop6
I0801 11:36:19.197278 12779 net.cpp:292] drop6 <- fc6
I0801 11:36:19.197281 12779 net.cpp:243] drop6 -> fc6 (in-place)
I0801 11:36:19.197295 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.197299 12779 net.cpp:121] drop6 needs backward computation.
I0801 11:36:19.197304 12779 net.cpp:67] Creating Layer fc7
I0801 11:36:19.197307 12779 net.cpp:292] fc7 <- fc6
I0801 11:36:19.197312 12779 net.cpp:253] fc7 -> fc7
I0801 11:36:19.626181 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.626215 12779 net.cpp:121] fc7 needs backward computation.
I0801 11:36:19.626224 12779 net.cpp:67] Creating Layer relu7
I0801 11:36:19.626229 12779 net.cpp:292] relu7 <- fc7
I0801 11:36:19.626235 12779 net.cpp:243] relu7 -> fc7 (in-place)
I0801 11:36:19.626265 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.626269 12779 net.cpp:121] relu7 needs backward computation.
I0801 11:36:19.626274 12779 net.cpp:67] Creating Layer drop7
I0801 11:36:19.626277 12779 net.cpp:292] drop7 <- fc7
I0801 11:36:19.626281 12779 net.cpp:243] drop7 -> fc7 (in-place)
I0801 11:36:19.626287 12779 net.cpp:84] Top shape: 128 4096 1 1 (524288)
I0801 11:36:19.626291 12779 net.cpp:121] drop7 needs backward computation.
I0801 11:36:19.626296 12779 net.cpp:67] Creating Layer fc8
I0801 11:36:19.626298 12779 net.cpp:292] fc8 <- fc7
I0801 11:36:19.626303 12779 net.cpp:253] fc8 -> fc8
I0801 11:36:19.731394 12779 net.cpp:84] Top shape: 128 1000 1 1 (128000)
I0801 11:36:19.731418 12779 net.cpp:121] fc8 needs backward computation.
I0801 11:36:19.731427 12779 net.cpp:67] Creating Layer loss
I0801 11:36:19.731432 12779 net.cpp:292] loss <- fc8
I0801 11:36:19.731439 12779 net.cpp:292] loss <- label
I0801 11:36:19.731444 12779 net.cpp:253] loss -> raw_loss
I0801 11:36:19.731462 12779 net.cpp:84] Top shape: 1 1 1 1 (1)
I0801 11:36:19.731467 12779 net.cpp:121] loss needs backward computation.
I0801 11:36:19.731470 12779 net.cpp:148] This network produces output raw_loss
I0801 11:36:19.731483 12779 net.cpp:404] Collecting Learning Rate and Weight Decay.
I0801 11:36:19.731492 12779 net.cpp:159] Network initialization done.
I0801 11:36:19.731497 12779 net.cpp:160] Memory required for data: 0
I0801 11:36:19.731525 12779 solver.cpp:81] Solver scaffolding done.
I0801 11:36:19.731533 12779 solver.cpp:88] Solving CaffeNet
F0801 11:36:19.999305 12779 softmax_loss_layer.cu:19] 1
*** Check failure stack trace: ***
    @     0x7f69f0f15e0d  google::LogMessage::Fail()
    @     0x7f69f0f17cad  google::LogMessage::SendToLog()
    @     0x7f69f0f159fc  google::LogMessage::Flush()
    @     0x7f69f0f185ce  google::LogMessageFatal::~LogMessageFatal()
    @           0x4e3eeb  caffe::SoftmaxWithLossLayer<>::Forward_gpu()
    @           0x46a27c  caffe::Net<>::ForwardPrefilled()
    @           0x46a4e7  caffe::Net<>::ForwardBackward()
    @           0x44d77d  caffe::Solver<>::Solve()
    @           0x40bb7b  main
    @     0x7f69ee4d6de5  (unknown)
    @           0x40def0  (unknown)

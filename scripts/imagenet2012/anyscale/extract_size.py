from subprocess import check_call;
import sys;

device_id = 2;
machine_name = "deep08";
caffe_dir = "/home/wangjianfeng/code/caffe/build/tools/";
#caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe_any_scale/build/tools/"; 
#caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"; 
snap_prefix = "/home/wangjianfeng/working/imagenet2012/anyscale/a";

cmd = [];
cmd.append("ssh");
cmd.append(machine_name);
sub_cmd = "";
sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH";
sub_cmd = sub_cmd + " && ";
sub_cmd = sub_cmd + "cd /home/wangjianfeng/code/examples/imagenet2012/anyscale && ";
sub_cmd = sub_cmd + caffe_dir + "extract_info.bin -input_type lmdb -input " + \
		"/home/wangjianfeng/data/imagenet2012/lmdb_256xN/imagenet_train_lmdb_256xN " + \
		"-output /home/wangjianfeng/data/imagenet2012/lmdb_256xN/imagenet_train_lmdb_256xN_size " + \
		"-info_type size";
cmd.append(sub_cmd);
check_call(cmd);


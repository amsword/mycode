from subprocess import check_call;
import sys;
python_tools = "/home/wangjianfeng/code/mycode/python_code";
sys.path.append(python_tools);
from train_revise_solver import *;

device_id = 3;
machine_name = "deep08";
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe_any_scale/build/tools/"; 
#caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"; 
snap_prefix = "/home/wangjianfeng/working/imagenet2012/anyscale/a";

net_base = "nizf_train_test.prototxt";
#net_base = "";
#net_base = "debug_train_test.prototxt";
solver_base = "nizf_solver.prototxt";

net_revise = "12:batch_size:1,13:crop_size:0,14:mirror:false";
solver_revise = "display:100";


num_iter = 1309; 
cmd = [];
cmd.append("ssh");
cmd.append(machine_name);
sub_cmd = "";
sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH";
sub_cmd = sub_cmd + " && ";
sub_cmd = sub_cmd + "cd /home/wangjianfeng/code/examples/imagenet2012/anyscale && ";
sub_cmd = sub_cmd + caffe_dir + "caffe test --model=" + net_base + \
          " --gpu=" + str(device_id) + \
          " --weights=" + snap_prefix + "__iter_" + str(num_iter) + ".caffemodel" + \
          " --iterations=500";
sub_cmd = "";
sub_cmd = sub_cmd + "export LD_LIBRARY_PATH=/usr/local/cuda/lib64/:LD_LIBRARY_PATH";
sub_cmd = sub_cmd + " && ";
sub_cmd = sub_cmd + "cd /home/wangjianfeng/code/examples/imagenet2012/anyscale && ";
sub_cmd = sub_cmd + caffe_dir + "caffe train --solver nizf_solver.prototxt"
cmd.append(sub_cmd);
print cmd;
#check_call(cmd);

train_revise_solver(net_base, solver_base,
        net_revise, solver_revise, 
        machine_name, device_id, caffe_dir,  
        snap_prefix);



import sys
python_code = "/mnt/jianfeng/code/mycode/python_code"
sys.path.append(python_code)
from com_utility import extract_all_acc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from numpy import *

output_file = "/mnt/jianfeng/a.png"

# weighted
log_file = '/mnt/jianfeng/visenze_caffe/data/weardex/Caltech101/ratio_0/weighted/caffe.gpu-visenze.jenkins.log.INFO.20150702-174658.17783';
all_acc_w, all_iter_w = extract_all_acc(log_file)

# no weighted
log_file = '/mnt/jianfeng/visenze_caffe/data/weardex/Caltech101/ratio_0/no_weight/caffe.gpu-visenze.jenkins.log.INFO.20150702-180114.19906';
all_acc, all_iter = extract_all_acc(log_file)

#ind = all_acc > 0.3
#all_acc = all_acc[ind]
#all_iter = all_iter[ind]
plt.close()
plt.plot(all_iter_w, all_acc_w, 'ro-');
plt.plot(all_iter, all_acc, 'b*-')
plt.legend(['weight', 'no_weight'], loc = 'best');
plt.grid()
plt.savefig(output_file)

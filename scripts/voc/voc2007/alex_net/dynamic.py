import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)
import matplotlib;
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd
import numpy
import glob
import subprocess


ca = caffe_cmd();
ca.set_machine('deep07');

#extract data layer
#p = [None] * 4;
#pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
#net_def = './extract_train_1_4.prototxt';
#blob_name = 'data';
#save_file = '/home/wangjianfeng/tmp/data.float.bin';
#phase = 'test';
#num_batch = 10;
#ca.set_device_ids([3]);
#p[0] = ca.extract_features1(pretrained, net_def, blob_name,
                          #save_file, num_batch, phase);
#p[0].communicate();

# plot result in /1/
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a';
#idx_end = 6;
#[all_acc, all_iter] = ca.collect_test1(snap_prefix + ''.replace(',', '_') + '_' + ''.replace(',', '_'));
#all_iter = all_iter[0 : idx_end];
#all_acc = all_acc[0 : idx_end];
#[all_acc0, all_iter0] = ca.collect_test1(snap_prefix + ''.replace(',', '_') + '_' + 'base_lr:0.0001'.replace(',', '_'));
#all_acc0 = all_acc0[0 : idx_end];
#all_iter0 = all_iter0[0 : idx_end];
#all_acc = numpy.concatenate((all_acc, all_acc0));
#all_iter = numpy.concatenate((all_iter, all_iter0));
#[all_acc0, all_iter0] = ca.collect_test1(snap_prefix + ''.replace(',', '_') + '_' + 'base_lr:0.00001'.replace(',', '_'));
#all_acc0 = all_acc0[0 : idx_end];
#all_iter0 = all_iter0[0 : idx_end];
#all_acc = numpy.concatenate((all_acc, all_acc0));
#all_iter = numpy.concatenate((all_iter, all_iter0));
#print all_acc.max()
#plt.close();
#plt.plot(all_iter, all_acc, '-*');
#plt.grid();
#plt.savefig('/home/wangjianfeng/tmp/a.png');

# plot result in /1_4/
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_4/a';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_227_2_128_2_128_initby0/a'
##snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_227_2_227/a';
##snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a';
#[all_acc, all_iter] = ca.collect_test1(snap_prefix + ''.replace(',', '_') + '_' + ''.replace(',', '_'), 
        #'test_1_227_2_227.prototxt');
#print all_acc.max()
#plt.close();
#plt.plot(all_iter, all_acc, '-*');
#plt.grid();
#plt.savefig('/home/wangjianfeng/tmp/a.png');

# remove some model;
#all_files = [f for f in glob.glob('/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a_base_lr:0.0001_iter_*')
        #if f.endswith('solverstate') or f.endswith('caffemodel')];
#to_be_deleted = [];
#for f in all_files:
    #num = f[f.rfind('_') + 1 : f.rfind('.')];
    #num = float(num);
    #if num > 600:
        #print f;
        #to_be_deleted.append(f);
#cmd = [];
#cmd.append('del');
#cmd.extend(to_be_deleted);
#subprocess.call(cmd);

# exract the features;
#p = [None] * 4;
#pretrained = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a__iter_300.caffemodel';
#net_def = './test_1_227_2_227.prototxt';
#blob_name = 'fc8_cp';
#save_file = '/home/wangjianfeng/tmp/fc8_cp.score.float.bin';
#phase = 'test';
#num_batch = 50;
#ca.set_device_ids([0]);
#p[0] = ca.extract_features1(pretrained, net_def, blob_name,
                          #save_file, num_batch, phase);
#p[0].communicate();



# fine-tune 0 
#net_base = './train_val.prototxt';
net_base = './tmp.prototxt';
solver_base = './solver.prototxt';
net_revise = '';
solver_revise = '';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a';
snap_prefix = '/home/wangjianfeng/tmp/a';
#finetune = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
finetune = '';
continuing = ''; 
ca.set_device_ids([0, 1, 2]);
ca.set_random_seeds([1, 2, 3]);
p_train, t = ca.train_revise_solver(net_base, solver_base, 
        net_revise, solver_revise, 
        snap_prefix, finetune, continuing);
#test_net = './test_1_227_2_227.prototxt';
#ca.set_device_ids([3]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);

# fine-tune 1
#net_base = './train_val.prototxt';
#solver_base = './solver.prototxt';
#net_revise = '';
#solver_revise = 'base_lr:0.0001';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a';
#finetune = '';
#continuing = snap_prefix + '__iter_300.solverstate';
#ca.set_device_ids([0, 1, 2]);
#ca.set_random_seeds([1, 2, 3]);
##p_train, t = ca.train_revise_solver(net_base, solver_base, 
        ##net_revise, solver_revise, 
        ##snap_prefix, finetune, continuing);
#test_net = './train_val_1_4.prototxt';
#ca.set_device_ids([1]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);
#p_train.communicate();

# fine-tune 2
#net_base = './train_val.prototxt';
#solver_base = './solver.prototxt';
#net_revise = '';
#solver_revise = 'base_lr:0.00001';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a';
#finetune = '';
#continuing = snap_prefix + '_base_lr:0.0001_iter_600.solverstate';
#ca.set_device_ids([0, 1, 2]);
#ca.set_random_seeds([1, 2, 3]);
#p_train, t = ca.train_revise_solver(net_base, solver_base, 
        #net_revise, solver_revise, 
        #snap_prefix, finetune, continuing);
#test_net = './train_val_1_4.prototxt';
#ca.set_device_ids([0]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);
#p_train.communicate();

# fine-tune 3
#net_base = './train_1_227_2_227.prototxt';
#solver_base = './solver.prototxt';
#net_revise = '';
#solver_revise = 'base_lr:0.00001';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_227_2_227/a';
#finetune = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a' + '_base_lr:0.00001_iter_900.caffemodel';
#continuing = '';
#ca.set_device_ids([0, 1, 2]);
#ca.set_random_seeds([1, 2, 3]);
#p_train, t = ca.train_revise_solver(net_base, solver_base, 
        #net_revise, solver_revise, 
        #snap_prefix, finetune, continuing);
#test_net = 'test_1_227_2_227.prototxt';
#ca.set_device_ids([3]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);
#p_train.communicate();

# fine-tune 3.1
#net_base = './train_1_227_2_128_2_128.prototxt';
#solver_base = './solver.prototxt';
#net_revise = '';
#solver_revise = '';
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_227_2_128_2_128_initby0/a';
##finetune = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1/a' + '_base_lr:0.00001_iter_900.caffemodel';
#finetune = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
#continuing = '';
#ca.set_device_ids([0, 1, 2]);
#ca.set_random_seeds([1, 2, 3]);
##p_train, t = ca.train_revise_solver(net_base, solver_base, 
        ##net_revise, solver_revise, 
        ##snap_prefix, finetune, continuing);
#test_net = 'test_1_227_2_227_2_128.prototxt';
#ca.set_device_ids([3]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);
#p_train.communicate();


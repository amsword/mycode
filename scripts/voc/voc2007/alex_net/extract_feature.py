import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd


ca = caffe_cmd();

ca.set_machine('deep07');

p = [None] * 4;
pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
net_def = './extract_train.prototxt';
blob_name = 'fc7';
save_file = '/home/wangjianfeng/working/voc/voc2007/alex_net/stdmodel.train.fc7.float.bin';
phase = 'test';
num_batch = 5011;
ca.set_device_ids([0]);
p[0] = ca.extract_features1(pretrained, net_def, blob_name,
                          save_file, num_batch, phase);

pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
net_def = './extract_train.prototxt';
blob_name = 'label';
save_file = '/home/wangjianfeng/working/voc/voc2007/alex_net/stdmodel.train.label.float.bin';
phase = 'test';
num_batch = 5011;
ca.set_device_ids([1]);
p[1] = ca.extract_features1(pretrained, net_def, blob_name,
                          save_file, num_batch, phase);

pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
net_def = './extract_test.prototxt';
blob_name = 'fc7';
save_file = '/home/wangjianfeng/working/voc/voc2007/alex_net/stdmodel.test.fc7.float.bin';
phase = 'test';
num_batch = 4952;
ca.set_device_ids([2]);
p[2] = ca.extract_features1(pretrained, net_def, blob_name,
                          save_file, num_batch, phase);

pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
net_def = './extract_test.prototxt';
blob_name = 'label';
save_file = '/home/wangjianfeng/working/voc/voc2007/alex_net/stdmodel.test.label.float.bin';
phase = 'test';
num_batch = 4952;
ca.set_device_ids([3]);
p[3] = ca.extract_features1(pretrained, net_def, blob_name,
                          save_file, num_batch, phase);

for sub in p:
    sub.communicate();

import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

# from com_utility import *
from caffe_cmd import caffe_cmd
# from subprocess import *

ca = caffe_cmd()
ca.set_machine("deep06")
ca.set_device_ids([0])

pretrained = '/home/wangjianfeng/data/vgg_model/' + \
    'VGG_ILSVRC_16_layers.caffemodel'
net_def = './VGG_ILSVRC_16_layers_extract.prototxt'

num_mini_batch = 4952  # 8 x 619
phase = 'test';

blob_name = 'fc7'
save_file = '/home/wangjianfeng/working/voc/voc2007/vgg_model/' + \
    'stdmodel.test.fc7.float.bin'
p = ca.extract_features1(pretrained, net_def, blob_name,
                         save_file, num_mini_batch, phase)

blob_name = 'label'
save_file = '/home/wangjianfeng/working/voc/voc2007/vgg_model/' + \
    'stdmodel.test.label.float.bin'
p = ca.extract_features1(pretrained, net_def, blob_name,
                         save_file, num_mini_batch, phase)


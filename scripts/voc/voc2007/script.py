import glob;
import os;
import subprocess

all_files = [f for f in glob.glob('a__iter_*')
        if f.endswith('solverstate') or f.endswith('caffemodel')];
print all_files
to_be_deleted = [];
for f in all_files:
    num = f[f.rfind('_') + 1 : f.rfind('.')];
    num = float(num);
    if num > 3000:
        to_be_deleted.append(f);
cmd = [];
cmd.append('/home/wangjianfeng/bin/del');
cmd.extend(to_be_deleted);
subprocess.call(cmd);

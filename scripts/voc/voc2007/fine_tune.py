python_code = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_code);

from caffe_cmd import caffe_cmd;

ca = caffe_cmd();

ca.set_device_ids([0, 1, 2]);
ca.set_random_seeds([1, 2, 3]);
ca.set_machine('deep11');

net_base = './VGG_ILSVRC_16_layers_train.prototxt';
solver_base = './vgg_solver.prototxt';

net_revise = '';
solver_revise = '';
snap_prefix = '/home/wangjianfeng/working/voc/voc2007/vgg_model/fine_tune/' + \
        'stdmodel_';

finetune = '/home/wangjianfeng/data/vgg_model/VGG_ILSVRC_16_layers.caffemodel';

ca.train_revise_solver(net_base, solver_base, 
        net_revise, solver_revise, 
        snap_prefix, finetune);

python_code = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_code);

from caffe_cmd import caffe_cmd;

ca = caffe_cmd();
ca.set_machine('deep07');
ca.set_device_ids([0]);
net_file = './VGG_ILSVRC_16_layers_train.prototxt';
weights = '/home/wangjianfeng/data/vgg_model/VGG_ILSVRC_16_layers.caffemodel';
num_iter = 619;
ca.test(net_file, weights, num_iter);


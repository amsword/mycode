python_code = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_code);

from caffe_cmd import caffe_cmd;

ca = caffe_cmd();
ca.set_machine('deep07');
ca.set_device_ids([0]);

net_file = './train_val.prototxt';
weights = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
num_iter = 619;
p = ca.test(net_file, weights, num_iter);
p.communicate();

#net_file = './train_val_1_4.prototxt';
#weights = '/home/wangjianfeng/working/voc/voc2007/alex_net/fine_tune/' + \
        #'cont_1000_2000_base_lr:0.00001_iter_3000.caffemodel'
#num_iter = 619;
#ca.test(net_file, weights, num_iter);

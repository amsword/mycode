import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)
import matplotlib;
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd
import numpy
import glob
import subprocess


ca = caffe_cmd();
ca.set_machine('deep07');

# remove some model;
#all_files = [f for f in glob.glob('/home/wangjianfeng/working/voc/voc2007/alex_net/softmax_include_0_label/agamma:3__iter_*')
        #if f.endswith('solverstate') or f.endswith('caffemodel')];
#to_be_deleted = [];
#for f in all_files:
    #num = f[f.rfind('_') + 1 : f.rfind('.')];
    #num = float(num);
    #if num > 500:
        #print f;
        #to_be_deleted.append(f);
#cmd = [];
#cmd.append('del');
#cmd.extend(to_be_deleted);
#subprocess.call(cmd);
#assert False;

# fine-tune 0 
pretrained = '/home/wangjianfeng/data/alex_net/bvlc_alexnet.caffemodel';
#net_base = './train_softmax.prototxt';
net_base = './train_softmax_last.prototxt';
#net_base = './train_no_dropout.prototxt';
#net_base = './tmp.prototxt';
solver_base = './solver.prototxt';
#net_revise = '';
net_revise = '';
#solver_revise = '';
solver_revise = '';
snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/softmax_no_dropout/a'
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/softmax_include_0_label/last'
#snap_prefix = '/home/wangjianfeng/working/voc/voc2007/alex_net/softmax/a'
finetune = pretrained;
#finetune = '';
#continuing = snap_prefix + 'gamma:2__iter_800.solverstate'; 
continuing = '';
#ca.set_device_ids([0, 1, 2]);
#ca.set_random_seeds([1, 2, 3]);
ca.set_device_ids([0]);
ca.set_random_seeds([1]);

p_train, t = ca.train_revise_solver(net_base, solver_base, 
        net_revise, solver_revise, 
        snap_prefix, finetune, continuing);
test_net = './test_softmax.prototxt';
#ca.set_device_ids([3]);
ca.set_device_ids([2]);
p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);

#solver_revise = 'base_lr:0.0005'
#continuing = snap_prefix + '_' + '_iter_1000.solverstate';
#p_train, t = ca.train_revise_solver(net_base, solver_base, 
        #net_revise, solver_revise, 
        #snap_prefix, finetune, continuing);
#test_net = './test_softmax.prototxt';
#ca.set_device_ids([3]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 619);

#test_net = './test_train_loss.prototxt'
#ca.set_device_ids([3]);
#p_test = ca.find_test1(test_net, snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 5011);

#[all_acc, all_iter] = ca.collect_test1(snap_prefix + ''.replace(',', '_') + '_' + ''.replace(',', '_'), 'test_train_loss.prototxt', 'Loss: ');
[all_acc, all_iter] = ca.collect_test1(snap_prefix + net_revise.replace(',', '_') + '_' + solver_revise.replace(',', '_'), 'test_softmax.prototxt'); 
plt.close();
plt.plot(all_iter, all_acc, '-o');
print numpy.max(all_acc);
print all_iter[numpy.argmax(all_acc)]
plt.grid();
plt.savefig('/home/wangjianfeng/tmp/b.png');


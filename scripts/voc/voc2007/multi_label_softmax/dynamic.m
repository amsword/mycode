%/home/wangjianfeng/code/caffe/build/tools/extract_keys_to_txt.bin -input_type lmdb -input /home/wangjianfeng/data/voc2007/VOCdevkit/lmdb/trainval -output /home/wangjianfeng/data/voc2007/VOCdevkit/lmdb/trainval.keys
%/home/wangjianfeng/code/caffe/build/tools/extract_keys_to_txt.bin -input_type lmdb -input /home/wangjianfeng/data/voc2007/VOCdevkit/lmdb/test -output /home/wangjianfeng/data/voc2007/VOCdevkit/lmdb/test.keys

%run('/home/wangjianfeng/code/mycode/matlab_code/jf_conf.m');

% check the output of the data layer
%x = read_mat('/home/wangjianfeng/tmp/data.float.bin', 'float');
%num_image = size(x, 2);
%x = reshape(x, [227 227 3 num_image]);
%for idx_image = 1 : num_image
    %for idx_channel = 1 : 3
        %x(:, :, idx_channel, idx_image) = x(:, :, idx_channel, idx_image)';
    %end
    %t = x(:, :, 1, idx_image);
    %x(:, :, 1, idx_image) = x(:, :, 3, idx_image);
    %x(:, :, 3, idx_image) = t;
%end
%x(:) = x(:) + 114;
%x = uint8(x);
%num_hyper_image = num_image / 5;
%for idx_image = 1 : num_hyper_image
    %for idx_crops = 1 : 5
        %im = x(:, :, :, (idx_image - 1) * 5 + idx_crops);
        %imwrite(im, ['/home/wangjianfeng/tmp/' num2str(idx_image) '_' num2str(idx_crops) '.jpg']);
    %end
%end

% check which image is not correctly classified
folder_name = '/home/wangjianfeng/working/voc/voc2007/alex_net/';
file_test_label = [folder_name 'stdmodel.test.label.float.bin'];
save_file = '/home/wangjianfeng/tmp/fc8_cp.score.float.bin';
scores = read_mat(save_file, 'float');
labels = read_mat(file_test_label, 'float');
count = min(size(scores, 2), size(labels, 2));
is_good = ones(count, 1);
for i = 1 : count
    curr_score = scores(:, i);
    curr_label = labels(:, i);
    if find(curr_score(curr_label == 1) < 0)
        is_good(i) = 0;
    end
end


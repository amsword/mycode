import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd


ca = caffe_cmd();

ca.set_machine('deep07');
ca.set_device_ids([3]);

folder = '/home/wangjianfeng/working/voc/voc2007/alex_net/fine_tune/'
#prefix = 'stdmodel__'
prefix = 'cont_1000_2000_base_lr:0.00001'
#print ca.collect_test(folder, prefix);
ca.find_test(folder, prefix, 619);

#folder = '/home/wangjianfeng/working/voc/voc2007/alex_net/multi_crop/1_4/'
#prefix = 'a_base_lr:0.01'
#ca.find_test(folder, prefix, 619);

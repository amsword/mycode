python_code = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_code);

from caffe_cmd import caffe_cmd;
ca = caffe_cmd();

ca.set_device_ids([0, 1, 2]);
ca.set_random_seeds([1, 2, 3]);
ca.set_machine('deep07');

net_base = './train_val.prototxt';
solver_base = './solver.prototxt';
net_revise = '';
solver_revise = '';
snap_prefix = '/home/wangjianfeng/working/cifar10/triangle/baseline/a';

finetune = '';
continuing = '';

p_train, t = ca.train_revise_solver(net_base, solver_base, 
        net_revise, solver_revise, 
        snap_prefix, finetune, continuing);

test_net = './train_val.prototxt';
ca.set_device_ids([3]);
ca.find_test1(test_net, snap_prefix, 100);

p_train.communicate();
#[all_acc, all_iter] = ca.collect_test1(snap_prefix);

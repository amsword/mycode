import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd

folder = '/home/wangjianfeng/working/voc/voc2007/vgg_model/fine_tune/'
prefix = 'stdmodel__'

ca = caffe_cmd();

ca.set_machine('deep07');
ca.set_device_ids([1]);

ca.find_test(folder, prefix, 619);

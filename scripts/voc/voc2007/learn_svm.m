addpath('/home/wangjianfeng/code/rcnn/bin'); % liblinear
run('/home/wangjianfeng/code/mycode/matlab_code/jf_conf.m');

folder_name = '/home/wangjianfeng/working/voc/voc2007/vgg_model/';
file_train_feature = [folder_name 'stdmodel.trainval.fc7.float.bin'];
file_train_label = [folder_name 'stdmodel.trainval.label.float.bin'];
file_test_feature = [folder_name 'stdmodel.test.fc7.float.bin'];
file_test_label = [folder_name 'stdmodel.test.label.float.bin'];
is_norm = false;
if ~is_norm
    file_save_projection = [folder_name 'stdmodel.svm.nonorm.projection.float'];
    file_save_bias = [folder_name 'stdmodel.svm.nonorm.bias.float'];
    file_info = [folder_name 'stdmodel.svm.nonorm.info'];
else
    file_save_projection = [folder_name 'stdmodel.svm.norm.projection.float'];
    file_save_bias = [folder_name 'stdmodel.svm.norm.bias.float'];
    file_info = [folder_name 'stdmodel.svm.norm.info'];
end

% read training data
train_feature = read_mat(file_train_feature, 'float');
if is_norm
    train_feature = bsxfun(@rdivide, train_feature, sqrt(sum(train_feature .^ 2, 1)));
end
train_label = read_mat(file_train_label, 'float');

% read testing data
test_feature = read_mat(file_test_feature, 'float');
if is_norm
    test_feature = bsxfun(@rdivide, test_feature, sqrt(sum(test_feature .^ 2, 1)));
end
test_label = read_mat(file_test_label, 'float');

all_c = 10 .^ [-5 -4 -3, -2, -1, 0, 1, 2];
num_labels = size(train_label, 1); 

all_acc_train = zeros(num_labels, numel(all_c));
all_acc_test = all_acc_train;

dim = size(train_feature, 1);
result_w = zeros(dim, num_labels);
result_b = zeros(num_labels, 1);

best_acc_test = -1;
best_c = 0;
for idx_c = 1 : numel(all_c)
    c = all_c(idx_c);

    acc_train = zeros(num_labels, 1);
    acc_test = acc_train;
    for i = 1 : num_labels
        y = train_label(i, :)';
        y(y == 0) = 1;
        model = liblinear_train(y, sparse(train_feature), ...
        ['-w1 1 -c ' num2str(c) ' -B 1 -s 3'], 'col');
        w = model.w(1 : end - 1)';
        b = model.w(end);
        result_w(:, i) = w;
        result_b(i) = b;

        feature = train_feature;
        label = train_label(i, :);
        prediction = bsxfun(@plus, w' * feature, b);
        [t, s_idx] = sort(prediction, 'descend');
        s_score = label(s_idx);
        s_score(s_score == 0) = [];
        s_score(s_score == -1) = 0;
        all_ap = cumsum(s_score) ./ [1 : numel(s_score)];
        acc_train(i) = mean(all_ap(s_score == 1));

        feature = test_feature;
        label = test_label(i, :);
        prediction = bsxfun(@plus, w' * feature, b);
        [t, s_idx] = sort(prediction, 'descend');
        s_score = label(s_idx);
        s_score(s_score == 0) = [];
        s_score(s_score == -1) = 0;
        all_ap = cumsum(s_score) ./ [1 : numel(s_score)];
        acc_test(i) = mean(all_ap(s_score == 1));
    end
    all_acc_train(:, idx_c) = acc_train;
    all_acc_test(:, idx_c) = acc_test;
    if mean(acc_test) > best_acc_test
        best_acc_test = mean(acc_test);
        best_w = result_w;
        best_b = result_b;
        best_c = c;
    end
end
save_mat(best_w, file_save_projection, 'float');
save_mat(best_b, file_save_bias, 'float');

mean(all_acc_train, 1)
mean(all_acc_test, 1)
fp = fopen(file_info, 'w');
for idx_c = 1 : numel(all_c)
    c = all_c(idx_c);
    fprintf(fp, 'c = %f; acc_train = %f; acc_test = %f\n', ...
        c, mean(all_acc_train(:, idx_c)), ...
        mean(all_acc_test(:, idx_c)));
end
fprintf(fp, 'saved c is %f\n', best_c);
fclose(fp);
%para = liblinear_train();

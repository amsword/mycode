utility_code = '/home/wangjianfeng/code/mycode/matlab_code/jf_conf.m';
run(utility_code);

sp1 = read_mat('/home/wangjianfeng/working/cifar10/plus1_loss/merge/finetunelower_bound:0.8_upper_bound:1_base_lr:0.01_iter_120000.caffemodel_extract_test_softmax', 'float');
sm = read_mat('/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_120000.caffemodel_extract_test_softmax', 'float');
label = read_mat('/home/wangjianfeng/data/cifar10/preprocessed/cifar10-lmdb/cifar-test-lmdb_label', 'int32');

[~, sm_predict] = max(sm, [], 1);
sm_predict = sm_predict - 1;
sm_correct = sm_predict == label;
sum(sm_correct) / numel(sm_correct)

[~, sp1_predict] = max(sp1(1 : end - 1, :), [], 1);
sp1_predict = sp1_predict - 1;
sp1_correct = sp1_predict == label;
sum(sp1_correct) / numel(sp1_correct)

selected = sp1_correct & (~sm_correct);
%selected = ~sp1_correct & sm_correct;

sum(selected)
s_idx = find(selected);

for i = 1 : 10
    idx = s_idx(i);
    sp1(:, idx)
    sm(:, idx)
    label(idx)
end

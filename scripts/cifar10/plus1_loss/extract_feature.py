import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);
from com_utility import * 

#caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"
#pretrained = '/home/wangjianfeng/working/cifar10/plus1_loss/merge/finetunelower_bound:0.8_upper_bound:1_base_lr:0.01_iter_120000.caffemodel';
#net_proto = 'cifar10_extract_net.prototxt'
#blob_name = 'softmax'; 
#save_file = pretrained + "_extract_test_" + blob_name; 
#device_id = 1
#machine_name = "deep07";
#num_mini_batches = 100;
#phase = 'test';

#cmd = caffe_dir + "extract_features1" + " " + \
    #pretrained + " " + net_proto + " " + \
    #blob_name + " " + save_file + " " + \
    #str(num_mini_batches) + " " + phase + " " + \
    #'GPU DEVICE_ID=' + str(device_id);

#p = throw_cmd(machine_name, cmd, pretrained + 'extract_test.log');
#p.communicate();

caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"
pretrained = '/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_120000.caffemodel';
net_proto = '/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/extract_net.prototxt'
blob_name = 'softmax'; 
save_file = '/home/wangjianfeng/working/cifar10/nin/baseline_new_caffe/base_iter_120000.caffemodel_extract_test_softmax'
device_id = 1
machine_name = "deep07";
num_mini_batches = 100;
phase = 'test';

cmd = caffe_dir + "extract_features1" + " " + \
    pretrained + " " + net_proto + " " + \
    blob_name + " " + save_file + " " + \
    str(num_mini_batches) + " " + phase + " " + \
    'GPU DEVICE_ID=' + str(device_id);

p = throw_cmd(machine_name, cmd, pretrained + 'extract.log');
p.communicate();

import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)
            for lb in range(8, 9) for up in range(10, 11) if up >= lb]
all_device_id, all_machine_name = get_free_gpu(['10', '12', '05', '04', '01']);

num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
model_type = "merge";
#assert False
for i in range(num):
    net_base = "cifar10_train_val_" + model_type + ".prototxt";
    solver_base = "cifar10_solver_merge.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "base_lr:0.01";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/cifar10/plus1_loss/" + \
        model_type + "/fine_tune_";
    pre_snap_prefix = "/home/wangjianfeng/working/cifar10/plus1_loss/" + \
        model_type + "/direct";
    #finetune = r"/home/wangjianfeng/working/cifar10/plus1_loss/" + model_type + "/direct_lower_bound:0.9_upper_bound:1.0_max_iter:200000_iter_120000.caffemodel";
    continuing = "";
    finetune = concat_snap_prefix(pre_snap_prefix,
                                  'lower_bound:0.9,upper_bound:1', '') + \
        '_iter_120000.caffemodel';

    p, log_file_name = train_revise_solver_asyc(net_base, solver_base,
        net_revise, solver_revise,
    	machine_name, device_id, caffe_dir, snap_prefix,
	    finetune, continuing);
    all_procs[i] = (p, log_file_name);

print [p.poll() for p, log_file_name in all_procs];


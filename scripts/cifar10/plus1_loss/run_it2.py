import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(6, 9) for up in range(10, 11) if up >= lb]
all_device_id, all_machine_name = get_free_gpu(['10', '12', '15', '05', '04', '03', '01']);

num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
model_type = "merge";
#assert False
for i in range(num):
    net_base = "cifar10_train_val_" + model_type + ".prototxt";
    solver_base = "cifar10_solver_merge.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "max_iter:200000";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/cifar10/plus1_loss/" + model_type + "/fine_tune_l0.9u1_";
    #continuing = r"/home/wangjianfeng/working/cifar10/plus1_loss/" + model_type + 
            #r"/direct_lower_bound:0.9_upper_bound:1.0_max_iter:200000_iter_100000.solverstate";
    continuing = "";
    finetune = r"/home/wangjianfeng/working/cifar10/plus1_loss/" + model_type + \
            r"/direct_continue_lower_bound:0.9_upper_bound:1.0_max_iter:200000_iter_107500.caffemodel";

    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
        net_revise, solver_revise, 
    	machine_name, device_id, caffe_dir, snap_prefix, 
	    finetune, continuing);
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];


#!/usr/bin/env sh 

TOOLS=/home/wangjianfeng/code/caffe/build/tools
INPUT_FILE=/home/wangjianfeng/data/cifar10/lmdb/cifar10-train-lmdb
OUTPUT_FILE=/home/wangjianfeng/working/cifar10/knncnn/cifar10-train-lmdb.keys

${TOOLS}/extract_keys.bin \
		-input_type lmdb -input ${INPUT_FILE} \
		-output ${OUTPUT_FILE}
echo "done"


#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/caffe/build/tools

INPUT=/home/wangjianfeng/data/cifar10/lmdb/cifar10-train-lmdb
OUTPUT=/home/wangjianfeng/working/cifar10/knncnn/cifar10-train-lmdb.labels

${TOOLS}/extract_labels.bin -input_type lmdb -input ${INPUT} -output ${OUTPUT}

INPUT=/home/wangjianfeng/data/cifar10/lmdb/cifar10-test-lmdb
OUTPUT=/home/wangjianfeng/working/cifar10/knncnn/cifar10-test-lmdb.labels

${TOOLS}/extract_labels.bin -input_type lmdb -input ${INPUT} -output ${OUTPUT}

echo "done"

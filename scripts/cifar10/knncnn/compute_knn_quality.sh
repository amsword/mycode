#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/mycode/c_code/bin
OUTPUT_LAYER=softmax
QUERY=/home/wangjianfeng/working/cifar10/knncnn/cifar10-test-lmdb.labels
DATABASE=/home/wangjianfeng/working/cifar10/knncnn/cifar10-train-lmdb.labels
RETRIEVAL_RESULT=/home/wangjianfeng/working/cifar10/knncnn/${OUTPUT_LAYER}_query.matrix.int
OUTPUT=/home/wangjianfeng/working/cifar10/knncnn/${OUTPUT_LAYER}_knn_quality.txt

${TOOLS}/SemanticLabel ${QUERY} ${DATABASE} ${RETRIEVAL_RESULT} ${OUTPUT}

echo "done"

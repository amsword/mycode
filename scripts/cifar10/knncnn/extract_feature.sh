#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/caffe/build/tools
MODEL=/home/wangjianfeng/working/cifar10/active_set/cifar10_full_iter_50000
PROTOCOL=cifar10_full_extract_train.prototxt
OUTPUT_LAYER=softmax
OUTPUT_FILE=/home/wangjianfeng/working/cifar10/knncnn/features_train_${OUTPUT_LAYER}.matrix.float
BATCH_SIZE=500
TEE_OUTPUT=${OUTPUT_FILE}.txt

${TOOLS}/extract_features.bin \
	${MODEL} \
	${PROTOCOL} \
	${OUTPUT_LAYER} \
	${OUTPUT_FILE} \
	${BATCH_SIZE} \
	GPU DEVICE_ID=1 2>&1 | tee ${TEE_OUTPUT}

#PROTOCOL=cifar10_full_extract_test.prototxt
#OUTPUT_FILE=/home/wangjianfeng/working/cifar10/knncnn/features_test_${OUTPUT_LAYER}.matrix.float
#BATCH_SIZE=100
#${TOOLS}/extract_features.bin \
	#${MODEL} \
	#${PROTOCOL} \
	#${OUTPUT_LAYER} \
	#${OUTPUT_FILE} \
	#${BATCH_SIZE} \
	#GPU DEVICE_ID=1 2>&1 | tee ${TEE_OUTPUT}
echo "done"


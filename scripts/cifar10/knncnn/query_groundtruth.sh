#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/mycode/c_code/bin
OUTPUT_LAYER=softmax
QUERY=/home/wangjianfeng/working/cifar10/knncnn/feature_test_mat_float_${OUTPUT_LAYER}
DATABASE=/home/wangjianfeng/working/cifar10/knncnn/feature_mat_float_${OUTPUT_LAYER}

OUTPUT_KNN=/home/wangjianfeng/working/cifar10/knncnn/${OUTPUT_LAYER}_query.gnd.float
OUTPUT_INDEX=/home/wangjianfeng/working/cifar10/knncnn/${OUTPUT_LAYER}_query.matrix.int
KNN=1000

${TOOLS}/BruteForceNNUsingDll.bin ${QUERY} ${DATABASE} ${OUTPUT_KNN} ${KNN}
${TOOLS}/ConvertGNDToMatrixIndex.bin ${OUTPUT_KNN} ${OUTPUT_INDEX}


echo "done"


#!/usr/bin/env sh

TOOLS=/home/wangjianfeng/code/caffe/build/tools
export GOOGLE_LOG_DIR=/home/wangjianfeng/glogs
GPU_ID=3
OUTPUT_TXT=/home/wangjianfeng/working/cifar10/knncnn/test_proposed${GPU_ID}.txt
MODEL=/home/wangjianfeng/working/cifar10/active_set/cifar10_full_iter_50000
WORKING_DIR=/home/wangjianfeng/working/cifar10/knncnn/

#$TOOLS/test_net.bin cifar10_full_test.prototxt\
		#${MODEL} 100 GPU 1 \
		#2>&1 | tee -a ${WORKING_DIR}baseline.txt

#$TOOLS/test_net_knn.bin \
    #cifar10_full_test_knn.prototxt \
	#${MODEL} \ 
	#2>&1 | tee ${WORKING}proposed.txt

$TOOLS/test_net_knn.bin \
	cifar10_full_solver_knn.prototxt \
	${MODEL} \
	2>&1 | tee -a ${OUTPUT_TXT}

#
echo "Done."

# sh test_proposed.sh

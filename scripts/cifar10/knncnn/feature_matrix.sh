#!/usr/bin/env sh


TOOLS=/home/wangjianfeng/code/caffe/build/tools
OUTPUT_LAYER=softmax
FEATURE_FILE=/home/wangjianfeng/working/cifar10/knncnn/features_leveldb_${OUTPUT_LAYER}
MAT_FILE=/home/wangjianfeng/working/cifar10/knncnn/feature_mat_float_${OUTPUT_LAYER}
TEE_OUTPUT=${MAT_FILE}.txt
COUNT=50000

${TOOLS}/database_to_mat.bin \
	-input_type leveldb \
	-input ${FEATURE_FILE} \
	-output ${MAT_FILE} \
	-count ${COUNT}
	2>&1 | tee -a ${TEE_OUTPUT}

FEATURE_FILE=/home/wangjianfeng/working/cifar10/knncnn/features_test_leveldb_${OUTPUT_LAYER}
MAT_FILE=/home/wangjianfeng/working/cifar10/knncnn/feature_test_mat_float_${OUTPUT_LAYER}
TEE_OUTPUT=${MAT_FILE}.txt
COUNT=10000

${TOOLS}/database_to_mat.bin \
	-input_type leveldb \
	-input ${FEATURE_FILE} \
	-output ${MAT_FILE} \
	-count ${COUNT}
	2>&1 | tee -a ${TEE_OUTPUT}
echo "done"


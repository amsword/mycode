#!/usr/bin/env sh
# This script converts the cifar data into leveldb format.

EXAMPLES=/home/jianfeng/caffe/build/examples/cifar10
DATA=/home/jianfeng/data/cifar10
TOOLS=/home/jianfeng/code/caffe/build/tools

echo "Creating leveldb..."

rm -rf cifar10-lmdb

#$EXAMPLES/convert_cifar_data.bin $DATA ./cifar10-leveldb
#$TOOLS/convert_to_database.bin -input_type leveldb \
		#-input $DATA/cifar10-leveldb/cifar-train-leveldb \
		#-output_type lmdb \
		#-output $DATA/cifar10-lmdb/cifar-train-lmdb

$TOOLS/convert_to_database.bin -input_type leveldb \
		-input $DATA/cifar10-leveldb/cifar-test-leveldb \
		-output_type lmdb \
		-output $DATA/cifar10-lmdb/cifar-test-lmdb
#echo "Computing image mean..."

#$TOOLS/compute_image_mean.bin ./cifar10-leveldb/cifar-train-leveldb mean.binaryproto

echo "Done."

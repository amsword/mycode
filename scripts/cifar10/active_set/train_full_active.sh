#!/usr/bin/env sh

TOOLS=~/code/caffe/build/tools
WORKING=/home/jianfeng/working/cifar10/active_set
export GOOGLE_LOG_DIR=/home/jianfeng/glogs


#$TOOLS/train_net.bin cifar10_full_solver_.prototxt \
		#2>&1 | tee ${WORKING}/a.txt

$TOOLS/train_net.bin cifar10_full_solver_.prototxt \
		2>&1 | tee ${WORKING}/active_set.txt

#!/usr/bin/env sh

TOOLS=~/caffe/build/tools


$TOOLS/train_net.bin cifar10_quick_solver_.prototxt \
		2>&1 | tee b.txt

$TOOLS/train_net.bin cifar10_quick_solver.prototxt \
	2>&1 | tee a.txt

#reduce learning rate by fctor of 10 after 8 epochs
$TOOLS/train_net.bin cifar10_quick_solver_lr1.prototxt cifar10_quick_iter_4000.solverstate \
		2>&1 | tee -a a.txt

#!/usr/bin/env sh

TOOLS=~/caffe/build/tools
export GOOGLE_LOG_DIR=/home/jianfeng/glogs

#$TOOLS/train_net.bin \
    #cifar10_full_solver.prototxt \
	#2>&1 | tee fa.txt

##reduce learning rate by factor of 10
#$TOOLS/train_net.bin \
    #cifar10_full_solver_lr1.prototxt \
    #cifar10_full_iter_60000.solverstate \
	#2>&1 | tee -a fa.txt

##reduce learning rate by factor of 10
#$TOOLS/train_net.bin \
    #cifar10_full_solver_lr2.prototxt \
    #cifar10_full_iter_65000.solverstate \
	#2>&1 | tee -a fa.txt

$TOOLS/train_net.bin \
    cifar10_full_solver_.prototxt

#!/usr/bin/python

import sys
import shlex
#print "hello"
#file_name_in = "/home/wangjianfeng/code/examples/cifar10/nin/cifar10_solver_sf.prototxt"
file_name_in = sys.argv[1]

#file_name_out = "a.txt"
file_name_out = sys.argv[3]

#patterns = "4:net:\"abc.prototxt\",7:#"
#patterns = "4:net:\"abc.prototxt\""
patterns = sys.argv[2]
#split_patterns = shlex.split(patterns);
split_patterns = patterns.split(",")

fin = open(file_name_in, "r")
fout = open(file_name_out, "w")

lines = fin.readlines()
line_number = 0
for line in lines:
	line_number = line_number + 1
	is_written = False
	for p in split_patterns:
		one_pattern = p.split(":", 1)
		if one_pattern[0].isdigit():
			if int(one_pattern[0]) == line_number:
				if one_pattern[1].split(":")[0] not in line:
					print one_pattern[1].split(":")[0], line
				fout.writelines(one_pattern[1] + "\n")
				is_written = True
	if is_written == False: 
		fout.writelines(line)

fout.close()
fin.close()

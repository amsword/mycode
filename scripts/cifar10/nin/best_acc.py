#!/usr/bin/python

import sys

log_file = sys.argv[1];

fp = open(log_file, "r");

lines = fp.readlines();
print "number of lines: ", len(lines)
best_acc = 0;
for line in lines:
	if "Test net output #0:" in line:
		split_line = line.split("=");
		assert len(split_line) == 2;
		acc = float(split_line[1]);
		if best_acc < acc:
			best_acc = acc;

print "best acc: ", best_acc

fp.close();

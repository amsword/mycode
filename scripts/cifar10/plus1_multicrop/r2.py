import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd

model_type = "data_augmented"
net_base = "cifar10_train_val_data_augmented.prototxt"
#net_base = "cifar10_net_300x.prototxt"
solver_base = "cifar10_solver_merge.prototxt"
net_revise = ""
solver_revise = ""
#solver_revise = ""
snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/b4"

pre_snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/b3"
continuing = concat_snap_prefix(pre_snap_prefix, "", "") + \
    "_iter_550000.solverstate"

#continuing = ''
finetune = ''

all_device_id, all_machine_name = get_free_gpu_lsls()
#all_device_id = [[0, 1, 2, 3]]
#all_machine_name = ['deep09']
print all_device_id, all_machine_name
assert len(all_device_id) > 0

ca = caffe_cmd()
print all_machine_name, all_device_id

machine_name = all_machine_name[0]
print "set machine", machine_name
ca.set_machine(machine_name)

random_seeds = range(1, len(all_device_id[0]) + 1)
ca.set_random_seeds(random_seeds)
print all_device_id[0]
ca.set_device_ids(all_device_id[0])

#input('press any key')
p, log_file_name = ca.train_revise_solver(
    net_base, solver_base,
    net_revise, solver_revise,
    snap_prefix,
    finetune, continuing, True)


import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)
import time

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd
import os


folder = '/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/'
prefix = 'a_'
ca = caffe_cmd()
ca.set_machine("deep02");
ca.set_device_ids([0]);


while 1:
    all_files = [f for root, dirs, files in os.walk(folder)
            for f in files
            if len(f) > len(prefix) and 
                f[0: len(prefix)] == prefix and 
                prefix + "_iter_" in f and 
                not f.endswith('.solverstate') and 
                not f.endswith('.tee') and 
                not f.endswith('.prototxt')]
    
    for f in all_files:
        if not f.endswith('.caffemodel'):
            continue;
        finded = [f1 for f1 in all_files if len(f1) > len(f) and f in f1]
        if finded != []:
            assert len(finded) == 1;
            continue;
        net_file = folder + prefix + "_n.prototxt";
        model_file = folder + f;
        num_iter = 100
        print 'test ', f
        ca.test(net_file, model_file, num_iter, folder + f + ".test");
    print 'begin waiting'
    time.sleep(60 * 5);

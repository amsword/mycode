import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd

net_base = 'cifar10_net2.prototxt'
#net_base = "cifar10_train_val_data_augmented.prototxt"
solver_base = "cifar10_solver_merge.prototxt"
net_revise = ""
solver_revise = "base_lr:0.01"

if net_base == 'cifar10_net.prototxt':
    snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/net/a"
elif net_base == 'cifar10_net1_2.prototxt':
    snap_prefix = '/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/net/b'
elif net_base == 'cifar10_net2.prototxt':
    snap_prefix = '/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/net/b1'
else:
    assert False

#pre_snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/b2"
#continuing = concat_snap_prefix(pre_snap_prefix, "", "") + \
    #"_iter_350000.solverstate"

assert False
finetune = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/net/b_base_lr:0.01_iter_2500.caffemodel"
continuing = '';

all_device_id, all_machine_name = get_free_gpu_lsls()
print all_machine_name, all_device_id
assert len(all_device_id) > 0

ca = caffe_cmd()

idx = 0
machine_name = all_machine_name[idx]
print "set machine", machine_name
ca.set_machine(machine_name)

random_seeds = range(1, len(all_device_id[idx]) + 1)
ca.set_random_seeds(random_seeds)
print all_device_id[idx]
ca.set_device_ids(all_device_id[idx])

p, log_file_name = ca.train_revise_solver(
    net_base, solver_base,
    net_revise, solver_revise,
    snap_prefix,
    finetune, continuing, True)

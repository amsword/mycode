import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd

folder = '/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/net/'
prefix = 'b1_base_lr:0.01'

ca = caffe_cmd();

ca.set_machine('deep03');
ca.set_device_ids([0]);

ca.find_test(folder, prefix);

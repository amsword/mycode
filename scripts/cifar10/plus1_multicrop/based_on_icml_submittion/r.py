import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)

from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd

net_base = "cifar10_train_val_data_augmented.prototxt"
solver_base = "cifar10_solver_merge.prototxt"
net_revise = "grid_dim:2,cover_size:20"
solver_revise = "base_lr:0.001"
snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/a"

#pre_snap_prefix = "/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/b2"
#continuing = concat_snap_prefix(pre_snap_prefix, "", "") + \
    #"_iter_350000.solverstate"

continuing = ''

finetune = "/home/wangjianfeng/working/cifar10/plus1_loss/data_augmented/fine_tune_lower_bound:0.8_upper_bound:1.0_max_iter:200000_iter_134500.caffemodel"

all_device_id, all_machine_name = get_free_gpu_lsls()
print all_machine_name, all_device_id
assert len(all_device_id) > 0

ca = caffe_cmd()

idx = 0
machine_name = all_machine_name[idx]
print "set machine", machine_name
ca.set_machine(machine_name)

random_seeds = range(1, len(all_device_id[idx]) + 1)
ca.set_random_seeds(random_seeds)
print all_device_id[idx]
ca.set_device_ids(all_device_id[idx])

p, log_file_name = ca.train_revise_solver(
    net_base, solver_base,
    net_revise, solver_revise,
    snap_prefix,
    finetune, continuing, True)

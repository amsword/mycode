import os

folder = '/home/wangjianfeng/working/cifar10/nin_plus1_muticrop/based_on_icml_submission/'
prefix = 'a_'

all_files = [f for root, dirs, files in os.walk(folder)
        for f in files
        if prefix + "_iter" in f and 
            f.endswith('.test')];

print all_files
all_iter = [];
all_acc = [];
for f in all_files:
    num_iter = f[0 : -16];
    k = num_iter.rfind('_');
    num_iter = num_iter[k + 1 :];
    num_iter = int(float(num_iter));

    with open(folder + f, 'r') as fp:
        lines = fp.readlines();
        line = lines[-1];
        x = line.split('accuracy = ');
        assert len(x) == 2;
        acc = float(x[1]);
        all_acc.append(acc);
        all_iter.append(num_iter);


seq = sorted(range(len(all_iter)),
        key = lambda k : all_iter[k], reverse = False);

all_iter = [all_iter[seq[i]] for i in range(len(all_iter))];
all_acc = [all_acc[seq[i]] for i in range(len(all_acc))];
print all_iter, all_acc

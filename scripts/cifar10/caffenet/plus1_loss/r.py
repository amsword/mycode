import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(9, 10) for up in range(10, 11) if up >= lb]
all_device_id, all_machine_name = get_free_gpu(['10', '12', '05', '01']);

num = len(all_net_revise);
print len(all_machine_name);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#assert False
for i in range(num):
    net_base = "cifar10_full_train_test.prototxt";
    solver_base = "cifar10_full_solver.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "max_iter:300000,base_lr:0.00001";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/plus1_loss/direct_";
    snap_prefix_pre = "/home/wangjianfeng/working/cifar10/caffenet/plus1_loss/direct_";
    #snap_prefix_pre = "/home/wangjianfeng/working/cifar10/caffenet/plus1_loss/as_baseline_iter/direct_";
    #finetune = r"/home/wangjianfeng/working/cifar10/plus1_loss/merge/direct_lower_bound:1.0_upper_bound:1.0_max_iter:200000_iter_200000.caffemodel";
    finetune = '';
    continuing = concate_snap_prefix(snap_prefix_pre, all_net_revise[i], 'max_iter:200000,base_lr:0.0001') + \
                 "_iter_200000.solverstate";
    #continuing = '';
     
    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
        net_revise, solver_revise, 
    	machine_name, device_id, caffe_dir, snap_prefix, 
	    finetune, continuing); 
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];


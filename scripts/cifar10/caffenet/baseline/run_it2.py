import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = [""];
all_device_id, all_machine_name = get_free_gpu(['10', '12', '16', '15', '05', '04', '03', '01']);

print len(all_device_id)
num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
#model_type = "merge";
#assert False
model_base = "preprocessed_data"
net_base = "cifar10_full_train_test_" + model_base + ".prototxt";
solver_base = "cifar10_full_solver.prototxt";
net_revise = "";
solver_revise = "";
machine_name = all_machine_name[0];
device_id = all_device_id[0];
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/" + model_base + "/a_" 
finetune = "" 
continuing = "";
all_procs = [None] * 3;
p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    net_revise, solver_revise, 
	machine_name, device_id, caffe_dir, snap_prefix, 
        finetune, continuing);
all_procs[0] = (p, log_file_name);
p.wait();

continuing = concate_snap_prefix(snap_prefix, net_revise, solver_revise) + "_iter_60000.solverstate";
solver_revise = "base_lr:0.0001";
snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/" + model_base + "/b_" 
solver_base = "cifar10_full_solver_lr1.prototxt";
p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    net_revise, solver_revise, 
	machine_name, device_id, caffe_dir, snap_prefix, 
        finetune, continuing);
all_procs[1] = (p, log_file_name);
p.wait();

continuing = concate_snap_prefix(snap_prefix, net_revise, solver_revise) + "_iter_65000.solverstate";
solver_revise = "base_lr:0.00001";
snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/" + model_base + "/c_" 
solver_base = "cifar10_full_solver_lr2.prototxt";
p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    net_revise, solver_revise, 
	machine_name, device_id, caffe_dir, snap_prefix, 
        finetune, continuing);
all_procs[2] = (p, log_file_name);
p.wait();


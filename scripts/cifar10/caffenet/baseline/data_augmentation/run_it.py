import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = [""];
all_device_id, all_machine_name = get_free_gpu(['10', '12', '05', '01']);

num = len(all_net_revise);
assert len(all_machine_name) >= num;
print len(all_machine_name)
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
#model_type = "merge";
#assert False
net_base = "cifar10_full_train_test_convnet.prototxt";
solver_base = "cifar10_full_solver.prototxt";
net_revise = ""
solver_revise = "max_iter:250000,base_lr:0.00001";
machine_name = all_machine_name[0];
device_id = all_device_id[0];
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/data_augmentation/convnet/a_" 
finetune = "" 
continuing = "";
continuing = concat_snap_prefix(snap_prefix, net_revise, 'max_iter:200000,base_lr:0.0001') + \
             "_iter_200000.solverstate";
all_procs = [None] * 3;
p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    net_revise, solver_revise, 
	machine_name, device_id, caffe_dir, snap_prefix, 
        finetune, continuing);
all_procs[0] = (p, log_file_name);
#p.wait();

#continuing = "/home/wangjianfeng/working/cifar10/caffenet/baseline/a___iter_60000.solverstate";
#snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/b_" 
#solver_base = "cifar10_full_solver_lr1.prototxt";
#p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    #net_revise, solver_revise, 
	#machine_name, device_id, caffe_dir, snap_prefix, 
        #finetune, continuing);
#all_procs[1] = (p, log_file_name);
#p.wait();

#continuing = "/home/wangjianfeng/working/cifar10/caffenet/baseline/b___iter_65000.solverstate";
#snap_prefix = "/home/wangjianfeng/working/cifar10/caffenet/baseline/c_" 
#solver_base = "cifar10_full_solver_lr2.prototxt";
#p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
    #net_revise, solver_revise, 
	#machine_name, device_id, caffe_dir, snap_prefix, 
        #finetune, continuing);
#all_procs[2] = (p, log_file_name);
#p.wait();


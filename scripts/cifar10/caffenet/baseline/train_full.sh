#!/usr/bin/env sh

TOOLS=/home/wangjianfeng/code/caffe_new/caffe/build/tools

$TOOLS/caffe train \
    --solver=cifar10_full_solver.prototxt

# reduce learning rate by factor of 10
$TOOLS/caffe train \
    --solver=cifar10_full_solver_lr1.prototxt \
    --snapshot=/home/wangjianfeng/working/cifar10/caffenet/baseline/cifar10_full_iter_60000.solverstate

# reduce learning rate by factor of 10
$TOOLS/caffe train \
    --solver=cifar10_full_solver_lr2.prototxt \
    --snapshot=/home/wangjianfeng/working/cifar10/caffenet/baseline/cifar10_full_iter_65000.solverstate

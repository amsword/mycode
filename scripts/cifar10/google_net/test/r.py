import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;
from caffe_cmd import caffe_cmd

model_type = "merge";
net_base = "train_val.prototxt";
solver_base = "solver.prototxt";
net_revise = ""
solver_revise = ""
machine_name = "deep13"
caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
snap_prefix = "/home/wangjianfeng/working/cifar10/google_net/test/3_gpu"

ca = caffe_cmd();
ca.set_machine(machine_name);
ca.set_device_id(0);
ca.set_device_ids([1, 2]);

ca.set_random_seed(1);
ca.set_random_seeds([2, 3]);

p, log_file_name = ca.train_revise_solver(net_base, solver_base, net_revise, solver_revise,
		snap_prefix, finetune = "", continuing = "");


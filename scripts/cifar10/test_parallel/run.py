python_tools = '/home/wangjianfeng/code/mycode/python_code/';

import sys;
sys.path.append(python_tools);

from caffe_cmd import caffe_cmd;
[all_device_id, all_machine_name] = get_free_gpu_lsls();

ca = caffe_cmd();

ca.set_machine(all_machine_name[0]);
ca.set_device_ids(all_device_id[0]);
ca.set_random_seeds([s + 1 for s in range(len(all_device_id[0]))]);

net_base = 'cifar10_train_val_parallel.prototxt';
solver_base = 'cifar10_solver.prototxt';
net_revise = '';
solver_revise = '';

snap_prefix = '/home/wangjianfeng/working/cifar10/test_parallel/p';

p = ca.train_revise_solver(net_base, solver_base, net_revise, solver_revise,
		snap_prefix, finetune = "", continuing = "", is_sync = False);


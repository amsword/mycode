
python_tools = '/home/wangjianfeng/code/mycode/python_code/';

import sys;
sys.path.append(python_tools);

from caffe_cmd import caffe_cmd;

ca = caffe_cmd();

ca.set_machine('deep18');
ca.set_device_ids([2]);

folder = '/home/wangjianfeng/working/cifar10/test_parallel/'
prefix = 'p_';
num_iter = 100;

ca.find_test(folder, prefix, num_iter);


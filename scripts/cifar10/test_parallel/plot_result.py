python_tools = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_tools);
from caffe_cmd import caffe_cmd;
from com_utility import *; 
import numpy as npy;

def iter_time(log_file):
    with open(log_file, 'r') as fp:
        all_lines = fp.readlines();
    all_num_iter = [];
    all_time = [];
    pre_time = -1;
    for line in all_lines:
        if 'Iteration ' in line and \
            'lr = ' in line:
                idx_end = line.find(',');
                line = line[:idx_end];
                all_splits = line.split(' ');
                num_iter = all_splits[-1];
                num_iter = int(float(num_iter));
                str_time = all_splits[1];
                hr_m_s = str_time.split(':');
                curr_time = float(hr_m_s[2]) + float(hr_m_s[1]) * 60 + float(hr_m_s[0]) * 3600;
                while curr_time < pre_time:
                    curr_time = curr_time + 24 * 3600;
                pre_time = curr_time;
                assert num_iter not in all_num_iter;
                all_num_iter.append(num_iter);
                all_time.append(curr_time);
    return npy.array(all_num_iter), npy.array(all_time) - (npy.array(all_time)).min();

ca = caffe_cmd();

folder = '/home/wangjianfeng/working/cifar10/test_parallel/'
prefix = 'a_';
[acc, num_iter] = ca.collect_test(folder, prefix);

plt.close();
prefix = 'a_';
[acc0, num_iter0] = ca.collect_test(folder, prefix);
idx = num_iter0 < 10000;
acc0 = acc0[idx];
num_iter0 = num_iter0[idx];
plt.plot(num_iter, acc, 'b.-');
[all_iter, all_time] = iter_time(folder + prefix + '_log.tee');
time0 = [];
for iter in num_iter0:
    idx = [i for i in range(len(all_iter)) if all_iter[i] == iter];
    assert len(idx) == 1;
    time0.append(all_time[idx[0]]);

prefix = 'p_';
[acc1, num_iter1] = ca.collect_test(folder, prefix);
idx = num_iter1 < 10000;
acc1 = acc1[idx];
num_iter1 = num_iter1[idx];
[all_iter, all_time] = iter_time(folder + prefix + '_log.tee');
time1 = [];
for iter in num_iter1:
    idx = [i for i in range(len(all_iter)) if all_iter[i] == iter];
    assert len(idx) == 1;
    time1.append(all_time[idx[0]]);

plt.plot(num_iter, acc, 'r*-');
plt.savefig("/home/wangjianfeng/a.png");

plt.close();
plt.plot(time0, acc0, 'b.-')
plt.plot(time1, acc1, 'r*-')
plt.savefig("/home/wangjianfeng/b.png");

import sys
python_code = "/home/wangjianfeng/code/mycode/python_code/"
sys.path.append(python_code)
import matplotlib;
matplotlib.use('Agg');
import matplotlib.pyplot as plt;
from com_utility import get_free_gpu_lsls, concat_snap_prefix
from caffe_cmd import caffe_cmd
from com_utility import extract_all_acc;
import numpy
import glob
import subprocess


ca = caffe_cmd();

ca.set_machine('deep07');

snap_prefix = '/home/wangjianfeng/working/cifar10/triangle/1/b_';
all_acc, all_iter = ca.collect_test1(snap_prefix, 'train_val.prototxt');
#all_acc, all_iter = extract_all_acc(snap_prefix + "" + "_" + "" +  "_log.tee");
#idx = all_acc > 0.8;
#all_acc = all_acc[idx];
#all_iter = all_iter[idx];
best_idx = all_acc.argmax();
print all_acc[best_idx];
print all_iter[best_idx];

plt.close();
plt.plot(all_iter, all_acc, '-*');
plt.grid();
plt.savefig('/home/wangjianfeng/tmp/a.png');

python_code = '/home/wangjianfeng/code/mycode/python_code/';
import sys;
sys.path.append(python_code);

from caffe_cmd import caffe_cmd;
ca = caffe_cmd();

ca.set_device_ids([0, 1, 2]);
ca.set_random_seeds([1, 2, 3]);
ca.set_machine('deep07');
#ca.set_device_ids([0]);
#ca.set_random_seeds([1]);

net_base = './train_val.prototxt';
#net_base = './train_val_zmt.prototxt';
#net_base = './train_val_xavier.prototxt';
#net_base = './origin_train.prototxt';
#net_base = './from_origin.prototxt';
solver_base = './solver.prototxt';
net_revise = '';
solver_revise = '';

#snap_prefix = '/home/wangjianfeng/working/cifar10/triangle/baseline/zmt';
snap_prefix = '/home/wangjianfeng/working/cifar10/triangle/2/a';
#pre_snap_prefix = '/home/wangjianfeng/working/cifar10/triangle/baseline/a';

#finetune = pre_snap_prefix + "__iter_200.caffemodel";
finetune = '';
#continuing = pre_snap_prefix + "__iter_40000.solverstate" 
continuing = '';

p_train, t = ca.train_revise_solver(net_base, solver_base, 
        net_revise, solver_revise, 
        snap_prefix, finetune, continuing);

#test_net = './from_origin_test.prototxt';
test_net = './train_val.prototxt';
ca.set_device_ids([3]);
net_revise_r = net_revise.replace(",", "_")
net_revise_r = net_revise_r.replace("\"", "")
solver_revise_r = solver_revise.replace(",", "_")
solver_revise_r = solver_revise_r.replace("\"", "")
#ca.find_test1(test_net, snap_prefix + net_revise_r + "_" + solver_revise_r, 100);

p_train.communicate();

#[all_acc, all_iter] = ca.collect_test1(snap_prefix);


import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;

all_device_id, all_machine_name = get_free_gpu(['10', '12', '15', '05', '04', '01']);

num = 1;
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#assert False
for i in range(num):
    net_base = "svhn_train_test.prototxt";
    solver_base = "svhn_solver_merge.prototxt";
    net_revise = "" 
    solver_revise = "";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/svhn/nin_baseline/from_cuizhen/a";
    
    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
        net_revise, solver_revise, 
    	machine_name, device_id, caffe_dir, snap_prefix, 
	finetune = "", continuing = "");
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];

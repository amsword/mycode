import sys;
import os;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);
from com_utility import * 

caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/"
pretrained = '/home/wangjianfeng/working/svhn/nin_plus1_loss/no_norm_preprocessed/fine_tune_l0.9u1.0_lower_bound:0.8_upper_bound:1.0_base_lr:0.005_iter_137000.caffemodel';
net_proto = 'svhn_extract.prototxt'
blob_name = 'softmax'; 
save_file = pretrained + "_extract_" + blob_name; 
device_id = 1
machine_name = "deep18";
num_mini_batches = 3053;
phase = 'test';

cmd = caffe_dir + "extract_features1" + " " + \
    pretrained + " " + net_proto + " " + \
    blob_name + " " + save_file + " " + \
    str(num_mini_batches) + " " + phase + " " + \
    'GPU DEVICE_ID=' + str(device_id);

p, log_file_name = throw_cmd(machine_name, cmd, pretrained + 'extract.log');


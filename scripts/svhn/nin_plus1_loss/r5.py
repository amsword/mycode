import sys;
python_code = "/home/wangjianfeng/code/mycode/python_code/";
sys.path.append(python_code);

from com_utility import *;


all_net_revise = ["lower_bound:" + str(lb * 0.1) + "," + "upper_bound:" + str(up * 0.1)  
            for lb in range(8, 10) for up in range(10, 11) if up >= lb]
all_device_id, all_machine_name = get_free_gpu(['10', '05', '01']);

print len(all_device_id)
num = len(all_net_revise);
assert len(all_machine_name) >= num;
assert len(all_device_id) >= num;
all_procs = [None] * num;
#model_type = "data_augmented";
#model_type = "merge";
model_type = "no_norm_preprocessed"
#assert False
for i in range(num):
    net_base = "svhn_train_test_" + model_type + ".prototxt";
    solver_base = "svhn_solver_merge.prototxt";
    net_revise = all_net_revise[i];
    solver_revise = "base_lr:0.005";
    machine_name = all_machine_name[i];
    device_id = all_device_id[i];
    caffe_dir = "/home/wangjianfeng/code/caffe_new/caffe/build/tools/";
    snap_prefix = "/home/wangjianfeng/working/svhn/nin_plus1_loss/" + model_type + "/continue_l0.9u1.0_175_";
    direct_snap_prefix = "/home/wangjianfeng/working/svhn/nin_plus1_loss/" + model_type + "/direct_";
    #finetune = r"/home/wangjianfeng/working/svhn/nin_plus1_loss/" + model_type + "/direct_lower_bound:0.9_upper_bound:1.0_max_iter:200000_iter_120000.caffemodel";
    #finetune = concate_snap_prefix(direct_snap_prefix, 'lower_bound:0.9,upper_bound:1.0', '') + \
                 #"_iter_133000.caffemodel";
    continuing = "";
    finetune = "";
    continuing = concate_snap_prefix(direct_snap_prefix, 'lower_bound:0.9,upper_bound:1.0', 
            "stepsize:1000000,max_iter:300000") + \
                 "_iter_175000.solverstate";

    p, log_file_name = train_revise_solver_asyc(net_base, solver_base, 
        net_revise, solver_revise, 
    	machine_name, device_id, caffe_dir, snap_prefix, 
	    finetune, continuing);
    all_procs[i] = (p, log_file_name);

print [p.poll() == None for p, log_file_name in all_procs];


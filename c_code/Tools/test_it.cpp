#include <cstdio>
#include <iostream>

#include "utility.h"

using namespace utility;
using namespace std;
int main(int argc, const char* argv[])
{
	long long size = (long long)1024 * (long long)1024 * (long long )1024 * (long long)15;
	cout << "start alloc" << endl;
	char* p = new char[size];
	cout << "finish alloc" << endl;

	double s = 0; 
	cout << s << endl;
	while (1)
	{
		double s = 0;
		cout << s << endl;
		for (long long i = 0; i < size; i++)
		{
			if ((i & 0xFFFFF) == 0)
			{
				cout << i << endl;
			}
			p[i] = i;
			s += p[i];
		}
		cout << s << endl;
	}
	delete[] p;
	return 0;
}

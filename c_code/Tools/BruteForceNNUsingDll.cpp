#include "utility.h"

using namespace utility;

int main(int argc, const char* argv[])
{
	SMART_ASSERT(argc >= 5).Exit();
	string str_query = argv[1];
	string str_data_base = argv[2];
	string str_result = argv[3];
	int k = atoi(argv[4]);
	SMART_ASSERT(k > 0)(k).Exit();

	BruteforceKNNer<float> knn;
	SMART_ASSERT(0)(str_query)(str_data_base)(str_result)(k);

	knn.RunHugeDataBase(str_query, str_data_base, k, str_result);

	return 0;
}

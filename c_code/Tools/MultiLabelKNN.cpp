#include "utility.h"

#define RUN_TEST

void top_index(const float* curr_confidence, 
			   int num_total_labels,
			   int num_selected_label,
			   vector<int> &vec_rank_idx)
{

	vector<int> vec_seq(num_total_labels);
	for (int j = 0; j < num_total_labels; j++)
	{
		vec_seq[j] = j;
	}
	partial_sort(vec_seq.begin(), vec_seq.end(), 
		vec_seq.begin() + num_selected_label, 
		[curr_confidence](int a, int b)
	{
		return curr_confidence[a] > curr_confidence[b];
	}
	);

#ifdef RUN_TEST
	for (int i = 0; i < num_selected_label - 1; i++)
	{
		SMART_ASSERT(curr_confidence[vec_seq[i]] >= 
			curr_confidence[vec_seq[i + 1]]).Exit();
	}
	for (int i = num_selected_label - 1; i < num_total_labels; i++)
	{
		SMART_ASSERT(curr_confidence[vec_seq[num_selected_label - 1]] >= 
			curr_confidence[vec_seq[i]]).Exit();
	}
#endif

	vec_rank_idx.resize(num_total_labels);
	for (int j = 0; j < vec_rank_idx.size(); j++)
	{
		vec_rank_idx[j] = -1;
	}
	for (int j = 0; j < num_selected_label; j++)
	{
		vec_rank_idx[vec_seq[j]] = j;
	}
}

class ComparePair
{
public:
	bool operator ()(const pair<int, float> &p1, 
		const pair<int, float> &p2)
	{
		return p1.second < p2.second;
	}
};

void update_vec_pq(
	const float* query_feature, 
	const SMatrix<float> &mat_database_feature, 
	const Vector<int> &vec_database_label, 
	const vector<int> &vec_rank_idx, 
	vector<priority_queue<pair<int, float>, 
	vector<pair<int, float> >,
	ComparePair> > &vec_heap, 
	int num_candiate_each)
{
	int num_database_feature = mat_database_feature.Rows();
	int feature_dim = mat_database_feature.Cols();
	for (int j = 0; j < num_database_feature; j++)
	{
		float* database_feature = mat_database_feature[j];

		int database_label = vec_database_label[j];
		int idx_top = vec_rank_idx[database_label];
		if (idx_top == -1)
		{
			continue;
		}
		priority_queue<pair<int, float>, 
			vector<pair<int, float> >, 
			ComparePair> &pq = vec_heap[idx_top];
		float d = squared_distance(query_feature, database_feature, feature_dim);
		if (pq.size() < num_candiate_each)
		{
			pq.push(pair<int, float>(j, d));
		}
		else
		{
			const pair<int, float>& top = pq.top();
			if (top.second > d)
			{
				pq.pop();
				pq.push(pair<int, float>(j, d));
			}
		}
	}
}

void save_to_file(
	vector<vector<priority_queue<pair<int, float>, 
	vector<pair<int, float> >,
	ComparePair> > > &vecvec_heap, 
	const string &file_output)
{
	FILE* fp = fopen(file_output.c_str(), "wb");
	SMART_ASSERT(fp)(file_output).Exit();

	int num_query = vecvec_heap.size();
	SMART_ASSERT(fwrite(&num_query, sizeof(int), 1, fp) == 1).Exit();
	PRINT << num_query;
	for (int i = 0; i < num_query; i++)
	{
		vector<priority_queue<pair<int, float>, 
			vector<pair<int, float> >,
			ComparePair> > &vec_heap = vecvec_heap[i];
		int num_selected_labels = vec_heap.size();
		SMART_ASSERT(fwrite(&num_selected_labels, sizeof(int), 1, fp) == 1).Exit();

		for (int j = 0;  j < vec_heap.size(); j++)
		{
			priority_queue<pair<int, float>, 
				vector<pair<int, float> >,
				ComparePair> &pq = vec_heap[j];

			int num_candidate_curr = pq.size();
			SMART_ASSERT(fwrite(&num_candidate_curr, sizeof(int), 1, fp) == 1).Exit();

			vector<pair<int, float> > vec_pair(pq.size());
			for (int k = 0; k < pq.size(); k++)
			{
				vec_pair[k] = pq.top();
				pq.pop();
			}

#ifdef RUN_TEST
			for (int k = 0; k < vec_pair.size() - 1; k++)
			{
				SMART_ASSERT(vec_pair[k].second >= vec_pair[k + 1].second).Exit();
			}
#endif

			for (int k = 0; k < vec_pair.size(); k++)
			{
				int idx = vec_pair[vec_pair.size() - 1 - k].first;
				SMART_ASSERT(fwrite(&idx, sizeof(int), 1, fp) == 1).Exit();
			}
		}
	}

	fclose(fp);
}

// .exe file_confidence_score num_selected_labels
// file_database_label
// file_query_feature file_database_feature
// num_candiate_each
// file_output
int main(int argc, char* argv[])
{
	SMART_ASSERT(argc >= 8)(argc).Exit();
	int idx = 1;
	const string file_confidence_score = argv[idx++];
	const int num_selected_label = atoi(argv[idx++]);
	const string file_database_label = argv[idx++];
	const string file_query_feature = argv[idx++];
	const string file_database_feature = argv[idx++];
	const int num_candiate_each = atoi(argv[idx++]);
	const string file_output = argv[idx++];

	SMatrix<float> mat_confidence_score;
	mat_confidence_score.LoadData(file_confidence_score);
	//SMART_ASSERT(0)(mat_confidence_score.Rows())
		//(mat_confidence_score.Cols());

	int num_queries = mat_confidence_score.Rows();
	int num_total_labels = mat_confidence_score.Cols();
	SMART_ASSERT(num_selected_label <= num_total_labels && 
			num_selected_label > 0)(num_selected_label)(num_total_labels).Exit();

	Vector<int> vec_database_label;
	vec_database_label.LoadData(file_database_label);
	//SMART_ASSERT(0)(vec_database_label.Size());

	SMatrix<float> mat_query_feature;
	mat_query_feature.LoadData(file_query_feature);
	//SMART_ASSERT(0)(mat_query_feature.Rows())
		//(mat_query_feature.Cols());
	int feature_dim = mat_query_feature.Cols();
	SMatrix<float> mat_database_feature;
	mat_database_feature.LoadData(file_database_feature);
	//SMART_ASSERT(0)(mat_database_feature.Rows())
		//(mat_database_feature.Cols());
	SMART_ASSERT(feature_dim == mat_database_feature.Cols()).Exit();
	//SMART_ASSERT(0).Exit();
	vector<vector<priority_queue<pair<int, float>, 
		vector<pair<int, float> >,
		ComparePair> > > vecvec_heap;
	vecvec_heap.resize(num_queries);
	int num_count = 0;
#pragma omp parallel for
	for (int i = 0; i < num_queries; i++)
	{
#pragma omp atomic
		num_count++;
		if ((num_count % 1000) == 0)
		{
			PRINT << num_count << "\n";
		}
		// for each query, we find the top num_selected_label 
		float* curr_confidence = mat_confidence_score[i];
		vector<int> vec_rank_idx;

		top_index(curr_confidence, num_total_labels, 
			num_selected_label, vec_rank_idx);

		vector<priority_queue<pair<int, float>, 
			vector<pair<int, float> >,
			ComparePair> >& vec_heap = vecvec_heap[i];
		vec_heap.resize(num_selected_label);

		float* query_feature = mat_query_feature[i];
		update_vec_pq(query_feature, mat_database_feature, 
			vec_database_label, vec_rank_idx, vec_heap, num_candiate_each);
	}

	save_to_file(vecvec_heap, file_output);

	return 0;
}

#include "utility.h"

template <class T>
class PAIRLabel
{
public:
	bool is_same_label;
	int index;
	T distance;
	friend bool operator < (const PAIRLabel<T> &n1, const PAIRLabel<T> &n2)
	{
		if (n1.is_same_label && !n2.is_same_label)
		{
			return false;
		}
		if (! n1.is_same_label && n2.is_same_label)
		{
			return true;
		}

		if (n1.distance == n2.distance) return n1.index > n2.index;
		return n1.distance > n2.distance;
	}
};

template<class typeData>
void Heap2SaveFile(
	Heap<PAIRLabel<typeData> >* p_top_knn, int heap_num, const int k_nn, const string& str_save_file)
{
	int* p_ulti_idx = new int[k_nn];
	EXIT_ASSERT(p_ulti_idx);
	typeData* p_ulti_dist = new typeData[k_nn];
	EXIT_ASSERT(p_ulti_dist);

	FILE* fp = fopen(str_save_file.c_str(), "wb");
	EXIT_ASSERT(fp);
	fwrite(&heap_num, sizeof(int), 1, fp);
	fwrite(&k_nn, sizeof(int), 1, fp);
	for (int i = 0; i < heap_num; i++)
	{
		Heap<PAIRLabel<typeData> >& heap_knn = p_top_knn[i];
		int k = k_nn - 1;
		while (!heap_knn.empty())
		{
			PAIRLabel<typeData> node;
			heap_knn.popMin(node);
			p_ulti_idx[k] = node.index;
			p_ulti_dist[k] = node.distance;
			k--;
		}	

		SMART_ASSERT(k == -1)(k).Exit();
		fwrite(&k_nn, sizeof(int), 1, fp);
		fwrite(p_ulti_idx, sizeof(int), k_nn, fp); // index of the NN
		fwrite(p_ulti_dist, sizeof(typeData), k_nn, fp); // squared distance to the query
	}
	fclose(fp);

	delete[] p_ulti_dist;
	delete[] p_ulti_idx;
}



template <class typeData>
void RunOneHeapLabel(
	const typeData* p_query,
	int query_label,
	const SMatrix<typeData> &mat_data_base,
	const Vector<int> &vec_database_label,
	int offset_data_base,
	const int k_nn,
	Heap<PAIRLabel<typeData> >& heap_knn)
{
	int num_data_base = mat_data_base.Rows();
	int dim = mat_data_base.Cols();
	for (int j = 0; j < num_data_base; j++)
	{
		PAIRLabel<typeData> node;
		node.is_same_label = 
			(query_label == vec_database_label[j + offset_data_base]);
#ifdef BFKNNSimNegative
		node.distance = -utility::squared_distance(tmp_p_query,
			pp_data_base[j], dim);
#else
		node.distance = utility::squared_distance(p_query,
			mat_data_base[j], dim);
#endif
		if (heap_knn.size() < k_nn)
		{
			node.index = j + offset_data_base;
			heap_knn.insert(node);
		}
		else
		{
			const PAIRLabel<typeData>& top = heap_knn.Top();
			if (top < node)
			{
				node.index = j + offset_data_base;
				heap_knn.popMin();
				heap_knn.insert(node);
			}
		}
	}
}

template<class typeData>
void RunHeap(const SMatrix<typeData> &mat_query,
			 const Vector<int> &vec_query_label,
			 const SMatrix<typeData> &mat_data_base,
			 const Vector<int> &vec_database_label,
			 int offset_data_base,
			 int k_nn,
			 Heap<PAIRLabel<typeData> >* p_top_knn)
{
	int num_query = mat_query.Rows();
	int count = 0;
#pragma omp parallel for 
	for (int i = 0; i < num_query; ++i)
	{
#pragma omp atomic
		count++;

		if ((count % 500) == 0)
		{
			PRINT << ::omp_get_thread_num() << ": " << count << "\t" << "offset: " << offset_data_base << "\n";
		}

		RunOneHeapLabel(mat_query[i], vec_query_label[i],
			mat_data_base, vec_database_label,
			offset_data_base, k_nn, p_top_knn[i]);
	}
}

template<class typeData>
void Heap2SaveFile(
	Heap<PAIR<typeData> >* p_top_knn, int heap_num, const int k_nn, const string& str_save_file)
{
	int* p_ulti_idx = new int[k_nn];
	EXIT_ASSERT(p_ulti_idx);
	typeData* p_ulti_dist = new typeData[k_nn];
	EXIT_ASSERT(p_ulti_dist);

	FILE* fp = fopen(str_save_file.c_str(), "wb");
	EXIT_ASSERT(fp);
	fwrite(&heap_num, sizeof(int), 1, fp);
	fwrite(&k_nn, sizeof(int), 1, fp);
	for (int i = 0; i < heap_num; i++)
	{
		Heap<PAIR<typeData> >& heap_knn = p_top_knn[i];
		int k = k_nn - 1;
		while (!heap_knn.empty())
		{
			PAIR<typeData> node;
			heap_knn.popMin(node);
			p_ulti_idx[k] = node.index;
			p_ulti_dist[k] = node.distance;
			k--;
		}	

		SMART_ASSERT(k == -1)(k).Exit();
		fwrite(&k_nn, sizeof(int), 1, fp);
		fwrite(p_ulti_idx, sizeof(int), k_nn, fp); // index of the NN
		fwrite(p_ulti_dist, sizeof(typeData), k_nn, fp); // squared distance to the query
	}
	fclose(fp);

	delete[] p_ulti_dist;
	delete[] p_ulti_idx;
}

int main(int argc, char* argv[])
{
	SMART_ASSERT(argc >= 7).Exit();
	string str_query = argv[1];
	string str_data_base = argv[2];
	string str_result = argv[3];
	int k_nn = atoi(argv[4]);
	string str_query_label = argv[5];
	string str_database_label = argv[6];
	SMART_ASSERT(k_nn > 0)(k_nn).Exit();

	//SMART_ASSERT(0)(str_query)(str_data_base)(str_result)(k_nn)(str_query_label)(str_database_label);

	//SMART_ASSERT(0).Exit();
	SMatrix<float> mat_query;
	mat_query.LoadData(str_query);
	int num_query = mat_query.Rows();

	Vector<int> vec_query_label;
	vec_query_label.LoadData(str_query_label);
	Vector<int> vec_database_label;
	vec_database_label.LoadData(str_database_label);

	const int batch_size = 1000000;
	int num_point;
	int dim;
	GetDims(str_database_label, num_point, dim);

	Heap<PAIRLabel<float> >* p_top_knn;

	PRINT << "Begin initializing heap" << "\n";
	InitializeHeap(p_top_knn, num_query, k_nn);
	PRINT << "Finish initializing heap\n";

	clock_t t_begin = clock();

	int batch_num = num_point / batch_size + 1;
	for (int i = 0; i < batch_num; i++)
	{
		int start = i * batch_size;
		int end = start + batch_size;

		if (start >= num_point)
		{
			break;
		}
		if (end > num_point)
		{
			end = num_point;
		}

		PRINT << "Loading data\n";
		SMatrix<float> mat_data_base;
		mat_data_base.LoadData(str_data_base, start, end);
		PRINT << "loaded data\n";

		RunHeap(mat_query, vec_query_label, 
			mat_data_base, vec_database_label,
			start, k_nn, p_top_knn);
		clock_t t_end = clock();
		PRINT << i << "/" << batch_num << "\t" << (double)(t_end - t_begin) / CLOCKS_PER_SEC << "\n";

	}

	Heap2SaveFile(p_top_knn, num_query, k_nn, str_result);

	UninitializeHeap(p_top_knn);
	return 0;
}

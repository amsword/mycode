#include "utility.h"


void ReadOneTest(FILE* fp_in, vector<vector<int> > &vecvec_label)
{
	int num_max_labels;
	SMART_ASSERT(fread(&num_max_labels, sizeof(int), 1, fp_in) == 1).Exit();

	vecvec_label.resize(num_max_labels);
	for (int j = 0; j < num_max_labels; j++)
	{
		int num_candidate;
		SMART_ASSERT(fread(&num_candidate, sizeof(int), 1, fp_in) == 1).Exit();
		vector<int> &vec_label = vecvec_label[j];
		vec_label.resize(num_candidate);
		SMART_ASSERT(fread(vec_label.data(), sizeof(int), num_candidate, fp_in) == num_candidate)
			(num_candidate).Exit();
	}
}

void SaveOneTest(const vector<vector<int> > &vecvec_label, 
				 int num_selected_label, 
			int batch_size_each_label, 
			int batch_num_one_test, FILE* fp_out)
{
	int num_required = batch_size_each_label * batch_num_one_test;
	SMART_ASSERT(vecvec_label.size() >= num_selected_label)(num_selected_label)(vecvec_label.size()).Exit();
	for (int i = 0; i < num_selected_label; i++)
	{
		SMART_ASSERT(vecvec_label[i].size() >= num_required)(num_required)(vecvec_label[i].size()).Exit();
	}

	int off_set = 0;
	for (int i = 0; i < batch_num_one_test; i++)
	{
		for (int j = 0; j < num_selected_label; j++)
		{
			const int* p = vecvec_label[j].data() + off_set; 
			SMART_ASSERT(fwrite(p, sizeof(int), batch_size_each_label, fp_out) == batch_size_each_label).Exit();
		}
		off_set += batch_size_each_label;
	}
}

// .exe inter_multilabel_knn num_selected_label 
// batch_size_each_label batch_num_one_test output
int main(int argc, char* argv[])
{
	SMART_ASSERT(argc >= 6)(argc).Exit();
	int idx = 1;
	const string &str_inter_multilabel_knn = argv[idx++];
	const int num_selected_label = atoi(argv[idx++]);
	const int batch_size_each_label = atoi(argv[idx++]);
	const int batch_num_one_test = atoi(argv[idx++]);
	const string &str_output = argv[idx++];
	SMART_ASSERT(idx == argc)(idx)(argc).Exit();

	FILE* fp_in = fopen(str_inter_multilabel_knn.c_str(), "rb");
	SMART_ASSERT(fp_in)(str_inter_multilabel_knn).Exit();
	FILE* fp_out = fopen(str_output.c_str(), "wb");
	SMART_ASSERT(fp_out)(str_output).Exit();

	int num_query;
	SMART_ASSERT(fread(&num_query, sizeof(int), 1, fp_in) == 1).Exit();
	PRINT << num_query << "\n";
	int knn = batch_num_one_test * batch_size_each_label * num_selected_label;
	SMART_ASSERT(fwrite(&num_query, sizeof(int), 1, fp_out) == 1).Exit();
	SMART_ASSERT(fwrite(&knn, sizeof(int), 1, fp_out) == 1).Exit();

	for (int i = 0; i < num_query; i++)
	{
		vector<vector<int> > vecvec_label;
		ReadOneTest(fp_in, vecvec_label);

		SaveOneTest(vecvec_label, num_selected_label, 
			batch_size_each_label, batch_num_one_test, fp_out);
	}

	fclose(fp_out);
	fclose(fp_in);
	return 0;
}

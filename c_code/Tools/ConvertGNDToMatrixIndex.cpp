#include "utility.h"

using namespace utility;

// exe input_gnd output
int main(int argc, const char* argv[])
{
	SMART_ASSERT(argc >= 3).Exit();
	string str_input = argv[1];
	string str_output = argv[2];

	ForwardIndex<float> fi;
	fi.LoadData(str_input);

	SMatrix<int> mat_index;
	mat_index.AllocateSpace(fi.Rows(), fi.Cols());
	for (int i = 0; i < fi.Rows(); i++)
	{
		int* p_idx;
		float* p_value;
		int num_valid;
		fi.Row(i, p_idx, p_value, num_valid);
		SMART_ASSERT(num_valid == fi.Cols());
		memcpy(mat_index[i], p_idx, sizeof(int) * num_valid);
	}
	mat_index.SaveData(str_output);
	return 0;
}


#include "utility.h"

int main(int argc, char* argv[])
{
	string str_input_file = argv[1];
	string str_output_file = argv[2];
	int used = atoi(argv[3]);

	SMART_ASSERT(used > 0)(used).Exit();

	FILE* fin = fopen(str_input_file.c_str(), "rb");
	SMART_ASSERT(fin)(str_input_file).Exit();
	FILE* fout = fopen(str_output_file.c_str(), "wb");
	SMART_ASSERT(fout)(str_output_file).Exit();

	int num_rows;
	int num_cols;
	SMART_ASSERT(fread(&num_rows, sizeof(int), 1, fin) == 1).Exit();
	SMART_ASSERT(fread(&num_cols, sizeof(int), 1, fin) == 1).Exit();
	SMART_ASSERT(num_rows >= used).Exit();

	SMART_ASSERT(fwrite(&used, sizeof(int), 1, fout) == 1).Exit();
	SMART_ASSERT(fwrite(&num_cols, sizeof(int), 1, fout) == 1).Exit();

	long long buffer_size = (size_t)used * (size_t)num_cols;
	Vector<float> vec_buffer(buffer_size);
	PRINT << vec_buffer.Size() << "\t" << used << "\t" << num_cols << "\n" << buffer_size;
	SMART_ASSERT(fread(vec_buffer.Ptr(), sizeof(float), buffer_size, fin) == buffer_size).Exit();
	PRINT << vec_buffer.Size() << "\n";
	SMART_ASSERT(fwrite(vec_buffer.Ptr(), sizeof(float), buffer_size, fout) == buffer_size).Exit();

	fclose(fout);
	fclose(fin);

	return 0;
}


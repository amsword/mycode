#include <queue>
#include <iostream>

using namespace std;

class Memory
{
public:
	
public:
	virtual void resize(size_t count) = 0;
	virtual void release() = 0;
	
};

class CPUMemory : public Memory
{
public:
	virtual void resize(size_t count) {}
	virtual void release() {}
};

#include <memory>
int main()
{

	return 0;
}
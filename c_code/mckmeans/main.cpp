#include "TypeConvert.h"
#include "MCKMeans.h"
#include "make_mex.h"
#include "make_mat.h"
#include <omp.h>

#define MAT_Z prhs[0] 
#define MAT_DIC prhs[1]
#define N_SPARSITY prhs[2]
#define NUM_CAN_BEST prhs[3]
#define OLD_BINARY_REPRESENTATION prhs[3]
#define PARAMETER prhs[3]

#define BINARY_REPRESENTATION plhs[0]

//#ifdef _WINDOWS
int main()
{
	int num_thd = omp_get_num_procs();
	omp_set_num_threads(1);

	SMatrix<double> matZ;
	Vector<SMatrix<double> > vecmatDictionary;
	int num_dic;

	string str_file_name = "test.mat";
	PRINT << "good\n";

	MATFile* fp = matOpen(str_file_name.c_str(), "r");
	SMART_ASSERT(fp)(str_file_name).Exist0();

	mexConvert(matGetVariable(fp, "subZ"), matZ);
	mexConvert(matGetVariable(fp, "sub_all_D"), vecmatDictionary);
	//mexConvert(matGetVariable(fp, "num_sub_dic_each_partition"), num_dic);
	num_dic = 2;
	PRINT << "good\n";


	matClose(fp);

	int num_partitions = vecmatDictionary.size();

	SMatrix<short> matRepresentation;
	matRepresentation.AllocateSpace(matZ.Rows(), num_partitions  * num_dic);
	// method:
	// 2: ock-means described in the paper
	// others: jck-means
	MCKMeans mp;
	{
		mp.SetIsInitialize(true);
		mp.Encoding(matZ, vecmatDictionary, num_dic, matRepresentation);
	}
	{
		clock_t begin = clock();
		mp.SetIsInitialize(false);
		mp.Solve(matZ, vecmatDictionary, num_dic, matRepresentation);
		clock_t end = clock();
		PRINT << "time cost: " << (end - begin) / (double)CLOCKS_PER_SEC << "\n";
	}
	
	string str_output = "benchmark";
	SMatrix<short> matBenchmark;
	matBenchmark.LoadData(str_output);

	for (int i = 0; i < matBenchmark.Rows(); i++)
	{
		for (int j = 0; j < matBenchmark.Cols(); j++)
		{
			SMART_ASSERT(matBenchmark[i][j] == matRepresentation[i][j]).Exist0();
		}
	}

	return 0;
}


int main1()
{
	MCKMeans mp;

	const string str_z = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\test\\z.double.bin";
	const string str_dic = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\test\\all_d_";
	const string str_output1 = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\test\\out2.short";

	SMatrix<double> matZ;
	Vector<SMatrix<double> > vecmatDictionary;

	matZ.LoadData(str_z, 0, 1000);

	vecmatDictionary.AllocateSpace(16);
	for (int i = 0; i < 16; i++)
	{
		string str_num;
		TypeConvert(i, str_num);
		string file_name = str_dic + str_num;
		vecmatDictionary[i].LoadData(file_name);
	}


	int num_dic = 1;
	SMatrix<short> matRepresentation;

	matRepresentation.AllocateSpace(matZ.Rows(), vecmatDictionary.size());

	clock_t begin = clock();

	mp.SetIsInitialize(true);

	mp.Solve(matZ, vecmatDictionary, num_dic, matRepresentation);
	clock_t end = clock();

	PRINT << (end - begin) / (double)CLOCKS_PER_SEC << "\n";
	SMART_ASSERT(0)(Vector<short>(matRepresentation[5], matRepresentation.Cols()));

	matRepresentation.SaveData(str_output1);

	return 0;
}
//#endif


//void main()
//{
//	string str_dir = "F:\\v-jianfw\\hash\\v-jianfw\\Data_HashCode\\Labelme\\working_sck_means\\test\\new_representation_b\\";
//
//	SMatrix<double> matZ;
//	matZ.LoadData(str_dir + "subZ");
//
//	int num_dic = -1;
//
//	int method = 0;
//
//	SMatrix<double> matDictionary;
//	matDictionary.LoadData(str_dir + "subD");
//
//	int size[2];
//	size[0] = num_dic == -1 ? 1 :num_dic;
//	size[1] = matZ.Rows();
//
//	MatchingPersuitCompact mp;
//	SMatrix<short> matRepresentation;
//	matRepresentation.AllocateSpace(size[1], size[0]);
//
//	mp.Solve(matZ, matDictionary, num_dic, matRepresentation, method);
//}

// demo code
//void main()
//{
//	//SMART_ASSERT(0).Exit();
//
//
//	SMatrix<double> matZ;
//	//mexConvert(MAT_Z, matZ);
//	
//	matZ.LoadData("../data/subZ.double.bin");
//	/*{
//		matZ.AllocateSpace(1, 4);
//		matZ.SetValueZeros();
//		matZ[0][0] = 1;
//		matZ[0][2] = 1;
//	}*/
//
//
//	int num_dic = 2;
//	//mexConvert(N_SPARSITY, num_dic);
//
//	int method = 52;
//	//mexConvert(NUM_CAN_BEST, method);
//
//	SMatrix<double> matDictionary;
//	//mexConvert(MAT_DIC, matDictionary);
//	matDictionary.LoadData("../data/subD.double.bin");
//	/*{
//		matDictionary.AllocateSpace(4, 4);
//		matDictionary.SetValueZeros();
//		for (int i = 0; i < 4; i++)
//		{
//			matDictionary[i][i] = 1;
//		}
//	}*/
//
//	//mwSize size[2];
//	//size[0] = num_dic == -1 ? 1 :num_dic;
//	//size[1] = matZ.Rows();
//
//	//BINARY_REPRESENTATION = mxCreateNumericArray(2, size, mxINT16_CLASS, mxREAL);
//
//	SMatrix<short> matRepresentation;
//	matRepresentation.AllocateSpace(matZ.Rows(), num_dic);
//	//mexConvert(BINARY_REPRESENTATION, matRepresentation);
//	
//	if (method == 5)
//	{
//		BinaryCplex cplexer;
//		cplexer.Solve(matZ, matDictionary, num_dic, matRepresentation);
//	}
//	else
//	{
//		MatchingPersuitCompact mp;
//		mp.Solve(matZ, matDictionary, num_dic, matRepresentation, method);
//	}
//}
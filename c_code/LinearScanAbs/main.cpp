#include "utility.h"
#include <list>
#include "TypeConvert.h"

using namespace std;

typedef short typeCode;

#define VEC_QUERY prhs[0]
#define RAW_DATABASE prhs[1]
#define GAMMA prhs[2]
#define MAX_EXPAND prhs[3]

#define GOOD_IDX plhs[0]

using namespace utility;

void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 4)(nrhs).Exit();
	SMART_ASSERT(nlhs == 1).Exit();

	Vector<double> vec_query;
	SMatrix<double> mat_raw_database;
	double gamma;
	int max_expanded;

	mexConvert(VEC_QUERY, vec_query);
	mexConvert(RAW_DATABASE, mat_raw_database);
	mexConvert(GAMMA, gamma);
	mexConvert(MAX_EXPAND, max_expanded);

	/*SMART_ASSERT(0)(gamma)(max_expanded)(vec_query.Size())
		(mat_raw_database.Rows())(mat_raw_database.Cols());*/

	int num_point = mat_raw_database.Rows();
	int dim = mat_raw_database.Cols();
	vector<GreatPair<double> > vec_idx_good;
	vec_idx_good.reserve(max_expanded);
	for (int i = 0; i < num_point; i++)
	{
		double s = dot(vec_query.Ptr(), mat_raw_database[i], dim); 
		s = s >= 0 ? s : -s;
		if (s > gamma)
		{
			vec_idx_good.push_back(GreatPair<double>(i, s));
			/*if (vec_idx_good.size() >= max_expanded)
			{
				break;
			}*/
		}
	}
	int num = vec_idx_good.size();

	if (vec_idx_good.size() > max_expanded)
	{
		sort(vec_idx_good.begin(), vec_idx_good.end());
		num = max_expanded;
	}

	GOOD_IDX = mxCreateDoubleMatrix(num, 1, mxREAL);
	Vector<double> vec_good;
	mexConvert(GOOD_IDX, vec_good);
	int s = vec_idx_good.size();
	for (int i = 0; i < num ; i++)
	{
		vec_good[i] = vec_idx_good[i].index + 1;
	}
}

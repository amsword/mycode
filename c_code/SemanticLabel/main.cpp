#include "utility.h"

using namespace utility;

// exe query_label database_label retrieval_result output_file;
int main(int argc, const char* argv[])
{
	SMART_ASSERT(argc == 5);
	string str_query_label = argv[1];
	string str_database_label = argv[2];
	string str_retrieval_result = argv[3];
	string str_output_file = argv[4];

	Vector<int> vec_query_label;
	vec_query_label.LoadData(str_query_label);
	PRINT << vec_query_label.size() << "\n";

	Vector<int> vec_database_label;
	vec_database_label.LoadData(str_database_label);
	PRINT << vec_database_label.size() << "\n";

	SMatrix<int> mat_retrieval_result;
	mat_retrieval_result.LoadData(str_retrieval_result);
	PRINT << mat_retrieval_result.Rows() << "\t"
		<< mat_retrieval_result.Cols() << "\n";

	SMART_ASSERT(vec_query_label.size() == mat_retrieval_result.Rows())
		(vec_query_label.size())(mat_retrieval_result.Rows()).Exit();

	PrecisionCriterion criteria;

	Vector<double> vec_pre;
	vec_pre.AllocateSpace(mat_retrieval_result.Cols());
	criteria.Initialize(&vec_query_label, &vec_database_label, vec_pre);

	criteria.BeginEvaluate();
	criteria.Evaluate(mat_retrieval_result, 0, mat_retrieval_result.Rows());
	criteria.EndEvaluate();

	for (int i = 1; i < vec_pre.Size(); i++)
	{
		vec_pre[i] = vec_pre[i] + vec_pre[i - 1];
	}

	int num_query = mat_retrieval_result.Rows();
	for (int i = 0; i < vec_pre.size(); i++)
	{
		vec_pre[i] /= (double)num_query * (double)(i + 1);
	}

	//vec_pre.SaveData(str_output_file);
	FILE* fp = fopen(str_output_file.c_str(), "w");
	SMART_ASSERT(fp)(str_output_file);
	for (int i = 0; i < vec_pre.size(); i++)
	{
		fprintf(fp, "%lf\n", vec_pre[i]);
	}
	fclose(fp);

	return 0;
}

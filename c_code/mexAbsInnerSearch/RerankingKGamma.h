#pragma once
#include "utility.h"
using namespace utility;
class RerankingKGamma
{
public:
	void Initialize(const SMatrix<double> &mat_raw_data_base, int max_expanded, double gamma);

	void SetMaxExpanded(int max_expanded);
	
	void Reranking(
		const double* query, 
		const Vector<int> &vec_idx, 
		Vector<int> &vec_reranking_result);

	void Select(const double* query, 
		const Vector<int> &vec_idx, 
		vector<int> &vec_reranking_result);

	void Verify(const double* query, const vector<int> &vec_good);

private:
	const SMatrix<double>* m_pmat_raw_data_base;
	int m_max_expanded;
	double m_gamma;
};

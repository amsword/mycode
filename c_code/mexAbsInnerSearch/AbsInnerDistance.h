#pragma once

#include "utility.h"

class AbsInnerDistance : public DistanceCalculator
{
public:
	void Initialize(
		int is_opt_R,
		const SMatrix<double> &matRotation,
		const Vector<SMatrix<double> > &vecmatCenters,
		int num_dic_each_partition
		);
public:
	virtual bool IsPreprocessingQuery() const;
	virtual double Distance(
		const void* p_query, const void* p_right) const;

	virtual void PreProcessing(const void* p_query, void* &p_pre_out) const;
	virtual double DistancePre(void* p_query, const void* p_right) const;
	virtual void PostProcessing(void* &p_pre_out) const;

private:
	struct InterPara
	{
		Vector<Vector<double> > lookup;
	};
public:
	int m_nNumberRawFeaturePartitions;
	int m_nDimension;
	int m_nNumDicEachPartitions;

	int m_is_opt_R;
    const SMatrix<double>* m_pmatRotation;
	const Vector<SMatrix<double> >* m_pvecmatCenters;
};

#include "utility.h"
#include <list>
#include "TypeConvert.h"
#include "AbsInnerDistance.h"
#include "RerankingKGamma.h"
#include "ExactLinearKEpsilon.h"

using namespace std;

typedef short typeCode;

#define VEC_QUERY prhs[0]
#define DATABASE_CODE prhs[1]
#define DISTANCE_INFO prhs[2]

#define RETRIEVAL_IDX plhs[0]
#define INFO plhs[1]

using namespace utility;


void ck_reranking(
	const Vector<double> &vecQuery, 
	const SMatrix<short> &mat_data_base_code, 
	const SMatrix<double> &mat_raw_data_base, 
	int max_expanded, 
	double gamma, 
	int num_candidate, 
	int is_opt_R,
	const SMatrix<double> &matRotationMatrix, 
	const Vector<SMatrix<double> > &vecmatCenters, 
	int num_dic_each_partition, 
	Vector<int> &vec_reranking_result, 
	double &time_scan, 
	double &time_reranking)
{
	AbsInnerDistance* p_dist;

	p_dist = new AbsInnerDistance();

	p_dist->Initialize(
		is_opt_R,
		matRotationMatrix,
		vecmatCenters,
		num_dic_each_partition);

	LinearSearchLargest<short> linear_search;
	linear_search.Initialize(&mat_data_base_code, p_dist);
	RerankingKGamma re_ranker;
	re_ranker.Initialize(mat_raw_data_base, max_expanded, gamma);

	Vector<int> vec_idx(num_candidate);

	clock_t begin = clock();
	linear_search.ANNSearching(vecQuery.Ptr(),  vec_idx.Ptr(), num_candidate);
	clock_t inter = clock();
	re_ranker.Reranking(vecQuery.Ptr(), vec_idx, vec_reranking_result);
	clock_t end = clock();

	time_scan = (inter - begin) / (double)CLOCKS_PER_SEC;
	time_reranking = (end - inter) / (double)CLOCKS_PER_SEC;
	delete p_dist;
	p_dist = NULL;
}

void ck_bound_reranking(
	const Vector<double> &vecQuery, 
	const SMatrix<short> &mat_data_base_code, 
	const SMatrix<double> &mat_raw_data_base, 
	int max_expanded, 
	double gamma, 
	int num_candidate, 
	int is_opt_R,
	const SMatrix<double> &matRotationMatrix, 
	const Vector<SMatrix<double> > &vecmatCenters, 
	int num_dic_each_partition, 
	const Vector<double> &vec_bound,
	Vector<int> &vec_reranking_result, 
	double &time_scan, 
	double &time_reranking)
{
	AbsInnerDistance* p_dist;

	p_dist = new AbsInnerDistance();

	p_dist->Initialize(
		is_opt_R,
		matRotationMatrix,
		vecmatCenters,
		num_dic_each_partition);

	ExactLinearKEpsilon linear_search;
	linear_search.Initialize(&mat_data_base_code, p_dist);
	linear_search.SetNumberCandidate(num_candidate);
	linear_search.SetBound(vec_bound);
	linear_search.SetGamma(gamma);

	RerankingKGamma re_ranker;
	re_ranker.Initialize(mat_raw_data_base, max_expanded, gamma);

	Vector<int> vec_idx;

	clock_t begin = clock();
	linear_search.ANNSearchingBoundReranking(vecQuery.Ptr(),  vec_idx);
	clock_t inter = clock();
	re_ranker.Reranking(vecQuery.Ptr(), vec_idx, vec_reranking_result);
	clock_t end = clock();

	time_scan = (inter - begin) / (double)CLOCKS_PER_SEC;
	time_reranking = (end - inter) / (double)CLOCKS_PER_SEC;
	delete p_dist;
	p_dist = NULL;
}


void ck_bound_less_reranking(
	const Vector<double> &vecQuery, 
	const SMatrix<short> &mat_data_base_code, 
	const SMatrix<double> &mat_raw_data_base, 
	int max_expanded, 
	double gamma, 
	int num_candidate, 
	int is_opt_R,
	const SMatrix<double> &matRotationMatrix, 
	const Vector<SMatrix<double> > &vecmatCenters, 
	int num_dic_each_partition, 
	const Vector<double> &vec_bound,
	Vector<int> &vec_reranking_result, 
	int &num_good,
	double &time_scan, 
	double &time_reranking)
{
	AbsInnerDistance* p_dist;

	p_dist = new AbsInnerDistance();

	p_dist->Initialize(
		is_opt_R,
		matRotationMatrix,
		vecmatCenters,
		num_dic_each_partition);

	ExactLinearKEpsilon linear_search;
	linear_search.Initialize(&mat_data_base_code, p_dist);
	linear_search.SetNumberCandidate(num_candidate);
	linear_search.SetBound(vec_bound);
	linear_search.SetGamma(gamma);
	linear_search.SetExpanded(max_expanded);

	RerankingKGamma re_ranker;
	re_ranker.Initialize(mat_raw_data_base, max_expanded, gamma);

	Vector<int> vec_idx;
	vector<int> vec_good;
	vec_good.clear();
	clock_t begin = clock();
	linear_search.ANNSearchingBoundLessReranking(vecQuery.Ptr(),  vec_good, vec_idx);
	clock_t inter = clock();
	num_good = vec_good.size();
	//PRINT << num_good << "\n";
	//SMART_ASSERT(num_good <= max_expanded)(num_good)(max_expanded).Exit();
	//re_ranker.Verify(vecQuery.Ptr(), vec_good);
	if (num_good >= max_expanded)
	{
		vec_reranking_result.AllocateSpace(num_good);
		for (int i = 0; i < num_good; i++)
		{
			vec_reranking_result[i] = vec_good[i];
		}
	}
	else
	{
		re_ranker.SetMaxExpanded(max_expanded - num_good);
		vector<int> vec_reranked;
		re_ranker.Select(vecQuery.Ptr(), vec_idx, vec_reranked);

		int total_size = vec_reranked.size() + vec_good.size();
		if (total_size > 0)
		{
			vec_reranking_result.AllocateSpace(total_size);
			int i = 0;
			for (; i < num_good; i++)
			{
				vec_reranking_result[i] = vec_good[i];
			}
			for (; i < total_size; i++)
			{
				vec_reranking_result[i] = vec_reranked[i - num_good];
			}
		}
	}
	clock_t end = clock();

	time_scan = (inter - begin) / (double)CLOCKS_PER_SEC;
	time_reranking = (end - inter) / (double)CLOCKS_PER_SEC;
	delete p_dist;
	p_dist = NULL;
}




/*
* DistanceInfo{0} = type:
* 	if type == 0 exhuasitive search, raw data;
DistanceInfo{1} = Database;
if type == 1 // exact search based on linear scan, ck-means;
DistanceInfo{1} = is_opt_r;
DistanceInfo{2} = R;
DistanceInfo{3} = all_D;
DistanceInfo{4} = num_dic_each_partition;
if type == 2
*/
void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 3)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2).Exit();

	Vector<double> vecQuery;
	SMatrix<short> mat_data_base_code;
	SMatrix<double> mat_raw_data_base;
	int is_opt_R;
	SMatrix<double> matRotationMatrix;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;
	int num_candidate; 
	double gamma;
	int max_expanded;
	int distance_type;
	Vector<double> vec_bounds;

	mexConvert(VEC_QUERY, vecQuery);
	mexConvert(DATABASE_CODE, mat_data_base_code);

	mxArray* p;
	p = mxGetField(DISTANCE_INFO, 0, "raw_database");
	mexConvert(p, mat_raw_data_base);
	p = mxGetField(DISTANCE_INFO, 0, "is_optimize_R");
	mexConvert(p, is_opt_R);
	p = mxGetField(DISTANCE_INFO, 0, "R");
	mexConvert(p, matRotationMatrix);
	p = mxGetField(DISTANCE_INFO, 0, "all_D");
	mexConvert(p, vecmatCenters);
	p = mxGetField(DISTANCE_INFO, 0, "num_sub_dic_each_partition");
	mexConvert(p, num_dic_each_partition);
	p = mxGetField(DISTANCE_INFO, 0, "num_ock_candidate");
	mexConvert(p, num_candidate);
	p = mxGetField(DISTANCE_INFO, 0, "gamma");
	mexConvert(p, gamma);
	p = mxGetField(DISTANCE_INFO, 0, "max_expanded");
	mexConvert(p, max_expanded);

	p = mxGetField(DISTANCE_INFO, 0, "distance_type");
	mexConvert(p, distance_type);

	Vector<int> vec_reranking_result;
	double time_scan;
	double time_reranking;
	int num_good;
	if (distance_type == 0)
	{
		ck_reranking(
			vecQuery, 
			mat_data_base_code, 
			mat_raw_data_base, 
			max_expanded, 
			gamma, 
			num_candidate, 
			is_opt_R,
			matRotationMatrix, 
			vecmatCenters, 
			num_dic_each_partition, 
			vec_reranking_result, 
			time_scan, 
			time_reranking);
	}
	else if (distance_type == 1)
	{
		p = mxGetField(DISTANCE_INFO, 0, "ock_bounds");
		mexConvert(p, vec_bounds);

		ck_bound_reranking(
			vecQuery, 
			mat_data_base_code, 
			mat_raw_data_base, 
			max_expanded, 
			gamma, 
			num_candidate, 
			is_opt_R,
			matRotationMatrix, 
			vecmatCenters, 
			num_dic_each_partition, 
			vec_bounds,
			vec_reranking_result, 
			time_scan, 
			time_reranking);
	}
	else if (distance_type == 2)
	{
		p = mxGetField(DISTANCE_INFO, 0, "ock_bounds");
		mexConvert(p, vec_bounds);

		ck_bound_less_reranking(
			vecQuery, 
			mat_data_base_code, 
			mat_raw_data_base, 
			max_expanded, 
			gamma, 
			num_candidate, 
			is_opt_R,
			matRotationMatrix, 
			vecmatCenters, 
			num_dic_each_partition, 
			vec_bounds,
			vec_reranking_result, 
			num_good,
			time_scan, 
			time_reranking);
	}

	int num_retrieved = vec_reranking_result.Size();

	RETRIEVAL_IDX = mxCreateDoubleMatrix(num_retrieved, 1, mxREAL);

	Vector<double> vec_RETRIEVAL_IDX;
	mexConvert(RETRIEVAL_IDX, vec_RETRIEVAL_IDX);
	for (int i = 0; i < num_retrieved; i++)
	{
		vec_RETRIEVAL_IDX[i] = vec_reranking_result[i] + 1;
	}

	mwSize size_struct[1];
	size_struct[0] = 1;
	const char * field_names[] = {"scan", "reranking", "num_good"}; 
	INFO = mxCreateStructArray(1, size_struct, 3, field_names); 

	mxSetFieldByNumber(INFO, 0, 0, mxCreateDoubleScalar(time_scan));
	mxSetFieldByNumber(INFO, 0, 1, mxCreateDoubleScalar(time_reranking));
	
	if (distance_type == 2)
	{
		mxSetFieldByNumber(INFO, 0, 2, mxCreateDoubleScalar(num_good));
	}

}

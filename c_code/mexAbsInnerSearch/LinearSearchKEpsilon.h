#pragma once

#include "utility.h"

using namespace utility;
#include "GlobalVar.h"
#include "LessPair.h"

class LinearSearchKEpsilon : public SearchEngine
{
public:
	void Initialize(const SMatrix<typeCode> * pDataBase, 
		DistanceCalculator* p_dist, 
		int numCandiate, 
		double gamma, 
		int max_expanded);

	void ANNSearching(void* p_query, Vector<int> &vec_idx);

private:
	void PopFromHeap(Heap<LessPair<double> >& heap, Vector<int> &vec_idx);
	void PopFromVector(const Vector<LessPair<double> >  &vec_satisfy_epsilon, 
		int num_satisfied_epsilon, Vector<int> &vec_idx) const;

	void ContineScanEpsilonFilter(void* query, 
		Heap<LessPair<double> > &heap_knn, 
		int num_checked_database_point);

	void VectorInsertHeap(
		const Vector<LessPair<double> > &vec_satisfy_epsilon, 
		int num_satisfied_epsilon, 
		Heap<LessPair<double> > &heap_knn);

	void FilterByEpsilon(
		void* p_query, 
		Vector<LessPair<double> >& vec_knn, 
		int &num_satisfied_epsilon, 
		int &num_checked_database_point) const;

	

protected:
	const SMatrix<typeCode>* m_pDataBase;
	const DistanceCalculator* m_pDistanceCalculator;
	int m_nNumCandidate;
	double m_dbGamma;
	int m_nMaxExpanded;
};

#pragma once
#include <list>
#include "utility.h"

#include "GlobalVar.h"

using namespace std;
using namespace utility;

class ExactLinearKEpsilon : public LinearSearchLargest<short>
{
public:
	void SetExpanded(int expanded);
	void SetBound(const Vector<double> &vec_bound);
	void SetGamma(double gamma);
	void ANNSearchingBoundReranking(const double* p_query, Vector<int> &vec_idx);
	void ANNSearchingBoundLessReranking(const double* p_query, vector<int> &vec_idx, Vector<int> &vec_candidate);

private:
	void SplitThreeSet(const double* p_query, 
		vector<int> &vec_good, 
			Heap<LessPair<double> > &heap_candidate) const;

	void SplitTwoSet(const double* p_query, Heap<LessPair<double> > &heap_candidate) const;
	void InsertHeap(const LessPair<double>& node, Heap<LessPair<double> > &heap_candidate) const;

private:
	const Vector<double>* m_pvecBound;
	double m_dbGamma;

	int m_nMaxExpanded;
};



#include "ExactLinearKEpsilon.h"
#include "LessPair.h"


void ExactLinearKEpsilon::SetExpanded(int expanded)
{
	m_nMaxExpanded = expanded;
}

void ExactLinearKEpsilon::SetBound(const Vector<double> &vec_bound)
{
	m_pvecBound = &vec_bound;
}

void ExactLinearKEpsilon::SetGamma(double gamma)
{
	m_dbGamma = gamma;
}

void ExactLinearKEpsilon::ANNSearchingBoundReranking(
	const double* p_query, Vector<int> &vec_idx)
{
	Heap<LessPair<double> > heap_candidate;

	SplitTwoSet(p_query, heap_candidate);
	int num_total = heap_candidate.size();
	vec_idx.AllocateSpace(num_total);
	PopFromHeap(heap_candidate, vec_idx.Ptr());
}

void ExactLinearKEpsilon::ANNSearchingBoundLessReranking(
	const double* p_query, vector<int> &vec_good, Vector<int> &vec_candidate)
{
	Heap<LessPair<double> > heap_candidate;
	SplitThreeSet(p_query, vec_good, heap_candidate);

	int num_good = vec_good.size();
	if (num_good >= m_nMaxExpanded)
	{
		return;
	}

	int num_total = heap_candidate.size();
	vec_candidate.AllocateSpace(num_total);
	PopFromHeap(heap_candidate, vec_candidate.Ptr());
}

void ExactLinearKEpsilon::InsertHeap(
	const LessPair<double>& node, Heap<LessPair<double> > &heap_candidate) const
{
	if (heap_candidate.size() < m_nNumCandidate)
	{
		heap_candidate.insert(node);
	}
	else
	{
		const LessPair<double> &top = heap_candidate.Top();
		if (top.distance < node.distance)
		{
			heap_candidate.popMin();
			heap_candidate.insert(node);
		}
	}
}

void ExactLinearKEpsilon::SplitTwoSet(
	const double* p_query, 
	Heap<LessPair<double> > &heap_candidate) const
{
	int num_database_point = this->m_pDataBase->Rows();
	heap_candidate.Reserve(m_nNumCandidate);
	void* p_pre_out;
	m_pDistanceCalculator->PreProcessing(p_query, p_pre_out);
	for (int i = 0; i < num_database_point; i++)
	{
		short* p_code = this->m_pDataBase->operator[](i);

		double app_dist = m_pDistanceCalculator->DistancePre(p_pre_out, p_code);
		double bound = m_pvecBound->operator[](i);

		if (app_dist > m_dbGamma)
		{
			InsertHeap(LessPair<double>(i, app_dist), heap_candidate);
		}
		else
		{
			if (app_dist + bound > m_dbGamma)
			{
				InsertHeap(LessPair<double>(i, app_dist), heap_candidate);
			}
		}
	}
}

void ExactLinearKEpsilon::SplitThreeSet(
	const double* p_query,	
	vector<int> &vec_good, 
	Heap<LessPair<double> > &heap_candidate) const
{
	vec_good.reserve(m_nMaxExpanded);
	int num_database_point = this->m_pDataBase->Rows();
	heap_candidate.Reserve(m_nNumCandidate);
	void* p_pre_out;
	m_pDistanceCalculator->PreProcessing(p_query, p_pre_out);
	for (int i = 0; i < num_database_point; i++)
	{
		short* p_code = this->m_pDataBase->operator[](i);

		double app_dist = m_pDistanceCalculator->DistancePre(p_pre_out, p_code);
		double bound = m_pvecBound->operator[](i);

		if (app_dist > m_dbGamma)
		{
			if (app_dist - bound > m_dbGamma)
			{
				vec_good.push_back(i);
				if (vec_good.size() >= m_nMaxExpanded)
				{
					break;
				}
			}
			else
			{
				InsertHeap(LessPair<double>(i, app_dist), heap_candidate);
			}
		}
		else
		{
			if (app_dist + bound > m_dbGamma)
			{
				InsertHeap(LessPair<double>(i, app_dist), heap_candidate);
			}
		}
	}
}


#include "LinearSearchKEpsilon.h"


void LinearSearchKEpsilon::VectorInsertHeap(
	const Vector<LessPair<double> > &vec_satisfy_epsilon, 
	int num_satisfied_epsilon, 
	Heap<LessPair<double> > &heap_knn)
{
	heap_knn.Reserve(m_nNumCandidate);
	int i;
	for (i = 0; i < m_nNumCandidate; i++)
	{
		heap_knn.insert(vec_satisfy_epsilon[i]);
	}
}

void LinearSearchKEpsilon::FilterByEpsilon(
	void* p_query, 
	Vector<LessPair<double> >& vec_knn, 
	int &num_satisfied_epsilon, 
	int &num_checked_database_point) const
{
	int i;
	num_satisfied_epsilon = 0;
	vec_knn.AllocateSpace(m_nNumCandidate);

	for (i = 0; i < m_nNumDataBasePoint; i++)
	{
		typeCode* p_code = m_pDataBase->operator[](i);
		double s = m_pDistanceCalculator->DistancePre(p_query, p_code);
		if (s > m_dbGamma)
		{
			vec_knn[num_satisfied_epsilon++] = LessPair<double>(i, s);
			if (num_satisfied_epsilon >= m_nNumCandidate)
			{
				break;
			}
		}
	}
	if (i < m_nNumDataBasePoint)
	{
		num_checked_database_point = i + 1;
	}
	else
	{
		num_checked_database_point = i;
	}
}

void LinearSearchKEpsilon::PopFromVector(
	const Vector<LessPair<double> >  &vec_satisfy_epsilon, 
	int num_satisfied_epsilon, 
	Vector<int> &vec_idx) const
{
	vec_idx.AllocateSpace(num_satisfied_epsilon);
	int i;
	for (i = 0; i < num_satisfied_epsilon; i++)
	{
		vec_idx[i] = vec_satisfy_epsilon[i].index;
	}
}

void LinearSearchKEpsilon::PopFromHeap(Heap<LessPair<double> >& heap, Vector<int> &vec_idx)
{
	int k = heap.size() - 1;
	vec_idx.AllocateSpace(m_nNumCandidate);
	int* p_idx = vec_idx.Ptr();

	while (!heap.empty())
	{
		LessPair<double> node;
		heap.popMin(node);
		*(p_idx + k) = node.index;
		k--;
	}
}

void LinearSearchKEpsilon::Initialize(
	const SMatrix<typeCode> * pDataBase, DistanceCalculator* p_dist, 
	int numCandiate, double gamma, int max_expanded)
{
	m_pDataBase = pDataBase;
	m_pDistanceCalculator = p_dist;

	m_nNumDataBasePoint = m_pDataBase->Rows();

	m_nNumCandidate = numCandiate;
	m_dbGamma = gamma;

	m_nMaxExpanded = max_expanded;
}

void LinearSearchKEpsilon::ANNSearching(void* p_query, Vector<int> &vec_idx)
{
	SMART_ASSERT (m_pDistanceCalculator->IsPreprocessingQuery()).Exit();

	void* query;
	m_pDistanceCalculator->PreProcessing(p_query, query);

	Vector<LessPair<double> > vec_satisfy_epsilon;
	int num_satisfied_epsilon;
	int num_checked_database_point;

	FilterByEpsilon(query, vec_satisfy_epsilon, num_satisfied_epsilon, num_checked_database_point);
	if (num_checked_database_point  >= m_nNumDataBasePoint)
	{
		PopFromVector(vec_satisfy_epsilon, num_satisfied_epsilon, vec_idx);
	}
	else
	{
		SMART_ASSERT(m_nNumCandidate == num_satisfied_epsilon).Exit();
		Heap<LessPair<double> > heap_knn;
		heap_knn.Reserve(m_nNumCandidate);

		VectorInsertHeap(vec_satisfy_epsilon, num_satisfied_epsilon, heap_knn);
		ContineScanEpsilonFilter(query, heap_knn, num_checked_database_point);
		PopFromHeap(heap_knn, vec_idx);
	}
	m_pDistanceCalculator->PostProcessing(query);
}

void LinearSearchKEpsilon::ContineScanEpsilonFilter(
	void* query, 
	Heap<LessPair<double> > &heap_knn, 
	int num_checked_database_point)
{
	int j;
	for (j = num_checked_database_point; j < m_nNumDataBasePoint; j++)
	{
		LessPair<double> node;

		double s = m_pDistanceCalculator->DistancePre(query, m_pDataBase->operator[](j));

		node.distance = s;
		const LessPair<double>& top = heap_knn.Top();
		if (top < node)
		{
			node.index = j;
			heap_knn.popMin();
			heap_knn.insert(node);
		}
	}
}

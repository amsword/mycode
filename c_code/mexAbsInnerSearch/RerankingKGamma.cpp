#include "RerankingKGamma.h"


void RerankingKGamma::Initialize(const SMatrix<double> &mat_raw_data_base, int max_expanded, double gamma)
{
	m_pmat_raw_data_base = &mat_raw_data_base;
	m_max_expanded = max_expanded;
	m_gamma = gamma;
}

void RerankingKGamma::SetMaxExpanded(int max_expanded)
{
	m_max_expanded = max_expanded;
}

void RerankingKGamma::Reranking(
	const double* query, 
	const Vector<int> &vec_idx, 
	Vector<int> &vec_reranking_result)
{
	int num_candiate = vec_idx.Size();
	int dim = m_pmat_raw_data_base->Cols();

	Heap<LessPair<double> > heap;
	heap.Reserve(m_max_expanded);

	for (int i = 0; i < num_candiate; i++)
	{
		int idx = vec_idx[i];
		double s = dot(query, m_pmat_raw_data_base->operator[](idx), dim);
		s = s >= 0 ? s : -s;
		if (s > m_gamma)
		{
			if (heap.size() < m_max_expanded)
			{
				heap.insert(LessPair<double>(idx, s));
			}
			else
			{
				const LessPair<double> &node = heap.Top();
				if (node.distance < s)
				{
					heap.popMin();
					heap.insert(LessPair<double>(idx, s));
				}
			}
		}
	}
	int num_retrieved = heap.size();
	vec_reranking_result.AllocateSpace(num_retrieved);
	for (int i = 0; i < num_retrieved; i++)
	{
		LessPair<double> node;
		heap.popMin(node);
		vec_reranking_result[num_retrieved - 1 - i] = node.index;
	}
}

void RerankingKGamma::Verify(const double* query, const vector<int> &vec_good)
{
	for (int i = 0; i < vec_good.size(); i++)
	{
		int idx = vec_good[i];
		double s = dot(query, m_pmat_raw_data_base->operator[](idx), m_pmat_raw_data_base->Cols());
		s = s >= 0 ? s : -s;
		SMART_ASSERT(s > m_gamma)(s)(m_gamma).Exit();

	}
}

void RerankingKGamma::Select(
	const double* query, 
	const Vector<int> &vec_idx, 
	vector<int> &vec_reranking_result)
{
	int num_candiate = vec_idx.Size();
	int dim = m_pmat_raw_data_base->Cols();

	vec_reranking_result.reserve(m_max_expanded);
	for (int i = 0; i < num_candiate; i++)
	{
		int idx = vec_idx[i];
		double s = dot(query, m_pmat_raw_data_base->operator[](idx), dim);
		s = s >= 0 ? s : -s;
		if (s > m_gamma)
		{
			vec_reranking_result.push_back(idx);
			if (vec_reranking_result.size() >= m_max_expanded)
			{
				break;
			}
		}
	}
}
#pragma once
#include <string>
#include "utility.h"


using namespace std;
using namespace utility;


typedef struct KmeansParam
{
	int max_iter;
	double min_error;

	int num_cluster;

	KmeansParam(int _max_iter, 
		double _min_error,
		int _num_cluster)
	{
		max_iter = _max_iter;
		min_error = _min_error;
		num_cluster = _num_cluster;
	}
	KmeansParam(){}
}KmeansParam;



class KMeans
{
public:
	KMeans();
	~KMeans();

public:
	
	void Training(
		const SMatrix<double> &mat_data, 
		KmeansParam &para, 
		SMatrix<double>& mat_centers);

	

	void GetInfo(int &num_iter, string &str);

private:
	void InitCenters();

	int UpdateAssignment();

	void UpdateCenter();

private:

	void SumUpCenters();
	void DivideByCount();

	void HandlingEmptyCenter(int idx_empty_center);
	int LargestCluster(int idx_empty_center);
	int FarthestPoint(int largest_cluster_idx);
private:
	const SMatrix<double>* m_pDataSet;
	SMatrix<double>* m_pCenters;
	Vector<int> m_vecAssignIndex;
	Vector<double> m_vecAssignSqError;
	
	Vector<int> m_vecClusterSize;
	int m_nEmptyCenter;

	KmeansParam* m_para;


	int m_numInterUsed;
	string m_strStopCondition;
};
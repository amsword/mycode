#include <string>

#include "KMeans.h"
#include "utility.h"

using namespace std;
using namespace utility;

#ifdef _WINDOWS
int main(int argc, char* argv[])
{
	try
	{
		KMeans kmeans;
		
		SMatrix<double> mat_data;
		mat_data.LoadData("C:\\Users\\J\\Desktop\\doc\\working\\NUS-WIDE-LITE\\training");

		SMatrix<double> mat_centers;
		mat_centers.LoadData("C:\\Users\\J\\Desktop\\doc\\working\\NUS-WIDE-LITE\\centers");

		KmeansParam para;
		para.max_iter = 100;
		para.min_error = 0.01;
		para.num_cluster = 256;
		kmeans.Training(mat_data, para, mat_centers);
	}
	catch( ...)
	{

	}

	return 0;
}
#endif
#include <omp.h>
#include "utility.h"
#include "KMeans.h"
#include "make_mex.h"
#include "TypeConvert.h"

#define DATA_BASE prhs[0]
#define NUM_CLUSTER prhs[1]
#define INPUT_CENTERS prhs[2]

#define MAX_ITER prhs[3]
#define MIN_ERROR prhs[4]

#define OUTPUT_CENTERS plhs[0]

//compact B
void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 5)(nrhs).Exit();
	SMART_ASSERT(nlhs == 1)(nlhs).Exit();
	int num_thd = omp_get_num_procs();
	omp_set_num_threads(num_thd);

	SMatrix<double> mat_database;
	SMatrix<double> mat_input_centers;
	SMatrix<double> mat_output_centers;
	int num_cluster;
	int max_iter;
	double min_error;

	mexConvert(DATA_BASE, mat_database);
	mexConvert(INPUT_CENTERS, mat_input_centers);
	mexConvert(NUM_CLUSTER, num_cluster);
	mexConvert(MAX_ITER, max_iter);
	mexConvert(MIN_ERROR, min_error);

	OUTPUT_CENTERS = mxCreateDoubleMatrix(mat_database.Cols(), num_cluster, mxREAL);
	mexConvert(OUTPUT_CENTERS, mat_output_centers);

	memcpy(mat_output_centers.Ptr(), 
		mat_input_centers.Ptr(), 
		sizeof(double) * mat_input_centers.Rows() * mat_input_centers.Cols());

	KMeans kmeans;
	KmeansParam para(max_iter, min_error, num_cluster);
	kmeans.Training(mat_database, para, mat_output_centers);

}
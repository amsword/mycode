#include "KMeans.h"

KMeans::KMeans()
{

}

KMeans::~KMeans()
{
}

void KMeans::InitCenters()
{
	int dim = m_pDataSet->Cols();
	if (m_pCenters->Rows() == m_para->num_cluster && m_pCenters->Cols() == dim)
	{
		return;
	}

	Vector<int> vec(m_para->num_cluster);

	utility::rand_perm(m_pDataSet->Rows(), vec.Ptr(), m_para->num_cluster);
		
	m_pCenters->AllocateSpace(m_para->num_cluster, dim);
	for (int i = 0; i < m_para->num_cluster; i++)
	{
		int idx_point = vec[i];
		memcpy(m_pCenters->operator[](i),
		m_pDataSet->operator[](idx_point), dim * sizeof(double));
	}
}

void KMeans::SumUpCenters()
{
	m_vecClusterSize.AllocateSpace(m_para->num_cluster);

	m_vecClusterSize.SetValueZeros();
	int num_point = m_vecAssignIndex.Size();

	int dim = m_pDataSet->Cols();
	for (int j = 0; j < num_point; j++)
	{
		int idx = m_vecAssignIndex[j];

		if (m_vecClusterSize[idx] == 0)
		{
			memcpy(m_pCenters->operator[](idx), 
				m_pDataSet->operator[](j), 
				sizeof(double) * dim);
		}
		else
		{
			VectorAdd(m_pCenters->operator[](idx), m_pDataSet->operator[](j), dim);
		}

		m_vecClusterSize[idx]++;
	}
}

void KMeans::DivideByCount()
{
	int num = m_pCenters->Cols();

	m_nEmptyCenter = 0;
	for (int i = 0; i < m_pCenters->Rows(); i++)
	{
		if (m_vecClusterSize[i] != 0)
		{
			double* p = (*m_pCenters)[i];
			double s = 1.0 / m_vecClusterSize[i];
			scale_multi_vector(s, (const double* )p, p, num);
		}
		else
		{
			HandlingEmptyCenter(i);
		}
	}
}

int KMeans::FarthestPoint(int largest_cluster_idx)
{
	int num_point = m_vecAssignIndex.Size();
	
	int farthest_point = -1;
	double farthest_dist = -1;
	for (int i = 0; i < num_point; i++)
	{
		int idx = m_vecAssignIndex[i];
		if (idx != largest_cluster_idx)
		{
			continue;
		}
		double dist = m_vecAssignSqError[i];
		if (dist > farthest_dist)
		{
			farthest_dist = dist;
			farthest_point = i;
		}
	}
}

int KMeans::LargestCluster(int idx_empty_center)
{
	int num_center = m_vecClusterSize.Size();

	int largest_cluster = -1;
	int largest_size = -1;
	for (int i = 0; i < num_center; i++)
	{
		if (i == idx_empty_center)
		{
			continue;
		}
		int current_size = m_vecClusterSize[i];
		if ( current_size > largest_size)
		{
			largest_size = current_size;
			largest_cluster = i;
		}
	}
	return largest_cluster;
}

void KMeans::HandlingEmptyCenter(int idx_empty_center)
{
	int num_point = m_pDataSet->Rows();
	int idx = rand() / RAND_MAX * num_point;
	idx = idx >= 1 ? idx : 1;
	idx = idx >= num_point ? idx - 1 : idx;
	memcpy(m_pCenters->operator[](idx_empty_center),
		m_pDataSet->operator[](idx),
		m_pDataSet->Cols());
}

void KMeans::UpdateCenter()
{
	SumUpCenters();
	DivideByCount();
}

int KMeans::UpdateAssignment()
{
	utility::BruteforceKNNer<double> knn;

	int num_changed = 0;

#pragma omp parallel for reduction (+ : num_changed)
	for (int i = 0; i < m_pDataSet->Rows(); i++)
	{
		const double* query = m_pDataSet->operator[](i);

		int idx;
		knn.NearestEuclidean(query, *m_pCenters, 
			idx,
			m_vecAssignSqError.operator[](i));

		SMART_ASSERT(idx >= 0 && idx < m_pCenters->Rows())(idx).Exit();

		int &pre_idx = m_vecAssignIndex.operator[](i);

		if (pre_idx != idx)
		{
			pre_idx = idx;
			num_changed++;
		}
	}

	return num_changed;
}

void KMeans::GetInfo(int &num_iter, string &str)
{
	num_iter = m_numInterUsed;
	str = m_strStopCondition;
}

void KMeans::Training(
	const SMatrix<double> &mat_data, 
	KmeansParam &para, 
	SMatrix<double>& mat_centers)
{
	m_pDataSet = &mat_data;
	m_pCenters = &mat_centers;

	m_para = &para;	


	int num_point = m_pDataSet->Rows();
	m_vecAssignIndex.AllocateSpace(num_point);
	m_vecAssignSqError.AllocateSpace(num_point);
	
	InitCenters();
	m_vecAssignIndex.SetValueZeros();

	double pre = FLT_MAX;

	clock_t begin_time = clock();
	int i;
	for (i = 0; i < m_para->max_iter; i++)
	{
		int num_changed = UpdateAssignment();
		if (num_changed == 0)
		{
			m_strStopCondition = "Training exit: assignment is not changed";
			PRINT << m_strStopCondition << "\n";
			break;
		}

		double after_assign = m_vecAssignSqError.SumUp();
		if (pre - after_assign < m_para->min_error)
		{
			m_strStopCondition = "min error reached";
			PRINT << m_strStopCondition << "\n";
		}

		clock_t curr_time = clock();
		PRINT << "iter: " << i << "\t"
			<< "assignment changed: " << num_changed << "\t"
			<< "obj.(after assign): " << after_assign << "\t" 
			<< "Average time: " << (curr_time - begin_time) / (double)CLOCKS_PER_SEC << "\n";


		UpdateCenter();		
		pre = after_assign;
	}
	if (i == m_para->max_iter)
	{
		m_strStopCondition = "Training exit: maximum iterations reached";
		PRINT << m_strStopCondition << "\n";
	}
	m_numInterUsed = i;
}
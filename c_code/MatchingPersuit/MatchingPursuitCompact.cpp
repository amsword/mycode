#include <cfloat>
#include "MatchingPursuitCompact.h"

MatchingPersuitCompact::MatchingPersuitCompact()
	: m_nNumBestCan(10)
{
}

void MatchingPersuitCompact::BestNextWords(const double* p_residual, 
										   const SMatrix<double>& matDictionary, 
										   int idx_start,
										   int idx_end,
										   SMatrix<double> &mat_diff, 
										   Vector<short>& best_idx) const
{
	SMatrix<double> mat_each_diff(idx_end - idx_start, m_nDimension);

	Heap<PAIR<double> > pq;
	pq.Reserve(m_nNumBestCan);

	for (int i = idx_start; i < idx_end; i++)
	{
		const double* p_dic = matDictionary[i];

		double e = 0;
		for (int j = 0; j < m_nDimension; j++)
		{
			double d = p_residual[j] - p_dic[j];
			mat_each_diff[i - idx_start][j] = d;
			e += d * d;
		}

		if (pq.size() >= m_nNumBestCan)
		{
			const PAIR<double> &p = pq.Top();
			if (p.distance > e)
			{
				pq.popMin();

				pq.insert(PAIR<double>(i - idx_start, e));
			}
		}
		else
		{
			pq.insert(PAIR<double>(i - idx_start, e));
		}

	}

	mat_diff.AllocateSpace(m_nNumBestCan, m_nDimension);
	best_idx.AllocateSpace(m_nNumBestCan);
	for (int i = m_nNumBestCan - 1; i >= 0; i--)
	{
		PAIR<double> p;
		pq.popMin(p);
		best_idx[i] = p.index;
		memcpy(mat_diff[i], mat_each_diff[p.index], sizeof(double) * m_nDimension);
	}
}
void MatchingPersuitCompact::BestNextWord(const double* p_residual, 
										  const SMatrix<double>& matDictionary, 
										  int idx_start,
										  int idx_end,
										  double& best_error, short& best_idx) const
{
	best_error = DBL_MAX;
	best_idx = -1;
	for(int i = idx_start; i < idx_end; i++)
	{
		double err = squared_distance(p_residual,
			matDictionary[i], m_nDimension);
		if (err < best_error)
		{
			best_error = err;;
			best_idx = i;
		}
	}
	//best_idx -= idx_start;
}

void MatchingPersuitCompact::BestNextWord(const double* p_residual, 
										  const SMatrix<double>& matDictionary, 
										  double& best_error, int& best_idx) const
{
	best_error = DBL_MAX;
	best_idx = -1;
	for(int i = 0; i < m_nDictionarySize; i++)
	{
		double err = squared_distance(p_residual,
			matDictionary[i], m_nDimension);
		if (err < best_error)
		{
			best_error = err;;
			best_idx = i;
		}
	}
}

void MatchingPersuitCompact::BestNextWord(double* p_residual, 
										  const SMatrix<double>& matDictionary, 
										  const bool* p_rest_dict,
										  double& best_error, 
										  int& best_idx) const
{
	best_error = DBL_MAX;
	best_idx = -1;
	for(int i = 0; i < m_nDictionarySize; i++)
	{
		if (!p_rest_dict[i])
		{
			double err = squared_distance(p_residual,
				matDictionary[i], m_nDimension);
			if (err < best_error)
			{
				best_error = err;;
				best_idx = i;
			}
		}
	}
}

void MatchingPersuitCompact::SolveOptimized(const double* pz,
											const SMatrix<double>& matDictionary,
											short* prepresentation) const
{
	Vector<double> vec_residual(m_nDimension);
	memcpy(vec_residual.Ptr(), pz, sizeof(double) * m_nDimension);

	double pre_error = vec_residual.Norm2Squared();

	Vector<bool> vec_representation(m_nDictionarySize);
	if (m_num_dic >= 0)
	{
		vec_representation.SetValueZeros();
	}

	double best_error;
	short best_idx;

	int idx_start = 0;
	for (int i = 0; i < m_num_dic; i++)
	{
		idx_start = i * m_nSubDictionarySize;

		//BestNextWord(vec_residual.Ptr(), matDictionary, vec_representation.Ptr(), 
		//best_error, best_idx);

		BestNextWord(vec_residual.Ptr(), matDictionary,
			idx_start, idx_start + m_nSubDictionarySize,
			best_error, best_idx);

		if (pre_error > best_error)
		{
			vec_residual -= matDictionary[best_idx];
			pre_error = best_error;

			//int mask_start = best_idx / m_nSubDictionarySize;
			//memset(vec_representation.Ptr() + mask_start, 1, sizeof(bool) * m_nSubDictionarySize);

			prepresentation[i] = best_idx;
		}
		else
		{
			break;
		}
	}
}

void MatchingPersuitCompact::BestNextWordsSMart(
	const Vector<double> &vec_x_map, 
	const SMatrix<double> &matInnerProduct, 
	const short* prepresentation,
	int idx, 
	short next_idx[], 
	double next_errors[]) const
{
	Heap<PAIR<double> > pq;
	pq.Reserve(m_nNumBestCan);

	int idx_start = idx * m_nSubDictionarySize;
	int idx_end = idx_start + m_nSubDictionarySize;

	for (int i = idx_start; i < idx_end; i++)
	{
		// compoute the relative error
		double e = -vec_x_map[i];
		const double* p_inner = matInnerProduct[i];
		for (int j = 0; j < idx; j++)
		{
			e += p_inner[prepresentation[j]];
		}
		e += 0.5 * p_inner[i];

		if (pq.size() >= m_nNumBestCan)
		{
			const PAIR<double> &p = pq.Top();
			if (p.distance > e)
			{
				pq.popMin();

				pq.insert(PAIR<double>(i, e));
			}
		}
		else
		{
			pq.insert(PAIR<double>(i, e));
		}
	}

	for (int i = m_nNumBestCan - 1; i >= 0; i--)
	{
		PAIR<double> p;
		pq.popMin(p);
		next_idx[i] = p.index;
		next_errors[i] = p.distance;
	}
}

void MatchingPersuitCompact::SolveOptimizedAdv2Recursive(
	const Vector<double> &vec_x_map, 
	const SMatrix<double> &matInnerProduct, 
	int idx, 
	short* prepresentation, 
	double &error) const
{
	if (idx >= m_num_dic)
	{
		return;
	}
	short next_idx[MAX_NUM_CANDIDATE];
	double next_errors[MAX_NUM_CANDIDATE];
	BestNextWordsSMart(vec_x_map, matInnerProduct, 
		prepresentation, idx, 
		next_idx, next_errors);

	//SMART_ASSERT(0)(Vector<short>(next_idx, 10)).Exit();


	double min_error = DBL_MAX;
	short min_representation[MAX_SPARSITY];

	for (int i = 0; i < m_nNumBestCan; i++)
	{
		*(prepresentation + idx) = next_idx[i];

		next_errors[i] += error;
		SolveOptimizedAdv2Recursive(vec_x_map, 
			matInnerProduct, 
			idx + 1, 
			prepresentation, 
			next_errors[i]);

		if (min_error > next_errors[i])
		{
			min_error = next_errors[i];
			memcpy(min_representation, prepresentation, sizeof(short) * m_num_dic);
		}
	}
	error = min_error;
	memcpy(prepresentation, min_representation, sizeof(short) * m_num_dic);
}

void MatchingPersuitCompact::SolveOptimizedAdvRecursive(const double* pz,
														const SMatrix<double>& matDictionary,
														int idx,
														short* prepresentation,
														double &error) const
{
	int idx_start = idx * m_nSubDictionarySize;

	if (idx == m_num_dic - 1) // just finding the best case is OK
	{
		BestNextWord(pz, matDictionary, idx_start, idx_start + m_nSubDictionarySize,
			error, prepresentation[0]);
	}
	else
	{
		Vector<short> vec_idx;
		SMatrix<double> mat_diff;
		double best_error = DBL_MAX;
		int best_idx;

		BestNextWords(pz, matDictionary, idx_start, idx_start + m_nSubDictionarySize,
			mat_diff, vec_idx);

		//SMART_ASSERT(0)(vec_idx).Exit();

		double tmp_error;
		Vector<short> vec_representation(m_num_dic - idx - 1);
		Vector<short> vec_best_representation(m_num_dic - idx - 1);

		for (int i = 0; i < m_nNumBestCan; i++)
		{
			SolveOptimizedAdvRecursive(mat_diff[i],
				matDictionary,
				idx + 1,
				vec_representation.Ptr(),
				tmp_error);

			if (tmp_error < best_error)
			{
				best_error = tmp_error;
				prepresentation[0] = vec_idx[i];

				//SMART_ASSERT (prepresentation[0] >= 0 
				//	&& prepresentation[0] < 512)(prepresentation[0]).Exit();


				vec_representation.CopyTo(vec_best_representation);
			}
		}
		error = best_error;
		memcpy(prepresentation + 1, 
			vec_best_representation.Ptr(), 
			sizeof(short) * (m_num_dic - idx - 1));
	}
}

void MatchingPersuitCompact::SolveOptimizedAdv(const double* pz,
											   const SMatrix<double>& matDictionary,
											   short* prepresentation) const
{
	double e;

	SolveOptimizedAdvRecursive(pz, matDictionary, 0, prepresentation, e);
}


void MatchingPersuitCompact::SolveOptimizedAdv2(const double* pz,
												const SMatrix<double>& matDictionary, 
												const SMatrix<double>& matInnerProduct,
												short* prepresentation) const
{
	double e = 0;

	Vector<double> vec_x_map(m_nDictionarySize);
	for (int i = 0; i < m_nDictionarySize; i++)
	{
		vec_x_map[i] = dot(pz, matDictionary[i], matDictionary.Cols());
	}

	SolveOptimizedAdv2Recursive(vec_x_map, matInnerProduct, 0, prepresentation, e);
}

void MatchingPersuitCompact::SolveKMeans(const double* pz,
										 const SMatrix<double>& matDictionary,
										 short* prepresentation) const
{
	Vector<double> vec_residual(m_nDimension);
	memcpy(vec_residual.Ptr(), pz, sizeof(double) * m_nDimension);

	double best_error;
	int best_idx;

	BestNextWord(vec_residual.Ptr(), matDictionary, best_error, best_idx);
	SMART_ASSERT(best_idx >= 0 && best_idx < m_nDictionarySize)(best_idx).Exit();
	prepresentation[0] = best_idx;
}


//void MatchingPersuitCompact::Solve(const double* pz,
//								   const SMatrix<double>& matDictionary,
//								   short* prepresentation) const
//{
//	Vector<double> vec_residual(m_nDimension);
//	memcpy(vec_residual.Ptr(), pz, sizeof(double) * m_nDimension);
//
//	double pre_error = vec_residual.Norm2Squared();
//
//	Vector<bool> vec_representation(m_nDictionarySize);
//	vec_representation.SetValueZeros();
//	
//	double best_error;
//	int best_idx;
//
//	int idx_start = 0;
//	for (int i = 0; i < m_num_dic; i++)
//	{
//		switch (m_method)
//		{
//		case 0:
//			BestNextWord(vec_residual.Ptr(), matDictionary, vec_representation.Ptr(), 
//				best_error, best_idx);
//			break;
//
//		case 1: 
//			BestNextWord(vec_residual.Ptr(), matDictionary, 
//				best_error, best_idx);
//			break;
//
//		case 2:
//			idx_start = i * m_nSubDictionarySize;
//
//			BestNextWord(vec_residual.Ptr(), matDictionary, vec_representation.Ptr(), 
//				best_error, best_idx);
//
//			/*BestNextWord(vec_residual.Ptr(), matDictionary,
//			idx_start, idx_start + m_nSubDictionarySize,
//			best_error, best_idx);*/
//			break;
//		}
//
//		//if (pre_error > best_error)
//		{
//			vec_residual -= matDictionary[best_idx];
//			pre_error = best_error;
//			if (m_method == 0)
//			{
//				vec_representation[best_idx] = 1;
//			}
//			else if (m_method == 2)
//			{
//
//			}
//			prepresentation[i] = best_idx;
//		}
//		//else
//		//{
//		//	break;
//		//}
//	}
//}

//void MatchingPersuitCompact::SolveKMeans(const SMatrix<double>& matZ,
//	const SMatrix<double>& matDictionary,
//	SMatrix<short>& matRepresentation)
//{
//	m_nNumPoint = matZ.Rows();
//	m_nDictionarySize = matDictionary.Rows();
//	m_nDimension = matZ.Cols();
//
//	SMART_ASSERT(m_nNumPoint == matRepresentation.Rows())(matRepresentation.Rows()).Exit();
//	
//	matRepresentation.SetValue(-1);
//
//#pragma omp parallel for
//	for (int i = 0; i < m_nNumPoint; i++)
//	{
//		const double* pz = matZ[i];
//		short* prepresentation = matRepresentation[i];
//
//		SolveKMeans(pz, matDictionary, prepresentation);
//	}
//}

void MatchingPersuitCompact::SetCandidateNumber(int num_can)
{
	m_nNumBestCan = num_can;
}


void MatchingPersuitCompact::MultiDictionaryPreprocess(
	SMatrix<double> &matInnerProduct) const
{
	matInnerProduct.AllocateSpace(m_nDictionarySize, m_nDictionarySize);
	for (int i = 0; i < m_nDictionarySize; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			matInnerProduct[i][j] = dot(
				m_pMatDictionary->operator[](i), 
				m_pMatDictionary->operator[](j), 
				m_nDimension);
		}
	}
	for (int i = 0; i < m_nDictionarySize; i++)
	{
		for (int j = i + 1; j < m_nDictionarySize; j++)
		{
			matInnerProduct[i][j] = matInnerProduct[j][i];
		}
	}

}
void MatchingPersuitCompact::Solve(const SMatrix<double>& matZ,
								   const SMatrix<double>& matDictionary,
								   int num_dic,
								   SMatrix<short>& matRepresentation)
{
	m_pMatDictionary = &matDictionary;
	m_nNumPoint = matZ.Rows();
	m_nDictionarySize = matDictionary.Rows();
	m_nDimension = matZ.Cols();
	m_num_dic = num_dic;
	m_nSubDictionarySize = m_nDictionarySize / m_num_dic;

	SMART_ASSERT(m_nNumPoint == matRepresentation.Rows())(matRepresentation.Rows()).Exit();
	SMART_ASSERT(m_num_dic <= MAX_SPARSITY)(m_num_dic)(MAX_SPARSITY).Exit();
	SMART_ASSERT((m_nDictionarySize % m_num_dic) == 0)
		(m_nDictionarySize)(m_num_dic).Exit();

	SMatrix<double> matInnerProduct;

	if (m_num_dic > 1) // for ock-means in the TKDE paper
	{
		MultiDictionaryPreprocess(matInnerProduct);
	}

	matRepresentation.SetValue(-1);

#pragma omp parallel for
	for (int i = 0; i < m_nNumPoint; i++)
	{
		const double* pz = matZ[i];
		short* prepresentation = matRepresentation[i];

		if (m_num_dic == 1)
		{
			SolveKMeans(pz, matDictionary, prepresentation);
		}
		else
		{
			//Vector<short> aux(matRepresentation.Cols());
			//SolveOptimizedAdv(pz, matDictionary, 
			//	aux.Ptr());
			SolveOptimizedAdv2(pz, matDictionary, 
				matInnerProduct, 
				prepresentation);

			//for (int j = 0; j < matRepresentation.Cols(); j++)
			//{
			//	SMART_ASSERT(aux[j] == prepresentation[j])(i)(aux)(Vector<short>(prepresentation, matRepresentation.Cols())).Exit();
			//}
		}

	}
}

#include "TypeConvert.h"
#include "MatchingPursuitCompact.h"
#include "make_mex.h"
#include <omp.h>

#define MAT_Z prhs[0] 
#define MAT_DIC prhs[1]
#define N_SPARSITY prhs[2]
#define NUM_CAN_BEST prhs[3]

#define BINARY_REPRESENTATION plhs[0]

//compact B
void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	int num_thd = omp_get_num_procs();
	omp_set_num_threads(num_thd);

	SMART_ASSERT(nrhs >= 3).Exit();

	SMatrix<double> matZ;
	mexConvert(MAT_Z, matZ);

	int num_dic;
	mexConvert(N_SPARSITY, num_dic);
	SMART_ASSERT(num_dic > 0)
		(num_dic).Exit();

	SMatrix<double> matDictionary;
	mexConvert(MAT_DIC, matDictionary);

	mwSize size[2];
	size[0] = num_dic;
	size[1] = matZ.Rows();

	BINARY_REPRESENTATION = mxCreateNumericArray(2, size, mxINT16_CLASS, mxREAL);

	SMatrix<short> matRepresentation;
	mexConvert(BINARY_REPRESENTATION, matRepresentation);

	// method:
	// 2: ock-means described in the paper
	// others: jck-means
	MatchingPersuitCompact mp;

	if (num_dic > 1)
	{
		SMART_ASSERT(nrhs == 4)(nrhs).Exit();
		int num_candidate;
		mexConvert(NUM_CAN_BEST, num_candidate);
		mp.SetCandidateNumber(num_candidate);
	}

	mp.Solve(matZ, matDictionary, num_dic, matRepresentation);
}


//void main()
//{
//	string str_dir = "F:\\v-jianfw\\hash\\v-jianfw\\Data_HashCode\\Labelme\\working_sck_means\\test\\new_representation_b\\";
//
//	SMatrix<double> matZ;
//	matZ.LoadData(str_dir + "subZ");
//
//	int num_dic = -1;
//
//	int method = 0;
//
//	SMatrix<double> matDictionary;
//	matDictionary.LoadData(str_dir + "subD");
//
//	int size[2];
//	size[0] = num_dic == -1 ? 1 :num_dic;
//	size[1] = matZ.Rows();
//
//	MatchingPersuitCompact mp;
//	SMatrix<short> matRepresentation;
//	matRepresentation.AllocateSpace(size[1], size[0]);
//
//	mp.Solve(matZ, matDictionary, num_dic, matRepresentation, method);
//}

// demo code
//void main()
//{
//	//SMART_ASSERT(0).Exit();
//
//
//	SMatrix<double> matZ;
//	//mexConvert(MAT_Z, matZ);
//	
//	matZ.LoadData("../data/subZ.double.bin");
//	/*{
//		matZ.AllocateSpace(1, 4);
//		matZ.SetValueZeros();
//		matZ[0][0] = 1;
//		matZ[0][2] = 1;
//	}*/
//
//
//	int num_dic = 2;
//	//mexConvert(N_SPARSITY, num_dic);
//
//	int method = 52;
//	//mexConvert(NUM_CAN_BEST, method);
//
//	SMatrix<double> matDictionary;
//	//mexConvert(MAT_DIC, matDictionary);
//	matDictionary.LoadData("../data/subD.double.bin");
//	/*{
//		matDictionary.AllocateSpace(4, 4);
//		matDictionary.SetValueZeros();
//		for (int i = 0; i < 4; i++)
//		{
//			matDictionary[i][i] = 1;
//		}
//	}*/
//
//	//mwSize size[2];
//	//size[0] = num_dic == -1 ? 1 :num_dic;
//	//size[1] = matZ.Rows();
//
//	//BINARY_REPRESENTATION = mxCreateNumericArray(2, size, mxINT16_CLASS, mxREAL);
//
//	SMatrix<short> matRepresentation;
//	matRepresentation.AllocateSpace(matZ.Rows(), num_dic);
//	//mexConvert(BINARY_REPRESENTATION, matRepresentation);
//	
//	if (method == 5)
//	{
//		BinaryCplex cplexer;
//		cplexer.Solve(matZ, matDictionary, num_dic, matRepresentation);
//	}
//	else
//	{
//		MatchingPersuitCompact mp;
//		mp.Solve(matZ, matDictionary, num_dic, matRepresentation, method);
//	}
//}
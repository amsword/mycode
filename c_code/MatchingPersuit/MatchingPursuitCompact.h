#pragma once

#include "utility.h"

#define MAX_SPARSITY 8
#define MAX_NUM_CANDIDATE 20

using namespace utility;

/*
contraint type 0: k-means
constraint type 1: change the cardinality of the vector to be c
constraint type 2: change the cardinality of the vector to be c. partition-based
*/
class MatchingPersuitCompact
{
public:
	MatchingPersuitCompact();

	void SetCandidateNumber(int num_can);
	void Solve(const SMatrix<double>& matZ,
		const SMatrix<double>& matDictionary,
		int num_dic,
		SMatrix<short>& matRepresentation); // the method value only valid when num_dic is larger than 1

	// constraint type 0:
	/*void SolveKMeans(const SMatrix<double>& matZ,
		const SMatrix<double>& matDictionary,
		SMatrix<short>& matRepresentation);
*/
private:
	void MultiDictionaryPreprocess(SMatrix<double> &matInnerProduct) const;
	void Solve(const double* pz,
		const SMatrix<double>& matDictionary,
		short* prepresentation) const;

	void SolveOptimized(const double* pz,
		const SMatrix<double>& matDictionary,
		short* prepresentation) const;

	void SolveOptimizedAdv(const double* pz,
		const SMatrix<double>& matDictionary,
		short* prepresentation) const;

	void SolveOptimizedAdv2(const double* pz,
		const SMatrix<double>& matDictionary, 
		const SMatrix<double>& matInnerProduct,
		short* prepresentation) const;

	void SolveOptimizedAdv2Recursive(
		const Vector<double> &vec_x_map, 
		const SMatrix<double> &matInnerProduct, 
		int idx, 
		short* prepresentation, 
		double &error) const;

	void BestNextWordsSMart(
		const Vector<double> &vec_x_map, 
		const SMatrix<double> &matInnerProduct, 
		const short* prepresentation,
		int idx, 
		short next_idx[], 
		double next_errors[]) const;


	void SolveOptimizedAdvRecursive(const double* pz,
		const SMatrix<double>& matDictionary,
		int idx,
		short* prepresentation,
		double &error) const;

	void SolveKMeans(const double* pz,
		const SMatrix<double>& matDictionary,
		short* prepresentation) const;


private:
	void BestNextWord(double* p_residual, 
		const SMatrix<double>& matDictionary, 
		const bool* p_rest_dict,
		double& best_error, int& best_idx) const;

	void BestNextWord(const double* p_residual, 
		const SMatrix<double>& matDictionary, 
		double& best_error, int& best_idx) const;

	void BestNextWord(const double* p_residual, 
		const SMatrix<double>& matDictionary, 
		int idx_start,
		int idx_end,
		double& best_error, short& best_idx) const;

	void BestNextWords(const double* p_residual, 
		const SMatrix<double>& matDictionary, 
		int idx_start,
		int idx_end,
		SMatrix<double> &mat_diff, Vector<short>& best_idx) const;


private:
	int m_nNumPoint;
	int m_nDictionarySize;
	int m_nSubDictionarySize;
	int m_nDimension;
	int m_num_dic;
	int m_nNumBestCan;

	const SMatrix<double>* m_pMatDictionary;
};

// #include "utility.h"
// #include "TypeConvert.h"
#include "mex.h"
#include <omp.h>
// using namespace utility;

void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	int x = 3;

printf("%d\n", omp_get_num_threads());
	int i = 0;
	int s = 0;
#pragma omp parallel for
	for (i = 0; i < 10; i++)
	{
#pragma omp atomic
		s += i;
	}


	plhs[0] = mxCreateDoubleScalar(s);
}

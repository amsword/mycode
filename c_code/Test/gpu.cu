#include <stdio.h>
#include <boost/thread.hpp>
#include <boost/thread/tss.hpp>

const size_t N = (long long)3 * (long long)1024 * (long long)1024 * (long long)1024; 

char* buffer_in_gpu_at_thread;
char* buffer_in_gpu_at_main;
char* buffer_in_cpu;

#define CUDA_CHECK(x) \
    if ((x) != cudaSuccess) { \
        printf("error"); \
    }
boost::thread_specific_ptr<int> tss;
bool is_ok;
void process() {
    cudaSetDevice(2);

    if (tss.get() == NULL) {
        tss.reset(new int());
        *tss = 2;
        printf("%d\n", *tss);
    }
    const int csize = N*sizeof(char);
    cudaMalloc( (void**)&buffer_in_gpu_at_thread, csize ); 

    buffer_in_cpu = new char[N];
    for (size_t i = 0; i < N; i++) {
        buffer_in_cpu[i] = i;
    }
    cudaMemcpy(buffer_in_gpu_at_thread, buffer_in_cpu, N, 
            cudaMemcpyHostToDevice);
    is_ok = true;
    while(1);
}

int main()
{
    const int* p = new int();
    int* const = new int();


    clock_t begin;
    clock_t end;
    buffer_in_cpu = new char[N];
    for (size_t i = 0; i < N; i++) {
        buffer_in_cpu[i] = i;
    }

    cudaSetDevice(2);
    CUDA_CHECK(cudaMalloc((void**)&buffer_in_gpu_at_main, N));

    begin = clock();
    CUDA_CHECK(cudaMemcpy(buffer_in_gpu_at_main, buffer_in_cpu, 
            N, cudaMemcpyDefault));
    end = clock();
    printf("cpu->gpu, sync: %lf\n", (end - begin) / (double)CLOCKS_PER_SEC);

    begin = clock();
    CUDA_CHECK(cudaMemcpy(buffer_in_cpu, 
            buffer_in_gpu_at_main, 
            N, cudaMemcpyDeviceToHost));
    end = clock();
    printf("gpu->cpu, sync: %lf\n", (end - begin) / (double)CLOCKS_PER_SEC);
    //cudaDeviceSynchronize();

    begin = clock();
    CUDA_CHECK(cudaMemcpyAsync(buffer_in_cpu, 
            buffer_in_gpu_at_main, 
            N, cudaMemcpyDeviceToHost));
    end = clock();
    printf("gpu->cpu, async: %lf\n", (end - begin) / (double)CLOCKS_PER_SEC);

    //cudaSetDevice(1);

    begin = clock();
    cudaDeviceSynchronize();
    end = clock();

    printf("sync %lf\n", (end - begin) / (double)CLOCKS_PER_SEC);
    while(1);
    
    cudaSetDevice(0);
    printf("good\n");
    return EXIT_SUCCESS;
}

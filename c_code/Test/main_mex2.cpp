#include "mex.h"
#include <math.h>
#include <omp.h>

void mexFunction(int nlhs,mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	omp_set_num_threads(4);
	int sum = 0;
	int i;
	#pragma omp parallel for reduction(+: sum)
	for(i = 0;i<10;i++)
	{
		sum += omp_get_num_threads();
	     // sum += sin(i * i * i * i)/cos(i * i * i * i * i);
	}
	printf("sum: %d\n", sum);
	printf("%d\n", omp_get_num_threads());


int count;
   #pragma omp parallel for private(count) num_threads(3)
   for( count = 0; count < 3; count++ )
   {
      int th_id = omp_get_thread_num();
      printf("Hello World from thread %d, %d\n", th_id, count);
   }
   printf( "Finished\n" );

	// SMART_ASSERT(0)("GOOD").Exit();
	//cout << __FILE__ << "\n";
	//Logger logger;
	//logger << 2 << "\n";
}
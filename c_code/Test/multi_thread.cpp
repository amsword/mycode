#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <string>
#include <fstream>

using namespace std;

enum Command {
    WAIT,
    QUIT,
    RUN,
};


class InfiniteWorker
{
public:
    InfiniteWorker() {
        is_finished_ = true;
        worker_type_ = WAIT;
        th_.reset(new boost::thread(&InfiniteWorker::entry, this));
    }

    virtual ~InfiniteWorker() {
        send_cmd(QUIT);
        th_->join();
    }

public:
    void send_cmd(Command type) {
        wait_ready();
        boost::mutex::scoped_lock lck(mu_worker_type_);
        worker_type_ = type;
        con_worker_type_.notify_one();
    }
    void wait_ready() {
        boost::mutex::scoped_lock lck(mu_status_);
        while (is_finished_ == false) {
            con_result_.wait(lck);
        }
    }

protected:
    void entry() {
        while (1) {
            // get current working type, -1 means waiting. 
            Command type = WAIT;
            {
                boost::mutex::scoped_lock lock(mu_worker_type_);
                while (worker_type_ == WAIT) {
                    con_worker_type_.wait(lock); 
                }
                type = worker_type_;
                worker_type_ = WAIT;
            }
            // begin to work
            is_finished_ = false;
            // working 
            if (type == QUIT) {
                break;
            } else {
                process();
            }
            // finish
            is_finished_ = true;
            con_result_.notify_one();
        }
    }

    virtual void process() {
    }

protected:
    boost::shared_ptr<boost::thread> th_;

    // received the worker type;
    boost::condition_variable con_worker_type_;
    boost::mutex mu_worker_type_;
    Command worker_type_;

    // finished working
    boost::condition_variable con_result_;
    boost::mutex mu_status_;
    bool is_finished_;
};

class Worker: public InfiniteWorker {
    public:
        Worker(int id) : id_(id) {
        }

    protected:
        virtual void process() {
            std::cout << id_; 
            boost::this_thread::sleep(boost::posix_time::seconds(1));
        }
    protected:
        int id_;
};

class WorkerGroup {
    public:
        WorkerGroup(int count) {
            vec_pworker.resize(count);
            for (int i = 0; i < count; i++)
            {
                vec_pworker[i].reset(new Worker(i));
            }
        }
        void Run() {
            for (size_t i = 0; i < vec_pworker.size(); i++) {
                vec_pworker[i]->send_cmd(RUN);
            }
            for (size_t i = 0; i < vec_pworker.size(); i++) {
                vec_pworker[i]->wait_ready();
            }
            std::cout << "\n";
        }

    protected:
        vector<boost::shared_ptr<Worker> > vec_pworker;
};
int main()
{
    WorkerGroup wg(10);
    wg.Run();
    return 0;
}
int main103()
{
    std::vector<boost::shared_ptr<Worker> > vec_workers(10);
    for (int i = 0; i < 10; i++) {
        vec_workers[i].reset(new Worker(i));
    }
    for (int iter = 0; iter < 10; iter++) {
        std::cout << "begin: " << iter << "\n";
        for (int i = 0; i < 10; i++) {
            vec_workers[i]->send_cmd(RUN);
        }

        for (int i = 0; i < 10; i++) {
            vec_workers[i]->wait_ready();
        }
        std::cout << "\nfinished\n";
    }
    return 0;
}

/*******************************************************************************
!   Copyright(C) 1999-2011 Intel Corporation. All Rights Reserved.
!   
!   The source code, information  and  material ("Material") contained herein is
!   owned  by Intel Corporation or its suppliers or licensors, and title to such
!   Material remains  with Intel Corporation  or its suppliers or licensors. The
!   Material  contains proprietary information  of  Intel or  its  suppliers and
!   licensors. The  Material is protected by worldwide copyright laws and treaty
!   provisions. No  part  of  the  Material  may  be  used,  copied, reproduced,
!   modified, published, uploaded, posted, transmitted, distributed or disclosed
!   in any way  without Intel's  prior  express written  permission. No  license
!   under  any patent, copyright  or  other intellectual property rights  in the
!   Material  is  granted  to  or  conferred  upon  you,  either  expressly,  by
!   implication, inducement,  estoppel or  otherwise.  Any  license  under  such
!   intellectual  property  rights must  be express  and  approved  by  Intel in
!   writing.
!   
!   *Third Party trademarks are the property of their respective owners.
!   
!   Unless otherwise  agreed  by Intel  in writing, you may not remove  or alter
!   this  notice or  any other notice embedded  in Materials by Intel or Intel's
!   suppliers or licensors in any way.
!
!*******************************************************************************
!  Content:
!      Intel(R) Math Kernel Library (MKL) interface for PBLAS routines
!******************************************************************************/

#ifndef _MKL_PBLAS_H_
#define _MKL_PBLAS_H_

#include "mkl_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

// PBLAS Level 1 Routines

void	PSAMAX( MKL_INT *n, float *amax, MKL_INT *indx, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDAMAX( MKL_INT *n, double *amax, MKL_INT *indx, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PCAMAX( MKL_INT *n, float *amax, MKL_INT *indx, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PZAMAX( MKL_INT *n, double *amax, MKL_INT *indx, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSASUM( MKL_INT *n, float *asum, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDASUM( MKL_INT *n, double *asum, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSCASUM( MKL_INT *n, float *asum, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDZASUM( MKL_INT *n, double *asum, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSAXPY( MKL_INT *n, float *a, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDAXPY( MKL_INT *n, double *a, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCAXPY( MKL_INT *n, float *a, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZAXPY( MKL_INT *n, double *a, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PICOPY( MKL_INT *n, MKL_INT *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, MKL_INT *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PSCOPY( MKL_INT *n, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDCOPY( MKL_INT *n, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCCOPY( MKL_INT *n, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZCOPY( MKL_INT *n, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSDOT( MKL_INT *n, float *dot, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDDOT( MKL_INT *n, double *dot, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PCDOTC( MKL_INT *n, float *dotu, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZDOTC( MKL_INT *n, double *dotu, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PCDOTU( MKL_INT *n, float *dotu, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZDOTU( MKL_INT *n, double *dotu, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSNRM2( MKL_INT *n, float *norm2, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDNRM2( MKL_INT *n, double *norm2, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PSCNRM2( MKL_INT *n, float *norm2, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDZNRM2( MKL_INT *n, double *norm2, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSSCAL( MKL_INT *n, float *a, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDSCAL( MKL_INT *n, double *a, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PCSCAL( MKL_INT *n, float *a, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PZSCAL( MKL_INT *n, double *a, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PCSSCAL( MKL_INT *n, float *a, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PZDSCAL( MKL_INT *n, double *a, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSSWAP( MKL_INT *n, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDSWAP( MKL_INT *n, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCSWAP( MKL_INT *n, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZSWAP( MKL_INT *n, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );


// PBLAS Level 2 Routines

void	PSGEMV( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDGEMV( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCGEMV( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZGEMV( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSAGEMV( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDAGEMV( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCAGEMV( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZAGEMV( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSGER( MKL_INT *m, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PDGER( MKL_INT *m, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PCGERC( MKL_INT *m, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PZGERC( MKL_INT *m, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PCGERU( MKL_INT *m, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PZGERU( MKL_INT *m, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PCHEMV( char *uplo, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZHEMV( char *uplo, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PCAHEMV( char *uplo, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZAHEMV( char *uplo, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PCHER( char *uplo, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PZHER( char *uplo, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PCHER2( char *uplo, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PZHER2( char *uplo, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PSSYMV( char *uplo, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDSYMV( char *uplo, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSASYMV( char *uplo, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDASYMV( char *uplo, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSSYR( char *uplo, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PDSYR( char *uplo, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PSSYR2( char *uplo, MKL_INT *n, float *alpha, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );
void	PDSYR2( char *uplo, MKL_INT *n, double *alpha, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca );

void	PSTRMV( char *uplo, char *trans, char *diag, MKL_INT *n, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDTRMV( char *uplo, char *trans, char *diag, MKL_INT *n, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PCTRMV( char *uplo, char *trans, char *diag, MKL_INT *n, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PZTRMV( char *uplo, char *trans, char *diag, MKL_INT *n, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );

void	PSATRMV( char *uplo, char *trans, char *diag, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PDATRMV( char *uplo, char *trans, char *diag, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PCATRMV( char *uplo, char *trans, char *diag, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, float *beta, float *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );
void	PZATRMV( char *uplo, char *trans, char *diag, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx, double *beta, double *y, MKL_INT *iy, MKL_INT *jy, MKL_INT *descy, MKL_INT *incy );

void	PSTRSV( char *uplo, char *trans, char *diag, MKL_INT *n, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PDTRSV( char *uplo, char *trans, char *diag, MKL_INT *n, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PCTRSV( char *uplo, char *trans, char *diag, MKL_INT *n, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );
void	PZTRSV( char *uplo, char *trans, char *diag, MKL_INT *n, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *x, MKL_INT *ix, MKL_INT *jx, MKL_INT *descx, MKL_INT *incx );


// PBLAS Level 3 Routines

void	PSGEMM( char *transa, char *transb, MKL_INT *m, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDGEMM( char *transa, char *transb, MKL_INT *m, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCGEMM( char *transa, char *transb, MKL_INT *m, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZGEMM( char *transa, char *transb, MKL_INT *m, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PCHEMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZHEMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PCHERK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZHERK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PCHER2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZHER2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSSYMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDSYMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCSYMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZSYMM( char *side, char *uplo, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSSYRK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDSYRK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCSYRK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZSYRK( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSSYR2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDSYR2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCSYR2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZSYR2K( char *uplo, char *trans, MKL_INT *n, MKL_INT *k, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSTRAN( MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDTRAN( MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PCTRANU( MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZTRANU( MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PCTRANC( MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZTRANC( MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSTRMM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PDTRMM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PCTRMM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PZTRMM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );

void	PSTRSM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PDTRSM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PCTRSM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );
void	PZTRSM( char *side, char *uplo, char *transa, char *diag, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *b, MKL_INT *ib, MKL_INT *jb, MKL_INT *descb );

void	PSGEADD( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDGEADD( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCGEADD( char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZGEADD( char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

void	PSTRADD( char *uplo, char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PDTRADD( char *uplo, char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PCTRADD( char *uplo, char *trans, MKL_INT *m, MKL_INT *n, float *alpha, float *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, float *beta, float *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );
void	PZTRADD( char *uplo, char *trans, MKL_INT *m, MKL_INT *n, double *alpha, double *a, MKL_INT *ia, MKL_INT *ja, MKL_INT *desca, double *beta, double *c, MKL_INT *ic, MKL_INT *jc, MKL_INT *descc );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MKL_PBLAS_H_ */

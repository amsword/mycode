#pragma once
#include "../utility.h"

using namespace utility;

#pragma comment(lib, "../UtilityDll/MKL/lib/mkl_solver_lp64.lib")
#pragma comment(lib, "../UtilityDll/MKL/lib/mkl_intel_lp64.lib")
#pragma comment(lib, "../UtilityDll/MKL/lib/mkl_intel_thread.lib") 
#pragma comment(lib, "../UtilityDll/MKL/lib/mkl_core.lib")
#pragma comment(lib, "../UtilityDll/MKL/lib/libiomp5mt.lib")


class MKLInvoker
{
public:
	// A = U \Sigmma Vt
	// if pmatU or pmatVt is null, we will not return these values
	// k = min(m, n); m: rows of A; n : cols of A
	// U: m * k
	// Vt : k * n
	void SVD(const SMatrix<double>& matA,
		SMatrix<double>* pmatU, 
		SMatrix<double>* pmatVt,
		Vector<double>& vec);
};
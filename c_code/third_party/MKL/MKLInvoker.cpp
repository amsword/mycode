#include "MKLInvoker.h"

#include "include\mkl_lapacke.h"

//lapack_int LAPACKE_dgesvd( 
//	int matrix_order, 
//	char jobu, 
//	char jobvt, 
//	lapack_int m, 
//	lapack_int n, 
//	double* a, 
//	lapack_int lda, 
//	double* s, 
//	double* u, 
//	lapack_int ldu, 
//	double* vt, 
//	lapack_int ldvt, 
//	double* superb );

void MKLInvoker::SVD(const SMatrix<double>& matA,
		SMatrix<double>* pmatU, 
		SMatrix<double>* pmatVt,
		Vector<double>& vec)
{
	char jobu = (pmatU == NULL)? 'N' : 's';
	char jobvt = (pmatVt == NULL) ? 'N' : 's';
	int m = matA.Rows();
	int n = matA.Cols();
	int k = m < n ? m : n;

	double* a = matA.Ptr();

	vec.AllocateSpace(k);
	double* s = vec.Ptr();
	
	double* u = NULL;
	if (pmatU)
	{
		pmatU->AllocateSpace(m, k);
		u = pmatU->Ptr();
	}

	double* vt = NULL;
	if (pmatVt)
	{
		pmatVt->AllocateSpace(k, n);
		vt = pmatVt->Ptr();
	}

	Vector<double> vecB(k);
	double* superb = vecB.Ptr();
	LAPACKE_dgesvd( 
		LAPACK_ROW_MAJOR,
		jobu, 
		jobvt, 
		m, 
		n, 
		a, 
		n, 
		s, 
		u, 
		k, 
		vt, 
		n, 
		superb);
	
}
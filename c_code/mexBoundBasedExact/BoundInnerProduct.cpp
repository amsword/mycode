#include "BoundInnerProduct.h"
#include "utility.h"

using namespace utility;

void BoundInnerProduct::SetTimeTracker(
	TimeTracker &time_tracker)
{
	m_p_time_tracker = &time_tracker;
}


void BoundInnerProduct::LargestMulti(const SMatrix<double> &mat_query,
									 Vector<int> &vec_idx)
{
	int num_query = mat_query.Rows();
	SMART_ASSERT(num_query == vec_idx.Size()).Exit();

	if (m_p_time_tracker)
	{
		m_p_time_tracker->Begin(TIME_ID_ALL_QUERIES);
	}

	for (int i = 0; i < num_query; i++)
	{
		const double* query = mat_query[i];
		int &idx = vec_idx[i];
		if (m_p_time_tracker)
		{
			m_p_time_tracker->Begin(TIME_ID_EACH_QUERIES);
		}

		LargestOne(query, idx);

		if (m_p_time_tracker)
		{
			m_p_time_tracker->End();
		}
	}
	if (m_p_time_tracker)
	{
		m_p_time_tracker->End();
	}
}


void BoundInnerProduct::LargestOne(const double* query,
								   int &idx)
{
	SMART_ASSERT(0).Exit();
}

void BoundInnerProduct::PruneIt(
	int idx_point,
	double s, double range, 
	Vector<GreatPair<double> > &heap_upper, 
	Heap<double> &highest_top_lower, 
	double gamma)
{
	double up = s + range;
	double low = s - range;

	if (up <= gamma)
	{
		return;
	}

	if (heap_upper.size() == 0)
	{
		heap_upper.push_back(GreatPair<double>(idx_point, up));
		highest_top_lower.insert(low);
	}
	else if (up <= highest_top_lower.Top())
	{
		return;
	}
	else
	{
		heap_upper.push_back(GreatPair<double>(idx_point, up));
		if (low > highest_top_lower.Top())
		{
			if (highest_top_lower.size() < highest_top_lower.ResearvedSize())
			{
				highest_top_lower.insert(low);
			}
			else
			{
				highest_top_lower.popMin();
				highest_top_lower.insert(low);
			}
		}
	}
}

void BoundInnerProduct::PruneIt(
	int idx_point,
	double s, double range, 
	Vector<GreatPair<double> > &heap_upper, 
	double &largest_lower)
{
	double up = s + range;
	double low = s - range;

	if (heap_upper.size() == 0)
	{
		heap_upper.push_back(GreatPair<double>(idx_point, up));
		largest_lower = low;
	}
	else if (up <= largest_lower)
	{
		return;
	}
	else
	{
		heap_upper.push_back(GreatPair<double>(idx_point, up));
		if (low > largest_lower)
		{
			largest_lower = low;
		}
	}
}

//void BoundInnerProduct::PruneIt(
//	int idx_point,
//	double s, double range, 
//	Heap<LessPair<double> > &heap_upper, 
//	double &largest_lower)
//{
//	double up = s + range;
//	double low = s - range;
//
//	if (heap_upper.size() == 0)
//	{
//		heap_upper.insert(LessPair<double>(idx_point, up));
//		largest_lower = low;
//	}
//	else if (up <= largest_lower)
//	{
//		return;
//	}
//	else
//	{
//		while (heap_upper.size() > 0)
//		{
//			const LessPair<double> &top = heap_upper.Top();
//			if (top.distance < low)
//			{
//				heap_upper.popMin();
//			}
//			else
//			{
//				break;
//			}
//		}
//		heap_upper.insert(LessPair<double>(idx_point, up));
//		if (low > largest_lower)
//		{
//			largest_lower = low;
//		}
//	}
//}

//void BoundInnerProduct::PruneIt(
//	int idx_point,
//	double s, double range, 
//	priority_queue<GreatPair<double> > &heap_upper, 
//	double &largest_lower)
//{
//	double up = s + range;
//	double low = s - range;
//
//	if (heap_upper.size() == 0)
//	{
//		heap_upper.push(GreatPair<double>(idx_point, up));
//		largest_lower = low;
//	}
//	else if (up <= largest_lower)
//	{
//		return;
//	}
//	else
//	{
//		while (heap_upper.size() > 0)
//		{
//			const GreatPair<double> &top = heap_upper.top();
//			if (top.distance < low)
//			{
//				heap_upper.pop();
//			}
//			else
//			{
//				break;
//			}
//		}
//		heap_upper.push(GreatPair<double>(idx_point, up));
//		if (low > largest_lower)
//		{
//			largest_lower = low;
//		}
//	}
//}

void BoundInnerProduct::ConvertHeap2Vector(
	Vector<GreatPair<double> > &heap,
	double &largest_lower)
{
	int num_all = heap.size();

	int idx_real_candidate = 0;

	for (int i = 0; i < num_all; i++)
	{
		GreatPair<double> &gp = heap[i];
		if (gp.distance >= largest_lower)
		{
			heap[idx_real_candidate++] = gp;
		}
	}

	//sort(heap.begin(), heap.begin() + idx_real_candidate);

	heap.truncate_size(idx_real_candidate);

	//SMART_ASSERT(0)(heap).Exit();
}

void BoundInnerProduct::ConvertHeap2Vector(
	Heap<LessPair<double> > &heap, 
	Vector<LessPair<double> > &vec_large_up)
{
	//int heap_size = heap.size();
	//vec_large_up.AllocateSpace(heap_size);
	//for (int i = 0; i < heap_size; i++)
	//{
	//	const LessPair<double> &top = heap.Top();
	//	vec_large_up[heap_size - 1 - i] = top;
	//	heap.popMin();
	//}

	int heap_size = heap.size();
	vec_large_up.AllocateSpace(heap_size);
	for (int i = 0; i < heap_size; i++)
	{
		const LessPair<double>* top = heap.heap + heap_size - i;
		vec_large_up[i] = *top;
	}
}

int BoundInnerProduct::Reranking(const double* query, 
								 Vector<GreatPair<double> > &heap, 
								 double &largest_lower, 
								 int &best_idx,
								 double &best_value)
{
	ConvertHeap2Vector(heap, largest_lower);

	int heap_size = heap.Size();
	best_value = -10000;
	best_idx = -1;
	int dim = m_pmat_database_raw->Cols();

	int num_true_computed = 0;

	int i;
	for (i = 0; i < heap_size; i++)
	{
		const GreatPair<double> &current = heap[i];
		int idx = current.index;
		double upper = current.distance;

		if (upper < best_value)
		{
			continue;
			//break;
		}

		double s = dot(query, m_pmat_database_raw->operator[](idx), dim);

		num_true_computed++;

		if (s > best_value)
		{
			best_value = s;
			best_idx = idx;
		}
	}

	return num_true_computed;
}

int BoundInnerProduct::Reranking(
	const double* query, 
	Vector<GreatPair<double> > &heap, 
	Heap<double> &highest_top_lower, 
	Vector<int> &vec_topk, 
	double gamma)
{
	double largest_lower = highest_top_lower.Top();
	ConvertHeap2Vector(heap, largest_lower);

	int heap_size = heap.Size();
	Heap<LessPair<double> > final_heap;
	int top_num = highest_top_lower.ResearvedSize();
	final_heap.Reserve(top_num);
	
	int dim = m_pmat_database_raw->Cols();

	int num_true_computed = 0;

	int i;
	for (i = 0; i < heap_size; i++)
	{
		const GreatPair<double> &current = heap[i];
		int idx = current.index;
		double upper = current.distance;

		if (final_heap.size() > 0)
		{
			if (final_heap.Top().distance > upper)
			{
				continue;
			}
		}

		double s = dot(query, m_pmat_database_raw->operator[](idx), dim);
		s = s >= 0 ? s : -s;
		num_true_computed++;
		if (s <= gamma)
		{
			continue;
		}
		if (final_heap.size() < top_num)
		{
			final_heap.insert(LessPair<double>(idx, s));
		}
		else
		{
			const LessPair<double> &top = final_heap.Top();
			if (top.distance < s)
			{
				final_heap.popMin();
				final_heap.insert(LessPair<double>(idx, s));
			}
		}
	}

	int final_num = final_heap.size();
	vec_topk.AllocateSpace(final_num);
	for (int i = 0; i < final_num; i++)
	{
		LessPair<double> top;
		final_heap.popMin(top);
		vec_topk[final_num - i - 1] = top.index;
	}

	return num_true_computed;
}

int BoundInnerProduct::Reranking(
	const double* query, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower, 
	int &best_idx)
{
	ConvertHeap2Vector(heap, largest_lower);

	int heap_size = heap.Size();
	double best_value = -10000;
	best_idx = -1;
	int dim = m_pmat_database_raw->Cols();

	int num_true_computed = 0;

	int i;
	for (i = 0; i < heap_size; i++)
	{
		const GreatPair<double> &current = heap[i];
		int idx = current.index;
		double upper = current.distance;

		if (upper < best_value)
		{
			continue;
			//break;
		}

		double s = dot(query, m_pmat_database_raw->operator[](idx), dim);

		num_true_computed++;

		if (s > best_value)
		{
			best_value = s;
			best_idx = idx;
		}
	}

	return num_true_computed;
}

int BoundInnerProduct::Reranking(
	const double* query, 
	Heap<LessPair<double> > &heap, 
	int &best_idx)
{
	Vector<LessPair<double> > vec_large_up;
	ConvertHeap2Vector(heap, vec_large_up);

	int heap_size = vec_large_up.Size();
	double best_value = -10000;
	best_idx = -1;
	int dim = m_pmat_database_raw->Cols();

	int i;
	for (i = 0; i < heap_size; i++)
	{
		const LessPair<double> &current = vec_large_up[i];
		int idx = current.index;
		double upper = current.distance;

		if (upper < best_value)
		{
			continue;
			//break;
		}

		double s = dot(query, m_pmat_database_raw->operator[](idx), dim);

		if (s > best_value)
		{
			best_value = s;
			best_idx = idx;
		}
	}

	return i;
}

//int BoundInnerProduct::Reranking(
//	const double* query, 
//	priority_queue<GreatPair<double> > &heap, 
//	int &best_idx)
//{
//	int heap_size = heap.size();
//
//	Vector<GreatPair<double> > vec_large_up(heap_size);
//	for (int i = 0; i < heap_size; i++)
//	{
//		const GreatPair<double> &top = heap.top();
//		vec_large_up[heap_size - 1 - i] = top;
//		heap.pop();
//	}
//
//	double best_value = -10000;
//	best_idx = -1;
//	int dim = m_pmat_database_raw->Cols();
//
//	int i;
//	for (i = 0; i < heap_size; i++)
//	{
//		const GreatPair<double> &current = vec_large_up[i];
//		int idx = current.index;
//		double upper = current.distance;
//
//		if (upper < best_value)
//		{
//			break;
//		}
//
//		double s = dot(query, m_pmat_database_raw->operator[](idx), dim);
//
//		if (s > best_value)
//		{
//			best_value = s;
//			best_idx = idx;
//		}
//	}
//
//	return i;
//}

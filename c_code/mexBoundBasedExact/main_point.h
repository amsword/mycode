#pragma once

#pragma warning(disable : 4251)


#include "make_mex.h"

#include "utility.h"
#include "LinearScanBound.h"
#include "TimeTracker.h"

#include "TypeConvert.h"


#define TYPE prhs[0]
#define MAT_QUERY prhs[1]
#define MAT_DATABASE prhs[2]
#define DISTANCE_INFO prhs[3]

#define RESULT plhs[0]
#define RESULT_INFO plhs[1]

void EachCalculated(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[]);

void BoundBasedCalculated(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[]);

void MainInvertedListBound(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[]);

void MainInvertedListBoundAbsKNN(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[]);

void MainSingleBallTree(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[]);

void main_exe_linear_scan();
void main_test_tree();
void main_mat();
void main_gamma_top_num();

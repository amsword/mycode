#include "main_point.h"
#include "InvertedListBound.h"
#include "make_mat.h"
#include "BallTree.h"

void main_exe_linear_scan()
{
	const string str_query = "C:\\Users\\t0908482\\Desktop\\mine\\data\\SIFT1M\\OriginXTest.double.bin";
	const string str_database = "C:\\Users\\t0908482\\Desktop\\mine\\data\\SIFT1M\\OriginXBase.double.bin";

	SMatrix<double> mat_query;
	SMatrix<double> mat_database;

	mat_query.LoadData(str_query, 0, 10);
	mat_database.LoadData(str_database);

	Vector<int> vec_nn_idx;

	int num_query = mat_query.Rows();

	Vector<int> vec_perm(mat_database.Rows());
	rand_perm(mat_database.Rows(), vec_perm.Ptr(), mat_database.Rows());


	vec_nn_idx.AllocateSpace(num_query);
	clock_t begin = clock();
	for (int i = 0; i < num_query; i++)
	{
		const double* query = mat_query[i];
		int best_idx = -1;
		double best_value = -10^5;

		for (int j = 0; j < mat_database.Rows(); j++)
		{
			int idx_db = vec_perm[j];
			double s =dot(query, mat_database[idx_db], mat_database.Cols());
			if (s > best_value)
			{
				best_value = s;
				best_idx = idx_db;
			}
		}

		vec_nn_idx[i] = best_idx;
	}
	clock_t end = clock();
	PRINT << (end - begin) / (double)CLOCKS_PER_SEC / (double)num_query << "\n";
	PRINT << vec_nn_idx << "\n";
}

void main_test_tree()
{
	SMatrix<double> mat_query;
	SMatrix<double> mat_database;

	const string str_query = "C:\\Users\\t0908482\\Desktop\\mine\\data\\GIST1M\\OriginXTest.double.bin";
	const string str_database = "C:\\Users\\t0908482\\Desktop\\mine\\data\\GIST1M\\OriginXBase.double.bin";

	mat_query.LoadData(str_query, 0, 10);
	mat_database.LoadData(str_database, 0, 10000);

	Vector<int> vec_num_mip;
	vec_num_mip.reserve(mat_query.Rows());
	Vector<int> vec_num_real_computed;
	vec_num_real_computed.reserve(mat_query.Rows());
	BallTree bt;
	Vector<int> vec_nn(mat_query.Rows());
	bt.SetVecNumberMIP(vec_num_mip);
	bt.SetVecNumberRealComputed(vec_num_real_computed);

	PRINT << "begin\n";

	TimeTracker time_tracker;

	bt.Building(mat_database);

	PRINT << "finished building\n";

	bt.SetTimeTracker(time_tracker);

	clock_t begin = clock();
	bt.LargestMulti(mat_query, vec_nn);
	clock_t end = clock();

	list<int> lst_id;
	list<vector<double> > lst_vec_time;
	time_tracker.Report(lst_id, lst_vec_time);

	PRINT << "begin\n";
	PRINT << lst_id.size() << "\n";

	for (list<int>::iterator iter = lst_id.begin(); iter != lst_id.end(); iter++)
	{
		cout << *iter << "\t";
	}

	cout << "\n";
	for (list<vector<double> >::iterator iter = lst_vec_time.begin(); iter != lst_vec_time.end(); iter++)
	{
		for (vector<double>::iterator i2 = iter->begin(); i2 != iter->end(); i2++)
		{
			cout << *i2 << "\t";
		}
		cout << "\n";
	}


	//SMART_ASSERT(0)(bt.m_nNumberRealComputed)(vec_num_real_computed.SumUp());

	SMART_ASSERT(0)((end - begin) / (double)CLOCKS_PER_SEC / mat_query.Rows());
	SMART_ASSERT(0)(Vector<int>(vec_nn.Ptr(), 10));
	SMART_ASSERT(0)(Vector<int>(vec_num_mip.Ptr(), 10));
}

void main_gamma_top_num()
{
	SMatrix<double> mat_query;
	SMatrix<double> mat_database_raw;
	SMatrix<double> mat_coarse_centers;
	Vector<Vector<int> > vecvec_inverted_idx;
	Vector<double> vec_bound_center;

	Vector<SMatrix<short> > vecmat_all_ock_codes;
	Vector<Vector<double> > vecvec_all_ock_bound;
	Vector<int> vec_nn_idx;
	// global ock-means
	int is_optimize_R;
	SMatrix<double> matRotation;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;

	{
		mxArray* p;
		string str_file_name = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\debug_data2.mat";

		MATFile* fp = matOpen(str_file_name.c_str(), "r");

		//mexConvert(matGetVariable(fp, "query"), mat_query);
		mexConvert(matGetVariable(fp, "database"), mat_database_raw);
		mxArray* distance_info = matGetVariable(fp, "distance_info");

		mxArray* p_coarse_model = mxGetField(distance_info, 0, "coarse_model");
		mexConvert(mxGetField(p_coarse_model, 0, "coarse_centers"), mat_coarse_centers);
		mexConvert(mxGetField(p_coarse_model, 0, "all_inverted_idx"), vecvec_inverted_idx);
		mexConvert(mxGetField(p_coarse_model, 0, "bound_center"), vec_bound_center);
		mxArray* p_fine_model = mxGetField(distance_info, 0, "fine_model");
		mexConvert(mxGetField(p_fine_model, 0, "all_ock_codes"), vecmat_all_ock_codes);
		mexConvert(mxGetField(p_fine_model, 0, "all_ock_bound"), vecvec_all_ock_bound);
		mxArray* p_ock_means_model = mxGetField(p_fine_model, 0, "global_ock_model");
		mexConvert(mxGetField(p_ock_means_model, 0, "is_optimize_R"), is_optimize_R);
		mexConvert(mxGetField(p_ock_means_model, 0, "R"), matRotation);
		mexConvert(mxGetField(p_ock_means_model, 0, "all_D"), vecmatCenters);
		mexConvert(mxGetField(p_ock_means_model, 0, "num_sub_dic_each_partition"), num_dic_each_partition);
		SMART_ASSERT(num_dic_each_partition == 1)(num_dic_each_partition).Exit();
		matClose(fp);

		str_file_name = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\query_tol0.0001_1_100.mat";
		fp = matOpen(str_file_name.c_str(), "r");
		mexConvert(matGetVariable(fp, "all_query"), mat_query);
		matClose(fp);

	}

	mwSize tmp_size[1]; 
	tmp_size[0] = mat_query.Rows();	
	mxArray* result = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(result, vec_nn_idx);

	InnerDistance dist_calculator;
	dist_calculator.Initialize(is_optimize_R, matRotation, vecmatCenters, num_dic_each_partition);

	TimeTracker time_tracker;

	InvertedListBound bounder;

	bounder.Initialize(mat_database_raw, 
		mat_coarse_centers, vec_bound_center, vecvec_inverted_idx, 
		&dist_calculator, vecmat_all_ock_codes, vecvec_all_ock_bound);



	bounder.SetTimeTracker(time_tracker);
	vector<int> vec_num_visited_inverted_list;
	vector<int> vec_heap_size;
	vector<int> vec_num_true_inner_compute;
	bounder.SetInsighters(vec_num_visited_inverted_list, vec_heap_size, vec_num_true_inner_compute);

	double gamma = 0.1;
	int top_num = 150;

	clock_t begin, end;

	//bounder.CheckBound();

	begin = clock();

	for (int i = 0; i < mat_query.Rows(); i++)
	{
		bounder.LargestAbsK(mat_query[i], vec_nn_idx, gamma, top_num);
	}
	end = clock();
	PRINT << (end - begin) / (double)CLOCKS_PER_SEC << "\n";

	//int idx;
	//begin = clock();
	//bounder.LargestOne(mat_query[0], idx);
	//end = clock();
	//PRINT << (end - begin) / (double)CLOCKS_PER_SEC << "\n";

	SMART_ASSERT(0)(vec_nn_idx).Exist0();

	string str_result = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\out.int32";

	double stmp = dot(mat_database_raw[vec_nn_idx[149]], mat_query[0], mat_query.Cols()); 
	vec_nn_idx.SaveData(str_result);

	list<int> lst_id;
	list<vector<double> > lst_vec_time_cost_second;
	time_tracker.Report(lst_id, lst_vec_time_cost_second);

	int num_kind = lst_id.size();
	mxArray* result_info = mxCreateCellMatrix(num_kind + 3, 1);

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time_cost_second.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}
		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(result_info, i, p);
	}

	{
		//SMART_ASSERT(0)(vec_heap_size.size())(vec_num_visited_inverted_list.size())
		//	(vec_num_true_inner_compute.size());


		mxArray* p = mxCreateDoubleMatrix(vec_heap_size.size(), 1, mxREAL);
		Vector<double> vec_tmp1;
		mexConvert(p, vec_tmp1);
		for (int i = 0; i < vec_heap_size.size(); i++)
		{
			vec_tmp1[i] = vec_heap_size[i];
		}
		mxSetCell(result_info, num_kind, p);

		p = mxCreateDoubleMatrix(vec_num_visited_inverted_list.size(), 1, mxREAL);
		Vector<double> vec_tmp2;
		mexConvert(p, vec_tmp2);
		for (int i = 0; i < vec_num_visited_inverted_list.size(); i++)
		{
			vec_tmp2[i] = vec_num_visited_inverted_list[i];
		}
		mxSetCell(result_info, num_kind + 1, p);

		p = mxCreateDoubleMatrix(vec_num_true_inner_compute.size(), 1, mxREAL);
		Vector<double> vec_tmp3;
		mexConvert(p, vec_tmp3);
		for (int i = 0; i < vec_num_true_inner_compute.size(); i++)
		{
			vec_tmp3[i] = vec_num_true_inner_compute[i];
		}
		mxSetCell(result_info, num_kind + 2, p);

	}
}

void main_mat()
{
	SMatrix<double> mat_query;
	SMatrix<double> mat_database_raw;
	SMatrix<double> mat_coarse_centers;
	Vector<Vector<int> > vecvec_inverted_idx;
	Vector<double> vec_bound_center;

	Vector<SMatrix<short> > vecmat_all_ock_codes;
	Vector<Vector<double> > vecvec_all_ock_bound;
	Vector<int> vec_nn_idx;
	// global ock-means
	int is_optimize_R;
	SMatrix<double> matRotation;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;

	{
		mxArray* p;
		string str_file_name = "C:\\Users\\t0908482\\Desktop\\mine\\working\\SIFT1M\\debug_data.mat";

		MATFile* fp = matOpen(str_file_name.c_str(), "r");

		mexConvert(matGetVariable(fp, "query"), mat_query);
		mexConvert(matGetVariable(fp, "database"), mat_database_raw);
		mxArray* distance_info = matGetVariable(fp, "distance_info");

		mxArray* p_coarse_model = mxGetField(distance_info, 0, "coarse_model");
		mexConvert(mxGetField(p_coarse_model, 0, "coarse_centers"), mat_coarse_centers);
		mexConvert(mxGetField(p_coarse_model, 0, "all_inverted_idx"), vecvec_inverted_idx);
		mexConvert(mxGetField(p_coarse_model, 0, "bound_center"), vec_bound_center);
		mxArray* p_fine_model = mxGetField(distance_info, 0, "fine_model");
		mexConvert(mxGetField(p_fine_model, 0, "all_ock_codes"), vecmat_all_ock_codes);
		mexConvert(mxGetField(p_fine_model, 0, "all_ock_bound"), vecvec_all_ock_bound);
		mxArray* p_ock_means_model = mxGetField(p_fine_model, 0, "global_ock_model");
		mexConvert(mxGetField(p_ock_means_model, 0, "is_optimize_R"), is_optimize_R);
		mexConvert(mxGetField(p_ock_means_model, 0, "R"), matRotation);
		mexConvert(mxGetField(p_ock_means_model, 0, "all_D"), vecmatCenters);
		mexConvert(mxGetField(p_ock_means_model, 0, "num_sub_dic_each_partition"), num_dic_each_partition);
		SMART_ASSERT(num_dic_each_partition == 1)(num_dic_each_partition).Exit();

	}

	mwSize tmp_size[1]; 
	tmp_size[0] = mat_query.Rows();	
	mxArray* result = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(result, vec_nn_idx);

	InnerDistance dist_calculator;
	dist_calculator.Initialize(is_optimize_R, matRotation, vecmatCenters, num_dic_each_partition);

	TimeTracker time_tracker;

	InvertedListBound bounder;

	bounder.Initialize(mat_database_raw, 
		mat_coarse_centers, vec_bound_center, vecvec_inverted_idx, 
		&dist_calculator, vecmat_all_ock_codes, vecvec_all_ock_bound);
	bounder.SetTimeTracker(time_tracker);
	vector<int> vec_num_visited_inverted_list;
	vector<int> vec_heap_size;
	vector<int> vec_num_true_inner_compute;
	bounder.SetInsighters(vec_num_visited_inverted_list, vec_heap_size, vec_num_true_inner_compute);

	bounder.LargestMulti(mat_query, vec_nn_idx);

	list<int> lst_id;
	list<vector<double> > lst_vec_time_cost_second;
	time_tracker.Report(lst_id, lst_vec_time_cost_second);

	int num_kind = lst_id.size();
	mxArray* result_info = mxCreateCellMatrix(num_kind + 3, 1);

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time_cost_second.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}
		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(result_info, i, p);
	}

	{
		//SMART_ASSERT(0)(vec_heap_size.size())(vec_num_visited_inverted_list.size())
		//	(vec_num_true_inner_compute.size());


		mxArray* p = mxCreateDoubleMatrix(vec_heap_size.size(), 1, mxREAL);
		Vector<double> vec_tmp1;
		mexConvert(p, vec_tmp1);
		for (int i = 0; i < vec_heap_size.size(); i++)
		{
			vec_tmp1[i] = vec_heap_size[i];
		}
		mxSetCell(result_info, num_kind, p);

		p = mxCreateDoubleMatrix(vec_num_visited_inverted_list.size(), 1, mxREAL);
		Vector<double> vec_tmp2;
		mexConvert(p, vec_tmp2);
		for (int i = 0; i < vec_num_visited_inverted_list.size(); i++)
		{
			vec_tmp2[i] = vec_num_visited_inverted_list[i];
		}
		mxSetCell(result_info, num_kind + 1, p);

		p = mxCreateDoubleMatrix(vec_num_true_inner_compute.size(), 1, mxREAL);
		Vector<double> vec_tmp3;
		mexConvert(p, vec_tmp3);
		for (int i = 0; i < vec_num_true_inner_compute.size(); i++)
		{
			vec_tmp3[i] = vec_num_true_inner_compute[i];
		}
		mxSetCell(result_info, num_kind + 2, p);

	}
}
#pragma once

#include "utility.h"
#include "BoundInnerProduct.h"

using namespace utility;

struct Node
{
	const Vector<int>* pvec_idx;
	const Vector<double>* pvec_center;
	double radius;

	Node* left;
	Node* right;
};


class BallTree : public BoundInnerProduct
{
public:
	BallTree();
	~BallTree();

public:
	int MaxDepth();

	void SetMaximumPointsLeaf(int nMaxPointsLeaf);

	void SetVecNumberRealComputed(Vector<int> &vec_num_real_computed);
	void SetVecNumberMIP(Vector<int> &vec_num_mip);

	void SetIsApproximate(int is_approximate);
	void SetAppPara(int approximate_num_leaf);
	void Building(const SMatrix<double> &mat_database);

	void LargestOne(const double* query, int &idx);

private:
	int MaxDepth(const Node* p);

private:
	void ReleaseTree(Node* &root);

	double MIP(const double* query, const Node* root);

	int TreeSearch(const double* query, 
		int &idx, double &maximum_inner, const Node* root, 
		double upper_bound,
		int &num_mip);

	int LinearSearch(const double* query, 
		const Vector<int>* pvec_idx, 
		int &idx, 
		double &maximum_inner);

	void MakeBallTree(const Vector<int>* pvec_idx, 
		Node* &proot, int level);

	void ComputeMean(const Vector<int>* pvec_idx, 
		Vector<double>* &p_center);

	void ComputeRadius(const Vector<int>* pvec_idx, 
		const double* p_center, 
		double &radius, 
		int &idx);

	void ComputeRadius1(const Vector<int>* pvec_idx, 
		const double* p_center, 
		double &radius, 
		int &idx);

	void MakeBallTreeSplit(const Vector<int>* pvec_idx, 
		int &idxA, 
		int &idxB);

	void SplitIndex(const Vector<int>* pvec_idx, int idxA, int idxB, 
		Vector<int>* &pvec_idx_left, Vector<int>* &pvec_idx_right);

	void SplitIndexToIndicator(const Vector<int>* pvec_idx, 
		const double* p_left_anchor, 
		const double* p_right_anchor, 
		Vector<int> &vec_indicator);

private:
	Node* m_proot;

	Vector<int>* m_pvecNumberRealComputed;
	Vector<int>* m_pvecNumberMIP;

	int m_nMaxPointsLeaf;

	int m_is_approximate;
	int m_num_max_real_computed;
public:
	//int m_nNumberRealComputed;
};

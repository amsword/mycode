
#include "LinearScanBound.h"



void LinearScanBound::Initialize(
	const SMatrix<double> &mat_database_raw, 
	const SMatrix<short> &mat_database_code, 
	InnerDistance &distance_calculator, 
	Vector<double> &vec_bound)
{
	m_pmat_database_raw = &mat_database_raw;
	m_pmat_database_code = &mat_database_code;
	m_pdistance_calculator = &distance_calculator;
	m_pvec_bound = &vec_bound;
}


void LinearScanBound::LargestOne(
	const double* query,
	int &idx)
{
	double highest_lower;
	Heap<LessPair<double> > heap;
	heap.Reserve(m_pmat_database_raw->Rows());

	//priority_queue<GreatPair<double> > heap;

	m_p_time_tracker->Begin(1);
	BoundProne(query, heap);
	m_p_time_tracker->End();

	m_p_time_tracker->Begin(2);
	Reranking(query, heap, idx);
	m_p_time_tracker->End();
}

void LinearScanBound::BoundProne(const double* query, 
								 Heap<LessPair<double> > &heap_upper)
{
	int num_data_base_point = m_pmat_database_code->Rows();

	InterPara* p_pre_out;
	m_pdistance_calculator->PreProcessing(query, p_pre_out);

	double largest_lower;
	for (int i = 0; i < num_data_base_point; i++)
	{

		const short* data_code = m_pmat_database_code->operator[](i);

		double s = m_pdistance_calculator->DistancePre(p_pre_out, data_code);
		double range = m_pvec_bound->operator[](i);

		double up = s + range;
		double low = s - range;

		//SMART_ASSERT(0)(i)(s)(range)(heap_upper.size());
		//SMART_ASSERT(i < 100).Exit();


		if (heap_upper.size() == 0)
		{
			heap_upper.insert(LessPair<double>(i, up));
			largest_lower = low;
		}
		else if (up <= largest_lower)
		{
			continue;
		}
		else
		{
			while (heap_upper.size() > 0)
			{
				const LessPair<double> &top = heap_upper.Top();
				if (top.distance < low)
				{
					heap_upper.popMin();
				}
				else
				{
					break;
				}
			}
			heap_upper.insert(LessPair<double>(i, up));
			if (low > largest_lower)
			{
				largest_lower = low;
			}
		}
	}

	m_pdistance_calculator->PostProcessing(p_pre_out);
}

void LinearScanBound::BoundProne(
	const double* query, 
	priority_queue<GreatPair<double> > &heap_upper)
{
	int num_data_base_point = m_pmat_database_code->Rows();

	InterPara* p_pre_out;
	m_pdistance_calculator->PreProcessing(query, p_pre_out);

	double largest_lower;
	for (int i = 0; i < num_data_base_point; i++)
	{

		const short* data_code = m_pmat_database_code->operator[](i);

		double s = m_pdistance_calculator->DistancePre(p_pre_out, data_code);
		double range = m_pvec_bound->operator[](i);

		double up = s + range;
		double low = s - range;

		//SMART_ASSERT(0)(i)(s)(range)(heap_upper.size());
		//SMART_ASSERT(i < 100).Exit();


		if (heap_upper.size() == 0)
		{
			heap_upper.push(GreatPair<double>(i, up));
			largest_lower = low;
		}
		else if (up <= largest_lower)
		{
			continue;
		}
		else
		{
			while (heap_upper.size() > 0)
			{
				const GreatPair<double> &top = heap_upper.top();
				if (top.distance < low)
				{
					heap_upper.pop();
				}
				else
				{
					break;
				}
			}
			heap_upper.push(GreatPair<double>(i, up));
			if (low > largest_lower)
			{
				largest_lower = low;
			}
		}


	}

	m_pdistance_calculator->PostProcessing(p_pre_out);
} 



#include "InvertedListBound.h"


void InvertedListBound::ScanInvertedList(
	InterPara* query_inter, 
	const SMatrix<short> &mat_ock_codes, 
	const Vector<int> &vec_inverted_idx, 
	const Vector<double> &vec_bound,
	double offset,
	Vector<GreatPair<double> > &heap, 
	Heap<double> &highest_top_lower, 
	double gamma)
{
	int num_point = vec_inverted_idx.Size();

	for (int i = 0; i < num_point;  i++)
	{
		int idx_point = vec_inverted_idx[i];
		const short* code = mat_ock_codes[i];
		double app = m_pdistance_calculator->DistancePre(query_inter, code);
		app += offset;

		app = app >= 0 ? app : -app;

		double range = vec_bound[i];
		PruneIt(idx_point, app, range, heap, highest_top_lower, gamma);
	}
}

void InvertedListBound::ScanInvertedList(
	InterPara* query_inter, 
	const SMatrix<short> &mat_ock_codes, 
	const Vector<int> &vec_inverted_idx, 
	const Vector<double> &vec_bound,
	double offset,
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_point = vec_inverted_idx.Size();

	for (int i = 0; i < num_point;  i++)
	{
		int idx_point = vec_inverted_idx[i];
		const short* code = mat_ock_codes[i];
		double app = m_pdistance_calculator->DistancePre(query_inter, code);
		app += offset;

		double range = vec_bound[i];
		PruneIt(idx_point, app, range, heap, largest_lower);
	}
}

//void InvertedListBound::ScanInvertedList(
//	InterPara* query_inter, 
//	const SMatrix<short> &mat_ock_codes, 
//	const Vector<int> &vec_inverted_idx, 
//	const Vector<double> &vec_bound,
//	double offset,
//	Heap<LessPair<double> > &heap, 
//	double &largest_lower)
//{
//	int num_point = vec_inverted_idx.Size();
//
//	for (int i = 0; i < num_point;  i++)
//	{
//		int idx_point = vec_inverted_idx[i];
//		const short* code = mat_ock_codes[i];
//		double app = m_pdistance_calculator->DistancePre(query_inter, code);
//		app += offset;
//
//		double range = vec_bound[i];
//		PruneIt(idx_point, app, range, heap, largest_lower);
//	}
//}

//void InvertedListBound::ScanInvertedList(
//	InterPara* query_inter, 
//	const SMatrix<short> &mat_ock_codes, 
//	const Vector<int> &vec_inverted_idx, 
//	const Vector<double> &vec_bound,
//	double offset,
//	priority_queue<GreatPair<double> > &heap, 
//	double &largest_lower)
//{
//	int num_point = vec_inverted_idx.Size();
//
//	for (int i = 0; i < num_point;  i++)
//	{
//		int idx_point = vec_inverted_idx[i];
//		const short* code = mat_ock_codes[i];
//		double app = m_pdistance_calculator->DistancePre(query_inter, code);
//		app += offset;
//
//		double range = vec_bound[i];
//		PruneIt(idx_point, app, range, heap, largest_lower);
//	}
//}

void InvertedListBound::ScanInvertedList(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<Triplet<int, double, double> > &vec_sorted_upper_bound, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int num_scanned_inverted_list = 0;
	for (int i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_upper_bound[i].first;
		double upper_coarse = vec_sorted_upper_bound[i].third;

		if (largest_lower >= upper_coarse)
		{
			continue;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
		num_scanned_inverted_list++;
	}
	{
		m_pvec_num_visited_inverted_list->push_back(num_scanned_inverted_list);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

void InvertedListBound::ScanInvertedListIterative(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int i;
	for (i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_upper_bound[i].index;
		double upper_coarse = vec_sorted_upper_bound[i].distance;

		if (largest_lower >= upper_coarse)
		{
			break;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);

		if (i > 0 && (i % 100) == 0)
		{
			double best_value;
			int best_idx;
			Reranking(query, heap, largest_lower, best_idx, best_value);
			heap.clear();
			heap.push_back(GreatPair<double>(best_idx, best_value));
			largest_lower = best_value;
		}
	}
	{
		m_pvec_num_visited_inverted_list->push_back(i);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

void InvertedListBound::ScanInvertedListAppExpecation(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<GreatPair<double> > &vec_sorted_expectation, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	if (m_max_num_inverted_list < num_coarse_clusters)
	{
		num_coarse_clusters = m_max_num_inverted_list;
	}

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int i;
	for (i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_expectation[i].index;
		double upper_coarse = vec_sorted_expectation[i].distance + 
			m_pvec_coarse_bound_cluster->operator[](idx_coarse_cluster);

		if (largest_lower >= upper_coarse)
		{
			continue;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
	}
	{
		m_pvec_num_visited_inverted_list->push_back(i);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

void InvertedListBound::ScanInvertedListApp(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	if (m_max_num_inverted_list < num_coarse_clusters)
	{
		num_coarse_clusters = m_max_num_inverted_list;
	}

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int i;
	for (i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_upper_bound[i].index;
		double upper_coarse = vec_sorted_upper_bound[i].distance;

		if (largest_lower >= upper_coarse)
		{
			continue;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
	}
	{
		m_pvec_num_visited_inverted_list->push_back(i);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

void InvertedListBound::ScanInvertedListAbsTop(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<GreatPair<double> > &vec_sorted_abs_upper_bound, 
	Vector<GreatPair<double> > &heap, 
	Heap<double> &highest_top_lower, 
	double gamma)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int i;
	for (i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_abs_upper_bound[i].index;
		double upper_abs_coarse = vec_sorted_abs_upper_bound[i].distance;

		if (upper_abs_coarse <= gamma)
		{
			break;
		}

		double largest_lower = highest_top_lower.size() > 0 ? highest_top_lower.Top() : -10000;
		if (largest_lower >= upper_abs_coarse)
		{
			break;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(
			query_inter, 
			mat_ock_codes, 
			vec_inverted_idx, 
			vec_bound, 
			inner_coarse, 
			heap, 
			highest_top_lower, gamma);
	}
	{
		m_pvec_num_visited_inverted_list->push_back(i);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

void InvertedListBound::ScanInvertedList(
	const double* query, 
	const Vector<double> &vec_inner_to_coarse_center, 
	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
	Vector<GreatPair<double> > &heap, 
	double &largest_lower)
{
	int num_coarse_clusters = vec_inner_to_coarse_center.Size();

	InterPara* query_inter;
	m_pdistance_calculator->PreProcessing(query, query_inter);

	int i;
	for (i = 0; i < num_coarse_clusters; i++)
	{
		int idx_coarse_cluster = vec_sorted_upper_bound[i].index;
		double upper_coarse = vec_sorted_upper_bound[i].distance;

		if (largest_lower >= upper_coarse)
		{
			break;
		}

		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];

		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);

		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
	}
	{
		m_pvec_num_visited_inverted_list->push_back(i);
	}
	m_pdistance_calculator->PostProcessing(query_inter);
}

//void InvertedListBound::ScanInvertedList(
//	const double* query, 
//	const Vector<double> &vec_inner_to_coarse_center, 
//	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
//	Heap<LessPair<double> > &heap)
//{
//	int num_coarse_clusters = vec_inner_to_coarse_center.Size();
//
//	InterPara* query_inter;
//	m_pdistance_calculator->PreProcessing(query, query_inter);
//
//	double largest_lower = -100000;
//
//	int i;
//	for (i = 0; i < num_coarse_clusters; i++)
//	{
//		int idx_coarse_cluster = vec_sorted_upper_bound[i].index;
//		int upper_coarse = vec_sorted_upper_bound[i].distance;
//
//		if (largest_lower >= upper_coarse)
//		{
//			break;
//		}
//
//		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];
//
//		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
//		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
//		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);
//
//		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
//	}
//	{
//		m_pvec_num_visited_inverted_list->push_back(i);
//	}
//	m_pdistance_calculator->PostProcessing(query_inter);
//}

//void InvertedListBound::ScanInvertedList(
//	const double* query, 
//	const Vector<double> &vec_inner_to_coarse_center, 
//	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
//	priority_queue<GreatPair<double> > &heap)
//{
//	int num_coarse_clusters = vec_inner_to_coarse_center.Size();
//
//	InterPara* query_inter;
//	m_pdistance_calculator->PreProcessing(query, query_inter);
//
//	double largest_lower = -100000;
//
//	int i;
//	for (i = 0; i < num_coarse_clusters; i++)
//	{
//		int idx_coarse_cluster = vec_sorted_upper_bound[i].index;
//		int upper_coarse = vec_sorted_upper_bound[i].distance;
//
//		if (largest_lower >= upper_coarse)
//		{
//			break;
//		}
//
//		double inner_coarse = vec_inner_to_coarse_center[idx_coarse_cluster];
//
//		const SMatrix<short> &mat_ock_codes = m_pvecmat_ock_codes->operator[](idx_coarse_cluster);
//		const Vector<int> &vec_inverted_idx = m_pvecvec_inverted_idx->operator[](idx_coarse_cluster);
//		const Vector<double> &vec_bound = m_pvecvec_bound->operator[](idx_coarse_cluster);
//
//		ScanInvertedList(query_inter, mat_ock_codes, vec_inverted_idx, vec_bound, inner_coarse, heap, largest_lower);
//	}
//	{
//		m_pvec_num_visited_inverted_list->push_back(i);
//	}
//	m_pdistance_calculator->PostProcessing(query_inter);
//}


void InvertedListBound::CheckBound()
{
	SMART_ASSERT(0).Exist0();
}

void InvertedListBound::SecureCheck()
{
	int num_toal_point = 0;
	for (int i = 0; i < m_pvecvec_inverted_idx->Size(); i++)
	{
		num_toal_point += m_pvecvec_inverted_idx->operator[](i).Size();
	}
	int num_cluster = m_pvec_coarse_bound_cluster->Size();

	SMART_ASSERT(num_toal_point > 0 && m_dim > 0 &&
		num_cluster > 0)(num_toal_point)(m_dim)(num_cluster).Exit();

	SMART_ASSERT(m_pmat_database_raw->Rows() == num_toal_point && m_pmat_database_raw->Cols() == m_dim &&
		m_pmat_coarse_clusters->Rows() == num_cluster && m_pmat_coarse_clusters->Cols() == m_dim).Exit();

	SMART_ASSERT(m_pvec_coarse_bound_cluster->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecmat_ock_codes->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecvec_bound->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecvec_inverted_idx->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecmat_ock_codes->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecvec_bound->Size() == num_cluster).Exit();
	SMART_ASSERT(m_pvecvec_inverted_idx->Size() == num_cluster).Exit();

	for (int i = 0; i < num_cluster; i++)
	{
		SMART_ASSERT(m_pvec_coarse_bound_cluster->operator[](i) >= 0).Exit();
	}

}

void InvertedListBound::SetAppPara(int max_num_inverted_list)
{
	m_max_num_inverted_list = max_num_inverted_list;
}

void InvertedListBound::SetIsApproximate(int is_approximate)
{
	m_is_approximate = is_approximate;
}

InvertedListBound::InvertedListBound() : m_is_approximate(false)
{

}

void InvertedListBound::Initialize(
	SMatrix<double> &mat_raw_database,
	SMatrix<double> &mat_coarse_clusters,
	const Vector<double> &vec_bound_center,
	const Vector<Vector<int> > &vecvec_inverted_idx, 
	InnerDistance* dist_calculator, 
	const Vector<SMatrix<short> > &vecmat_all_ock_codes, 
	const Vector<Vector<double> > &vecvec_all_ock_bound)
{
	m_pmat_database_raw = &mat_raw_database;

	m_pmat_coarse_clusters = &mat_coarse_clusters;
	m_pvec_coarse_bound_cluster = &vec_bound_center;
	m_pvecvec_inverted_idx = &vecvec_inverted_idx;

	m_pdistance_calculator = dist_calculator;
	m_pvecmat_ock_codes = &vecmat_all_ock_codes;

	m_pvecvec_bound = &vecvec_all_ock_bound;

	m_dim = m_pmat_coarse_clusters->Cols();

	SecureCheck();
}

void InvertedListBound::SetInsighters(vector<int> &vec_num_visited_inverted_list,
									  vector<int> &vec_heap_size,
									  vector<int> &vec_num_true_inner_compute)
{
	m_pvec_num_visited_inverted_list = &vec_num_visited_inverted_list;
	m_pvec_heap_size = &vec_heap_size;
	m_pvec_num_true_inner_compute = &vec_num_true_inner_compute;


	m_pvec_num_visited_inverted_list->clear();
	m_pvec_heap_size->clear();
	m_pvec_num_true_inner_compute->clear();
}

void InvertedListBound::LargestOne_general(const double* query,
										   int &idx)
{
	double s = squared_L2_norm(query, m_dim);
	s = sqrt(s);
	Vector<double> vec_normalized_query(m_dim);

	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), m_dim);

	double highest_lower = -100000;
	//priority_queue<GreatPair<double> > heap;
	Vector<GreatPair<double> > vec_candidate;
	vec_candidate.reserve(m_pmat_database_raw->Rows());

	m_p_time_tracker->Begin(1);
	PruningTriplet(vec_normalized_query.Ptr(), vec_candidate, highest_lower);
	m_p_time_tracker->End();

	m_p_time_tracker->Begin(2);
	int num_calculated = Reranking(vec_normalized_query.Ptr(), vec_candidate, highest_lower, idx);
	m_p_time_tracker->End();

	m_pvec_num_true_inner_compute->push_back(num_calculated);
}


void InvertedListBound::LargestOneIterative(const double* query,
											int &idx)
{
	double s = squared_L2_norm(query, m_dim);
	s = sqrt(s);
	Vector<double> vec_normalized_query(m_dim);

	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), m_dim);

	double highest_lower = -100000;

	Vector<GreatPair<double> > vec_candidate;
	vec_candidate.reserve(m_pmat_database_raw->Rows());

	m_p_time_tracker->Begin(1);
	Pruning(vec_normalized_query.Ptr(), vec_candidate, highest_lower);
	m_p_time_tracker->End();

	Reranking(vec_normalized_query.Ptr(), vec_candidate, highest_lower, idx);


}


void InvertedListBound::LargestOne(const double* query,
								   int &idx)
{
	//LargestOneIterative(query, idx);
	LargestOne_good(query, idx);
}

void InvertedListBound::LargestAbsK(
	const double* query, 
	Vector<int> &vec_topk, 
	double gamma, 
	int top_num)
{
	double s = squared_L2_norm(query, m_dim);
	s = sqrt(s);
	Vector<double> vec_normalized_query(m_dim);

	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), m_dim);

	gamma /= s;

	Heap<double> highest_top_lower;
	highest_top_lower.Reserve(top_num);

	//priority_queue<GreatPair<double> > heap;
	Vector<GreatPair<double> > vec_candidate;
	vec_candidate.reserve(m_pmat_database_raw->Rows());

	m_p_time_tracker->Begin(1);
	Pruning(vec_normalized_query.Ptr(), vec_candidate, highest_top_lower, gamma);
	m_p_time_tracker->End();

	m_p_time_tracker->Begin(2);
	int num_calculated = Reranking(
		vec_normalized_query.Ptr(), 
		vec_candidate, 
		highest_top_lower, 
		vec_topk, 
		gamma);
	m_p_time_tracker->End();

	m_pvec_num_true_inner_compute->push_back(num_calculated);
}

void InvertedListBound::LargestOne_good(const double* query,
										int &idx)
{
	double s = squared_L2_norm(query, m_dim);
	s = sqrt(s);
	Vector<double> vec_normalized_query(m_dim);

	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), m_dim);

	double highest_lower = -100000;
	//priority_queue<GreatPair<double> > heap;
	Vector<GreatPair<double> > vec_candidate;
	vec_candidate.reserve(m_pmat_database_raw->Rows());

	m_p_time_tracker->Begin(1);
	Pruning(vec_normalized_query.Ptr(), vec_candidate, highest_lower);
	m_p_time_tracker->End();

	m_p_time_tracker->Begin(2);
	int num_calculated = Reranking(vec_normalized_query.Ptr(), vec_candidate, highest_lower, idx);
	m_p_time_tracker->End();

	m_pvec_num_true_inner_compute->push_back(num_calculated);
}

//void InvertedListBound::LargestOne3(
//	const double* query,
//	int &idx)
//{
//	double s = squared_L2_norm(query, m_dim);
//	s = sqrt(s);
//	Vector<double> vec_normalized_query(m_dim);
//
//	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), m_dim);
//
//	double highest_lower;
//	//priority_queue<GreatPair<double> > heap;
//	Heap<LessPair<double> > heap;
//	heap.Reserve(m_pmat_database_raw->Rows());
//
//	m_p_time_tracker->Begin(1);
//	Pruning(vec_normalized_query.Ptr(), heap);
//	m_p_time_tracker->End();
//
//	m_p_time_tracker->Begin(2);
//	int num_calculated = Reranking(vec_normalized_query.Ptr(), heap, idx);
//	m_p_time_tracker->End();
//
//	m_pvec_num_true_inner_compute->push_back(num_calculated);
//}

//void InvertedListBound::LargestOne_2(
//	const double* query,
//	int &idx)
//{
//	double highest_lower;
//	//priority_queue<GreatPair<double> > heap;
//	Heap<LessPair<double> > heap;
//
//	heap.Reserve(m_pmat_database_raw->Rows());
//
//	m_p_time_tracker->Begin(1);
//	Pruning(query, heap);
//	m_p_time_tracker->End();
//
//	m_pvec_heap_size->push_back(heap.size());
//
//	m_p_time_tracker->Begin(2);
//	int num_calculated = Reranking(query, heap, idx);
//	m_p_time_tracker->End();
//
//	m_pvec_num_true_inner_compute->push_back(num_calculated);
//}


//void InvertedListBound::LargestOne_1(
//	const double* query,
//	int &idx)
//{
//	double highest_lower;
//	priority_queue<GreatPair<double> > heap;
//
//	m_p_time_tracker->Begin(1);
//	Pruning(query, heap);
//	m_p_time_tracker->End();
//
//	m_pvec_heap_size->push_back(heap.size());
//
//	m_p_time_tracker->Begin(2);
//	int num_calculated = Reranking(query, heap, idx);
//	m_p_time_tracker->End();
//
//	m_pvec_num_true_inner_compute->push_back(num_calculated);
//}

void InvertedListBound::SortInvertedList(const Vector<double> &vec_inner_to_coarse_center, 
										 Vector<Triplet<int, double, double> > &vec_sorted_upper_bound)
{
	int num_coarse_cluster = vec_inner_to_coarse_center.Size();
	vec_sorted_upper_bound.AllocateSpace(num_coarse_cluster);
	for (int i = 0; i < num_coarse_cluster; i++)
	{
		double app = vec_inner_to_coarse_center[i];
		double range = m_pvec_coarse_bound_cluster->operator[](i);
		vec_sorted_upper_bound[i] = Triplet<int, double, double>(i, app, app + range);
	}
	Triplet<int, double, double>* begin = vec_sorted_upper_bound.Ptr();
	Triplet<int, double, double>* end = begin + num_coarse_cluster;

	sort(begin, end, GreatTripletSecond<int, double, double>);
}

void InvertedListBound::SortByExpectation(const Vector<double> &vec_inner_to_coarse_center, 
										  Vector<GreatPair<double> > &vec_sorted_expectation)
{
	int num_coarse_cluster = vec_inner_to_coarse_center.Size();
	vec_sorted_expectation.AllocateSpace(num_coarse_cluster);
	for (int i = 0; i < num_coarse_cluster; i++)
	{
		double app = vec_inner_to_coarse_center[i];
		vec_sorted_expectation[i] = GreatPair<double>(i, app);
	}
	GreatPair<double>* begin = vec_sorted_expectation.Ptr();
	GreatPair<double>* end = begin + num_coarse_cluster;
	sort(begin, end);
}

void InvertedListBound::SortByAbsUpperBound(const Vector<double> &vec_inner_to_coarse_center, 
											Vector<GreatPair<double> > &vec_sorted_upper_bound)
{
	int num_coarse_cluster = vec_inner_to_coarse_center.Size();
	vec_sorted_upper_bound.AllocateSpace(num_coarse_cluster);
	for (int i = 0; i < num_coarse_cluster; i++)
	{
		double app = vec_inner_to_coarse_center[i];
		app = app > 0 ? app : -app;
		double range = m_pvec_coarse_bound_cluster->operator[](i);
		vec_sorted_upper_bound[i] = GreatPair<double>(i, app + range);
	}
	GreatPair<double>* begin = vec_sorted_upper_bound.Ptr();
	GreatPair<double>* end = begin + num_coarse_cluster;
	sort(begin, end);
}

void InvertedListBound::SortByUpperBound(const Vector<double> &vec_inner_to_coarse_center, 
										 Vector<GreatPair<double> > &vec_sorted_upper_bound)
{
	int num_coarse_cluster = vec_inner_to_coarse_center.Size();
	vec_sorted_upper_bound.AllocateSpace(num_coarse_cluster);
	for (int i = 0; i < num_coarse_cluster; i++)
	{
		double app = vec_inner_to_coarse_center[i];
		double range = m_pvec_coarse_bound_cluster->operator[](i);
		vec_sorted_upper_bound[i] = GreatPair<double>(i, app + range);
	}
	GreatPair<double>* begin = vec_sorted_upper_bound.Ptr();
	GreatPair<double>* end = begin + num_coarse_cluster;
	sort(begin, end);
}

void InvertedListBound::PruningTriplet(const double* db_query, 
									   Vector<GreatPair<double> > &heap, double &highest_lower)
{
	Vector<double> vec_inner_to_coarse_center;
	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center);

	Vector<Triplet<int, double, double> > vec_sorted_inverted_list;
	SortInvertedList(vec_inner_to_coarse_center, vec_sorted_inverted_list);
	//V: FilterCoarseCluster(vec_inner_to_coarse_center, vec_idx_coarse_center);

	ScanInvertedList(db_query, vec_inner_to_coarse_center, vec_sorted_inverted_list, heap, highest_lower);
}

void InvertedListBound::PruningIterative(const double* db_query, 
										 Vector<GreatPair<double> > &heap, double &highest_lower)
{
	Vector<double> vec_inner_to_coarse_center;
	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center);

	Vector<GreatPair<double> > vec_sorted_upper_bound;
	SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);
	//V: FilterCoarseCluster(vec_inner_to_coarse_center, vec_idx_coarse_center);

	ScanInvertedListIterative(
		db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap, highest_lower);
}

void InvertedListBound::Pruning(
	const double* query, 
	Vector<GreatPair<double> > &heap, 
	Heap<double> &highest_top_lower, double gamma)
{
	Vector<double> vec_inner_to_coarse_center;
	m_pmat_coarse_clusters->Multiple(query, vec_inner_to_coarse_center);

	if (!m_is_approximate)
	{
		Vector<GreatPair<double> > vec_sorted_abs_upper_bound;
		SortByAbsUpperBound(vec_inner_to_coarse_center, vec_sorted_abs_upper_bound);

		ScanInvertedListAbsTop(
			query, vec_inner_to_coarse_center, vec_sorted_abs_upper_bound, 
			heap, highest_top_lower, gamma);
	}
	else
	{
		/*
		Vector<GreatPair<double> > vec_sorted_upper_bound;
		SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);
		ScanInvertedListApp(db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap, highest_lower);
		*/
		SMART_ASSERT(0)("NOT CHECKED").Exit();
		//Vector<GreatPair<double> > vec_sorted_expectation;
		//SortByExpectation(vec_inner_to_coarse_center, vec_sorted_expectation);
		//ScanInvertedListAppExpecation(
		//	db_query, 
		//	vec_inner_to_coarse_center, 
		//	vec_sorted_expectation, 
		//	heap, 
		//	highest_lower);
	}
}

void InvertedListBound::Pruning(const double* db_query, 
								Vector<GreatPair<double> > &heap, double &highest_lower)
{
	Vector<double> vec_inner_to_coarse_center;
	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center);

	if (!m_is_approximate)
	{
		Vector<GreatPair<double> > vec_sorted_upper_bound;
		SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);

		ScanInvertedList(db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap, highest_lower);
	}
	else
	{
		/*
		Vector<GreatPair<double> > vec_sorted_upper_bound;
		SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);
		ScanInvertedListApp(db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap, highest_lower);
		*/

		Vector<GreatPair<double> > vec_sorted_expectation;
		SortByExpectation(vec_inner_to_coarse_center, vec_sorted_expectation);
		ScanInvertedListAppExpecation(
			db_query, 
			vec_inner_to_coarse_center, 
			vec_sorted_expectation, 
			heap, 
			highest_lower);
	}
}

//void InvertedListBound::Pruning(const double* db_query, 
//								Heap<LessPair<double> > &heap)
//{
//	Vector<double> vec_inner_to_coarse_center;
//	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center);
//
//	Vector<GreatPair<double> > vec_sorted_upper_bound;
//	SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);
//	//V: FilterCoarseCluster(vec_inner_to_coarse_center, vec_idx_coarse_center);
//
//	ScanInvertedList(db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap);
//}

//void InvertedListBound::Pruning(
//	const double* db_query, 
//	priority_queue<GreatPair<double> > &heap)
//{
//	Vector<double> vec_inner_to_coarse_center;
//	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center);
//
//	Vector<GreatPair<double> > vec_sorted_upper_bound;
//	SortByUpperBound(vec_inner_to_coarse_center, vec_sorted_upper_bound);
//	//V: FilterCoarseCluster(vec_inner_to_coarse_center, vec_idx_coarse_center);
//
//	ScanInvertedList(db_query, vec_inner_to_coarse_center, vec_sorted_upper_bound, heap);
//}

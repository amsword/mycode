#pragma once

#include "utility.h"
#include "InnerDistance.h"
#include "TimeTracker.h"

using namespace utility;

class BoundInnerProduct
{
public:
	void SetTimeTracker(TimeTracker &time_tracker);

	virtual void LargestMulti(const SMatrix<double> &mat_query,
		Vector<int> &vec_idx);

	virtual void LargestOne(const double* query,
		int &idx);




protected:

	//void PruneIt(
	//	int idx_point,
	//	double s, double range, 
	//	priority_queue<GreatPair<double> > &heap_upper, 
	//	double &largest_lower);

	//void PruneIt(
	//	int idx_point,
	//	double s, double range, 
	//	Heap<LessPair<double> > &heap_upper, 
	//	double &largest_lower);

	void PruneIt(
		int idx_point,
		double s, double range, 
		Vector<GreatPair<double> > &heap_upper, 
		double &largest_lower);

	void PruneIt(
		int idx_point,
		double s, double range, 
		Vector<GreatPair<double> > &heap_upper, 
		Heap<double> &highest_top_lower, 
		double gamma);


	//int Reranking(const double* query, 
	//	priority_queue<GreatPair<double> > &heap, 
	//	int &idx);

	int Reranking(const double* query, 
		Heap<LessPair<double> > &heap, 
		int &idx);

	int Reranking(const double* query, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower, 
		int &idx);

	int Reranking(const double* query, 
		Vector<GreatPair<double> > &heap, 
		Heap<double> &highest_top_lower, 
		Vector<int> &vec_topk, 
		double gamma);

	int Reranking(const double* query, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower, 
		int &idx,
		double &inner);

	void ConvertHeap2Vector(Heap<LessPair<double> > &heap, 
		Vector<LessPair<double> > &vec_large_up);

	void ConvertHeap2Vector(Vector<GreatPair<double> > &heap,
		double &largest_lower);

protected:
	const SMatrix<double>* m_pmat_database_raw;
	InnerDistance* m_pdistance_calculator;

	TimeTracker* m_p_time_tracker;





};

#pragma once
#include <vector>
#include <time.h>
#include <stack>

#include "utility.h"

using namespace std;
using namespace utility;


#define TIME_ID_ALL_QUERIES 0
#define TIME_ID_EACH_QUERIES 10


class TimeTracker
{
public:
	void Begin(int id);
	void End();
	void Report(list<int>& lst_id, list<vector<double> > &lst_vec_time_second);
	void ReportSingle(Vector<double> &vec_second);

private:
	stack<int> m_stk_id;
	stack<clock_t> m_stk_begin;

	vector<pair<int, double> > m_vec_pairs_ms;


};

#include "InnerDistance.h"

void InnerDistance::Initialize(
	int is_opt_R,
	const SMatrix<double> &matRotation,
	const Vector<SMatrix<double> > &vecmatCenters,
	int num_dic_each_partition)
{
	m_is_opt_R = is_opt_R;
	m_pvecmatCenters = &vecmatCenters;
	m_pmatRotation = &matRotation;
	m_nNumDicEachPartitions = num_dic_each_partition;
	m_nNumberRawFeaturePartitions = m_pvecmatCenters->Size();

	m_nDimension = 0;
	for (int i = 0; i < m_nNumberRawFeaturePartitions; i++)
	{
		m_nDimension += m_pvecmatCenters->operator[](i).Cols();
	}

	SMART_ASSERT(m_nNumDicEachPartitions == 1)(m_nNumDicEachPartitions).Exit();
}



void InnerDistance::PreProcessing(const double* pf_query, InterPara* &p_pre_out) const
{
	Vector<double> vec_rotated(m_nDimension);
	if (m_is_opt_R)
	{
		m_pmatRotation->Multiple(pf_query, vec_rotated.Ptr());
	}
	else
	{
		memcpy(vec_rotated.Ptr(), pf_query, sizeof(double) * m_nDimension);
	}

	p_pre_out = new InterPara();

	Vector<Vector<double> > &lookup = p_pre_out->lookup;

	lookup.AllocateSpace(m_nNumberRawFeaturePartitions);

	int k = 0;
	int idx_row = 0;
	const double* p_rotated = vec_rotated.Ptr();

	for (int i = 0; i < m_nNumberRawFeaturePartitions; i++)
	{
		const SMatrix<double>& mat_centers = m_pvecmatCenters->operator[](i);
		int sub_dim = mat_centers.Cols();

		int num_words = mat_centers.Rows();
		Vector<double> &vec_lookup = lookup[i];
		vec_lookup.AllocateSpace(num_words);
		for (int j = 0; j < num_words; j++)
		{
			vec_lookup[j] = dot(p_rotated, mat_centers[j], sub_dim); 
		}
		p_rotated += sub_dim;
	}
}
double InnerDistance::DistancePre(InterPara* para, const short* p_code) const
{
	double s = 0;
	for (int i = 0; i < m_nNumberRawFeaturePartitions; i++)
	{
		Vector<double> &vec_lookup = para->lookup[i];
		//for (int j = 0; j < m_nNumDicEachPartitions; j++)
		{
			s += vec_lookup[*p_code++];
		}
	}
	return s;
}
void InnerDistance::PostProcessing(InterPara* &para) const
{
	if (para)
	{
		delete para;
		para = NULL;
	}
}

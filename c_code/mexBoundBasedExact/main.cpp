
#include "main_point.h"

using namespace utility;


#ifdef _WINDOWS
void main()
{
	try
	{
			//main_mat();
		//main_test_tree();
		main_gamma_top_num();
		//main_exe_linear_scan();
	}
	catch (...)
	{
	}
}
#endif

void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	int type = 0;

	mexConvert(TYPE, type);

	if (type == 0)
	{
		EachCalculated(nlhs, plhs, nrhs, prhs);
	}
	else if (type == 1)
	{
		BoundBasedCalculated(nlhs, plhs, nrhs, prhs);
	}
	else if (type == 2)
	{
		MainInvertedListBound(nlhs, plhs, nrhs, prhs);
	}
	else if (type == 3)
	{
		MainSingleBallTree(nlhs, plhs, nrhs, prhs);
	}
	else if (type == 4)
	{
		MainInvertedListBoundAbsKNN(nlhs, plhs, nrhs, prhs);
	}

}

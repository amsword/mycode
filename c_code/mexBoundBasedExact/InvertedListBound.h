#pragma once

#include "utility.h"

#include "InnerDistance.h"
#include "TimeTracker.h"
#include "BoundInnerProduct.h"

using namespace utility;

class InvertedListBound : public BoundInnerProduct
{
public:
	InvertedListBound();
	void Initialize(
		SMatrix<double> &mat_raw_database,
		SMatrix<double> &mat_coarse_clusters,
		const Vector<double> &vec_bound_center,
		const Vector<Vector<int> > &vecvec_inverted_idx, 
		InnerDistance* dist_calculator, 
		const Vector<SMatrix<short> > &vecmat_all_ock_codes, 
		const Vector<Vector<double> > &vecvec_all_ock_bound);

	void SetIsApproximate(int is_approximate);
	void SetAppPara(int max_num_inverted_list);
	void SecureCheck();
	void CheckBound();
	//void LargestOne3(const double* query,
	//	int &idx);

	//void LargestOne_2(const double* query,
	//	int &idx);

	void LargestOne(const double* query,
		int &idx);

	void LargestAbsK(const double* query, Vector<int> &vec_topk, double gamma, int top_num);

	void LargestOne_good(const double* query,
		int &idx);

	void LargestOneIterative(const double* query,
		int &idx);

	void LargestOne_general(const double* query,
		int &idx);

	//void LargestOne_1(const double* query,
	//	int &idx);

	void SetInsighters(vector<int> &vec_num_visited_inverted_list,
		vector<int> &vec_heap_size,
		vector<int> &vec_num_true_inner_compute);


private:
	void Pruning(const double* query, 
		Vector<GreatPair<double> > &heap, double &highest_lower);

	void Pruning(const double* query, 
		Vector<GreatPair<double> > &heap, Heap<double> &highest_top_lower, double gamma);

	void PruningIterative(const double* query, 
		Vector<GreatPair<double> > &heap, double &highest_lower);


	void PruningTriplet(const double* query, 
		Vector<GreatPair<double> > &heap, double &highest_lower);

	//void Pruning(const double* query, 
	//	priority_queue<GreatPair<double> > &heap);

	//void Pruning(const double* query, 
	//	Heap<LessPair<double> > &heap);

	void SortByUpperBound(const Vector<double> &vec_inner_to_coarse_center, 
		Vector<GreatPair<double> > &vec_sorted_upper_bound);

	void SortByAbsUpperBound(const Vector<double> &vec_inner_to_coarse_center, 
		Vector<GreatPair<double> > &vec_sorted_upper_bound);

	void SortByExpectation(const Vector<double> &vec_inner_to_coarse_center, 
		Vector<GreatPair<double> > &vec_sorted_expectation);


	void SortInvertedList(const Vector<double> &vec_inner_to_coarse_center, 
		Vector<Triplet<int, double, double> > &vec_sorted_upper_bound);


	//void ScanInvertedList(
	//	InterPara* query_inter, 
	//	const SMatrix<short> &mat_ock_codes, 
	//	const Vector<int> &vec_inverted_idx, 
	//	const Vector<double> &vec_bound,
	//	double offset,
	//	priority_queue<GreatPair<double> > &heap, 
	//	double &largest_lower_bound);

	//void ScanInvertedList(
	//	InterPara* query_inter, 
	//	const SMatrix<short> &mat_ock_codes, 
	//	const Vector<int> &vec_inverted_idx, 
	//	const Vector<double> &vec_bound,
	//	double offset,
	//	Heap<LessPair<double> > &heap, 
	//	double &largest_lower_bound);

	void ScanInvertedList(
		InterPara* query_inter, 
		const SMatrix<short> &mat_ock_codes, 
		const Vector<int> &vec_inverted_idx, 
		const Vector<double> &vec_bound,
		double offset,
		Vector<GreatPair<double> > &heap, 
		double &largest_lower_bound);

	void ScanInvertedList(
		InterPara* query_inter, 
		const SMatrix<short> &mat_ock_codes, 
		const Vector<int> &vec_inverted_idx, 
		const Vector<double> &vec_bound,
		double offset,
		Vector<GreatPair<double> > &heap, 
		Heap<double> &highest_top_lower, 
		double gamma);


	//void ScanInvertedList(const double* query, 
	//	const Vector<double> &vec_inner_to_coarse_center, 
	//	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
	//	priority_queue<GreatPair<double> > &heap);

	//void ScanInvertedList(const double* query, 
	//	const Vector<double> &vec_inner_to_coarse_center, 
	//	const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
	//	Heap<LessPair<double> > &heap);

	void ScanInvertedList(const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower);

	void ScanInvertedListAbsTop(const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<GreatPair<double> > &vec_sorted_abs_upper_bound, 
		Vector<GreatPair<double> > &heap, 
		Heap<double> &highest_top_lower, 
		double gamma);

	void ScanInvertedListApp(const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower);

	void ScanInvertedListAppExpecation(
		const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower);

	void ScanInvertedListIterative(const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<GreatPair<double> > &vec_sorted_upper_bound, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower);

	void ScanInvertedList(const double* query, 
		const Vector<double> &vec_inner_to_coarse_center, 
		const Vector<Triplet<int, double, double> > &vec_sorted_inverted_index, 
		Vector<GreatPair<double> > &heap, 
		double &largest_lower);

private:
	SMatrix<double> *m_pmat_coarse_clusters;
	const Vector<double>* m_pvec_coarse_bound_cluster;

	const Vector<SMatrix<short> > *m_pvecmat_ock_codes;
	const Vector<Vector<double> >* m_pvecvec_bound;
	const Vector<Vector<int> >* m_pvecvec_inverted_idx;

	int m_dim;

	vector<int>* m_pvec_num_visited_inverted_list;
	vector<int>* m_pvec_heap_size;
	vector<int>* m_pvec_num_true_inner_compute;

	int m_is_approximate;
	int m_max_num_inverted_list;
};


#include "BallTree.h"

void BallTree::ComputeMean(const Vector<int>* pvec_idx, 
						   Vector<double>* &p_center)
{
	int num_point = pvec_idx->size();
	int dim = m_pmat_database_raw->Cols();
	p_center = new Vector<double>(dim);
	p_center->SetValueZeros();
	for (int i = 0; i < num_point; i++)
	{
		int idx_point = pvec_idx->operator[](i);
		const double* p_point = m_pmat_database_raw->operator[](idx_point);
		utility::VectorAdd(p_center->Ptr(), p_point, dim);
	}
	*p_center /=(num_point);
}

void BallTree::ComputeRadius(
	const Vector<int>* pvec_idx, 
	const double* p_center, 
	double &radius, 
	int &idx)
{
	int num_point = pvec_idx->size();
	int dim = m_pmat_database_raw->Cols();
	idx = -1;
	radius = -10^5;

	Vector<double> sq_dist(num_point);

	if (num_point > 1000)
	{
#pragma omp parallel for
		for (int i = 0; i < num_point; i++)
		{
			int idx_point = pvec_idx->operator[](i);
			SMART_ASSERT(idx_point >= 0 && idx_point < m_pmat_database_raw->Rows())(idx_point).Exit();
			const double* p_point = m_pmat_database_raw->operator[](idx_point);
			sq_dist[i] = squared_distance(p_point, p_center, dim);
		}
	}
	else
	{
		for (int i = 0; i < num_point; i++)
		{
			int idx_point = pvec_idx->operator[](i);
			SMART_ASSERT(idx_point >= 0 && idx_point < m_pmat_database_raw->Rows())(idx_point).Exit();
			const double* p_point = m_pmat_database_raw->operator[](idx_point);
			sq_dist[i] = squared_distance(p_point, p_center, dim);
		}
	}

	for (int i = 0; i < num_point; i++)
	{
		int idx_point = pvec_idx->operator[](i);
		double s = sq_dist[i];
		if (s > radius)
		{
			radius = s;			
			idx = idx_point;
		}
	}

	radius = sqrt(radius);
	SMART_ASSERT(idx != -1)(idx).Exit();
}

void BallTree::ComputeRadius1(
	const Vector<int>* pvec_idx, 
	const double* p_center, 
	double &radius, 
	int &idx)
{
	int num_point = pvec_idx->size();
	int dim = m_pmat_database_raw->Cols();
	idx = -1;
	radius = -10^5;
	for (int i = 0; i < num_point; i++)
	{
		int idx_point = pvec_idx->operator[](i);
		SMART_ASSERT(idx_point >= 0 && idx_point < m_pmat_database_raw->Rows())(idx_point).Exit();
		const double* p_point = m_pmat_database_raw->operator[](idx_point);
		double s = squared_distance(p_point, p_center, dim);
		s = sqrt(s);
		if (s > radius)
		{
			radius = s;			
			idx = idx_point;
		}
	}
	SMART_ASSERT(idx != -1)(idx).Exit();
}

void BallTree::SplitIndexToIndicator(
	const Vector<int>* pvec_idx, 
	const double* p_left_anchor, 
	const double* p_right_anchor, 
	Vector<int> &vec_indicator)
{
	int num_point = pvec_idx->size();
	int dim = m_pmat_database_raw->Cols();
	vec_indicator.AllocateSpace(num_point);

	// #pragma omp parallel for
	for (int i = 0; i < num_point; i++)
	{
		int idx_curr = pvec_idx->operator[](i);
		const double* p_curr = m_pmat_database_raw->operator[](idx_curr);
		double left = squared_distance(p_curr, p_left_anchor, dim);
		double right = squared_distance(p_curr, p_right_anchor, dim);

		if (left <= right)
		{
			vec_indicator[i] = 0;
		}
		else
		{
			vec_indicator[i] = 1;
		}
	}
}

void BallTree::SplitIndex(
	const Vector<int>* pvec_idx, int idxA, int idxB, 
	Vector<int>* &pvec_idx_left, Vector<int>* &pvec_idx_right)
{
	const double* p_left_anchor = m_pmat_database_raw->operator[](idxA);
	const double* p_right_anchor = m_pmat_database_raw->operator[](idxB);

	int num_point = pvec_idx->size();

	Vector<int> vec_indicator;

	SplitIndexToIndicator(pvec_idx, p_left_anchor, p_right_anchor, 
		vec_indicator);

	int num_right = vec_indicator.SumUp();
	int num_left = num_point - num_right;
	if (num_left > 0)
	{
		pvec_idx_left = new Vector<int>();
		pvec_idx_left->AllocateSpace(num_left);
	}
	else
	{
		pvec_idx_left = NULL;
	}

	if (num_right > 0)
	{
		pvec_idx_right = new Vector<int>();
		pvec_idx_right->AllocateSpace(num_right);
	}
	else
	{
		pvec_idx_right = NULL;
	}

	int idx_left = 0;
	int idx_right = 0;
	for (int i = 0; i < num_point; i++)
	{
		int idx_curr = pvec_idx->operator[](i);
		if (vec_indicator[i])
		{
			pvec_idx_right->operator[](idx_right++) = idx_curr;
		}
		else
		{
			pvec_idx_left->operator[](idx_left++) = idx_curr;
		}
	}
	SMART_ASSERT(idx_left == num_left && idx_right == num_right).Exit();
}

void BallTree::MakeBallTreeSplit(const Vector<int>* pvec_idx, 
								 int &idxA, 
								 int &idxB)
{
	SMART_ASSERT(pvec_idx->size() != 411697)(pvec_idx->size());

	int num_point = pvec_idx->size();
	int selected = (int)(rand() / (double)RAND_MAX * num_point); 
	selected = selected >= num_point ? num_point - 1 : selected;
	selected = pvec_idx->operator[](selected);

	double radius;

	ComputeRadius(pvec_idx, 
		m_pmat_database_raw->operator[](selected), 
		radius, 
		idxA);


	ComputeRadius(pvec_idx, 
		m_pmat_database_raw->operator[](idxA), 
		radius, 
		idxB);

}

void BallTree::MakeBallTree(
	const Vector<int>* pvec_idx, 
	Node* &proot, int level)
{
	if (pvec_idx == NULL && pvec_idx->size() == 0)
	{
		proot = NULL;
		return;
	}

	Vector<double>* p_center;
	ComputeMean(pvec_idx, p_center);

	double radius = -10^5;
	int tmp_idx;

	ComputeRadius(pvec_idx, p_center->Ptr(), 
		radius, tmp_idx);

	proot = new Node();
	proot->pvec_idx = pvec_idx;
	proot->pvec_center = p_center;
	proot->radius = radius;

	//{
	//	static clock_t begin = clock();
	//	clock_t curr = clock();
	//	if (curr - begin > CLOCKS_PER_SEC * 2)
	//	{
	//		PRINT << level << "\n";
	//		begin = curr;
	//	}

	//}
	if (pvec_idx->size() <= m_nMaxPointsLeaf)
	{
		proot->left = NULL;
		proot->right = NULL;
	}
	else
	{
		int idxA, idxB;
		Vector<int>* pvec_idx_left;
		Vector<int>* pvec_idx_right;

		MakeBallTreeSplit(pvec_idx, idxA, idxB);

		if (squared_distance(m_pmat_database_raw->operator[](idxA),
			m_pmat_database_raw->operator[](idxB), 
			m_pmat_database_raw->Cols()) <  0.000001)
		{
			proot->left = NULL;
			proot->right = NULL;
			return;
		}

		SplitIndex(pvec_idx, idxA, idxB, 
			pvec_idx_left, pvec_idx_right);

		SMART_ASSERT(pvec_idx_left->size() > 0 && pvec_idx_right->size() > 0).Exit();

		MakeBallTree(pvec_idx_left, proot->left, ++level);
		MakeBallTree(pvec_idx_right, proot->right, level);
	}
}

double BallTree::MIP(const double* query, const Node* root)
{
	if (root == NULL)
	{
		return -10^5;
	}
	else
	{
		double s = dot(query, root->pvec_center->Ptr(), root->pvec_center->size());
		s += root->radius;
		return s;
	}
}

int BallTree::LinearSearch(
	const double* query, 
	const Vector<int>* pvec_idx, 
	int &idx, 
	double &maximum_inner)
{
	int num_point = pvec_idx->size();

	int dim = m_pmat_database_raw->Cols();
	for (int i = 0; i < num_point; i++)
	{
		int idx_curr = pvec_idx->operator[](i);
		const double* p_point = m_pmat_database_raw->operator[](idx_curr);

		double s = dot(query, p_point, dim);
		if (s > maximum_inner)
		{
			maximum_inner = s;
			idx = idx_curr;
		}
	}
	return num_point;
}

int BallTree::TreeSearch(
	const double* query, 
	int &idx, double &maximum_inner, const Node* root, 
	double upper_bound, int &num_mip)
{
	int real_computed = 0;

	if (root == NULL)
	{
		return 0;
	}
	num_mip = 0;

	if (upper_bound == DBL_MAX)
	{
		upper_bound = MIP(query, root);
		num_mip++;
	}
	if (maximum_inner < upper_bound)
	{
		if (root->left == NULL && root->right == NULL)
		{
			real_computed = LinearSearch(query, root->pvec_idx, idx, maximum_inner);
			//m_nNumberRealComputed += real_computed;
		}
		else
		{
			double left_upper = MIP(query, root->left);
			double right_upper = MIP(query, root->right);
			int n_l = 0, n_r = 0;
			if (left_upper <= right_upper)
			{
				real_computed += TreeSearch(query, idx, maximum_inner, root->right, right_upper, n_r);

				if ((m_is_approximate && real_computed < m_num_max_real_computed) || !m_is_approximate)
				{
					real_computed += TreeSearch(query, idx, maximum_inner, root->left, left_upper, n_l);
				}
			}
			else
			{
				real_computed += TreeSearch(query, idx, maximum_inner, root->left, left_upper, n_l);
				if ((m_is_approximate && real_computed < m_num_max_real_computed) || !m_is_approximate)
				{
					real_computed += TreeSearch(query, idx, maximum_inner, root->right, right_upper, n_r);
				}
			}
			num_mip += 2 + n_l + n_r;
		}
	}
	return real_computed;
}

void BallTree::LargestOne(const double* query, int &idx)
{
	int dim = m_pmat_database_raw->Cols();
	double s = squared_L2_norm(query, dim);
	s = sqrt(s);
	Vector<double> vec_normalized_query(dim);

	scale_multi_vector(1.0 / s, query, vec_normalized_query.Ptr(), dim);

	double maximum_inner = -10^5;
	idx = -1;

	int num_mip;
	int num_real_computed = TreeSearch(vec_normalized_query.Ptr(), idx, maximum_inner, m_proot, 
		DBL_MAX, num_mip);

	if (m_pvecNumberRealComputed)
	{
		m_pvecNumberRealComputed->push_back(num_real_computed);
	}
	if (m_pvecNumberMIP)
	{
		m_pvecNumberMIP->push_back(num_mip);
	}

}

void BallTree::ReleaseTree(Node* &root)
{
	if (root)
	{
		if (root->pvec_idx)
		{
			delete root->pvec_idx;
			root->pvec_idx = NULL;
		}
		if (root->pvec_center)
		{
			delete root->pvec_center;
			root->pvec_center = NULL;
		}
		ReleaseTree(root->left);
		ReleaseTree(root->right);
	}
}

BallTree::BallTree() : m_nMaxPointsLeaf(20), m_is_approximate(0)
{

}

BallTree::~BallTree()
{
	ReleaseTree(m_proot);
}

void BallTree::SetVecNumberMIP(Vector<int> &vec_num_mip)
{
	m_pvecNumberMIP = &vec_num_mip;
}

int BallTree::MaxDepth(const Node* p)
{
	if (p == NULL)
	{
		return 0;
	}
	else if (p->left == NULL && p->right == NULL)
	{
		return 1;
	}
	else
	{
		int left = MaxDepth(p->left);
		int right = MaxDepth(p->right);
		if (left > right)
		{
			return left + 1;
		}
		else
		{
			return right + 1;
		}
	}
}

int BallTree::MaxDepth()
{
	return MaxDepth(m_proot);
}

void BallTree::SetMaximumPointsLeaf(int nMaxPointsLeaf)
{
	m_nMaxPointsLeaf = nMaxPointsLeaf;
}

void BallTree::SetAppPara(int num_max_real_computed)
{
	m_num_max_real_computed = num_max_real_computed;
}

void BallTree::SetIsApproximate(int is_approximate)
{
	m_is_approximate = is_approximate;
}

void BallTree::SetVecNumberRealComputed(Vector<int> &vec_num_real_computed)
{
	m_pvecNumberRealComputed = &vec_num_real_computed;
}

void BallTree::Building(const SMatrix<double> &mat_database)
{
	m_pmat_database_raw = &mat_database;

	m_proot = new Node();
	int num_point = m_pmat_database_raw->Rows();

	Vector<int>* pvec_idx = new Vector<int>(num_point);

	for (int i = 0; i < num_point; i++)
	{
		pvec_idx->operator[](i) = i;
	}

	MakeBallTree(pvec_idx, m_proot, 0);
}


#include "TimeTracker.h"

void TimeTracker::Report(
	list<int>& lst_id, 
	list<vector<double> > &lst_vec_time_second)
{
	int total_pairs = m_vec_pairs_ms.size();
	vector<bool> vec_visited(total_pairs, false);

	for (int i = 0; i < total_pairs; i++)
	{
		if (vec_visited[i])
		{
			continue;
		}

		vec_visited[i] = true;

		const pair<int, double> &p = m_vec_pairs_ms[i];
		int id = p.first;
		double time_cost = p.second / (double)CLOCKS_PER_SEC;
		lst_id.push_back(id);
		vector<double> vec_time_cost;
		vec_time_cost.push_back(time_cost);
		
		for (int j = i + 1; j < total_pairs; j++)
		{
			const pair<int, double> &p2 = m_vec_pairs_ms[j];
			if (p2.first == id)
			{
				time_cost = p2.second / (double)CLOCKS_PER_SEC;
				vec_time_cost.push_back(time_cost);
				vec_visited[j] = true;
			}
		}
		lst_vec_time_second.push_back(vec_time_cost);

		



	}



}

void TimeTracker::Begin(int id)
{
	m_stk_id.push(id);
	clock_t begin = clock();
	m_stk_begin.push(begin);
}

void TimeTracker::End()
{
	clock_t end = clock();

	int id = m_stk_id.top();
	double begin = m_stk_begin.top();

	m_stk_id.pop();
	m_stk_begin.pop();

	m_vec_pairs_ms.push_back(pair<int, double> (id, end - begin));
}

void TimeTracker::ReportSingle(Vector<double> &vec_second)
{
	SMART_ASSERT(m_stk_begin.empty()).Exit();
	SMART_ASSERT(m_stk_id.empty()).Exit();

	SMART_ASSERT(vec_second.Size() == m_vec_pairs_ms.size()).Exit();

	int num = vec_second.Size();

	int id = -1;
	if (num > 0)
	{
		id = m_vec_pairs_ms[0].first;
	}

	for (int i = 0; i < num; i++)
	{
		const pair<int, double> &p = m_vec_pairs_ms[i];
		
		SMART_ASSERT(id == p.first).Exit();

		vec_second[i] = p.second / (double)CLOCKS_PER_SEC;
	}
}

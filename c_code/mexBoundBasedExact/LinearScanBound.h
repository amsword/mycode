#pragma once

#include "utility.h"
#include "InnerDistance.h"
#include "TimeTracker.h"
#include "BoundInnerProduct.h"

using namespace utility;

class LinearScanBound : public BoundInnerProduct
{
public:
	void Initialize(const SMatrix<double> &mat_database_raw, 
		const SMatrix<short> &mat_database_code, 
		InnerDistance &distance_calculator, 
		Vector<double> &vec_bound);

	void LargestOne(
		const double* query,
		int &idx);

protected:
	void BoundProne(const double* query, 
		priority_queue<GreatPair<double> > &heap);

	void BoundProne(const double* query, 
		Heap<LessPair<double> > &heap);


protected:
	const SMatrix<short>* m_pmat_database_code;
	const Vector<double>* m_pvec_bound;
};

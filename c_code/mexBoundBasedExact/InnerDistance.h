#pragma once

#include "utility.h"

struct InterPara
{
	Vector<Vector<double> > lookup;
};

class InnerDistance
{
public:
	void Initialize(
		int is_opt_R,
		const SMatrix<double> &matRotation,
		const Vector<SMatrix<double> > &vecmatCenters,
		int num_dic_each_partition
		);
public:
	virtual void PreProcessing(const double* p_query, InterPara* &p_pre_out) const;
	virtual double DistancePre(InterPara* p_query, const short* p_right) const;
	virtual void PostProcessing(InterPara* &p_pre_out) const;

private:
	int m_nNumberRawFeaturePartitions;
	int m_nDimension;
	int m_nNumDicEachPartitions;

	int m_is_opt_R;
	const SMatrix<double>* m_pmatRotation;
	const Vector<SMatrix<double> >* m_pvecmatCenters;
};

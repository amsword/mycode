#include "main_point.h"
#include "InvertedListBound.h"
#include "BallTree.h"

//#ifndef _WINDOWS
//#include <sys/time.h>
//#include <sys/resource.h>
//#endif



void SetResult(
	mxArray* result, 
	int idx_start, 
	const string &name, 
	const Vector<int> &vec_real_computed)
{
	char** c_names = new char*[1];
	c_names[0] = new char[256];
	strcpy(c_names[0], name.c_str());
	mxArray* p = mxCreateStructMatrix(1, 1, 1, (const char**)c_names);
	delete[] c_names[0];
	delete[] c_names;


	int num_element = vec_real_computed.size();
	mxArray* p2 = mxCreateDoubleMatrix(num_element, 1, mxREAL);
	Vector<double> vec_inter;
	mexConvert(p2, vec_inter);
	for (int i = 0; i < num_element; i++)
	{
		vec_inter[i] = vec_real_computed[i];
	}
	mxSetFieldByNumber(p, 0, 0, p2);
	mxSetCell(result, idx_start, p);
}


void SetResult(mxArray* result, int start_idx, list<int> &lst_id,
			   list<vector<double> > &lst_vec_time)
{
	int num_kind = lst_id.size();
	SMART_ASSERT(num_kind == lst_vec_time.size()).Exit();

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}

		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(result, i + start_idx, p);
	}
}





void EachCalculated(int nlhs, mxArray *plhs[], 
					int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 3)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2)(nlhs).Exit();

	SMatrix<double> mat_query;
	SMatrix<double> mat_database;
	Vector<int> vec_nn_idx;

	mexConvert(MAT_QUERY, mat_query);
	int num_query = mat_query.Rows();
	mexConvert(MAT_DATABASE, mat_database);
	mwSize tmp_size[1]; 
	tmp_size[0] = mat_query.Rows();	
	RESULT = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(RESULT, vec_nn_idx);
	const char* field_names[] = {"each_cost"};
	RESULT_INFO = mxCreateStructMatrix(1, 1, 1, field_names);
	mxArray *p = mxCreateDoubleMatrix(num_query, 1, mxREAL);
	Vector<double> vec_times;
	mexConvert(p, vec_times);
	mxSetField(RESULT_INFO, 0, field_names[0], p);

	TimeTracker time_tracker;

	BruteforceKNNer<double> knn;

	vec_nn_idx.AllocateSpace(num_query);
	for (int i = 0; i < num_query; i++)
	{
		const double* query = mat_query[i];
		int best_idx;
		double best_value;

		time_tracker.Begin(__LINE__);
		knn.LargetInnerProduct(query, mat_database, best_idx, best_value);
		time_tracker.End();

		vec_nn_idx[i] = best_idx;
	}

	time_tracker.ReportSingle(vec_times);
}


void MainSingleBallTree(
	int nlhs, mxArray *plhs[], 
	int nrhs, const mxArray *prhs[])
{
	int num_thd = omp_get_num_procs();
	omp_set_num_threads(num_thd);

	SMART_ASSERT(nrhs == 4)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2)(nlhs).Exit();

	SMatrix<double> mat_query;
	SMatrix<double> mat_database_raw;

	Vector<int> vec_nn_idx;
	// global ock-means



	int max_leaf;
	mexConvert(MAT_QUERY, mat_query);
	mexConvert(MAT_DATABASE, mat_database_raw);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "max_leaf_size"), max_leaf);
	mxArray* p = mxGetField(DISTANCE_INFO, 0, "is_approximate");
	int is_approximate;
	mexConvert(p, is_approximate);
	int num_max_real_computed = mat_database_raw.Rows();
	if (is_approximate)
	{
		mexConvert(mxGetField(DISTANCE_INFO, 0, "num_max_real_computed"), num_max_real_computed);
	}

	int num_queries = mat_query.Rows();

	mwSize tmp_size[1]; 
	tmp_size[0] = num_queries;	
	RESULT = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(RESULT, vec_nn_idx);

	TimeTracker time_tracker;

	Vector<int> vec_real_computed;
	Vector<int> vec_num_mip;
	vec_real_computed.reserve(num_queries);
	vec_num_mip.reserve(num_queries);


	//#ifndef _WINDOWS
	//	struct rlimit limit_info;
	//	int no_ok = getrlimit(RLIMIT_STACK, &limit_info);
	//	SMART_ASSERT(no_ok == 0).Exit();
	//	limit_info.rlim_cur = 100 * 1024 * 1024;
	//	no_ok = setrlimit(RLIMIT_STACK, &limit_info);
	//	SMART_ASSERT(no_ok == 0).Exit();
	//#endif

	srand(10);

	BallTree bt;
	bt.SetVecNumberRealComputed(vec_real_computed);
	bt.SetVecNumberMIP(vec_num_mip);
	bt.SetTimeTracker(time_tracker);
	bt.SetIsApproximate(is_approximate);
	bt.SetAppPara(num_max_real_computed);
	bt.SetMaximumPointsLeaf(max_leaf);

	bt.Building(mat_database_raw);

	SMART_ASSERT(0)(bt.MaxDepth());

	bt.LargestMulti(mat_query, vec_nn_idx);

	list<int> lst_id;
	list<vector<double> > lst_vec_time;
	time_tracker.Report(lst_id, lst_vec_time);

	int num_reported = lst_id.size() + 2;
	RESULT_INFO = mxCreateCellMatrix(num_reported, 1);
	int idx_start = 0;
	SetResult(RESULT_INFO, idx_start, lst_id, lst_vec_time);
	idx_start += lst_id.size();

	SetResult(RESULT_INFO, idx_start, "num_real_computed", vec_real_computed);
	idx_start++;
	SetResult(RESULT_INFO, idx_start, "num_mip", vec_num_mip);
}

void MainInvertedListBound(int nlhs, mxArray *plhs[], 
						   int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 4)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2)(nlhs).Exit();

	SMatrix<double> mat_query;
	SMatrix<double> mat_database_raw;
	SMatrix<double> mat_coarse_centers;
	Vector<Vector<int> > vecvec_inverted_idx;
	Vector<double> vec_bound_center;

	Vector<SMatrix<short> > vecmat_all_ock_codes;
	Vector<Vector<double> > vecvec_all_ock_bound;
	Vector<int> vec_nn_idx;
	// global ock-means
	int is_optimize_R;
	SMatrix<double> matRotation;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;
	int is_approximate;
	int max_num_inverted_list;

	mexConvert(MAT_QUERY, mat_query);
	mexConvert(MAT_DATABASE, mat_database_raw);
	mxArray* p = mxGetField(DISTANCE_INFO, 0, "is_approximate");
	mexConvert(p, is_approximate);
	if (is_approximate)
	{
		p = mxGetField(DISTANCE_INFO, 0, "max_num_inverted_list");
		mexConvert(p, max_num_inverted_list);
	}
	mxArray* p_coarse_model = mxGetField(DISTANCE_INFO, 0, "coarse_model");
	mexConvert(mxGetField(p_coarse_model, 0, "coarse_centers"), mat_coarse_centers);
	mexConvert(mxGetField(p_coarse_model, 0, "all_inverted_idx"), vecvec_inverted_idx);
	mexConvert(mxGetField(p_coarse_model, 0, "bound_center"), vec_bound_center);
	mxArray* p_fine_model = mxGetField(DISTANCE_INFO, 0, "fine_model");
	mexConvert(mxGetField(p_fine_model, 0, "all_ock_codes"), vecmat_all_ock_codes);
	mexConvert(mxGetField(p_fine_model, 0, "all_ock_bound"), vecvec_all_ock_bound);
	mxArray* p_ock_means_model = mxGetField(p_fine_model, 0, "global_ock_model");
	mexConvert(mxGetField(p_ock_means_model, 0, "is_optimize_R"), is_optimize_R);
	mexConvert(mxGetField(p_ock_means_model, 0, "R"), matRotation);
	mexConvert(mxGetField(p_ock_means_model, 0, "all_D"), vecmatCenters);
	mexConvert(mxGetField(p_ock_means_model, 0, "num_sub_dic_each_partition"), num_dic_each_partition);
	SMART_ASSERT(num_dic_each_partition == 1)(num_dic_each_partition).Exit();

	mwSize tmp_size[1]; 
	tmp_size[0] = mat_query.Rows();	
	RESULT = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(RESULT, vec_nn_idx);

	InnerDistance dist_calculator;
	dist_calculator.Initialize(is_optimize_R, matRotation, vecmatCenters, num_dic_each_partition);

	TimeTracker time_tracker;

	InvertedListBound bounder;
	bounder.SetIsApproximate(is_approximate);
	if (is_approximate)
	{
		bounder.SetAppPara(max_num_inverted_list);
	}

	bounder.Initialize(mat_database_raw, 
		mat_coarse_centers, vec_bound_center, vecvec_inverted_idx, 
		&dist_calculator, vecmat_all_ock_codes, vecvec_all_ock_bound);
	bounder.SetTimeTracker(time_tracker);

	vector<int> vec_num_visited_inverted_list;
	vector<int> vec_heap_size;
	vector<int> vec_num_true_inner_compute;
	bounder.SetInsighters(vec_num_visited_inverted_list, vec_heap_size, vec_num_true_inner_compute);

	bounder.LargestMulti(mat_query, vec_nn_idx);

	list<int> lst_id;
	list<vector<double> > lst_vec_time_cost_second;
	time_tracker.Report(lst_id, lst_vec_time_cost_second);

	int num_kind = lst_id.size();
	RESULT_INFO = mxCreateCellMatrix(num_kind + 3, 1);

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time_cost_second.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}
		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(RESULT_INFO, i, p);
	}

	{
		//SMART_ASSERT(0)(vec_heap_size.size())(vec_num_visited_inverted_list.size())
		//(vec_num_true_inner_compute.size());


		mxArray* p = mxCreateDoubleMatrix(vec_heap_size.size(), 1, mxREAL);
		Vector<double> vec_tmp1;
		mexConvert(p, vec_tmp1);
		for (int i = 0; i < vec_heap_size.size(); i++)
		{
			vec_tmp1[i] = vec_heap_size[i];
		}
		mxSetCell(RESULT_INFO, num_kind, p);

		p = mxCreateDoubleMatrix(vec_num_visited_inverted_list.size(), 1, mxREAL);
		Vector<double> vec_tmp2;
		mexConvert(p, vec_tmp2);
		for (int i = 0; i < vec_num_visited_inverted_list.size(); i++)
		{
			vec_tmp2[i] = vec_num_visited_inverted_list[i];
		}
		mxSetCell(RESULT_INFO, num_kind + 1, p);

		p = mxCreateDoubleMatrix(vec_num_true_inner_compute.size(), 1, mxREAL);
		Vector<double> vec_tmp3;
		mexConvert(p, vec_tmp3);
		for (int i = 0; i < vec_num_true_inner_compute.size(); i++)
		{
			vec_tmp3[i] = vec_num_true_inner_compute[i];
		}
		mxSetCell(RESULT_INFO, num_kind + 2, p);

	}

}

void MainInvertedListBoundAbsKNN(int nlhs, mxArray *plhs[], 
						   int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 4)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2)(nlhs).Exit();

	Vector<double> vec_query;
	SMatrix<double> mat_database_raw;
	SMatrix<double> mat_coarse_centers;
	Vector<Vector<int> > vecvec_inverted_idx;
	Vector<double> vec_bound_center;

	Vector<SMatrix<short> > vecmat_all_ock_codes;
	Vector<Vector<double> > vecvec_all_ock_bound;
	
	// global ock-means
	int is_optimize_R;
	SMatrix<double> matRotation;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;
	int is_approximate;
	int max_num_inverted_list;

	mexConvert(MAT_QUERY, vec_query);
	mexConvert(MAT_DATABASE, mat_database_raw);
	mxArray* p = mxGetField(DISTANCE_INFO, 0, "is_approximate");
	mexConvert(p, is_approximate);
	if (is_approximate)
	{
		p = mxGetField(DISTANCE_INFO, 0, "max_num_inverted_list");
		mexConvert(p, max_num_inverted_list);
	}
	mxArray* p_coarse_model = mxGetField(DISTANCE_INFO, 0, "coarse_model");
	mexConvert(mxGetField(p_coarse_model, 0, "coarse_centers"), mat_coarse_centers);
	mexConvert(mxGetField(p_coarse_model, 0, "all_inverted_idx"), vecvec_inverted_idx);
	mexConvert(mxGetField(p_coarse_model, 0, "bound_center"), vec_bound_center);
	mxArray* p_fine_model = mxGetField(DISTANCE_INFO, 0, "fine_model");
	mexConvert(mxGetField(p_fine_model, 0, "all_ock_codes"), vecmat_all_ock_codes);
	mexConvert(mxGetField(p_fine_model, 0, "all_ock_bound"), vecvec_all_ock_bound);
	mxArray* p_ock_means_model = mxGetField(p_fine_model, 0, "global_ock_model");
	mexConvert(mxGetField(p_ock_means_model, 0, "is_optimize_R"), is_optimize_R);
	mexConvert(mxGetField(p_ock_means_model, 0, "R"), matRotation);
	mexConvert(mxGetField(p_ock_means_model, 0, "all_D"), vecmatCenters);
	mexConvert(mxGetField(p_ock_means_model, 0, "num_sub_dic_each_partition"), num_dic_each_partition);
	SMART_ASSERT(num_dic_each_partition == 1)(num_dic_each_partition).Exit();

	double gamma;
	mexConvert(mxGetField(DISTANCE_INFO, 0, "gamma"), gamma);
	double top_num;
	mexConvert(mxGetField(DISTANCE_INFO, 0, "top_num"), top_num);


	InnerDistance dist_calculator;
	dist_calculator.Initialize(is_optimize_R, matRotation, vecmatCenters, num_dic_each_partition);

	TimeTracker time_tracker;

	InvertedListBound bounder;
	bounder.SetIsApproximate(is_approximate);
	if (is_approximate)
	{
		bounder.SetAppPara(max_num_inverted_list);
	}
	bounder.Initialize(mat_database_raw, 
		mat_coarse_centers, vec_bound_center, vecvec_inverted_idx, 
		&dist_calculator, vecmat_all_ock_codes, vecvec_all_ock_bound);
	bounder.SetTimeTracker(time_tracker);

	vector<int> vec_num_visited_inverted_list;
	vector<int> vec_heap_size;
	vector<int> vec_num_true_inner_compute;
	bounder.SetInsighters(vec_num_visited_inverted_list, vec_heap_size, vec_num_true_inner_compute);

	Vector<int> vec_nn_idx;

	time_tracker.Begin(0);
	bounder.LargestAbsK(vec_query.Ptr(), vec_nn_idx, gamma, top_num);
	time_tracker.End();

	RESULT = mxCreateDoubleMatrix(vec_nn_idx.size(), 1, mxREAL);
	Vector<double> vec_nn_idx2;
	mexConvert(RESULT, vec_nn_idx2);
	for (int i = 0; i < vec_nn_idx.size(); i++)
	{
		vec_nn_idx2[i] = vec_nn_idx[i] + 1;
	}


	list<int> lst_id;
	list<vector<double> > lst_vec_time_cost_second;
	time_tracker.Report(lst_id, lst_vec_time_cost_second);

	int num_kind = lst_id.size();
	RESULT_INFO = mxCreateCellMatrix(num_kind + 3, 1);

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time_cost_second.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}
		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(RESULT_INFO, i, p);
	}

	{
		mxArray* p = mxCreateDoubleMatrix(vec_heap_size.size(), 1, mxREAL);
		Vector<double> vec_tmp1;
		mexConvert(p, vec_tmp1);
		for (int i = 0; i < vec_heap_size.size(); i++)
		{
			vec_tmp1[i] = vec_heap_size[i];
		}
		mxSetCell(RESULT_INFO, num_kind, p);

		p = mxCreateDoubleMatrix(vec_num_visited_inverted_list.size(), 1, mxREAL);
		Vector<double> vec_tmp2;
		mexConvert(p, vec_tmp2);
		for (int i = 0; i < vec_num_visited_inverted_list.size(); i++)
		{
			vec_tmp2[i] = vec_num_visited_inverted_list[i];
		}
		mxSetCell(RESULT_INFO, num_kind + 1, p);

		p = mxCreateDoubleMatrix(vec_num_true_inner_compute.size(), 1, mxREAL);
		Vector<double> vec_tmp3;
		mexConvert(p, vec_tmp3);
		for (int i = 0; i < vec_num_true_inner_compute.size(); i++)
		{
			vec_tmp3[i] = vec_num_true_inner_compute[i];
		}
		mxSetCell(RESULT_INFO, num_kind + 2, p);

	}





}

void BoundBasedCalculated(int nlhs, mxArray *plhs[], 
						  int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 4)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2)(nlhs).Exit();

	SMatrix<double> mat_query;
	SMatrix<double> mat_database_raw;
	SMatrix<short> mat_database_code;
	Vector<double> vec_bound;
	Vector<int> vec_nn_idx;

	int is_opt_R;
	SMatrix<double> matRotation;
	Vector<SMatrix<double> > vecmatCenters;
	int num_dic_each_partition;

	mexConvert(MAT_QUERY, mat_query);
	mexConvert(MAT_DATABASE, mat_database_raw);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "is_optimize_R"), is_opt_R);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "R"), matRotation);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "all_D"), vecmatCenters);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "num_sub_dic_each_partition"), num_dic_each_partition);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "ock_bounds"), vec_bound);
	mexConvert(mxGetField(DISTANCE_INFO, 0, "ock_codes"), mat_database_code);
	mwSize tmp_size[1]; 
	tmp_size[0] = mat_query.Rows();	
	RESULT = mxCreateNumericArray(1, tmp_size, mxINT32_CLASS, mxREAL); 
	mexConvert(RESULT, vec_nn_idx);

	InnerDistance dist_calculator;
	dist_calculator.Initialize(is_opt_R, matRotation, vecmatCenters, num_dic_each_partition);

	TimeTracker time_tracker;

	LinearScanBound bounder;
	bounder.Initialize(mat_database_raw, mat_database_code, dist_calculator, vec_bound);
	bounder.SetTimeTracker(time_tracker);

	//SMART_ASSERT(0)("begin_largest_multi");

	bounder.LargestMulti(mat_query, vec_nn_idx);
	//SMART_ASSERT(0)("end_largest_multi").Exit();

	list<int> lst_id;
	list<vector<double> > lst_vec_time_cost_second;
	time_tracker.Report(lst_id, lst_vec_time_cost_second);

	int num_kind = lst_id.size();
	RESULT_INFO = mxCreateCellMatrix(num_kind, 1);

	list<int>::iterator iter_id = lst_id.begin();
	list<vector<double> >::iterator iter_time = lst_vec_time_cost_second.begin();

	for (int i = 0; i < num_kind; i++)
	{
		const char* field_names[] = {"id", "time_cost"};
		mxArray* p = mxCreateStructMatrix(1, 1, 2, field_names);
		mxSetFieldByNumber(p, 0, 0, mxCreateDoubleScalar(*iter_id++));

		const vector<double> &vec_time_cost = *iter_time++;
		mxArray* mx_time_cost = mxCreateDoubleMatrix(vec_time_cost.size(), 1, mxREAL);
		Vector<double> vec2_time_cost;
		mexConvert(mx_time_cost, vec2_time_cost);
		for (int j = 0; j < vec_time_cost.size(); j++)
		{
			vec2_time_cost[j] = vec_time_cost[j];
		}
		mxSetFieldByNumber(p, 0, 1, mx_time_cost);

		mxSetCell(RESULT_INFO, i, p);
	}
}

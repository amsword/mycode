#include <omp.h>
#include "utility.h"
#include "make_mex.h"
#include "TypeConvert.h"

#define DATA_BASE prhs[0]
#define INPUT_CENTERS prhs[1]

#define OUTPUT_IDX plhs[0]
#define OUTPUT_SQ_ERRORS plhs[1]

//compact B
void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 2)(nrhs).Exit();
	SMART_ASSERT(nlhs >= 1)(nlhs).Exit();
	int num_thd = omp_get_num_procs();
	omp_set_num_threads(num_thd);

	SMatrix<double> mat_database;
	SMatrix<double> mat_input_centers;
	//SMatrix<double> mat_output_centers;
	//int num_cluster;
	//int max_iter;
	//double min_error;

	mexConvert(DATA_BASE, mat_database);
	mexConvert(INPUT_CENTERS, mat_input_centers);
	//mexConvert(NUM_CLUSTER, num_cluster);
	//mexConvert(MAX_ITER, max_iter);
	//mexConvert(MIN_ERROR, min_error);

	//OUTPUT_CENTERS = mxCreateDoubleMatrix(mat_database.Cols(), num_cluster, mxREAL);
	//mexConvert(OUTPUT_CENTERS, mat_output_centers);

	//memcpy(mat_output_centers.Ptr(), 
	//mat_input_centers.Ptr(), 
	//sizeof(double) * mat_input_centers.Rows() * mat_input_centers.Cols());

	int num_database_point = mat_database.Rows();
	OUTPUT_IDX = mxCreateDoubleMatrix(num_database_point, 1, mxREAL);
	Vector<double> vec_output_idx;
	mexConvert(OUTPUT_IDX, vec_output_idx);

	OUTPUT_SQ_ERRORS = mxCreateDoubleMatrix(num_database_point, 1, mxREAL);
	Vector<double> vec_output_sq_errors; 
	mexConvert(OUTPUT_SQ_ERRORS, vec_output_sq_errors);

	BruteforceKNNer<double> knn;

#pragma omp parallel for
	for (int i = 0; i < num_database_point; i++)
	{
		const double* p_query = mat_database[i];
		int best_idx;
		double best_error;

		knn.NearestEuclidean(p_query, mat_input_centers, best_idx, best_error);
		vec_output_idx[i] = best_idx + 1;
		vec_output_sq_errors[i] = best_error;
	}



}
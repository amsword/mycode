#pragma once

#include "utility.h"

using namespace utility;

class LocalityOCKMeans
{
public:
	void Initialize(
		SMatrix<double> &mat_raw_database,
		SMatrix<double> &mat_coarse_clusters,
		Vector<SMatrix<short> > &vecmat_ock_codes,
		Vector<int> &vec_is_opt_r,
		Vector<SMatrix<double> > &vecmat_ock_rotations,
		Vector<Vector<SMatrix<double> > > &vecvecmat_ock_centers,
		Vector<Vector<double> > &vecvec_bound,
		Vector<Vector<int> > &vecvec_inverted_idx, 
		int omega,
		double gamma,
		int max_expanded);

	void ANNSearching(const void* query, 
		vector<int> &vec_idx);

private:
	void FilterCoarseCluster(
		const Vector<double> &inner_product, 
		Vector<int> &idx);

	void FilterByBound(
		const double* db_query, 
		const Vector<double> &vec_inner_to_coarse_center,
		const Vector<int> &vec_idx_coarse_center, 
		vector<int> &vec_idx, 
		vector<LessPair<double> > &vec_candidate);

	void ReRanking(
		const double* db_query, 
		vector<LessPair<double> > &vec_candidate, 
		vector<int> &v_idx);

	void RotatedQuery(
		const double *db_query, 
		int idx_coarse_center, 
		Vector<double> &rotated_query);

	void GenerateLookup(
		const Vector<double> &rotated_query, 
		int idx_coarse_center, 
		Vector<Vector<double> > &vecvec_lookup);

	bool ScanInvertedList(
		int idx_coarse_center, 
		double inner_coarse_center, 
		const Vector<Vector<double> > &vecvec_lookup, 
		vector<int> &vec_idx_good, 
		vector<LessPair<double> > &vec_candidate);


private:
	SMatrix<double> *m_pmat_coarse_clusters;
	SMatrix<double> *m_pmatRawDataBase;
	Vector<SMatrix<short> > *m_pvecmat_ock_codes;
	Vector<int> *m_pvec_is_opt_r;
	Vector<SMatrix<double> >* m_pvecmat_ock_rotations;
	Vector<Vector<SMatrix<double> > >* m_pvecvecmat_ock_centers;
	Vector<Vector<double> >* m_pvecvec_bound;
	Vector<Vector<int> >* m_pvecvec_inverted_idx;

	int m_estimated_candidate_buffer;
	int m_omega;
	int m_dim;

	double m_gamma;
	int m_max_expanded;
};

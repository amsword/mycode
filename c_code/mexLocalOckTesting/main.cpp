#include "utility.h"
#include <list>
#include "TypeConvert.h"
#include "LocalityOCKMeans.h"

using namespace std;

typedef short typeCode;

#define VEC_QUERY prhs[0]
#define DISTANCE_INFO prhs[1]

#define GOOD_IDX plhs[0]
#define TIME_COST plhs[1]

using namespace utility;


void ConvertAll(
	const mxArray *dist_info, 
	SMatrix<double> &mat_raw_database,
	SMatrix<double> &mat_coarse_clusters,
	Vector<SMatrix<short> > &vecmat_ock_codes,
	Vector<int> &vec_is_opt_r,
	Vector<SMatrix<double> > &vecmat_ock_rotations,
	Vector<Vector<SMatrix<double> > > &vecvecmat_ock_centers,
	Vector<Vector<double> > &vecvec_bound,
	Vector<Vector<int> > &vecvec_inverted_idx, 
	int &omega,
	double &gamma,
	int &max_expanded)
{
	mxArray* p;

	p = mxGetField(dist_info, 0, "raw_database");
	mexConvert(p, mat_raw_database);

	p = mxGetField(dist_info, 0, "coarse_centers");
	mexConvert(p, mat_coarse_clusters);

	p = mxGetField(dist_info, 0, "all_ock_codes");
	mexConvert(p, vecmat_ock_codes);

	p = mxGetField(dist_info, 0, "all_ock_model");
	int num_coarse_centers = vecmat_ock_codes.Size();
	vec_is_opt_r.AllocateSpace(num_coarse_centers);
	vecmat_ock_rotations.AllocateSpace(num_coarse_centers);
	vecvecmat_ock_centers.AllocateSpace(num_coarse_centers);
	for (int i = 0; i < num_coarse_centers; i++)
	{
		mxArray* p1 = mxGetCell(p, i);
		mexConvert(mxGetField(p1, 0, "is_optimize_R"), 
			vec_is_opt_r[i]);
		mexConvert(mxGetField(p1, 0, "R"), 
			vecmat_ock_rotations[i]);
		mexConvert(mxGetField(p1, 0, "all_D"), 
			vecvecmat_ock_centers[i]);
	}

	mexConvert(mxGetField(dist_info, 0, "all_ock_bound"), vecvec_bound);

	mexConvert(mxGetField(dist_info, 0, "all_inverted_idx"), vecvec_inverted_idx);

	mexConvert(mxGetField(dist_info, 0, "omega"), omega);

	mexConvert(mxGetField(dist_info, 0, "gamma"), gamma);

	mexConvert(mxGetField(dist_info, 0, "max_expanded"), max_expanded);

}


/*
* DistanceInfo{0} = type:
* 	if type == 0 exhuasitive search, raw data;
DistanceInfo{1} = Database;
if type == 1 // exact search based on linear scan, ck-means;
DistanceInfo{1} = is_opt_r;
DistanceInfo{2} = R;
DistanceInfo{3} = all_D;
DistanceInfo{4} = num_dic_each_partition;
if type == 2
*/
void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
{
	SMART_ASSERT(nrhs == 2)(nrhs).Exit();
	SMART_ASSERT(nlhs == 2).Exit();

	Vector<double> vec_query;
	SMatrix<double> mat_coarse_clusters;

	SMatrix<double> mat_raw_database;
	Vector<SMatrix<short> > vecmat_ock_codes;
	Vector<int> vec_is_opt_r;
	Vector<SMatrix<double> > vecmat_ock_rotations;
	Vector<Vector<SMatrix<double> > > vecvecmat_ock_centers;
	Vector<Vector<double> > vecvec_bound;
	Vector<Vector<int> > vecvec_inverted_idx;
	vector<int> vec_idx;
	int omega;
	double gamma;
	int max_expanded;

	mexConvert(VEC_QUERY, vec_query);

	ConvertAll(DISTANCE_INFO, 
		mat_raw_database,
		mat_coarse_clusters,
		vecmat_ock_codes,
		vec_is_opt_r,
		vecmat_ock_rotations,
		vecvecmat_ock_centers,
		vecvec_bound,
		vecvec_inverted_idx, 
		omega,
		gamma,
		max_expanded);

	LocalityOCKMeans local_ock;
	local_ock.Initialize(
		mat_raw_database,
		mat_coarse_clusters,
		vecmat_ock_codes,
		vec_is_opt_r,
		vecmat_ock_rotations,
		vecvecmat_ock_centers,
		vecvec_bound,
		vecvec_inverted_idx, 
		omega,
		gamma,
		max_expanded);


	/*SMART_ASSERT(0)
	(mat_coarse_clusters.Rows())(mat_coarse_clusters.Cols())
	(vecmat_ock_codes.Size())
	(vecmat_ock_codes[5].Rows())(vecmat_ock_codes[5].Cols())
	(vec_is_opt_r.Size())
	(vecmat_ock_rotations.Size())
	(vecmat_ock_rotations[6].Rows())(vecmat_ock_rotations[6].Cols())
	(vecvecmat_ock_centers.Size())
	(vecvecmat_ock_centers[8].Size())
	(vecvecmat_ock_centers[8][0].Rows())
	(vecvecmat_ock_centers[8][0].Cols())
	(vecvec_bound.Size())
	(vecvec_bound[0].Size())
	(vecvec_inverted_idx.Size())
	(vecvec_inverted_idx[0].Size())
	(omega)(gamma)(max_expanded).Exit();*/

	clock_t begin = clock();
	local_ock.ANNSearching(vec_query.Ptr(), vec_idx);
	clock_t end = clock();

	GOOD_IDX = mxCreateDoubleMatrix(vec_idx.size(), 1, mxREAL);
	Vector<double> vec_good;
	mexConvert(GOOD_IDX, vec_good);
	for (int i = 0; i < vec_idx.size(); i++)
	{
		vec_good[i] = vec_idx[i] + 1;
	}

	TIME_COST = mxCreateDoubleScalar((end - begin) / (double)CLOCKS_PER_SEC);
}

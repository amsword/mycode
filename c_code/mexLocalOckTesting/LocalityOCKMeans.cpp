
#include "LocalityOCKMeans.h"


void LocalityOCKMeans::FilterCoarseCluster(
	const Vector<double> &inner_product, 
	Vector<int> &idx)
{
	Heap<LessPair<double> > heap;
	heap.Reserve(m_omega);

	int i;
	for (i = 0; i < m_omega; i++)
	{
		heap.insert(LessPair<double>(i, abs(inner_product[i])));
	}
	int num_coarse_center = inner_product.Size();

	for (; i < num_coarse_center; i++)
	{
		const LessPair<double> &node = heap.Top();
		double curr_product = abs(inner_product[i]);

		if (node.distance < curr_product)
		{
			heap.popMin();
			heap.insert(LessPair<double>(i, curr_product));
		}
	}

	LessPair<double> v;
	idx.AllocateSpace(m_omega);
	for (i = 0; i < m_omega; i++)
	{
		heap.popMin(v);
		idx[m_omega - 1 - i] = v.index;
	}
}

void LocalityOCKMeans::RotatedQuery(
	const double *db_query, 
	int idx_coarse_center, 
	Vector<double> &rotated_query)
{
	rotated_query.AllocateSpace(m_dim);
	if ((*m_pvec_is_opt_r)[idx_coarse_center])
	{
		(*m_pvecmat_ock_rotations)[idx_coarse_center].Multiple(db_query, rotated_query.Ptr());
	}
	else
	{
		memcpy(rotated_query.Ptr(), db_query, sizeof(double) * m_dim);
	}
}


void LocalityOCKMeans::GenerateLookup(
	const Vector<double> &rotated_query, 
	int idx_coarse_center, 
	Vector<Vector<double> > &vecvec_lookup)
{
	const Vector<SMatrix<double> > &vecmat_ock_centers = 
		(*m_pvecvecmat_ock_centers)[idx_coarse_center];

	int idx_dim_start = 0;
	int num_partitions = vecmat_ock_centers.Size();
	vecvec_lookup.AllocateSpace(num_partitions);

	const double* p_sub_query = rotated_query.Ptr();

	for (int i = 0; i < num_partitions; i++)
	{
		const SMatrix<double> &mat_ock_centers = vecmat_ock_centers[i];
		int num_centers = mat_ock_centers.Rows();
		int center_dim = mat_ock_centers.Cols();

		vecvec_lookup[i].AllocateSpace(num_centers);

		mat_ock_centers.Multiple(p_sub_query, vecvec_lookup[i].Ptr());
		p_sub_query += center_dim;
	}
}

bool LocalityOCKMeans::ScanInvertedList(
	int idx_coarse_center, 
	double inner_coarse_center, 
	const Vector<Vector<double> > &vecvec_lookup, 
	vector<int> &vec_idx_good, 
	vector<LessPair<double> > &vec_candidate)
{
	const Vector<int> &vec_inverted_idx = (*m_pvecvec_inverted_idx)[idx_coarse_center];
	const SMatrix<short> &mat_ock_codes = (*m_pvecmat_ock_codes)[idx_coarse_center];
	const Vector<double> &vec_bound = (*m_pvecvec_bound)[idx_coarse_center];

	int num_partition = vecvec_lookup.Size();

	int inverted_list_size = vec_inverted_idx.Size();
	for (int i = 0; i < inverted_list_size; i++)
	{
		int idx_point = vec_inverted_idx[i];
		const short* p_codes = mat_ock_codes[i];

		double s = inner_coarse_center;
		for (int j = 0; j < num_partition; j++)
		{
			s += vecvec_lookup[j][p_codes[j]];
		}

		s = s >= 0 ? s : -s;
		double bound = vec_bound[i];
		if (s - bound > m_gamma)
		{
			vec_idx_good.push_back(idx_point);
			if (vec_idx_good.size() == m_max_expanded)
			{
				return true;
			}
		}
		else if (s + bound > m_gamma)
		{
			vec_candidate.push_back(LessPair<double>(idx_point, s));
		}
	}
	return false;
}

void LocalityOCKMeans::Initialize(
	SMatrix<double> &mat_raw_database,
	SMatrix<double> &mat_coarse_clusters,
	Vector<SMatrix<short> > &vecmat_ock_codes,
	Vector<int> &vec_is_opt_r,
	Vector<SMatrix<double> > &vecmat_ock_rotations,
	Vector<Vector<SMatrix<double> > > &vecvecmat_ock_centers,
	Vector<Vector<double> > &vecvec_bound,
	Vector<Vector<int> > &vecvec_inverted_idx, 
	int omega,
	double gamma,
	int max_expanded)
{
	m_pmatRawDataBase = &mat_raw_database;
	m_pmat_coarse_clusters = &mat_coarse_clusters;
	m_pvecmat_ock_codes = &vecmat_ock_codes;
	m_pvec_is_opt_r = &vec_is_opt_r;
	m_pvecmat_ock_rotations = &vecmat_ock_rotations;
	m_pvecvecmat_ock_centers = &vecvecmat_ock_centers;
	m_pvecvec_bound = &vecvec_bound;
	m_pvecvec_inverted_idx = &vecvec_inverted_idx;
	m_omega = omega;
	m_gamma = gamma;
	m_max_expanded = max_expanded;

	m_dim = m_pmat_coarse_clusters->Cols();

	int total_point = 0;
	for (int i = 0; i < m_pmat_coarse_clusters->Rows(); i++)
	{
		total_point += m_pvecvec_inverted_idx->operator[](i).Size();
	}
	m_estimated_candidate_buffer = total_point * 3 / m_omega;
}


void LocalityOCKMeans::FilterByBound(
	const double* db_query, 
	const Vector<double> &vec_inner_to_coarse_center,
	const Vector<int> &vec_idx_coarse_center, 
	vector<int> &vec_idx, 
	vector<LessPair<double> > &vec_candidate)
{
	vec_idx.reserve(m_max_expanded);
	vec_candidate.reserve(m_estimated_candidate_buffer);

	for (int i = 0; i < m_omega; i++)
	{
		int idx_coarse_center = vec_idx_coarse_center[i];

		Vector<double> rotated_query;
		RotatedQuery(db_query, idx_coarse_center, rotated_query);

		Vector<Vector<double> > vecvec_lookup;
		GenerateLookup(rotated_query, idx_coarse_center, vecvec_lookup);

		bool is_enough = ScanInvertedList(
			idx_coarse_center, 
			vec_inner_to_coarse_center[idx_coarse_center], 
			vecvec_lookup, 
			vec_idx, 
			vec_candidate);

		if (is_enough)
		{
			break;
		}
	}
}

void LocalityOCKMeans::ReRanking(
	const double* db_query, 
	vector<LessPair<double> > &vec_candidate, 
	vector<int> &vec_idx)
{
	sort(vec_candidate.begin(), vec_candidate.end());

	for (int i = vec_candidate.size() - 1; i >= 0; i--)
	{
		LessPair<double> &node = vec_candidate[i];

		double s = dot(m_pmatRawDataBase->operator[](node.index), 
			db_query, m_dim);
		s = s > 0 ? s : -s;
		if (s > m_gamma)
		{
			vec_idx.push_back(node.index);
			if (vec_idx.size() >= m_max_expanded)
			{
				break;
			}
		}
	}
}

void LocalityOCKMeans::ANNSearching(
	const void* query, 
	vector<int> &vec_idx)
{
	const double* db_query = (const double*)query;

	Vector<double> vec_inner_to_coarse_center;
	vec_inner_to_coarse_center.AllocateSpace(m_pmat_coarse_clusters->Rows());

	m_pmat_coarse_clusters->Multiple(db_query, vec_inner_to_coarse_center.Ptr());

	Vector<int> vec_idx_coarse_center;
	FilterCoarseCluster(vec_inner_to_coarse_center, vec_idx_coarse_center);

	vector<LessPair<double> > vec_candidate;

	FilterByBound(db_query, vec_inner_to_coarse_center, vec_idx_coarse_center, vec_idx, vec_candidate);

	if (vec_idx.size() >= m_max_expanded)
	{
		SMART_ASSERT(0)("no need to re-ranking");
		return;
	}

	SMART_ASSERT(0)(vec_idx.size());

	ReRanking(db_query, vec_candidate, vec_idx);


}

#include <iostream>
#include <algorithm>
using namespace std;

void sort_left_right(const float* prob, 
					 float* gt_info, const int num_gt,
					 int num_label)  
{
	// sort the result
	sort(gt_info, gt_info + num_gt, 
		[prob](float left, float right) {
			return prob[(int)left] > prob[(int)right];
	});
	sort(gt_info + num_gt, gt_info + num_label,
		[prob](float left, float right) {
			return prob[(int)left] > prob[(int)right];
	});
}

void find_largest_i(const float* prob, const float* gt_info, const int num_gt, 
					const int num_label, const float epsilon, 
					int &largest_i) 
{
	// find the largest index of i and the candidate largest of s
	largest_i = -1;
	for (int i = num_gt - 1; i >= 0; i--) {
		float epsilon_p = prob[(int)gt_info[i]];
		float p = epsilon_p / epsilon;
		float left = 0;
		for (int j = i + 1; j < num_gt; j++) {
			left += log(epsilon_p / max(prob[(int)gt_info[j]], 0.0000001f));
		}
		float right = 0;
		for (int s = num_gt; s < num_label; s++) {
			float ps = prob[(int)gt_info[s]];
			if (ps > p) {
				right += log(ps / max(p, 0.0000001f));
			} else {
				break;
			}
		}
		right /= epsilon;
		if (left >= right) {
			largest_i = i;
			break;
		}
	}
}

void refine_smallest_t(const float* prob, const float* gt_info,
					   int num_gt, int num_label, float epsilon, 
					   int &smallest_t) 
{
	smallest_t = num_label;
	int t;
	for (t = num_gt; t < num_label; t++) {
		float p = prob[(int)gt_info[t]];
		float epsilon_p = epsilon * p;
		float right = 0;
		for (int s = num_gt; s < t; s++) {
			float ps = prob[(int)gt_info[s]];
			right += log(ps / max(p, 0.00000001f));
		}
		right /= epsilon;
		float left = 0;
		for (int j = num_gt - 1; j >= 0; j--) {
			float pj = prob[(int)gt_info[j]];
			if (epsilon_p > pj) {
				left += log(epsilon_p / pj);
			} else {
				break;
			}
		}
		if (right >= left) {
			smallest_t = t;
			break;
		}
	}
}

bool check(const float* prob, const float* gt_info, const int largest_i, const int num_gt,  
		   const int smallest_t, const int num_label, float p, float epsilon) 
{
	bool is_correct = true;
	float left = 0;
	float epsilon_p = epsilon * p;
	for (int j = largest_i + 1; j < num_gt; j++) {
		float pj = prob[(int)gt_info[j]];
		left += log(epsilon_p / max(pj, 0.0000001f));
		if (pj > epsilon_p) {
			is_correct = false;
			break;
		}
	}
	if (is_correct) {
		for (int i = 0; i <= largest_i; i++) {
			float pi = prob[(int)gt_info[i]];
			if (pi < epsilon_p) {
				is_correct = false;
				break;
			}
		}
	}
	float right = 0;
	for (int s = num_gt; s < smallest_t; s++) {
		float ps = prob[(int)gt_info[s]];
		right += log(ps / max(p, 0.00000001f));
		if (ps < p) {
			is_correct = false;
			break;
		}
	}
	for (int t = smallest_t; t < num_label; t++) {
		float pt = prob[(int)gt_info[t]];
		if (pt > p) {
			is_correct = false;
		}
	}
	right /= epsilon;
	//cout << "checking: " << left << "," << right << ":" 
	//	<< (left - right) / (left + right) << "\n";
	return is_correct;
}

bool compute_reall_p(const float* prob, const float* gt_info, 
					 float epsilon, int num_gt, int num_label, 
					 const int largest_i, 
					 const int smallest_t, float &p)
{
	float x = 0;
	for (int s = num_gt; s < smallest_t; s++) {
		x += log(max(prob[(int)gt_info[s]], 0.00000001f));
	}
	x /= epsilon;
	for (int j = largest_i + 1; j < num_gt; j++ ) {
		x += log(max(prob[(int)gt_info[j]], 0.00000001f));
	}
	x -= (num_gt - largest_i - 1) * log(epsilon);
	float y = (num_gt - largest_i - 1) + (smallest_t - num_gt) / epsilon;
	p = exp(x / y);
	return check(prob, gt_info, largest_i, num_gt, smallest_t, num_label, p, epsilon);
}

void thresholding_prob(const float* prob, const float* gt_info, 
					   int largest_i, int num_gt, int smallest_t, 
					   int num_label, float epsilon, float p, 
					   float* target_prob) 
{
	for (int i = 0; i <= largest_i; i++) {
		target_prob[(int)gt_info[i]] = prob[(int)gt_info[i]];
	}
	for (int t = smallest_t; t < num_label; t++) {
		target_prob[(int)gt_info[t]] = prob[(int)gt_info[t]];
	}
	float epsilon_p = epsilon * p;
	for (int j = largest_i + 1; j < num_gt; j++) {
		target_prob[(int)gt_info[j]] = epsilon_p;
	}
	for (int s = num_gt; s < smallest_t; s++) {
		target_prob[(int)gt_info[s]] = p;
	}
}

void normalize_prob(float* target_prob, const float* gt_info, int num_label) {
	// normalization
	float s = 0;
	for (int i = 0; i < num_label; i++) {
		s += target_prob[(int)gt_info[i]];
	}
	for (int i = 0; i < num_label; i++) {
		target_prob[(int)gt_info[i]] /= s;
	}
}

bool generate_target(const float* prob, float* gt_info, const int num_gt,
					 const int num_label, const float epsilon,
					 float* target_prob) 
{
	//CHECK_EQ(step, 1);
	//CHECK_GT(num_label, 0);
	//CHECK_LE(num_gt, num_label);
	//CHECK_GE(num_gt, 0);
	//CHECK_GT(epsilon, 1);
	if (num_gt == 0 || num_gt == num_label) 
	{
		for (int i = 0; i < num_label; i++) {
			target_prob[i] = prob[i];
		}
		return false;
	}
	sort_left_right(prob, gt_info, num_gt, num_label);
	int largest_i;
	find_largest_i(prob, gt_info, num_gt, num_label, epsilon, 
		largest_i);
	// refine largest index of s
	int smallest_t;
	refine_smallest_t(prob, gt_info, num_gt, 
		num_label, epsilon, smallest_t);
	float p;
	bool is_correct = compute_reall_p(prob, gt_info, epsilon, num_gt, num_label, largest_i, smallest_t, p);
	// compute the estimated prob without normalization
	thresholding_prob(prob, gt_info, largest_i, num_gt, smallest_t, num_label, epsilon, 
		p, target_prob);
	normalize_prob(target_prob, gt_info, num_label);

	//if (! is_correct) {
	//	cout << "error\n";
	//	exit(0);
	//}
	return smallest_t < num_label - 1;
}

int main()
{
	while(1);
	const int num_label = 10;
	// initialize the probe
	float prob[num_label];
	bool is_good;
	float target_prob[num_label];
	for (int seed = 1; seed < 100; seed++) {
		srand(seed);
		for (int i = 0; i < num_label; i++) {
			prob[i] = rand();
			//prob[i] = num_label - i;
		}

		// initialize the gt_info
		float gt_info[num_label];
		int step = 1;
		for (int i = 0; i < num_label; i++) {
			gt_info[i] = i * step;
		}
		normalize_prob(prob, gt_info, num_label);

		for (int num_gt = 1; num_gt < 9; num_gt++) {
			cout << seed << "\t" << num_gt << "\n";
			generate_target(prob, gt_info, num_gt, num_label, 2.0, target_prob);
		}
	}
	return 0;
}
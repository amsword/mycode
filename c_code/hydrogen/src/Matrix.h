#pragma once

#include "SyncedMemory.h"



template <class T>
class Matrix
{
public:
	Matrix();
	Matrix(size_t rows, size_t cols);
public:
	void resize(size_t rows, size_t cols);
	void attach_to_cpu(T* p, size_t rows, size_t cols);

public:
	const T* cpu_ptr(size_t row_offset = 0, size_t rows = 0);
	T* mutable_cpu_ptr(size_t row_offset = 0, size_t rows = 0);
	const T& get(int idx_row, int idx_col)
	{
		return *((const T*)(m_memory.cpu_data()) + idx_row * m_cols + idx_col);
	}

	void set(int idx_row, int idx_col, T v)
	{
		*((T*)(m_memory.mutable_cpu_data()) + idx_row * m_cols + idx_col) = v;
	}
	//Type* gpu_data(size_t offset = 0, size_t rows = 0);
	//Type* mutable_gpu_data(size_t offfset, size_t rows = 0);
	size_t rows();
	size_t cols();
	size_t size();

private:
	SyncedMemory m_memory;

	size_t m_rows;
	size_t m_cols;
};




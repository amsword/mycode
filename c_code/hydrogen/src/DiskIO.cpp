#include "DiskIO.h"	


void DiskIO::open_read(const string &file_name, FILE* &fp)
{
	fp = fopen(file_name.c_str(), "rb");
	SMART_ASSERT(fp)(file_name).Fatal();
}
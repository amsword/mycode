#include <cstdlib>
#include "Assert.h"
#include "utility_global.h"

Assert::Assert() : SMART_ASSERT_A(*this),
	SMART_ASSERT_B(*this)
{

}

Assert& Assert::print_context(const char* exp)
{
	*GetGlobalLogger() << "Failed: " << exp << "\n";
	return *this;
}

void Assert::Fatal()
{
	(*GetGlobalLogger()).Flush();
	(*GetGlobalLogger()).CloseFile();
	//throw -1;
	exit(-1);
}

void Assert::Info()
{
	(*GetGlobalLogger()).Flush();
	(*GetGlobalLogger()).CloseFile();
}

Assert make_assert(const char *file_name, int line_number)
{
	*GetGlobalLogger() << file_name << ", " << line_number << ": ";
	return Assert();
}

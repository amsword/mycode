#pragma once
#include "encoder.h"

class EncoderTestSet
{
public:
	static bool create_test_set(Matrix<DataType> &mat_data, 
		shared_ptr<IEncoder> &encoder, 
		Matrix<CodeType> &mat_code);

	static bool create_training_set(Matrix<DataType> &mat_data, 
		shared_ptr<IEncoder> &encoder, 
		shared_ptr<IEncoderModel> &encoder_model);

private:
	static void create_test_set_g1(Matrix<DataType> &mat_data, 
		shared_ptr<IEncoder> &encoder, 
		Matrix<CodeType> &mat_code);

	static void create_test_set_in_ock(Matrix<DataType> &mat_data, 
		shared_ptr<IEncoder> &encoder, 
		Matrix<CodeType> &mat_code);

	static void create_test_set_g2(Matrix<DataType> &mat_data, 
		shared_ptr<IEncoder> &encoder, 
		Matrix<CodeType> &mat_code);
};

#pragma once
#include "DllExportHeader.h"
#include <iostream>
#include "Assert.h"


//------------------------------------------------------------------------
 //#define SMART_ASSERT(expr) if( (expr) ) ; else make_assert( #expr).print_context(__FILE__,__LINE__).SMART_ASSERT_A
#define SMART_ASSERT(expr) if( (expr) ) ; else make_assert(__FILE__, __LINE__).print_context(#expr).SMART_ASSERT_A

#define SMART_ASSERT_A(x) SMART_ASSERT_OP(x,B)
#define SMART_ASSERT_B(x) SMART_ASSERT_OP(x,A)
#define SMART_ASSERT_OP(x,next) SMART_ASSERT_A.print_current_val((x),#x).SMART_ASSERT_##next

#define NOT_READY SMART_ASSERT(0)("not ready").Fatal()

#define PRINT (*GetGlobalLogger()) << __FILE__ << ": Line:" << __LINE__ << "\t"

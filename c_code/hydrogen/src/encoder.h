#pragma once

typedef double DataType;
typedef unsigned char CodeType;

#include "Matrix.h"


class IEncoderModel
{
public:
	virtual void CheckDifference(shared_ptr<IEncoderModel> &ref);
};

class IEncoder
{
public:
	virtual void training(Matrix<DataType> &mat) = 0;
	virtual void encoding(Matrix<DataType> &mat, Matrix<CodeType> &code) = 0;
	virtual void export_model(shared_ptr<IEncoderModel> &pmodel);
};

struct OptOption
{
	int max_iter;
	bool is_display_loss;
	//double min_error; // note, sometimes, the obj does not decrease, but it is not bad
};

enum RVQMethod
{
	Group1,
	Group2, 
	In_AQ,
	In_OCK
};

class NearestResidualAlg
{
public:
	NearestResidualAlg(RVQMethod method);

	void cpu_nearest(const DataType* p_curr_sample, 
		Matrix<DataType> &quantizers, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);
private:
	void cpu_nearest(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);
	void cpu_nearest(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		CodeType* p_curr_code);
	void cpu_nearest_g1(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);
	bool cpu_nearest_g1_scan(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code, int idx_dic);
	void cpu_nearest_g2(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);
	bool cpu_nearest_g2(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code, int idx_dic_a, int idx_dic_b);
	void PrecomputeA(
		int idx_dic_a, int idx_dic_b, 
		Matrix<DataType> &vec_x_map, 
		Matrix<DataType> &matPrecomputed, 
		int num_sub_dic_partition, 
		CodeType* prepresentation, 
		Matrix<double> &vec_pre_a, const int* quantizer_offset);
	void cpu_nearest_in_aq(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);

protected: // relative loss
	DataType cpu_residual_loss(Matrix<DataType> &vec_x_map, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		const CodeType* p_curr_code);

protected: // in ock
	int cpu_nearest_in_ock(Matrix<DataType> &x_ip_q, 
		Matrix<DataType> &quan_ip, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		CodeType* p_curr_code);
	void NearestResidualAlg::SolveOptimizedAdv2Recursive(
		Matrix<DataType> &vec_x_map, 
		Matrix<DataType> &matInnerProduct, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		int idx, 
		CodeType* prepresentation, 
		DataType &error);
	void BestNextWordsSMart(
		Matrix<DataType> &vec_x_map, 
		Matrix<DataType> &matInnerProduct, 
		const int* quantizer_offset, 
		int num_sub_dic, 
		const CodeType* prepresentation,
		int idx, 
		CodeType next_idx[], 
		DataType next_errors[]);
private:
	RVQMethod m_rvqmethod;
	bool m_is_use_pre;

	union
	{
		int num_candidate; // used in ock and aq
	} m_para;

};

class VQEncoderModel : public IEncoderModel
{
public:
	VQEncoderModel();
	VQEncoderModel(shared_ptr<Matrix<DataType> > &pmat_rotation, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_dics, 
		shared_ptr<Matrix<int> > &pmat_quantizer_partition);
public:
	virtual void CheckDifference(shared_ptr<IEncoderModel> &ref);
	void Set(shared_ptr<Matrix<DataType> > &pmat_rotation, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_dics, 
		shared_ptr<Matrix<int> > &pmat_quantizer_partition);
private:
	shared_ptr<Matrix<DataType> > m_pmat_rotation;
	shared_ptr<vector<Matrix<DataType> > > m_pvecmat_dics;
	shared_ptr<Matrix<int> > m_pmat_quantizer_partition;
};


class VQEncoder : public IEncoder
{
public:
	VQEncoder(RVQMethod method = Group1);
	//VQEncoder(int num_partition, int num_sub_dic, int sub_dic_size);
	VQEncoder(shared_ptr<Matrix<DataType> > &pmat_rotate, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
		int num_sub_dic, RVQMethod method = Group1, int iter = 100);

public: // parameter setttings 
	void initialize(shared_ptr<Matrix<DataType> > &pmat_rotate, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
		int num_sub_dic);

	void set_max_iteration(int iter);

public:
	virtual void training(Matrix<DataType> &mat_data);
	virtual void encoding(Matrix<DataType> &mat_data, 
		Matrix<CodeType> &mat_code);
	virtual void export_model(shared_ptr<IEncoderModel> &pmodel);

protected:
	void infer_quantizer_partition(shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
		int num_sub_dic);

protected:
	virtual void update_codebook(Matrix<DataType> &mat_data, 
		Matrix<CodeType> &mat_code);
protected:
	void update_quantizers(Matrix<CodeType> &mat_code);
	void update_rotation(Matrix<DataType> &mat_data, Matrix<CodeType> &mat_code);

protected:
	void quantizer_recover(Matrix<DataType> &mat_data, 
		Matrix<CodeType> &mat_code, Matrix<DataType> &mat_recovered);
	void update_quantizers(
		int data_dim_start, int data_dim_end, 
		Matrix<CodeType> &mat_code, int code_dim_start, int code_dim_end, 
		const int* p_curr_quantizer_partition, 
		Matrix<DataType> & mat_quantizers);

protected:
	void cpu_rotating(Matrix<DataType> &mat_data, Matrix<DataType>* &p_rotated);
	void cpu_nearest(Matrix<DataType>* p_rotated, Matrix<CodeType> &mat_code);
	//void gpu_encoding(Matrix<DataType>* p_rotated, Matrix<CodeType> &mat_code);
	void cpu_quantizers_inner_product();

private:
	OptOption m_opt_option;

private:
	// set in the initialization
	shared_ptr<Matrix<int> > m_pmat_data_partition; // [i] = start index of the i-th partition in the original space.
	shared_ptr<Matrix<int> > m_pmat_code_partition; // [i] = start index of the i-th partition for the binary code. 

	// set in the training or encoding
	Matrix<DataType>* m_pmat_rotated;
	shared_ptr<Matrix<DataType> > m_pmat_aux;
	vector<Matrix<DataType> > m_vecmat_quantizers_inner_product;
	NearestResidualAlg m_residual_alg;
	bool m_is_rotate;

protected:
	// model parameters. 
	shared_ptr<Matrix<DataType> > m_pmat_rotation;
	shared_ptr<vector<Matrix<DataType> > > m_pvecmat_quantizers;
	shared_ptr<Matrix<int> > m_pmat_quantizer_partition; // [i][j] = i-th partition, j-th sub dic to generate the j-th code in the i-th partition 

};

// hierarchical encoder;
class HierarchicalEncoder : public VQEncoder
{
public:
	HierarchicalEncoder(int num_partitions, int num_sub_dic, 
		int sub_dic_size, 
		shared_ptr<VQEncoder> pvqencoder);
	void set_start_initialize(shared_ptr<Matrix<DataType> > &pmat_rotation, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers); 
public:
	//void initialize(shared_ptr<Matrix<DataType> > &pmat_rotation, 
	//	shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers);
	virtual void training(Matrix<DataType> &mat_data);

protected:
	void construct_codebooks(
		shared_ptr<Matrix<DataType> > &pmat_rotation,
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_in, 
		shared_ptr<vector<Matrix<DataType> > > &pvecmat_out);

private:
	shared_ptr<Matrix<DataType> > m_pmat_init_rotation;
	shared_ptr<vector<Matrix<DataType> > > m_pvecmat_init_quantizers;

protected:
	shared_ptr<VQEncoder> m_pvqencoder;
	int m_num_partitions;
	int m_num_sub_dic;
	int m_sub_dic_size;
};

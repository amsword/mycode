#include "Criterion.h"

namespace utility
{
	void Criterion::BeginEvaluate()
	{
	}

	void Criterion::Evaluate(const SMatrix<int> &matRetrievedIndex, int idx_begin, int idx_end)
	{
		SMART_ASSERT(0).Fatal();
	}

	void Criterion::EndEvaluate()
	{
	}
}

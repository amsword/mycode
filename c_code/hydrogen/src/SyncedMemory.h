#pragma once

#include <vector>
#include <memory>
using namespace std;


//----------------------------------------------------
// this is the interface
class Memory
{
public:
	Memory();
	virtual ~Memory();

public:
	virtual void resize(size_t count) = 0;
	virtual void release();
	void attach(void* ptr, size_t count);
	virtual void* data();
	size_t size();

protected:
	void* m_ptr;
	size_t m_count;
	bool m_is_own;
};

class CPUMemory : public Memory
{
public:
	virtual void resize(size_t count);
	virtual void release();
};

class GPUMemory : public Memory
{
public:
	virtual void resize(size_t count);
	virtual void release();
};

//---------------------------------------------------------------------

/*
if we need to write the content of matrix in one gpu. it is assumed that, there
is no intersection among different gpus. That is to say, if we write the bytes 
in one gpu, we do not need to check the status in the other gpus. 
*/

// solve the problem of how to sync 
// memory between one cpu and multiple gpu(s)
// not fully implemented now
class SyncedMemory
{
public:
	SyncedMemory();

public:
	void resize(size_t count);
	void attach_to_cpu(void* p, size_t count);

public:
	const void* cpu_data(size_t offset = 0, size_t count = 0);
	void* mutable_cpu_data(size_t offset = 0, size_t count = 0);
	//const void* gpu_data(size_t offset = 0, size_t count = 0);
	//void* mutable_gpu_data(size_t offset = 0, size_t count = 0);

private:
	vector<shared_ptr<Memory> > m_vec_memory;
	
	size_t m_count; // how many bytes the memory manages
};

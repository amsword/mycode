#pragma once
#include "Matrix.h"
#include "mkl.h"
#include "mkl_lapacke.h"
class MatCompute
{
public:
	//template <class T>
	//static void cpu_multiple_a_bt(
	//	Matrix<T> &mat_a, Matrix<T> &mat_b, Matrix<T> &mat_out);

	template <class T>
	static void cpu_multiple_a_bt(
		const T* mat_a, const T* mat_b, T* mat_out, int a_rows, int b_cols, int a_cols);

	template <class T>
	static void cpu_multiple_at_b(const T* mat_a, const T* mat_b, T* mat_out, int a_rows, int a_cols, int b_cols);

	template <class T>
	static void cpu_multiple_a_b(const T* mat_a, const T* mat_b, T* mat_out, int a_rows, int a_cols, int b_cols);

	// matA = U * Sigma * Vt. U: a_rows * k; Vt: k * a_cols; psigmas has at least k elements. k = min(a_rows, a_cols)
	template <class T>
	static void svd(T* matA, T* pmatU, T* pmatVt, T* psigmas, int a_rows, int a_cols);

	template <class T>
	static void cpu_xpy(T* y, const T* x, size_t num)
	{
		for (size_t i = 0; i < num; i++)
		{
			y[i] += x[i];
		}
	}

	template <class T>
	static void cpu_ax(T* x, T value, size_t num)
	{
		for (size_t i = 0; i < num; i++)
		{
			x[i] *= value;
		}
	}

	template <class T>
	// a is symmetric positive-definite. 
	static void linear_system_sax_b(T* pa, T* pb, int M_x, int N_x) {}

	template <class T>
	static void linear_system_ax_b(T* pa, T* pb, int M_x, int N_x) {}

	template <class T>
	static T L1_distance(const T* pa, const T* pb, size_t num)
	{
		T s = 0;
		for (size_t i = 0; i < num; i++)
		{
			T diff = pa[i] - pb[i];
			s += diff > 0 ? diff : -diff;
		}
		return s;
	}
	template <class T>
	static T L1(const T* pa, size_t num)
	{
		T s = 0;
		for (size_t i = 0; i < num; i++)
		{
			s += pa[i] > 0 ? pa[i] : -pa[i];
		}
		return s;
	}
};

//template <class T>
//void MatCompute::linear_system_sax_b(T* pa, T* pb, int M_x, int N_x)
//{
//}
//template <>
//void MatCompute<float>::linear_system_sax_b(float* pa, float* pb, int M_x, int N_x)
//{
//
//}
//
template<>
inline void MatCompute::linear_system_sax_b<double>(double* pa, double* pb, int M_x, int N_x)
{
	Matrix<int> vec_aux;
	vec_aux.resize(1, M_x);
	int* ipiv = vec_aux.mutable_cpu_ptr();


	LAPACKE_dsytrf(LAPACK_ROW_MAJOR, 'U', M_x, pa, M_x, ipiv);
	LAPACKE_dsytrs(LAPACK_ROW_MAJOR, 'U', M_x, N_x, 
		pa, M_x, ipiv, pb, N_x);
	/*LAPACKE_dgesv( LAPACK_ROW_MAJOR, M_x, N_x, pa, M_x, ipiv,
	pb, N_x);*/
}

template<>
inline void MatCompute::linear_system_ax_b<double>(double* pa, double* pb, int M_x, int N_x)
{
	Matrix<int> vec_aux;
	vec_aux.resize(1, M_x);
	int* ipiv = vec_aux.mutable_cpu_ptr();

	LAPACKE_dgesv( LAPACK_ROW_MAJOR, M_x, N_x, pa, M_x, ipiv,
		pb, N_x);
}


template <class T>
void MatCompute::cpu_multiple_a_bt(const T* pa, 
								   const T* pb, T* pout, int out_rows, int out_cols, int dim)
{
	long long out_length = out_rows * out_cols;

#pragma omp parallel for
	for (long long i = 0; i < out_length; i++)
	{
		int idx_row = (i / out_cols);
		int idx_col = (i % out_cols);
		T s = 0;
		for (int k = 0; k < dim; k++)
		{
			T left = *(pa + idx_row * dim + k);
			T right = *(pb + idx_col * dim + k);
			s += left * right;
		}
		*(pout + i) = s;
	}
}

//template <>
//inline void MatCompute::cpu_multiple_a_bt(const double* pa, 
//								   const double* pb, double* pout, int out_rows, int out_cols, int dim)
//{
//	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, out_rows, out_cols, dim, 
//		1, pa, dim, pb, dim, 0, pout, out_cols);
//}


template <>
inline void MatCompute::svd<double>(double* mat_a, 
									double* pmatU, 
									double* pmatVt,
									double* psigmas, 
									int a_rows, int a_cols)
{
	char jobu = (pmatU == NULL)? 'N' : 's';
	char jobvt = (pmatVt == NULL) ? 'N' : 's';
	int k = a_rows < a_cols ? a_rows : a_cols;

	vector<double> vec_b;
	vec_b.resize(k);
	double* superb = vec_b.data();

	LAPACKE_dgesvd( 
		LAPACK_ROW_MAJOR,
		jobu, 
		jobvt, 
		a_rows, 
		a_cols, 
		mat_a, 
		a_cols, 
		psigmas, 
		pmatU, 
		k, 
		pmatVt, 
		a_cols, 
		superb);
}

template <>
inline void MatCompute::cpu_multiple_at_b<double>(const double* mat_a, const double* mat_b, 
												  double* mat_out, int a_rows, int a_cols, int b_cols)
{
	cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, a_cols, b_cols, a_rows, 1, mat_a, a_cols, 
		mat_b, b_cols, 0, mat_out, b_cols);
}
template <>
inline void MatCompute::cpu_multiple_a_b<double>(const double* mat_a, const double* mat_b, double* mat_out, int a_rows, int a_cols, int b_cols)
{
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, a_rows, b_cols, a_cols, 1, mat_a, a_cols, 
		mat_b, b_cols, 0, mat_out, b_cols);
}

//template <class T>
//void MatCompute::cpu_multiple_a_bt(
//	Matrix<T> &mat_a, Matrix<T> &mat_b, Matrix<T> &mat_out)
//{
//	int out_rows = mat_a.rows();
//	int out_cols = mat_b.rows();
//	int dim = mat_a.cols();
//	SMART_ASSERT(dim == mat_b.cols())(dim)(mat_b.cols()).Fatal();
//	mat_out.resize(out_rows, out_cols);
//	long long out_length = out_rows * out_cols;
//	const T* pa = mat_a.cpu_ptr();
//	const T* pb = mat_b.cpu_ptr();
//	T* pout = mat_out.mutable_cpu_ptr();
//
//#pragma omp parallel for
//	for (long long i = 0; i < out_length; i++)
//	{
//		int idx_row = (i / out_cols);
//		int idx_col = (i % out_cols);
//		T s = 0;
//		for (int k = 0; k < dim; k++)
//		{
//			T left = *(pa + idx_row * dim + k);
//			T right = *(pb + idx_col * dim + k);
//			s += left * right;
//		}
//		*(pout + idx_row * out_cols + idx_col) = s;
//	}
//}
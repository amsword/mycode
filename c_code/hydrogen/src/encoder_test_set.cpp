#include <string>
#include "DiskIO.h"
#include "encoder_test_set.h"
#include "MatCompute.h"

void EncoderTestSet::create_test_set_g1(Matrix<DataType> &mat_data, 
										shared_ptr<IEncoder> &encoder, 
										Matrix<CodeType> &mat_code)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "base.double";
	const string str_code = str_dir + "code";
	const string str_all_D = str_dir + "all_D";
	const int rows_read = 10;

	DiskIO::read_data(str_base, mat_data, rows_read);
	DiskIO::read_data(str_code, mat_code, rows_read);
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(1));
	DiskIO::read_data(str_all_D, pvecmat_dics->operator[](0));

	pvecmat_dics->operator[](0).mutable_cpu_ptr();
	VQEncoder* pencoder = new VQEncoder(
		shared_ptr<Matrix<DataType> >(NULL), pvecmat_dics, 4);
	encoder.reset(pencoder);
}

void EncoderTestSet::create_test_set_in_ock(Matrix<DataType> &mat_data, 
											shared_ptr<IEncoder> &encoder, 
											Matrix<CodeType> &mat_code)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "base.double";
	const string str_code = str_dir + "code_ock";
	const string str_all_D = str_dir + "all_D";
	const string str_rotation = str_dir + "R_ock";
	const int rows_read = 10;

	DiskIO::read_data(str_base, mat_data, rows_read);
	DiskIO::read_data(str_code, mat_code, rows_read);
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(2));
	for (int i = 0; i < 2; i++)
	{
		char buf[256];
		sprintf(buf, "%s%d_ock", str_all_D.c_str(), i);
		DiskIO::read_data(buf, pvecmat_dics->operator[](i));
	}
	shared_ptr<Matrix<DataType> > pmat_rotation;
	pmat_rotation.reset(new Matrix<DataType>());
	DiskIO::read_data(str_rotation, *pmat_rotation.get());

	VQEncoder* pencoder = new VQEncoder(pmat_rotation, pvecmat_dics, 2, In_OCK);
	encoder.reset(pencoder);
}

void EncoderTestSet::create_test_set_g2(Matrix<DataType> &mat_data, 
										shared_ptr<IEncoder> &encoder, 
										Matrix<CodeType> &mat_code)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "base.double";
	const string str_code = str_dir + "code2";
	const string str_all_D = str_dir + "all_D2";
	const int rows_read = 10;

	DiskIO::read_data(str_base, mat_data, rows_read);
	DiskIO::read_data(str_code, mat_code, rows_read);
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(1));
	DiskIO::read_data(str_all_D, pvecmat_dics->operator[](0));

	pvecmat_dics->operator[](0).mutable_cpu_ptr();
	VQEncoder* pencoder = new VQEncoder(
		shared_ptr<Matrix<DataType> >(NULL), pvecmat_dics, 4, Group2);
	encoder.reset(pencoder);
}

bool EncoderTestSet::create_test_set(Matrix<DataType> &mat_data, 
									 shared_ptr<IEncoder> &encoder, 
									 Matrix<CodeType> &mat_code)
{
	static int to_be_created = 0;
	bool is_created = false;
	switch (to_be_created)
	{
	case 0:
		create_test_set_g1(mat_data, encoder, mat_code);
		is_created = true;
		break;
	case 1:
		create_test_set_g2(mat_data, encoder, mat_code);
		is_created = true;
		break;
	case 2:
		create_test_set_in_ock(mat_data, encoder, mat_code);
		is_created = true;
		break;
	default:
		is_created = false;
		break;
	}
	if (is_created)
	{
		to_be_created++;
	}
	return is_created;
}


// test group1
void create_training_set_real(Matrix<DataType> &mat_data, 
							  shared_ptr<IEncoder> &encoder, 
							  shared_ptr<IEncoderModel> &encoder_model, 
							  RVQMethod rvq_method)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "X.double";
	const int num_partitions = 2;

	string pre_fix;
	switch (rvq_method)
	{
	case Group1:
		pre_fix = "1";
		break;
	case Group2:
		pre_fix = "2";
		break;
	case In_OCK:
		pre_fix = "in_ock";
		break;
	case In_AQ:
		NOT_READY;
		break;
	}

	DiskIO::read_data(str_base, mat_data);

	// input and output dics
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(num_partitions));
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics_true; 
	pvecmat_dics_true.reset(new vector<Matrix<DataType> >(num_partitions));

	for (int i = 0; i < num_partitions; i++)
	{
		char buf[256];
		string str_i = itoa(i, buf, 10);
		string str_all = itoa(num_partitions, buf, 10);
		string str_file_name = str_dir + pre_fix 
			+ "_input_all_d" + str_i + "_" + str_all + ".double";
		DiskIO::read_data(str_file_name, pvecmat_dics->operator[](i));

		str_file_name = str_dir + pre_fix 
			+ "_output_all_d" + str_i + "_" + str_all + ".double";
		DiskIO::read_data(str_file_name, pvecmat_dics_true->operator[](i));
	}
	//PRINT << MatCompute::L1(pvecmat_dics_true->operator[](0).cpu_ptr(), 
	//	pvecmat_dics_true->operator[](0).size()) << "\n";


	// input and output rotations
	shared_ptr<Matrix<DataType> > pmat_rotation;
	pmat_rotation.reset(new Matrix<DataType>());
	pmat_rotation->resize(mat_data.cols(), mat_data.cols());
	memset(pmat_rotation->mutable_cpu_ptr(), 0, 
		sizeof(DataType) * mat_data.cols() * mat_data.cols());
	for (int i = 0; i < mat_data.cols(); i++)
	{
		pmat_rotation->set(i, i, 1);
	}
	shared_ptr<Matrix<DataType> > pmat_rotation_true;
	pmat_rotation_true.reset(new Matrix<DataType>());
	string str_rotation_true = str_dir + pre_fix + "_rotation.double";
	DiskIO::read_data(str_rotation_true, *pmat_rotation_true);

	// initialize the encoder
	VQEncoder* pencoder = new VQEncoder(
		pmat_rotation, pvecmat_dics, 4, rvq_method, 1);
	encoder.reset(pencoder);

	// multiple sub dic, parititon schemes. 
	shared_ptr<Matrix<int> > pmat_quantizer_partition;
	pmat_quantizer_partition.reset(new Matrix<int>());
	pmat_quantizer_partition->resize(2, 5);
	for (int i = 0; i < 5; i++)
	{
		pmat_quantizer_partition->set(0, i, i * 256);
		pmat_quantizer_partition->set(1, i, i * 256);
	}

	// true model
	VQEncoderModel* model = new VQEncoderModel(pmat_rotation_true, 
		pvecmat_dics_true, pmat_quantizer_partition);
	encoder_model.reset(model);
}

void create_training_set_real_hie(Matrix<DataType> &mat_data, 
								  shared_ptr<IEncoder> &encoder, 
								  shared_ptr<IEncoderModel> &encoder_model)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "X.double";
	const int num_partitions = 4;
	RVQMethod rvq_method = Group1;
	string pre_fix = "hie";

	DiskIO::read_data(str_base, mat_data);

	// input and output dics
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(num_partitions));
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics_true; 
	pvecmat_dics_true.reset(new vector<Matrix<DataType> >(1));

	for (int i = 0; i < num_partitions; i++)
	{
		char buf[256];
		string str_i = itoa(i, buf, 10);
		string str_all = itoa(num_partitions, buf, 10);
		string str_file_name = str_dir + pre_fix 
			+ "_input_all_d" + str_i + "_" + str_all + ".double";
		DiskIO::read_data(str_file_name, pvecmat_dics->operator[](i));
	}
	string str_file_name = str_dir + pre_fix 
		+ "_output_all_d0_1.double";
	DiskIO::read_data(str_file_name, pvecmat_dics_true->operator[](0));

	// input and output rotations
	shared_ptr<Matrix<DataType> > pmat_rotation;
	shared_ptr<Matrix<DataType> > pmat_rotation_true;
	pmat_rotation.reset(new Matrix<DataType>());
	pmat_rotation_true.reset(new Matrix<DataType>());
	pmat_rotation->resize(mat_data.cols(), mat_data.cols());
	pmat_rotation_true->resize(mat_data.cols(), mat_data.cols());
	memset(pmat_rotation->mutable_cpu_ptr(), 0, sizeof(DataType) * mat_data.cols() * mat_data.cols());
	memset(pmat_rotation_true->mutable_cpu_ptr(), 0, sizeof(DataType) * mat_data.cols() * mat_data.cols());

	for (int i = 0; i < mat_data.cols(); i++)
	{
		pmat_rotation->set(i, i, 1);
		pmat_rotation_true->set(i, i, 1);
	}

	shared_ptr<VQEncoder> pvqencoder;
	pvqencoder.reset(new VQEncoder());
	pvqencoder->set_max_iteration(1);

	// initialize the encoder
	HierarchicalEncoder* pencoder = new HierarchicalEncoder(1, 4, 256, pvqencoder);
	pencoder->set_start_initialize(pmat_rotation, pvecmat_dics);
	encoder.reset(pencoder);

	// multiple sub dic, parititon schemes. 
	shared_ptr<Matrix<int> > pmat_quantizer_partition;
	pmat_quantizer_partition.reset(new Matrix<int>());
	pmat_quantizer_partition->resize(1, 5);
	for (int i = 0; i < 5; i++)
	{
		pmat_quantizer_partition->set(0, i, i * 256);
	}

	// true model
	VQEncoderModel* model = new VQEncoderModel(pmat_rotation_true, 
		pvecmat_dics_true, pmat_quantizer_partition);
	encoder_model.reset(model);
}


void create_training_set_real(Matrix<DataType> &mat_data, 
							  shared_ptr<IEncoder> &encoder, 
							  shared_ptr<IEncoderModel> &encoder_model)
{
	const string str_dir = "C:\\Users\\t0908482\\Desktop\\test_data\\";
	const string str_base = str_dir + "Z.double";
	const string str_all_D_input = str_dir + "all_D.double.0.input";
	const string str_all_D_true = str_dir + "all_D.double.0.true";

	DiskIO::read_data(str_base, mat_data);
	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics;
	pvecmat_dics.reset(new vector<Matrix<DataType> >(1));
	DiskIO::read_data(str_all_D_input, pvecmat_dics->operator[](0));

	VQEncoder* pencoder = new VQEncoder(
		shared_ptr<Matrix<DataType> >(NULL), pvecmat_dics, 4, Group1, 1);
	encoder.reset(pencoder);

	shared_ptr<vector<Matrix<DataType> > > pvecmat_dics_true; 
	pvecmat_dics_true.reset(new vector<Matrix<DataType> >(1));
	DiskIO::read_data(str_all_D_true, pvecmat_dics_true->operator[](0));

	shared_ptr<Matrix<int> > pmat_quantizer_partition;
	pmat_quantizer_partition.reset(new Matrix<int>());
	pmat_quantizer_partition->resize(1, 5);
	for (int i = 0; i < 5; i++)
	{
		pmat_quantizer_partition->set(0, i, i * 256);
	}

	VQEncoderModel* model = new VQEncoderModel(shared_ptr<Matrix<DataType> >(NULL), 
		pvecmat_dics_true, pmat_quantizer_partition);
	encoder_model.reset(model);
}

void create_training_set_toy(Matrix<DataType> &mat_data, 
							 shared_ptr<IEncoder> &encoder, 
							 shared_ptr<IEncoderModel> &encoder_model)
{
	{
		mat_data.resize(4, 2);
		mat_data.set(0, 0, 0);
		mat_data.set(0, 1, 0);
		mat_data.set(1, 0, 0);
		mat_data.set(1, 1, 1);
		mat_data.set(2, 0, 10);
		mat_data.set(2, 1, 0);
		mat_data.set(3, 0, 10);
		mat_data.set(3, 1, 1);
	}
	{
		shared_ptr<vector<Matrix<DataType> > > pvecmat_quantizers;
		pvecmat_quantizers.reset(new vector<Matrix<DataType> > (1));
		Matrix<DataType> &mat_quantizers = pvecmat_quantizers->operator[](0);
		mat_quantizers.resize(2, 2);
		mat_quantizers.set(0, 0, 0);
		mat_quantizers.set(0, 1, 0);
		mat_quantizers.set(1, 0, 10);
		mat_quantizers.set(1, 1, 0);

		VQEncoder* vq = new VQEncoder(shared_ptr<Matrix<DataType> >(NULL), 
			pvecmat_quantizers, 1);
		encoder.reset(vq);
	}
	{
		shared_ptr<vector<Matrix<DataType> > > pvecmat_quantizers;
		pvecmat_quantizers.reset(new vector<Matrix<DataType> > (1));
		Matrix<DataType> &mat_quantizers = pvecmat_quantizers->operator[](0);
		mat_quantizers.resize(2, 2);
		mat_quantizers.set(0, 0, 0);
		mat_quantizers.set(0, 1, 0.5);
		mat_quantizers.set(1, 0, 10);
		mat_quantizers.set(1, 1, 0.5);

		shared_ptr<Matrix<int> > pmat_quantizer_partition;
		pmat_quantizer_partition.reset(new Matrix<int>());
		pmat_quantizer_partition->resize(1, 2);
		pmat_quantizer_partition->set(0, 0, 0);
		pmat_quantizer_partition->set(0, 1, 2);
		VQEncoderModel* vq_model = new VQEncoderModel(shared_ptr<Matrix<DataType> >(NULL), 
			pvecmat_quantizers, pmat_quantizer_partition);
		encoder_model.reset(vq_model);
	}
}

bool EncoderTestSet::create_training_set(Matrix<DataType> &mat_data, 
										 shared_ptr<IEncoder> &encoder, 
										 shared_ptr<IEncoderModel> &encoder_model)
{
	static int to_be_created = 5;
	bool is_created = false;
	switch (to_be_created)
	{
	case 0:
		create_training_set_toy(mat_data, encoder, encoder_model);
		is_created = true;
		break;
	case 1:
		create_training_set_real(mat_data, encoder, encoder_model);
		is_created = true;
		break;
	case 2:
		create_training_set_real(mat_data, encoder, encoder_model, Group1);
		is_created = true;
		break;
	case 3:
		create_training_set_real(mat_data, encoder, encoder_model, Group2);
		is_created = true;
		break;
	case 4:
		create_training_set_real(mat_data, encoder, encoder_model, In_OCK);
		is_created = true;
		break;
	case 5:
		create_training_set_real_hie(mat_data, encoder, encoder_model);
		is_created = true;
		break;
	default:
		is_created = false;
		break;
	}
	if (is_created)
	{
		to_be_created++;
	}
	return is_created;
}

#pragma once
#include "Matrix.h"
#include "Macro.h"

class DiskIO
{
public:
	template <class T>
	static void read_data(const string &file_name, Matrix<T> &mat, int rows_read = 0)
	{
		FILE* fp;
		open_read(file_name, fp);
		read_data(fp, mat, rows_read);
		fclose(fp);
	}

	static void open_read(const string &file_name, FILE* &fp);

	template <class T>
	static void read_data(FILE* &fp, Matrix<T> &mat, int rows_read = 0)
	{
		int rows;
		int cols;

		SMART_ASSERT(fread(&rows, sizeof(int), 1, fp) == 1).Fatal();
		SMART_ASSERT(fread(&cols, sizeof(int), 1, fp) == 1).Fatal();

		if (rows_read > 0 && rows_read < rows)
		{
			rows = rows_read;
		}
		mat.resize(rows, cols);
		T* p = mat.mutable_cpu_ptr();
		size_t total_count = (size_t)rows * (size_t)cols;
		SMART_ASSERT(fread(p, sizeof(T), total_count, fp) == total_count).Fatal();
	}

public:
	template <class T>
	static void save_data(Matrix<T> &mat, const string &file_name);

	static void open_write(const string &file_name, FILE* fp);

	template <class T>
	static void save_data(FILE* fp, Matrix<T> &mat)
	{
		size_t rows = mat.rows();
		SMART_ASSERT(rows < (1 << 30))(rows).Fatal();

		size_t cols = mat.cols();
		SMART_ASSERT(cols < (1 << 30))(cols).Fatal();

		int n_rows = rows;
		int n_cols = cols;
		SMART_ASSERT(fwrite(&n_rows, sizeof(int), 1, fp) == 1).Fatal();
		SMART_ASSERT(fwrite(&n_cols, sizeof(int), 1, fp) == 1).Fatal();
	}
};

#include "SyncedMemory.h"
#include "Macro.h"

Memory::Memory() : 
	m_ptr(NULL), m_count(0), 
	m_is_own(false)
{
}
Memory::~Memory()
{
	release();
}
void Memory::attach(void* ptr, size_t count)
{
	release();
	m_ptr = ptr;
	m_count = count;
	m_is_own = false;
}
void Memory::release()
{
}
void* Memory::data()
{
	return m_ptr;
}
size_t Memory::size()
{
	return m_count;
}

// cpu memory
void CPUMemory::resize(size_t count)
{
	if (m_count < count || !m_is_own)
	{
		release();
		m_ptr = malloc(count);
	}
	m_count = count;
	m_is_own = true;
}
void CPUMemory::release()
{
	if (m_is_own && m_ptr)
	{
		free(m_ptr);
	}
	m_ptr = NULL;
	m_count = 0;
	m_is_own = false;
}

// gpu memory
void GPUMemory::resize(size_t count)
{
	NOT_READY;
}
void GPUMemory::release()
{
	NOT_READY;
}

//LazyMemory::LazyMemory(Memory* memory):
//	m_memory(memory), m_is_resized(false)
//{
//
//}

//void LazyMemory::resize(size_t count)
//{
//	m_memory->release();
//	m_is_resized = false;
//	m_count = count;
//}
//
//void LazyMemory::release()
//{
//	if (m_is_resized)
//	{
//		m_memory->release();
//	}
//	m_memory = NULL;
//	m_count = 0;
//}
//void* LazyMemory::data()
//{
//	if (!m_is_resized)
//	{
//		m_memory->resize(m_count);		
//		m_is_resized = true;
//	}
//
//	return m_memory->data();
//}
//---------------------------------------------------------

SyncedMemory::SyncedMemory() : m_count(0)
{

}

void SyncedMemory::resize(size_t count)
{
	m_count = count;
	m_vec_memory.clear();
}


void SyncedMemory::attach_to_cpu(void* p, size_t count)
{
	if (m_vec_memory.size() == 0)
	{
		m_vec_memory.resize(1);
	}
	shared_ptr<Memory> & ptr_mem = m_vec_memory[0];
	ptr_mem.reset(new CPUMemory());
	ptr_mem->attach(p, count);
}

const void* SyncedMemory::cpu_data(size_t offset, size_t count)
{
	if (m_vec_memory.size() == 0)
	{
		m_vec_memory.resize(1);
		shared_ptr<Memory> & ptr_mem = m_vec_memory[0];
		ptr_mem.reset(new CPUMemory());
		SMART_ASSERT(m_count > 0 && m_count >= offset + count)
			(m_count)(offset)(count).Fatal();
		ptr_mem->resize(m_count);
	}

	return (char*)m_vec_memory[0]->data() + offset;
}

void* SyncedMemory::mutable_cpu_data(size_t offset, size_t count)
{
	if (m_vec_memory.size() == 0)
	{
		m_vec_memory.resize(1);
		shared_ptr<Memory> & ptr_mem = m_vec_memory[0];
		ptr_mem.reset(new CPUMemory());
		SMART_ASSERT(m_count > 0 && m_count >= offset + count)
			(m_count)(offset)(count).Fatal();
		ptr_mem->resize(m_count);
	}
	return (char*)m_vec_memory[0]->data() + offset;
}

#include "Matrix.h"
#include "Macro.h"


template <class T>
Matrix<T>::Matrix() : 
	m_rows(0), m_cols(0)
{
}

template <class T>
Matrix<T>::Matrix(size_t rows, size_t cols): 
	m_rows(0), m_cols(0)
{
	resize(rows, cols);
}

template <class T>
void Matrix<T>::resize(size_t rows, size_t cols)
{
	m_memory.resize(rows * cols * sizeof(T));
	m_rows = rows;
	m_cols = cols;
}

template <class T>
void Matrix<T>::attach_to_cpu(T* p, size_t rows, size_t cols)
{
	m_memory.attach_to_cpu(p, rows * cols * sizeof(T));

	m_rows = rows;
	m_cols = cols;
}

template <class T>
const T* Matrix<T>::cpu_ptr(size_t row_offset = 0, size_t rows = 0)
{
	return (T*)m_memory.cpu_data(
		row_offset * m_cols * sizeof(T), 
		rows * m_cols *  sizeof(T));
}

template <class T>
T* Matrix<T>::mutable_cpu_ptr(size_t row_offset = 0, size_t rows = 0)
{
	return (T*)m_memory.mutable_cpu_data(
		row_offset * m_cols * sizeof(T), 
		rows * m_cols * sizeof(T));
}

template <class T>
size_t Matrix<T>::rows()
{
	return m_rows;
}

template <class T>
size_t Matrix<T>::cols()
{
	return m_cols;
}

template <class T>
size_t Matrix<T>::size()
{
	return m_rows * m_cols;
}




template class Matrix<double>;
template class Matrix<float>;
template class Matrix<unsigned char>;
template class Matrix<short>;
template class Matrix<int>;

#include <queue>
#include "encoder.h"
#include "Macro.h"
#include "pair_compare.h"
#include "MatCompute.h"


void IEncoder::export_model(shared_ptr<IEncoderModel> &pmodel)
{
}

void VQEncoder::training(Matrix<DataType> &mat_data)
{
	Matrix<CodeType> mat_code;
	for (int i = 0; i < m_opt_option.max_iter; i++)
	{
		// think the encoding as the forward computing in the neural network 
		encoding(mat_data, mat_code);
		const CodeType* p = mat_code.cpu_ptr();
		// the intermediate parameters should be saved in the member variables.
		update_codebook(mat_data, mat_code);
	}
}

void VQEncoder::set_max_iteration(int iter)
{
	m_opt_option.max_iter = iter;
}

//--------------------------------------
VQEncoder::VQEncoder(RVQMethod method) : m_residual_alg(method)
{
	set_max_iteration(100);
}
VQEncoder::VQEncoder(shared_ptr<Matrix<DataType> > &pmat_rotation, 
					 shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
					 int num_dic_each, RVQMethod method, int iter) : m_residual_alg(method), m_is_rotate(true)
{
	initialize(pmat_rotation, pvecmat_quantizers, num_dic_each);
	set_max_iteration(iter);
}

void VQEncoder::initialize(shared_ptr<Matrix<DataType> > &pmat_rotation, 
						   shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
						   int num_sub_dic)
{
	m_is_rotate = true;

	int num_partitions = pvecmat_quantizers->size();
	{  
		m_pmat_data_partition.reset(new Matrix<int>());
		m_pmat_data_partition->resize(1, num_partitions + 1);
		int idx = 0;
		int* p_data_partition = m_pmat_data_partition->mutable_cpu_ptr();
		for (int i = 0; i < num_partitions; i++)
		{
			m_pmat_data_partition->set(0, i, idx);
			idx += pvecmat_quantizers->operator[](i).cols();
		}
		m_pmat_data_partition->set(0, num_partitions, idx);
	}

	{
		m_pmat_code_partition.reset(new Matrix<int>());
		m_pmat_code_partition->resize(1, num_partitions + 1);
		int* p_code_partition = m_pmat_code_partition->mutable_cpu_ptr();
		for (int i = 0; i < num_partitions; i++)
		{
			*(p_code_partition + i) = i * num_sub_dic;
		}
		*(p_code_partition + num_partitions) = num_partitions * num_sub_dic;
	}

	infer_quantizer_partition(pvecmat_quantizers, num_sub_dic);

	m_pmat_rotation = pmat_rotation;
	m_pvecmat_quantizers = pvecmat_quantizers;
	if (m_pvecmat_quantizers->size() == 1)
	{
		m_is_rotate = false;
	}
}

void VQEncoder::infer_quantizer_partition(shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers, 
										  int num_sub_dic)
{
	int num_partitions = pvecmat_quantizers->size();

	if (m_pmat_quantizer_partition.get() == NULL)
	{
		m_pmat_quantizer_partition.reset(new Matrix<int>());
	}

	m_pmat_quantizer_partition->resize(num_partitions, num_sub_dic + 1);
	int* p_quantizer_partition = m_pmat_quantizer_partition->mutable_cpu_ptr();
	for (int i = 0; i < num_partitions; i++)
	{
		int sub_dic_size = pvecmat_quantizers->operator[](i).rows() / num_sub_dic;
		SMART_ASSERT(sub_dic_size * num_sub_dic == pvecmat_quantizers->operator[](i).rows())
			(sub_dic_size)(num_sub_dic)(pvecmat_quantizers->operator[](i).rows()).Fatal();

		for (int j = 0; j < num_sub_dic + 1; j++)
		{
			m_pmat_quantizer_partition->set(i, j, sub_dic_size * j);
		}
	}
}

void VQEncoder::export_model(shared_ptr<IEncoderModel> &pmodel)
{
	pmodel.reset(new VQEncoderModel(m_pmat_rotation, 
		m_pvecmat_quantizers, m_pmat_quantizer_partition));
}

void VQEncoder::encoding(Matrix<DataType> &mat_data, 
						 Matrix<CodeType> &mat_code)
{
	int num_code_unit = 0;
	for (int i = 0; i < m_pmat_quantizer_partition->rows(); i++)
	{
		int num_col = m_pmat_quantizer_partition->cols();
		int v =	m_pmat_quantizer_partition->get(i, num_col - 1);
		if (v == m_pvecmat_quantizers->operator[](i).rows())
		{
			num_code_unit += num_col - 1;
		}
		else
		{
			num_code_unit += num_col;
		}
	}

	mat_code.resize(mat_data.rows(), num_code_unit);

	cpu_rotating(mat_data, m_pmat_rotated);
	cpu_nearest(m_pmat_rotated, mat_code);
}



void VQEncoder::update_codebook(Matrix<DataType> &mat_data, 
								Matrix<CodeType> &mat_code)
{
	update_quantizers(mat_code);
	update_rotation(mat_data, mat_code);
}

void VQEncoder::update_quantizers(Matrix<CodeType> &mat_code)
{
	int num_partitions = m_pvecmat_quantizers->size();
	const int* p_code_partition = m_pmat_code_partition->cpu_ptr();
	const int* p_quantizer_partition = m_pmat_quantizer_partition->cpu_ptr();
	int quantizer_col = m_pmat_quantizer_partition->cols();
	const int* p_data_partition = m_pmat_data_partition->cpu_ptr();
	for (int i = 0; i < num_partitions; i++)
	{
		const int* p_curr_quantizer_partition = p_quantizer_partition + i * quantizer_col;
		update_quantizers(p_data_partition[i], p_data_partition[i + 1], 
			mat_code, p_code_partition[i], p_code_partition[i + 1], 
			p_curr_quantizer_partition, 
			m_pvecmat_quantizers->operator[](i));
	}
}

void VQEncoder::update_quantizers(int data_dim_start, int data_dim_end, 
								  Matrix<CodeType> &mat_code, int code_dim_start, int code_dim_end, 
								  const int* p_curr_quantizer_partition, 
								  Matrix<DataType> & mat_quantizers)
{
	// ZB^{T}
	int num_point = m_pmat_rotated->rows();
	int sub_dim = data_dim_end - data_dim_start;
	int num_sub_dic = code_dim_end - code_dim_start;

	//matZBt.SetValueZeros();

	const DataType* p_rotated = m_pmat_rotated->cpu_ptr();
	size_t data_col = m_pmat_rotated->cols();

	const CodeType* p_code = mat_code.cpu_ptr();
	int code_col = mat_code.cols();

	DataType* pout = mat_quantizers.mutable_cpu_ptr();
	memset(pout, 0, mat_quantizers.rows() * mat_quantizers.cols() * sizeof(DataType));
	int out_col = mat_quantizers.cols();
	for (int i = 0; i < num_point; i++)
	{
		const DataType* p_point = p_rotated + i * data_col + data_dim_start;
		const CodeType* p_words_idx = p_code + i * code_col + code_dim_start;
		for (int j = 0; j < num_sub_dic; j++)
		{
			int idx_word = p_words_idx[j] + p_curr_quantizer_partition[j];
			MatCompute::cpu_xpy(pout + idx_word * out_col, p_point, sub_dim);
		}
	}

	// BBt
	Matrix<DataType> matBBt;
	int num_quantizers = p_curr_quantizer_partition[num_sub_dic] - p_curr_quantizer_partition[0];
	matBBt.resize(num_quantizers, num_quantizers);
	DataType* pbbt = matBBt.mutable_cpu_ptr();
	size_t bbt_col = matBBt.cols();
	memset(pbbt, 0, sizeof(DataType) * matBBt.rows() * matBBt.cols());
	for (int i = 0; i < num_point; i++)
	{
		const CodeType* p_idx_words = p_code + i * code_col + code_dim_start;
		for (int j1 = 0; j1 < num_sub_dic; j1++)
		{
			int k1 = p_idx_words[j1] + p_curr_quantizer_partition[j1];
			for (int j2 = 0; j2 < num_sub_dic; j2++)
			{
				int k2 = p_idx_words[j2] + p_curr_quantizer_partition[j2];
				(*(pbbt + k1 * bbt_col + k2))++ ;
			}
		}
	}

	for (int i = 0; i < num_quantizers; i++)
	{
		*(pbbt + i * bbt_col + i) += 0.00001; 
	}

	if (num_sub_dic == 1)
	{
		for (int i = 0; i < num_quantizers; i++)
		{
			MatCompute::cpu_ax(pout + i * sub_dim, 1.0 / *(pbbt + i * bbt_col + i), sub_dim);
		}
	}
	else
	{
		MatCompute::linear_system_sax_b(pbbt, pout, num_quantizers, sub_dim);
	}
}

void VQEncoder::quantizer_recover(Matrix<DataType> &mat_data, 
								  Matrix<CodeType> &mat_code, Matrix<DataType> &mat_recovered)
{
	int num_point = mat_data.rows();
	int dim = mat_data.cols();
	mat_recovered.resize(num_point, dim);
	DataType* p_recovered = mat_recovered.mutable_cpu_ptr();
	const DataType* p_data = mat_data.cpu_ptr();
	const CodeType* p_code = mat_code.cpu_ptr();
	int code_units = mat_code.cols();
	int num_partitions = m_pvecmat_quantizers->size();
	const int* p_quantizer_partition = m_pmat_quantizer_partition->cpu_ptr();
	int quantizer_partition_stride = m_pmat_quantizer_partition->cols();
#pragma omp parallel for
	for (int i = 0; i < num_point; i++)
	{
		DataType* p_curr_recovered = p_recovered + i * dim;
		const CodeType* p_curr_code = p_code + i * code_units;

		for (int i = 0; i < num_partitions; i++)
		{
			Matrix<DataType> &matDictionary = m_pvecmat_quantizers->operator[](i);
			const int* p_curr_quantizer_partition = p_quantizer_partition + i * quantizer_partition_stride;
			int num_sub_dic = quantizer_partition_stride - 1;

			const DataType* p_word = matDictionary.cpu_ptr();
			int curr_dim = matDictionary.cols();
			memcpy(p_curr_recovered, p_word + (*p_curr_code++) * curr_dim, 
				sizeof(DataType) * curr_dim);

			for (int j = 1; j < num_sub_dic; j++)
			{
				MatCompute::cpu_xpy(p_curr_recovered, 
					p_word + ((*p_curr_code++) + p_curr_quantizer_partition[j]) * curr_dim, curr_dim);
			}

			p_curr_recovered += curr_dim;
		}
	}
}

void VQEncoder::update_rotation(Matrix<DataType> &mat_data, Matrix<CodeType> &mat_code)
{
	if (m_is_rotate)
	{
		Matrix<DataType> mat_recovered;
		quantizer_recover(mat_data, mat_code, mat_recovered);
		int dim = mat_data.cols();
		int num_point = mat_data.rows();

		Matrix<DataType> mat_output;
		mat_output.resize(dim, dim);
		DataType* p_inner = mat_output.mutable_cpu_ptr();
		MatCompute::cpu_multiple_at_b(mat_data.cpu_ptr(), mat_recovered.cpu_ptr(), 
			p_inner, num_point, dim, dim);

		Matrix<DataType> mat_u(dim, dim), mat_vt(dim, dim), vec_sigma(1, dim);
		DataType* p_u = mat_u.mutable_cpu_ptr();
		DataType* p_vt = mat_vt.mutable_cpu_ptr();

		MatCompute::svd(p_inner, p_u, p_vt,	vec_sigma.mutable_cpu_ptr(), dim, dim);

		MatCompute::cpu_multiple_a_b(p_u, p_vt, m_pmat_rotation->mutable_cpu_ptr(), dim, dim, dim);
	}
}

void VQEncoder::cpu_rotating(Matrix<DataType> &mat_data, 
							 Matrix<DataType>* &p_rotated)
{
	if (m_pmat_rotation.get() &&  
		m_pmat_rotation->rows() > 0)
	{
		if (m_pmat_aux.get() == NULL)
		{
			m_pmat_aux.reset(new Matrix<DataType>());
			m_pmat_aux->resize(mat_data.rows(), mat_data.cols());
		}
		p_rotated = m_pmat_aux.get();
		MatCompute::cpu_multiple_a_bt(mat_data.cpu_ptr(),
			m_pmat_rotation->cpu_ptr(), 
			p_rotated->mutable_cpu_ptr(), 
			mat_data.rows(), m_pmat_rotation->rows(), 
			m_pmat_rotation->cols());
	}
	else
	{
		p_rotated = &mat_data;
	}
}
void VQEncoder::cpu_nearest(Matrix<DataType>* p_rotated, 
							Matrix<CodeType> &mat_code)
{ // only cpu version
	int dim_data = p_rotated->cols();
	const DataType* ptr_data = p_rotated->cpu_ptr();
	CodeType* ptr_code = mat_code.mutable_cpu_ptr();
	int dim_code = mat_code.cols();
	int num_partition = m_pvecmat_quantizers->size();
	int num_samples = p_rotated->rows();
	long long total = num_partition * num_samples;
	const int* p_dim_offset = m_pmat_data_partition->cpu_ptr();
	const int* p_code_offset = m_pmat_code_partition->cpu_ptr();
	const int* quantizer_offset = m_pmat_quantizer_partition->cpu_ptr();
	int num_sub_dic = m_pmat_quantizer_partition->cols() - 1;
	int quantizer_col = m_pmat_quantizer_partition->cols();

	cpu_quantizers_inner_product();
#pragma omp parallel for
	for (long long idx = 0; idx < total; idx++)
	{
		int idx_sample = idx / num_partition;
		int idx_partition = idx % num_partition;
		const DataType* p_curr_sample = ptr_data + dim_data * idx_sample + p_dim_offset[idx_partition];
		int sub_dim = m_pvecmat_quantizers->operator[](idx_partition).cols();
		Matrix<DataType> &quantizers = m_pvecmat_quantizers->operator[](idx_partition);
		Matrix<DataType> &quan_ip = m_vecmat_quantizers_inner_product[idx_partition];
		CodeType* p_curr_code = ptr_code + dim_code * idx_sample + p_code_offset[idx_partition];

		m_residual_alg.cpu_nearest(p_curr_sample, quantizers, quan_ip, 
			quantizer_offset + idx_partition * quantizer_col, 
			num_sub_dic, p_curr_code);
	}
}
void VQEncoder::cpu_quantizers_inner_product()
{
	int num_partition = m_pvecmat_quantizers->size();
	if (m_vecmat_quantizers_inner_product.size() != num_partition)
	{
		m_vecmat_quantizers_inner_product.resize(num_partition);
	}
	for (int i = 0; i < num_partition; i++)
	{
		Matrix<DataType> &quantizers = m_pvecmat_quantizers->operator[](i);
		Matrix<DataType> &inner_product = m_vecmat_quantizers_inner_product[i];
		inner_product.resize(quantizers.rows(), quantizers.rows());
		MatCompute::cpu_multiple_a_bt(quantizers.cpu_ptr(), 
			quantizers.cpu_ptr(), 
			inner_product.mutable_cpu_ptr(), 
			quantizers.rows(), 
			quantizers.rows(), 
			quantizers.cols());
	}
	for (int i = 0; i < num_partition; i++)
	{
		Matrix<DataType> &inner_product = m_vecmat_quantizers_inner_product[i];
		DataType* p_ip = inner_product.mutable_cpu_ptr();
		for (int j = 0; j < inner_product.rows(); j++)
		{
			(*(p_ip + j * inner_product.cols() + j)) *= 0.5;
		}
	}
}


//-----------------------------------------
NearestResidualAlg::NearestResidualAlg(RVQMethod method) : 
	m_rvqmethod(method), m_is_use_pre(false)
{
	m_para.num_candidate = 10;
}

void NearestResidualAlg::cpu_nearest(const DataType* p_curr_sample, 
									 Matrix<DataType> &quantizers, 
									 Matrix<DataType> &quan_ip, 
									 const int* quantizer_offset, 
									 int num_sub_dic, 
									 CodeType* p_curr_code)
{
	Matrix<DataType> x_ip_q;
	int num_quantizers = quantizers.rows();
	x_ip_q.resize(1, num_quantizers);
	int dim = quantizers.cols();
	DataType* pout = x_ip_q.mutable_cpu_ptr();
	const DataType* p_quan = quantizers.cpu_ptr();

	MatCompute::cpu_multiple_a_bt(p_curr_sample, 
		p_quan, 
		pout, 
		1, 
		num_quantizers, 
		dim);
	cpu_nearest(x_ip_q, 
		quan_ip, 
		quantizer_offset, 
		num_sub_dic, 
		p_curr_code);
}

void NearestResidualAlg::cpu_nearest(Matrix<DataType> &x_ip_q, 
									 Matrix<DataType> &quan_ip, 
									 CodeType* p_curr_code)
{
	const DataType* p_xipq = x_ip_q.cpu_ptr();
	const DataType* p_quan_ip = quan_ip.cpu_ptr();
	int num_quantizers = quan_ip.cols();

	double best_value = DBL_MAX;
	int idx_end_center = quan_ip.rows();

	CodeType best_idx;
	for (int idx_center = 0; idx_center < idx_end_center; idx_center++)
	{
		double s = *(p_quan_ip + idx_center * num_quantizers + idx_center);
		s -= *(p_xipq + idx_center); 
		if (s < best_value)
		{
			best_value = s;
			best_idx = idx_center;
		}
	}
	*p_curr_code = best_idx;
}

void NearestResidualAlg::cpu_nearest(Matrix<DataType> &x_ip_q, 
									 Matrix<DataType> &quan_ip, 
									 const int* quantizer_offset, 
									 int num_sub_dic, 
									 CodeType* p_curr_code)
{
	if (num_sub_dic == 1)
	{
		cpu_nearest(x_ip_q, quan_ip, p_curr_code);
		return;
	}

	switch (m_rvqmethod)
	{
	case Group1:
		cpu_nearest_g1(x_ip_q, quan_ip, quantizer_offset, 
			num_sub_dic, p_curr_code);
		return;
	case Group2: 
		cpu_nearest_g2(x_ip_q, quan_ip, quantizer_offset, 
			num_sub_dic, p_curr_code);
		return;
	case In_AQ:
		cpu_nearest_in_aq(x_ip_q, quan_ip, quantizer_offset, 
			num_sub_dic, p_curr_code);
		return;
	case In_OCK:
		cpu_nearest_in_ock(x_ip_q, quan_ip, quantizer_offset, 
			num_sub_dic, p_curr_code);
		return;
	}
}

void NearestResidualAlg::cpu_nearest_g1(Matrix<DataType> &x_ip_q, 
										Matrix<DataType> &quan_ip, 
										const int* quantizer_offset, 
										int num_dic, 
										CodeType* p_curr_code)
{
	for (int idx_dic = 0; idx_dic < num_dic; idx_dic++)
	{
		cpu_nearest_g1_scan(x_ip_q, quan_ip, quantizer_offset, idx_dic + 1, p_curr_code, idx_dic);
	}

	bool is_changed = false;
	int times = 0;
	do
	{
		is_changed = false;
		for (int idx_dic = 0; idx_dic < num_dic; idx_dic++)
		{
			bool is_sub_changed = cpu_nearest_g1_scan(x_ip_q, 
				quan_ip, quantizer_offset, num_dic, p_curr_code, idx_dic);
			if (is_sub_changed)
			{
				is_changed = true;
			}
		}
	}while(is_changed);
}

//double residual_distance(Matrix<DataType> &vec_x_map, 
//						 Matrix<DataType> &quan_ip, 
//						 const int* quantizer_offset, 
//						 int idx_center, 
//						 int idx_dic, 
//						 const CodeType* p_curr_code, 
//						 int num_dic_used)
//{
//	double s = 0;
//	int i_idx;
//	const DataType* p = quan_ip.cpu_data();
//	int num_quantizers = quan_ip.cols();
//	const DataType* px = vec_x_map.cpu_data();
//	for (int i = 0; i < num_dic_used; i++)
//	{
//		if (i == idx_dic)
//		{
//			i_idx = idx_center + quantizer_offset[i];
//		}
//		else
//		{
//			i_idx = p_curr_code[i] + quantizer_offset[i];
//		}
//		s += *(p + i_idx * num_quantizers + i_idx);
//		s -= *(px + i_idx);
//		for (int j = i + 1; j < num_dic_used; j++)
//		{
//			int j_idx = p_curr_code[j];
//			if (j_idx == -1 && j == idx_sub_cluster)
//			{
//				j_idx = idx_center;
//			}
//			else if (j_idx == -1)
//			{
//				continue;
//			}
//			s += quan_ip[i_idx][j_idx];
//		}
//	}
//	return s;
//}

bool NearestResidualAlg::cpu_nearest_g1_scan(Matrix<DataType> &x_ip_q, 
											 Matrix<DataType> &quan_ip, 
											 const int* quantizer_offset, 
											 int num_dic, 
											 CodeType* p_curr_code, int idx_dic)
{
	const DataType* p_xipq = x_ip_q.cpu_ptr();
	const DataType* p_quan_ip = quan_ip.cpu_ptr();
	int num_quantizers = quan_ip.cols();

	double best_value = DBL_MAX;
	int idx_start_center = quantizer_offset[idx_dic];
	int idx_end_center = quantizer_offset[idx_dic + 1];

	int best_idx;
	for (int idx_center = idx_start_center; idx_center < idx_end_center; idx_center++)
	{
		double s = 0;
		for (int idx_dic2 = 0; idx_dic2 < num_dic; idx_dic2++)
		{
			if (idx_dic == idx_dic2)
			{
				s += *(p_quan_ip + idx_center * num_quantizers + idx_center);
			}
			else
			{
				int curr_idx = p_curr_code[idx_dic2] + quantizer_offset[idx_dic2];
				s += *(p_quan_ip + idx_center * num_quantizers + curr_idx);
			}
		}
		s -= *(p_xipq + idx_center); 
		if (s < best_value)
		{
			best_value = s;
			best_idx = idx_center;
		}
	}
	bool is_changed = false;
	CodeType candidate = best_idx - quantizer_offset[idx_dic];
	if (p_curr_code[idx_dic] != candidate)
	{
		is_changed = true;
		p_curr_code[idx_dic] = candidate;
	}
	return is_changed;
}

void NearestResidualAlg::cpu_nearest_g2(Matrix<DataType> &x_ip_q, 
										Matrix<DataType> &quan_ip, 
										const int* quantizer_offset, 
										int num_sub_dic, 
										CodeType* p_curr_code)
{
	SMART_ASSERT(num_sub_dic >= 2)(num_sub_dic).Fatal();

	for (int i = 0; i < num_sub_dic - 1; i++)
	{
		cpu_nearest_g2(x_ip_q, quan_ip, quantizer_offset, 
			i + 2, p_curr_code, i, i + 1);
	}
	cpu_nearest_g2(x_ip_q, quan_ip, quantizer_offset, 
		num_sub_dic, p_curr_code, num_sub_dic - 1, 0);

	bool is_changed = false;
	do
	{
		is_changed = false;
		for (int i = 0; i < num_sub_dic; i++)
		{
			bool is_sub_changed = cpu_nearest_g2(x_ip_q, quan_ip, quantizer_offset, 
				num_sub_dic, p_curr_code, i, (i + 1) % num_sub_dic);
			if (is_sub_changed)
			{
				is_changed = is_sub_changed;
			}
		}	
	}while(is_changed);
}

bool NearestResidualAlg::cpu_nearest_g2(Matrix<DataType> &vec_x_map, 
										Matrix<DataType> &quan_ip, 
										const int* quantizer_offset, 
										int num_sub_dic, 
										CodeType* p_curr_code, int idx_dic_a, int idx_dic_b)
{
	Matrix<DataType> vec_pre_a;
	Matrix<DataType> vec_pre_b;

	int off_set_a = quantizer_offset[idx_dic_a];
	int off_set_b = quantizer_offset[idx_dic_b];

	PrecomputeA(idx_dic_a, idx_dic_b, vec_x_map, quan_ip, num_sub_dic, p_curr_code, 
		vec_pre_a, quantizer_offset);

	PrecomputeA(idx_dic_b, idx_dic_a, vec_x_map, quan_ip, num_sub_dic, p_curr_code, 
		vec_pre_b, quantizer_offset);

	double best_s = DBL_MAX;
	int best_i = -1, best_j = -1;
	int sub_dic_size_a = quantizer_offset[idx_dic_a + 1] - quantizer_offset[idx_dic_a];
	int sub_dic_size_b = quantizer_offset[idx_dic_b + 1] - quantizer_offset[idx_dic_b];
	const DataType* p_pre_a = vec_pre_a.cpu_ptr();
	const DataType* p_pre_b = vec_pre_b.cpu_ptr();
	const DataType* p_quan_ip = quan_ip.cpu_ptr();
	int num_quantizers = quan_ip.cols();
	for (int i = 0; i < sub_dic_size_a; i++)
	{
		DataType ai = *(p_pre_a + i);
		int off_ai = off_set_a + i;
		const DataType* pdic = p_quan_ip + off_ai * num_quantizers;
		for (int j = 0; j < sub_dic_size_b; j++)
		{
			int off_bj = off_set_b + j;
			DataType aj = (*(p_pre_b + j));
			DataType s = ai + aj + pdic[off_bj];
			if (s < best_s)
			{
				best_s = s;
				best_i = i;
				best_j = j;
			}
		}
	}


	if (p_curr_code[idx_dic_a] == best_i && p_curr_code[idx_dic_b] == best_j)
	{
		return false;
	}
	else
	{
		p_curr_code[idx_dic_a] = best_i;
		p_curr_code[idx_dic_b] = best_j;
		return true;
	}
}

void NearestResidualAlg::PrecomputeA(
	int idx_dic_a, int idx_dic_b, 
	Matrix<DataType> &vec_x_map, 
	Matrix<DataType> &matPrecomputed, 
	int num_dic_each_partition, 
	CodeType* prepresentation, 
	Matrix<double> &vec_pre_a, const int* quantizer_offset)
{
	int off_set_a = quantizer_offset[idx_dic_a];
	int off_set_b = quantizer_offset[idx_dic_b];

	int sub_dic_size = quantizer_offset[idx_dic_a + 1] - quantizer_offset[idx_dic_a];
	vec_pre_a.resize(1, sub_dic_size);
	DataType* pout = vec_pre_a.mutable_cpu_ptr();
	const DataType* p_ip = matPrecomputed.cpu_ptr();
	int num_quantizers = matPrecomputed.cols();
	const DataType* p_xip = vec_x_map.cpu_ptr();
	for (int i = 0; i < sub_dic_size; i++)
	{
		int idx_word = off_set_a + i;
		DataType s = *(p_ip + idx_word * num_quantizers + idx_word);
		for (int j = 0; j < num_dic_each_partition; j++)
		{
			if (j == idx_dic_a || j == idx_dic_b)
			{
				continue;
			}
			int curr_idx = prepresentation[j] + quantizer_offset[j];
			s += *(p_ip + idx_word * num_quantizers + curr_idx);
		}
		s -= *(p_xip + idx_word);
		*(pout + i) = s;
	}
}


void NearestResidualAlg::cpu_nearest_in_aq(Matrix<DataType> &x_ip_q, 
										   Matrix<DataType> &quan_ip, 
										   const int* quantizer_offset, 
										   int num_sub_dic, 
										   CodeType* p_curr_code)
{
	NOT_READY;
}

DataType NearestResidualAlg::cpu_residual_loss(Matrix<DataType> &vec_x_map, 
											   Matrix<DataType> &quan_ip, 
											   const int* quantizer_offset, 
											   int num_sub_dic, 
											   const CodeType* p_curr_code)
{
	const DataType* pvec_x_map = vec_x_map.cpu_ptr();
	const DataType* pquan_ip = quan_ip.cpu_ptr();
	int num_quantizers = quan_ip.cols();

	DataType loss = 0;
	const DataType* pvec_pre = vec_x_map.cpu_ptr();
	for (int i = 0; i < num_sub_dic; i++)
	{
		loss -= *(pvec_x_map + p_curr_code[i] + quantizer_offset[i]);
	}
	for (int i = 0; i < num_sub_dic; i++)
	{
		int idx_i = *(pvec_pre + i) + quantizer_offset[i];
		for (int j = i; j < num_sub_dic; j++)
		{
			int idx_j = *(pvec_pre + j) + quantizer_offset[j];
			loss += *(pquan_ip + idx_i * num_quantizers + idx_j);
		}
	}
	return loss;
}

int NearestResidualAlg::cpu_nearest_in_ock(Matrix<DataType> &vec_x_map, 
										   Matrix<DataType> &quan_ip, 
										   const int* quantizer_offset, 
										   int num_sub_dic, 
										   CodeType* p_curr_code)
{
	DataType e = 0;

	Matrix<CodeType> vec_pre;
	CodeType* p_pre;
	if (m_is_use_pre)
	{
		vec_pre.resize(1, num_sub_dic);
		p_pre = vec_pre.mutable_cpu_ptr();
		memcpy(p_pre, p_curr_code, sizeof(CodeType) * num_sub_dic);
	}

	SolveOptimizedAdv2Recursive(vec_x_map, quan_ip, quantizer_offset, 
		num_sub_dic, 0, p_curr_code, e);

	int is_not_changed = 0;
	if (m_is_use_pre)
	{
		DataType after_value = cpu_residual_loss(vec_x_map, quan_ip, quantizer_offset, num_sub_dic, p_curr_code);
		DataType pre_value = cpu_residual_loss(vec_x_map, quan_ip, quantizer_offset, num_sub_dic, p_pre);
		if (after_value > pre_value)
		{
			is_not_changed = 1;
			memcpy(p_curr_code, p_pre, sizeof(CodeType) * num_sub_dic);
		}
	}
	return is_not_changed;
}

void NearestResidualAlg::BestNextWordsSMart(
	Matrix<DataType> &vec_x_map, 
	Matrix<DataType> &matInnerProduct, 
	const int* quantizer_offset, 
	int num_sub_dic, 
	const CodeType* prepresentation,
	int idx, 
	CodeType next_idx[], 
	DataType next_errors[])
{
	priority_queue<pair<CodeType, DataType>, vector<pair<CodeType, DataType> >,  
		LessPairSecond<CodeType, DataType> > pq;

	int sub_dic_size = quantizer_offset[idx + 1] - quantizer_offset[idx];

	int idx_start = quantizer_offset[idx];
	int idx_end = quantizer_offset[idx + 1];

	const DataType* pvec_x_map = vec_x_map.cpu_ptr();
	const DataType* pmatInnerProduct = matInnerProduct.cpu_ptr();
	int num_quantizers = matInnerProduct.cols();
	for (int i = idx_start; i < idx_end; i++)
	{
		// compoute the relative error
		DataType e = -(*(pvec_x_map + i));
		const DataType* p_inner = pmatInnerProduct + i * num_quantizers;
		for (int j = 0; j < idx; j++)
		{
			e += p_inner[prepresentation[j] + quantizer_offset[j]];
		}
		e += p_inner[i];

		if (pq.size() >= m_para.num_candidate)
		{
			const pair<CodeType, DataType> &p = pq.top();
			if (p.second > e)
			{
				pq.pop();
				pq.push(pair<CodeType, DataType>(i - idx_start, e));
			}
		}
		else
		{
			pq.push(pair<CodeType, DataType>(i - idx_start, e));
		}
	}

	SMART_ASSERT(m_para.num_candidate == pq.size())(m_para.num_candidate)(pq.size()).Fatal();
	for (int i = m_para.num_candidate - 1; i >= 0; i--)
	{
		const pair<CodeType, DataType> &p = pq.top();
		next_idx[i] = p.first;
		next_errors[i] = p.second;
		pq.pop();
	}
}

void NearestResidualAlg::SolveOptimizedAdv2Recursive(
	Matrix<DataType> &vec_x_map, 
	Matrix<DataType> &matInnerProduct, 
	const int* quantizer_offset, 
	int num_sub_dic, 
	int idx, 
	CodeType* prepresentation, 
	DataType &error) 
{
	if (idx >= num_sub_dic)
	{
		return;
	}
	const int MAX_NUM_CANDIDATE = 20;
	CodeType next_idx[MAX_NUM_CANDIDATE];
	DataType next_errors[MAX_NUM_CANDIDATE];

	BestNextWordsSMart(vec_x_map, matInnerProduct, 
		quantizer_offset, num_sub_dic, 
		prepresentation, idx, 
		next_idx, next_errors);

	DataType min_error = FLT_MAX;
	const int MAX_SPARSITY = 32;
	CodeType min_representation[MAX_SPARSITY];

	for (int i = 0; i < m_para.num_candidate; i++)
	{
		*(prepresentation + idx) = next_idx[i];

		next_errors[i] += error;
		SolveOptimizedAdv2Recursive(vec_x_map, 
			matInnerProduct, 
			quantizer_offset, 
			num_sub_dic, 
			idx + 1, 
			prepresentation, 
			next_errors[i]);

		if (min_error > next_errors[i])
		{
			min_error = next_errors[i];
			memcpy(min_representation, prepresentation, sizeof(CodeType) * num_sub_dic);
		}
	}
	error = min_error;
	memcpy(prepresentation, min_representation, sizeof(CodeType) * num_sub_dic);
}

//------------------------------
void IEncoderModel::CheckDifference(shared_ptr<IEncoderModel> &ref)
{

}
//-------------------------------------
VQEncoderModel::VQEncoderModel()
{
}
VQEncoderModel::VQEncoderModel(shared_ptr<Matrix<DataType> > &pmat_rotation, 
							   shared_ptr<vector<Matrix<DataType> > > &pvecmat_dics, 
							   shared_ptr<Matrix<int> > &pmat_quantizer_partition) :
m_pmat_rotation(pmat_rotation), 
	m_pvecmat_dics(pvecmat_dics), 
	m_pmat_quantizer_partition(pmat_quantizer_partition)
{
}

void VQEncoderModel::Set(shared_ptr<Matrix<DataType> > &pmat_rotation, 
						 shared_ptr<vector<Matrix<DataType> > > &pvecmat_dics, 
						 shared_ptr<Matrix<int> > &pmat_quantizer_partition)
{
	m_pmat_rotation = pmat_rotation;
	m_pvecmat_dics = pvecmat_dics;
	m_pmat_quantizer_partition = pmat_quantizer_partition;
}

void VQEncoderModel::CheckDifference(shared_ptr<IEncoderModel> &ref)
{
	VQEncoderModel* pref = (VQEncoderModel*)ref.get();

	if (! pref->m_pmat_rotation.get() && !m_pmat_rotation.get())
	{
		PRINT << "both have no rotations\n";
	}
	if (pref->m_pvecmat_dics->size() == m_pvecmat_dics->size())
	{
		size_t num_partitions = m_pvecmat_dics->size();
		for (size_t i = 0;  i < num_partitions; i++)
		{
			Matrix<DataType> &mat = m_pvecmat_dics->operator[](i);
			Matrix<DataType> &mat_ref = pref->m_pvecmat_dics->operator[](i);
			if (mat.rows() == mat_ref.rows() && mat.cols() == mat_ref.cols())
			{
				size_t total = mat.rows()  * mat.cols();
				PRINT << "Difference of " << i << "-th dic: " << 
					MatCompute::L1_distance(m_pvecmat_dics->operator[](i).cpu_ptr(), 
					pref->m_pvecmat_dics->operator[](i).cpu_ptr(),
					total) << "\n";
			}
		}
	}
}


//-----------------
void HierarchicalEncoder::set_start_initialize(
	shared_ptr<Matrix<DataType> > &pmat_rotation, 
	shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers)
{
	m_pmat_init_rotation = pmat_rotation;
	m_pvecmat_init_quantizers = pvecmat_quantizers;
}
//
HierarchicalEncoder::HierarchicalEncoder(
	int num_partitions, int num_sub_dic, int sub_dic_size, 
	shared_ptr<VQEncoder> pvqencoder)
	: m_num_partitions(num_partitions), m_num_sub_dic(num_sub_dic), 
	m_sub_dic_size(sub_dic_size)
{
	m_pvqencoder = pvqencoder;
	m_num_partitions = num_partitions;
	m_num_sub_dic = num_sub_dic;
	m_sub_dic_size = sub_dic_size;
	SMART_ASSERT(num_partitions > 0 && ((num_partitions - 1) & num_partitions) == 0)
		(num_partitions).Fatal();
	SMART_ASSERT(num_sub_dic > 0 && ((num_sub_dic - 1) & num_sub_dic) == 0)
		(num_sub_dic).Fatal();
	SMART_ASSERT(sub_dic_size > 0)(sub_dic_size).Fatal();
}

//
void start_initialize(Matrix<DataType> &mat_data, 
					  int n_partition, 
					  int sub_dic_size,
					  shared_ptr<Matrix<DataType> > &pmat_rotation, 
					  shared_ptr<vector<Matrix<DataType> > > &pvecmat_quantizers)
{
	int dim = mat_data.cols();
	int num_points = mat_data.rows();
	SMART_ASSERT((dim % n_partition) == 0)(dim).Fatal();

	// rotation as an identical matrix
	pmat_rotation->resize(dim, dim);
	memset(pmat_rotation->mutable_cpu_ptr(), 0, sizeof(DataType) * pmat_rotation->size());
	for (int i = 0; i < dim; i++)
	{
		pmat_rotation->set(i, i, 1);
	}

	// random set the quantizers. 
	pvecmat_quantizers.reset(new vector<Matrix<DataType> >(n_partition));
	int sub_dim = dim / n_partition;
	for (int i = 0; i < n_partition; i++)
	{
		Matrix<DataType> &mat_quantizers = pvecmat_quantizers->operator[](i);
		mat_quantizers.resize(sub_dic_size, sub_dim);
	}
	for (int i = 0; i < sub_dic_size; i++)
	{
		int s = (DataType)rand() / (DataType)RAND_MAX * num_points;
		s = (s >= num_points) ? num_points - 1 : s;
		const DataType* pdata = mat_data.cpu_ptr(s);
		for (int j = 0; j < n_partition; j++)
		{
			DataType* pquan = pvecmat_quantizers->operator[](j).mutable_cpu_ptr(i);
			memcpy(pquan, pdata, sizeof(DataType) * sub_dim);
			pdata += sub_dim;
		}
	}
}
//
void HierarchicalEncoder::construct_codebooks(
	shared_ptr<Matrix<DataType> > &pmat_rotation,
	shared_ptr<vector<Matrix<DataType> > > &pvecmat_in, 
	shared_ptr<vector<Matrix<DataType> > > &pvecmat_out)
{
	int num_partition_in = pvecmat_in->size();
	int num_partition_out = num_partition_in >> 1;
	SMART_ASSERT(num_partition_out > 0)(num_partition_in).Fatal();
	if (pvecmat_out.get() == NULL || pvecmat_out->size() != num_partition_out)
	{
		pvecmat_out.reset(new vector<Matrix<DataType> >(num_partition_out));
	}
	for (int i = 0; i < num_partition_out; i++)
	{
		int idx_in_0 = i << 1;
		int idx_in_1 = idx_in_0 + 1;
		Matrix<DataType> &mat_in0 = pvecmat_in->operator[](idx_in_0);
		Matrix<DataType> &mat_in1 = pvecmat_in->operator[](idx_in_1);
		int sub_dim = mat_in0.cols() + mat_in1.cols();
		int sub_num = mat_in0.rows() + mat_in1.rows();
		Matrix<DataType> &mat_out = pvecmat_out->operator[](i);
		mat_out.resize(sub_num, sub_dim);
		memset(mat_out.mutable_cpu_ptr(), 0, sizeof(DataType) * mat_out.size());
		for (int j = 0; j < mat_in0.rows(); j++)
		{
			memcpy(mat_out.mutable_cpu_ptr(j), 
				mat_in0.cpu_ptr(j), sizeof(DataType) * mat_in0.cols());
		}
		for (int j = 0; j < mat_in1.rows(); j++)
		{
			memcpy(mat_out.mutable_cpu_ptr(j + mat_in0.rows()) + mat_in0.cols(), 
				mat_in1.cpu_ptr(j), sizeof(DataType) * mat_in1.cols());
		}
	}

	if (pvecmat_out->size() == 1)
	{
		shared_ptr<vector<Matrix<DataType> > > rotated;
		rotated.reset(new vector<Matrix<DataType> >(1));
		rotated->operator[](0).resize(pvecmat_out->operator[](0).rows(),
			pvecmat_out->operator[](0).cols());
		MatCompute::cpu_multiple_a_b(pvecmat_out->operator[](0).cpu_ptr(), 
			pmat_rotation->cpu_ptr(), rotated->operator[](0).mutable_cpu_ptr(), 
			pvecmat_out->operator[](0).rows(), 
			pvecmat_out->operator[](0).cols(), 
			pmat_rotation->cols());

		memset(pmat_rotation->mutable_cpu_ptr(), 
			0, sizeof(DataType) * pmat_rotation->size());
		for (int i = 0; i < pmat_rotation->rows(); i++)
		{
			pmat_rotation->set(i, i, 1);
		}
	}
}
//
void HierarchicalEncoder::training(Matrix<DataType> &mat_data)
{
	int n_partition = m_num_partitions * m_num_sub_dic;
	int n_sub_dic = 1;
	shared_ptr<Matrix<DataType> > pmat_rotation;
	pmat_rotation.reset(new Matrix<DataType>());

	if (!m_pvecmat_init_quantizers.get())
	{
		start_initialize(mat_data, n_partition, m_sub_dic_size, m_pmat_init_rotation, 
			m_pvecmat_init_quantizers);
	}

	shared_ptr<vector<Matrix<DataType> > > pvecmat_quantizers_constructed;
	for (int i = 0; m_pvecmat_init_quantizers->size() != m_num_partitions; i++)
	{
		PRINT << m_pvecmat_init_quantizers->size() << "\n";
		m_pvqencoder->initialize(m_pmat_init_rotation, m_pvecmat_init_quantizers, 
			n_sub_dic);
		m_pvqencoder->training(mat_data);

		construct_codebooks(m_pmat_init_rotation, m_pvecmat_init_quantizers, 
			pvecmat_quantizers_constructed);
		m_pvecmat_init_quantizers = pvecmat_quantizers_constructed;
		n_sub_dic = n_sub_dic << 1;
	}
	m_pvqencoder->initialize(m_pmat_init_rotation, m_pvecmat_init_quantizers, 
		n_sub_dic);
	m_pvqencoder->training(mat_data);
	initialize(m_pmat_init_rotation, m_pvecmat_init_quantizers, m_num_sub_dic);
}

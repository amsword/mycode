#include <stdlib.h>
#include <stdio.h>
#include "encoder_test_set.h"
#include "hydragon.h"
#include "DiskIO.h"
#include "MatCompute.h"

int test_encoding(int argc, char* argv[])
{
	Matrix<double> mat_data;
	Matrix<unsigned char> mat_true_code;
	shared_ptr<IEncoder> encoder;

	shared_ptr<Matrix<DataType> > p_rotated_mat;
	shared_ptr<vector<Matrix<DataType> > > vec_mat_quantizers;

	while (EncoderTestSet::create_test_set(mat_data, encoder, mat_true_code) )
	{
		Matrix<CodeType> mat_code;
		encoder->encoding(mat_data, mat_code);

		PRINT << "finished\n";
		for (size_t row = 0; row < mat_code.rows(); row++)
		{
			for (size_t col = 0; col < mat_code.cols(); col++)
			{
				CodeType left = *(mat_code.cpu_ptr() + row * mat_code.cols() + col);
				CodeType right = *(mat_true_code.cpu_ptr() + row * mat_true_code.cols() + col);
				SMART_ASSERT(left == right)((int)left)((int)right)(row)(col).Fatal();
			}
		}
		PRINT << "congratulations on the test\n";
	}
	return 0;
}

void test_training()
{
	Matrix<DataType> mat_data;
	shared_ptr<IEncoder> encoder;

	shared_ptr<Matrix<DataType> > p_rotated_mat;
	shared_ptr<vector<Matrix<DataType> > > vec_mat_quantizers;

	shared_ptr<IEncoderModel> encoder_model, true_encoder_model;

	while (EncoderTestSet::create_training_set(mat_data, encoder, true_encoder_model) )
	{
		Matrix<CodeType> mat_code;
		encoder->training(mat_data);

		encoder->export_model(encoder_model);

		encoder_model->CheckDifference(true_encoder_model);
	}
}

int main(int argc, char* argv[])
{
	//try
	//{
	//test_encoding(argc, argv);
	test_training();
	//}
	//catch(...)
	//{
	//}
	return 0;
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Net;
using System.Web;

namespace c_sharp
{
    class Program
    {
        public static long FromHttp(string uri, string filename, string localFolder)
        {
            long totalBytesRead = 0;
            const int blockSize = 4096;
            Byte[] buffer = new Byte[blockSize];



            try
            {
                HttpWebRequest httpRequest =
          (HttpWebRequest)WebRequest.Create(Path.Combine(uri, filename));
                httpRequest.Method = "GET";
                // if the URI doesn't exist, an exception will be thrown here...
                using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = httpResponse.GetResponseStream())
                    {
                        using (FileStream localFileStream =
           new FileStream(Path.Combine(localFolder, filename), FileMode.Create))
                        {
                            int bytesRead;
                            while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                totalBytesRead += bytesRead;
                                localFileStream.Write(buffer, 0, bytesRead);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // You might want to handle some specific errors : Just pass on up for now...
                throw;
            }

            return totalBytesRead;
        }
        static void Main(string[] args)
        {
            
            string str_id_list = @"C:\Users\t0908482\Desktop\mine\data\ImageNet\image_list.txt";
            StreamReader sr = new StreamReader(str_id_list);
            WebClient client = new WebClient();
            string str_prefix_original1 = @"http://www.image-net.org/api/download/imagenet.sbow.synset?wnid=";
            string str_prefix_original2 = @"http://www.image-net.org/downloads/features/sbow/";
            string str_output_folder = @"C:\Users\t0908482\Desktop\mine\data\ImageNet";

            string str_line = "n02100735";
            string str_file = str_line + ".sbow.mat";
            client.DownloadFile(str_prefix_original2 + str_line + ".sbow.mat", str_output_folder + @"\" + str_file);


            //while (!sr.EndOfStream)
            //{
            //    str_line = sr.ReadLine();
            //    str_line = str_line.Trim();
            //    if (str_line.Length != 9)
            //    {
            //        continue;
            //    }

            //    str_file = str_line + ".sbow.mat";
            //    Console.WriteLine(str_file);
            //    //client.DownloadFile(str_prefix_original1 + str_line, str_output_folder + @"\" + str_file);
            //    client.DownloadFile(str_prefix_original2 + str_line + ".sbow.mat", str_output_folder + @"\" + str_file);
            //}
            sr.Close();
        }
    }
}